(******************************************************************************)
(* ApronUtil *)
(* utilities for manipulating APRON entities *)
(* author: Peter Schrammel *)
(* version: 0.9.0 *)
(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)
(******************************************************************************)

open Apron
open Format

type 'a man_t = 'a Manager.t
type env_t = Environment.t
type lincons0 = Lincons0.t
type linexpr0 = Linexpr0.t
type linexpr1 = Linexpr1.t
type lincons1 = Lincons1.t
type linconss = Lincons1.earray
type var_t = Var.t
type vars_t = Var.t array
type equ_t = var_t * linexpr1
type equs_t = equ_t array

type 'a num0 = 'a Abstract0.t
type 'a num1 = 'a Abstract1.t

(******************************************************************************)
(* printing functions *)
(******************************************************************************)
(** prints an APRON linear equation *)
let print_equation fmt (v,e) =
  Var.print fmt v;
  pp_print_string fmt "' = ";
  Linexpr1.print fmt e

(******************************************************************************)
(** prints an APRON abstract value *)
let print_abstract = Abstract1.print

(******************************************************************************)
(** prints APRON linear equations *)
let print_equations fmt equs =
  Array.iter
    (fun eq -> print_equation fmt eq;
      pp_print_string fmt "; "
    ) equs

(******************************************************************************)
(** prints an array of APRON variables *)
let print_vars fmt vars =
  pp_print_string fmt "[";
  Array.iter
    (fun v -> Var.print fmt v;
      pp_print_string fmt "; ")
    vars;
  pp_print_string fmt "]"

(******************************************************************************)
(** prints APRON linear constraints *)
let print_linconss fmt linconss = Lincons1.array_print fmt linconss

module PPrt = struct
  open Abstract0
  open Lincons0
  open Coeff
  open Scalar

  let pp_cmp: 'a -> (unit, formatter, unit) format = function
    | SUPEQ -> "@<1>≥"
    | SUP -> ">"
    | EQ | EQMOD _ -> "="
    | DISEQ -> "@<1>≠"

  let pp_revcmp: 'a -> (unit, formatter, unit) format = function
    | SUPEQ -> "@<1>≤"
    | SUP -> "<"
    | EQ | EQMOD _ -> "="
    | DISEQ -> "@<1>≠"

  let sgn_of c = match reduce c with
    | Scalar x -> Scalar.sgn x
    | Interval i -> if Interval.is_zero i then 0 else 1

  let zero = Apron.Coeff.s_of_int 0

  let pp_lincons assoc fmt cons =
    let coeff_neg_sgns = ref 0 in
    Linexpr0.iter (fun c _ -> let s = sgn_of c in
                           if s < 0 then incr coeff_neg_sgns
                           else if s > 0 then decr coeff_neg_sgns)
      cons.linexpr0;
    let cst = Linexpr0.get_cst cons.linexpr0 in
    let lexpr, pp_cmp, ncst =
      if !coeff_neg_sgns < 0
      then cons.linexpr0, pp_cmp, Coeff.neg cst
      else let lexpr = Linexpr0.copy cons.linexpr0 in
           let neg_coeff c d = Linexpr0.set_coeff lexpr d (Coeff.neg c) in
           Linexpr0.iter neg_coeff cons.linexpr0;
           lexpr, pp_revcmp, cst
    in
    Linexpr0.set_cst lexpr zero;
    Linexpr0.print assoc fmt lexpr;
    Linexpr0.set_cst lexpr cst;
    fprintf fmt "%(%)%a" (pp_cmp cons.typ) Coeff.print ncst;
    match cons.typ with
      | EQMOD x -> fprintf fmt " mod %a" Scalar.print x;
      | _ -> ()

  let pp_num0 assoc fmt a =
    let man = manager a
    and pp_lincons = pp_lincons assoc
    and pp_array = print_array ~first:"@[<hov 0>" ~sep:"@ ∧@ " ~last:"@]" in
    if is_bottom man a then fprintf fmt "@<1>⊥"
    else if is_top man a then fprintf fmt "@<1>⊤"
    else pp_array pp_lincons fmt (to_lincons_array man a)

end

let pp_cmp fmt t = fprintf fmt "%(%)" (PPrt.pp_cmp t)
let pp_revcmp fmt t = fprintf fmt "%(%)" (PPrt.pp_revcmp t)
let pp_lincons = PPrt.pp_lincons
let pp_num0 = PPrt.pp_num0

(******************************************************************************)
(* utilities *)
(******************************************************************************)
(** primes a variable *)
let get_primed_var v = Var.of_string ((Var.to_string v)^"'")

(******************************************************************************)
(** builds the constraint array  v'=e from the given equations *)
let get_fexpr get_primed_var env eqs =
  let minus_one = Coeff.s_of_int (-1) in
  let len = Array.length eqs in
  let fexpr = Lincons1.array_make env len in
  for i=0 to len-1 do begin
    let (v,e) = eqs.(i) in
    let lincons = Lincons1.make e Lincons1.EQ in
    Lincons1.set_coeff lincons (get_primed_var v) minus_one;
    Lincons1.array_set fexpr i lincons;
  end done;
  fexpr

(******************************************************************************)
(** gets the array of variables in the APRON environment *)
let vars_of_env env =
  let (ivars,rvars) = Environment.vars env in
  Array.append ivars rvars

(******************************************************************************)
(** returns the sub-environment containing the given variables *)
let env_of_vars env vars =
  let (ivars,rvars) =
    List.partition (fun v ->
      match Environment.typ_of_var env v with
	|Environment.INT -> true |_ -> false)
      (Array.to_list vars)
  in
  Environment.make (Array.of_list ivars) (Array.of_list rvars)

(******************************************************************************)
(** returns the number of dimensions of env *)
let dims_of_env env =
  let dim = Environment.dimension env in
  dim.Dim.intd+dim.Dim.reald

(******************************************************************************)
(** checks whether the expression depends on at least one of the given
    variables *)
let linexpr_depends_on_some_var e vars =
  let dims = dims_of_env (Linexpr1.get_env e) in
  let e = Linexpr1.get_linexpr0 e in
  let rec check_coeffs j =
    if j>=dims then false
    else
      if Coeff.is_zero (Linexpr0.get_coeff e j) then
        check_coeffs (j+1)
      else true
  in
  check_coeffs 0

(******************************************************************************)
(** creates the linexpr 0 *)
let make_zero_linexpr env =
  let dims = dims_of_env env in
  let zero = Coeff.s_of_int 0 in
  let linexpr = Linexpr0.make (Some dims) in
  Linexpr0.set_cst linexpr zero;
  for i=0 to dims-1 do
    Linexpr0.set_coeff linexpr i zero;
  done;
  {Linexpr1.linexpr0 = linexpr; Linexpr1.env = env}

(******************************************************************************)
(** creates equations (v,0) for the given variables *)
let get_zero_equations_for env vars =
  Array.map (fun v ->  (v,make_zero_linexpr env)) vars

(******************************************************************************)
(** creates the constraints (v=0) for the given variables *)
let get_zero_linconss_for env vars =
  let len = Array.length vars in
  let one = Coeff.s_of_int 1 in
  let conss = Lincons1.array_make env len in
  for i=0 to len-1 do
    let expr = Linexpr1.make env in
    Linexpr1.set_coeff expr vars.(i) one;
    let cons = Lincons1.make expr Lincons0.EQ in
    Lincons1.array_set conss i cons
  done;
  conss

(******************************************************************************)
(** APRON abstract bottom *)
let bottom0 man env =
  let dim = Environment.dimension env in
  Abstract0.bottom man dim.Dim.intd dim.Dim.reald
let top0 man env =
  let dim = Environment.dimension env in
  Abstract0.top man dim.Dim.intd dim.Dim.reald

let num0_of_num1 = Abstract1.abstract0

(******************************************************************************)
let abstract1_of_abstract0 env s =
  {Abstract1.abstract0 = s;
   Abstract1.env = env}

(******************************************************************************)
let lincons1_of_lincons0 env c =
  {Lincons1.lincons0 = c;
   Lincons1.env = env}

let linconss_of_num0 env s =
  {Lincons1.lincons0_array = Abstract0.to_lincons_array (Abstract0.manager s) s;
   Lincons1.array_env = env}

let linconss_of_num1 s =
  Abstract1.to_lincons_array (Abstract1.manager s) s

let fold_lincons0_of_num0 f s i =
  Array.fold_right f (Abstract0.to_lincons_array (Abstract0.manager s) s) i

(******************************************************************************)
(** rename primed variables to unprimed variables in the abstract value *)
let rename_primed_to_unprimed get_primed_var abstract1 vars =
  let env = Abstract1.env abstract1 in
  let man = Abstract1.manager abstract1 in
  let zero = Coeff.s_of_int 0 in
  let indexmap =
    Array.map (fun v ->
      (Environment.dim_of_var env (get_primed_var v),
       Environment.dim_of_var env v))
      vars in
  let earr = Abstract1.to_lincons_array man abstract1 in
  Array.iter
    (fun lincons0 ->
      let linexpr0 = lincons0.Lincons0.linexpr0 in
      Array.iter
        (fun (iprimed,i) ->
          Linexpr0.set_coeff linexpr0 i
            (Linexpr0.get_coeff linexpr0 iprimed);
          Linexpr0.set_coeff linexpr0 iprimed zero)
        indexmap)
    earr.Lincons1.lincons0_array;
  let dim = Environment.dimension env in
  {Abstract1.abstract0 =
      Abstract0.of_lincons_array man dim.Dim.intd
        dim.Dim.reald earr.Lincons1.lincons0_array;
   Abstract1.env = env}

(******************************************************************************)
(* operations on abstract values *)
(******************************************************************************)
(** converts the abstract value s to the domain of the given manager *)
let change_domain s man =
  let sman = Abstract1.manager s in
  let sarr = Abstract1.to_lincons_array sman s in
  Abstract1.of_lincons_array man (Abstract1.env s) sarr

(******************************************************************************)
(** generalized time elapse = s+t*d, t>=0 *)
let elapse s d =
  let man = Abstract1.manager d in
  let dgen = Abstract1.to_generator_array man d in
  for i=0 to (Generator1.array_length dgen)-1 do
    let g1 = Generator1.array_get dgen i in
    if Generator1.get_typ g1 == Generator0.VERTEX then
      Generator1.set_typ g1 Generator0.RAY
  done;
  Abstract1.add_ray_array man s dgen

(******************************************************************************)
(** generalized time elapse in the domain of d,
    the domain of s may be different *)
let elapse_with_domain s d =
  let dman = Abstract1.manager d in
  let snew = elapse (change_domain s dman) d in
  change_domain snew (Abstract1.manager s)

(******************************************************************************)
(** projects out the given variables from s, but keeps their dimensions *)
let project_forget s forget_vars =
  Abstract1.forget_array (Abstract1.manager s) s forget_vars false

(******************************************************************************)
(** projects the zero_vars of s onto the hyperplane
    where all zero_vars=0, i.e. (exists zero_vars.S)/\(/\zero_vars=0) *)
let project_to_zero s zero_vars =
  let man = Abstract1.manager s in
  let env = Abstract1.env s in
  let cp1 = Coeff.Scalar (Scalar.of_int 1) in
  let lexpr0 = Array.fold_left
    (fun lexprs v ->
      let e = Linexpr1.make env in
      Linexpr1.set_coeff e v cp1;
      e::lexprs)
    [] zero_vars
  in
  let linconsarr = Lincons1.array_make env (Array.length zero_vars) in
  Array.iteri (fun i lc -> Lincons1.array_set linconsarr i
    (Lincons1.make lc Lincons1.EQ))
    (Array.of_list lexpr0);
  let s0 = Abstract1.of_lincons_array man env linconsarr in
  Abstract1.meet man
    (Abstract1.forget_array man s zero_vars false) s0

(******************************************************************************)
(** negation of an abstract value: x in -S <=> -x in S *)
let neg s =
  let man = Abstract1.manager s in
  let env = Abstract1.env s in
  let linconsarr = Abstract1.to_lincons_array man s in
  for i=0 to (Lincons1.array_length linconsarr)-1 do
    begin
      let lincons = Lincons1.array_get linconsarr i in
      let linexpr = Lincons1.get_linexpr1 lincons in
      Linexpr1.iter
        (fun c v -> Linexpr1.set_coeff linexpr v (Coeff.neg c))
        linexpr
    end done;
  Abstract1.of_lincons_array man env linconsarr

(******************************************************************************)
(** builds abstract versions for each linear conditions in the given list,
    assuming they all share the same environment *)
let abstract_lincons_list man lc =
  let env = Lincons1.get_env (List.hd lc) in         (* assume same env *)
  let lcs = Lincons1.array_make env 1 in
  lc |> List.rev_map begin fun c ->
    Lincons1.array_set lcs 0 c;
    Abstract1.of_lincons_array man env lcs
  end |> List.rev

(******************************************************************************)
(** returns the translator d in the domain man *)
let get_translator man eqs g ivars =
  (*  Log.debug3_o logger (print_equations) "eqs = " eqs;
      Log.debug3_o logger (print_linconss) "g = " g;
      Log.debug3_o logger print_vars "ivars = " ivars;*)
  let env = Lincons1.array_get_env g in
  let dt = Lincons1.array_make env (Array.length eqs) in
  let minus_one = Coeff.s_of_int (-1) in
  Array.iteri
    (fun j eq ->
      let (v,expr) = eq in
      let newexpr = Linexpr1.copy expr in
      Linexpr1.set_coeff newexpr v minus_one;
      Lincons1.array_set dt j
        (Lincons1.make newexpr Lincons1.EQ)
    )
    eqs;
  (* J xi <= k /\ d = T xi + u *)
  let dxi = Abstract1.meet_lincons_array man
    (Abstract1.of_lincons_array man env g) dt in
  project_forget dxi ivars

(******************************************************************************)
let lincons0_is_eq c1 c2 =
  (c1.Lincons0.typ = c2.Lincons0.typ) &&
    ((Linexpr0.compare
        c1.Lincons0.linexpr0 c2.Lincons0.linexpr0)=0)

let linconss_is_eq a1 a2 =
  ((Lincons1.array_get_env a1)=(Lincons1.array_get_env a2)) &&
    (List.fold_right2
       (fun c1 c2 res -> res && (lincons0_is_eq c1 c2))
       (Array.to_list a1.Lincons1.lincons0_array)
       (Array.to_list a2.Lincons1.lincons0_array)
       true)

let linconss_empty env =
  {Lincons1.lincons0_array=[||];Lincons1.array_env=env}

(******************************************************************************)
(* OLD STUFF *)
(******************************************************************************)
(* removes the dimensions of the given variables from s *)
(*let project_remove s remove_vars =
  let spenv = Environment.remove (Abstract1.env s) remove_vars in
  Abstract1.change_environment (Abstract1.manager s) s spenv false
*)

(******************************************************************************)
(* adds the abstract values s1 and s2 point-by-point (Minkowski sum) *)
(*let add s1 s2 =
  let man = (Abstract1.manager s1) in
  assert(Environment.equal (Abstract1.env s1)
  (Abstract1.env s2));
  let (s1ivars,s1rvars) = Environment.vars (Abstract1.env s1) in
  let to_prime1 v = Var.of_string ((Var.to_string v)^"_") in
  let to_prime2 v = Var.of_string ((Var.to_string v)^"__") in
  let (s1nivars,s1nrvars) =
  ((Array.map to_prime1 s1ivars),(Array.map to_prime1 s1rvars)) in
  let (s2nivars,s2nrvars) =
  ((Array.map to_prime2 s1ivars),(Array.map to_prime2 s1rvars)) in

  let s1vars = Array.append s1ivars s1rvars in
  let s1nvars = Array.append s1nivars s1nrvars in
  let s2nvars = Array.append s2nivars s2nrvars in
  let s12nivars = Array.append (Array.append s1ivars s1nivars) s2nivars in
  let s12nrvars = Array.append (Array.append s1rvars s1nrvars) s2nrvars in
  let s12nenv = Environment.make s12nivars s12nrvars in
  let xvars = Array.mapi
  (fun i v -> (v,Array.get s1nvars i,Array.get s2nvars i)) s1vars in

  let s1n = Abstract1.change_environment man
  (Abstract1.rename_array man s1
  s1vars s1nvars)
  s12nenv false in

  let s2n = Abstract1.change_environment man
  (Abstract1.rename_array man s2
  s1vars s2nvars)
  s12nenv false in
  (* P' /\ Q''*)
  let s1n2n = Abstract1.meet man s1n s2n in
  (* /\x=x'+x'' *)
  let xexx =
  let n = Array.length s1vars in
  let cm1 = Coeff.Scalar (Scalar.of_int (-1)) in
  let cp1 = Coeff.Scalar (Scalar.of_int 1) in
  let linarr = Lincons1.array_make s12nenv n in
  Array.iteri
  (fun i xline ->
  let (x,x1,x2) = xline in
  let linexpr = Linexpr1.make s12nenv in
  Linexpr1.set_array linexpr
  (Array.of_list [(cp1,x);(cm1,x1);(cm1,x2)]) None;
  let lincons = Lincons1.make linexpr Lincons1.EQ in
  Lincons1.array_set linarr i lincons)
  xvars;
  linarr in
  (* P' /\ Q'' /\ /\x=x'+x'' *)
  let s12n = Abstract1.meet_lincons_array man s1n2n xexx in
  (* exists x',x''. P' /\ Q'' /\ /\x=x'+x'' *)
  Abstract1.change_environment  man s12n (Abstract1.env s1) false
*)
