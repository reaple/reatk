module type T = sig
  module Symb: SymbSig.T

  (* ------------------------------------------------------------------------ *)
  (** {2 Types} *)

  (* ------------------------------------------------------------------------ *)
  (** {3 Facilities} *)

  type positive = private int
  type symb = Symb.t
  module SMap = Symb.Map
  module SSet = Symb.Set

  (* ------------------------------------------------------------------------ *)
  (** {3 Typing} *)

  type typname = symb
  type label
  type 'f typdef' =
    | EnumDef of (label * 'f option) list
  type 'f typdef = 'f typdef' * 'f option
  type width = int
  type ntyp = [ `Int | `Real | `Bint of bool * width ]
  type etyp = [ `Enum of typname ]
  type typ = [ `Bool | etyp | ntyp ]

  (* ------------------------------------------------------------------------ *)
  (** {3 Expressions} *)

  type eqrel = [ `Eq | `Ne ]
  type totrel = [ eqrel | `Lt | `Le | `Gt | `Ge ]
  type buop = [ `Neg ]
  type bbop = [ `Imp ]
  type bnop = [ `Conj | `Disj | `Excl ]
  type nuop = [ `Opp ]
  type nnop = [ `Sum | `Sub | `Mul | `Div ]
  type luop = [ `LNot ]
  type lbop = [ `LAnd | `LOr | `LXor ]
  type lsop = [ `Lsl | `Lsr | `Asl | `Asr ]

  type ('t, 'b) cond = [ `Ite of 'b * 't * 't ]
  type ('t, 'f) flag = [ `Flag of 'f * 't ]

  type 'f nexp =
    [
    | `Ref of symb
    | `Int of int
    | `Real of float
    | `Mpq of Mpqf.t
    | `Ncst of bool * width * 'f nexp
    | `Nuop of nuop * 'f nexp
    | `Nnop of nnop * 'f nexp * 'f nexp * 'f nexp list
    | `Luop of luop * 'f nexp
    | `Lbop of lbop * 'f nexp * 'f nexp
    | `Lsop of lsop * 'f nexp * 'f nexp
    | ('a, 'f bexp) cond
    | ('a, 'f) flag
    ] as 'a
  and 'f eexp =
    [
    | `Ref of symb
    | `Enum of label
    | ('a, 'f bexp) cond
    | ('a, 'f) flag
    ] as 'a
  and 'f bexp =
    [
    | `Ref of symb
    | `Bool of bool
    | `Buop of buop * 'f bexp
    | `Bbop of bbop * 'f bexp * 'f bexp
    | `Bnop of bnop * 'f bexp * 'f bexp * 'f bexp list
    | `Bcmp of eqrel * 'f bexp * 'f bexp
    | `Ncmp of totrel * 'f nexp * 'f nexp
    | `Ecmp of eqrel * 'f eexp * 'f eexp
    | `Pcmp of eqrel * 'f exp * 'f exp
    | `Pin of 'f exp * 'f exp * 'f exp list
    | `Bin of 'f bexp * 'f bexp * 'f bexp list
    | `Ein of 'f eexp * 'f eexp * 'f eexp list
    | `BIin of 'f nexp * 'f nexp * 'f nexp list
    | ('a, 'f bexp) cond
    | ('a, 'f) flag
    ] as 'a
  and 'f exp =
    [
    | `Bexp of 'f bexp
    | `Eexp of 'f eexp
    | `Nexp of 'f nexp
    | `Ref of symb
    | ('a, 'f bexp) cond                       (** temporarily untyped expression *)
    | ('a, 'f) flag
    ] as 'a

  (* ------------------------------------------------------------------------ *)
  (** {3 Variable defintions} *)

  type group = positive
  type rank = positive
  type 'f default_spec = [ `None | `Expr of 'f exp | `Ascend | `Descend ]
  type 'f state_var_spec = [ `State of 'f exp * 'f option ]
  type 'f state_var_decl = [ `State' ]
  type 'f local_var_spec = [ `Local of 'f exp * 'f option ]
  type 'f input_var_spec = [ `Input of group ]
  type 'f contr_var_spec = [ `Contr of group * rank * 'f default_spec ]
  type 'f output_var_spec = [ `Output of group * 'f exp * 'f option ]
  type 'f var_spec =
    [
    | 'f state_var_spec
    | 'f state_var_decl
    | 'f local_var_spec
    | 'f input_var_spec
    | 'f contr_var_spec
    | 'f output_var_spec
    ]
  type ('f, +'s) decls = (typ * 's * 'f option) SMap.t
  constraint 's = [< 'f var_spec]

  val typof: (typ * _ * 'f option) SMap.t -> symb -> typ

  (* ------------------------------------------------------------------------ *)
  (** {3 Nodes} *)

  type 'f node_var_spec =
    [
    | 'f state_var_spec
    | 'f local_var_spec
    | 'f input_var_spec
    | 'f contr_var_spec
    ]
  type 'f node_decls = ('f, 'f node_var_spec) decls
  type 'f typdefs
  type 'f node_desc =
      {
        cn_typs: 'f typdefs;
        cn_decls: 'f node_decls;
        cn_init: 'f bexp;
        cn_assertion: 'f bexp;
        cn_invariant: 'f bexp option;
        cn_reachable: 'f bexp option;
        cn_attractive: 'f bexp option;
      }
  type 'f checked_node_desc
  type 'f node =
    [
    | `Node of 'f checked_node_desc
    | `Desc of 'f node_desc
    ]
  type 'f checked_node = private [< 'f node > `Node ]

  (* ------------------------------------------------------------------------ *)
  (** {3 CFGs} *)

  type 'f cfg_var_spec =
    [
    | 'f state_var_decl
    | 'f local_var_spec
    | 'f input_var_spec
    | 'f contr_var_spec
    ]
  type 'f cfg_decls = ('f, 'f cfg_var_spec) decls

  module CFG: sig
    type 'f updates = 'f state_var_spec SMap.t
    type 'f arc = private
        {
          arc_dest: label;
          arc_guard: 'f bexp option;
          arc_updates: 'f updates;
          arc_flag: 'f option;
        }
    type 'f arcs = private 'f arc list
    type 'f location = private
        {
          loc_name: label;
          loc_flag: 'f option;
          loc_arcs: 'f arcs;
          loc_init: 'f bexp option;
          loc_final: 'f bexp option;
          loc_assertion: 'f bexp option;
          loc_reachable: 'f bexp option;
        }
    type 'f t
    val no_arc: 'f arcs
    val prepend_arc
      : ?flag:'f -> ?guard:'f bexp -> 'f updates -> dest:label -> 'f arcs -> 'f arcs
    val append_arc
      : 'f arcs -> ?flag:'f -> ?guard:'f bexp -> 'f updates -> dest:label -> 'f arcs
    val iter_arcs: ('f arc -> unit) -> 'f arcs -> unit
    val fold_arcs: ('f arc -> 'a -> 'a) -> 'f arcs -> 'a -> 'a
    val fold_arcs_rev: ('f arc -> 'a -> 'a) -> 'f arcs -> 'a -> 'a
    val map_arcs: ('f arc -> 'f arc) -> 'f arcs -> 'f arcs
    val empty: ?flag: 'f -> ?loc_var:symb * 'f option -> ?loc_typ:typname * 'f option
      -> unit -> 'f t
    val location_var: 'f t -> symb option
    val location_var': 'f t -> (symb * 'f option) option
    val location_typ: 'f t -> typname option
    val location_typ': 'f t -> (typname * 'f option) option
    val has_location: label -> 'f t -> bool
    val add_location
      : 'f t -> ?flag:'f -> label -> ?init:'f bexp -> ?final:'f bexp
      -> ?assertion:'f bexp -> ?reachable:'f bexp
      -> 'f arcs -> 'f t
    val find_location: label -> 'f t -> 'f location
    val iter_locations: ('f location -> unit) -> 'f t -> unit
    val fold_locations: ('f location -> 'a -> 'a) -> 'f t -> 'a -> 'a
    val repl_locations: ('f location -> 'f location) -> 'f t -> 'f t
    val all_locations: 'f t -> (label * 'f option) list
  end

  type 'f cfg_desc =
      {
        cg_typs: 'f typdefs;
        cg_decls: 'f cfg_decls;
        cg_graph: 'f CFG.t;
        cg_init: 'f bexp;
        cg_assertion: 'f bexp;
        cg_invariant: 'f bexp option;
        cg_reachable: 'f bexp option;
        cg_attractive: 'f bexp option;
      }
  type 'f checked_cfg_desc
  type 'f cfg =
    [
    | `Cfg of 'f checked_cfg_desc
    | `Desc of 'f cfg_desc
    ]
  type 'f checked_cfg = private [< 'f cfg > `Cfg ]

  (* ------------------------------------------------------------------------ *)
  (** {3 Functions} *)

  type 'f func_var_spec =
    [
    | 'f local_var_spec
    | 'f output_var_spec
    | 'f input_var_spec
    ]
  type 'f func_decls = ('f, 'f func_var_spec) decls
  type 'f func_desc =
      {
        fn_typs: 'f typdefs;
        fn_decls: 'f func_decls;
        fn_assertion: 'f bexp;
      }
  type 'f checked_func_desc
  type 'f func =
    [
    | `Func of 'f checked_func_desc
    | `Desc of 'f func_desc
    ]
  type 'f checked_func = private [< 'f func > `Func ]

  (* ------------------------------------------------------------------------ *)
  (** {3 Predicates} *)

  type 'f pred_var_spec =
    [
    | 'f local_var_spec
    | 'f contr_var_spec
    | 'f input_var_spec
    ]
  type 'f pred_decls = ('f, 'f pred_var_spec) decls
  type 'f pred_desc =
      {
        pn_typs: 'f typdefs;
        pn_decls: 'f pred_decls;
        pn_value: 'f bexp;
      }
  type 'f checked_pred_desc
  type 'f pred =
    [
    | `Pred of 'f checked_pred_desc
    | `Desc of 'f pred_desc
    ]
  type 'f checked_pred = private [< 'f pred > `Pred ]

  (* ------------------------------------------------------------------------ *)
  (** {2 Utilities} *)

  val mk_symb: string -> symb
  val mk_fresh_symb: ?cnt_ref:int ref -> string -> symb
  val one: positive
  val succ: positive -> positive

  (* ------------------------------------------------------------------------ *)
  (** {3 Building, declaring and manipulating types} *)

  val empty_typdefs: 'f typdefs
  val declare_typ: typname -> 'f typdef -> 'f typdefs -> 'f typdefs
  val mem_typ: typname -> 'f typdefs -> bool
  val mk_typname: symb -> typname
  val mk_label: symb -> label
  val mk_etyp: ?flag:'f -> label list -> 'f typdef
  val mk_etyp': ?flag:'f -> (label * 'f) list -> 'f typdef
  val mk_etyp'': ?flag:'f -> (label * 'f option) list -> 'f typdef
  val label_symb: label -> symb
  val fold_typdefs: (typname -> 'f typdef -> 'a -> 'a) -> 'f typdefs -> 'a -> 'a
  val find_typdef: typname -> 'f typdefs -> 'f typdef
  val label_typs: 'f typdefs -> (typ * 'f option) SMap.t
  val label_typ: [`Tds of 'f typdefs | `Map of (typ * 'f option) SMap.t] -> label
    -> typ

  (* ------------------------------------------------------------------------ *)
  (** {3 Building expressions} *)

  (** The functions bellow are helpers for building expressions.

      Non-primed functions are given polymorphic expressions ({!exp}) and raise
      {!TypeError} exceptions when actual types mismatch; primed versions do not, as
      they take arguments of type {!bexp}, {!nexp} or {!eexp} directly. Note a
      "relaxed" type checking is performed, e.g., to verify that Boolean
      expressions are not present where a numerical one is expected; the actual
      types of variables is not taken into account. *)

  val mk_bref' : symb -> 'f bexp
  val mk_bcst' : bool -> 'f bexp
  val mk_neg' : 'f bexp -> 'f bexp
  val mk_and' : 'f bexp -> 'f bexp -> 'f bexp
  val mk_or' : 'f bexp -> 'f bexp -> 'f bexp
  val mk_xor' : 'f bexp -> 'f bexp -> 'f bexp
  val mk_imp' : 'f bexp -> 'f bexp -> 'f bexp
  val mk_conj' : 'f bexp -> 'f bexp list -> 'f bexp
  val mk_disj' : 'f bexp -> 'f bexp list -> 'f bexp
  val mk_excl' : 'f bexp -> 'f bexp list -> 'f bexp
  val mk_pin' : 'f exp -> 'f exp -> 'f exp list -> 'f bexp
  val mk_bin' : 'f bexp -> 'f bexp -> 'f bexp list -> 'f bexp
  val mk_ein' : 'f eexp -> 'f eexp -> 'f eexp list -> 'f bexp
  val mk_biin' : 'f nexp -> 'f nexp -> 'f nexp list -> 'f bexp
  val mk_beq' : 'f bexp -> 'f bexp -> 'f bexp
  val mk_eeq' : 'f eexp -> 'f eexp -> 'f bexp
  val mk_neq' : 'f nexp -> 'f nexp -> 'f bexp
  val mk_peq' : 'f exp -> 'f exp -> 'f bexp
  val mk_bne' : 'f bexp -> 'f bexp -> 'f bexp
  val mk_ene' : 'f eexp -> 'f eexp -> 'f bexp
  val mk_nne' : 'f nexp -> 'f nexp -> 'f bexp
  val mk_pne' : 'f exp -> 'f exp -> 'f bexp
  val mk_lt' : 'f nexp -> 'f nexp -> 'f bexp
  val mk_le' : 'f nexp -> 'f nexp -> 'f bexp
  val mk_gt' : 'f nexp -> 'f nexp -> 'f bexp
  val mk_ge' : 'f nexp -> 'f nexp -> 'f bexp

  val mk_eref' : symb -> 'f eexp
  val mk_ecst' : label -> 'f eexp

  val mk_nref' : symb -> 'f nexp
  val mk_nicst' : int -> 'f nexp
  val mk_nbicst' : bool -> width -> int -> 'f nexp
  val mk_ncast' : bool -> width -> 'f nexp -> 'f nexp
  val mk_nrcst' : float -> 'f nexp
  val mk_nqcst' : Mpqf.t -> 'f nexp
  val mk_opp' : 'f nexp -> 'f nexp
  val mk_sum' : 'f nexp -> 'f nexp -> 'f nexp list -> 'f nexp
  val mk_sub' : 'f nexp -> 'f nexp -> 'f nexp list -> 'f nexp
  val mk_mul' : 'f nexp -> 'f nexp -> 'f nexp list -> 'f nexp
  val mk_div' : 'f nexp -> 'f nexp -> 'f nexp list -> 'f nexp
  val mk_lnot' : 'f nexp -> 'f nexp
  val mk_land' : 'f nexp -> 'f nexp -> 'f nexp
  val mk_lxor' : 'f nexp -> 'f nexp -> 'f nexp
  val mk_lor' : 'f nexp -> 'f nexp -> 'f nexp
  val mk_lsl' : 'f nexp -> 'f nexp -> 'f nexp
  val mk_lsr' : 'f nexp -> 'f nexp -> 'f nexp
  val mk_asl' : 'f nexp -> 'f nexp -> 'f nexp
  val mk_asr' : 'f nexp -> 'f nexp -> 'f nexp

  (* val mk_cond' : 'f bexp -> ([> ('a, 'f bexp) cond] as 'a) -> 'a -> 'a *)
  val mk_cond' : 'f bexp -> ('f exp as 'a) -> 'a -> 'a
  val mk_bcond' : 'f bexp -> 'f bexp -> 'f bexp -> 'f bexp
  val mk_econd' : 'f bexp -> 'f eexp -> 'f eexp -> 'f eexp
  val mk_ncond' : 'f bexp -> 'f nexp -> 'f nexp -> 'f nexp

  (* - *)

  exception TypeError of (Format.formatter -> unit)

  val as_bexp: 'f exp -> 'f bexp
  val as_eexp: 'f exp -> 'f eexp
  val as_nexp: 'f exp -> 'f nexp

  val mk_ref: symb -> 'f exp

  type 'f bexp' = [ `Bexp of 'f bexp ]
  and 'f eexp' = [ `Eexp of 'f eexp ]
  and 'f nexp' = [ `Nexp of 'f nexp ]

  val mk_bref : symb -> [> 'f bexp' ]
  val mk_bcst : bool -> [> 'f bexp' ]
  val mk_neg : 'f exp -> [> 'f bexp' ]
  val mk_and : 'f exp -> 'f exp -> [> 'f bexp' ]
  val mk_or : 'f exp -> 'f exp -> [> 'f bexp' ]
  val mk_imp : 'f exp -> 'f exp -> [> 'f bexp' ]
  val mk_xor : 'f exp -> 'f exp -> [> 'f bexp' ]
  val mk_conj : 'f exp -> 'f exp list -> [> 'f bexp' ]
  val mk_disj : 'f exp -> 'f exp list -> [> 'f bexp' ]
  val mk_excl : 'f exp -> 'f exp list -> [> 'f bexp' ]
  val mk_eq : 'f exp -> 'f exp -> [> 'f bexp' ]
  val mk_ne : 'f exp -> 'f exp -> [> 'f bexp' ]
  val mk_lt : 'f exp -> 'f exp -> [> 'f bexp' ]
  val mk_le : 'f exp -> 'f exp -> [> 'f bexp' ]
  val mk_gt : 'f exp -> 'f exp -> [> 'f bexp' ]
  val mk_ge : 'f exp -> 'f exp -> [> 'f bexp' ]

  val mk_in : 'f exp -> 'f exp -> 'f exp list -> [> 'f bexp' ]
  val mk_bin : 'f exp -> 'f exp -> 'f exp list -> [> 'f bexp' ]
  val mk_ein : 'f exp -> 'f exp -> 'f exp list -> [> 'f bexp' ]
  val mk_biin : 'f exp -> 'f exp -> 'f exp list -> [> 'f bexp' ]

  val mk_eref : symb -> [> 'f eexp' ]
  val mk_ecst : label -> [> 'f eexp' ]

  val mk_nref : symb -> [> 'f nexp' ]
  val mk_nicst : int -> [> 'f nexp' ]
  val mk_nbicst : bool -> width -> int -> [> 'f nexp' ]
  val mk_ncast : bool -> width -> 'f exp -> [> 'f nexp' ]
  val mk_nrcst : float -> [> 'f nexp' ]
  val mk_nqcst : Mpqf.t -> [> 'f nexp' ]
  val mk_opp : 'f exp -> [> 'f nexp' ]
  val mk_sum : 'f exp -> 'f exp -> 'f exp list -> [> 'f nexp' ]
  val mk_sub : 'f exp -> 'f exp -> 'f exp list -> [> 'f nexp' ]
  val mk_mul : 'f exp -> 'f exp -> 'f exp list -> [> 'f nexp' ]
  val mk_div : 'f exp -> 'f exp -> 'f exp list -> [> 'f nexp' ]
  val mk_lnot : 'f exp -> [> 'f nexp' ]
  val mk_land : 'f exp -> 'f exp -> [> 'f nexp' ]
  val mk_lxor : 'f exp -> 'f exp -> [> 'f nexp' ]
  val mk_lor : 'f exp -> 'f exp -> [> 'f nexp' ]
  val mk_lsl : 'f exp -> 'f exp -> [> 'f nexp' ]
  val mk_lsr : 'f exp -> 'f exp -> [> 'f nexp' ]
  val mk_asl : 'f exp -> 'f exp -> [> 'f nexp' ]
  val mk_asr : 'f exp -> 'f exp -> [> 'f nexp' ]

  val mk_cond : 'f exp -> 'f exp -> 'f exp -> 'f exp
  val mk_condb : 'f bexp -> 'f exp -> 'f exp -> 'f exp

  (* ------------------------------------------------------------------------ *)
  (** {3 Manipulating expressions} *)

  val fold_exp_dependencies': (label -> 'a -> 'a) -> (symb -> 'a -> 'a) -> 'f exp -> 'a -> 'a
  val fold_exp_dependencies: (symb -> 'a -> 'a) -> 'f exp -> 'a -> 'a
  val exp_dependencies: 'f exp -> SSet.t

  (* ------------------------------------------------------------------------ *)
  (** {3 Flag management} *)

  val flag: 'f -> ([> ('t, 'f) flag] as 't) -> 't
  val flagof: [> ('t, 'f) flag] -> 'f option
  val deflag: ([> ('t, 'f) flag] as 't) -> 't
  val apply: ('f option -> ([> ('t, 'f) flag] as 't) -> 'a) -> 't -> 'a
  val apply': (?flag:'f -> ([> ('t, 'f) flag] as 't) -> 'a) -> 't -> 'a
  val deflag_bexp: ?max_depth:int -> 'f bexp -> 'f bexp
  val deflag_eexp: ?max_depth:int -> 'f eexp -> 'f eexp
  val deflag_nexp: ?max_depth:int -> 'f nexp -> 'f nexp
  val deflag_exp: ?max_depth:int -> 'f exp -> 'f exp

  (* ------------------------------------------------------------------------ *)
  (** {3 Other kinds of description} *)

  type 'f uc_info =
      {
        cnig_input_vars: typ SMap.t;
        cnig_contr_vars: (typ * rank * 'f default_spec) SMap.t;
        cnig_contr_vars': (symb * typ * 'f default_spec) list;
      }
  type 'f node_info =
      {
        cni_state_vars: typ SMap.t;
        cni_uc_vars: 'f uc_info list;
        cni_local_vars: typ SMap.t;
        cni_local_specs: 'f exp SMap.t;
        cni_trans_specs: 'f exp SMap.t;
      }
  val gather_node_info: [< 'f node] -> 'f node_info
  val gather_cfg_info: [< 'f cfg] -> 'f node_info

  type io_info =
      {
        fnig_input_vars: typ SMap.t;
        fnig_output_vars: typ SMap.t;
      }
  type 'f func_info =
      {
        fni_io_vars: io_info list;
        fni_local_vars: typ SMap.t;
        fni_all_specs: 'f exp SMap.t;
      }
  val gather_func_info: [< 'f func] -> 'f func_info

  type 'f pred_info =
      {
        pni_uc_vars: 'f uc_info list;
        pni_local_vars: typ SMap.t;
        pni_local_specs: 'f exp SMap.t;
      }
  val gather_pred_info: [< 'f pred] -> 'f pred_info

  val node_desc: [< 'f node] -> 'f node_desc
  val cfg_desc: [< 'f cfg] -> 'f cfg_desc
  val func_desc: [< 'f func] -> 'f func_desc
  val pred_desc: [< 'f pred] -> 'f pred_desc

  (* ------------------------------------------------------------------------ *)
  (** {2 Consistency checking} *)

  type 'f loc_msg =
    [
    | `MError of ('f option * (Format.formatter -> unit)) list
    ]

  type 'f equs = (symb * 'f exp) list

  val check_node: [< 'f node] -> 'f checked_node option * 'f loc_msg list
  val check_cfg: [< 'f cfg] -> 'f checked_cfg option * 'f loc_msg list
  val check_func: [< 'f func] -> 'f checked_func option * 'f loc_msg list
  val check_pred: [< 'f pred] -> 'f checked_pred option * 'f loc_msg list
  val sorted_node_definitions: 'f checked_node -> 'f equs
  val sorted_cfg_definitions: 'f checked_cfg -> 'f equs
  val sorted_func_definitions: 'f checked_func -> 'f equs
  val sorted_pred_definitions: 'f checked_pred -> 'f equs

  (* ------------------------------------------------------------------------ *)
  (** {3 Delayed checking of expressions} *)

  type 'f exp_checker = ?flag:'f -> ?typ:typ -> 'f exp -> ('f exp, 'f loc_msg list) result
  val exp_checker_from_node: 'f checked_node -> 'f exp_checker
  val exp_checker_from_cfg: 'f checked_cfg -> 'f exp_checker
  val exp_checker_from_func: 'f checked_func -> 'f exp_checker
  val exp_checker_from_pred: 'f checked_pred -> 'f exp_checker

  (* ------------------------------------------------------------------------ *)
  (** {2 Operations} *)

  val split_func: 'f checked_func -> 'f func list

  (* ------------------------------------------------------------------------ *)
  (** {2 Pretty-printing} *)

  open Format

  val print_symb: formatter -> symb -> unit
  val print_typdef: formatter -> 'f typdef -> unit
  val print_typdefs: formatter -> 'f typdefs -> unit
  val print_typ: formatter -> typ -> unit
  val print_bexp: formatter -> 'f bexp -> unit
  val print_eexp: formatter -> 'f eexp -> unit
  val print_nexp: formatter -> 'f nexp -> unit
  val print_exp: formatter -> 'f exp -> unit
  val print_node: ?print_header:(formatter -> string -> string -> unit) ->
    formatter -> [< 'f node] -> unit
  val print_cfg: ?print_header:(formatter -> string -> string -> unit) ->
    formatter -> [< 'f cfg] -> unit
  val print_func: ?print_header:(formatter -> string -> string -> unit) ->
    formatter -> [< 'f func] -> unit
  val print_pred: ?print_header:(formatter -> string -> string -> unit) ->
    formatter -> [< 'f pred] -> unit

end

(* -------------------------------------------------------------------------- *)
