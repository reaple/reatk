(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)

(** utilities for BddApron *)

exception NotLinearAction of string (** a BddApron expression is not convertible
                                        into an APRON linear expression *)

exception NotSupported of string (** some feature is not supported *)


type 'a env_t = 'a Bddapron.Env.t (** BddApron environment *)
type 'a cond_t = 'a Bddapron.Cond.t (** BddApron conditions *)
type 'a boolexpr_t = 'a Bddapron.Expr0.Bool.t (** boolean expression (BDD) *)
type 'a numexpr_t = 'a Bddapron.Expr0.Apron.t (** numerical expression *)
type ('a, 'b, 'c, 'd) doman_t = ('a, 'b, 'c, 'd) Bddapron.Domain0.man
                                     (** BddApron domain manager *)
type 'a var_t = 'a  (** variable *)
type 'a vars_t = 'a list  (** list of variables *)
type 'a vset = 'a PSette.t
type 'a vararr_t = 'a array  (** array of variables *)
type 'd abstract_t = 'd Bddapron.Domain0.t (** BddApron abstract value *)
type 'a expr_t = 'a Bddapron.Expr0.t (** BddApron expression *)
type 'a equ_t = 'a * 'a expr_t (** BddApron equation *)
type 'a equs_t = 'a equ_t list (** BddApron equations *)
type 'a num_t = 'a Bddapron.Apronexpr.t
type 'a action_t = 'a num_t                       (** leaf of a BddApron numerical
                                                    expression *)
type 'a actions_t = 'a action_t array                      (** array of actions *)

(** {2 Printing } *)

(** prints a boolean expression (a BDD) *)
val print_boolexpr : 'a env_t -> 'a cond_t -> Format.formatter ->
  'a boolexpr_t -> unit

(** prints a BddApron equation *)
val print_equation : ?cont:bool -> 'a env_t -> 'a cond_t -> Format.formatter ->
  'a equ_t -> unit

(** prints BddApron equations *)
val print_equations : ?cont:bool -> 'a env_t -> 'a cond_t ->
  Format.formatter -> 'a equs_t -> unit

(** prints a BddApron abstract value *)
val print_abstract : 'a env_t -> ('a, 'b, 'c, 'd) doman_t ->
  Format.formatter -> 'd abstract_t -> unit

(** prints a BddApron expression value *)
val print_expr : 'a env_t -> 'a cond_t -> Format.formatter -> 'a expr_t -> unit

(** prints a list of variables *)
val print_vars : 'a env_t -> Format.formatter -> 'a vars_t -> unit


(** {2 BDD variable permutation helpers } *)

val permute_boolexpr: int array -> ('a boolexpr_t as 'b) -> 'b
val permute_numexpr: int array -> ('a numexpr_t as 'b) -> 'b
val permute_expr: int array -> ('a expr_t as 'b) -> 'b
val permute_equs: int array -> ('a equs_t as 'b) -> 'b

val permute_boolexpr': int array option -> ('a boolexpr_t as 'b) -> 'b
val permute_numexpr': int array option -> ('a numexpr_t as 'b) -> 'b
val permute_expr': int array option -> ('a expr_t as 'b) -> 'b
val permute_equs': int array option -> ('a equs_t as 'b) -> 'b

(** {2 Operations on Boolean expressions } *)

(** simplifies a boolean expression by phi (and the careset) *)
val simplify_boolexpr
  : 'a env_t -> 'a cond_t
  -> ?enums:'a boolexpr_t
  -> 'a boolexpr_t -> 'a boolexpr_t -> 'a boolexpr_t

(** splits a boolean expression by phi *)
val split_boolexpr : 'a env_t -> 'a cond_t ->
  'a boolexpr_t -> 'a boolexpr_t -> 'a boolexpr_t *  'a boolexpr_t

(** computes the careset of meaningful values for an enumerated type variable *)
val get_enum_careset_for_var : 'a env_t -> 'a cond_t -> 'a var_t -> 'a var_t ->
  'a boolexpr_t

val full_enum_careset: 'a env_t -> 'a cond_t -> 'a boolexpr_t

(** removes meaningless values of enumerated types from boolexpr *)
val normalize_benumneg : 'a env_t -> 'a cond_t -> 'a boolexpr_t -> 'a boolexpr_t

(** removes meaningless values of enumerated types from general expression *)
val normalize_enumneg : 'a env_t -> 'a cond_t -> 'a expr_t -> 'a expr_t

val boolexpr_foldcut_numconvex: 'a env_t -> 'a cond_t ->
  ('n -> 'a boolexpr_t -> 'a Bddapron.Cond.cond -> 'n list) -> 'n ->
  ('b -> 'a boolexpr_t -> 'n -> 'b) -> 'b ->
  'a boolexpr_t -> 'b

val vdd_foldcut_numconvex: 'a env_t -> 'a cond_t ->
  ('n -> 'a boolexpr_t -> 'a Bddapron.Cond.cond -> 'n list) -> 'n ->
  ('b -> 'x Cudd.Vdd.t -> 'n -> 'b) -> 'b ->
  'x Cudd.Vdd.t -> 'b

(** splits a boolexpr in a list of
    (numerical constraint conjunction /\ boolean bdd *)
val boolexpr_to_numconvex_list : 'a env_t -> 'a cond_t ->
  'a boolexpr_t -> 'a boolexpr_t list

(** returns the list of conjunctions (paths) of the given bdd *)
val boolexpr_to_dnf : 'a env_t -> 'a boolexpr_t -> 'a boolexpr_t list

(** traverses the list of conjunctions (paths) of the given bdd *)
val boolexpr_fold_paths: 'a env_t -> ('a boolexpr_t -> 'b -> 'b) -> 'a boolexpr_t -> 'b -> 'b

(** removes the expressions in boolexprlist from boolexpr
    by tdrestrict *)
val boolexpr_remove_exprs : 'a env_t -> 'a cond_t ->
  'a boolexpr_t -> 'a boolexpr_t list -> 'a boolexpr_t

(** computes the boolean expression with topologically closed constraints
    (the expression will be approximated by the conjunction of an
    expression over boolean variables and
    a conjunction of numerical constraints *)
val boolexpr_topoclose : 'a env_t -> 'a cond_t -> ('a, 'b, 'c, 'd) doman_t ->
  'a boolexpr_t -> 'a boolexpr_t

(** splits a boolexpr into a list of boolexprs
    with convex numerical constraints *)
val boolexpr_to_numconvex_list2 : ?forget_vars:'a vars_t -> 'a env_t ->
  'a cond_t -> ('a, 'b, 'c, 'd) doman_t -> 'a boolexpr_t -> 'a boolexpr_t list

(** removes variables from a boolexpr,
    assuming that the numerical constraints are convex *)
val boolexpr_forget_vars : 'a env_t -> 'a cond_t -> ('a, 'b, 'c, 'd) doman_t ->
  'a vars_t -> 'a boolexpr_t -> 'a boolexpr_t

(** removes the variables in the given support from the boolean expression *)
val boolexpr_forget_supp : 'a boolexpr_t -> 'a boolexpr_t -> 'a boolexpr_t

(** removes the variables in the given support from the non-numerical expression *)
val expr_forget_supp : 'a boolexpr_t -> 'a expr_t -> 'a expr_t

(** removes the numerical constraints from a boolean expression *)
val boolexpr_remove_cond : 'a env_t -> 'a cond_t -> 'a boolexpr_t -> 'a boolexpr_t

(** computes the number of boolean states represented by boolexpr
    (boolexpr is supposed to contain only boolean state variables) *)
val bool_space_size : 'a env_t -> 'a cond_t -> 'a vars_t -> 'a boolexpr_t -> int



(** {2 Operations on expressions and equations} *)

(** simplifies an equation by the given expression (and the careset) *)
val simplify_equ
  : 'a env_t -> 'a cond_t
  -> ?enums:'a boolexpr_t
  -> 'a boolexpr_t
  -> 'a equ_t -> 'a equ_t

(** simplifies an equation system by the given expression (and the careset) *)
val simplify_equs
  : 'a env_t -> 'a cond_t
  -> ?enums:'a boolexpr_t
  -> 'a boolexpr_t
  -> 'a equs_t -> 'a equs_t

(** returns the set of guards in the given equations *)
val get_guards_equs : 'a env_t -> 'a equs_t ->  'a boolexpr_t PSette.t

(** returns the identity transition function for a set of variables *)
val get_id_equs_for : 'a env_t -> 'a cond_t -> 'a vars_t -> 'a equs_t

(** returns the =cst 0 transition function for a set of numerical variables *)
val get_zero_equs_for : 'a env_t -> 'a cond_t -> 'a vars_t -> 'a equs_t

(** computes the boolean expression x'=f(x) *)
val get_fexpr : ('a var_t -> 'a var_t) -> 'a env_t -> 'a cond_t ->
  'a equs_t -> 'a boolexpr_t

(** computes the list of boolean expressions x'=f(x) *)
val get_fexprlist : ('a var_t -> 'a var_t) -> 'a env_t -> 'a cond_t ->
  'a equs_t -> 'a boolexpr_t list

val diseq_linexpr: 'a env_t -> 'a num_t -> ApronUtil.lincons1 * ApronUtil.lincons1

(** {2 Operations on abstract values} *)

(** convexifies the given BddApron value *)
val abstract_convexify : 'a env_t -> 'a cond_t -> ('a, 'b, 'c, 'd) doman_t ->
  'd abstract_t -> 'd abstract_t

(** changes the domain of the given abstract value *)
val change_domain : 'a env_t -> ('a, 'b, 'c, 'd) doman_t -> 'd abstract_t
  -> ('a, 'e, 'f, 'g) doman_t -> 'g abstract_t

(** {2 Checking equations and expressions} *)

(** checks whether a non-numerical equation is either identity or constant *)
val is_id_or_const_equ : 'a env_t -> 'a cond_t -> 'a equ_t -> bool

(** checks whether all non-numerical equations
    are either identity or constant *)
val is_id_or_const_equs : 'a env_t -> 'a cond_t -> 'a equs_t -> bool

(** checks whether all equations are identity *)
val is_id_equs : 'a env_t -> 'a cond_t -> 'a equs_t -> bool

(** returns true if the boolean expression contains only numerical constraints*)
val is_num_boolexpr : 'a cond_t -> 'a boolexpr_t -> bool

(** checks whether the numerical equation is x'<>0 *)
val is_nonzero_equ : 'a env_t -> 'a equ_t -> bool

(** checks whether the equation is numeric *)
val is_num_equ : 'a env_t -> 'a equ_t -> bool

(** checks whether expr depends on the variable v *)
val depends_on_var_expr : 'a env_t -> 'a cond_t -> 'a expr_t -> 'a var_t -> bool

(** checks whether expr depends on at least one variable in vars *)
val depends_on_some_var_expr : 'a env_t -> 'a cond_t -> 'a expr_t ->
  'a vars_t -> bool

(** checks whether the equations depend on at least one variable in vars *)
val depends_on_some_var_equs : 'a env_t -> 'a cond_t -> 'a equs_t ->
  'a vars_t -> bool



(** {2 Variables and support} *)

val fintyp_size: 'a env_t -> 'a Bdd.Typ.t -> int
val typ_finsize: 'a env_t -> [> 'a Bdd.Typ.t] -> int
val typs_finsize: 'a env_t -> [> 'a Bdd.Typ.t] list -> int
val vars_finsize: 'a env_t -> 'a vars_t -> int

(** Suffix used to prime variables; changes to this variable must be done before
    any call to either {!get_primed_var'}, {!get_primed_var}, or
    {!get_unprimed_var}. *)
val prime_suffix: string ref

(** primes a variable *)
val get_primed_var': 'a Bdd.Symb.t -> 'a var_t -> 'a var_t
val get_primed_var : 'a env_t -> 'a var_t -> 'a var_t

(** unprimes a variable *)
val get_unprimed_var : 'a env_t -> 'a var_t -> 'a var_t

(** returns the boolean (and enumerated) variables *)
val boolvars_of_env : 'a env_t -> 'a vars_t

(** returns the support of the given variables *)
val supp_of_vars : 'a env_t -> 'a vars_t -> 'a boolexpr_t

(** computes the support set of the leaves of an Bddapron.Apron expression *)
val get_numleaf_suppset : 'a env_t -> 'a expr_t -> 'a var_t PSette.t

(** primes all variables in the expression *)
val get_primed_expr : ?ignorevars:'a var_t PSette.t -> ('a var_t -> 'a var_t) -> 'a env_t -> 'a cond_t -> 'a expr_t -> 'a expr_t

(** primes all variables in the boolean expression *)
val get_primed_boolexpr : ?ignorevars:'a var_t PSette.t -> ('a var_t -> 'a var_t) -> 'a env_t -> 'a cond_t -> 'a boolexpr_t -> 'a boolexpr_t

(** un-primes all variables in the expression *)
val get_unprimed_expr : ('a var_t -> 'a var_t) -> 'a env_t -> 'a cond_t -> 'a expr_t -> 'a expr_t

(** un-primes all variables in the boolean expression *)
val get_unprimed_boolexpr : ('a var_t -> 'a var_t) -> 'a env_t -> 'a cond_t -> 'a boolexpr_t -> 'a boolexpr_t

(** {2 Product of equations} *)

(** builds the product factorizes a numerical transition function
    by common actions *)
val product_numequs : 'a env_t -> 'a action_t MtbddUtil.array_table_t ->
  'a equs_t ->
  'a action_t MtbddUtil.array_mtbdd_t

(** builds the product factorizes a transition function by common actions *)
val product_equs : 'a env_t ->
  'a MtbddUtil.poly_t MtbddUtil.arrset_table_t ->  'a equs_t ->
  'a MtbddUtil.poly_t MtbddUtil.arrset_mtbdd_t


(** {2 Conversions} *)

val var_to_apronvar : 'a env_t -> 'a var_t -> ApronUtil.var_t
val vars_to_apronvars : 'a env_t -> 'a vars_t -> ApronUtil.vars_t

val apronvar_to_var : 'a env_t -> ApronUtil.var_t -> 'a var_t
val apronvars_to_vars : 'a env_t -> ApronUtil.vars_t -> 'a vars_t

(** converts a Bddapron numerical leaf expression into a
    Bddapron.Apron expression *)
val apronexpr_to_apronexprDD : 'a env_t -> 'a action_t ->
  'a Bddapron.Expr0.Apron.t

(** extracts the Bddapron numerical leaf expression from
    a Bddapron.Apron expression (provided that there is a single leaf) *)
val apronexprDD_to_apronexpr : 'a env_t -> 'a Bddapron.Expr0.Apron.t ->
  'a action_t

(** converts an APRON expression into a BddApron numerical expression *)
val linexpr_to_apronexprDD : 'a env_t -> ApronUtil.linexpr1 ->
  'a Bddapron.Expr0.Apron.t

(** converts an APRON constraint into a boolean expression *)
val lincons_to_boolexpr : 'a env_t -> 'a cond_t -> ApronUtil.lincons1 ->
  'a boolexpr_t

(** converts a conjunction of APRON constraints into a
    BddApron boolean expression *)
val linconss_to_boolexpr : 'a env_t -> 'a cond_t -> ApronUtil.linconss ->
  'a boolexpr_t
val linconss_to_boolexpr': 'a env_t -> 'a cond_t -> ApronUtil.linconss ->
  'a boolexpr_t

(** converts an APRON abstract value into a
    BddApron boolean expression *)
val apron_to_boolexpr : 'a env_t -> 'a cond_t -> 'b ApronUtil.man_t ->
  'b ApronUtil.num1 -> 'a boolexpr_t
val apron_to_boolexpr': 'a env_t -> 'a cond_t -> 'b ApronUtil.man_t ->
  'b ApronUtil.num1 -> 'a boolexpr_t

(** builds a BddApron abstract value from a (purely) boolean expression and
    an APRON abstract value *)
val bddapron_to_abstract : 'a env_t -> ('a, 'b, 'c, 'd) doman_t ->
  'a boolexpr_t -> 'b ApronUtil.num1 -> 'd abstract_t

(** converts a boolean expression into a BddApron abstract value *)
val boolexpr_to_abstract : 'a env_t -> 'a cond_t -> ('a, 'b, 'c, 'd) doman_t ->
  'a boolexpr_t -> 'd abstract_t

(** converts a BddApron abstract value into a boolean expression *)
val abstract_to_boolexpr : 'a env_t -> 'a cond_t -> ('a, 'b, 'c, 'd) doman_t ->
  'd abstract_t -> 'a boolexpr_t


(** converts a BddApron.Apronexpr.t into an Apron.Linexpr1.t *)
val apronaction_to_linexpr : 'a env_t -> 'a cond_t -> 'a action_t ->
  ApronUtil.linexpr1

(** extracts the numerical part of a boolean expression as a polyhedron;
    only exact for convex expressions *)
val boolexpr_to_linconss : 'a env_t -> 'a cond_t->
  ('a, 'b, 'c, 'd) doman_t -> 'a vars_t -> 'a boolexpr_t -> ApronUtil.linconss

(** converts a BddApron.Expr0.Apron.t into an Apron.Linexpr1.t,
    provided that the guard equals true *)
val apronexpr_to_linexpr : 'a env_t -> 'a cond_t ->
  'a Bddapron.Expr0.Apron.t ->  ApronUtil.linexpr1

(** extracts a list of equations with APRON linear expressions from
    BDD APRON equations *)
val numequs_of_equs : 'a env_t -> 'a cond_t -> 'a equs_t -> ApronUtil.equs_t

(** extracts a list of equations with APRON linear expressions from
    BDD APRON equations *)
val numequs_of_equs_with_guards : 'a env_t -> 'a cond_t ->
  'a vars_t -> 'a equs_t ->
  ('a boolexpr_t * ApronUtil.equs_t) list

(******************************************************************************)

module type BDDAPRON_TYPES = sig
  type var
  type typ = var Bddapron.Typ.t
  type vars = var vars_t
  type bexpr = var boolexpr_t
  type nexpr = var numexpr_t
  type expr = var expr_t
  type equ = var equ_t
  type equs = var equs_t
  type vset = var PSette.t
  type 'a vmap = (var, 'a) PMappe.t
  type equmap = expr vmap
end

val bddaprontypes_module: (module BDDAPRON_TYPES with type var = 'a)

module type BDDAPRON_ENV = sig
  include BDDAPRON_TYPES
  val env: var env_t
  val cond: var cond_t
end

val bddapronenv_module: 'a env_t -> 'a cond_t ->
  (module BDDAPRON_ENV with type var = 'a)

(* --- *)

module EnvUtils: functor (E: BDDAPRON_ENV) -> sig
  open E
  val cuddman: Cudd.Man.vt
  val apronenv: unit -> Apron.Environment.t
  val symbol: var Bdd.Symb.t
  val memvar: var -> bool
  val vartyp: var -> typ
  val is_num: var -> bool
  val has_nums: vars -> bool
  val empty_vset: vset
  val empty_vmap: 'a vmap
  val varset: vars -> vset
end

(* --- *)

(** Fast substitutions of finite variables, to build pairs of functions
    [(to_prime, of_prime)]; take care that only one pair of such resulting
    routines can be used at any time. *)
module type FAST_SUBSTS = sig
  type var
  type expr
  val setup_fast_substs'
    : ?of_prime_first:bool
    -> (var * var) list
    -> (expr -> expr) * (expr -> expr)
  val setup_fast_substs
    : ?of_prime_first:bool
    -> (var, var) PMappe.t
    -> (expr -> expr) * (expr -> expr)
end

(* --- *)

module BOps: functor (E: BDDAPRON_ENV) -> sig
  open E
  include module type of EnvUtils (E)
  val var: var -> bexpr
  val tt: bexpr
  val ff: bexpr
  val (!~): bexpr -> bexpr
  val (&&~): bexpr -> bexpr -> bexpr
  val (||~): bexpr -> bexpr -> bexpr
  val (=>~): bexpr -> bexpr -> bexpr
  val (==~): bexpr -> bexpr -> bexpr
  val ite: bexpr -> bexpr -> bexpr -> bexpr
  val is_tt: bexpr -> bool
  val is_ff: bexpr -> bool
  val is_cst: bexpr -> bool
  val is_eq: bexpr -> bexpr -> bool
  val is_leq: bexpr -> bexpr -> bool
  val (<=>): bexpr -> bexpr -> bool
  val (//~): bexpr -> equs -> bexpr
  val cofactor: bexpr -> bexpr -> bexpr
  val (^^~): bexpr -> bexpr -> bexpr
  val (^~): bexpr -> bexpr -> bexpr
  val simplify: ?enums:bexpr -> bexpr -> bexpr
  val enums_careset: unit -> bexpr
  val permute: int array -> bexpr -> bexpr
  type support = (* private *) bexpr
  val support': bexpr -> support
  val exist': support -> bexpr -> bexpr
  val forall': support -> bexpr -> bexpr
  val existand': support -> bexpr -> bexpr -> bexpr
  val project': support -> bexpr -> bexpr
  (** [bsupp] computes the support based on finite variables only. *)
  val bsupp: vars -> support
  (** [bsupp'] also takes {e infinite variables} into account, so the computed
      support may comprise interpreted BDD variables (that proxy arithmetic
      conditions). *)
  val bsupp': vars -> support
  val varsupp: vars -> support
  val support: bexpr -> vset
  val exist: vars -> bexpr -> bexpr
  val forall: vars -> bexpr -> bexpr
  val project: vars -> bexpr -> bexpr
  val vsubst: bexpr -> var -> expr -> bexpr
  val vsubsts: bexpr -> equs -> bexpr
  val vsubst_vars: bexpr -> (var * var) list -> bexpr
  val print: Format.formatter -> bexpr -> unit
  val print': Format.formatter -> bexpr -> unit
  include FAST_SUBSTS with type var := var and type expr := bexpr
  module BddType: Util.ORDERED_TYPE with type t = bexpr
end

(* --- *)

module NOps: functor (E: BDDAPRON_ENV) -> sig
  open E
  open Apron
  type 'a binop = nexpr -> nexpr -> 'a
  val var: var -> nexpr
  val cst': Scalar.t -> Scalar.t -> nexpr
  val cst: Scalar.t -> nexpr
  val csti: int -> nexpr
  val cstf: float -> nexpr
  val cstm: Mpqf.t -> nexpr
  val zero: nexpr
  val one: nexpr
  val add: nexpr binop
  val sub: nexpr binop
  val mul: nexpr binop
  val div: nexpr binop
  val ( +.. ): nexpr binop
  val ( -.. ): nexpr binop
  val ( *.. ): nexpr binop
  val ( /.. ): nexpr binop
  val negate: nexpr -> nexpr
  val eqz: nexpr -> bexpr
  val supz: nexpr -> bexpr
  val supeqz: nexpr -> bexpr
  val ( ==.. ): bexpr binop
  val ( <.. ): bexpr binop
  val ( >.. ): bexpr binop
  val ite: bexpr -> nexpr -> nexpr -> nexpr
  val ( //.. ): nexpr -> equs -> nexpr
  val cofactor: nexpr -> bexpr -> nexpr
  val ( ^^.. ): nexpr -> bexpr -> nexpr
  val ( ^.. ): nexpr -> bexpr -> nexpr
  include FAST_SUBSTS with type var := var and type expr := nexpr
end

(* --- *)

module XOps: functor (E: BDDAPRON_ENV) -> sig
  open E
  open Apron
  type xexpr = var Bddapron.AerexprDD.t
  type 'a binop = xexpr -> xexpr -> 'a
  val of_nexpr: nexpr -> xexpr
  val table: var Bddapron.AerexprDD.table
  val of_arith: var Bddapron.AerexprDD.expr -> xexpr
  val var: var -> xexpr
  val cst': Scalar.t -> Scalar.t -> xexpr
  val cst: Scalar.t -> xexpr
  val csti: int -> xexpr
  val cstf: float -> xexpr
  val cstm: Mpqf.t -> xexpr
  val zero: xexpr
  val one: xexpr
  val plus_inf: xexpr
  val minus_inf: xexpr
  val nan: xexpr
  val is_nan: xexpr -> bool
  val is_limit: xexpr -> bool
  val ( =.~ ): xexpr -> xexpr -> bool
  val add: xexpr binop
  val sub: xexpr binop
  val mul: xexpr binop
  val div: xexpr binop
  val ( +.~ ): xexpr binop
  val ( -.~ ): xexpr binop
  val ( *.~ ): xexpr binop
  val ( /.~ ): xexpr binop
  val negate: xexpr -> xexpr
  val eqz: xexpr -> bexpr
  val supz: xexpr -> bexpr
  val supeqz: xexpr -> bexpr
  val ( ==.~ ): bexpr binop
  val ( <.~ ): bexpr binop
  val ( >.~ ): bexpr binop
  val ite: bexpr -> xexpr -> xexpr -> xexpr
  val ( //.~ ): xexpr -> equs -> xexpr
  val cofactor: xexpr -> bexpr -> xexpr
  val ( ^^.~): xexpr -> bexpr -> xexpr
  val ( ^.~ ): xexpr -> bexpr -> xexpr
  val simplify: xexpr -> xexpr
  val print: Format.formatter -> xexpr -> unit
  val permute: int array -> xexpr -> xexpr
  val support: xexpr -> vset
  val nan_guard: xexpr -> bexpr
  val ( !!.~ ): xexpr -> xexpr
  val to_linexpr0: xexpr -> ApronUtil.linexpr0
  val to_texpr0: xexpr -> Apron.Texpr0.t
  module XArith: sig
    type t = var Bddapron.Aerexpr.t
    type coeff = Bddapron.Aerexpr.Coeff.t
    include Util.ORDERED_TYPE with type t := t
    include Hashtbl.HashedType with type t := t
    val of_xexpr: xexpr -> t
    val var: var -> t
    val cst: Apron.Coeff.t -> t
    val cst': Bddapron.Aerexpr.Coeff.t -> t
    val zero: t
    val is_zero: t -> bool
    val is_limit: t -> bool
    val add: t -> t -> t
    val sub: t -> t -> t
    val mul: t -> t -> t
    val negate: t -> t
    val support: t -> vset
    val to_linexpr0: t -> ApronUtil.linexpr0
    module Lin: sig
      type t = var Bddapron.Aerexpr.Lin.t
      include Util.ORDERED_TYPE with type t := t
      include Hashtbl.HashedType with type t := t
      val var_coeff: t -> var -> coeff option
      val term: coeff -> var -> t
      val negate: t -> t
      val add: t -> t -> t
      val sub: t -> t -> t
      val scale: coeff -> t -> t
      val map_terms: (coeff * var -> coeff * var) -> t -> t
      val project_out': vset -> t -> t
    end
    val of_lin: Lin.t -> t
    val to_lin: t -> Lin.t
    module Cond: sig
      type e = t
      type t = var Bddapron.Aerexpr.Condition.t
      type t' = [ `Cond of t | `Bool of bool ]
      val print: Format.formatter -> t -> unit
      val support: t -> vset
      val to_bexpr: t' -> bexpr
      val of_lincons: ApronUtil.lincons0 -> t'
    end
    val gt: t -> t -> Cond.t'
    val ge: t -> t -> Cond.t'
    val lt: t -> t -> Cond.t'
    val le: t -> t -> Cond.t'
  end
  val fold: (bexpr -> XArith.t -> 'a -> 'a) -> xexpr -> 'a -> 'a
  val fold_u: (bexpr -> XArith.t Cudd.Mtbdd.unique -> 'a -> 'a) -> xexpr -> 'a -> 'a
end

(* --- *)

module POps: functor (E: BDDAPRON_ENV) -> sig
  open E
  include module type of EnvUtils (E)
  val var: var -> expr
  val ite: bexpr -> expr -> expr -> expr
  val of_bexpr: bexpr -> expr
  val to_bexpr: expr -> bexpr
  val of_nexpr: nexpr -> expr
  val to_nexpr: expr -> nexpr
  val ( ==~~ ): expr -> expr -> bexpr
  val support: expr -> vset
  val support': expr -> bexpr
  val cofactor: expr -> bexpr -> expr
  val ( ^^~~ ): expr -> bexpr -> expr
  val ( ^~~ ): expr -> bexpr -> expr
  val simplify_equ: ?enums:bexpr -> bexpr -> equ -> equ
  val simplify_equs: ?enums:bexpr -> bexpr -> equs -> equs
  val enums_careset: unit -> bexpr
  val equmap: equs -> equmap
  val depends_on_var: expr -> var -> bool
  val depends_on_any_var: expr -> vset -> bool
  val evols_supports: ?exclude:vset -> equs -> vset vmap
  val subst_vars: (var * var) list -> expr -> expr
  val subst_vars': vars -> vars -> expr -> expr
  val ( //~ ): expr -> equs -> expr
  val substmap: equmap -> expr -> expr
  val ( ///~ ): expr list -> equs -> expr list
  val substmap_list: equmap -> expr list -> expr list
  val gencof_elim: bexpr -> vars -> expr -> expr
  val vsubsts: equs -> expr -> expr
  val setup_fast_substs''
    : ('x -> 'x)
    -> ?of_prime_first:bool
    -> (var * var) list
    -> ('x -> 'x) * ('x -> 'x)
  include FAST_SUBSTS with type var := var and type expr := expr
  module Arith: sig
    type t = var Bddapron.Apronexpr.t
    include Hashtbl.HashedType with type t := t
    include Util.ORDERED_TYPE with type t := t
    val support: t -> vset
    val to_nexpr: t -> nexpr
    val is_zero: t -> bool
    val add: t -> t -> t
    val sub: t -> t -> t
    module Cond: sig
      type t = var Bddapron.Apronexpr.Condition.t
      val support: t -> vset
    end
  end
  val labels: var -> bexpr -> vars
end

(* --- *)

module PPrt: functor (E: BDDAPRON_ENV) -> sig
  open E

  type 'a pp = Format.formatter -> 'a -> unit
  val pp_var: var pp
  val pp_vars: var list pp
  val pp_varr: var array pp
  val pp_vset: vset pp
  val pp_bexpr: bexpr pp
  val pp_nexpr: nexpr pp
  val pp_expr: expr pp
  val pp_equ: equ pp
  val pp_evol: equ pp
  val pp_equs: equs pp
  val pp_evols: equs pp
  val pp_equmap: expr vmap pp

  val pp_vmap: 'a pp -> 'a vmap pp
  val pp_lst: 'a pp -> 'a list pp

  val pp_env: Format.formatter -> unit
  val pp_cond: Format.formatter -> unit
  val pp_cond_info: Format.formatter -> unit
  val pp_careset: Format.formatter -> unit
  val pp_apronenv: Format.formatter -> unit
end

(* --- *)

module BEnvExt: functor (E: BDDAPRON_ENV) -> sig
  open E
  type upgrade
  val no_upgrade: upgrade
  val upgrade_of_perms: int array option -> upgrade
  val upgrade_to_perms: upgrade -> int array option
  val upgrade_expr: upgrade -> expr -> expr
  val upgrade_bexpr: upgrade -> bexpr -> bexpr
  val upgrade: (int array -> 'a -> 'a) -> upgrade -> 'a -> 'a
  val upgrade': (int array -> 'a -> 'a) -> upgrade -> 'a option -> 'a option
  val compose_upgrades: upgrade -> upgrade -> upgrade
  type extension
  val restore_environment: extension -> upgrade
  val make_primes: vset -> var vmap * extension * upgrade
  val decl_vars: (int * bool) vmap
    -> var array vmap * var vmap * extension * upgrade
end

(* --- *)

module type BDDAPRON_ENV_NS = sig
  module BEnv: BDDAPRON_ENV
  include BDDAPRON_TYPES with type var = BEnv.var
  module PPrt: module type of PPrt (BEnv)
  module BOps: module type of BOps (BEnv)
  module NOps: module type of NOps (BEnv)
  module POps: module type of POps (BEnv)
  module PAll: sig
    include BDDAPRON_TYPES with type var = var
    include module type of PPrt
    include module type of POps
  end
  module BAll: sig
    include module type of PAll
    include module type of BOps
  end
  module NAll: sig
    include module type of PAll
    include module type of NOps
  end
  module BEnvExt: module type of BEnvExt (BEnv)
  module BddSet: module type of Util.MakeSet (BOps.BddType)
  module BddMap: module type of Util.MakeMap (BOps.BddType)
end

module BEnvNS: functor (E: BDDAPRON_ENV) -> BDDAPRON_ENV_NS with module BEnv = E

(* -------------------------------------------------------------------------- *)

module CTrv: functor (BEnvNS: BDDAPRON_ENV_NS) -> sig
  open BEnvNS

  (* --- *)

  module type CTRV = sig
    type t
    val foldcut_numconvex:
      convex:('b -> bexpr -> var Bddapron.Apronexpr.Condition.t -> 'b list) ->
      diseq:('b -> bexpr -> var Bddapron.Apronexpr.expr -> 'b list) ->
      'b -> ('a -> t -> 'b -> 'a) -> 'a -> t -> 'a
    val fold_numconvex: bexpr -> ('a -> t -> bexpr -> 'a) -> 'a -> t -> 'a
  end
  module Bool: CTRV with type t := bexpr
  module Num: CTRV with type t := nexpr
  module XNum (X: sig type xexpr = var Bddapron.AerexprDD.t end)
    : CTRV with type t := X.xexpr
end

(* -------------------------------------------------------------------------- *)

module CaresetMan: functor (E: BDDAPRON_ENV) -> sig
  type t
  val make: ?update_period:int -> int -> t
  val maybe_update_n_val: ?force:bool -> t -> E.var boolexpr_t
  val force_careset_update: unit -> unit
end

(* -------------------------------------------------------------------------- *)
