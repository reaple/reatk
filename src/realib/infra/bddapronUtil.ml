(******************************************************************************)
(* BddapronUtil *)
(* utilities for manipulating BDD APRON entities *)
(* author: Peter Schrammel *)
(* version: 0.9.0 *)
(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)
(******************************************************************************)

let logger = Log.mk ~level:Log.Debug ".B"

exception NotLinearAction of string (** a BddApron expression is not convertible
                                        into an APRON linear expression *)

exception NotSupported of string (** some feature is not supported *)


type 'a env_t = 'a Bddapron.Env.t (** BddApron environment *)
type 'a cond_t = 'a Bddapron.Cond.t (** BddApron conditions *)
type 'a boolexpr_t = 'a Bddapron.Expr0.Bool.t (** boolean expression (BDD) *)
type 'a numexpr_t = 'a Bddapron.Expr0.Apron.t (** numerical expression *)
type ('a, 'b, 'c, 'd) doman_t = ('a, 'b, 'c, 'd) Bddapron.Domain0.man
                                     (** BddApron domain manager *)
type 'a var_t = 'a  (** variable *)
type 'a vars_t = 'a list  (** list of variables *)
type 'a vset = 'a PSette.t
type 'a vararr_t = 'a array  (** array of variables *)
type 'd abstract_t = 'd Bddapron.Domain0.t (** BddApron abstract value *)
type 'a expr_t = 'a Bddapron.Expr0.t (** BddApron expression *)
type 'a equ_t = 'a * 'a expr_t (** BddApron equation *)
type 'a equs_t = 'a equ_t list (** BddApron equations *)
type 'a num_t = 'a Bddapron.Apronexpr.t
type 'a action_t = 'a num_t                       (** leaf of a BddApron numerical
                                                    expression *)
type 'a actions_t = 'a action_t array                      (** array of actions *)

(******************************************************************************)
(* printing *)
(******************************************************************************)

(** prints a boolean expression (a BDD) *)
let print_boolexpr env cond fmt boolexpr =
  Bddapron.Expr0.Bool.print env cond fmt boolexpr

(** prints a BddApron equation *)
let print_equation ?(cont=false) env cond fmt (v,e) =
  if cont then Format.pp_print_string fmt ".";
  env.Bdd.Env.symbol.Bdd.Env.print fmt v;
  if not cont then Format.pp_print_string fmt "'";
  Format.pp_print_string fmt " = ";
  Bddapron.Expr0.print env cond fmt e

(** prints BddApron equations *)
let print_equations ?(cont=false) env cond =
  Util.list_print' ~copen:(fun fmt -> ()) ~cclose:(fun fmt -> ())
    ~csep:(fun fmt -> Format.pp_print_space fmt ())
    (fun fmt x -> print_equation env cond fmt x; Format.fprintf fmt ";")

(** prints a BddApron abstract value *)
let print_abstract env doman fmt s =
  Bddapron.Domain0.print doman env fmt s

(** prints a BddApron expression value *)
let print_expr env cond fmt expr =
  Bddapron.Expr0.print env cond fmt expr

(** prints a list of variables *)
let print_vars env fmt vars =
  Util.list_print (env.Bdd.Env.symbol.Bdd.Env.print) fmt vars

(******************************************************************************)
(* basic helpers for BDD variable permutations *)
(******************************************************************************)
let permute_boolexpr p b = Bddapron.Expr0.Bool.permute b p
let permute_boolexpr' = function
  | None -> fun e -> e
  | Some p -> fun e -> Bddapron.Expr0.Bool.permute e p

let permute_numexpr p b = Bddapron.Expr0.Apron.permute b p
let permute_numexpr' = function
  | None -> fun e -> e
  | Some p -> fun e -> Bddapron.Expr0.Apron.permute e p

let permute_expr p e = Bddapron.Expr0.permute e p
let permute_expr' = function
  | None -> fun e -> e
  | Some p -> fun e -> Bddapron.Expr0.permute e p

let permute_equs p e = List.map (fun (v, e) -> v, Bddapron.Expr0.permute e p) e
let permute_equs' = function
  | None -> fun e -> e
  | Some p -> (fun e -> List.map (fun (v, e) -> (v, Bddapron.Expr0.permute e p)) e)

(******************************************************************************)
(* operations on boolean expressions *)
(******************************************************************************)
(** simplifies a boolean expression by phi (and the careset) *)
let simplify_boolexpr env cond ?enums expr phi =
  let phi2 = Bddapron.Expr0.Bool.dand env cond phi cond.Bdd.Cond.careset in
  let phi2 = match enums with
    | None -> phi2
    | Some e -> Bddapron.Expr0.Bool.dand env cond phi2 e
  in
  (* fun expr -> *)
    if not (Bddapron.Expr0.Bool.is_cst env cond phi2)
    then Bddapron.Expr0.Bool.tdrestrict expr phi2
    else expr

(******************************************************************************)
(** splits a boolean expression by phi
       e /\ phi, e /\ -phi *)
let split_boolexpr env cond e phi =
  let e1 = Bddapron.Expr0.Bool.tdrestrict
    (Bddapron.Expr0.Bool.dand env cond e phi)
    cond.Bdd.Cond.careset
  in
  let e2 = Bddapron.Expr0.Bool.tdrestrict
    (Bddapron.Expr0.Bool.dand env cond e
      (Bddapron.Expr0.Bool.dnot env cond phi))
    cond.Bdd.Cond.careset
  in
  (e1,e2)

(******************************************************************************)
(** Quicker computation of carsets for enumerations.  There before it is
    included in module {!Bdd.Enum}. *)
let bdd_enum_welldef
    (Bdd.Env.{ cudd; symbol } as env)
    (Bdd.Enum.{ typ; reg }: 'd Bdd.Enum.t) : 'd Cudd.Bdd.t
    =
  let open Bdd in
  let maxcode = Enum.maxcode_of_typ env (symbol.Env.unmarshal typ) in
  Cudd.Bdd.dand
    (Reg.highereq_int cudd reg 0)
    (Reg.highereq cudd (Reg.of_int env.cudd (Reg.width reg) maxcode) reg)

(** computes the careset of meaningful values for an enumerated type variable *)
let get_enum_careset_for_var env cond var typname =
  bdd_enum_welldef env (Bddapron.Expr0.Benum.var env cond var)

(******************************************************************************)
(** removes meaningless values of enumerated types from general expression *)
let normalize_enumneg env cond expr =
  let supp = Bddapron.Expr0.support env cond expr in
  PSette.fold
    (fun v e ->
      match Bddapron.Env.typ_of_var env v with
	|`Benum(typname) ->
            let careset = get_enum_careset_for_var env cond v typname in
            Bddapron.Expr0.tdrestrict e careset
	|_ -> e)
    supp expr

(******************************************************************************)
(** computes the careset of all meaningful values for every enumerated type
    variables in the given environment *)
let full_enum_careset env cond =
  PMappe.fold (fun v -> function
    | `Benum t -> Cudd.Bdd.dand (get_enum_careset_for_var env cond v t)
    | _ -> fun acc -> acc)
    env.Bdd.Env.vartyp
    (Cudd.Bdd.dtrue env.Bdd.Env.cudd)

(******************************************************************************)
(** removes meaningless values of enumerated types from boolexpr *)
let normalize_benumneg env cond boolexpr =
  Bddapron.Expr0.Bool.tdrestrict boolexpr (full_enum_careset env cond)

(******************************************************************************)
(** [boolexpr_foldcut_numconvex env cond fnum inum f init b] traverses all pair
    (numerical constraint conjunction, boolean bdd) contained in the given BDD
    [b], and:

    - builds an object [num] by incrementally applying [fnum accnum c] to each
    numerical condition [c] of the numerical constraint conjunction, always
    restarting with [accnum = inum], and until [fnum] returns [None];

    - and then successively calling [f acc bool num] to each pair for which
    [num] was successfully constructed; initially, [acc = init]. *)
let boolexpr_foldcut_numconvex env cond fnum inum f acc boolexpr =
  let cuddman = env.Bdd.Env.cudd in
  let rec descend b accnum supp acc =
    if Cudd.Bdd.is_false b then acc                               (* shortcut *)
    else if Cudd.Bdd.is_cst supp then f acc b accnum
    else begin
      let topvar = Cudd.Bdd.topvar supp in
      let topvarbdd = Cudd.Bdd.ithvar cuddman topvar in
      let newsupp = Cudd.Bdd.exist topvarbdd supp in
      if not (PMappe.mem topvar env.Bdd.Env.id2var) then
        let topvarnbdd = Cudd.Bdd.dnot topvarbdd in
        let bbdd = Cudd.Bdd.tdrestrict b topvarbdd in
        let bnbdd = Cudd.Bdd.tdrestrict b topvarnbdd in
        if Cudd.Bdd.is_equal bbdd bnbdd then          (* irrelevant condition *)
          descend bbdd accnum newsupp acc
        else
          let c = PDMappe.x_of_y (topvar, true) cond.Bdd.Cond.condidb
          and nc = PDMappe.x_of_y (topvar, false) cond.Bdd.Cond.condidb in
          let acc =
            fnum accnum topvarbdd c |>
                List.fold_left (fun acc n -> descend bbdd n newsupp acc) acc in
          let acc =
            fnum accnum topvarnbdd nc |>
                List.fold_left (fun acc n -> descend bnbdd n newsupp acc) acc in
          acc
      else
        descend b accnum newsupp acc
    end
  in
  descend boolexpr inum (Cudd.Bdd.support boolexpr) acc

(******************************************************************************)
(** [boolexpr_foldcut_numconvex env cond fnum inum f init b] traverses all pair
    (numerical constraint conjunction, boolean bdd) contained in the given BDD
    [b], and:

    - builds an object [num] by incrementally applying [fnum accnum c] to each
    numerical condition [c] of the numerical constraint conjunction, always
    restarting with [accnum = inum], and until [fnum] returns [None];

    - and then successively calling [f acc bool num] to each pair for which
    [num] was successfully constructed; initially, [acc = init]. *)
let vdd_foldcut_numconvex env cond fnum inum f acc expr =
  let cuddman = env.Bdd.Env.cudd in
  let rec descend e accnum supp acc =
    if Cudd.Bdd.is_cst supp then f acc e accnum
    else begin
      let topvar = Cudd.Bdd.topvar supp in
      let topvarbdd = Cudd.Bdd.ithvar cuddman topvar in
      let newsupp = Cudd.Bdd.exist topvarbdd supp in
      if not (PMappe.mem topvar env.Bdd.Env.id2var) then
        let topvarnbdd = Cudd.Bdd.dnot topvarbdd in
        let ebdd = Cudd.Vdd.tdrestrict e topvarbdd in
        let enbdd = Cudd.Vdd.tdrestrict e topvarnbdd in
        if Cudd.Vdd.is_equal ebdd enbdd then          (* irrelevant condition *)
          descend ebdd accnum newsupp acc
        else
          let c = PDMappe.x_of_y (topvar, true) cond.Bdd.Cond.condidb
          and nc = PDMappe.x_of_y (topvar, false) cond.Bdd.Cond.condidb in
          let acc =
            fnum accnum topvarbdd c |>
                List.fold_left (fun acc n -> descend ebdd n newsupp acc) acc in
          let acc =
            fnum accnum topvarnbdd nc |>
                List.fold_left (fun acc n -> descend enbdd n newsupp acc) acc in
          acc
      else
        descend e accnum newsupp acc
    end
  in
  descend expr inum (Cudd.Vdd.support expr) acc

(******************************************************************************)
(** splits a boolexpr in a list of
     (numerical constraint conjunction /\ boolean bdd *)
let boolexpr_to_numconvex_list env cond boolexpr =
 let cuddman = env.Bdd.Env.cudd in
 let rec descend b n supp =
    if Cudd.Bdd.is_cst supp then
      if (Cudd.Bdd.is_false b) || (Cudd.Bdd.is_false n) then []
      else [(Cudd.Bdd.dand b n)]
    else
    begin
      let topvar = Cudd.Bdd.topvar supp in
      let topvarbdd = Cudd.Bdd.ithvar cuddman topvar in
      let newsupp = Cudd.Bdd.exist topvarbdd supp in
      if not (PMappe.mem topvar env.Bdd.Env.id2var) then
      begin
        let topvarnbdd = Cudd.Bdd.dnot topvarbdd in
        let nbdd = (Cudd.Bdd.dand n topvarbdd) in
        let nnbdd = (Cudd.Bdd.dand n topvarnbdd) in
        let bbdd = (Cudd.Bdd.tdrestrict b topvarbdd) in
        let bnbdd = (Cudd.Bdd.tdrestrict b topvarnbdd) in
        List.append
          (descend bbdd nbdd newsupp)
          (descend bnbdd nnbdd newsupp)
      end
      else
        (descend b n newsupp)
   end
  in
  descend boolexpr (Cudd.Bdd.dtrue cuddman) (Cudd.Bdd.support boolexpr)

(******************************************************************************)
(** returns the list of conjunctions (paths) of the given bdd *)
let boolexpr_to_dnf env boolexpr  =
  let cubelist = ref [] in
  let add_to_list cubarr =
    cubelist := Cudd.Bdd.cube_of_minterm env.Bdd.Env.cudd cubarr :: !cubelist
  in
  Cudd.Bdd.iter_cube add_to_list boolexpr;
  !cubelist

(******************************************************************************)
(** traverses the list of conjunctions (paths) of the given bdd *)
let boolexpr_fold_paths { Bdd.Env.cudd = cudd } f b i =
  let acc = ref i in
  Cudd.Bdd.iter_cube (fun c -> acc := f (Cudd.Bdd.cube_of_minterm cudd c) !acc) b;
  !acc

(******************************************************************************)
(** removes the expressions in boolexprlist from boolexpr
   by tdrestrict *)
let boolexpr_remove_exprs env cond boolexpr boolexprlist =
  List.fold_right
    (fun e ee ->
      let ee2 = Cudd.Bdd.tdrestrict ee e in
      if not (Bddapron.Expr0.Bool.is_false env cond ee2) then
        ee2
      else ee)
    boolexprlist boolexpr

(******************************************************************************)
(** removes the variables in the given support from the boolean expression *)
let boolexpr_forget_supp supp boolexpr =
  Cudd.Bdd.exist supp boolexpr

(******************************************************************************)
(** computes the number of boolean states represented by boolexpr
    (boolexpr is supposed to contain only boolean state variables) *)
let bool_space_size env cond boolstatevars boolexpr =
  Log.debug_o logger (print_boolexpr env cond) "bexpr=" boolexpr;
  let n_bvars = float_of_int
    (List.fold_left
      (fun n v ->
        let tid = PMappe.find v env.Bdd.Env.vartid in
        n + (Array.length tid))
      0 boolstatevars) in
  Log.debug3_o logger (Format.pp_print_float) "n_bvars=" n_bvars;
  let cnt = ref(0.0) in
  Cudd.Bdd.iter_cube
    (fun cube ->
       Log.debug3_o logger (Util.list_print
           (fun fmt a -> Format.pp_print_string fmt (match a with
             |Cudd.Man.False -> "f" |Cudd.Man.True -> "t" |_ -> "0")))
         "arr=" (Array.to_list cube);
      let n_cvars = float_of_int (Array.fold_left
        (fun n a -> match a with |Cudd.Man.False |Cudd.Man.True ->  n+1 |_ -> n)
        0 cube) in
      cnt := !cnt +. (2.0 ** (n_bvars -. n_cvars));
      Log.debug3_o logger (Format.pp_print_float) "n_bvars=" n_bvars;
      Log.debug3_o logger (Format.pp_print_float) "n_cvars=" n_cvars;
      Log.debug3_o logger (Format.pp_print_float) "(b-c) ="
        (n_bvars -. n_cvars);
      Log.debug3_o logger (Format.pp_print_float) "2^(b-c) ="
        (2.0 ** (n_bvars -. n_cvars));
      Log.debug3_o logger (Format.pp_print_float) "cnt=" !cnt;
    )
    boolexpr;
  (int_of_float !cnt)

(******************************************************************************)
(* operations on expressions and equations *)
(******************************************************************************)
(** simplifies an equation by the given expression (and the careset) *)
let simplify_equ env cond ?enums phi =
  let phi2 = Bddapron.Expr0.Bool.dand env cond phi cond.Bdd.Cond.careset in
  let phi2 = match enums with
    | None -> phi2
    | Some e -> Bddapron.Expr0.Bool.dand env cond phi2 e
  in
  fun (v, expr) ->
    if not (Bddapron.Expr0.Bool.is_cst env cond phi2)
    then (v, Bddapron.Expr0.tdrestrict expr phi2)
    else (v, expr)

(******************************************************************************)
(** simplifies an equation system by phi *)
let simplify_equs env cond ?enums phi equs =
  List.rev_map (simplify_equ env cond ?enums phi) equs |> List.rev

(******************************************************************************)
(** returns the set of guards in the given equations *)
let get_guards_equs env equs =
  List.fold_right
    (fun (_,e) set ->
      match e with
      |`Bool(e) -> PSette.add (Cudd.Bdd.dnot e) (PSette.add e set)
      |`Apron(e) ->
        let guardleaves = Cudd.Mtbdd.guardleafs e in
        Array.fold_right (fun (g,_) set -> PSette.add g set)
          guardleaves set
      |`Benum(e) ->
        let guardleaves = Bdd.Enum.guardlabels env e in
        List.fold_right (fun (g,_) set -> PSette.add g set)
          guardleaves set
      |`Bint(e) ->
        let guardleaves = Bdd.Int.guardints env.Bdd.Env.cudd e in
        List.fold_right (fun (g,_) set -> PSette.add g set)
          guardleaves set)
    equs (PSette.empty (compare))

(******************************************************************************)
(** returns the identity transition function for a set of variables *)
let get_id_equs_for env cond vars =
  List.map (fun v -> (v,Bddapron.Expr0.var env cond v)) vars

(******************************************************************************)
(** returns the =cst 0 transition function for a set of numerical variables *)
let get_zero_equs_for env cond vars =
  List.map (fun v ->
    (v,`Apron (Bddapron.Expr0.Apron.cst env cond
                   (Apron.Coeff.s_of_int 0))))
    vars

(******************************************************************************)
(** computes the boolean expression x'=f(x) *)
let get_fexpr get_primed_var env cond equs =
  List.fold_left
    (fun faccu (v,expr) ->
      let vexpr = Bddapron.Expr0.var env cond (get_primed_var v) in
      Bddapron.Expr0.Bool.dand env cond faccu
        (Bddapron.Expr0.eq env cond vexpr expr))
    (Bddapron.Expr0.Bool.dtrue env cond)
    equs

(*****************************************************************************)
(** computes the list of boolean expressions x'=f(x) *)
let get_fexprlist get_primed_var env cond equs =
  List.map
    (fun (v,expr) ->
      (* Log.d3 logger "equ: %a" (print_equation env cond) (v, expr); *)
      let vexpr = Bddapron.Expr0.var env cond (get_primed_var v) in
      let fe = (Bddapron.Expr0.eq env cond vexpr expr) in
      (* Log.d3 logger "fe: %a" (print_boolexpr env cond) fe; *)
      fe)
    equs

(******************************************************************************)
(** computes two linear conditions whose conjunction expresses the disequality
    with the given linear expression *)
let diseq_linexpr env expr =
  let symbol = env.Bdd.Env.symbol and apron = Bddapron.Env.apron env in
  let nexpr = Bddapron.Apronexpr.negate expr in
  let e = Bddapron.Apronexpr.to_linexpr0 symbol apron expr in
  let ne = Bddapron.Apronexpr.to_linexpr0 symbol apron nexpr in
  let inf = Apron.Lincons0.make ne Apron.Lincons0.SUP
  and sup = Apron.Lincons0.make e Apron.Lincons0.SUP in
  (ApronUtil.lincons1_of_lincons0 apron inf,
   ApronUtil.lincons1_of_lincons0 apron sup)

(******************************************************************************)
(* operations on abstract values *)
(******************************************************************************)
(** convexifies the given BddApron value *)
let abstract_convexify env cond doman s =
  let apronman = Bddapron.Domain0.man_get_apron doman in
  let l = Bddapron.Domain0.to_bddapron doman s in
  let (b,n) = List.fold_left
    (fun (bacc,nacc) (be,ne) ->
      (Bddapron.Expr0.Bool.dor env cond bacc be,
       Apron.Abstract0.join apronman nacc ne))
    (Bddapron.Expr0.Bool.dfalse env cond,
     ApronUtil.bottom0 apronman (Bddapron.Env.apron env))
    l
  in
  Bddapron.Domain0.meet_condition doman env cond
    (Bddapron.Domain0.of_apron doman env n) b

(******************************************************************************)
(** changes the domain of the given abstract value *)
let change_domain env old_doman s new_doman =
  let old_bddapron = Bddapron.Domain0.to_bddapron old_doman s in
  let new_apronman = Bddapron.Domain0.man_get_apron new_doman  in
  let apronenv = Bddapron.Env.apron env in
  let new_bddapron = List.map
    (fun (b,n) ->
      (b,
       Apron.Abstract1.abstract0
         (ApronUtil.change_domain (ApronUtil.abstract1_of_abstract0 apronenv n)
           new_apronman)))
    old_bddapron
  in
  Bddapron.Domain0.of_bddapron new_doman env new_bddapron

(******************************************************************************)
(* checking equations and expressions *)
(******************************************************************************)
(** checks whether a non-numerical equation is either identity or constant *)
let is_id_or_const_equ env cond (v,expr) =
  (Bddapron.Expr0.Bool.is_true env cond
     (Bddapron.Expr0.eq env cond expr (Bddapron.Expr0.var env cond v))) ||
    (let is_cst =
       match expr with
         |`Bool(e) -> Bddapron.Expr0.Bool.is_cst env cond e
         |`Benum(e) -> Bdd.Enum.is_cst e
         |`Bint(e) -> Bdd.Int.is_cst e
         |_ -> assert(false)
     in
     if is_cst then true
     else
       begin
         Log.debug3_o logger (print_expr env cond)
           "is_id_or_const: false; failed for " expr;
         false
       end)

(******************************************************************************)
(** checks whether all non-numerical equations
    are either identity or constant *)
let is_id_or_const_equs env cond equs =
  let rec checkequ equs =
    match equs with
      |[] -> true
      |equ::tl -> if is_id_or_const_equ env cond equ then checkequ tl else false
  in checkequ equs

(******************************************************************************)
(** checks whether all equations are identity *)
let is_id_equs env cond equs =
  let rec checkequ equs =
    match equs with
      |[] -> true
      |(v,expr)::tl ->
         if Bddapron.Expr0.Bool.is_true env cond
          (Bddapron.Expr0.eq env cond expr (Bddapron.Expr0.var env cond v)) then
           checkequ tl
         else
         begin
           Log.debug3_o logger (print_equation env cond)
             "is_id: false; failed for " (v,expr);
           false
         end
  in checkequ equs

(******************************************************************************)
(** returns true if the BDD contains only numerical constraints *)
let is_num_boolexpr cond boolexpr =
  let supp = Cudd.Bdd.support boolexpr in
  let condsupp = cond.Bdd.Cond.condsupp in
  Cudd.Bdd.is_true (Cudd.Bdd.support_diff supp condsupp)

(******************************************************************************)
(** checks whether the numerical equation is x'<>0 *)
let is_nonzero_equ env (_,expr) =
  match expr with
    |`Apron(e) ->
       let guardleaves = Cudd.Mtbdd.guardleafs e in
       if (Array.length guardleaves)<>1 then true
       else
         let (_,l) = guardleaves.(0) in
         not (Bddapron.ApronexprDD.is_zero env l)
    |_ -> assert(false)

(******************************************************************************)
(** checks whether the variable is numeric *)
let is_num_var env v = match Bddapron.Env.typ_of_var env v with
  | `Int | `Real -> true
  | _ -> false

(******************************************************************************)
(** checks whether the equation is numeric *)
let is_num_equ env (v, _) = is_num_var env v

(******************************************************************************)
(** checks whether expr depends on the variable v *)
let depends_on_var_expr env cond expr v =
  PSette.mem v (Bddapron.Expr0.support env cond expr)

(******************************************************************************)
(** checks whether expr depends on at least one variable in vars *)
let depends_on_some_var_expr env cond expr vars =
  let expr_supp = Bddapron.Expr0.support env cond expr in
  List.exists (fun v -> PSette.mem v expr_supp) vars

(******************************************************************************)
(** checks whether the equations depend on at least one variable in vars *)
let depends_on_some_var_equs env cond equs vars =
  let dvars = Util.list2psette (compare) vars in
  Log.debug3_o logger
    (PSette.print (env.Bdd.Env.symbol.Bdd.Env.print)) "vars=" dvars;
  if PSette.is_empty dvars then false                                  (* XXX *)
  else
  begin
    let rec check flist =
    match flist with
      |[] -> false
      |(_,expr)::tl ->
      begin
        let suppvars = Bddapron.Expr0.support env cond expr in
        Log.debug3_o logger (PSette.print (env.Bdd.Env.symbol.Bdd.Env.print))
          "suppvars=" suppvars;
        let empty = PSette.is_empty (PSette.inter dvars suppvars) in
        if empty then check tl
        else true
      end
    in
    check equs
  end



(******************************************************************************)
(* variables and support *)
(******************************************************************************)
(** returns the boolean (and enumerated) variables *)
let boolvars_of_env env =
  List.filter
    (fun v -> match (Bddapron.Env.typ_of_var env v) with
      |`Bool -> true
      |`Benum(_) -> true
      |`Bint(_,_) -> true
      |_ -> false
    )
    (Util.psette2list (Bddapron.Env.vars env))

(******************************************************************************)
(** returns the support of the given variables *)
let supp_of_vars env vars =
  Bdd.Expr0.O.bddsupport env vars

(******************************************************************************)
(** removes the numerical constraints from a boolean expression *)
let boolexpr_remove_cond _env cond b =
  Cudd.Bdd.exist cond.Bdd.Cond.condsupp b

(******************************************************************************)
(** computes the support set of the leaves of an Bddapron.Apron expression *)
let get_numleaf_suppset env expr =
  match expr with
    |`Apron(e) ->
       let guardleaves = Cudd.Mtbdd.guardleafs e in
       Array.fold_right (fun (_,l) supp ->
         PSette.union supp (Bddapron.Apronexpr.support env.Bdd.Env.symbol l))
         guardleaves (PSette.empty (compare))
    |_ -> assert(false)  (* numerical expression expected *)

(******************************************************************************)
(** primes all variables in the expression *)
let get_primed_expr ?(ignorevars=(PSette.empty (compare)))
    get_primed_var env cond expr =
  let substvars = List.map (fun v -> (v,get_primed_var v))
    (PSette.elements (PSette.diff (Bddapron.Expr0.support env cond expr)
                                  ignorevars)) in
  Bddapron.Expr0.substitute_by_var env cond expr substvars

(** primes all variables in the boolean expression *)
let get_primed_boolexpr ?(ignorevars=(PSette.empty (compare)))
    get_primed_var env cond boolexpr =
  Bddapron.Expr0.Bool.of_expr
    (get_primed_expr ~ignorevars get_primed_var env cond (Bddapron.Expr0.Bool.to_expr boolexpr))

(** un-primes all variables in the expression *)
let get_unprimed_expr get_unprimed_var env cond expr =
  let substvars = List.map (fun v -> (v, get_unprimed_var v))
    (PSette.elements (Bddapron.Expr0.support env cond expr)) in
  Bddapron.Expr0.substitute_by_var env cond expr substvars

(** un-primes all variables in the boolean expression *)
let get_unprimed_boolexpr get_unprimed_var env cond boolexpr =
  Bddapron.Expr0.Bool.of_expr
    (get_unprimed_expr get_unprimed_var env cond (Bddapron.Expr0.Bool.to_expr boolexpr))

(******************************************************************************)
(* product of equations *)
(******************************************************************************)
(** builds the product factorizes a numerical transition function
    by common actions *)
let product_numequs env array_table numequs =
  match numequs with
    |(_,`Apron(e))::restequs -> List.fold_left
      (fun arr equ ->
        match equ with
          |(_,`Apron(e)) -> MtbddUtil.arraymtbdd_product array_table arr
            (MtbddUtil.basemtbdd_to_arraymtbdd array_table e)
          |_ -> failwith "no numerical equation"
       )
      (MtbddUtil.basemtbdd_to_arraymtbdd array_table e)
      restequs
    |_ -> failwith "no numerical equation"

(******************************************************************************)
(** builds the product factorizes a transition function by common actions *)
let product_equs env arrset_table equs =
  match equs with
    |(_,e)::restequs -> List.fold_left
      (fun set (_,e) ->
        MtbddUtil.arrsetmtbdd_product arrset_table set
          (MtbddUtil.bddapronexpr_to_arrsetmtbdd env arrset_table e))
      (MtbddUtil.bddapronexpr_to_arrsetmtbdd env arrset_table e)
      restequs
    |_ -> failwith "no equation"

(******************************************************************************)

let fintyp_size env = function
  | `Bool -> 1
  | `Benum td -> Bdd.Enum.size_of_typ env td
  | `Bint (_,s) -> s

let typ_finsize env = function
  | #Bdd.Typ.t as t -> fintyp_size env t
  | _ -> 0

let typs_finsize env =
  List.fold_left (fun size t -> size + typ_finsize env t) 0

let vars_finsize env =
  List.fold_left
    (fun size v -> size + typ_finsize env (Bddapron.Env.typ_of_var env v))
    0

(******************************************************************************)
(* conversions *)
(******************************************************************************)

let prime_suffix = ref "'"
let get_suffix =
  let suff = ref None in
  fun () -> match !suff with
    | Some s -> s
    | None -> suff := Some !prime_suffix; !prime_suffix

let get_primed_var' symbol v =
  symbol.Bdd.Env.unmarshal (symbol.Bdd.Env.marshal v ^ get_suffix ())

(** primes a variable *)
let get_primed_var env v = get_primed_var' env.Bdd.Env.symbol v

(******************************************************************************)
(** unprimes a variable *)
let get_unprimed_var env v =
  env.Bdd.Env.symbol.Bdd.Env.unmarshal
    (String.sub (env.Bdd.Env.symbol.Bdd.Env.marshal v)
       0 ((String.length (env.Bdd.Env.symbol.Bdd.Env.marshal v))-
             (String.length (get_suffix ()))))

(******************************************************************************)
let var_to_apronvar env v = Apron.Var.of_string
  (env.Bdd.Env.symbol.Bdd.Env.marshal v)
let vars_to_apronvars env vars =
  Array.of_list
    (List.fold_right
      (fun v l ->
         match Bddapron.Env.typ_of_var env v with
	   |`Int |`Real -> (var_to_apronvar env v)::l
           |_ -> l)
       vars [])

(******************************************************************************)
let apronvar_to_var env v =
  env.Bdd.Env.symbol.Bdd.Env.unmarshal (Apron.Var.to_string v)
let apronvars_to_vars env apronvars =
  Array.to_list (Array.map (apronvar_to_var env) apronvars)

(******************************************************************************)
(** converts a Bddapron numerical leaf expression into a
    Bddapron.Apron expression *)
let apronexpr_to_apronexprDD env apronexpr = Cudd.Mtbdd.cst_u env.Bdd.Env.cudd
  (Cudd.Mtbdd.unique env.Bdd.Env.ext.Bddapron.Env.table apronexpr)

(******************************************************************************)
(** extracts the Bddapron numerical leaf expression from
    a Bddapron.Apron expression (provided that there is a single leaf) *)
let apronexprDD_to_apronexpr env apronexprDD =
  let gl = Cudd.Mtbdd.guardleafs apronexprDD in
  assert((Array.length gl)=1);
  Stdlib.snd (gl.(0))

(******************************************************************************)
(** converts an APRON expression into a BddApron numerical expression *)
let linexpr_to_apronexprDD env linexpr1 =
  let symbol = env.Bdd.Env.symbol in
  apronexpr_to_apronexprDD env
    (Bddapron.Apronexpr.Lin
      (Bddapron.Apronexpr.Lin.of_linexpr1 symbol linexpr1))

(******************************************************************************)
(** converts an APRON constraint into a boolean expression *)
let lincons_to_boolexpr env cond lincons1 =
  let linexpr = linexpr_to_apronexprDD env
    (Apron.Lincons1.get_linexpr1 lincons1) in
  match Apron.Lincons1.get_typ lincons1 with
    |Apron.Lincons0.EQ ->
          Bddapron.Expr0.Apron.eq env cond linexpr
    |Apron.Lincons0.SUPEQ ->
          Bddapron.Expr0.Apron.supeq env cond linexpr
    |Apron.Lincons0.SUP ->
          Bddapron.Expr0.Apron.sup env cond linexpr
    |Apron.Lincons0.DISEQ ->
          Bddapron.Expr0.Bool.dnot env cond
            (Bddapron.Expr0.Apron.eq env cond linexpr)
    |_ -> raise (NotSupported "congruences")

(******************************************************************************)
(** converts a conjunction of APRON constraints into a
   BddApron boolean expression *)
let linconss_to_boolexpr env cond linconss =
  let apronenv = Bddapron.Env.apron env in
  Array.fold_left
    (fun b lincons0 ->
      let c = ApronUtil.lincons1_of_lincons0 apronenv lincons0 in
      let bexpr = lincons_to_boolexpr env cond c in
      Bddapron.Expr0.Bool.dand env cond b bexpr)
    (Bddapron.Expr0.Bool.dtrue env cond)
    (linconss.Apron.Lincons1.lincons0_array)

(******************************************************************************)
(** converts an APRON abstract value into a
   BddApron boolean expression *)
let apron_to_boolexpr env cond apronman n =
  linconss_to_boolexpr env cond (Apron.Abstract1.to_lincons_array apronman n)

(******************************************************************************)
(** converts a conjunction of APRON constraints into a
   BddApron boolean expression *)
let linconss_to_boolexpr' env cond linconss =
  let apronenv = Bddapron.Env.apron env in
  Array.fold_left
    (fun b lincons0 ->
      let c = ApronUtil.lincons1_of_lincons0 apronenv lincons0 in
      let bexpr = lincons_to_boolexpr env cond c in
      Bddapron.Expr0.Bool.dor env cond b (Bddapron.Expr0.Bool.dnot env cond bexpr))
    (Bddapron.Expr0.Bool.dfalse env cond)
    (linconss.Apron.Lincons1.lincons0_array)

(******************************************************************************)
(** converts an APRON abstract value into a
   BddApron boolean expression *)
let apron_to_boolexpr' env cond apronman n =
  linconss_to_boolexpr' env cond (Apron.Abstract1.to_lincons_array apronman n)

(******************************************************************************)
(** builds a BddApron abstract value from a (purely) boolean expression and
    an APRON abstract value *)
let bddapron_to_abstract env doman boolexpr numabstract =
  Bddapron.Domain0.of_bddapron doman env
    [(boolexpr,numabstract.Apron.Abstract1.abstract0)]

(******************************************************************************)
(** converts a BddApron.Apronexpr.t into an Apron.Linexpr1.t *)
let apronaction_to_linexpr env cond action =
  let symbol = env.Bdd.Env.symbol in
  let apronenv = Bddapron.Env.apron env in
  match action with
    |Bddapron.Apronexpr.Lin(e) ->
	Bddapron.Apronexpr.Lin.to_linexpr1 symbol apronenv e
    |Bddapron.Apronexpr.Tree(e) ->
    begin
      try Bddapron.Apronexpr.Lin.to_linexpr1 symbol apronenv
        (Bddapron.Apronexpr.lin_of_tree symbol e)
      with Exit -> raise (NotLinearAction (Print.string_of_print
            (Bddapron.Apronexpr.print symbol) action))
    end
    |_ -> raise (NotLinearAction (Print.string_of_print
            (Bddapron.Apronexpr.print symbol) action))

(******************************************************************************)
(** extracts the numerical part of a boolean expression as a polyhedron;
    only exact for convex expressions *)
let boolexpr_to_linconss env cond doman boolvars expr =
  let apronman = Bddapron.Domain0.man_get_apron doman in
  let apronenv = Bddapron.Env.apron env in

  let bn = Bddapron.Domain0.to_bddapron doman
    (Bddapron.Domain0.meet_condition doman env cond
      (Bddapron.Domain0.top doman env)
      (Bddapron.Expr0.Bool.exist env cond boolvars expr)) in
  (* if expr is contradictory return bottom *)
  if Util.list_is_empty bn then
    Apron.Abstract1.to_lincons_array apronman
      (Apron.Abstract1.bottom apronman apronenv)
  else
    let (_,nn) = (List.hd bn) in
    Apron.Abstract1.to_lincons_array apronman
      (ApronUtil.abstract1_of_abstract0 apronenv nn)

(******************************************************************************)
(** converts a boolean expression into a BddApron abstract value *)
let boolexpr_to_abstract env cond doman boolexpr =
  Bddapron.Domain0.meet_condition doman env cond
    (Bddapron.Domain0.top doman env) boolexpr

(******************************************************************************)
(** converts a BddApron abstract value into a boolean expression *)
let abstract_to_boolexpr env cond doman abstract =
  let apronman = Bddapron.Domain0.man_get_apron doman in
  let apronenv = Bddapron.Env.apron env in
  let l = Bddapron.Domain0.to_bddapron doman abstract in
  List.fold_left
    (fun bacc (be,ne)->
       Bddapron.Expr0.Bool.dor env cond bacc
         (Bddapron.Expr0.Bool.dand env cond be
           (linconss_to_boolexpr env cond
             (Apron.Abstract1.to_lincons_array apronman
                (ApronUtil.abstract1_of_abstract0 apronenv ne)))))
    (Bddapron.Expr0.Bool.dfalse env cond)
    l

(******************************************************************************)
(** converts a BddApron.Expr0.Apron.t into an Apron.Linexpr1.t,
    provided that the guard equals true *)
let apronexpr_to_linexpr env cond expr =
  let guardleaves = Cudd.Mtbdd.guardleafs expr in
  assert((Array.length guardleaves)=1);
  let (g,a) = guardleaves.(0) in
  Log.debug3_o logger (Bddapron.Apronexpr.print env.Bdd.Env.symbol)
    "action: " a;
  assert(Bddapron.Expr0.Bool.is_true env cond g);
  apronaction_to_linexpr env cond a

(******************************************************************************)
(** extracts a list of equations with APRON linear expressions from
   BDD APRON equations *)
let numequs_of_equs env cond equs =
  let numequs = List.filter (is_num_equ env) equs in
  Array.of_list (List.map
    (fun (v,expr) ->
      Log.debug3_o logger (print_equation env cond) "equation: " (v,expr);
      match expr with
	|`Apron(e) ->
           (var_to_apronvar env v,
            apronexpr_to_linexpr env cond e)
        |_ -> assert(false))
    numequs)

(******************************************************************************)
(** extracts a list of equations and their guard conjunctions
     with APRON linear expressions from BDD APRON equations *)
let numequs_of_equs_with_guards env cond boolvars (equs:'a equs_t) =
  let numequs = List.filter (is_num_equ env) equs in
  let (vars,_) = List.split numequs in
  let nvars = Array.of_list vars in
  let bsupp = supp_of_vars env boolvars in
  let tabarr = MtbddUtil.make_table_action_array env in
  let numarr = product_numequs env tabarr numequs in
  let tabarrset = MtbddUtil.make_table_action_arrset env in
  let numarrset = MtbddUtil.arraymtbdd_to_arrsetmtbdd (compare) tabarrset
    numarr in
  let numarrset = MtbddUtil.arrsetmtbdd_exists tabarrset numarrset bsupp in
  let dnfgl = MtbddUtil.mtbdd_to_dnf env.Bdd.Env.cudd numarrset in
  List.fold_left
    (fun res (g,arrset) ->
      PSette.fold
        (fun arr res ->
          let eqs = Util.array_map2 (fun v a ->
             (var_to_apronvar env v, apronaction_to_linexpr env cond a))
            nvars arr in
          (g,eqs)::res)
        arrset res)
    [] dnfgl

(******************************************************************************)
(** computes the boolean expression with topologically closed constraints
    (the expression will be approximated by the conjunction of an
     expression over boolean variables and
     a conjunction of numerical constraints *)
(* g^bool /\ closure(/\i g^num_i) *)
(* TODO: better solution directly manipulating BDD???
   ite(c,f+,f-) ===> ite(c,ite(_c,f-,f+),f-) *)
let boolexpr_topoclose env cond doman boolexpr =
  let apronman = Bddapron.Domain0.man_get_apron doman in
  let apronenv = Bddapron.Env.apron env in
  let abstract = Bddapron.Domain0.meet_condition doman env cond
        (Bddapron.Domain0.top doman env) boolexpr in
  let l = Bddapron.Domain0.to_bddapron doman abstract in
  Log.debug3_o logger (print_abstract env doman) "abstract = " abstract;
  List.fold_right
    (fun (be,ne) res ->
      let closed_conss = Apron.Abstract1.to_lincons_array apronman
              (Apron.Abstract1.closure apronman
                (ApronUtil.abstract1_of_abstract0 apronenv ne)) in
       Bddapron.Expr0.Bool.dor env cond res
         (Bddapron.Expr0.Bool.dand env cond be
         (linconss_to_boolexpr env cond closed_conss)))
    l (Bddapron.Expr0.Bool.dfalse env cond)

(******************************************************************************)
(** splits a boolexpr into a list of boolexprs
   with convex numerical constraints *)
let boolexpr_to_numconvex_list2 ?(forget_vars=[]) env cond doman boolexpr =
  let apronman = Bddapron.Domain0.man_get_apron doman in
  let apronenv = Bddapron.Env.apron env in
  let l = Bddapron.Domain0.to_bddapron doman
    (Bddapron.Domain0.forget_list doman env
      (Bddapron.Domain0.meet_condition doman env cond
        (Bddapron.Domain0.top doman env) boolexpr)
      forget_vars)
  in
  List.map
    (fun (be,ne)-> Bddapron.Expr0.Bool.dand env cond be
         (linconss_to_boolexpr env cond
           (Apron.Abstract1.to_lincons_array apronman
             (ApronUtil.abstract1_of_abstract0 apronenv ne))))
    l

(******************************************************************************)
(** removes variables from a boolexpr,
    assuming that the numerical constraints are convex *)
let boolexpr_forget_vars env cond doman forget_vars boolexpr =
(*  Bddapron.Formula.Expr0.Bool.forget doman env cond boolexpr forget_vars *)
  let apronman = Bddapron.Domain0.man_get_apron doman in
  let apronenv = Bddapron.Env.apron env in
  let l = Bddapron.Domain0.to_bddapron doman
    (Bddapron.Domain0.forget_list doman env
      (Bddapron.Domain0.meet_condition doman env cond
        (Bddapron.Domain0.top doman env) boolexpr)
      forget_vars)
  in
  List.fold_right
    (fun (be,ne) res -> Bddapron.Expr0.Bool.dor env cond res
       (Bddapron.Expr0.Bool.dand env cond be
         (linconss_to_boolexpr env cond
           (Apron.Abstract1.to_lincons_array apronman
             (ApronUtil.abstract1_of_abstract0 apronenv ne)))))
    l ( Bddapron.Expr0.Bool.dfalse env cond)

(******************************************************************************)
(** removes the non-numerical inputs from the non-numerical expression *)
let expr_forget_supp supp expr =
  match expr with
    |`Bool(e) -> `Bool(Cudd.Bdd.exist supp e)
    |`Benum(e) ->
       `Benum({Bdd.Enum.typ=e.Bdd.Enum.typ;
               Bdd.Enum.reg=(Array.map
                (fun bdd -> Cudd.Bdd.exist supp bdd) e.Bdd.Enum.reg)})
    |`Bint(e) ->
       `Bint({Bdd.Int.signed=e.Bdd.Int.signed;
              Bdd.Int.reg=(Array.map
                (fun bdd -> Cudd.Bdd.exist supp bdd) e.Bdd.Int.reg)})
    |`Apron(e) -> assert(false)  (* boolean expression expected *)

(******************************************************************************)

module type BDDAPRON_TYPES = sig
  type var
  type typ = var Bddapron.Typ.t
  type vars = var vars_t
  type bexpr = var boolexpr_t
  type nexpr = var numexpr_t
  type expr = var expr_t
  type equ = var equ_t
  type equs = var equs_t
  type vset = var PSette.t
  type 'a vmap = (var, 'a) PMappe.t
  type equmap = expr vmap
end

let bddaprontypes_module (type v) =
  (module struct
    type var = v
    type typ = var Bddapron.Typ.t
    type vars = var vars_t
    type bexpr = var boolexpr_t
    type nexpr = var numexpr_t
    type expr = var expr_t
    type equ = var equ_t
    type equs = var equs_t
    type vset = var PSette.t
    type 'a vmap = (var, 'a) PMappe.t
    type equmap = expr vmap
  end : BDDAPRON_TYPES with type var = v)

module type BDDAPRON_ENV = sig
  include BDDAPRON_TYPES
  val env: var env_t
  val cond: var cond_t
end

let bddapronenv_module (type v) env cond =
  let module T = (val bddaprontypes_module: BDDAPRON_TYPES with type var = v) in
  (module struct
    include T
    let env = env
    let cond = cond
  end : BDDAPRON_ENV with type var = v)

(* --- *)

module EnvUtils = functor (E: BDDAPRON_ENV) -> struct
  open E
  let cuddman: Cudd.Man.vt = env.Bdd.Env.cudd
  let apronenv () = Bddapron.Env.apron env
  let symbol: var Bdd.Symb.t = env.Bdd.Env.symbol
  let memvar: var -> bool = Bddapron.Env.mem_var env
  let vartyp: var -> typ = Bddapron.Env.typ_of_var env
  let is_num v = match vartyp v with
    | `Int | `Real -> true
    | _ -> false
  let has_nums = List.exists is_num
  let empty_vset = PSette.empty symbol.Bdd.Env.compare
  let empty_vmap = PMappe.empty symbol.Bdd.Env.compare
  let varset = List.fold_left (fun m v -> PSette.add v m) empty_vset
end

(* --- *)

let setup_fast_substs env cond varmap ?(of_prime_first = true) to_prime =
  let permutation m = Bddapron.Expr0.O.permutation_of_rename env cond m in
  let of_prime = let v, v' = List.split to_prime in List.combine v' v in
  let of_prime_perm, to_prime_perm =
    if of_prime_first then
      let of_prime_perm, _ = permutation of_prime in
      let to_prime_perm, _ = permutation to_prime in
      of_prime_perm, to_prime_perm
    else
      let to_prime_perm, _ = permutation to_prime in
      let of_prime_perm, _ = permutation of_prime in
      of_prime_perm, to_prime_perm
  in
  (* Log.d logger "of_prime_perm: @[%a@]" Bdd.Idx.Map.print' of_prime_perm; *)
  (* Log.d logger "to_prime_perm: @[%a@]" Bdd.Idx.Map.print' to_prime_perm; *)
  match to_prime_perm, of_prime_perm with
    | Some to_prime_perm, Some of_prime_perm ->
        let module SwapState = struct type t = None | To | Of end in
        let current_setup = ref SwapState.None in
        let varmap setup =
          let set = Cudd.Man.set_varmap (Bdd.Env.cuddman env) in
          begin match setup with
            | s when s = !current_setup -> ()
            | SwapState.None -> ()
            | SwapState.To -> set to_prime_perm; current_setup := SwapState.To
            | SwapState.Of -> set of_prime_perm; current_setup := SwapState.Of
          end;
          varmap
        (* and nvarsmap vmap q = substitute_by_var env cond q vmap *)
        in
        (fun q -> varmap SwapState.To q (* |> nvarsmap to_prime_n *)),
        (fun q -> varmap SwapState.Of q (* |> nvarsmap of_prime_n *))
    | None, None -> (fun q -> q), (fun q -> q)
    | _ -> failwith "BddapronUtil.setup_fast_substs: internal error"

(* --- *)

module type FAST_SUBSTS = sig
  type var
  type expr
  val setup_fast_substs'
    : ?of_prime_first:bool
    -> (var * var) list
    -> (expr -> expr) * (expr -> expr)
  val setup_fast_substs
    : ?of_prime_first:bool
    -> (var, var) PMappe.t
    -> (expr -> expr) * (expr -> expr)
end

(* --- *)

(** Operations on Boolean expressions *)
module BOps = functor (E: BDDAPRON_ENV) -> struct
  open E
  open Bddapron.Expr0.Bool

  (** Miscellaneous infos about environment *)
  include EnvUtils (E)

  let var = var env cond

  (** Constants *)
  let tt = dtrue env cond
  let ff = dfalse env cond

  (** Standard operators *)
  let (!~) = dnot env cond
  let (&&~) = dand env cond
  let (||~) = dor  env cond
  let (=>~) = fun a b -> ite env cond a b tt
  let (==~) = eq env cond
  let ite = ite env cond

  (** Standard predicates *)
  let is_tt = is_true env cond
  let is_ff = is_false env cond
  let is_cst = is_cst env cond
  let is_eq = is_eq env cond
  let is_leq = is_leq env cond
  let (<=>) = is_eq

  (** Cofactors *)
  let (//~) = substitute env cond
  let cofactor = cofactor
  let (^^~) = restrict
  let (^~) = tdrestrict

  (** Simplification w.r.t current caresets for interpreted variable and
      enumerations *)
  let simplify ?(enums = tt) b = (b ^~ enums) ^~ cond.Bdd.Cond.careset
  let enums_careset () = full_enum_careset env cond

  let permute = permute_boolexpr

  type support = bexpr
  let support': bexpr -> support = Cudd.Bdd.support

  (** Eliminations *)
  let exist': support -> bexpr -> bexpr = Cudd.Bdd.exist
  let forall': support -> bexpr -> bexpr = Cudd.Bdd.forall
  let existand': support -> bexpr -> bexpr -> bexpr = Cudd.Bdd.existand
  let project' supp b = exist' (Cudd.Bdd.support_diff (support' b) supp) b

  let bsupp: vars -> support = supp_of_vars env
  let bsupp': vars -> support = fun vars ->
    let nvars, bvars = List.partition is_num vars in
    let nvars = varset nvars in
    let conds_supp = Bdd.Cond.fold_ddconds' cond begin fun _ n (`Apron c) acc ->
      let cvars = Bddapron.Apronexpr.Condition.support symbol c in
      if PSette.is_empty (PSette.inter nvars cvars) then acc else acc &&~ n
    end cond.Bdd.Cond.condsupp tt in
    bsupp bvars &&~ conds_supp
  let varsupp = bsupp                                                (* alias *)

  let support: bexpr -> vset = fun b -> Bddapron.Expr0.support env cond (to_expr b)
  let exist = exist env cond
  let forall = forall env cond
  let project vars = project' (bsupp vars)

  (* --- *)

  (** Variable substitution within predicates in case [e] does not involve [v];
      normally faster that [q //~ [v,e]]. *)
  let vsubst q v e =
    exist [v] (q &&~ (Bddapron.Expr0.eq env cond
                        (Bddapron.Expr0.var env cond v) e))

  (** [vsubsts q equs] performs parallel variable substitutions within the
      predicate [q] in case right-hand-side expressions in [equs] do not involve
      any variable from the left-hand-side of [equs]; normally faster that [q
      //~ equs]. *)
  let vsubsts q equs =
    let v_list, q = List.fold_left begin fun (v_list, q) (v, e) ->
      v :: v_list,
      q &&~ (Bddapron.Expr0.eq env cond (Bddapron.Expr0.var env cond v) e)
    end ([], q) equs in
    exist v_list q

  (** Variable substitutions within predicates in case [e] does not involve any
      variable in [vars]; normally faster that [q //~ e]. *)
  let vsubst_vars q vv'_list =
    let v_list, q = List.fold_left begin fun (v_list, q) (v, v') ->
      v :: v_list,
      q &&~ (Bddapron.Expr0.eq env cond
               (Bddapron.Expr0.var env cond v)
               (Bddapron.Expr0.var env cond v'))
    end ([], q) vv'_list in
    exist v_list q

  (* --- *)

  (** Pretty-printing *)
  let print = print env cond
  let print' fmt (b: bexpr) =
    Format.fprintf fmt "(%in,%gm)" (Cudd.Bdd.size b) (Cudd.Bdd.nbpaths b)

  (* --- *)

  let setup_fast_substs' ?of_prime_first x =
    setup_fast_substs env cond varmap ?of_prime_first x
  let setup_fast_substs ?of_prime_first vmap =
    setup_fast_substs' ?of_prime_first (PMappe.bindings vmap)

  (* --- *)

  module BddType = struct
    type t = bexpr
    let compare: t -> t -> int = fun a b -> Stdlib.compare a b
    let print: Format.formatter -> t -> unit = print
  end
end

(* --- *)

(** Operations on arithmetic expressions *)
module NOps = functor (E: BDDAPRON_ENV) -> struct
  open E
  open Apron
  open Bddapron.Expr0.Apron
  type 'a binop = nexpr -> nexpr -> 'a
  let var = var env cond
  let cst' l h = cst env cond (Coeff.Interval (Interval.of_scalar l h))
  let cst n = cst env cond (Coeff.Scalar n)
  let csti x = cst (Scalar.of_int x)
  let cstf x = cst (Scalar.of_float x)
  let cstm x = cst (Scalar.of_mpqf x)
  let zero = csti 0
  let one = csti 1
  let add a b = add env cond a b
  let sub a b = sub env cond a b
  let mul a b = mul env cond a b
  let div a b = div env cond a b
  let ( +.. ) = add
  let ( -.. ) = sub
  let ( *..  ) = mul
  let ( /..  ) = div
  let negate = negate env cond
  let eqz = eq env cond
  let supz = sup env cond
  let supeqz = supeq env cond
  let ( ==.. ) a b = eqz (b -.. a)
  let ( <.. ) a b = supz (b -.. a)
  let ( >.. ) a b = supz (a -.. b)
  let ite = ite env cond
  let ( //.. ) = substitute env cond
  let cofactor: nexpr -> bexpr -> nexpr = cofactor
  let ( ^^.. ): nexpr -> bexpr -> nexpr = restrict
  let ( ^.. ): nexpr -> bexpr -> nexpr = tdrestrict

  (* --- *)

  let setup_fast_substs' ?of_prime_first x
      : (nexpr -> nexpr) * (nexpr -> nexpr) =
    setup_fast_substs env cond varmap ?of_prime_first x
  let setup_fast_substs ?of_prime_first vmap =
    setup_fast_substs' ?of_prime_first (PMappe.bindings vmap)
end

(* --- *)

(** Operations on extended arithmetic expressions *)
module XOps = functor (E: BDDAPRON_ENV) -> struct
  open E
  open Apron
  open Bddapron
  open AerexprDD
  type xexpr = var t
  type 'a binop = xexpr -> xexpr -> 'a
  let table = make_table env.Bdd.Env.symbol
  let print = print (Expr0.Bool.print env cond) env.Bdd.Env.symbol
  let of_arith x = of_arith table env (Aerexpr.normalize env.Bdd.Env.symbol x)
  let var = var table env
  let cst' = cst' table env
  let plus_inf = cst' AERDom.plus_inf
  let minus_inf = cst' AERDom.minus_inf
  let nan = cst' AERDom.nan
  let cst' l h = cst table env (Coeff.Interval (Interval.of_scalar l h))
  let cst n = cst table env (Coeff.Scalar n)
  let csti x = cst (Scalar.of_int x)
  let cstf x = cst (Scalar.of_float x)
  let cstm x = cst (Scalar.of_mpqf x)
  let zero = csti 0
  let one = csti 1
  let ( =.~ ): xexpr -> xexpr -> bool = fun a b -> Cudd.Mtbdd.is_equal a b
  let is_nan: xexpr -> bool = ( =.~ ) nan
  let is_limit: xexpr -> bool = fun x -> x =.~ plus_inf || x =.~ minus_inf
  let add a b = add table env a b
  let sub a b = sub table env a b
  let mul a b = mul table env a b
  let div a b = div table env a b
  let ( +.~ ) = add
  let ( -.~ ) = sub
  let ( *.~ ) = mul
  let ( /.~  ) = div
  let negate = negate table env
  let eqz = Condition.eq env cond
  let supz = Condition.sup env cond
  let supeqz = Condition.supeq env cond
  let ( ==.~ ) a b = eqz (a -.~ b)
  let ( <.~ ) a b = supz (b -.~ a)
  let ( >.~ ) a b = supz (a -.~ b)
  let ite: bexpr -> xexpr -> xexpr -> xexpr = Cudd.Mtbdd.ite
  let ( //.~ ): xexpr -> equs -> xexpr = fun e s ->
    let esupp = Expr0.O.Support (Cudd.Mtbdd.support e) in
    Expr0.O.compose_of_subst env cond esupp s |> parsubstdd' table env e
  let cofactor: xexpr -> bexpr -> xexpr = Cudd.Mtbdd.cofactor
  let ( ^^.~ ): xexpr -> bexpr -> xexpr = Cudd.Mtbdd.restrict
  let ( ^.~ ): xexpr -> bexpr -> xexpr = Cudd.Mtbdd.tdrestrict
  (** Simplification w.r.t current caresets for interpreted variable and
      enumerations *)
  let simplify e = (e ^^.~ full_enum_careset env cond) ^^.~ cond.Bdd.Cond.careset
  let permute p : xexpr -> xexpr = fun e -> Cudd.Mtbdd.permute e p
  let support: xexpr -> vset = fun e ->
    let fsupp = Cudd.Mtbdd.support e in
    PSette.union
      (Expr0.support env cond (Expr0.Bool.to_expr fsupp))
      (support_leaf env e)
  let nan_guard: xexpr -> bexpr =
    let nan_u = cst'u table env AERDom.nan in
    fun e -> Cudd.Mtbdd.guard_of_leaf_u e nan_u
  let ( !!.~ ) x = let ng = Cudd.Bdd.dnot (nan_guard x) in
                   if Cudd.Bdd.is_false ng then x else x ^^.~ ng

  let to_linexpr0: xexpr -> ApronUtil.linexpr0 = fun e ->
    let `Apron e' = to_expr' env e in
    Cudd.Mtbdd.dval e' |>
        Apronexpr.to_linexpr0 env.Bdd.Env.symbol (Env.apron env)

  let to_texpr0: xexpr -> Apron.Texpr0.t = fun e ->
    let `Apron e' = to_expr' env e in
    Cudd.Mtbdd.dval e' |>
        Apronexpr.to_texpr0 env.Bdd.Env.symbol (Env.apron env)

  (** Non-guarded arithmetic expressions *)
  module XArith = struct
    type t = var Aerexpr.t
    type coeff = Aerexpr.Coeff.t
    let compare: t -> t -> int = Aerexpr.compare env.Bdd.Env.symbol
    let print: Format.formatter -> t -> unit = Aerexpr.print env.Bdd.Env.symbol
    let equal: t -> t -> bool = Aerexpr.equal env.Bdd.Env.symbol
    let hash: t -> int = Aerexpr.hash env.Bdd.Env.symbol
    let of_xexpr: xexpr -> t = AerexprDD.to_arith
    let var: var -> t = Aerexpr.var env.Bdd.Env.symbol (Env.typ_of_var env)
    let cst: Apron.Coeff.t -> t = Aerexpr.cst
    let cst': Aerexpr.Coeff.t -> t = Aerexpr.cst'
    let zero: t = Aerexpr.zero
    let is_zero: t -> bool = Aerexpr.is_zero
    let is_limit: t -> bool = Aerexpr.is_limit
    let add: t -> t -> t = Aerexpr.add env.Bdd.Env.symbol
    let sub: t -> t -> t = Aerexpr.sub env.Bdd.Env.symbol
    let mul: t -> t -> t = Aerexpr.mul env.Bdd.Env.symbol
    let negate: t -> t = Aerexpr.negate
    let support: t -> vset = Aerexpr.support env.Bdd.Env.symbol
    let to_linexpr0 e = Aerexpr.to_linexpr0 env.Bdd.Env.symbol (Env.apron env) e
    module Lin = struct
      type t = var Aerexpr.Lin.t
      let compare: t -> t -> int = Aerexpr.Lin.compare env.Bdd.Env.symbol
      let print: Format.formatter -> t -> unit = Aerexpr.Lin.print env.Bdd.Env.symbol
      let equal: t -> t -> bool = fun a b -> compare a b = 0
      let hash: t -> int = Hashtbl.hash
      let var_coeff: t -> var -> coeff option = Aerexpr.Lin.var_coeff env.Bdd.Env.symbol
      let term: coeff -> var -> t = Aerexpr.Lin.term
      let negate: t -> t = Aerexpr.Lin.negate
      let add: t -> t -> t = Aerexpr.Lin.add env.Bdd.Env.symbol
      let sub: t -> t -> t = Aerexpr.Lin.sub env.Bdd.Env.symbol
      let scale: coeff -> t -> t = Aerexpr.Lin.scale
      let map_terms: (coeff * var -> coeff * var) -> t -> t = Aerexpr.Lin.map_terms
      let project_out': vset -> t -> t = Aerexpr.Lin.project_out'
    end
    let of_lin: Lin.t -> t = fun l -> Aerexpr.Lin l
    let to_lin: t -> Lin.t = Aerexpr.to_lin
    module Cond = struct
      type e = t
      type t = var Aerexpr.Condition.t
      type t' = [ `Cond of t | `Bool of bool ]
      let print = Aerexpr.Condition.print env.Bdd.Env.symbol
      let support: t -> vset = Aerexpr.Condition.support env.Bdd.Env.symbol
      let to_bexpr: t' -> bexpr = AerexprDD.Condition.of_condition env cond
      let of_lincons: ApronUtil.lincons0 -> t' = fun l ->
        Aerexpr.Condition.of_lincons0
          env.Bdd.Env.symbol (Env.typ_of_var env) (Env.apron env) l
    end
    let gt: t -> t -> Cond.t' = Aerexpr.gt env.Bdd.Env.symbol (Env.typ_of_var env)
    let ge: t -> t -> Cond.t' = Aerexpr.ge env.Bdd.Env.symbol (Env.typ_of_var env)
    let lt: t -> t -> Cond.t' = Aerexpr.lt env.Bdd.Env.symbol (Env.typ_of_var env)
    let le: t -> t -> Cond.t' = Aerexpr.le env.Bdd.Env.symbol (Env.typ_of_var env)
  end

  let fold: (bexpr -> XArith.t -> 'a -> 'a) -> xexpr -> 'a -> 'a =
    Cudd.Mtbdd.fold_guardleaves
  let fold_u: (bexpr -> XArith.t Cudd.Mtbdd.unique -> 'a -> 'a) -> xexpr -> 'a -> 'a =
    Cudd.Mtbdd.fold_guardleaves_u

  let of_nexpr: nexpr -> xexpr =
    let symbol = env.Bdd.Env.symbol and ae = Env.apron env in
    fun n -> ApronexprDD.fold_guardvalues begin fun c t ->
      let t' = match Apronexpr.to_apron0 symbol ae t with
        | `Lin t -> Aerexpr.of_linexpr0 symbol ae t
        | `Tree t -> Aerexpr.of_texpr0 symbol ae t
      in
      ite c (of_arith t')
    end n nan
end

(* --- *)

(** Operations on general expressions *)
module POps = functor (E: BDDAPRON_ENV) -> struct
  open Bddapron.Expr0
  open E

  (** Miscellaneous infos about environment *)
  include EnvUtils (E)

  let var = var env cond
  let ite = ite env cond
  let of_bexpr: bexpr -> expr = Bool.to_expr
  let to_bexpr: expr -> bexpr = Bool.of_expr
  let of_nexpr: nexpr -> expr = Apron.to_expr
  let to_nexpr: expr -> nexpr = Apron.of_expr

  let ( ==~~ ): expr -> expr -> bexpr = eq env cond
  let support: expr -> vset = support env cond
  let support': expr -> bexpr = support_cond cuddman
  let cofactor: expr -> bexpr -> expr = cofactor
  let ( ^^~~ ): expr -> bexpr -> expr = restrict
  let ( ^~~ ): expr -> bexpr -> expr = tdrestrict
  let simplify_equ: ?enums:bexpr -> bexpr -> equ -> equ = simplify_equ env cond
  let simplify_equs: ?enums:bexpr -> bexpr -> equs -> equs = simplify_equs env cond
  let enums_careset () = full_enum_careset env cond

  let equmap: equs -> equmap =
    List.fold_left (fun m (v, e) -> PMappe.add v e m) empty_vmap

  let depends_on_var: expr -> var -> bool =
    depends_on_var_expr env cond

  let depends_on_any_var: expr -> vset -> bool = fun e vars ->
    PSette.exists (fun v -> PSette.mem v vars) (support e)

  let evols_supports ?(exclude = empty_vset) evols =
    let vmap = List.fold_left begin fun vmap (v, e) ->
      PMappe.add v (PSette.diff (support e) exclude) vmap
    end empty_vmap evols in
    vmap

  let subst_vars substs b = substitute_by_var env cond b substs

  (** [subst_vars' v v' b] substitutes variables in [v] by their counterpart of
      [v'] in [b]. *)
  let subst_vars' vars vars' = subst_vars (List.combine vars vars')

  let ( //~ ) = substitute env cond
  let substmap equmap = let bdgs = PMappe.bindings equmap in fun e -> e //~ bdgs

  let ( ///~ ) = substitute_list env cond
  let substmap_list em = let bdgs = PMappe.bindings em in fun e -> e ///~ bdgs

  (* --- *)

  let gencof_elim: bexpr -> vars -> expr -> expr = fun q v_list ->
    let module BOps = BOps (E) in
    let assign bdd = BOps.exist v_list (BOps.(&&~) bdd q) in
    function
      | `Bool b
        -> `Bool (assign b)
      | `Bint (Bdd.Repr.Int.{ reg } as e)
        -> `Bint Bdd.Repr.Int.{ e with reg = Array.map assign reg }
      | `Benum (Bdd.Repr.Enum.{ reg } as e)
        -> `Benum Bdd.Repr.Enum.{ e with reg = Array.map assign reg }
      | `Apron _
        -> failwith "Finite expression expected"

  (** [vsubsts equs e] performs parallel variable substitutions within the {b
      finite} expression [e] in case right-hand-side expressions in [equs] do not
      involve any variable from the left-hand-side of [equs]; normally faster
      that [e //~~ equs]. *)
  let vsubsts: equs -> expr -> expr = fun equs ->
    let v_list, q = List.fold_left begin fun (v_list, q) (v, e) ->
      v :: v_list, Bool.dand env cond q (eq env cond (var v) e)
    end ([], Bool.dtrue env cond) equs in
    gencof_elim q v_list

  (* --- *)

  let setup_fast_substs'' varmap = setup_fast_substs env cond varmap
  let setup_fast_substs' ?of_prime_first x : (expr -> expr) * (expr -> expr) =
    setup_fast_substs'' varmap ?of_prime_first x
  let setup_fast_substs ?of_prime_first vmap =
    setup_fast_substs' ?of_prime_first (PMappe.bindings vmap)

  (* --- *)

  (** Non-guarded arithmetic expressions *)
  module Arith = struct
    type t = var Bddapron.Apronexpr.t
    let equal: t -> t -> bool = Bddapron.Apronexpr.equal symbol
    let hash: t -> int = Bddapron.Apronexpr.hash symbol
    let compare: t -> t -> int = Bddapron.Apronexpr.compare symbol
    let print: Format.formatter -> t -> unit = Bddapron.Apronexpr.print symbol
    let support: t -> vset = Bddapron.Apronexpr.support symbol
    let to_nexpr = Bddapron.ApronexprDD.of_apronexpr env
    let is_zero = Bddapron.ApronexprDD.is_zero env
    let add: t -> t -> t = Bddapron.Apronexpr.add symbol
    let sub: t -> t -> t = Bddapron.Apronexpr.sub symbol
    module Cond = struct
      type t = var Bddapron.Apronexpr.Condition.t
      let support: t -> vset = Bddapron.Apronexpr.Condition.support symbol
    end
  end

  (* --- *)
  let labels var p =
    let open Bdd.Normalform in
    let open Bdd.Expr0.O.Expr in
    match dnf_of_bdd env p with
      | Disjunction clbls -> List.fold_left (fun s -> function
          | Conjunction [Tatom (Tenum (_, ls))] -> List.rev_append ls s
          | Conjunction _ | Cfalse -> failwith "Malformed predicate") [] clbls
      | Dtrue -> match Bdd.Env.typ_of_var env var with
          | `Benum t -> (match Bdd.Env.typdef_of_typ env t with
              | `Benum la -> Bdd.Labels.as_list la)
          | _ -> failwith (Format.asprintf "%a is not an enumeration variable"
                            env.Bdd.Env.symbol.Bdd.Env.print var)
end

(* --- *)

(** Pretty-printing utilities *)
module PPrt = functor (E: BDDAPRON_ENV) -> (struct
  open E
  open Bddapron
  open Format

  type 'a pp = formatter -> 'a -> unit
  let pp_var = env.Bdd.Env.symbol.Bdd.Env.print
  let pp_bexpr = Expr0.Bool.print env cond
  let pp_nexpr = Expr0.Apron.print env cond
  let pp_expr = Expr0.print env cond
  let pp_equ fmt (v, e) = fprintf fmt "%a = @[%a@]" pp_var v pp_expr e
  let pp_evol fmt (v, e) = fprintf fmt "%a := @[%a@]" pp_var v pp_expr e
  let pp_equs pp_equ = Util.list_print'
    ~copen:(fun fmt -> ()) ~cclose:(fun fmt -> ())
    ~csep:(fun fmt -> pp_print_space fmt ())
    (fun fmt x -> pp_equ fmt x; pp_print_char fmt ';')

  let pp_evols = pp_equs pp_evol
  let pp_equs = pp_equs pp_equ

  let pp_lst (pp_x: 'a pp) = Util.list_print'
    ~copen:(fun fmt -> fprintf fmt "{@[<hov>")
    ~csep:(fun fmt -> fprintf fmt ",@;")
    ~cclose:(fun fmt -> fprintf fmt "@]}")
    pp_x

  let pp_arr (pp_x: 'a pp) fmt a = Util.list_print'
    ~copen:(fun fmt -> fprintf fmt "[@[<hov>")
    ~csep:(fun fmt -> fprintf fmt ",@;")
    ~cclose:(fun fmt -> fprintf fmt "@]]")
    pp_x fmt (Array.to_list a)

  let pp_vars = pp_lst pp_var
  let pp_varr = pp_arr pp_var

  type unitfmt = (unit, formatter, unit) format
  let pp_vset = PSette.print
    ~first:("{@[<hov>": unitfmt)
    ~sep:(",@;": unitfmt)
    ~last:("@]}": unitfmt)
    pp_var

  let pp_vmap pp_x = PMappe.print
    ~first:("{@[<hov>": unitfmt)
    ~sep:(",@;": unitfmt)
    ~last:("@]}": unitfmt)
    ~firstbind:("@[<hov>": unitfmt)
    ~sepbind:(" => ": unitfmt)
    ~lastbind:("@]": unitfmt)
    pp_var
    pp_x

  let pp_equmap = PMappe.print
    ~first:("@[": unitfmt)
    ~sep:("@;": unitfmt)
    ~last:("@]": unitfmt)
    ~firstbind:("@[<hov>": unitfmt)
    ~sepbind:(" = ": unitfmt)
    ~lastbind:(";@]": unitfmt)
    pp_var
    pp_expr

  let pp_env fmt = Env.print fmt env
  let pp_cond fmt = Cond.print env fmt cond
  let pp_cond_info fmt =
    let open Bdd.Cond in
    if Bdd.Idx.size cond.idx = 0 then fprintf fmt "none" else
      Bdd.Idx.iter cond.idx (fun i ->
        let c = PDMappe.x_of_y (i, true) cond.condidb in
        fprintf fmt "%a@ " (Cond.print_cond env) c);
    fprintf fmt "@\n(%d@ remainging@ slots)" (Bdd.Idx.rem_size cond.idx)
  let pp_careset fmt = pp_bexpr fmt cond.Bdd.Cond.careset
  let pp_apronenv fmt = Apron.Environment.print fmt (Env.apron env)

end : sig
  open E

  type 'a pp = Format.formatter -> 'a -> unit
  val pp_var: var pp
  val pp_vars: var list pp
  val pp_varr: var array pp
  val pp_vset: vset pp
  val pp_bexpr: bexpr pp
  val pp_nexpr: nexpr pp
  val pp_expr: expr pp
  val pp_equ: equ pp
  val pp_evol: equ pp
  val pp_equs: equs pp
  val pp_evols: equs pp
  val pp_equmap: expr vmap pp

  val pp_vmap: 'a pp -> 'a vmap pp
  val pp_lst: 'a pp -> 'a list pp

  val pp_env: Format.formatter -> unit
  val pp_cond: Format.formatter -> unit
  val pp_cond_info: Format.formatter -> unit
  val pp_careset: Format.formatter -> unit
  val pp_apronenv: Format.formatter -> unit
end)

(* --- *)

(** Temporary environment extensions *)
module BEnvExt = functor (E: BDDAPRON_ENV) -> (struct
  module POps = POps (E)
  module PP = PPrt (E)
  open POps
  open Bddapron.Expr0
  open E

  (* --- *)

  type upgrade = int array option

  let no_upgrade = None
  let upgrade_of_perms : int array option -> upgrade = fun eu -> eu
  let upgrade_to_perms : upgrade -> int array option = fun eu -> eu
  let upgrade u p x = match p with
    | None -> x
    | Some p -> u p x
  let upgrade' p u = function
    | Some e -> Some (upgrade p u e)
    | None -> None
  let upgrade_expr = upgrade permute_expr
  let upgrade_bexpr = upgrade permute_boolexpr
  let compose_upgrades = Bdd.Idx.Map.compose'

  (* --- *)

  type extension =
      {
        new_vars: vars;
      }

  (* --- *)

  let extend_environment ?groups_spec new_vars =
    let packing = match groups_spec with
      | None -> `None
      | Some g -> `Group g
    in
    let new_vars = List.filter (fun (v, _) -> not (memvar v)) new_vars in
    let perms = if new_vars = [] then None else
        (* TODO: encapsulate permutations in bddapron to avoid the above test *)
        Bddapron.Env.add_vars_with ~packing ~allow_bddenv_growth:true env
          new_vars
    in
    { new_vars = List.rev_map fst new_vars }, perms

  (* --- *)

  let prime v = get_primed_var env v, vartyp v
  let maybe_add_var acc (v, t) = if memvar v then acc else (v, t) :: acc

  (* --- *)

  let make_group_spec pmap kmap group_policy =
    let group_prime_n_by_type gtm v va gtl =
      let gt = List.rev_map (fun v -> try PMappe.find v gtm with
        | Not_found -> `Var v) (Array.to_list va) in
      (* (`Reord gt) :: gtl *)
      (* (`Fixed gt) :: gtl *)
      List.rev_append gt gtl
    and group_prime_n_by_type' v va gtl =
      let gt = List.rev_map (fun v -> try [v; PMappe.find v pmap] with
        | Not_found -> [v]) (Array.to_list va) in
      `InterleavedVars (List.flatten gt) :: gtl
    and group_by_type v va gtl =
      let gt = List.fold_left
        (fun gtl v -> `Var v :: try `Var (PMappe.find v pmap) :: gtl with
          | Not_found -> gtl) [] (Array.to_list va)
      in
      `Fixed (List.rev gt) :: gtl
    in
    let group = match group_policy with
      | `TypeNPrime ->
          PMappe.mapi (fun v v' -> `Fixed [`Var v; `Var v']) pmap |>
              group_prime_n_by_type
      | `Type -> group_by_type
      | `InterleavePrime -> group_prime_n_by_type'
    in
    PMappe.fold (fun v -> match vartyp v with
      | #Bdd.Typ.t -> group v
      | _ -> fun va gtl -> gtl) kmap []

  (* --- *)

  let grouping = Some `InterleavePrime

  let decl_vars vmap : var array vmap * var vmap * extension * upgrade =
    let mkvar = symbol.Bdd.Env.unmarshal in
    let vdecls = [] in
    let kmap, pmap, vdecls = PMappe.fold begin fun v (k, p) (kmap, pmap, vtyps) ->
      let t = vartyp v in
      let strep = symbol.Bdd.Env.marshal v in
      let mkrep = Printf.sprintf "%s$%d" in
      let nvars = Array.init (succ k) (function
        | 0 -> v
        | i -> mkrep strep (succ i) |> mkvar) in
      let vtyp = List.map (fun x -> x, t) (Array.to_list nvars |> List.tl) in
      let kmap = PMappe.add v nvars kmap in
      let pmap = if not p then pmap else              (* primed version req'd *)
          Array.fold_right (fun v -> PMappe.add v t) nvars pmap in
      (kmap, pmap, List.rev_append vtyp vtyps)
    end vmap (empty_vmap, empty_vmap, vdecls) in
    let pmap, vdecls = PMappe.fold begin fun v t (pmap, vtyps) ->
      let v' = get_primed_var env v in
      (PMappe.add v v' pmap, maybe_add_var vtyps (v', t))
    end pmap (empty_vmap, vdecls) in

    let groups_spec = match grouping with
      | None -> None
      | Some gp -> Some (make_group_spec pmap kmap gp)
    in
    let extension, upgrade = extend_environment ?groups_spec vdecls in
    kmap, pmap, extension, upgrade

  (* --- *)

  (** [make_primes vars] declares one primed version per variable in [vars] if
      it is not already declared.

      Returns an environment upgrade specification (of type [upgrade]) that
      should be applied to any alive expression involving variables declared in
      the environment given as argument to the functor. *)
  let make_primes vars : var vmap * extension * upgrade =
    PSette.fold (fun v -> PMappe.add v (0, true)) vars empty_vmap |> decl_vars |>
        (fun (_, vars, e, u) -> vars, e, u)

  (* --- *)

  let restore_conditions { new_vars } =
    let cudd = Bdd.Env.cuddman env in
    let to_rem = varset new_vars in
    let rsupp = PDMappe.fold (fun c (id, p) rsupp ->
      let csupp = cond.Bdd.Cond.support_cond env c in
      if PSette.is_empty (PSette.inter to_rem csupp)
      then Cudd.Bdd.dand (Cudd.Bdd.ithvar cudd id) rsupp
      else rsupp)
      cond.Bdd.Cond.condidb
      (Cudd.Bdd.dtrue cudd)
    in
    Bdd.Cond.reduce_with cond rsupp

  (** [restore_environment extension] removes all variables declared in the
      given environment extension.

      Returns an upgrade descriptor that should be applied to any alive
      expression involving variables declared in the environment given as
      argument to the functor. *)
  let restore_environment ({ new_vars } as ext) : upgrade =
    let perms = restore_conditions ext in
    match Bddapron.Env.remove_vars_with env ~packing:`None new_vars with
      | None -> Some perms
      | Some p -> Some (Bdd.Idx.Map.compose perms p)

end : sig
  open E
  type upgrade
  val no_upgrade: upgrade
  val upgrade_of_perms: int array option -> upgrade
  val upgrade_to_perms: upgrade -> int array option
  val upgrade_expr: upgrade -> expr -> expr
  val upgrade_bexpr: upgrade -> bexpr -> bexpr
  val upgrade: (int array -> 'a -> 'a) -> upgrade -> 'a -> 'a
  val upgrade': (int array -> 'a -> 'a) -> upgrade -> 'a option -> 'a option
  val compose_upgrades: upgrade -> upgrade -> upgrade
  type extension
  val restore_environment: extension -> upgrade
  val make_primes: vset -> var vmap * extension * upgrade
  val decl_vars: (int * bool) vmap
    -> var array vmap * var vmap * extension * upgrade
end)

(* --- *)

module type BDDAPRON_ENV_NS = sig
  module BEnv: BDDAPRON_ENV
  include BDDAPRON_TYPES with type var = BEnv.var
  module PPrt: module type of PPrt (BEnv)
  module BOps: module type of BOps (BEnv)
  module NOps: module type of NOps (BEnv)
  module POps: module type of POps (BEnv)
  module PAll: sig
    include BDDAPRON_TYPES with type var = var
    include module type of PPrt
    include module type of POps
  end
  module BAll: sig
    include module type of PAll
    include module type of BOps
  end
  module NAll: sig
    include module type of PAll
    include module type of NOps
  end
  module BEnvExt: module type of BEnvExt (BEnv)
  module BddSet: module type of Util.MakeSet (BOps.BddType)
  module BddMap: module type of Util.MakeMap (BOps.BddType)
end

(** Gathering above utilities in a single functor *)
module BEnvNS = functor (BEnv: BDDAPRON_ENV) -> (struct
  module BEnv = BEnv
  include BEnv
  module PPrt = PPrt (BEnv)
  module BOps = BOps (BEnv)
  module NOps = NOps (BEnv)
  module POps = POps (BEnv)
  module PAll = struct
    include BEnv
    include PPrt
    include POps
  end
  module BAll = struct
    include PAll
    include BOps
  end
  module NAll = struct
    include PAll
    include NOps
  end
  module BEnvExt = BEnvExt (BEnv)
  module BddSet = Util.MakeSet (BOps.BddType)
  module BddMap = Util.MakeMap (BOps.BddType)
end : BDDAPRON_ENV_NS with module BEnv = BEnv)

(* -------------------------------------------------------------------------- *)

(** Traversal of convex expressions/guards *)
module CTrv = functor (BEnvNS: BDDAPRON_ENV_NS) -> struct
  open Bddapron.Apronexpr.Condition
  open BEnvNS.BEnv
  open BEnvNS.NOps
  open BEnvNS.BAll

  (* --- *)

  module type CTRV = sig
    type t
    val foldcut_numconvex:
      convex:('b -> bexpr -> var Bddapron.Apronexpr.Condition.t -> 'b list) ->
      diseq:('b -> bexpr -> var Bddapron.Apronexpr.expr -> 'b list) ->
      'b -> ('a -> t -> 'b -> 'a) -> 'a -> t -> 'a
    val fold_numconvex: bexpr -> ('a -> t -> bexpr -> 'a) -> 'a -> t -> 'a
  end

  let split ~convex ~diseq acc numcond (`Apron (typ, expr)) =
    match typ with
      | SUPEQ | SUP | EQ -> convex acc numcond (typ, expr)
      | DISEQ -> diseq acc numcond expr
      | EQMOD _ -> failwith "Unsupported modulo comparison"

  module Bool = struct

    let foldcut_numconvex
        ~(convex: 'b -> bexpr -> var Bddapron.Apronexpr.Condition.t -> 'b list)
        ~(diseq: 'b -> bexpr -> var Bddapron.Apronexpr.expr -> 'b list) :
        'b -> ('a -> bexpr -> 'b -> 'a) -> 'a -> bexpr -> 'a
      = boolexpr_foldcut_numconvex env cond (split ~convex ~diseq)

    let fold_numconvex: bexpr -> ('a -> bexpr -> bexpr -> 'a) -> 'a -> bexpr -> 'a = fun i ->
      foldcut_numconvex
        ~convex:(fun acc c _ -> [ acc &&~ c ])
        ~diseq:(fun acc _ e -> let e = Bddapron.ApronexprDD.of_apronexpr env e in
                            [ acc &&~ supz (negate e); acc &&~ supz e ])
        i

  end

  module Num = struct

    let foldcut_numconvex
        ~(convex: 'b -> bexpr -> var Bddapron.Apronexpr.Condition.t -> 'b list)
        ~(diseq: 'b -> bexpr -> var Bddapron.Apronexpr.expr -> 'b list) :
        'b -> ('a -> nexpr -> 'b -> 'a) -> 'a -> nexpr -> 'a
      = vdd_foldcut_numconvex env cond (split ~convex ~diseq)

    let fold_numconvex: bexpr -> ('a -> nexpr -> bexpr -> 'a) -> 'a -> nexpr -> 'a = fun i ->
      foldcut_numconvex
        ~convex:(fun acc c _ -> [ acc &&~ c ])
        ~diseq:(fun acc _ e -> let e = Bddapron.ApronexprDD.of_apronexpr env e in
                            [ acc &&~ supz (negate e); acc &&~ supz e ])
        i

  end

  module XNum (XOps: sig type xexpr = var Bddapron.AerexprDD.t end) = struct
    type xexpr = XOps.xexpr

    let foldcut_numconvex
        ~(convex: 'b -> bexpr -> var Bddapron.Apronexpr.Condition.t -> 'b list)
        ~(diseq: 'b -> bexpr -> var Bddapron.Apronexpr.expr -> 'b list) :
        'b -> ('a -> xexpr -> 'b -> 'a) -> 'a -> xexpr -> 'a
      = fun i ->
        vdd_foldcut_numconvex env cond
          (fun acc numcond (`Apron (typ, expr)) -> match typ with
            | SUPEQ | SUP | EQ -> convex acc numcond (typ, expr)
            | DISEQ -> diseq acc numcond expr
            | EQMOD _ -> failwith "Unsupported modulo comparison")
          i

    let fold_numconvex: bexpr -> ('a -> xexpr -> bexpr -> 'a) -> 'a -> xexpr -> 'a = fun i ->
      foldcut_numconvex
        ~convex:(fun acc c _ -> [ acc &&~ c ])
        ~diseq:(fun acc _ e -> let e = Bddapron.ApronexprDD.of_apronexpr env e in
                            [ acc &&~ supz (negate e); acc &&~ supz e ])
        i

  end

end

(* -------------------------------------------------------------------------- *)
(* careset-related utils *)

(** Basic helper to periodically update the careset. *)
module CaresetMan = functor (E: BDDAPRON_ENV) -> struct
  module PPrt = PPrt (E)
  open E

  type t =
      {
        careset_update_period: int;
        mutable next_careset_update: int;
      }

  let make ?(update_period = 0) threshold =
    let careset_update_period =
      let s = Apron.Environment.size (Bddapron.Env.apron env) in
      if s >= threshold then begin
        Log.w logger "Disabling automatic careset update (num size = %d ≥ %d)"
          s threshold;
        0
      end else update_period
    in
    { careset_update_period; next_careset_update = careset_update_period; }

  let force_careset_update () =
    Log.i logger "Updating careset…";
    Bdd.Cond.update_careset cond;
    Log.d3 logger "conds = @[%t@]" PPrt.pp_cond_info

  let maybe_update_n_val ?(force = false) man =
    man.next_careset_update <- pred man.next_careset_update;
    if man.next_careset_update = 0 ||
      ((* be.careset_update_period <> 0 &&  *)force) &&
      not (Cudd.Bdd.is_true cond.Bdd.Cond.condsupp)
    then
      begin
        man.next_careset_update <- man.careset_update_period;
        force_careset_update ();
      end
    else if man.next_careset_update < man.careset_update_period - 10000 then
      begin
        (* reset when too low. *)
        man.next_careset_update <- man.careset_update_period
      end;
    cond.Bdd.Cond.careset

end

(* -------------------------------------------------------------------------- *)
