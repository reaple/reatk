(* -*- coding: utf-8 -*- *)
(******************************************************************************)
(* BddapronSynthesis *)
(* utilities for synthesis *)
(* author: Nicolas Berthier *)
(* version: 0.9.0 *)
(* This file is part of ReaTK released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)
(******************************************************************************)

(** BddApron-based Boolean synthesis *)

(* XXX NB: I don't know how strings with utf8 characters interact with Format,
   other than by breaking some length computations, but for now that seems
   ok. *)

(**/**)

open BddapronUtil
open Bddapron.Expr0

(* -------------------------------------------------------------------------- *)

module Logger = struct
  let logger = Log.mk ~level:Log.Debug3 "sB"
  let log3 f = Log.cd3 logger f
  let l3_1 f x fmt = log3 f fmt x
  let l3_2 f x y fmt = log3 f fmt x y
end
open Logger

(**/**)

(* -------------------------------------------------------------------------- *)

(** {2 Utilities for Synthesis and Controller Triangulation} *)

(* -------------------------------------------------------------------------- *)

(** Decompositions to be used for controller construction *)
type 'x decomposition =
  [
  | `Auto of CuddUtil.Decomp.algorithm option * int
  | `Manual of 'x list
  ]

module Core = functor (BEnvNS: BDDAPRON_ENV_NS) -> struct
  module CM = CaresetMan (BEnvNS.BEnv)
  module RP = BddapronReorderPolicy.Make (Logger) (BEnvNS.BEnv)
  open BEnvNS.BAll
  open BEnvNS.BEnv

  (* --- *)

  let print' =
    if Log.check_level logger Log.Debug then print else print'

  (* --- *)

  (** Internal type representing U/C groups. *)
  type uc_spec =
      {
        u_vars: var list;
        c_vars: var list;
        c_supp: bexpr;
        u_supp: bexpr;
        exist_c: bexpr -> bexpr;                                      (* alias *)
        forall_c: bexpr -> bexpr;                                     (* alias *)
        exist_u: bexpr -> bexpr;                                      (* alias *)
        forall_u: bexpr -> bexpr;                                     (* alias *)
        existand_u: bexpr -> bexpr -> bexpr;                           (* alias *)
        exist_i: bexpr -> bexpr;                                      (* alias *)
      }

  (** Internal type encapsulating data for Boolean synthesis. Its main purpose
      is to help keeping signatures "simple", and easily defining operators. *)
  type boolenv =
      {
        uc_specs: uc_spec list;              (* reversed groups (from n to 1) *)
        all_u_supp: bexpr;
        all_c_supp: bexpr;
        all_i_supp: bexpr;
        cm: CM.t;
      }

  (* --- *)

  let make_boolenv ?(careset_update_period=0) uc_groups =

    let uc_specs, all_u_supp, all_c_supp = List.fold_left
      begin fun (uc_specs, all_u_supp, all_c_supp) (bu_vars, bc_vars) ->
        let u_vars = bu_vars and c_vars = bc_vars in

        Log.d3 logger "@[U = %a@]" pp_vars u_vars;
        Log.d3 logger "@[C = %a@]" pp_vars c_vars;

        let u_supp = supp_of_vars env u_vars in
        let c_supp = supp_of_vars env c_vars in
        let exist_u = exist u_vars and forall_u = forall u_vars in
        let exist_c = exist c_vars and forall_c = forall c_vars in
        let existand_u = existand' u_supp in
        let exist_i = exist' (Cudd.Bdd.support_union u_supp c_supp) in

        ({ u_vars; c_vars; u_supp; c_supp;
           exist_u; forall_u; exist_c; forall_c;
           existand_u; exist_i } :: uc_specs,
         u_supp &&~ all_u_supp,
         c_supp &&~ all_c_supp)
      end ([], tt, tt) uc_groups
    in

    let careset_update_threshold = 60 in               (* TODO: "optionalize" *)
    let cm = CM.make ~update_period:careset_update_period
      careset_update_threshold in

    { uc_specs;
      all_u_supp; all_c_supp;
      all_i_supp = all_u_supp &&~ all_c_supp; cm; }

  (** [make_boolenv uc_groups] builds an environment for Boolean synthesis and
      triangulation. *)
  let make_boolenv
      ?(disable_dynamic_variable_reordering = true)
      ?careset_update_period
      =
    (if disable_dynamic_variable_reordering then
        CuddUtil.dynamic_reordering_suspend1 env
          (make_boolenv ?careset_update_period)
     else (make_boolenv ?careset_update_period))

  (* --- *)

  (** Simplifies the given Boolean expression (possibly updating the
      careset). *)
  let simpl ?force_careset_update be b : bexpr =
    b ^~ CM.maybe_update_n_val ?force:force_careset_update be.cm

  (* --- *)

  (** Restricts the given Boolean expression according to legitimate enumaration
      variables. *)
  let norm_enums = normalize_benumneg env cond

  (* --- *)

  (** [pre_open be evolutions x] computes the "open" pre-image of the set of
      states [x] (as predicate) w.r.t the given evolutions; that is, the set of
      all potential states leading to a state in [x] by "executing" one set of
      equations in [evolutions] iff its associated guard is satisfied. *)
  let pre_open (evols: 'a equs_t) (x: bexpr) : bexpr =
    CuddUtil.dynamic_reordering_suspend1 env ((//~) x) evols

  (* -------------------------------------------------------------------------- *)

  (** {2 Boolean Synthesis} *)

  (** Type encapsulating data for Boolean synthesis. Its main purpose is to help
      keeping signatures "simple". *)
  type synth_data =
      {
        be: boolenv;
        evolutions: var equs_t;
        assertion: bexpr;
        oldassert: bool;
      }

  (* --- *)

  (** Basic `pre-image' operator. *)
  let pre ({ be = { all_i_supp }; assertion; evolutions }) lp
      (e: bexpr) : bexpr =

    Log.d3 logger "%t@<5>→pre(%a):" lp print e;

    let x = pre_open evolutions e in
    Log.d3 logger "%t  <$> = %a" lp print x;

    (* Select all previous states of X. *)
    if not (Log.check_level logger Log.Debug3)
    then Cudd.Bdd.existand all_i_supp assertion x (* shortcut for ∃i,X∩A *)
    else let xa = x &&~ assertion in              (* decompose for logging... *)
         Log.d3 logger "%t@<8>  ^∩A = %a" lp print xa;
         let xa = Cudd.Bdd.exist all_i_supp xa in
         Log.d3 logger "%t@<5>  ∃i^@[ = %a (= pre($))@]" lp print xa;
         xa

  (* --- *)

  let pre_u_oldassert ({ be = { uc_specs }; assertion; evolutions }) lp
      (e: bexpr) : bexpr =

    Log.d3 logger "%t@<7>→pre_u(%a):" lp print e;

    let x = pre_open evolutions e in
    Log.d3 logger "%t <$> = %a" lp print x;

    let xa = x &&~ assertion in
    Log.d3 logger "%t@<7> ^∩A = %a" lp print xa;

    List.fold_left begin fun e { forall_c; exist_u } ->

      (* Select all previous states that can lead to a state in X through a
         controllable transition. *)
      let e = forall_c e in
      Log.d3 logger "%t@<7> ∀c^ = %a" lp print e;
      let e = exist_u e in
      Log.d3 logger "%t@<7> ∃u^ = %a" lp print e;
      e

    end xa uc_specs

  (** [pre_u sd lp b] computes the states uncontrollably leading to states
      belonging to [b]. *)
  let pre_u ({ be = { uc_specs }; assertion; evolutions }) lp : bexpr -> bexpr =
    let al, _, _ = List.fold_left begin fun (acc, a, i) { forall_c; exist_i } ->
      let a' = forall_c a in
      Log.d3 logger "%t A%i = %a" lp i print a';
      a' :: acc, exist_i a, succ i
    end ([], assertion, 0) uc_specs in
    let al = List.rev al in
    fun x ->

      Log.d3 logger "%t@<7>→pre_u(%a):" lp print x;

      let x = pre_open evolutions x in
      Log.d3 logger "%t  <$> = %a" lp print x;

      List.fold_left2 begin fun (x, i) { forall_c; exist_c; existand_u } ai ->
        (* Select all previous states that can lead to a state in X through a
           controllable transition admissible w.r.t the assertion. *)
        let f = forall_c x in
        Log.d3 logger "%t@<8>  ∀c^ = %a" lp print f;
        let g = ai =>~ exist_c x in
        Log.d3 logger "%tA%i@<6>⇒∃c^= %a" lp i print g;
        let x = existand_u f g in
        Log.d3 logger "%t@<8> ∃u^∩'= %a" lp print x;
        x, succ i
      end (x, 0) uc_specs al |> fst

  let pre_u = function
    | { oldassert = true } as sd -> pre_u_oldassert sd
    | sd -> pre_u sd

  (* --- *)

  (* (\** [pre_c be sd g] computes the safe states of [g] (not uncontrollably *)
  (*     leaving [g]). *\) *)
  (* let pre_c lp *)
  (*     ({ cond; env; uc_specs; pp_bexpr } as be) *)
  (*     ({ assertion; evolutions }) : bexpr -> bexpr = *)

  (*   let tt, ff, (!~), (&&~), (||~), (=>~), (==~), ite = operators be in *)

  (*   fun e -> *)
  (*     if not (Log.check_level logger Log.Debug3) then *)
  (*       Log.d2 logger "%t→pre_c(%a)" lp pp_bexpr e *)
  (*     else begin *)
  (*       Log.d3 logger "%t→pre_c(E):" lp; *)
  (*       Log.d3 logger "%t  E  = %a" lp pp_bexpr e; *)
  (*     end; *)

  (*     let x = pre_open evolutions e in *)
  (*     Log.d3 logger "%t <E> = %a (= X)" lp pp_bexpr x; *)

  (*     let xa = x &&~ assertion in *)
  (*     Log.d3 logger "%t X∩A = %a" lp pp_bexpr xa; *)

  (*     List.fold_left begin fun e { forall_u; exist_c } -> *)

  (*       (\* Select all previous states that can lead to a state in X through a *)
  (*          controllable transition. *\) *)
  (*       let e = exist_c e in *)
  (*       Log.d3 logger "%t    ∃c,^ = %a" lp pp_bexpr e; *)
  (*       let e = forall_u e in *)
  (*       Log.d3 logger "%t ∀u,∃c,^ = %a" lp pp_bexpr e; *)
  (*       e *)

  (*     end xa uc_specs *)

  (* --- *)

  (**/**)

  (* WARNING: Take care this one operates on the set of GOOD states! *)
  let pre_a ({ be = { uc_specs; all_i_supp }; assertion; evolutions }) lp
      (g: bexpr) : bexpr -> bexpr =

    Log.d2 logger "%t@<8>  ~β  = %a" lp print g;
    let g = pre_open evolutions g in
    Log.d3 logger "%t  <^> = %a" lp print g;
    let x = !~(g &&~ assertion) in
    Log.d3 logger "%t   X @[ = %a@ (= ~(<~β>∩A))@]" lp print x;

    fun e ->
      (* Select all states of E always leading to F. *)
      Log.d3 logger "%t@<11>→pre_a(~β, %a):" lp print e;

      let y = pre_open evolutions e in
      Log.d3 logger "%t  <$> = %a" lp print y;

      let xy = x ||~ y in
      Log.d3 logger "%t@<8>  X∪^ = %a" lp print xy;
      let xy = Cudd.Bdd.forall all_i_supp xy in
      Log.d3 logger "%t@<5>  ∀i^@[ = %a@ (= pre_a(~β,$))@]" lp print xy;

      xy

  (* --- *)

  let fixedpoint' ~cached f = Util.fixedpoint_cacheable
    ~eq:(fun (a, _) (b, _) -> is_eq a b)
    ~hash:(fun (a, _) -> Hashtbl.hash a)
    ~cached
    (fun (x, i) -> f (x, succ i))

  let outter ?ofp ~lp ~rp (x, i) = match ofp with
    | None -> x, i
    | Some (f: ?lp:_ -> _) -> f ~lp ~rp (x, succ i)

  let lpx lp f =
    if Log.check_level logger Log.Debug3 then Util.pp f
    else Util.pp1 ("%t"^^f) lp

  (* --- *)

  (* Operates on BAD states. *)
  let lfp_invar_control sd ?ofp bad =
    Log.d logger "@<6>+ β = %a" print bad;
    let fixedpoint' = fixedpoint' ~cached:false in
    let betaf i : _ format = if i <= 9 then "@<2> β" else "@<1>β" in
    fun ?(lp = Util.pu) ~rp (b, i) ->
      let lp = lpx lp "+" in
      let pre_u = pre_u sd lp in
      Log.d logger "%t+> Invariance:" lp;
      let b = bad ||~ b |> norm_enums in
      Log.d2 logger "%t%u:%(%).@[ = %a@]" lp i (betaf i) print b;
      RP.handle_bdd rp ~lp ~i ~v:"β" b;
      let y = fixedpoint' begin fun (y, i) ->
        let y = y ||~ pre_u y |> norm_enums in
        Log.i logger "%t%u:%(%) @[ = %a%t@]\
                     " lp i (betaf i) print' y (log3 "@ (= β∪pre_u(β))");
        RP.handle_bdd rp ~lp ~i ~v:"β" y;
        outter ?ofp ~lp ~rp (y, i)
      end (b, i) in
      Log.d logger "%t+<" lp;
      y

  (* --- *)

  (* Operates on BAD states. *)
  let lfp_reach sd ?ofp reachable =
    Log.d logger "@<7>- χ = %a" print reachable;
    let fixedpoint' = fixedpoint' ~cached:true in
    let rhof i : _ format = if i <= 9 then "@<2> ρ" else "@<1>ρ" in
    fun ?(lp = Util.pu) ~rp (b, i) ->
      let lp = lpx lp "-" in
      let pre = pre sd lp in
      Log.d logger "%t-> Reachability:" lp;
      let r = reachable =>~ b |> norm_enums in
      Log.d2 logger "%t%u:%(%).@[ = %a@ (= ~χ∪β)@]" lp i (rhof i) print r;
      RP.handle_bdd rp ~lp ~i ~v:"ρ" r;
      let y =  fixedpoint' begin fun (y, i) ->
        let y = y &&~ (pre !~y =>~ b) |> norm_enums in
        Log.i logger "%t%u:%(%) @[ = %a%t@]\
                     " lp i (rhof i) print' y (log3 "@ (= ρ∩(β∪~pre(~ρ))");
        RP.handle_bdd rp ~lp ~i ~v:"ρ" y;
        outter ?ofp ~lp ~rp (y, i)
      end (r, i) in
      Log.d logger "%t-<" lp;
      y

  (* --- *)

  (* Operates on BAD states. *)
  let lfp_attract sd ?ofp target =
    Log.d logger "@<7>∙ ξ = %a" print target;
    let fixedpoint' = fixedpoint' ~cached:true in
    let gammaf i : _ format = if i <= 9 then "@<2> γ" else "@<1>γ" in
    fun ?(lp = Util.pu) ~rp (b, i) ->
      let lp = lpx lp "@<1>∙" in
      Log.d logger "%t∙> Attractivity:" lp;
      let pre_a = pre_a sd lp !~b in
      let c = target =>~ b |> norm_enums in
      Log.d2 logger "%t%u:%(%).@[ = %a@ (= ~ξ∪β)@]" lp i (gammaf i) print c;
      RP.handle_bdd rp ~lp ~i ~v:"γ" c;
      let y = fixedpoint' begin fun (y, i) ->
        let y = y &&~ (pre_a !~y =>~ b) |> norm_enums in
        Log.i logger "%t%u:%(%) @[ = %a%t@]\
                  " lp i (gammaf i) print' y (log3 "@ (= γ∩(β∪~pre_a(~β,~γ)))");
        RP.handle_bdd rp ~lp ~i ~v:"γ" y;
        outter ?ofp ~lp ~rp (!~y, i) |> (fun (y, i) -> (!~y, i))
      end (c, i) in
      Log.d logger "%t∙<" lp;
      y

  (**/**)

  let synth be
      ~(rp: BddapronReorderPolicy.config)
      ~(oldassert: bool)
      ~(evolutions: var equs_t)
      ~(assertion: bexpr)
      ~(init: bexpr)
      ?(reach: bexpr option = None)
      ?(attract: bexpr option = None)
      ~(bad: bexpr) : bexpr option =

    let sd = { be; evolutions; assertion; oldassert } in
    Log.d3 logger "| A = %a" print assertion;

    let rp = RP.make rp in

    let fp = match attract with
      | Some a -> Some (lfp_attract sd a)
      | None -> None
    in
    let fp = match reach with
      | Some r -> Some (lfp_reach sd ?ofp:fp r)
      | None -> fp
    in
    let fp = Some (lfp_invar_control sd ?ofp:fp bad) in

    let fp = match fp with None -> assert false | Some fp -> fp ~rp in

    (* Main computation, starting from empty space (ff): *)
    let b, i = Log.roll fp (ff, 0) in
    Log.i logger "@[Reached@ fixpoint@ after@ %i@ iteration%s.@]"
      i (if i > 1 then "s" else "");

    RP.dispose rp;

    (* TODO Maybe we should remove outputs (state variables not appearing in the
       right hand side of an equation) from b (or at least from g). *)
    Log.i  logger "@<4>β = %a" print' b;

    Log.d3 logger "I = %a" print init;

    let init = if oldassert then init else init ^~ assertion in
    let bi = Log.roll (simpl ~force_careset_update:true be) (b &&~ init) in
    Log.d3 logger "@<6>I∩β = %a" print bi;

    let success = is_ff bi in
    Log.d logger "@<8>I∩β=∅ = %b (= success)" success;

    if not success then begin
      let bi = bi ^~ init in
      Log.d logger "@<8> β↑I  = %a" print bi;
      Log.e logger "@<8> β↑I↑A= %a" print (bi ^~ assertion);
      None
    end else begin
      let g = simpl be !~b in
      Log.d3 logger "G = %a" print g;
      Some g
    end

  (* ------------------------------------------------------------------------ *)

  (** [make_controller ?no_bnd ~evolutions ~assertion g]
      builds a controller based on the given volutions (equations), assertion,
      and sets of states [g]. *)
  let make_controller ?(no_reorder = false) ?(no_bnd = false)
      ?decomp ~evolutions ~assertion g =

    let rs = CuddUtil.dynamic_reordering_save_n_stop env in

    Log.i logger "Building controller…";
    let g' = match decomp with
      | None -> Log.roll (pre_open evolutions) g
      | Some (`Auto (algo, limit))
        -> let open CuddUtil.Decomp in
          let stop b = Cudd.Bdd.size b <= limit in
          let kind = Disjunctive
          and algorithm = match algo with
            | None -> VariableBased
            | Some a -> a
          in
          (* Log.i logger "Size of disjunctive decomposition: %d\ *)
          (*              " (count ~kind ~algorithm ~stop g); *)
          let ti_timed = Log.ti_timed ~period:0.1 logger in
          Log.roll (fold ~kind ~algorithm ~stop begin fun b (i, acc) ->
            ti_timed "%u/?" i;
            Log.d3 logger "Disjunct: %a" pp_bexpr b;
            succ i, acc ||~ pre_open evolutions b
          end g) (1, ff) |> snd
      | Some (`Manual base_disjuncts)
        -> let n = List.length base_disjuncts in
          Log.i logger "@[Manual@ decomposition@ into@ %d@ disjuncts@<1>…@]" n;
          let ti_timed = Log.ti_timed ~period:0.1 logger in
          Log.roll (List.fold_left begin fun (i, acc) d ->
            ti_timed "%u/%u" i n;
            let gd = g &&~ d in
            Log.d3 logger "Disjunct: %a" pp_bexpr gd;
            succ i, if is_ff gd then acc else acc ||~ pre_open evolutions gd
          end (1, ff)) base_disjuncts |> snd
    in
    Log.d3 logger " <G> = %a" print g';

    CuddUtil.dynamic_reordering_restore env rs;

    let rs = CuddUtil.dynamic_reordering_save_n_stop_if no_reorder env in

    let k = g' &&~ assertion in

    let k, info =
      if no_bnd then k, "" else
        (Log.d logger "Computing boundary transtions…";
         let k' = k &&~ g in      (* Conditionally select boudary states only *)
         if Cudd.Bdd.size k < Cudd.Bdd.size k' then k, ""
         else (Log.d3 logger "Selecting boundary transtions only."; k', "∩G"))
    in

    CuddUtil.dynamic_reordering_restore env rs;

    Log.d logger "Simplifying controller…";
    let k = normalize_benumneg env cond k in
    (* let k = simpl (\* ~force_careset_update:true *\) be k in *)
    Log.d logger "  K @[ = %a%t@]\
                 " print k (fun fmt -> log3 "@ (=@ <G>∩A%s)" fmt info);

    k

end

(* -------------------------------------------------------------------------- *)

(** Implementation of the first algorithm of "Efficient supervisory synthesis of
    large systems" by Vahidi et al. *)
module Relational = functor (BEnvNS: BDDAPRON_ENV_NS) -> struct
  module Core = Core (BEnvNS)
  open Core
  open BEnvNS
  open BEnvNS.BAll
  open BEnvNS.BEnvExt

  type varext =
      {
        s_vars: var list;
        p_vars: var vmap;
        envext: extension;
      }

  let setup s_vars =
    Log.d logger "@[Extending@ environment@ with@ extra@ primed@ variables.@]";
    let p_vars, envext, eu = make_primes (varset s_vars) in
    { s_vars; p_vars; envext; }, eu

  let dispose ?(dirty = false) { envext } =
    if dirty then begin
      Log.d logger "@[Leaving@ environment@ as@ is@ as@ requested@ \
                      (dirty=true).@]";
      no_upgrade
    end else begin
      Log.d logger "@[Restoring@ environment.@]";
      restore_environment envext
    end

  (* --- *)

  type rel =
    | Monolithic of bexpr
    | Disjunctive of bexpr list

  let pp_rel fmt = function
    | Monolithic p -> pp_bexpr fmt p
    | Disjunctive d -> pp_lst pp_bexpr fmt d

  let as_monolithic = function
    | Monolithic p -> p
    | Disjunctive d -> List.fold_left (||~) ff d

  (* --- *)

  type data =
      {
        sd: synth_data;
        varext: varext;
        tf: rel;
        s_supp: bexpr;
        s'_supp: bexpr;
        good_s: bexpr;
        good_s': bexpr;
        good_ss': bexpr;
        to_prime: bexpr -> bexpr;
        of_prime: bexpr -> bexpr;
        pp_s_size: bexpr Util.pp;
        pp_rel_size: bexpr Util.pp;
      }

  (* --- *)

  let trans_func_dynamics rp { p_vars } { evolutions } =
    let get_prime v = PMappe.find v p_vars in
    List.fold_left begin fun (tfis, j) (i, ei) ->
      let i' = POps.var (get_prime i) in
      let t = !~(i' ==~~ ei) =>~ (i' ==~~ POps.var i) in
      RP.handle_bdd rp ~lp:Util.pu ~i:j ~v:(Format.asprintf "t[%a]" pp_var i) t;
      t :: tfis, succ j
    end ([], 0) evolutions

  (* --- *)

  let monolithic_trans_func rp varext sd =
    Log.d logger "@[Computing@ monolithic@ transition@ relation@<1>…@]";
    let tfis, n = trans_func_dynamics rp varext sd in
    let tf, _ = List.fold_left begin fun (tf, j) t ->
      Log.ti logger "%i/%i" j n;
      let tf = tf &&~ t |> simplify in
      RP.handle_bdd rp ~lp:Util.pu ~i:j ~v:"tf" tf;
      tf, succ j
    end (tt, 0) tfis in
    Monolithic tf

  (* --- *)

  let rec acc_full_disjdec acc = function
    | [] -> acc
    | x :: tl -> match Cudd.Bdd.vardisjdecomp x with
        | None -> acc_full_disjdec (x :: acc) tl
        | Some (x1, x2) -> acc_full_disjdec acc (x1 :: x2 :: tl)

  let (|.&&) l b =
    List.fold_left begin fun acc x ->
      let x' = x &&~ b in
      if is_ff x' then acc else acc_full_disjdec acc [x']
    end [] l

  let disjunctive_trans_func rp varext sd =
    Log.d logger "@[Computing@ disjunctive@ transition@ relation@<1>…@]";
    let tfis, n = trans_func_dynamics rp varext sd in
    let tf, _ = List.fold_left begin fun (tf, j) t ->
      Log.ti logger "%i/%i" j n;
      let tf = tf |.&& simplify t in
      tf, succ j
    end ([tt], 0) tfis in
    Disjunctive tf

  (* --- *)

  let transrel
      ({ be = { all_i_supp }; assertion }) (x: bexpr) : bexpr =
    Log.d3 logger "  <$> = %a" print x;
    if not (Log.check_level logger Log.Debug3)
    then Cudd.Bdd.existand all_i_supp assertion x      (* shortcut for ∃i,X∩A *)
    else let xa = x &&~ assertion in              (* decompose for logging... *)
         Log.d3 logger "@<8>  ^∩A = %a" print xa;
         let xa = exist' all_i_supp xa in
         Log.d3 logger "@<5>  ∃i^@[ = %a (= pre($))@]" print xa;
         xa

  let transrel_u
      ({ be = { uc_specs }; assertion; oldassert }) (x: bexpr) : bexpr =
    Log.d3 logger "  <$> = %a" print x;
    if oldassert then begin
      let xa = x &&~ assertion in
      Log.d3 logger "@<8>  ^∩A = %a" print xa;
      List.fold_left begin fun e { forall_c; exist_u } ->
        let e = forall_c e in
        Log.d3 logger "@<8>  ∀c^ = %a" print e;
        let e = exist_u e in
        Log.d3 logger "@<8>  ∃u^ = %a" print e;
        e
      end xa uc_specs
    end else begin
      Log.d3 logger "  A   = %a" print assertion;
      let al, _, _ = List.fold_left begin fun (acc, a, i) { forall_c; exist_i } ->
        let a' = forall_c a in
        Log.d3 logger "  A%i  = %a" i print a';
        a' :: acc, exist_i a, succ i
      end ([], assertion, 0) uc_specs in
      let al = List.rev al in
      List.fold_left2 begin fun (x, i) { forall_c; exist_c; existand_u } ai ->
        let f = forall_c x in
        Log.d3 logger "@<8>  ∀c^ = %a" print f;
        let g = ai =>~ exist_c x in
        Log.d3 logger "A%i@<6>⇒∃c^= %a" i print g;
        let x = existand_u f g in
        Log.d3 logger "@<8> ∃u^∩'= %a" print x;
        x, succ i
      end (x, 0) uc_specs al |> fst
    end

  (* --- *)

  let expand_trans_func { s_supp; s'_supp } = function
    | Disjunctive t ->
        let open Cudd.Bdd in
        let ss'_supp = support_union s_supp s'_supp in
        let tinfo t =
          let ts = project' ss'_supp t in
          let ti_supp = support_diff (support t) ss'_supp in
          (* Log.d3 logger "[t        = %a]" pp_bexpr t; *)
          (* Log.d3 logger "[ts       = %a]" pp_bexpr ts; *)
          (* Log.d3 logger "[supp_i[t]= %a]" pp_bexpr ti_supp; *)
          ts, ti_supp
        in
        let merge ((t, (ts, ti_supp)) as a, tl) ((t', (t's, t'i_supp)) as b) =
          if is_inter_empty ts t's || is_tt (support_inter ti_supp t'i_supp)
          then a, (b :: tl) (* t and t' cannot share controllable transitions *)
          else let ts_n_t's = ts &&~ t's in
               let t  = t ||~ (t' &&~ ts_n_t's)
               and t' = t' ||~ (t &&~ ts_n_t's) in
               let tl = (t', tinfo t') :: tl in
               (t, tinfo t), List.filter (fun (x, _) -> not (is_leq x t)) tl
        in
        let rec gather_ctfs acc = function
          | [] -> acc
          | tf :: tl
            -> let (tf', _), tl = List.fold_left merge (tf, []) tl in
              gather_ctfs (tf' :: acc) tl
        in
        Disjunctive (List.rev_map (fun t -> t, tinfo t) t |> gather_ctfs [])
    | tf -> tf

  (* --- *)

  let good_state_enums ~enable s_supp s'_supp =
    let s, s' =
      if enable then
        let e = full_enum_careset BEnv.env BEnv.cond in
        let enum_supp = Cudd.Bdd.support e in
        let enum_supp_s = Cudd.Bdd.support_inter s_supp enum_supp in
        let enum_supp_s' = Cudd.Bdd.support_inter s'_supp enum_supp in
        Cudd.Bdd.exist (Cudd.Bdd.support_diff enum_supp enum_supp_s) e,
        Cudd.Bdd.exist (Cudd.Bdd.support_diff enum_supp enum_supp_s') e
      else
        tt, tt
    in
    s, s', s &&~ s'

  let setup_data ?decomp rp ({ s_vars; p_vars } as varext) sd =
    let build_trans_func = match decomp with
      | None -> monolithic_trans_func
      | Some `Disj -> disjunctive_trans_func
    in
    let tf = build_trans_func rp varext sd in
    (* Build of_prime substitutions first, as some conditions involving primed
       variables and constants only have been created while constructing the
       transition function. *)
    let to_prime, of_prime = setup_fast_substs ~of_prime_first:true p_vars in
    let s_supp = BOps.bsupp' s_vars in
    let s'_supp = to_prime s_supp in
    let good_s, good_s', good_ss' =
      good_state_enums ~enable:false s_supp s'_supp
    in
    let s_size = vars_finsize BEnv.env s_vars in
    let pp_s_size fmt q =
      Format.fprintf fmt "%.0f" (Cudd.Bdd.nbminterms s_size (q &&~ good_s))
    and pp_rel_size fmt r =
      Format.fprintf fmt "%.0f" (Cudd.Bdd.nbminterms (2*s_size) (r &&~ good_ss'))
    in
    { sd; tf; varext; s_supp; s'_supp;
      good_s; good_s'; good_ss';
      to_prime; of_prime;
      pp_s_size; pp_rel_size; }

  (* --- *)

  let map_rel mk = function
    | Monolithic x -> Monolithic (mk x)
    | Disjunctive d -> Disjunctive (List.rev_map mk d)

  let transrel sd = map_rel (transrel sd)
  let transrel_u sd = map_rel (transrel_u sd)

  (* --- *)

  let image ?(orbit = tt) { s_supp; of_prime } =
    let image t r = of_prime (Cudd.Bdd.existand s_supp t r) in
    function
      | Monolithic t ->
          image (t &&~ orbit)
      | Disjunctive t ->
          let t = List.rev_map (fun t -> image (t &&~ orbit)) t in
          fun r -> List.fold_left (fun x apply -> x ||~ apply r) ff t


  let pre_image ?(orbit' = tt) { s'_supp; to_prime } =
    let pre_image t r = Cudd.Bdd.existand s'_supp t (to_prime r) in
    function
      | Monolithic t ->
          pre_image (t &&~ orbit')
      | Disjunctive t ->
          let t = List.rev_map (fun t -> pre_image (t &&~ orbit')) t in
          fun r -> List.fold_left (fun x apply -> x ||~ apply r) ff t

  (* --- *)

  let synth
      ({ s_vars; p_vars } as varext)
      ~be
      ~(rp: BddapronReorderPolicy.config)
      ~(oldassert: bool)
      ?(decomp: [> `Disj] option)
      ?(check = false)
      ~(evolutions: 'a equs_t)
      ~(assertion: bexpr)
      ~(reachable: bexpr)
      ~(init: bexpr)
      ~(bad: bexpr)
      target
      : bexpr option
      =

    let lfp_ref =
      if check
      then Some (lfp_invar_control { be; evolutions; assertion; oldassert })
      else None
    in
    let init = if oldassert then init else init ^~ assertion in

    let rp = RP.make rp in
    let { sd; tf; to_prime; of_prime; good_s; good_s'; good_ss';
          pp_s_size; pp_rel_size; } as data
        = (setup_data ?decomp rp varext
             { be; evolutions; assertion; oldassert }) in
    let tf = expand_trans_func data tf in

    (* --- *)

    Log.d logger "   tf = %a" pp_rel tf;
    let rel = transrel sd tf |> map_rel simplify in
    Log.d logger "  rel = %a" pp_rel rel;

    let reach_forward =
      let succ = image data rel in
      fixedpoint' ~cached:false begin fun (q, i) ->
        let q = q ||~ succ q |> simplify in
        Log.d2 logger "forward: q%i%t" i (l3_2 " = %a" pp_bexpr q);
        RP.handle_bdd rp ~lp:Util.pu ~i ~v:"Q" q;
        q, i
      end
    in

    let i = 0 in
    Log.d2 logger "   I  = %a" pp_bexpr init;
    let orbit, i = reach_forward (init, i) in
    let orbit' = to_prime orbit in
    let orbit_rel = orbit &&~ orbit' in
    Log.d2 logger "   O  = %a" pp_bexpr orbit;
    Log.d  logger "  |O| = %a" pp_s_size orbit;

    (* --- *)

    let rel_u = transrel_u sd tf in
    let rel = map_rel ((&&~) orbit_rel) rel |> map_rel simplify in
    let rel_u = map_rel ((&&~) orbit_rel) rel_u |> map_rel simplify in
    Log.d logger "rel_u = %a" pp_rel rel_u;

    (* --- *)

    let restricted_backward qm (qx, i) =
      let pred = pre_image ~orbit' data rel in
      let nqx = !~qx in
      fixedpoint' ~cached:false begin fun (q, i) ->
        let q = q ||~ pred q |> simplify in
        Log.d2 logger "restrict': Q%i%t" i (l3_2 " = %a" pp_bexpr q);
        RP.handle_bdd rp ~lp:Util.pu ~i ~v:"Q" q;
        q &&~ nqx, i
      end (qm &&~ nqx, i)
    in

    let uncontrollable_backward =
      let pred_u = pre_image ~orbit' data rel_u in
      fixedpoint' ~cached:false begin fun (q, i) ->
        let q = q ||~ pred_u q |> simplify in
        Log.d2 logger "uncontrollable': Q%i%t" i (l3_2 " = %a" pp_bexpr q);
        RP.handle_bdd rp ~lp:Util.pu ~i ~v:"Q" q;
        q, i
      end
    in

    let safe_state_synthesis qm (qx, i) =
      let x, i = fixedpoint' ~cached:false begin fun (x, i) ->
        let q', i = restricted_backward qm (x, i) in
        let q'', i = uncontrollable_backward (!~q', i) in
        let x, i = x ||~ q'', i + 1 in
        Log.d2 logger "safety: X%i%t" i (l3_2 " = %a" pp_bexpr x);
        RP.handle_bdd rp ~lp:Util.pu ~i ~v:"X" x;
        x, i
      end (qx, i) in
      x, i
    in

    (* --- *)

    Log.d logger "   B  = %a" pp_bexpr bad;
    Log.d logger "   R  = %a" pp_bexpr reachable;

    let k =
      if not (is_tt reachable) && not (is_leq reachable orbit) then begin
        Log.d logger "@<7>R∩¬O = %a" print (reachable &&~ !~orbit);
        Log.e logger "@[Some@ marked@ states@ are@ not@ reachable.@]";
        None
      end else begin

        let bad', _ = safe_state_synthesis reachable (bad, i - 1) in
        Log.d  logger "  B*  = %a" pp_bexpr bad';

        let safe = !~bad' &&~ pre_image ~orbit' data rel !~bad' in
        Log.d2 logger "   S  = %a" pp_bexpr safe;
        Log.d  logger "  |S| = %a" pp_s_size safe;

        begin match lfp_ref with
          | None -> ()
          | Some lfp_ref
            -> let ibad, _ = lfp_ref bad ~rp (ff, 0) in
              Log.d logger " ibad = %a" pp_bexpr ibad;
              if not (Cudd.Bdd.is_equal ibad !~safe) then begin
                Log.e logger "ibad ∩ S  = %a" pp_bexpr (ibad &&~ safe);
                Log.e logger "ibad - B* = %a" pp_bexpr (ibad &&~ !~bad');
                Log.e logger "B* - ibad = %a" pp_bexpr (bad' &&~ !~ibad);
                failwith "Mismatch in relational Boolean synthesis (which is \
                          therefore probably broken)";
              end
        end;

        if not (is_leq (init &&~ assertion) safe) then begin
          let bi = (!~safe &&~ init &&~ good_s) ^~ init in
          Log.i logger "@<8>¬S∩I  = %a" print bi;
          Log.e logger "@<8>¬S∩I↑A= %a" print (bi ^~ sd.assertion);
          None
        end else begin match target with
          | `SafeStates ->
              Some safe
          | `Controller ->
              let to_safe' = as_monolithic tf in
              let k = Cudd.Bdd.existand data.s'_supp to_safe' (to_prime safe) in
              let k = k &&~ assertion |> simplify in

              Log.d logger " K  = %a" pp_bexpr k;
              Some k
        end
      end
    in

    RP.dispose rp;

    k

end

(* -------------------------------------------------------------------------- *)

(** {2 Triangulation} *)

(** Akin a subtype of boolexpr, for default expression specifications *)
type 'a default_expr =
  [
  | `Bool of 'a boolexpr_t
  | `Order of [ `Ascend | `Descend ]
  ]

let permute_default_expr perm = function
  | `Bool b -> `Bool (BddapronUtil.permute_boolexpr perm b)
  | e -> e

module Triang = functor (BEnvNS: BDDAPRON_ENV_NS) -> struct
  open BEnvNS.BAll
  open BEnvNS.BEnv

  (* --- *)

  type eqsys_data =
      {
        allow_careset_updates: bool;
        allow_simplifications: bool;
        eqsys: bexpr;
        careset: bexpr;
      }

  (** The [allow_careset_updates] argument enables careset updates and
      simplification of expressions after each substitution (expensive if the
      number of conditions grows). *)
  let mk_eqsys_data ?(allow_simplifications=true) ?(allow_careset_updates=true)
      eqsys =
    let enum_careset = full_enum_careset env cond in
    {
      allow_simplifications;
      allow_careset_updates;
      eqsys = eqsys &&~ enum_careset;
      careset = enum_careset;
    }

  (* --- *)

  (** [step eqsys_data k v d] computes both an equation {i eq} assigning {i v}
      and satisfying {i K} (= [k]), and the new system {i K'} where {i v} has
      been substituted according to {i eq}.  The default expression {i d} is
      used whenever {i K} does not fully determine {i v}:

      - if {i v} is a Boolean variable ([d] matches [`Bool p]), then {i eq}
      assigns {i v} to [p];

      - if the domain of {i v} is an enumeration of labels [l1 … ln] ([d]
      matches [`Benum [| l1; …; ln |]]), then {i eq} assigns to {i v} the first
      label in [l1 … ln] that satisfties {i K}.

      [d] MUST NOT involve [v] itself (plus several other constraints…) *)
  let step ({ eqsys; careset } as config) = fun q v ->
    (* Controllable variables are computed from Q with c directly substituted
       by its expression to avoid combinatorial loops: *)
    let assign_v e =
      let q = vsubst q v e in
      (* Compute careset after substitution as it may introduce new
         conditions. *)
      if config.allow_careset_updates then
        Bdd.Cond.update_careset cond;
      let q, e =
        if config.allow_simplifications then
          (q ^~ careset) ^~ cond.Bdd.Cond.careset,
          (e ^~~ eqsys) ^~~ cond.Bdd.Cond.careset
        else
          q, e
      in
      Log.d3 logger "%a = %a" pp_var v pp_expr e;
      (q, (v, e))
    in
    function
      | `Bool p ->
          let c = var v in
          let qc  = cofactor q c                                 (* Q[c→tt] *)
          and qc' = cofactor q !~c in                            (* Q[c→ff] *)
          (* In case of non-deterministic choice for c (Q[c→tt] ∧ Q[c→ff] =
             tt), let it be the value of its default expression. Otherwise,
             let it be Q[c→tt]. *)
          let e = ite (qc &&~ qc') p qc in
          assign_v (Bool.to_expr e)
      | `Order way -> match Bddapron.Env.typ_of_var env v with
          | `Benum tn ->
              let `Benum labels = Bddapron.Env.typdef_of_typ env tn in
              let c = Benum.var env cond v in
              (* Build up a formula trying to assign c to each possible labels
                 of its type (l1 … ln), considered in their order of
                 declaration:

                 c = if Q[c→l1] then l1 else if Q[c→l2] then l2 else ... else
                 ln *)
              let labels = match way with
                | `Ascend -> Bdd.Labels.as_list labels |> List.rev
                | `Descend -> Bdd.Labels.as_list labels
              in
              let e = List.fold_left begin fun acc label ->
                let l = Bdd.Enum.of_label env label in match acc with
                  | None -> Some l
                  | Some e ->
                      let c = Benum.eq_label env cond c label in (* c = label *)
                      let qc = cofactor q c in                  (* Q[c→label] *)
                      Some (Benum.ite env cond qc l e)
              end None labels in
              (match e with
                | None -> assert false
                | Some e -> assign_v (Benum.to_expr e))
          | `Bint (sign, width) as typ ->
              let c = Bint.var env cond v in
              let fold = match way with
                | `Ascend -> Util.BInt.fold_descend
                | `Descend -> Util.BInt.fold_ascend
              in
              (* Similar to enums; with increasing values *)
              let e = fold sign width begin fun i acc ->
                let l = Bint.of_int env cond typ i in match acc with
                  | None -> Some l
                  | Some e ->
                      let c = Bint.eq_int env cond c i in
                      let qc = cofactor q c in
                      Some (Bint.ite env cond qc l e)
              end None in
              (match e with
                | None -> assert false
                | Some e -> assign_v (Bint.to_expr e))
          | _ -> failwith "Invalid type of controllble variable"

end

(* -------------------------------------------------------------------------- *)
