(** Controllable-Nbac representation and output module. Mostly an AST.

    @author Nicolas Berthier *)

module Make (Symb: SymbSig.T) = struct
  module Symb = Symb

  let (&) f a = f a

  module IMap = Map.Make (struct
    type t = int
    let compare = Stdlib.compare
  end)

  (** Mapping from symbols *)
  module SMap = Symb.Map

  (** Sets of symbols *)
  module SSet = Symb.Set

  (* ------------------------------------------------------------------------ *)

  (** Strictly positive Integer *)
  type positive = int

  (** Alias type for symbols *)
  type symb = Symb.t

  type symbs = SSet.t
  type deps = symbs SMap.t

  (** User-defined type names *)
  type typname = symb

  (** Enumaration labels *)
  type label = symb

  type 'f typdef' =
    | EnumDef of (label * 'f option) list
  (** Enumeration type definition *)
  type 'f typdef = 'f typdef' * 'f option

  (** Collection of enumeration type definitions *)
  type 'f typdefs = 'f typdef SMap.t

  (** Bounded integer width *)
  type width = int

  (** All numerical types *)
  type ntyp = [ `Int | `Real | `Bint of bool * width ]

  (** Enumerated type *)
  type etyp = [ `Enum of typname ]

  (** All handled types *)
  type typ = [ `Bool | etyp | ntyp ]

  (* ------------------------------------------------------------------------ *)
  (* {3 Expressions} *)

  (** Equivalence relation operators *)
  type eqrel = [ `Eq | `Ne ]

  (** Total ordering relation operators *)
  type totrel = [ eqrel | `Lt | `Le | `Gt | `Ge ]

  (** Boolean unary operator *)
  type buop = [ `Neg ]

  (** Boolean binary operator *)
  type bbop = [ `Imp ]

  (** Boolean nary operators. [`Excl] denotes {e mutual exclusion} between all its
      arguments. *)
  type bnop = [ `Conj | `Disj | `Excl ]

  (** Numerical unary operator *)
  type nuop = [ `Opp ]

  (** Numerical nary operators *)
  type nnop = [ `Sum | `Sub | `Mul | `Div ]

  (** Bitwise unary operator for Bounded Integers *)
  type luop = [ `LNot ]

  (** Bitwise binary operators for Bounded Integers *)
  type lbop = [ `LAnd | `LOr | `LXor ]

  (** Bitwise shift operators for Bounded Integers *)
  type lsop = [ `Lsl | `Lsr | `Asl | `Asr ]

  (** Polymorphic conditional operator *)
  type ('t, 'b) cond = [ `Ite of 'b * 't * 't ]

  (** Extra variant for ornament of expressions *)
  type ('t, 'f) flag = [ `Flag of 'f * 't ]

  (** Numerical expressions *)
  type 'f nexp =
    [
    | `Ref of symb
    | `Int of int
    | `Real of float
    | `Mpq of Mpqf.t
    | `Ncst of bool * width * 'f nexp        (** (un)signed extension/truncation *)
    | `Nuop of nuop * 'f nexp
    | `Nnop of nnop * 'f nexp * 'f nexp * 'f nexp list     (** (>=2)-ary operations *)
    | `Luop of luop * 'f nexp
    | `Lbop of lbop * 'f nexp * 'f nexp
    | `Lsop of lsop * 'f nexp * 'f nexp
    | ('a, 'f bexp) cond
    | ('a, 'f) flag
    ] as 'a

  (** Enumeration expressions *)
  and 'f eexp =
    [
    | `Ref of symb
    | `Enum of label
    | ('a, 'f bexp) cond
    | ('a, 'f) flag
    ] as 'a

  (** Boolean expressions *)
  and 'f bexp =
    [
    | `Ref of symb
    | `Bool of bool
    | `Buop of buop * 'f bexp
    | `Bbop of bbop * 'f bexp * 'f bexp
    | `Bnop of bnop * 'f bexp * 'f bexp * 'f bexp list   (** (>=2)-ary operations *)
    | `Bcmp of eqrel * 'f bexp * 'f bexp
    | `Ncmp of totrel * 'f nexp * 'f nexp
    | `Ecmp of eqrel * 'f eexp * 'f eexp
    | `Pcmp of eqrel * 'f exp * 'f exp               (** polymorphic comparison *)
    | `Bin of 'f bexp * 'f bexp * 'f bexp list        (** membership *)
    | `Ein of 'f eexp * 'f eexp * 'f eexp list
    | `BIin of 'f nexp * 'f nexp * 'f nexp list
    | `Pin of 'f exp * 'f exp * 'f exp list           (** polymorphic membership *)
    | ('a, 'f bexp) cond
    | ('a, 'f) flag
    ] as 'a

  (** Untyped expressions *)
  and 'f exp =
    [
    | `Bexp of 'f bexp
    | `Eexp of 'f eexp
    | `Nexp of 'f nexp
    | `Ref of symb                                     (** untyped symbol *)
    | ('a, 'f bexp) cond                                 (** untyped expression *)
    | ('a, 'f) flag
    ] as 'a

  (* ------------------------------------------------------------------------ *)
  (* {3 Variable definitions} *)

  (** Non-controllable & controllable input groups (U/C groups) *)
  type group = positive

  (** Rank of controllable inputs, in a U/C group *)
  type rank = positive

  (** Specification of default expression for controllable inputs *)
  type 'f default_spec = [ `None | `Expr of 'f exp | `Ascend | `Descend ]

  (** State variables define the transition function *)
  type 'f state_var_spec = [ `State of 'f exp * 'f option ]

  (** State variables declaration only *)
  type 'f state_var_decl = [ `State' ]

  (** Local variables represent named expressions *)
  type 'f local_var_spec = [ `Local of 'f exp * 'f option ]

  (** Input variables only belong to a group *)
  type 'f input_var_spec = [ `Input of group ]

  (** Controllable variables belong to a group, and have a {e unique} rank (the
      absence of duplicate ranks in the same group is assumed) *)
  type 'f contr_var_spec = [ `Contr of group * rank * 'f default_spec ]

  (** Output variables are akin groupped local variables (used by functions
      only) *)
  type 'f output_var_spec = [ `Output of group * 'f exp * 'f option ]

  (** All kinds of variable specifications for Controllable-Nbac variables *)
  type 'f var_spec =
    [
    | 'f state_var_spec
    | 'f state_var_decl
    | 'f local_var_spec
    | 'f input_var_spec
    | 'f contr_var_spec
    | 'f output_var_spec
    ]

  (** Variable declarations combine a type, a specification, and an optional
      flag *)
  type ('f, +'s) decls = (typ * 's * 'f option) SMap.t
  constraint 's = [< 'f var_spec]

  let typof d v = match SMap.find v d with typ, _, _ -> typ

  (* ------------------------------------------------------------------------ *)
  (* {3 Consistency checking utilities} *)

  (** Potentially localized error messages returned by {!check_node},
      {!check_func} and {!check_pred} bellow. *)
  type 'f loc_msg =
    [
    | `MError of ('f option * (Format.formatter -> unit)) list
    ]

  type 'f exp_checker = ?flag:'f -> ?typ:typ -> 'f exp -> ('f exp, 'f loc_msg list) result

  (* ------------------------------------------------------------------------ *)
  (* {3 Nodes} *)

  type 'f node_var_spec =
    [
    | 'f state_var_spec
    | 'f local_var_spec
    | 'f input_var_spec
    | 'f contr_var_spec
    ]

  (** Variable declarations for nodes. Note that the conformance between the
      declared type and any expression in the specification needs to be checked
      by using {!check_node}. *)
  type 'f node_decls = ('f, 'f node_var_spec) decls

  (** Controllable-Nbac node description. Note the initial state specification
      may not define all values of the state variables. *)
  type 'f node_desc =
      {
        cn_typs: 'f typdefs;            (** all type definitions *)
        cn_decls: 'f node_decls;        (** all variable specifications *)
        cn_init: 'f bexp;               (** initial state specification *)
        cn_assertion: 'f bexp;          (** assertion on environment *)
        cn_invariant: 'f bexp option;   (** {e invariance} property to enforce *)
        cn_reachable: 'f bexp option;   (** {e reachability} property to enforce *)
        cn_attractive: 'f bexp option;  (** {e attractivity} property to enforce *)
      }

  (** Equations sorted according to local variable dependencies. *)
  type 'f equs = (symb * 'f exp) list

  (** Consistent description of Controllable-Nbac node. *)
  type 'f checked_node_desc = 'f node_desc * 'f equs * 'f exp_checker

  (** Nodes that successfully passed the {!check_node} test. *)
  type 'f checked_node =
    [
    | `Node of 'f checked_node_desc
    ]

  (** Nodes may not already have been checked. *)
  type 'f node =
    [
    | 'f checked_node
    | `Desc of 'f node_desc
    ]

  (* ------------------------------------------------------------------------ *)
  (* {3 CFGs} *)

  type 'f cfg_var_spec =
    [
    | 'f state_var_decl
    | 'f local_var_spec
    | 'f input_var_spec
    | 'f contr_var_spec
    ]
  type 'f cfg_decls = ('f, 'f cfg_var_spec) decls

  module CFG = struct
    type 'f updates = 'f state_var_spec SMap.t
    type 'f arc =
        {
          arc_dest: label;
          arc_guard: 'f bexp option;
          arc_updates: 'f updates;
          arc_flag: 'f option;
        }
    type 'f arcs = 'f arc list
    type 'f location =
        {
          loc_name: label;
          loc_flag: 'f option;
          loc_arcs: 'f arcs;
          loc_init: 'f bexp option;
          loc_final: 'f bexp option;
          loc_assertion: 'f bexp option;
          loc_reachable: 'f bexp option;
        }
    type 'f t =
        {
          cfg_loc_var: (symb * 'f option) option;
          cfg_loc_typ: (typname * 'f option) option;
          cfg_flag: 'f option;
          cfg_locs: 'f location SMap.t;
        }

    (* Note arcs are in revered order *)
    let no_arc = []
    let mk_arc flag guard updates dest =
      { arc_dest = dest; arc_guard = guard; arc_updates = updates;
        arc_flag = flag; }
    let prepend_arc ?flag ?guard updates ~dest arcs =
      List.append arcs [ mk_arc flag guard updates dest ]
    let append_arc arcs ?flag ?guard updates ~dest =
      mk_arc flag guard updates dest :: arcs
    let iter_arcs f arcs = List.iter f (List.rev arcs)
    let fold_arcs = List.fold_right
    let fold_arcs_rev f arcs acc = List.fold_left (fun acc arc -> f arc acc) acc arcs
    let map_arcs = List.map

    let empty ?flag ?loc_var ?loc_typ () =
      { cfg_loc_var = loc_var;
        cfg_loc_typ = loc_typ;
        cfg_flag = flag;
        cfg_locs = SMap.empty }
    let location_var = function { cfg_loc_var = Some (v, _) } -> Some v | _ -> None
    let location_var' = function { cfg_loc_var = Some vf } -> Some vf | _ -> None
    let location_typ = function { cfg_loc_typ = Some (v, _) } -> Some v | _ -> None
    let location_typ' = function { cfg_loc_typ = Some vf } -> Some vf | _ -> None
    let has_location l { cfg_locs } = SMap.mem l cfg_locs
    let add_location t ?flag label ?init ?final ?assertion ?reachable arcs =
      let loc =
        { loc_name = label; loc_flag = flag; loc_arcs = arcs;
          loc_init = init; loc_final = final; loc_assertion = assertion;
          loc_reachable = reachable; }
      in
      { t with cfg_locs = SMap.add label loc t.cfg_locs }
    let find_location l { cfg_locs } = SMap.find l cfg_locs
    let iter_locations f { cfg_locs } = SMap.iter (fun _ -> f) cfg_locs
    let fold_locations f { cfg_locs } = SMap.fold (fun _ -> f) cfg_locs
    let repl_locations f ({ cfg_locs } as g) =
      let repl_loc l =
        let l' = f l in
        if l'.loc_name = l.loc_name then l'
        else failwith "Location names must not change"
      in
      { g with cfg_locs = SMap.map repl_loc cfg_locs; }
    let all_locations { cfg_locs } =
      SMap.map (fun { loc_name; loc_flag } -> loc_flag) cfg_locs |> SMap.bindings
  end

  (** Controllable-Nbac CFG description. *)
  type 'f cfg_desc =
      {
        cg_typs: 'f typdefs;
        cg_decls: 'f cfg_decls;
        cg_graph: 'f CFG.t;
        cg_init: 'f bexp;
        cg_assertion: 'f bexp;
        cg_invariant: 'f bexp option;
        cg_reachable: 'f bexp option;
        cg_attractive: 'f bexp option;
      }

  (** Consistent description of Controllable-Nbac CFGs. *)
  type 'f checked_cfg_desc = 'f cfg_desc * 'f equs * 'f exp_checker

  (** CFGs that successfully passed the {!check_cfg} test. *)
  type 'f checked_cfg =
    [
    | `Cfg of 'f checked_cfg_desc
    ]

  (** CFGs may not already have been checked. *)
  type 'f cfg =
    [
    | 'f checked_cfg
    | `Desc of 'f cfg_desc
    ]

  (* ------------------------------------------------------------------------ *)
  (* {3 Functions} *)

  type 'f func_var_spec =
    [
    | 'f local_var_spec
    | 'f output_var_spec
    | 'f input_var_spec
    ]

  (** Variable declarations for functions. Note that the conformance between the
      declared type and any expression in the specification needs to be checked
      by using {!check_func}. *)
  type 'f func_decls = ('f, 'f func_var_spec) decls

  type 'f func_desc =
      {
        fn_typs: 'f typdefs;                   (** all type definitions *)
        fn_decls: 'f func_decls;               (** all variable specifications *)
        fn_assertion: 'f bexp;                 (** assertion *)
      }

  type 'f checked_func_desc = 'f func_desc * 'f equs * 'f exp_checker

  type 'f checked_func =
    [
    | `Func of 'f checked_func_desc
    ]

  type 'f func =
    [
    | 'f checked_func
    | `Desc of 'f func_desc
    ]

  (* ------------------------------------------------------------------------ *)
  (* {3 Predicates} *)

  type 'f pred_var_spec =
    [
    | 'f local_var_spec
    | 'f contr_var_spec
    | 'f input_var_spec
    ]

  (** Variable declarations for predicates. Note that the conformance between
      the declared type and any expression in the specification needs to be
      checked by using {!check_pred}. *)
  type 'f pred_decls = ('f, 'f pred_var_spec) decls

  type 'f pred_desc =
      {
        pn_typs: 'f typdefs;                   (** all type definitions *)
        pn_decls: 'f pred_decls;               (** all variable specifications *)
        pn_value: 'f bexp;                     (** predicate expression *)
      }

  type 'f checked_pred_desc = 'f pred_desc * 'f equs * 'f exp_checker

  type 'f checked_pred =
    [
    | `Pred of 'f checked_pred_desc
    ]

  type 'f pred =
    [
    | 'f checked_pred
    | `Desc of 'f pred_desc
    ]

  (* ------------------------------------------------------------------------ *)
  (* {2 Utilities} *)

  (** Symbol creation *)
  let mk_symb = Symb.of_string

  (** Fresh symbol creation. To be used for new local variables AFTER all
      interface symbols have been passed through {!mk_symb}. *)
  let mk_fresh_symb = Symb.fresh

  (** First positive Integer *)
  let one = 1

  (** Next positive Integer *)
  let succ = succ

  (* ------------------------------------------------------------------------ *)
  (* {3 Other kinds of descriptions} *)

  (** One U/C group *)
  type 'f uc_info =
      {
        cnig_input_vars: typ SMap.t;               (** non-controllable decls *)
        cnig_contr_vars: (typ * rank * 'f default_spec) SMap.t;  (** controllable
                                                                    decls *)
        cnig_contr_vars': (symb * typ * 'f default_spec) list; (** ordered
                                                                  controllables *)
      }

  (** Type of data returned by {!gather_node_info} *)
  type 'f node_info =
      {
        cni_state_vars: typ SMap.t;           (** state variable declarations *)
        cni_uc_vars: 'f uc_info list;          (** input variable declarations *)
        cni_local_vars: typ SMap.t;           (** local variable declarations *)
        cni_local_specs: 'f exp SMap.t;        (** local variable definitions *)
        cni_trans_specs: 'f exp SMap.t;        (** state variable definitions *)
      }

  let imap_add g k v map =
    try IMap.add g (SMap.add k v (IMap.find g map)) map with
      | Not_found -> IMap.add g (SMap.singleton k v) map

  let gather_uc_decls decls =
    let u, c = SMap.fold begin fun v (t, e, _) -> match e with
      | `Input  g        -> fun (u, c) -> (imap_add g v t u, c)
      | `Contr (g, r, s) -> fun (u, c) -> (u, imap_add g v (t, r, s) c)
      | _ -> fun x -> x
    end decls (IMap.empty, IMap.empty) in
    let uc = IMap.merge begin fun _ a b -> match a, b with
      | Some a, Some b -> Some (a, b)
      | Some a, None -> Some (a, SMap.empty)
      | None, Some b -> Some (SMap.empty, b)
      | _ -> assert false
    end u c in
    let ucl = IMap.bindings uc in
    (* Already sorded w.r.t keys. *)
    (* let icl = List.sort (fun a b -> Stdlib.compare (fst a) (fst b)) icl in *)
    List.rev & List.fold_left begin fun acc (_, (u, c)) ->
      let cl = SMap.bindings c in
      let cl = List.sort
        (fun (_, (_, a, _)) (_, (_, b, _)) -> Stdlib.compare a b) cl in
      let cl = List.map (fun (v, (t, _, p)) -> (v, t, p)) cl in     (* forget rank *)
      { cnig_input_vars = u; cnig_contr_vars = c; cnig_contr_vars' = cl } :: acc
    end [] ucl

  let gather_stateful_info decls =

    let s, l, d, f = SMap.fold begin fun v (t, e, _) -> match e with
      | `State (e, _) -> fun (s, l, d, f) -> (SMap.add v t s, l, d, SMap.add v e f)
      | `State' -> fun (s, l, d, f) -> (SMap.add v t s, l, d, f)
      | `Local (e, _) -> fun (s, l, d, f) -> (s, SMap.add v t l, SMap.add v e d, f)
      | _ -> fun x -> x
    end decls (SMap.empty, SMap.empty, SMap.empty, SMap.empty) in

    { cni_state_vars = s;
      cni_uc_vars = gather_uc_decls decls;
      cni_local_vars = l;
      cni_local_specs = d;
      cni_trans_specs = f; }

  (** [gather_node_info node] computes data structures suitable for fast
      retrieval of various information about [node]. *)
  let gather_node_info (`Node ({ cn_decls }, _, _) | `Desc ({ cn_decls })) =
    gather_stateful_info (cn_decls :> ('f, 'f var_spec) decls)

  (** [gather_cfg_info cfg] computes data structures suitable for fast retrieval
      of various information about [cfg]; cni_trans_specs should be empty for
      now. *)
  let gather_cfg_info (`Cfg ({ cg_decls }, _, _) | `Desc ({ cg_decls })) =
    gather_stateful_info (cg_decls :> ('f, 'f var_spec) decls)

  (** Function I/O group *)
  type io_info =
      {
        fnig_input_vars: typ SMap.t;                         (** input decls *)
        fnig_output_vars: typ SMap.t;                        (** output decls *)
      }

  (** Type of data returned by {!gather_func_info} *)
  type 'f func_info =
      {
        fni_io_vars: io_info list;   (** I/O variable declarations *)
        fni_local_vars: typ SMap.t;  (** local variable declarations *)
        fni_all_specs: 'f exp SMap.t; (** local and output variable definitions *)
      }

  (** [gather_func_info func] computes data structures suitable for fast
      retrieval of various information about [func]. *)
  let gather_func_info (`Func ({ fn_decls }, _, _) | `Desc ({ fn_decls })) =

    let l, d = SMap.fold begin fun v (t, e, _) -> match e with
      | `Output (_, e, _) -> fun (l, d) -> (l, SMap.add v e d)
      | `Local (e,_)      -> fun (l, d) -> (SMap.add v t l, SMap.add v e d)
      | _ -> fun x -> x
    end fn_decls (SMap.empty, SMap.empty) in

    let i, o = SMap.fold begin fun v (t, e, _) -> match e with
      | `Input   g        -> fun (i, o) -> (imap_add g v t i, o)
      | `Output (g, _, _) -> fun (i, o) -> (i, imap_add g v t o)
      | _ -> fun x -> x
    end fn_decls (IMap.empty, IMap.empty) in
    let io = IMap.merge begin fun _ a b -> match a, b with
      | Some a, Some b -> Some (a, b)
      | Some a, None -> Some (a, SMap.empty)
      | None, Some b -> Some (SMap.empty, b)
      | _ -> assert false
    end i o in
    let iol = IMap.bindings io in
    (* Already sorded w.r.t keys. *)
    (* let icl = List.sort (fun a b -> Stdlib.compare (fst a) (fst b)) icl in *)
    let iol = List.rev & List.fold_left begin fun acc (_, (i, o)) ->
      { fnig_input_vars = i; fnig_output_vars = o } :: acc
    end [] iol in

    { fni_io_vars = iol;
      fni_local_vars = l;
      fni_all_specs = d; }

  (** Type of data returned by {!gather_pred_info} *)
  type 'f pred_info =
      {
        pni_uc_vars: 'f uc_info list;            (** input variable declarations *)
        pni_local_vars: typ SMap.t;             (** local variable declarations *)
        pni_local_specs: 'f exp SMap.t;          (** local variable definitions *)
      }

  (** [gather_pred_info pred] computes data structures suitable for fast
      retrieval of various information about [pred]. *)
  let gather_pred_info (`Pred ({ pn_decls }, _, _) | `Desc ({ pn_decls })) =

    let l, d = SMap.fold begin fun v (t, e, _) -> match e with
      | `Local (e,_) -> fun (l, d) -> (SMap.add v t l, SMap.add v e d)
      | _ -> fun x -> x
    end pn_decls (SMap.empty, SMap.empty) in

    { pni_uc_vars = gather_uc_decls pn_decls;
      pni_local_vars = l;
      pni_local_specs = d; }

  (** Returns the description attached to a given node. *)
  let node_desc (`Node (d, _, _) | `Desc (d)) = d

  (** Returns the description attached to a given CFG. *)
  let cfg_desc (`Cfg (d, _, _) | `Desc (d)) = d

  (** Returns the description attached to a given function. *)
  let func_desc (`Func (d, _, _) | `Desc (d)) = d

  (** Returns the description attached to a given predicate. *)
  let pred_desc (`Pred (d, _, _) | `Desc (d)) = d

  (** [sorted_node_definitions n] returns the list of local equations of node
      [n]. *)
  let sorted_node_definitions: 'f checked_node -> 'f equs = function
    | `Node (_, equs, _) -> equs

  (** [sorted_cfg_definitions g] returns the list of local equations of CFG
      [g]. *)
  let sorted_cfg_definitions: 'f checked_cfg -> 'f equs = function
    | `Cfg (_, equs, _) -> equs

  (** [sorted_func_definitions f] returns the list of local equations of function
      [f]. *)
  let sorted_func_definitions: 'f checked_func -> 'f equs = function
    | `Func (_, equs, _) -> equs

  (** [sorted_pred_definitions p] returns the list of local equations of predicate
      [n]. *)
  let sorted_pred_definitions: 'f checked_pred -> 'f equs = function
    | `Pred (_, equs, _) -> equs

  (* --- *)

  let exp_checker: _ -> 'f exp_checker = function
    | `Node (_, _, ec)
    | `Cfg (_, _, ec)
    | `Func (_, _, ec)
    | `Pred (_, _, ec) -> ec

  (** [exp_checker_from_* x] returns a function that can be used to type-check an
      expression against variables and types defined within [x]. *)
  let exp_checker_from_node = exp_checker
  let exp_checker_from_cfg = exp_checker
  let exp_checker_from_func = exp_checker
  let exp_checker_from_pred = exp_checker

  (* ------------------------------------------------------------------------ *)
  (* {3 Building, declaring and manipulating types } *)

  (** Empty set of type definitions *)
  let empty_typdefs = SMap.empty

  (** Adds a type definition into the given set. Any type previously defined with
      the same name is removed. *)
  let declare_typ = SMap.add

  (** Checks already defined typename *)
  let mem_typ = SMap.mem

  (** Traversal of type definitions *)
  let fold_typdefs = SMap.fold

  (** Type definition retrieval *)
  let find_typdef = SMap.find

  (** Builds a type name; enforces compatibility with Nbac format. *)
  let mk_typname = Symb.capitalize

  (** Builds a label; enforces compatibility with Nbac format. *)
  let mk_label s = (* Symb.capitalize *)s

  (** Builds an enumeration type definition *)
  let mk_etyp ?flag l = EnumDef (List.map (fun l -> (l, None)) l), flag
  let mk_etyp' ?flag l = EnumDef (List.map (fun (l, loc) -> (l, Some loc)) l), flag
  let mk_etyp'' ?flag l = EnumDef l, flag

  (** Returns the symbol associated to the given label *)
  let label_symb l = l

  (** Returns a mapping from labels to their associated types and declaration
      flag. *)
  let label_typs typdefs =
    SMap.fold begin fun (tn: typname) tdef acc -> match tdef with
      | EnumDef labels, _ ->
          List.fold_left begin fun acc (l, flag) ->
            SMap.add l ((`Enum tn :> typ), flag) acc
          end acc labels
    end typdefs SMap.empty

  let label_typ m =
    let m = match m with `Tds td -> label_typs td | `Map m -> m in
    fun l -> fst (SMap.find l m)

  (* ------------------------------------------------------------------------ *)
  (* {3 Flag management} *)

  (** [flag f e] decorates flaggable [e] with flag [f] *)
  let flag f e = `Flag (f, e)

  (** [deflag e] removes all decorations at the top of flaggable [e] *)
  let rec deflag = function
    | `Flag (a, e) -> deflag e
    | e -> e

  (** [apply g e] behaves as [(g (Some f) e')] if [e] is [e'] decorated with
      [f], or [(g None e)] otherwise *)
  let apply (f: 'f option -> 't -> 'a) = function
    | `Flag (a, e) -> f (Some a) e
    | e -> f None e

  (** Labeled version of {!apply}. *)
  let apply' (f: ?flag:'f -> 't -> 'a) = function
    | `Flag (a, e) -> f ~flag:a e
    | e -> f e

  (** [flagof e] returns any decoration at the top of flaggable [e] *)
  let flagof = function
    | `Flag (a, e) -> Some a
    | e -> None

  (* ------------------------------------------------------------------------ *)
  (* {2 Pretty-printing} *)

  (* TODO: handle precedence. *)

  open Format

  (** Maps of types (internal) *)
  module TMap = Map.Make (struct
    type t = typ
    let compare a b = Stdlib.compare a b
  end)

  let fpf = fprintf

  let peqrel = function
    | `Eq -> " = "
    | `Ne -> " <> "

  let ptotrel = function
    | #eqrel as op -> peqrel op
    | `Lt -> " < " | `Le -> " <= "
    | `Gt -> " > " | `Ge -> " >= "

  let plop = function
    | `LAnd -> " & "
    | `LXor -> " ^ "
    | `LOr -> " | "

  let psop = function
    | `Lsl -> " << "
    | `Lsr -> " >> "
    | `Asl -> " <<< "
    | `Asr -> " >>> "

  let plst print l s r fmt = function
    | [] -> ()
    | x::t -> fpf fmt "%s%a%a%s" l print x
        (fun fmt -> List.iter (fpf fmt "%s@ %a" s print)) t r

  let plst' print l nxt r fmt = function
    | [] -> ()
    | x::t -> fpf fmt "%s%a%a%s" l print x
        (fun fmt -> List.iter (nxt print fmt)) t r

  let pterm pol print fmt = function
    | `Nuop (`Opp, _) as e when pol -> fpf fmt "@ %a" print e
    | `Nuop (`Opp, e) -> fpf fmt "@ %a" print e
    | e when pol -> fpf fmt "@ +@ %a" print e
    | e -> fpf fmt "@ -@ %a" print e

  let ps = Symb.print
  let print_symb = ps

  let print_typdef f : 'f typdef -> unit = function
    | EnumDef lst, _ -> plst ps "{" "," "}" f (List.map fst lst)

  let print_typ fmt = function
    | `Bool -> pp_print_string fmt "bool"
    | `Int -> pp_print_string fmt "int"
    | `Real -> pp_print_string fmt "real"
    | `Bint (false, w) -> fpf fmt "uint[%d]" w
    | `Bint (true,  w) -> fpf fmt "sint[%d]" w
    | `Enum tn -> ps fmt tn

  let print_cond pb pe fmt = function
    | `Ite (b, e, f) ->
        fpf fmt "(if@ @[%a@]@ then@ @[%a@]@ else@ @[%a@])" pb b pe e pe f

  let min_bint_width s i =
    if i = 0 then 0 else
      let sign = i < 0 in
      let rec cont acc w =
        if (not sign && acc <= i) || (sign && acc < -i)
        then cont (acc lsl 1) (w + 1)
        else if sign then w + 1 else w
      in
      cont 1 0 + if s && not sign then 1 else 0

  let rec pb fmt : 'f bexp -> unit = function
    | `Ref v -> ps fmt v
    | `Bool b -> fpf fmt "%b" b
    | `Buop (`Neg,`Ref v) -> fpf fmt "not@ %a" ps v
    | `Buop (`Neg,e) -> fpf fmt "not@ (%a)" pb e
    | `Bbop (`Imp,e,f) -> plst pb "(" " =>" ")" fmt [e;f]
    | `Bnop (`Conj,e,f,l) -> plst pb "" " and" "" fmt (e::f::l)
    | `Bnop (`Disj,e,f,l) -> plst pb "(" " or" ")" fmt (e::f::l)
    | `Bnop (`Excl,e,f,l) -> plst pb "#(" "," ")" fmt (e::f::l)
    | `Bcmp (cmp,e,f) -> fpf fmt "(%a%s%a)" pb e (peqrel cmp) pb f
    | `Ecmp (cmp,e,f) -> fpf fmt "(%a%s%a)" pe e (peqrel  cmp) pe f
    | `Ncmp (cmp,e,f) -> fpf fmt "(%a%s%a)" pn e (ptotrel cmp) pn f
    | `Pcmp (cmp,e,f) -> fpf fmt "(%a%s%a)" pp e (peqrel  cmp) pp f
    | `Pin (e, f, f') -> fpf fmt "%a@ in@ %a" pp e (plst pp "{" "," "}") (f::f')
    | `Bin (e, f, f') -> fpf fmt "%a@ in@ %a" pb e (plst pb "{" "," "}") (f::f')
    | `Ein (e, f, f') -> fpf fmt "%a@ in@ %a" pe e (plst pe "{" "," "}") (f::f')
    | `BIin (e, f, f') -> fpf fmt "%a@ in@ %a" pn e (plst pn "{" "," "}") (f::f')
    | #cond as c -> print_cond pb pb fmt c
    | #flag as f -> pb fmt & deflag f
  and pe fmt : 'f eexp -> unit = function
    | `Ref v -> ps fmt v
    | `Enum s -> fpf fmt "%a" ps s
    | #cond as c -> print_cond pb pe fmt c
    | #flag as f -> pe fmt & deflag f
  and pn fmt : 'f nexp -> unit = function
    | `Ref v -> ps fmt v
    | `Int i -> fpf fmt "%d" i
    | `Real f -> fpf fmt "%f" f
    | `Mpq q -> pmpq fmt q
    (* | `Bint (s, w, i) when w >= min_bint_width s i -> fpf fmt "%d" i *)
    (* | `Bint (false, w, i) -> fpf fmt "uint[%d](%d)" w i *)
    (* | `Bint (true,  w, i) -> fpf fmt "sint[%d](%d)" w i *)
    | `Ncst (s, w, e) -> fpf fmt "%a(%a)" print_typ (`Bint (s, w)) pn e
    | `Nuop (`Opp,`Ref v) -> fpf fmt "-%a" ps v
    | `Nuop (`Opp,`Int i) -> fpf fmt "-%d" i
    | `Nuop (`Opp,`Real f) -> fpf fmt "-%f" f
    | `Nuop (`Opp,`Mpq q) -> fpf fmt "-%a" pmpq q
    | `Nuop (`Opp,e) -> fpf fmt "-@ (%a)" pn e
    | `Nnop (`Sum,e,f,l) -> plst' pn "(" (pterm true) ")" fmt (e::f::l)
    | `Nnop (`Sub,e,f,l) -> plst' pn "(" (pterm false) ")" fmt (e::f::l)
    | `Nnop (`Mul,e,f,l) -> plst pn "" " *" "" fmt (e::f::l)
    | `Nnop (`Div,e,f,l) -> plst pn "" " /" "" fmt (e::f::l)
    | `Luop (`LNot,`Ref v) -> fpf fmt "~%a" ps v
    | `Luop (`LNot,`Int i) -> fpf fmt "~%d" i
    | `Luop (`LNot,e) -> fpf fmt "~(%a)" pn e
    | `Lbop (lop,e,f) -> fpf fmt "(%a%s%a)" pn e (plop lop) pn f
    | `Lsop (lop,e,s) -> fpf fmt "(%a%s%a)" pn e (psop lop) pn s
    | #cond as c -> print_cond pb pn fmt c
    | #flag as f -> pn fmt & deflag f
  and pp fmt : 'f exp -> unit = function
    | `Bexp b -> pb fmt b
    | `Eexp e -> pe fmt e
    | `Nexp n -> pn fmt n
    | `Ref v -> ps fmt v
    | #cond as c -> print_cond pb pp fmt c
    | #flag as f -> pp fmt & deflag f
  and pmpq fmt q =
    if Mpf.is_integer (Mpqf._mpq q |> Mpf.of_mpq)
    then fpf fmt "%a.0" Mpqf.print q
    else Mpqf.print fmt q

  let print_bexp = pb
  let print_eexp = pe
  let print_nexp = pn
  let print_exp  = pp

  let print_typdefs f = SMap.iter
    (fun tn -> fpf f "%a = enum @[%a@];@\n" ps tn print_typdef)

  let print_decls f = SMap.iter
    (fun v -> fpf f "%a: %a;@\n" ps v print_typ)
  (* let print_decll f = List.iter *)
  (*   (fun (v, t) -> fpf f "%a: %a;@\n" ps v print_typ t) *)
  let print_decls' f m =
    let typ2symbs = SMap.fold begin fun s t acc ->
      let symbs = try TMap.find t acc with Not_found -> SSet.empty in
      TMap.add t (SSet.add s symbs) acc
    end m TMap.empty in
    let pset fmt s = SSet.print' ~l:"" ~r:"" fmt s in
    TMap.iter begin fun t symbs ->
      fpf f "@[<hov>%a:@ %a;@]@\n" pset symbs print_typ t;
    end typ2symbs
  let print_decll' f = List.iter (function
    | v, t, `None -> fpf f "%a: %a;@\n" ps v print_typ t
    | v, t, `Expr p -> fpf f "%a ? %a : %a;@\n" ps v pp p print_typ t
    | v, t, `Ascend -> fpf f "%a ? < : %a;@\n" ps v print_typ t
    | v, t, `Descend -> fpf f "%a ? > : %a;@\n" ps v print_typ t)

  let print_equs f = List.iter (fun (v, e) -> fpf f "%a = @[%a@];@\n" ps v pp e)
  let print_defs f = SMap.iter (fun v -> fpf f "%a = @[%a@];@\n" ps v pp)
  let print_trans f = SMap.iter (fun v -> fpf f "%a'= @[%a@];@\n" ps v pp)
  let print_predicate f = fpf f "%a;" pb

  let print_update f v (`State (e, _)) = fpf f "%a := @[%a@]" ps v pp e
  let print_updates f m =
    try let last, _ = SMap.max_binding m in
        SMap.iter (fun v (`State (e, _)) ->
          fpf f "%a := @[%a@]%s@ " ps v pp e (if v == last then "" else ";")) m
    with Not_found -> ()

  let print_loc_formulae ~runin f l =
    let plf ?implicit ~name f = function
      | Some b ->
          let is_implicit = match implicit with
            | None -> false
            | Some implicit -> deflag b = `Bool implicit
          in
          if runin && is_implicit then
            fpf f "@ !%s" name
          else if not runin && not is_implicit then
            fpf f "@;!%s@[<2> %a@]" name pb b
      | None -> ()
    in
    plf ~name:"initial" ~implicit:true f l.CFG.loc_init;
    plf ~name:"final" ~implicit:true f l.CFG.loc_final;
    plf ~name:"reachable" ~implicit:true f l.CFG.loc_reachable;
    plf ~name:"assertion" f l.CFG.loc_assertion

  let print_arc f = let open CFG in function
    | { arc_guard = (None | Some (`Bool true)); arc_updates; arc_dest }
          when SMap.is_empty arc_updates ->
        fpf f "--> %a;@;" ps arc_dest;
    | { arc_guard = (None | Some (`Bool true)); arc_updates; arc_dest } ->
        fpf f "-- @[@[<hov>%a@]--> %a@];@;\
                  " print_updates arc_updates ps arc_dest;
    | { arc_guard = Some g; arc_updates; arc_dest } ->
        fpf f "-- @[%a ?@ @[<hov>%a@]--> %a@];@;\
                  " pb g print_updates arc_updates ps arc_dest

  let print_auto f secname cfg =
    let open CFG in
    fpf f "!%s" secname;
    (match location_var cfg with Some v -> fpf f " %a" ps v; | None -> ());
    fpf f "@\n@[<v>";
    CFG.iter_locations begin fun ({ loc_name; loc_arcs; loc_flag } as loc) ->
      fpf f "!location %a@[<h>%a@]" ps loc_name
        (print_loc_formulae ~runin:true) loc;
      fpf f "%a%s" (print_loc_formulae ~runin:false) loc
        (if loc_arcs = [] then ";" else "");
      fpf f "@\n@[<v>";
      CFG.iter_arcs (print_arc f) loc_arcs;
      fpf f "@]@;";
    end cfg;
    fpf f "@]"

  let print_sec f = fpf f "!%s@\n  @[%a@]@\n"
  let print_sec' f n p m = if not (SMap.is_empty m) then print_sec f n p m
  let print_sel' f n p l = if l <> [] then print_sec f n p l
  let print_cat f = fpf f "!%s@[<2> %a@]@\n"
  let print_cao f n p = function | None -> () | Some e -> print_cat f n p e

  (** [print_node ?print_header fmt n] dumps node [n] using formatter [fmt]; if
      given, calls [print_header] with comment delimiters before outputing the
      node. *)
  let print_node ?print_header fmt
      ((`Node (node, _, _) | `Desc (node)) as n) =

    (match print_header with None -> () | Some pp -> pp fmt "(*" "*)");
    let ni = gather_node_info n in
    print_sec' fmt "typedef" print_typdefs node.cn_typs;
    print_sec  fmt "state" print_decls ni.cni_state_vars;
    List.iter begin fun g ->
      print_sec' fmt "input" print_decls g.cnig_input_vars;
      print_sel' fmt "controllable" print_decll' g.cnig_contr_vars';
    end ni.cni_uc_vars;
    print_sec' fmt "local" print_decls' ni.cni_local_vars;
    print_sec' fmt "definition" print_defs ni.cni_local_specs;
    print_sec  fmt "transition" print_trans ni.cni_trans_specs;
    print_cat  fmt "initial" print_predicate node.cn_init;
    print_cat  fmt "assertion" print_predicate node.cn_assertion;
    print_cao  fmt "invariant" print_predicate node.cn_invariant;
    print_cao  fmt "reachable" print_predicate node.cn_reachable;
    print_cao  fmt "attractive" print_predicate node.cn_attractive

  (** [print_cfg ?print_header fmt g] dumps CFG [g] using formatter [fmt]; if
      given, calls [print_header] with comment delimiters before outputing the
      CFG. *)
  let print_cfg ?print_header fmt
      ((`Cfg (cfg, _, _) | `Desc (cfg)) as c) =

    (match print_header with None -> () | Some pp -> pp fmt "(*" "*)");
    let ci = gather_cfg_info c in
    print_sec' fmt "typedef" print_typdefs cfg.cg_typs;
    print_sec' fmt "state" print_decls' ci.cni_state_vars;
    List.iter begin fun g ->
      print_sec' fmt "input" print_decls g.cnig_input_vars;
      print_sel' fmt "controllable" print_decll' g.cnig_contr_vars';
    end ci.cni_uc_vars;
    print_sec' fmt "local" print_decls' ci.cni_local_vars;
    print_sec' fmt "definition" print_defs ci.cni_local_specs;
    print_auto fmt "automaton" cfg.cg_graph;
    print_cat  fmt "initial" print_predicate cfg.cg_init;
    print_cat  fmt "assertion" print_predicate cfg.cg_assertion;
    print_cao  fmt "invariant" print_predicate cfg.cg_invariant;
    print_cao  fmt "reachable" print_predicate cfg.cg_reachable;
    print_cao  fmt "attractive" print_predicate cfg.cg_attractive

  let print_func ?print_header fmt
      ((`Func (func, _, _) | `Desc (func)) as f) =

    (match print_header with None -> () | Some pp -> pp fmt "(*" "*)");
    let fi = gather_func_info f in
    print_sec' fmt "typedef" print_typdefs func.fn_typs;
    List.iter begin fun g ->
      print_sec' fmt "input" print_decls g.fnig_input_vars;
      print_sec' fmt "output" print_decls g.fnig_output_vars;
    end fi.fni_io_vars;
    print_sec' fmt "local" print_decls' fi.fni_local_vars;
    (match f with
      | `Func (_, equs, _) ->
          print_sel' fmt "definition" print_equs equs
      | _ ->
          print_sec' fmt "definition" print_defs fi.fni_all_specs);
    print_cat  fmt "assertion" print_predicate func.fn_assertion

  (** [print_pred ?print_header fmt p] dumps predicate [p] using formatter
      [fmt]; if given, calls [print_header] with comment delimiters before
      outputing the predicate. *)
  let print_pred ?print_header fmt
      ((`Pred (pred, _, _) | `Desc (pred)) as p) =

    (match print_header with None -> () | Some pp -> pp fmt "(*" "*)");
    let pi = gather_pred_info p in
    print_sec' fmt "typedef" print_typdefs pred.pn_typs;
    List.iter begin fun g ->
      print_sec' fmt "input" print_decls g.cnig_input_vars;
      print_sel' fmt "controllable" print_decll' g.cnig_contr_vars';
    end pi.pni_uc_vars;
    print_sec' fmt "local" print_decls' pi.pni_local_vars;
    print_sec' fmt "definition" print_defs pi.pni_local_specs;
    print_cat  fmt "value" print_predicate pred.pn_value;

  (* ------------------------------------------------------------------------ *)
  (* {3 Building expressions} *)

  (** Exception raised when an invalid expression is given to the non-primed
      functions bellow. *)
  exception TypeError of (formatter -> unit)

  let pp_msg m = fun fmt -> fprintf fmt m
  let rec as_bexp: 'f exp -> 'f bexp = function
    | `Bexp e -> e
    | `Ref v -> `Ref v
    | `Ite (c, t, e) -> `Ite (c, as_bexp t, as_bexp e)
    | `Flag (f, e) -> `Flag (f, as_bexp e)
    | _ -> raise (TypeError (pp_msg "Boolean@ expression@ expected"))

  let rec as_eexp: 'f exp -> 'f eexp = function
    | `Eexp e -> e
    | `Ref v -> `Ref v
    | `Ite (c, t, e) -> `Ite (c, as_eexp t, as_eexp e)
    | `Flag (f, e) -> `Flag (f, as_eexp e)
    | _ -> raise (TypeError (pp_msg "Enumeration@ expression@ expected"))

  let rec as_nexp: 'f exp -> 'f nexp = function
    | `Nexp e -> e
    | `Ref v -> `Ref v
    | `Ite (c, t, e) -> `Ite (c, as_nexp t, as_nexp e)
    | `Flag (f, e) -> `Flag (f, as_nexp e)
    | _ -> raise (TypeError (pp_msg "Numerical@ expression@ expected"))

  (** Untyped reference *)
  let mk_ref v = `Ref v

  type 'f bexp' = [ `Bexp of 'f bexp ]
  and 'f eexp' = [ `Eexp of 'f eexp ]
  and 'f nexp' = [ `Nexp of 'f nexp ]
  (** [bexp'], [eexp'] and [nexp'] are alias types for shortening signatures;
      recall that a type [[> t]] can be coerced into a larger one without
      further annotations ({i e.g.}, the result of [(mk_bref v)] can be used
      directly as if it were of type {!exp}). *)

  let mk_bref' v :> 'f bexp = `Ref v
  let mk_bcst' c :> 'f bexp = `Bool c

  let _mk_buop': buop -> 'f bexp -> 'f bexp = fun o e -> `Buop (o, e)

  let mk_neg': 'f bexp -> 'f bexp = function
    | `Bool b -> `Bool (not b)
    | `Buop (`Neg, e) -> e
    | e -> _mk_buop' `Neg e

  let _mk_bnop': bnop -> 'f bexp -> 'f bexp -> 'f bexp list -> 'f bexp =
    fun o e f l -> `Bnop (o, e, f, l)

  let (mk_and': ('f bexp as 'b) -> 'b -> 'b), (mk_or': 'b -> 'b -> 'b) =
    let mk op neutral =
      let rec cont alt a = function
        | `Bool v when v = neutral -> a
        | `Bool _ as b -> b
        | `Bnop (o,e,f,l) as b when o = op -> (match a with
            | `Bnop (o,e',f',l') when o = op ->
                `Bnop (op, e, f, e' :: f' :: l @ l')
            | a when alt ->
                `Bnop (op, e, f, a :: l)
            | a ->
                cont true b a)
        | b when alt -> `Bnop (op, b, a, [])
        | b -> cont true b a
      in cont false
    in
    (fun a b -> mk `Conj true a b),
    (fun a b -> mk `Disj false a b)

  let _mk_bbop': bbop -> 'f bexp -> 'f bexp -> 'f bexp = fun o e f -> `Bbop (o, e, f)
  let mk_imp': 'f bexp -> 'f bexp -> 'f bexp = fun a b -> _mk_bbop' `Imp a b

  (**/**)

  let _mk_bcmp': eqrel -> 'f bexp -> 'f bexp -> 'f bexp = fun op a b ->
    match op, deflag a, deflag b with
      | `Eq, `Bool true, _ | `Ne, `Bool false, _ -> b
      | `Eq, `Bool false, _ | `Ne, `Bool true, _  -> mk_neg' b
      | `Eq, _, `Bool true  | `Ne, _, `Bool false -> a
      | `Eq, _, `Bool false | `Ne, _, `Bool true  -> mk_neg' a
      | op, _, _ -> `Bcmp (op, a, b)
  let _mk_ecmp': eqrel -> 'f eexp -> 'f eexp -> 'f bexp = fun op a b -> `Ecmp (op, a, b)
  let _mk_ncmp': totrel -> 'f nexp -> 'f nexp -> 'f bexp = fun op a b -> `Ncmp (op, a, b)
  let _mk_pcmp': eqrel -> 'f exp -> 'f exp -> 'f bexp = fun op a b -> `Pcmp (op, a, b)

  let _mk ~bexp ~eexp ~nexp ~pexp x y = match deflag x, deflag y with
    | `Bexp x, `Bexp y -> bexp x y
    | `Eexp x, `Eexp y -> eexp x y
    | `Nexp x, `Nexp y -> nexp x y
    | `Bexp x, `Ref y | `Ref y, `Bexp x -> bexp x (`Ref y)
    | `Eexp x, `Ref y | `Ref y, `Eexp x -> eexp x (`Ref y)
    | `Nexp x, `Ref y | `Ref y, `Nexp x -> nexp x (`Ref y)
    | `Bexp x, `Ite (c,t,e)
    | `Ite (c,t,e), `Bexp x -> bexp x (`Ite (c, as_bexp t, as_bexp e))
    | `Eexp x, `Ite (c,t,e)
    | `Ite (c,t,e), `Eexp x -> eexp x (`Ite (c, as_eexp t, as_eexp e))
    | `Nexp x, `Ite (c,t,e)
    | `Ite (c,t,e), `Nexp x -> nexp x (`Ite (c, as_nexp t, as_nexp e))
    | `Ref _, `Ref _ | `Ite _, `Ite _
    | `Ref _, `Ite _ | `Ite _, `Ref _ -> pexp x y
    | _ -> raise (TypeError (pp_msg "Type@ mismatch"))

  let _mk_bnary' a1 an e = function
    | [] -> a1 e
    | f :: l -> an e f l

  let _mk_bnary filter a1 an e el =
    _mk_bnary' a1 an (filter e) (List.rev_map filter el)

  let _mk_bary' filter an e f = an (filter e) (filter f)
  let _mk_nary' filter an e f el = an (filter e) (filter f) (List.map filter el)

  (**/**)

  let mk_conj': 'f bexp -> 'f bexp list -> 'f bexp = fun a -> _mk_bnary'
    (fun e -> e) (fun e f l -> List.fold_left mk_and' (mk_and' e f) l) a
  let mk_disj': 'f bexp -> 'f bexp list -> 'f bexp = fun a -> _mk_bnary'
    (fun e -> e) (fun e f l -> List.fold_left mk_or' (mk_or' e f) l) a
  let mk_excl': 'f bexp -> 'f bexp list -> 'f bexp = fun a -> _mk_bnary'
    (fun e -> `Bool true) (fun e f l -> `Bnop (`Excl, e, f, l)) a

  let mk_pin': 'f exp -> 'f exp -> 'f exp list -> 'f bexp = fun e l ll ->
    `Pin (e, l, ll)
  let mk_bin': 'f bexp -> 'f bexp -> 'f bexp list -> 'f bexp = fun e l ll ->
    `Bin (e, l, ll)
  let mk_ein': 'f eexp -> 'f eexp -> 'f eexp list -> 'f bexp = fun e l ll ->
    `Ein (e, l, ll)
  let mk_biin': 'f nexp -> 'f nexp -> 'f nexp list -> 'f bexp = fun e l ll ->
    `BIin (e, l, ll)

  let mk_beq' a = _mk_bcmp' `Eq a and mk_eeq' a = _mk_ecmp' `Eq a
  and mk_neq' a = _mk_ncmp' `Eq a and mk_peq' a = _mk_pcmp' `Eq a
  and mk_bne' a = _mk_bcmp' `Ne a and mk_ene' a = _mk_ecmp' `Ne a
  and mk_nne' a = _mk_ncmp' `Ne a and mk_pne' a = _mk_pcmp' `Ne a
  and mk_lt'  a = _mk_ncmp' `Lt a and mk_le'  a = _mk_ncmp' `Le a
  and mk_gt'  a = _mk_ncmp' `Gt a and mk_ge'  a = _mk_ncmp' `Ge a
  let mk_xor': 'f bexp -> 'f bexp -> 'f bexp = mk_bne'

  let mk_eref' v :> 'f eexp = `Ref v
  let mk_ecst' l :> 'f eexp = `Enum l

  let mk_nref' v :> 'f nexp = `Ref v
  let mk_nicst' i :> 'f nexp = `Int i
  let mk_nbicst' s w i :> 'f nexp = `Ncst (s, w, `Int i)
  let mk_nrcst' f :> 'f nexp = `Real f
  let mk_nqcst' q :> 'f nexp = `Mpq q

  let mk_ncast' s w e :> 'f nexp = `Ncst (s, w, e)

  let _mk_nuop': nuop -> 'f nexp -> 'f nexp = fun op e -> `Nuop (op, e)
  let mk_opp' a = _mk_nuop' `Opp a

  let _mk_nnop': nnop -> 'f nexp -> 'f nexp -> 'f nexp list -> 'f nexp = fun op e f l ->
    `Nnop (op, e, f, l)
  let mk_sum' a = _mk_nnop' `Sum a and mk_sub' a = _mk_nnop' `Sub a
  and mk_mul' a = _mk_nnop' `Mul a and mk_div' a = _mk_nnop' `Div a

  let _mk_luop': luop -> 'f nexp -> 'f nexp = fun op e -> `Luop (op, e)
  let mk_lnot' a = _mk_luop' `LNot a

  let _mk_lbop': lbop -> 'f nexp -> 'f nexp -> 'f nexp = fun op a b -> `Lbop (op, a, b)
  let mk_land' a = _mk_lbop' `LAnd a and mk_lor' a = _mk_lbop' `LOr a
  and mk_lxor' a = _mk_lbop' `LXor a

  let _mk_lsop': lsop -> 'f nexp -> 'f nexp -> 'f nexp = fun op a b -> `Lsop (op, a, b)
  let mk_lsl' a = _mk_lsop' `Lsl a and mk_lsr' a = _mk_lsop' `Lsr a
  let mk_asl' a = _mk_lsop' `Asl a and mk_asr' a = _mk_lsop' `Asr a

  (** Polymorphic conditional operator. *)
  let mk_cond' = function
    | `Bool true -> fun t _e -> t
    | `Bool false -> fun _t e -> e
    | c -> fun t e -> `Ite (c, t, e)

  (** Conditional operator constructor, specialized for Boolean
      expressions. This constructor may simplify the structure of expression if
      one or more of of its arguments is a constant. *)
  let mk_bcond': 'f bexp -> 'f bexp -> 'f bexp -> 'f bexp = function
    | `Bool true -> fun t _e -> t
    | `Bool false -> fun _t e -> e
    | c -> fun t e -> match t, e with
        | `Bool true, `Bool false -> c
        | `Bool false, `Bool true -> mk_neg' c
        | `Bool true, e -> mk_or' c e
        | `Bool false, e -> mk_and' (mk_neg' c) e
        | t, `Bool true -> mk_or' (mk_neg' c) t
        | t, `Bool false -> mk_and' c t
        | t, e -> `Ite (c, t, e)

  (** Conditional operator constructor, specialized for enumeration
      expressions. *)
  let mk_econd': 'f bexp -> 'f eexp -> 'f eexp -> 'f eexp = mk_cond'

  (** Conditional operator constructor, specialized for enumeration
      expressions. *)
  let mk_ncond': 'f bexp -> 'f nexp -> 'f nexp -> 'f nexp = mk_cond'

  (* --- *)

  let mk_bref v :> [> 'f bexp' ] = `Bexp (mk_bref' v)
  let mk_bcst c :> [> 'f bexp' ] = `Bexp (mk_bcst' c)

  let mk_neg e : [> 'f bexp' ] = `Bexp (mk_neg' (as_bexp e))
  let mk_and a b : [> 'f bexp' ] = `Bexp (mk_and' (as_bexp a) (as_bexp b))
  let mk_or a b : [> 'f bexp' ] = `Bexp (mk_or' (as_bexp a) (as_bexp b))
  let mk_xor a b : [> 'f bexp' ] = `Bexp (mk_xor' (as_bexp a) (as_bexp b))
  let mk_imp a b : [> 'f bexp' ] = `Bexp (mk_imp' (as_bexp a) (as_bexp b))

  let mk_conj: 'f exp -> 'f exp list -> [> 'f bexp' ] = fun a -> _mk_bnary as_bexp
    (fun e -> `Bexp e) (fun e f l -> `Bexp (List.fold_left mk_and' (mk_and' e f) l)) a
  let mk_disj: 'f exp -> 'f exp list -> [> 'f bexp' ] = fun a -> _mk_bnary as_bexp
    (fun e -> `Bexp e) (fun e f l -> `Bexp (List.fold_left mk_or' (mk_or' e f) l)) a
  let mk_excl: 'f exp -> 'f exp list -> [> 'f bexp' ] = fun a -> _mk_bnary as_bexp
    (fun e -> `Bexp (`Bool true)) (fun e f l -> `Bexp (mk_excl' e (f::l))) a

  let mk_in: 'f exp -> 'f exp -> 'f exp list -> [> 'f bexp' ] = fun e l ll ->
    `Bexp (mk_pin' e l ll)
  let mk_bin: 'f exp -> 'f exp -> 'f exp list -> [> 'f bexp' ] = fun e l ll ->
    `Bexp (mk_bin' (as_bexp e) (as_bexp l) (List.map as_bexp ll))
  let mk_ein: 'f exp -> 'f exp -> 'f exp list -> [> 'f bexp' ] = fun e l ll ->
    `Bexp (mk_ein' (as_eexp e) (as_eexp l) (List.map as_eexp ll))
  let mk_biin: 'f exp -> 'f exp -> 'f exp list -> [> 'f bexp' ] = fun e l ll ->
    `Bexp (mk_biin' (as_nexp e) (as_nexp l) (List.map as_nexp ll))

  let _mk_cmp: eqrel -> 'f exp -> 'f exp -> [> 'f bexp' ] = fun op -> _mk
    ~bexp:(fun a b -> `Bexp (_mk_bcmp' op a b))
    ~eexp:(fun a b -> `Bexp (_mk_ecmp' op a b))
    ~nexp:(fun a b -> `Bexp (_mk_ncmp' (op :> totrel) a b))
    ~pexp:(fun a b -> `Bexp (_mk_pcmp' op a b))
  let mk_eq a = _mk_cmp `Eq a and mk_ne a = _mk_cmp `Ne a

  let _mk_ncmp: totrel -> 'f exp -> 'f exp -> [> 'f bexp' ] = fun op a b ->
    `Bexp (_mk_ncmp' op (as_nexp a) (as_nexp b) :> 'f bexp)
  let mk_lt a = _mk_ncmp `Lt a and mk_le a = _mk_ncmp `Le a
  and mk_gt a = _mk_ncmp `Gt a and mk_ge a = _mk_ncmp `Ge a

  (* --- *)

  let mk_eref v : [> 'f eexp' ] = `Eexp (mk_eref' v)
  and mk_ecst l : [> 'f eexp' ] = `Eexp (mk_ecst' l)

  let mk_nref v : [> 'f nexp' ] = `Nexp (mk_nref' v)
  and mk_nicst i : [> 'f nexp' ] = `Nexp (mk_nicst' i)
  and mk_nbicst s w i : [> 'f nexp' ] = `Nexp (mk_nbicst' s w i)
  and mk_ncast s w e : [> 'f nexp' ] = `Nexp (mk_ncast' s w (as_nexp e))
  and mk_nrcst f : [> 'f nexp' ] = `Nexp (mk_nrcst' f)
  and mk_nqcst q : [> 'f nexp' ] = `Nexp (mk_nqcst' q)

  (* --- *)

  let _mk_nuop: nuop -> 'f exp -> [> 'f nexp' ] = fun op e ->
    `Nexp (_mk_nuop' op (as_nexp e))
  let mk_opp a = _mk_nuop `Opp a

  let _mk_nnop: nnop -> 'f exp -> 'f exp -> 'f exp list -> [> 'f nexp' ] = fun op ->
    _mk_nary' as_nexp (fun e f l -> `Nexp (_mk_nnop' op e f l))
  let mk_sum a = _mk_nnop `Sum a and mk_sub a = _mk_nnop `Sub a
  and mk_mul a = _mk_nnop `Mul a and mk_div a = _mk_nnop `Div a

  let _mk_luop: luop -> 'f exp -> [> 'f nexp' ] = fun op e ->
    `Nexp (_mk_luop' op (as_nexp e))
  let mk_lnot a = _mk_luop `LNot a

  let _mk_lbop: lbop -> 'f exp -> 'f exp -> [> 'f nexp' ] = fun op ->
    _mk_bary' as_nexp (fun e f -> `Nexp (_mk_lbop' op e f))
  let mk_land a = _mk_lbop `LAnd a and mk_lor a = _mk_lbop `LOr a
  and mk_lxor a = _mk_lbop `LXor a

  let _mk_lsop: lsop -> 'f exp -> 'f exp -> [> 'f nexp' ] = fun op ->
    _mk_bary' as_nexp (fun e f -> `Nexp (_mk_lsop' op e f))
  let mk_lsl a = _mk_lsop `Lsl a and mk_lsr a = _mk_lsop `Lsr a
  let mk_asl a = _mk_lsop `Asl a and mk_asr a = _mk_lsop `Asr a

  (* --- *)

  let mk_condb: 'f bexp -> 'f exp -> 'f exp -> 'f exp = fun c -> _mk
    ~bexp:(fun a b -> `Bexp (mk_bcond' c a b))
    ~eexp:(fun a b -> `Eexp (mk_cond' c a b))
    ~nexp:(fun a b -> `Nexp (mk_cond' c a b))
    ~pexp:(fun a b -> `Ite (c, a, b))

  let mk_cond c = mk_condb (as_bexp c)

  (* ------------------------------------------------------------------------ *)

  let _deflag_bexp, _deflag_eexp, _deflag_nexp, _deflag_exp =
    let mapq f =
      let rec aux = function
        | [] as x -> x
        | h :: tl as x -> let h' = f h and tl' = aux tl in
                         if h' == h && tl' == tl then x else h' :: tl'
      in aux
    in
    let pred i = max 0 (i - 1) in
    (* Helper functions to achieve physical equality for non-flagged expressions
       and memory sharing *)
    let ite tb te (`Ite (c, t, e) as x) =
      let c', t', e' as tpl' = tb c, te t, te e in
      if c' == c && t' == t && e' == e then x else `Ite tpl'
    and op mk t e x =
      let e' = t e in
      if e' == e then x else mk e
    and uop mk t (o, e) x =
      let e' = t e in
      if e' == e then x else mk o e'
    and bop mk t (o, e, f) x =
      let e', f' = t e, t f in
      if e' == e && f' == f then x else mk o e' f'
    and xop mk t (e, f, l) x =
      let e', f', l' = t e, t f, mapq t l in
      if e' == e && f' == f && l' == l then x else mk e' f' l'
    and nop mk t (o, e, f, l) x =
      let e', f', l' = t e, t f, mapq t l in
      if e' == e && f' == f && l' == l then x else mk o e' f' l'
    in
    let rec dflgb' i : 'f bexp -> 'f bexp =
      if i <= 1 then deflag else
        let i' = pred i in
        let dflgb = dflgb' i' and dflge = dflge' i'
        and dflgn = dflgn' i' and dflgp = dflgp' i' in fun x -> match x with
          | `Ref _ | `Bool _ -> x
          | `Buop tpl -> uop _mk_buop' dflgb tpl x
          | `Bbop tpl -> bop _mk_bbop' dflgb tpl x
          | `Bnop tpl -> nop _mk_bnop' dflgb tpl x
          | `Bcmp tpl -> bop _mk_bcmp' dflgb tpl x
          | `Ncmp tpl -> bop _mk_ncmp' dflgn tpl x
          | `Ecmp tpl -> bop _mk_ecmp' dflge tpl x
          | `Pcmp tpl -> bop _mk_pcmp' dflgp tpl x
          | `Bin tpl -> xop mk_bin' dflgb tpl x
          | `Ein tpl -> xop mk_ein' dflge tpl x
          | `BIin tpl -> xop mk_biin' dflgn tpl x
          | `Pin tpl -> xop mk_pin' dflgp tpl x
          | #cond as x -> ite dflgb dflgb x
          | `Flag (a, e) -> dflgb' i e
    and dflge' i : 'f eexp -> 'f eexp =
      if i <= 1 then deflag else
        let i' = pred i in
        let dflgb = dflgb' i' and dflge = dflge' i' in fun x -> match x with
          | `Ref _ | `Enum _ -> x
          | #cond as x -> ite dflgb dflge x
          | `Flag (a, e) -> dflge' i e
    and dflgn' i : 'f nexp -> 'f nexp =
      if i <= 1 then deflag else
        let i' = pred i in
        let dflgb = dflgb' i' and dflgn = dflgn' i' in fun x -> match x with
          | `Ref _  | `Int _ | `Real _ | `Mpq _ -> x
          | `Ncst (s, w, e) -> op (fun e -> `Ncst (s, w, e)) dflgn e x
          | `Nuop tpl -> uop _mk_nuop' dflgn tpl x
          | `Nnop tpl -> nop _mk_nnop' dflgn tpl x
          | `Luop tpl -> uop _mk_luop' dflgn tpl x
          | `Lbop tpl -> bop _mk_lbop' dflgn tpl x
          | `Lsop tpl -> bop _mk_lsop' dflgn tpl x
          | #cond as x -> ite dflgb dflgn x
          | `Flag (a, e) -> dflgn' i e
    and dflgp' i : 'f exp -> 'f exp =
      if i <= 1 then deflag else
        let i' = pred i in
        let dflgb = dflgb' i' and dflge = dflge' i'
        and dflgn = dflgn' i' and dflgp = dflgp' i' in fun x -> match x with
          | `Ref _ -> x
          | `Flag (a, e) -> dflgp' i e
          | #cond as x -> ite dflgb dflgp x
          | `Bexp e -> op (fun e -> `Bexp e) dflgb e x
          | `Eexp e -> op (fun e -> `Eexp e) dflge e x
          | `Nexp e -> op (fun e -> `Nexp e) dflgn e x
    in
    (fun ?(max_depth = 0) -> dflgb' max_depth),
    (fun ?(max_depth = 0) -> dflge' max_depth),
    (fun ?(max_depth = 0) -> dflgn' max_depth),
    (fun ?(max_depth = 0) -> dflgp' max_depth)

  (** [deflag_bexp ~max_depth e] recursively removes all decorations from the
      flaggable Boolean expression [e].  Removes all flags up to level
      [max_depth] (first level being [1]) if given and strictly positive; every
      flag is removed otheriwse.  Preserves physical equality for non-flagged or
      below [max_depth] (sub-)expressions. *)
  let deflag_bexp = _deflag_bexp

  (** Similar to {!deflag_bexp} for enumeration expressions *)
  let deflag_eexp = _deflag_eexp

  (** Similar to {!deflag_bexp} for numerical expressions *)
  let deflag_nexp = _deflag_nexp

  (** Similar to {!deflag_bexp} for untyped expressions *)
  let deflag_exp = _deflag_exp

  (* ------------------------------------------------------------------------ *)

  let fold_exp_deps fdl fdv (e: 'f exp) (init: 'a) : 'a =
    let fd2 fd s e f = fd (fd s e) f in
    let fdx fd s e f l = List.fold_left fd (fd2 fd s e f) l in
    let fdite fdb fd s (`Ite (c, t, e)) = fd2 fd (fdb s c) t e in
    let rec fdb s = function
      | `Ref v -> fdv v s
      | `Bool _ -> s
      | `Buop (_, e) -> fdb s e
      | `Bbop (_, e, f) -> fd2 fdb s e f
      | `Bnop (_, e, f, l) -> fdx fdb s e f l
      | `Bcmp (_, e, f) -> fd2 fdb s e f
      | `Ncmp (_, e, f) -> fd2 fdn s e f
      | `Ecmp (_, e, f) -> fd2 fde s e f
      | `Pcmp (_, e, f) -> fd2 fdp s e f
      | `Pin (e, f, l) -> fdx fdp s e f l
      | `Bin (e, f, l) -> fdx fdb s e f l
      | `Ein (e, f, l) -> fdx fde s e f l
      | `BIin (e, f, l) -> fdx fdn s e f l
      | #cond as c -> fdite fdb fdb s c
      | #flag as e -> fdb s (deflag e)
    and fde s = function
      | `Ref v -> fdv v s
      | `Enum l -> fdl l s
      | #cond as c -> fdite fdb fde s c
      | #flag as e -> fde s (deflag e)
    and fdn s = function
      | `Ref v -> fdv v s
      | `Int _ | `Real _ | `Mpq _ -> s
      | `Ncst (_, _, e) | `Nuop (_, e) | `Luop (_, e) -> fdn s e
      | `Nnop (_, e, f, l) -> fdx fdn s e f l
      | `Lbop (_, e, f) | `Lsop (_, e, f) -> fd2 fdn s e f
      | #cond as c -> fdite fdb fdn s c
      | #flag as e -> fdn s (deflag e)
    and fdp s = function
      | `Bexp e -> fdb s e
      | `Eexp e -> fde s e
      | `Nexp e -> fdn s e
      | `Ref v -> fdv v s
      | #cond as c -> fdite fdb fdp s c
      | #flag as e -> fdp s (deflag e)
    in
    fdp init e

  (** Traversal of expression dependencies. *)
  let fold_exp_dependencies fdv e = fold_exp_deps (fun _ l -> l) fdv e
  let fold_exp_dependencies' fdl fdv e = fold_exp_deps fdl fdv e

  (** [exp_dependencies e] gathers all direct dependencies of expression [e]. *)
  let exp_dependencies: 'f exp -> symbs =
    fun e -> fold_exp_dependencies SSet.add e SSet.empty

  (** [hashtbl_exp_dependencies e] gathers all direct dependencies of expression [e], returns a set implemented in a hashtable. *)
  let hashtbl_exp_dependencies: 'f exp -> (symb, unit) Hashtbl.t =
    fun e -> fold_exp_dependencies
               (fun v s -> Hashtbl.add s v (); s) e (Hashtbl.create 3)

  (** Topological sort. Ok iff the set of remaining dependencies is empty. *)
  let topo_sort: deps -> symbs list * deps =
    let gather set = SMap.fold (fun a _ -> SSet.add a) set SSet.empty in
    let forget set = SMap.map (fun d -> SSet.diff d set) in
    let rec srt acc deps =
      let sinks, rem_deps = SMap.partition (fun _ -> SSet.is_empty) deps in
      if SMap.is_empty sinks then
        List.rev acc, rem_deps
      else
        let sinks = gather sinks in
        srt (sinks :: acc) (forget sinks rem_deps)
    in
    srt []

  let rev_topo_fold: (symb -> 'a -> 'a) -> deps -> 'a -> 'a * deps = fun f ->
    let gather set = SMap.fold (fun a _ -> SSet.add a) set SSet.empty in
    let forget set = SMap.map (fun d -> SSet.diff d set) in
    let rec srt deps acc =
      let sinks, rem_deps = SMap.partition (fun _ -> SSet.is_empty) deps in
      if SMap.is_empty sinks then acc, rem_deps else
        let sinks = gather sinks in
        srt (forget sinks rem_deps) (SSet.fold f sinks acc)
    in
    srt

  (** Inverse topological traversal of dependencies. *)
  let rev_topo_iter: (symb -> unit) -> deps -> deps = fun f ->
    let gather set = SMap.fold (fun a _ -> SSet.add a) set SSet.empty in
    let forget set = SMap.map (fun d -> SSet.diff d set) in
    let rec srt deps =
      let sinks, rem_deps = SMap.partition (fun _ -> SSet.is_empty) deps in
      if SMap.is_empty sinks then rem_deps else
        let sinks = gather sinks in
        SSet.iter f sinks;
        srt (forget sinks rem_deps)
    in
    srt

  (** Transposition *)
  let transpose_deps: deps -> deps = fun deps ->
    let deps' = SMap.map (fun _ -> SSet.empty) deps in
    let deps' = SMap.fold (fun v -> SSet.fold begin fun w deps' ->
      SMap.add w (SSet.add v (SMap.find w deps')) deps'
    end) deps deps' in
    deps'

  (** All locals and states definitons *)
  let gather_all_deps (decls: ('f, [> 'f var_spec]) decls) : deps =
    SMap.fold begin fun v (_, spec, _) acc -> match spec with
      | `State (e, _) | `Local (e, _) | `Output (_, e, _) ->
          SMap.add v (exp_dependencies e) acc
      | _ -> acc
    end decls SMap.empty

  (** Exception thrown by {!sort_defs} bellow. *)
  exception CircularDefs of deps

  (** Raises {!CircularDefs} in case of circular dependencies between local
      variables. *)
  let sort_defs ~input_spec ~local_exp cn_decls =

    (* XXX: hummm that's a little messy… *)

    let non_local, all_inputs = SMap.fold
      begin fun s (_, spec, _) (non_local, inputs) ->
        (if local_exp spec = None then SSet.add s non_local else non_local),
        (if input_spec spec       then SSet.add s    inputs    else inputs)
      end
      cn_decls (SSet.empty, SSet.empty)
    in

    let local_deps, local_deps_all = SMap.fold
      begin fun v (t, spec, _) (ld, lda) ->
        match local_exp spec with
          | None -> (ld, lda)
          | Some e -> let all_deps = exp_dependencies e in
                     let local_deps = SSet.diff all_deps non_local in
                     SMap.add v local_deps ld, SMap.add v all_deps lda
      end
      cn_decls (SMap.empty, SMap.empty)
    in

    let local_equs, rem_deps = topo_sort local_deps in
    if not (SMap.is_empty rem_deps) then
      raise (CircularDefs (transpose_deps & snd & topo_sort &
                             transpose_deps rem_deps));

    let local_equs = List.rev & List.fold_left
      (fun acc vars -> SSet.fold begin fun var acc ->
        let t, spec, _ = SMap.find var cn_decls in
        match local_exp spec with
          | Some e -> (var, e) :: acc
          | None -> assert false
      end vars acc)
      [] local_equs
    in

    let indir_input_deps = List.fold_left
      begin fun local_input_deps_so_far (v, _) ->
        let local_deps = SMap.find v local_deps in
        let input_deps = SSet.inter all_inputs (SMap.find v local_deps_all) in
        let all_input_deps =
          SSet.fold
            (fun v -> SSet.union & SMap.find v local_input_deps_so_far)
            local_deps input_deps in
        SMap.add v all_input_deps local_input_deps_so_far
      end
      SMap.empty local_equs
    in

    local_equs, indir_input_deps

  (** Raises {!CircularDefs} in case of circular dependencies between local
      variables. Version without [indir_input_deps] as result *)
  let sort_defs' ~input_spec ~local_exp cn_decls =

    (* XXX: hummm that's a little messy… *)

    let non_local, all_inputs = SMap.fold
      begin fun s (_, spec, _) (non_local, inputs) ->
        (if local_exp spec = None then SSet.add s non_local else non_local),
        (if input_spec spec       then SSet.add s    inputs    else inputs)
      end
      cn_decls (SSet.empty, SSet.empty)
    in

    let local_deps, local_deps_all = SMap.fold
      begin fun v (t, spec, _) (ld, lda) ->
        match local_exp spec with
          | None -> (ld, lda)
          | Some e -> let all_deps = exp_dependencies e in
                     let local_deps = SSet.diff all_deps non_local in
                     SMap.add v local_deps ld, SMap.add v all_deps lda
      end
      cn_decls (SMap.empty, SMap.empty)
    in

    let local_equs, rem_deps = topo_sort local_deps in
    if not (SMap.is_empty rem_deps) then
      raise (CircularDefs (transpose_deps & snd & topo_sort &
                             transpose_deps rem_deps));

    let local_equs = List.rev & List.fold_left
      (fun acc vars -> SSet.fold begin fun var acc ->
        let t, spec, _ = SMap.find var cn_decls in
        match local_exp spec with
          | Some e -> (var, e) :: acc
          | None -> assert false
      end vars acc)
      [] local_equs
    in
    local_equs

  (* ------------------------------------------------------------------------ *)
  (* {2 Consistency checking} *)

  type 'f err_state =
      {
        mutable err_msgs: 'f loc_msg list;
      }

  let errors { err_msgs } = List.exists (function | `MError _ -> true) err_msgs
  let stop_on_errors es = if errors es then raise Exit

  let add_msg es msg = es.err_msgs <- msg :: es.err_msgs
  let err0 es ?flag msg = add_msg es (`MError [ flag, msg ])
  let err  es ?flag f = err0 es ?flag (fun fmt -> fprintf fmt f)
  and err1 es ?flag f a = err0 es ?flag (fun fmt -> fprintf fmt f a)
  and err2 es ?flag f a b = err0 es ?flag (fun fmt -> fprintf fmt f a b)
  and err3 es ?flag f a b c = err0 es ?flag (fun fmt -> fprintf fmt f a b c)
  and err4 es ?flag f a b c d = err0 es ?flag (fun fmt -> fprintf fmt f a b c d)
  and err5 es ?flag f a b c d e = err0 es ?flag (fun fmt -> fprintf fmt f a b c d e)

  (* --- *)

  let gather_all_labels es typdefs =
    SMap.fold
      begin fun (tn: typname) -> function
        | EnumDef labels, _ -> List.fold_right begin fun (l, flag) acc ->
          try
            let t', flag' = SMap.find l acc in
            let msgs = [ flag, fun fmt ->
              fprintf fmt "In@ declaration@ of@ type@ `%a',@ label@ `%a'@ \
                           already@ associated@ to@ type@ `%a'\
                          " ps tn ps l print_typ t' ] in
            let msgs = match flag' with
              | None -> msgs
              | Some _ -> msgs @ [ flag', fun fmt ->
                fprintf fmt "Declaration@ of@ label@ `%a'@ in@ type@ `%a'\
                            " ps l print_typ t' ] in
            add_msg es (`MError msgs); acc
          with
            | Not_found -> SMap.add l ((`Enum tn :> typ), flag) acc
        end labels
      end typdefs SMap.empty

  type typ' = [ `Any | typ ]

  let check_typdef ?flag es typdefs var = function
    | `Enum tn when not (mem_typ tn typdefs) ->
        err4 es ?flag "Unknown@ type@ `%a'@ in@ declaration@ of@ variable@ `%a'\
                      " ps tn ps var
    | _ -> ()

  let print_typ' fmt = function
    | #typ as t -> print_typ fmt t
    | _ -> pp_print_string fmt "<any type>"

  let gather_all_symbs_info es typdefs (decls: ('f, [> 'f var_spec]) decls) =
    let all_labels = gather_all_labels es typdefs in
    let labels = SMap.keys all_labels in

    let all_symbs, ranks, ldeps = SMap.fold
      begin fun v (t, spec, flag) (symbs, ranks, ldeps) ->
        let acc =
          if SMap.mem v all_labels then
            (err4 es ?flag "Symbol@ `%a'@ already@ used@ as@ label@ of@ type@ \
                            `%a'\
                           " ps v print_typ' (fst & SMap.find v all_labels);
             (symbs, ranks, ldeps))
          else
            let symbs = SMap.add v (t, flag) symbs in
            let ranks = match spec with
              | `Input g -> SMap.add v (g, 0) ranks
              | `Contr (g, r, _) -> SMap.add v (g, r) ranks
              | #output_var_spec -> SMap.add v (-1, 0) ranks
              | _ -> ranks
            in
            let ldeps = match spec with
              | `Local (e, _) -> let all_deps = exp_dependencies e in
                                let local_deps = SSet.diff all_deps labels in
                                SMap.add v local_deps ldeps
              | _ -> ldeps
            in
            (symbs, ranks, ldeps)
        in
        check_typdef ?flag es typdefs v t;
        acc
      end decls (all_labels, SMap.empty, SMap.empty)
    in

    let nlocal = SSet.diff (SMap.keys all_symbs) labels in
    let ldeps = SSet.fold
      (fun v m -> if SMap.mem v m then m else SMap.add v SSet.empty m)
      nlocal ldeps
    in

    (* Compute ranks of local variables: select minimal group/rank of non-local
       dependencies (may be rather expensive) *)
    let ranks, _rem_deps = rev_topo_fold begin fun v ranks ->
      if SMap.mem v ranks then ranks else
        let lg, lr = SSet.fold (fun ld (lg, lr) ->                      (* local *)
          let g', r' = SMap.find ld ranks in
          if g' > lg || g' = lg && r' > lr then g', r' else lg, lr)
          (SMap.find v ldeps) (one, 0)
        in
        SMap.add v (lg, lr) ranks
    end ldeps ranks in

    (* An error should be raised later if some symbol remains in _rem_deps *)
    all_labels, all_symbs, ranks

  (* ------------------------------------------------------------------------ *)

  let bndi es ?flag w i (* : 'f nexp *) =
    let s = i < 0 in
    (* (if not s && i < 0 then *)
    (*     err es ?flag "Unsigned constant@ Integer@ expected" *)
    (*  else *)
    let mw = min_bint_width s i in
    if mw > w then
      err2 es ?flag "Constant@ Integer@ too@ large:@ %i@ bits@ needed@ \
                     for@ %(%)@ representation\
                    " mw (if s then "a@ signed" else "an@ unsigned");
    (`Ncst (s, w, `Int i))

  (* coercing numerical expressions *)

  let force_explicit_cast = ref false

  let coercen es s ?flag ?(explicit = false) (td: ntyp) (ts: typ') nexp =
    let explicit = explicit || !force_explicit_cast in
    let apply_reflag f = function
      | `Flag (a, _) as e -> `Flag (a, apply' f e)
    in
    let err_sign ?flag e = err es ?flag "Unsigned Integer@ expected"; e in
    let rec i2r ?flag : 'f nexp -> 'f nexp = function
      | `Ref v as x when ts = `Int -> x
      | `Ref v -> err2 es ?flag "Unable@ to@ coerce@ variable@ `%a'@ into@ \
                                type@ `real'" ps v; `Ref v
      | `Int i -> `Real (float_of_int i)
      |(`Real _ | `Mpq _) as e -> e
      | `Nuop (op, e) -> `Nuop (op, i2r e)
      | `Nnop (op, e, f, l) -> `Nnop (op, i2r e, i2r f, List.map i2r l)
      | `Ite (b, e, f) -> `Ite (b, i2r e, i2r f)
      | #flag as f -> apply_reflag i2r f
      | _ -> failwith "Internal error during coercion to real"
    and i2bi s w ?flag : 'f nexp -> 'f nexp =
      let rec i2bi' ?flag = function
        | `Ref v -> err4 es ?flag "Unable@ to@ coerce@ variable@ `%a'@ into@ \
                                  type@ `%a'" ps v print_typ (`Bint (s,w));
            `Ref v
        | `Int i when explicit -> bndi es ?flag w i
        | `Int i -> let `Ncst (s', w', `Int i) as e = bndi es ?flag w i in
                   bi2bi s w s' w' ?flag e
        | `Nuop (`Opp, _) as e when not s -> err_sign ?flag e
        | `Nuop (op, e) -> `Nuop (op, i2bi' e)
        | `Nnop (op, e, f, l) -> `Nnop (op, i2bi' e, i2bi' f, List.map i2bi' l)
        | `Luop (op, e) -> `Luop (op, i2bi' e)
        | `Lbop (op, e, f) -> `Lbop (op, i2bi' e, i2bi' f)
        | `Ite (b, e, f) -> `Ite (b, i2bi' e, i2bi' f)
        | #flag as f -> apply_reflag i2bi' f
        | _ -> failwith "Internal error during coercion to bounded integer"
      in
      i2bi' ?flag
    and bi2bi s w s' w' ?flag : 'f nexp -> 'f nexp =
      assert (not explicit);
      let rec bi2bi' ?flag = function
        | `Ncst (_, _, `Int i) as e when not s && i < 0 -> err_sign ?flag e
        | `Ncst (_, _, `Int i) when i >= 0 -> `Ncst (s, w, `Int i)
        | `Ncst (_, _, e) when s = s' && w' >= w -> `Ncst (s, w, e)
        | `Nuop (`Opp, _) as e when not s' -> err_sign ?flag e
        | `Ref _ | `Ncst _
        | `Nuop _ | `Nnop _ | `Luop _ | `Lbop _ | `Ite _ as e -> `Ncst (s, w, e)
        | #flag as f -> apply_reflag bi2bi' f
        | _ -> failwith "Internal error during casting of bounded integers"
      in
      bi2bi' ?flag
    in
    match td, ts with
      | _, `Any ->
          `Any, nexp                                  (* skip error reporting *)
      | td, (#ntyp as ts) when td = ts ->
          (ts, nexp)
      | `Real, _ ->
          `Real, i2r ?flag nexp
      | `Bint (s, w) as td, `Int (* when not explicit *) ->
          td, i2bi s w ?flag nexp
      | `Bint (s, w) as td, `Bint (s', w') when not explicit ->
          td, bi2bi s w s' w' ?flag nexp
      | `Bint _, (`Bint _ (* | `Int *)) when explicit ->
          err5 es ?flag "Incompatible@ types@ in@ %t:@ type@ `%a'@ must@ be@ \
                         explicitly@ coerced@ into@ type@ `%a'\
                        " (pp_msg s) print_typ' ts print_typ td; `Any, nexp
      | _ ->
          err5 es ?flag "Incompatible@ types@ in@ %t:@ type@ `%a'@ cannot@ be@ \
                         coerced@ into@ type@ `%a'\
                        " (pp_msg s) print_typ' ts print_typ td; `Any, nexp

  let compatypn es s ?flag t1 e1 t2 e2 = match t1, t2 with
    | `Any, _ | _, `Any -> `Any, e1, e2
    | t1, t2 when t1 = t2 -> t1, e1, e2
    | `Real, _ -> `Real, e1, coercen es s ?flag `Real t2 e2 |> snd
    | _, `Real -> `Real, coercen es s ?flag `Real t1 e1 |> snd, e2
    | (`Bint _ as t1), _ -> t1, e1, coercen es s ?flag t1 t2 e2 |> snd
    | _, (`Bint _ as t2) -> t2, coercen es s ?flag t2 t1 e1 |> snd, e2
    | _ -> err5 es ?flag "Incompatible@ types@ in@ numerical@ %t:@ type@ `%a'@ \
                         is@ not@ compatible@ with@ type@ `%a'\
                        " (pp_msg s) print_typ' t1 print_typ' t2; `Any, e1, e2

  (* --- *)

  (* coercing polymorphic expressions *)

  let coercep es s ?flag td (ts, e) : typ' * 'f exp = match td, ts with
    | _, `Any -> `Any, e
    | td, ts when (td :> typ') = ts -> (td :> typ'), e
    | (#ntyp as td), (#ntyp as ts) ->
        let e = as_nexp e in
        let flag = match flagof e with None -> flag | flag -> flag in
        let t, e = coercen es s ?flag ~explicit:true td ts e in
        t, `Nexp e
    | _ -> err5 es ?flag "Incompatible@ types@ in@ %t:@ type@ `%a'@ cannot@ be@ \
                         coerced@ into@ type@ `%a'\
                        " (pp_msg s) print_typ' ts print_typ td; `Any, e

  (* strict type equality: no coercion *)
  let compatyps es s ?flag t1 e1 t2 e2 = match t1, t2 with
    | `Any, _ | _, `Any -> `Any, e1, e2
    | t1, t2 when t1 = t2 -> t1, e1, e2
    | _ -> err5 es ?flag "Incompatible@ types@ in@ %t:@ type@ `%a'@ is@ not@ \
                         compatible@ with@ type@ `%a'\
                        " (pp_msg s) print_typ' t1 print_typ' t2; `Any, e1, e2

  let compatypp es s ?flag t1 e1 t2 e2 = match t1, t2 with
    | (#ntyp as t1), (#ntyp as t2) ->
        let e1, e2 = as_nexp e1, as_nexp e2 in
        let t, e1, e2 = compatypn es s ?flag t1 e1 t2 e2 in
        (t :> typ'), `Nexp e1, `Nexp e2
    | _ -> compatyps es s ?flag t1 e1 t2 e2

  (* --- *)

  let chkcond es st ?flag cb cp ct = function
    | `Ite (b, e1, e2) ->
        let s: (unit, formatter, unit) format = "conditional" in
        let _, b = cb st ?flag b
        and t1, e1 = cp st ?flag e1
        and t2, e2 = cp st ?flag e2 in
        let t, e1, e2 = ct es s ?flag t1 e1 t2 e2 in
        t, `Ite (b, e1, e2)

  let pnoptyp compatyp chktyp init f l =
    List.fold_left begin fun (t, el) (t2, e2) -> match el with
      | [] -> let t, _, e = compatyp t e2 t2 e2 in (t, [e]) (* ← XXX not so sure *)
      | e1 :: tl -> let t, e1, e2 = compatyp t e1 t2 e2 in (t, e2 :: e1 :: tl)
    end init (chktyp f :: List.rev_map chktyp l)

  let intyp mk compatyp chktyp ?flag (t, e) f l =
    let _t, lst = pnoptyp (compatyp ?flag) chktyp (t, []) f l in
    `Bool, mk e (List.hd lst) (List.tl lst)

  let bintyp es t =
    intyp mk_bin' (compatyps es "Boolean@ membership@ test") t
  and eintyp es t =
    intyp mk_ein' (compatyps es "enumeration@ membership@ test") t
  and biintyp es t =
    intyp mk_biin' (compatypn es "membership@ test") t

  (* --- *)

  let as_b = function
    | `Bool, e -> `Bool, as_bexp e
    | t, _ -> t, mk_bref' Symb.dummy
  and as_e = function
    | #etyp as t, e -> t, as_eexp e
    | t, _ -> t, mk_eref' Symb.dummy
  and as_n = function
    | #ntyp as t, e -> t, as_nexp e
    | t, _ -> t, mk_nref' Symb.dummy
  and as_p = function
    | `Any, e -> `Any, e
    | `Bool, e -> `Bool, `Bexp (as_bexp e)
    | #etyp as t, e -> t, `Eexp (as_eexp e)
    | #ntyp as t, e -> t, `Nexp (as_nexp e)

  let chkb es ?flag = function
    | `Bool, e -> `Bool, as_bexp e
    | t, _
      -> err es ?flag "Boolean@ expression@ expected"; t, mk_bref' Symb.dummy

  let chke es ?flag = function
    | #etyp as t, e -> t, as_eexp e
    | t, _
      -> err es ?flag "Enumeration@ expression@ expected"; t, mk_eref' Symb.dummy

  let chkbnd es ?flag ?(bnd = false) (t, e) =
    if not bnd then t, e else match t, e with
      | (`Int | `Bint _) as t, e -> t, e
      | #typ as t, e ->
          err2 es ?flag "Integer@ constant@ or@ Bounded@ Integer@ expected:@ \
                         `%a'@ found" print_typ t; `Any, e
      | x -> (* err es ?flag "Bounded@ Integer@ expected."; (already rep.) *) x

  (* --- *)

  type ('f, 'e) checker = (?flag:'f -> symb -> typ' * 'f exp) -> ?flag:'f -> 'e -> typ' * 'e

  type 'f exptyp_checkers =
      {
        ptyp: ('f, 'f exp) checker;
        btyp: ('f, 'f bexp) checker;
      }

  let gen_exptyp_checkers es ltyp =

    let typ = fst and exp = snd in
    let apply_reflag f = function
      | `Flag (a, _) as e -> let x = apply' f e in typ x, `Flag (a, exp x)
    in

    let rec ptyp st ?flag : 'f exp -> typ' * 'f exp = function
      | `Ref v -> as_p (st ?flag v)
      | `Bexp e -> let e = btyp st ?flag e in typ e, `Bexp (exp e)
      | `Eexp e -> let e = etyp st ?flag e in typ e, `Eexp (exp e)
      | `Nexp e -> let e = ntyp st ?flag e in typ e, `Nexp (exp e)
      | #cond as c -> as_p (chkcond es st ?flag btyp ptyp compatypp c)
      | #flag as f -> apply_reflag (ptyp st) f
    and btyp st ?flag : 'f bexp -> typ' * 'f bexp = function
      | `Ref v -> chkb es ?flag (st ?flag v)
      | `Bool _ as e -> `Bool, e
      | `Buop (o,e) -> `Bool, `Buop (o, exp & btyp st e)
      | `Bbop (o,e,f)-> `Bool, `Bbop (o, exp & btyp st e, exp & btyp st f)
      | `Bnop (o,e,f,l)-> `Bool, `Bnop (o, exp & btyp st e, exp & btyp st f,
                                        List.map (fun e -> exp & btyp st e) l)
      | `Bcmp (c,e,f) -> `Bool, `Bcmp (c, exp & btyp st e, exp & btyp st f)
      | `Ncmp (c,e,f) -> ncmptyp st ?flag c e f
      | `Ecmp (c,e,f) -> pcmptyp st ?flag c (`Eexp e) (`Eexp f)
      | `Pcmp (c,e,f) -> pcmptyp st ?flag c e f
      | `Pin (e,f,f') -> pintyp st ?flag e f f'
      | `Bin (e,f,f') -> bintyp es (btyp st) ?flag (btyp st e) f f'
      | `Ein (e,f,f') -> eintyp es (etyp st) ?flag (etyp st e) f f'
      | `BIin (e,f,f') -> biintyp es (bityp st) ?flag (bityp st e) f f'
      | #cond as c -> chkcond es st ?flag btyp btyp compatyps c
      | #flag as f -> apply_reflag (btyp st) f
    and etyp st ?flag : 'f eexp -> typ' * 'f eexp = function
      | `Ref v -> chke es ?flag (st ?flag v)
      | `Enum l as e -> ltyp ?flag l, e
      | #cond as c -> chkcond es st ?flag btyp etyp compatyps c
      | #flag as f -> apply_reflag (etyp st) f
    and ntyp st ?flag ?(bnd = false) : 'f nexp -> typ' * 'f nexp = function
      | `Ref v -> chkbnd es ?flag ~bnd (as_n (st ?flag v))
      | `Int _ as e -> chkbnd es ?flag ~bnd (`Int, e)
      |(`Real _ | `Mpq _) as e -> chkbnd es ?flag ~bnd (`Real, e)
      | `Ncst (s, w, e) -> bicsttyp st ?flag s w e
      | `Nuop (o,e) -> let t, e = ntyp st ~bnd e in t, `Nuop (o, e)
      | `Nnop (o,e,f,l) -> nnoptyp st ?flag ~bnd o e f l
      | `Luop (o,e) -> let t, e = bityp st e in t, `Luop (o, e)
      | `Lbop (o,e,f) -> lboptyp st ?flag o e f
      | `Lsop (o,e,s) -> lsoptyp st ?flag o e s
      | #cond as c -> chkcond es st ?flag btyp (ntyp ~bnd) compatypn c
      | #flag as f -> apply_reflag (ntyp st ~bnd) f
    and bityp st ?flag e = ntyp st ~bnd:true ?flag e
    and pintyp st ?flag e f l : typ' * 'f bexp =
      let t, e = ptyp st e in
      let pintyp' mk as_x ttyp e =
        let chktd e = try as_x (t, e) |> exp |> ttyp st ?flag with
          | TypeError msg -> err0 es ?flag:(flagof e) msg; as_x (`Any, e)
        in
        mk es (fun x -> x) ?flag (t, e) (chktd f) (List.map chktd l)
      in
      let ptyp_exp st ?flag e = ptyp st ?flag e |> exp in
      match t with
        | `Any -> `Any, `Pin (e, ptyp_exp st f, List.map (ptyp_exp st) l)
        | `Bool -> pintyp' bintyp as_b btyp (as_bexp e)
        | #etyp -> pintyp' eintyp as_e etyp (as_eexp e)
        | #ntyp -> pintyp' biintyp as_n bityp (as_nexp e)
    and pcmptyp st ?flag c e1 e2: typ' * 'f bexp =
      let t1, e1 = ptyp st e1 and t2, e2 = ptyp st e2 in
      let t, e1, e2 = compatypp es "comparison" ?flag t1 e1 t2 e2 in
      match t with
        | `Any -> `Any, `Pcmp (c, e1, e2)
        | `Bool -> `Bool, `Bcmp (c, as_bexp e1, as_bexp e2)
        | #etyp -> `Bool, `Ecmp (c, as_eexp e1, as_eexp e2)
        | #ntyp -> `Bool, `Ncmp ((c:> totrel), as_nexp e1, as_nexp e2)
    and ncmptyp st ?flag c e1 e2: typ' * 'f bexp =
      let t1, e1 = ntyp st e1 and t2, e2 = ntyp st e2 in
      let _, e1, e2 = compatypn es "comparison" ?flag t1 e1 t2 e2 in
      `Bool, `Ncmp (c, e1, e2)
    and nnoptyp st ?flag ?bnd o e f l: typ' * 'f nexp =
      let ntyp = ntyp st ?bnd in
      let t1, e1 = ntyp e in
      let t, el =
        pnoptyp (compatypn es "operation" ?flag) ntyp (t1, [e1]) f l
      in
      match List.rev el with
        | e :: f :: tl -> t, `Nnop (o, e, f, tl)
        | _ -> assert false
    and bicsttyp st ?flag s w e : typ' * 'f nexp =
      let td = `Bint (s, w) in
      let ts, e = bityp st e in
      coercen es "cast@ operation" ?flag td ts e
    and lboptyp st ?flag o e1 e2: typ' * 'f nexp =
      let t1, e1 = bityp st e1 and t2, e2 = bityp st e2 in
      let t, e1, e2 = compatypn es "bitwise@ operation" ?flag t1 e1 t2 e2 in
      (t :> typ'), `Lbop (o, e1, e2)
    and lsoptyp st ?flag o e s: typ' * 'f nexp =
      let t, e = bityp st e in
      let _, s = bityp st s in
      (t :> typ'), `Lsop (o, e, s)
    in

    { ptyp; btyp; }

  (* --- *)

  type 'f styp = ?flag:'f -> symb -> typ' * 'f exp

  type 'f symtyp_checkers =
      {
        ltyp: ?flag:'f -> symb -> typ';
        styp: 'f styp;
        styp_no_inputs: inputp:(symb -> bool) -> indir_input_deps:deps -> 'f styp;
        styp_check_rank: int -> int -> 'f styp;
      }

  let gen_symtyp_checkers es labels symbs ranks =

    (* Label type retrieval *)
    let ltyp ?flag l =
      try (fst & SMap.find l labels :> typ') with
        | Not_found -> `Any
    in

    (* Symbol type retrieval *)
    let styp ?flag = function
      | s when SMap.mem s labels -> ltyp ?flag s, mk_ecst s
      | s -> try (fst & SMap.find s symbs :> typ'), mk_ref s with
          | Not_found ->
              err2 es ?flag "Undefined@ symbol@ `%a'" ps s; (`Any, mk_ref s)
    in
    let styp_no_inputs ~inputp ~indir_input_deps ?flag s =
      match styp ?flag s with
        |(`Any, _) as res -> res
        | res when inputp s ->
            err2 es ?flag "Incorrect@ use@ of@ input@ `%a'" ps s; res
        | res -> try let input_deps = SMap.find s indir_input_deps in
                     if not (SSet.is_empty input_deps) then
                       err4 es "Incorrect@ use@ of@ variable@ `%a',@ as@ it@ \
                               (indirectly)@ depends@ on@ the@ following@ \
                               inputs:@ %a" ps s SSet.print input_deps; res
          with
            | Not_found -> res
    in
    let styp_check_rank max_g max_r ?flag s =
      match styp ?flag s with
        |(`Any, _) as res -> res
        | res -> try match SMap.find s ranks with
            | g, r when g > max_g || (g = max_g && r >= max_r) ->
                err2 es ?flag "Incorrect@ use@ of@ `%a'" ps s; res
            | _ -> res
          with
            | Not_found -> res
    in

    { ltyp; styp; styp_no_inputs; styp_check_rank; }

  (* --- *)

  let check_exp { styp } { ptyp } ?flag ?(typ: typ option) e =
    let es = { err_msgs = [] } in
    try
      let xt, _ as x = ptyp styp ?flag e in
      let _, e = match typ with
        | Some t -> coercep es "expression" ?flag t x
        | None ->
            if xt = `Any then
              err2 es "Unable@ to@ type@ expression@ `%a'" print_exp e;
            x
      in
      stop_on_errors es;
      Ok e
    with
      | Exit -> Error (List.rev es.err_msgs)

  (* --- *)

  let chkldef es { styp } { ptyp } _v (tv, `Local (e, flag), fv) =
    let e = ptyp styp ?flag e |> coercep es "definition" ?flag tv in
    tv, `Local (snd e, flag), fv

  let chkcspec es { styp_check_rank } { ptyp } v
      ((tv, `Contr (g, r, d), flag) as x) =
    match d with
      | `None | `Ascend | `Descend -> x
      | `Expr p when tv <> `Bool ->
          let flag = match flagof p with None -> flag | flag -> flag in
          err2 es ?flag "Spurious@ default@ expression@ for@ non-Boolean@ \
                         variable@ %a" ps v; x
      | `Expr e ->
          let st = styp_check_rank g r in
          let e = ptyp st ?flag e |> coercep es "default@ expression" ?flag tv in
          tv, `Contr (g, r, `Expr (snd e)), flag

  (* for functions *)
  let chkodef es { styp } { ptyp } _v (tv, `Output (g, e, flag), fv) =
    let e = ptyp styp ?flag e |> coercep es "output@ definition" ?flag tv in
    tv, `Output (g, snd e, flag), fv

  (* --- *)

  let check_node'
      ({ cn_typs; cn_decls; cn_init; cn_assertion;
         cn_invariant; cn_reachable; cn_attractive } as node: 'f node_desc)
      : 'f checked_node option * 'f loc_msg list =

    let es = { err_msgs = [] } in

    (* --- *)

    let labels, symbs, ranks = gather_all_symbs_info es cn_typs
      (cn_decls :> ('f, [> 'f var_spec]) decls) in

    let { ltyp; styp; styp_no_inputs; } as stc
        = gen_symtyp_checkers es labels symbs ranks in
    let { ptyp; btyp; } as etc
        = gen_exptyp_checkers es ltyp in

    (* --- *)

    let chkeq' st ?flag (tv: typ) fv e =
      ptyp st ?flag e |> coercep es "assignment" ?flag tv |> snd
    in
    let chkdecl v (tv, spec, fv) = match spec with
      | `State (e, flag) -> tv, `State (chkeq'  styp ?flag tv fv e, flag), fv
      | `Local _ as spec -> chkldef es stc etc v (tv, spec, fv)
      | `Contr _ as spec -> chkcspec es stc etc v (tv, spec, fv)
      | _ -> (tv, spec, fv)
    in
    let chkb st e = snd & btyp st e in

    (* --- *)

    try
      let cn_decls      = SMap.mapi chkdecl cn_decls
      and cn_assertion  = chkb styp cn_assertion in
      stop_on_errors es;

      let input_spec = function
        | #input_var_spec | #contr_var_spec -> true
        | _ -> false
      in
      let defs, indir_input_deps =
        sort_defs
          ~input_spec
          ~local_exp:(function | `Local (e, _) -> Some e | _ -> None)
          cn_decls in
      stop_on_errors es;

      let chkb_opt st = function | None -> None | Some e -> Some (chkb st e) in
      let inputp v = try let _, s, _ = SMap.find v cn_decls in input_spec s with
        | Not_found -> false in
      let styp = styp_no_inputs ~inputp ~indir_input_deps in  (* ¬inputs for: *)
      let cn_init       = chkb     styp cn_init
      and cn_invariant  = chkb_opt styp cn_invariant
      and cn_reachable  = chkb_opt styp cn_reachable
      and cn_attractive = chkb_opt styp cn_attractive in
      stop_on_errors es;

      let n =
        { node with cn_decls; cn_init; cn_assertion; cn_invariant;
          cn_reachable; cn_attractive; }
      in
      Some (`Node (n, defs, check_exp stc etc)), List.rev es.err_msgs
    with
      | Exit -> None, List.rev es.err_msgs
      | CircularDefs defs ->
          None, List.rev ((`MError [ None, fun fmt ->
            fprintf fmt "Circular dependencies in definitions: %a"
              (SMap.print SSet.print) defs ]) :: es.err_msgs)

  (** Checks syntactic and typing consistency of the given node, plus local
      variable dependencies, and returns a potentially corrected version plus
      possible error/warning messages.

      Returns [None] in case of unrecoverable error. *)
  let check_node = function
    | `Desc descr -> check_node' descr
    | `Node _ as n -> Some n, []

  (* --- *)

  let declare_cfg_implicits es
      ({ cg_typs; cg_decls; cg_graph } as cfg : 'f cfg_desc) : 'f cfg_desc =

    let (loc_typname, loc_typ_flag), cg_graph =
      match CFG.location_typ' cg_graph with
        | Some ((loc_typname, flag) as x) ->
            begin try
                let flag' = snd (find_typdef loc_typname cg_typs) in
                add_msg es (`MError ([
                  flag, fun fmt -> fprintf fmt
                    "Type@ `%a'@ given@ for@ CFG@ locations@ is@ already@ \
                   defined" ps loc_typname;
                ] @ if flag' = None then [] else [
                  flag', fun fmt -> fprintf fmt "Previous@ declaration"
                ]))
              with Not_found -> ()
            end;
            x, cg_graph
        | None ->
            let x = Symb.fresh' "Location", cg_graph.CFG.cfg_flag in
            x, CFG.{ cg_graph with cfg_loc_typ = Some x }
    in
    let loc_typdef =
      mk_etyp'' ?flag:loc_typ_flag (CFG.all_locations cg_graph) in
    let loc_typ = `Enum loc_typname in
    let (loc_var, loc_var_flag), cg_graph =
      match CFG.location_var' cg_graph with
        | Some ((loc_var, flag) as x) ->
            begin try
                let _, _, flag' = SMap.find loc_var cg_decls in
                add_msg es (`MError ([
                  flag, fun fmt -> fprintf fmt
                    "Variable@ `%a'@ given@ for CFG@ locations@ is@ already@ \
                   defined" ps loc_var;
                ] @ if flag' = None then [] else [
                  flag', fun fmt -> fprintf fmt "Previous@ declaration"
                ]))
              with Not_found -> ()
            end;
            (* XXX: What about name clash with labels? *)
            x, cg_graph
        | None -> let x = Symb.fresh' "loc", cg_graph.CFG.cfg_flag in
                  x, CFG.{ cg_graph with cfg_loc_var = Some x }
    in
    { cfg with
      cg_typs = declare_typ loc_typname loc_typdef cg_typs;
      cg_decls = SMap.add loc_var (loc_typ, `State', loc_var_flag) cg_decls;
      cg_graph }

  let check_cfg' (cfg: 'f cfg_desc) : 'f checked_cfg option * 'f loc_msg list =

    let es = { err_msgs = [] } in

    (* --- *)

    (* Declare (implicit) enumeration type for locations *)
    let { cg_typs; cg_decls; cg_graph; cg_init;
          cg_assertion; cg_invariant; cg_reachable; cg_attractive }
        = declare_cfg_implicits es cfg in

    (* --- *)

    let labels, symbs, ranks = gather_all_symbs_info es cg_typs
      (cg_decls :> ('f, [> 'f var_spec]) decls) in

    let { ltyp; styp; styp_no_inputs; } as stc
        = gen_symtyp_checkers es labels symbs ranks in
    let { ptyp; btyp; } as etc
        = gen_exptyp_checkers es ltyp in

    (* --- *)

    let chkdecl v (tv, spec, fv) = match spec with
      | `Local _ as spec -> chkldef es stc etc v (tv, spec, fv)
      | `Contr _ as spec -> chkcspec es stc etc v (tv, spec, fv)
      | _ -> (tv, spec, fv)
    in
    let chkb st e = snd & btyp st e in
    let chkb_opt st = function | None -> None | Some e -> Some (chkb st e) in

    (* --- *)

    let open CFG in

    let chkupds =
      let chkupd ?flag (tv: typ) e =
        ptyp styp ?flag e |> coercep es "update" ?flag tv |> snd
      in
      SMap.mapi begin fun v (`State (e, flag) as s) ->
        try let tv,_,_ = SMap.find v cg_decls in
            `State (chkupd ?flag tv e, flag)
        with
          | Not_found ->
              err2 es ?flag "Undeclared@ state@ variable@ `%a'" ps v; s
      end
    and chkforms ~inputp ~indir_input_deps =
      let styp_no_inputs = styp_no_inputs ~inputp ~indir_input_deps in
      fun l ->
        { l with
          loc_init      = chkb_opt styp_no_inputs l.loc_init;
          loc_final     = chkb_opt styp_no_inputs l.loc_final;
          loc_assertion = chkb_opt styp l.loc_assertion; }
    in

    let chk_graph ~inputp ~indir_input_deps g =
      let chkforms = chkforms ~inputp ~indir_input_deps in
      repl_locations begin fun ({ loc_name; loc_arcs } as loc) ->
        let arcs = map_arcs
          begin fun ({ arc_dest; arc_guard; arc_updates; arc_flag = flag
                     } as arc) ->
            if not (has_location arc_dest g) then
              err2 es ?flag "Unknown@ location@ `%a'" ps arc_dest;
            { arc with
              arc_guard = chkb_opt styp arc_guard;
              arc_updates = chkupds arc_updates; }
          end loc_arcs in
        chkforms { loc with loc_arcs = arcs }
      end g
    in

    (* --- *)

    try
      let cg_decls      = SMap.mapi chkdecl cg_decls
      and cg_assertion  = chkb styp cg_assertion in
      stop_on_errors es;

      let input_spec = function
        | #input_var_spec | #contr_var_spec -> true
        | _ -> false
      in
      let defs, indir_input_deps =
        sort_defs
          ~input_spec
          ~local_exp:(function | `Local (e, _) -> Some e | _ -> None)
          cg_decls in
      stop_on_errors es;

      let inputp v = try let _, s, _ = SMap.find v cg_decls in input_spec s with
        | Not_found -> false in
      let cg_graph      = chk_graph ~inputp ~indir_input_deps cg_graph in
      let styp = styp_no_inputs ~inputp ~indir_input_deps in  (* ¬inputs for: *)
      let cg_init       = chkb     styp cg_init
      and cg_invariant  = chkb_opt styp cg_invariant
      and cg_reachable  = chkb_opt styp cg_reachable
      and cg_attractive = chkb_opt styp cg_attractive in
      stop_on_errors es;

      let g =
        { cg_typs; cg_decls; cg_graph; cg_init; cg_assertion; cg_invariant;
          cg_reachable; cg_attractive; }
      in
      Some (`Cfg (g, defs, check_exp stc etc)), List.rev es.err_msgs
    with
      | Exit -> None, List.rev es.err_msgs
      | CircularDefs defs ->
          None, List.rev ((`MError [ None, fun fmt ->
            fprintf fmt "Circular dependencies in definitions: %a"
              (SMap.print SSet.print) defs ]) :: es.err_msgs)

  (** Checks syntactic and typing consistency of the given CFG, plus local
      variable dependencies, and returns a potentially corrected version plus
      possible error/warning messages.

      Returns [None] in case of unrecoverable error. *)
  let check_cfg = function
    | `Desc descr -> check_cfg' descr
    | `Cfg _ as n -> Some n, []

  (* --- *)

  let check_io_deps es decls defs deps =
    let gr = Hashtbl.create (SMap.cardinal deps) in

    (* Add all inputs to the set of dependencies for rev_topo_sort to operate as
       expected: *)
    let deps = SMap.fold begin fun v -> function
      | _, `Input g, _ -> Hashtbl.add gr v g; SMap.add v SSet.empty
      | _ -> fun acc -> acc
    end decls deps in

    let rem_deps = rev_topo_iter begin fun v -> match SMap.find v decls with
      | _, `Input g', _ -> ()                                 (* nothing to do *)
      | _, `Local _, _ ->
          let v_deps = SMap.find v deps in
          let g =
            if SSet.is_empty v_deps then 0
            else SSet.fold (fun v' -> min (Hashtbl.find gr v')) v_deps max_int
          in
          Hashtbl.add gr v g
      | _, `Output (g, e, flag), _ ->
          let o_deps = SMap.find v deps in
          let bad_deps = SSet.filter (fun v' -> g < Hashtbl.find gr v') o_deps in
          if not (SSet.is_empty bad_deps) then
            err4 es ?flag "Output@ variable@ `%a'@ (indirectly)@ depends@ on\
                           @ variables@ from@ further@ I/O@ groups@ \
                           (through:@ @[%a@])" ps v SSet.print bad_deps;
          Hashtbl.add gr v g
    end deps in
    assert (SMap.is_empty rem_deps)

  (* --- *)

  let check_func'
      ({ fn_typs; fn_decls; fn_assertion; } as func: 'f func_desc)
      : 'f checked_func option * 'f loc_msg list =

    let es = { err_msgs = [] } in

    (* --- *)

    let labels, symbs, ranks = gather_all_symbs_info es fn_typs
      (fn_decls :> ('f, [> 'f var_spec]) decls) in

    let { ltyp; styp; } as stc
        = gen_symtyp_checkers es labels symbs ranks in
    let { ptyp; btyp; } as etc
        = gen_exptyp_checkers es ltyp in

    (* --- *)

    let chkdecl v (tv, spec, fv) = match spec with
      | `Local _ as spec -> chkldef es stc etc v (tv, spec, fv)
      | `Output _ as spec -> chkodef es stc etc v (tv, spec, fv)
      | _ -> (tv, spec, fv)
    in
    let chkb st e = snd & btyp st e in

    (* --- *)

    try
      let fn_decls      = SMap.mapi chkdecl fn_decls
      and fn_assertion  = chkb styp fn_assertion in
      stop_on_errors es;

      let defs =
        sort_defs'
          ~input_spec:(function
            | #input_var_spec -> true
            | _ -> false)
          ~local_exp:(function
            | `Local (e, _) | `Output (_, e, _) -> Some e
            | _ -> None)
          fn_decls in
      stop_on_errors es;

      let all_deps = gather_all_deps (fn_decls :> ('f, [> 'f var_spec]) decls) in
      check_io_deps es fn_decls defs all_deps;
      stop_on_errors es;

      let f = { func with fn_decls; fn_assertion; } in
      Some (`Func (f, defs, check_exp stc etc)), List.rev es.err_msgs
    with
      | Exit -> None, List.rev es.err_msgs
      | CircularDefs defs ->
          None, List.rev ((`MError [ None, fun fmt ->
            fprintf fmt "Circular dependencies in definitions: %a"
              (SMap.print SSet.print) defs ]) :: es.err_msgs)

  (** Checks syntactic and typing consistency of the given function, plus local
      variable dependencies, and returns a potentially corrected version plus
      possible error/warning messages.

      Returns [None] in case of unrecoverable error. *)
  let check_func = function
    | `Desc descr -> check_func' descr
    | `Func _ as f -> Some f, []

  (* --- *)

  let check_pred'
      ({ pn_typs; pn_decls; pn_value } as pred: 'f pred_desc)
      : 'f checked_pred option * 'f loc_msg list =

    let es = { err_msgs = [] } in

    (* --- *)

    let labels, symbs, ranks = gather_all_symbs_info es pn_typs
      (pn_decls :> ('f, [> 'f var_spec]) decls) in

    let { ltyp; styp; styp_no_inputs; } as stc
        = gen_symtyp_checkers es labels symbs ranks in
    let { ptyp; btyp; } as etc
        = gen_exptyp_checkers es ltyp in

    (* --- *)

    let chkdecl v (tv, spec, fv) = match spec with
      | `Local _ as spec -> chkldef es stc etc v (tv, spec, fv)
      | `Contr _ as spec -> chkcspec es stc etc v (tv, spec, fv)
      | _ -> (tv, spec, fv)
    in
    let chkb st e = snd & btyp st e in

    (* --- *)

    try
      let pn_decls = SMap.mapi chkdecl pn_decls
      and pn_value = chkb styp pn_value in
      stop_on_errors es;

      let input_spec = function
        | #input_var_spec | #contr_var_spec -> true
        | _ -> false
      in
      let defs =
        sort_defs'
          ~input_spec
          ~local_exp:(function | `Local (e, _) -> Some e | _ -> None)
          pn_decls in
      stop_on_errors es;

      let p = { pred with pn_decls; pn_value } in
      Some (`Pred (p, defs, check_exp stc etc)), List.rev es.err_msgs
    with
      | Exit -> None, List.rev es.err_msgs
      | CircularDefs defs ->
          None, List.rev ((`MError [ None, fun fmt ->
            fprintf fmt "Circular dependencies in definitions: %a"
              (SMap.print SSet.print) defs ]) :: es.err_msgs)

  (** Checks syntactic and typing consistency of the given predicate, plus local
      variable dependencies, and returns a potentially corrected version plus
      possible error/warning messages.

      Returns [None] in case of unrecoverable error. *)
  let check_pred = function
    | `Desc descr -> check_pred' descr
    | `Pred _ as p -> Some p, []

  (* ------------------------------------------------------------------------ *)

  (** [split_func f] translates a checked function {i f} into as many functions
      as I/O groups {i f} comprises.

      Each resulting function {i f{_i}} involves one I/O group for which the
      output component consists in the output component of the {i i}th I/O group
      of {i f}; its input component is the union of the input component of the
      {i i}th I/O group and input AND output of all preceding I/O groups (from
      the {i 1}st to the {i i-1}th).

      The resulting functions {i f{_i}}s are sorted according to {i i}. *)
  let split_func (`Func ({ fn_decls } as desc, defs, _): 'f checked_func) =

    let deps = List.fold_left (fun deps (v, e) ->
      let v_deps = hashtbl_exp_dependencies e in
      (* let v_deps =
       *   SSet.fold
       *     (fun v' acc -> try
       *         let v_deps' = SMap.find v' deps in
       *         SSet.fold
       *           (fun v s -> SSet.add v s)
       *           acc v_deps'
       *       with
       *       | Not_found -> acc) vd vd in *)
      SMap.add v v_deps deps)
      SMap.empty
      defs
    in

    let gdecls, locals =
      SMap.fold begin fun v ((t, spec, l) as e) (acc, locals) -> match spec with
        | `Output (g, _, _) -> (imap_add g v e acc, locals)
        | `Input g -> (imap_add g v e acc, locals)
        | `Local _ -> (acc, SMap.add v e locals)
      end fn_decls (IMap.empty, SMap.empty)
    in

    let _, funcs = IMap.fold                         (* increasing group order… *)
      begin fun _g iodecls (base_decls, rev_funcs) ->

        let decls = SMap.merge begin fun v a b -> match a, b with
          | Some (t, `Output _, l), None   (* change outputs of base declarations
                                              into inputs *)
          | None, Some (t, `Input _, l) -> Some (t, `Input one, l)
          | Some (_, `Local _, _), None -> None                (* remove locals *)
          | Some e, None | None, Some e -> Some e              (* copy otherwise *)
          | Some _, Some _ -> failwith "Internal error in `split_func'"
          | None, None -> assert false
        end base_decls iodecls in

        let decls = SMap.fold begin fun v (_, spec, _) acc -> match spec with
          | `Local _ | `Input _ -> acc
          | `Output _ ->
             let new_deptable = ref (SMap.find v deps) in
             let deptable = Hashtbl.create (Hashtbl.length !new_deptable) in
             let current_deptable = ref (Hashtbl.create 9) in
             (* Populating deptable: dependencies closure *)
             while (Hashtbl.length !new_deptable) > 0 do
               Hashtbl.iter
                 (fun v () ->
                   if not (Hashtbl.mem deptable v) then
                     begin
                       Hashtbl.add deptable v ();
                       (* Add each new dependency of v in new_deptable *)
                       try
                         let hdeps = SMap.find v deps in
                         Hashtbl.iter
                           (fun v' () ->
                             if not (Hashtbl.mem deptable v') then
                               Hashtbl.add !current_deptable v' ())
                           hdeps
                       with Not_found -> ()
                     end)
                 !new_deptable;
               let aux = !current_deptable in
               current_deptable := !new_deptable;
               new_deptable := aux;
               Hashtbl.clear !current_deptable
             done;
             Hashtbl.fold begin fun v_dep () acc ->
               try match SMap.find v_dep locals with
                   | _, `Local _, _ as e -> SMap.add v_dep e acc
                   | _ -> acc
               with
               | Not_found -> acc
               end deptable acc
        end iodecls decls in

        (* match check_func { desc with fn_decls = decls } with *)
        (*   | Some fn, [] -> (decls, fn :: rev_funcs) *)
        (*   | _ -> failwith "Internal error in `split_func' (2)" *)

        decls, `Desc { desc with fn_decls = decls } :: rev_funcs

      end gdecls (SMap.empty, [])
    in

    List.rev funcs

end

(* -------------------------------------------------------------------------- *)

include Make (Symb)
