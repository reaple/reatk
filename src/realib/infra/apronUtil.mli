(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)

(** utilities for APRON *)

type 'a man_t = 'a Apron.Manager.t                  (** APRON manager *)
type env_t = Apron.Environment.t                  (** APRON environment *)
type lincons0 = Apron.Lincons0.t
type linexpr0 = Apron.Linexpr0.t
type linexpr1 = Apron.Linexpr1.t                  (** APRON linear expression *)
type lincons1 = Apron.Lincons1.t                  (** APRON linear constraint *)

(** conjunction of APRON linear constraints *)
type linconss = Apron.Lincons1.earray

type var_t = Apron.Var.t (** APRON variable *)
type vars_t = Apron.Var.t array (** APRON variables *)
type equ_t = var_t * linexpr1 (** APRON equation *)
type equs_t = equ_t array (** APRON equations *)

type 'a num0 = 'a Apron.Abstract0.t
type 'a num1 = 'a Apron.Abstract1.t

(** {2 Printing} *)

(** prints an APRON abstract value *)
val print_abstract : Format.formatter -> 'a num1 -> unit

(** prints an APRON equation *)
val print_equation : Format.formatter -> equ_t -> unit

(** prints APRON equations *)
val print_equations : Format.formatter -> equs_t -> unit

(** prints APRON variables *)
val print_vars : Format.formatter -> vars_t -> unit

(** prints a conjunction of APRON constraints *)
val print_linconss : Format.formatter -> linconss -> unit

val pp_cmp: Format.formatter -> Apron.Lincons0.typ -> unit
val pp_revcmp: Format.formatter -> Apron.Lincons0.typ -> unit
val pp_lincons : (Apron.Dim.t -> string) -> Format.formatter -> lincons0 -> unit
val pp_num0 : (Apron.Dim.t -> string) -> Format.formatter -> 'a num0 -> unit

(** {2 APRON Helpers} *)

(** primes a variable *)
val get_primed_var : var_t -> var_t

(** builds the conjunction of constraints  v'=e from the given equations *)
val get_fexpr : (var_t -> var_t) -> env_t -> equs_t -> linconss

(** gets the array of variables in the APRON environment *)
val vars_of_env : env_t -> vars_t

(** returns the sub-environment containing the given variables *)
val env_of_vars : env_t -> vars_t -> env_t

(** returns the number of dimensions of env *)
val dims_of_env : env_t -> int

(** checks whether the expression depends on at least one of the given
    variables *)
val linexpr_depends_on_some_var : linexpr1 -> vars_t -> bool

(** creates the linexpr 0 *)
val make_zero_linexpr : env_t -> linexpr1

(** creates equations (v,0) for the given variables *)
val get_zero_equations_for : env_t -> vars_t -> equs_t

(** creates the constraints (v=0) for the given variables *)
val get_zero_linconss_for : env_t -> vars_t -> linconss

(** APRON abstract bottom *)
val bottom0 : 'a man_t -> env_t -> 'a num0
val top0 : 'a man_t -> env_t -> 'a num0

val num0_of_num1: 'a num1 -> 'a num0
val abstract1_of_abstract0 : env_t -> 'a num0 -> 'a num1

val lincons1_of_lincons0 : env_t -> lincons0 -> lincons1

val linconss_of_num0: env_t -> 'a num0 -> linconss
val linconss_of_num1: 'a num1 -> linconss
val fold_lincons0_of_num0: (lincons0 -> 'a -> 'a) -> 'b num0 -> 'a -> 'a

(** checks two conjunctions of constraints for syntactical equality
    (without permutations) *)
val linconss_is_eq : linconss -> linconss -> bool

(** returns an empty conjunction of constraints *)
val linconss_empty : env_t -> linconss

(** rename primed variables to unprimed variables in the abstract value *)
val rename_primed_to_unprimed : (var_t -> var_t) ->'a num1 -> vars_t -> 'a num1

(** {2 Abstract domain operations} *)

(** generalized time elapse operator *)
val elapse : 'a num1 -> 'a num1 -> 'a num1

(** generalized time elapse in the domain of d,
    the domain of s may be different *)
val elapse_with_domain : 'b num1 -> 'a num1 -> 'b num1

(** changes the domain of an abstract value *)
val change_domain : 'a num1 -> 'b man_t -> 'b num1

(** projects out the given variables from s, but keeps their dimensions *)
val project_forget : 'a num1 -> vars_t -> 'a num1

(** projects the zero_vars of s onto the hyperplane where zero_vars=0 *)
val project_to_zero : 'a num1 -> vars_t -> 'a num1

(** negation of an abstract value *)
val neg : 'a num1 -> 'a num1

(** returns the translator d in the domain man *)
val get_translator : 'a man_t -> equs_t -> linconss -> vars_t -> 'a num1

val abstract_lincons_list: 'a man_t -> lincons1 list -> 'a num1 list
