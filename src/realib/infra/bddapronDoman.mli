(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)

(** BddApron/APRON domain managers *)

(** {2 Interface of Bddapron domain managers } *)

(** BddApron domain manager interface *)
module type T = sig
  module N: ApronDoman.T
  type aprondom = N.dom
  type 'a bddapronman
  type ('a, 'b) t = ('a, aprondom, 'a bddapronman, 'b) Bddapron.Domain0.man

  val make: unit -> ('a, 'b) t * aprondom Apron.Manager.t

  (** prints an informal description *)
  val descr: Format.formatter -> unit
end

(** {2 Logico-numerical domain managers for convex polyhedra } *)

(** domain manager for convex polyhedra without strict inequalities *)
module LoosePol: T

(** domain manager for convex polyhedra without strict inequalities (MTBDD-based implementation) *)
module LoosePolMt: T

(** domain manager for convex polyhedra with strict inequalities *)
module StrictPol: T

(** domain manager for convex polyhedra with strict inequalities (MTBDD-based implementation) *)
module StrictPolMt: T


(** {2 Logico-numerical domain managers for octagons } *)

(** domain manager for octagons *)
module Oct: T

(** domain manager for octagons (MTBDD-based implementation) *)
module OctMt: T


(** {2 Logico-numerical domain managers for intervals (boxes) } *)

(** domain manager for intervals *)
module Box: T

(** domain manager for intervals (MTBDD-based implementation) *)
module BoxMt: T
