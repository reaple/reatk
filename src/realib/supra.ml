(** Main entry point of Realib library. *)

let level = Log.Info
let logger = Log.mk ~level "Supra"

(* -------------------------------------------------------------------------- *)
(* {2 Printing & Logging} *)

(** Logs some summarized data about the given environment. *)
let log_env_info ?cs ({ Env.env = e; Env.cond = c } as env) =
  let bs_vars = Env.number_of_boolvars env env.Env.bs_vars in
  let ns_vars = List.length env.Env.ns_vars in
  let bi_vars = Env.number_of_boolvars env env.Env.bi_vars in
  let ni_vars = List.length env.Env.ni_vars in
  Log.i logger "Variables(bool/num): @[state=(%d/%d),@ i=(%d/%d)%a@]"
    bs_vars ns_vars bi_vars ni_vars
    (fun fmt -> function
      | None -> ()
      | Some cs -> CtrlSpec.print_nc_vars_info env fmt cs)
    cs;
  Log.d3 logger "@[<v>Env: %a@ Conds: %a@ env-order: %a@ careset = %a@ \
                   bool_size = %d@]"
    Bddapron.Env.print e
    (Bddapron.Cond.print e) c
    (fun f e -> Bddapron.Env.print_order e f) e
    (BddapronUtil.print_boolexpr e c) c.Bdd.Cond.careset
    env.Env.bool_size

(** Logs some summarized data about the given data-flow program. *)
let log_dfprog_info ({ Env.env = e; Env.cond = c }) dfprog =
  Log.d2 logger "@[Program infos:@.init = %a@\nerr = %a@\nassert = %a@\n\
                 reach = %a@\nattract = %a@\nequs: @[<v>%a@]@]"
    (BddapronUtil.print_boolexpr e c) dfprog.Program.d_init
    (BddapronUtil.print_boolexpr e c) dfprog.Program.d_final
    (BddapronUtil.print_boolexpr e c) dfprog.Program.d_ass
    (BddapronUtil.print_boolexpr e c) dfprog.Program.d_reach
    (BddapronUtil.print_boolexpr e c) dfprog.Program.d_attract
    (BddapronUtil.print_equations e c) dfprog.Program.d_disc_equs

(** Logs some summarized data about the given data-flow program. *)
let log_cfprog_info ({ Env.env = e; Env.cond = c } as env) cfprog =
  Log.d2 logger "@[Program infos:@.CFG: %a@\ninit = %a@\nerr = %a@\nassert = %a@\n\
                 reach = %a@\nattract = %a@\nequs: @[<v>%a@]@]"
    (Cfg.print_short env) cfprog.Program.c_cfg
    (BddapronUtil.print_boolexpr e c) cfprog.Program.c_init
    (BddapronUtil.print_boolexpr e c) cfprog.Program.c_final
    (BddapronUtil.print_boolexpr e c) cfprog.Program.c_ass
    (BddapronUtil.print_boolexpr e c) cfprog.Program.c_reach
    (BddapronUtil.print_boolexpr e c) cfprog.Program.c_attract
    (BddapronUtil.print_equations e c) cfprog.Program.c_disc_equs


(** Dumps a control-flow graph in DOT format, unless [dotfile = ""]. [dotfile =
    "-"] means standard output. *)
let output_dot_file ~dotfile ~arcs env cf =
  let gen out =
    let dotfmt = Format.formatter_of_out_channel out in
    Cfg.print_dot env dotfmt cf.Program.c_cfg arcs
  in
  match dotfile with
    | "" -> ()
    | "-" -> gen stdout
    | f -> let o = open_out f in gen o; close_out o

(* -------------------------------------------------------------------------- *)

(** Translates the given data-flow program into a control-flow graph using the
    specified method. *)
let df2cf meth env df = Df2cfEngine.run env df meth

(** Translates the given control-flow graph *back* into a data-flow program *)
let cf2df _env = Program.cfprog_as_dfprog

(* -------------------------------------------------------------------------- *)

(** Internal representation of verification strategy specification *)
type verif_spec = VerifEngine.spec

(** [parse_verif env spec] parses the verification strategy specification [spec]
    w.r.t the environment [env]. *)
let parse_verif = VerifEngine.str_to_strategy

(** Result of the {!verif} function *)
type verif_res = Analysis.result

(** [verif spec env cf] executes verification strategy [spec] on control flow
    graph [cf]. *)
let verif spec env cf = let res, _, _ = VerifEngine.run env spec cf in res

(* -------------------------------------------------------------------------- *)

(** Internal representation of synthesis algorithm specification *)
type synth_spec = SynthEngine.spec

(** [parse_synth env spec] parses the synthesis algorithm specification [spec]
    w.r.t the environment [env]. *)
let parse_synth = SynthEngine.str_to_algo

(** Main input of the {!synth} function *)
type synth_inp = CtrlSpec.t * Env.boolexpr_t * Program.cfprog_t

(** Result of the {!synth} function *)
type synth_res = CtrlSpec.t * Synthesis.controller * Program.cfprog_t

(** Raised by {!synth} in case of failure *)
exception SynthesisFailure of CtrlSpec.t * Env.t * Program.cfprog_t

(** [synth spec env (cs, assertion, cf)] executes the synthesis algorithm
    specified by [spec] on the control-flow program [cf]. *)
let synth spec env (cs, assertion, cf) =
  match SynthEngine.run env (cs, assertion, cf) spec with
    | cs, Some res, cf -> cs, res, cf
    | cs, None, cf -> raise (SynthesisFailure (cs, env, cf))

(** Result of the {!coreach} function *)
type coreach_res = CtrlSpec.t * Env.boolexpr_t * Program.cfprog_t

(** [coreach spec env (cs, assertion, cf)] executes the synthesis algorithm
    specified by [spec] on the control-flow program [cf]. *)
let coreach spec env (cs, assertion, cf) =
  SynthEngine.run_coreach_only env (cs, assertion, cf) spec

(* -------------------------------------------------------------------------- *)

(** Internal representation of the optimization procedure specification *)
type optim_spec = OptimEngine.spec

(** [parse_optim env spec] parses the optimization specification [spec] w.r.t
    the environment [env]. *)
let parse_optim = OptimEngine.str_to_specs

(** Result of the {!optim} function *)
type optim_res = CtrlSpec.t * Synthesis.controller * Program.cfprog_t

(** [optim spec cs env df k] executes the optimization steps specified by [spec]
    on the controller [k] and w.r.t the dataflow program [df] and control
    specifications [cs]. The environment [env] must be compatible (NB: for now
    physically identical) with the one used when building [spec]. *)
let optim spec env res = OptimEngine.run env res spec

(* -------------------------------------------------------------------------- *)

(** Result of the {!triang} function *)
type triang_res = Env.equs_t list * Env.boolexpr_t

(** [triang ?allow_careset_updates ?enable_reorderings cs env ctrlr]
    triangulates the controller [ctrlr] based on the controllable program
    specification [cs], and returns a causally ordered sequence of equations
    defining values of the controllable inputs.

    Optional argument [allow_careset_updates] defaults to [true].  Optional
    argument [enable_reorderings] defaults to [false] (enable it for big
    systems, or to obtain smaller controllers --- maybe). *)
let triang
    ?allow_careset_updates
    ?enable_reorderings
    ?enable_dynamic_reorderings
    cs env res =
  let p = Triang.make_param ?allow_careset_updates ?enable_reorderings
    ?enable_dynamic_reorderings () in
  Triang.run p env cs res

(* -------------------------------------------------------------------------- *)

(** Result of the {!merge} function *)
type merge_res = Env.t * CtrlSpec.t * Program.dfprog_t

(** [merge cs df env equs] merges the triangularized controller [equs] into the
    data-flow program [df]. *)
let merge
    ?allow_careset_updates
    ?enable_reorderings
    ?enable_dynamic_reorderings
    ?discard_controllables
    cs cf env equs =
  let p = Merge.make_param ?allow_careset_updates ?enable_reorderings
    ?enable_dynamic_reorderings ?discard_controllables () in
  let module E = (val Env.as_module env) in
  let module M = Merge.Basic (E) in
  M.run p cs cf equs

(* -------------------------------------------------------------------------- *)
