opam-version: "1.2"
maintainer: "Nicolas Berthier <m@nberth.space>"
authors: ["Nicolas Berthier" "Peter Schrammel"]
license: "GPL-3"
homepage: "http://nberth.space/reatk"
dev-repo: "https://framagit.org/reaple/reatk"
bug-reports: "https://framagit.org/reaple/reatk/-/issues"
build: [
  ["./configure" "--prefix" prefix "--docdir" doc
		 "--opam-pkgdev-dir" "%{opam-pkgdev:share}%"
		 "--disable-annots"]
  [make "JOBS=%{jobs}%"]
]
install: [
  [make "install-opam"]
]
remove: [
  ["ocamlfind" "remove" "reatk"]
  ["rm" "-r" "-f" "%{reatk:doc}%"]
]
tags: [ "flags:light-uninstall" ]
depends: [
  "base-unix"
  "base-bigarray"
  "num"
  "ocamlfind"    {build}
  "ocamlbuild"   {build}
  "stdlib-shims"
  "menhir"       {build}
  "mlgmpidl"     {>= "1.2.11"}
  "opam-pkgdev"  {build & >= "0.7"}
]
depopts: [ "camllib" "fixpoint" "bddapron" "ocamlgraph" ]
conflicts: [
  "camllib"    {< "1.3.0"}
  "fixpoint"   {< "1.2.0"}
  "mlcuddidl"  {< "3.0.6"}
  "apron"      {< "0.9.11"}
  "bddapron"   {< "2.99.8"}
# "ocamlgraph" {< "1.8"}
]
available: [ ocaml-version >= "4.03.0" ]
messages: [
  "Missing package camllib."  { !camllib:installed }
  "Missing package fixpoint." { !fixpoint:installed }
  "Missing package bddapron." { !bddapron:installed }
  "Missing package ocamlgraph." { !ocamlgraph:installed }
  "Due to the above missing package(s), the ReaX and ReaVer tools won't be
available, nor will be the reatk.realib, reatk.rutils, reatk.cn2rl, and
reatk.rl2cn libraries. Install it/them to have the complete toolkit."
    { !camllib:installed | !fixpoint:installed
    | !bddapron:installed | !ocamlgraph:installed }
]
post-messages: [
  "Due to some missing optional package(s), the ReaX and ReaVer tools may
not be available, nor may the reatk.realib, reatk.rutils, reatk.cn2rl, and
reatk.rl2cn libraries.

Run `opam install camllib fixpoint bddapron` to be sure to have the
complete toolkit."
    { !camllib:installed | !fixpoint:installed
    | !bddapron:installed | !ocamlgraph:installed }
]
