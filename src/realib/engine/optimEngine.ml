(******************************************************************************)
(* optim *)
(* optimization engine (basic) *)
(* author: Nicolas Berthier *)
(* This file is part of ReaTK released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)
(******************************************************************************)

open Format
open ParseParams

(** ReaX optimization engine *)

let (&) f g = f g
let logger = Log.mk "Optim"

exception InvalidSpec of (formatter -> unit)
exception InvalidGoal of (formatter -> unit)

(* -------------------------------------------------------------------------- *)

type descr = formatter -> unit

type optstep =
  | OneStepOptim of Optim.one_step_param
  | KStepOptim of Optim.k_step_param
  | DiscountedOptim of Optim.d_param

type algo = optstep list

type spec = Env.t * algo

let opts =          (* identifier * short description * long description list *)
  [
    ("o1",("1-step optimization", "basic 1-step optimization algorithm"));
    ("ok",("k-step optimization", "k-step optimization algorithm"));
    ("od",("discounted optimization", "discounted optimization algorithm"));
  ]

(* let pp_short_optdesc s fmt = pp_print_string fmt (fst & List.assoc s opts) *)

let print_opts fmt () =
  List.iter
    (fun (s,(_,longdesc)) ->
      fprintf fmt "@[ ";
      Util.print_fixed fmt 5 s;
      fprintf fmt "@[<hov>";
      Util.print_breakable fmt longdesc;
      fprintf fmt "@]@]@.")
    opts

(******************************************************************************)
(* parsing of synthesis algo options *)
(******************************************************************************)

let invalid_spec msg = raise @@ InvalidSpec (fun fmt -> fprintf fmt msg)
let invalid_spec1 msg o = raise @@ InvalidSpec (fun fmt -> fprintf fmt msg o)
let invalid_spec2 msg o p = raise @@ InvalidSpec (fun fmt -> fprintf fmt msg o p)
let invalid_spec3 msg o p q = raise @@ InvalidSpec (fun fmt -> fprintf fmt msg o p q)
let invalid_goal msg = raise @@ InvalidGoal (fun fmt -> fprintf fmt msg)
let invalid_goal1 msg o = raise @@ InvalidGoal (fun fmt -> fprintf fmt msg o)
let invalid_goal2 msg o p = raise @@ InvalidGoal (fun fmt -> fprintf fmt msg o p)

let check_state_var (env, _) v =
  if not (List.mem v env.Env.s_vars) then
    invalid_goal2 "only@ state@ variables@ are@ expected@ (got@ `%a')\
                    " Env.Symb.print v

let check_non_ctlr_var loc_msg s (_, cs) =
  let c_vars = CtrlSpec.c_vars cs in
  fun v ->
    if List.mem v c_vars then
      invalid_spec3 ("controllable@ input@ variables@ are@ forbidden@ "
                     ^^loc_msg^^"@ (got@ `%a')") s Env.Symb.print v

let numexpr_support (Env.{ env; cond }, _) x =
  Bddapron.Expr0.support env cond (Bddapron.Expr0.Apron.to_expr x)

let check_numexpr_state_vars e x =
  numexpr_support e x |> PSette.iter (check_state_var e); x
let check_numexpr_non_ctrl_vars loc_msg s e x =
  numexpr_support e x |> PSette.iter (check_non_ctlr_var loc_msg s e); x

(* --- *)

let mk_opt_goal v = function
  | "min" -> `Minimize v
  | "max" -> `Maximize v
  | rel -> invalid_goal1 "unknown@ goal@ `%s'" rel

let mk_numexpr_var
    ?input_expr
    ?(state_only=true)
    (Env.{ env = e; cond = c } as e', _ as env)
    str
    =
  let v = Env.Symb.mk str in
  begin try Bddapron.Expr0.Apron.var e c v with
    | (Not_found | Failure _) as e -> match input_expr with
        | None -> raise e
        | Some f -> try Bddapron.Expr0.Apron.of_expr (f e' str) with
            | Not_found -> invalid_goal1 "unknown@ `%s'" str
  end
  |> (fun e -> if state_only then check_numexpr_state_vars env e else e)

let mk_numexpr_opt_goal ?input_expr ?state_only env v rel =
  mk_opt_goal (mk_numexpr_var ?input_expr ?state_only env v, Some v) rel

(* --- *)

open Scanf

let try_parse name str fmts =
  let rec try_parse' = function
    | [] -> None
    | f :: tl -> try f str with End_of_file | Scan_failure _ -> try_parse' tl
  in
  match try_parse' fmts with
    | Some s -> s
    | None -> invalid_goal2 "syntax@ error@ in@ %(%)@ `%s'" name str

let mkp f cstr str = Some (sscanf str f cstr)

(* --- *)

let parse_o1_spec ?input_expr env str =
  let pair rel v = rel, (* Env.Symb.mk  *)v in
  let rel, v = try_parse "1-step@ goal@ specification" str [
    mkp " %[^( ] ( %[^) ] ) " pair;
    mkp " %[^ ] %[^ ] " pair;
  ] in
  mk_numexpr_opt_goal ?input_expr env v rel

let parse_o1 ?input_expr env (specs, _opts) =
  let goals = List.map (parse_o1_spec ?input_expr env) specs in
  OneStepOptim (Optim.make_o1_param goals)

(* --- *)

let parse_ok_spec ?input_expr env str =
  let quad rel integr v k = rel, integr, v, k in
  let rel, integr, v, k = try_parse "k-step@ goal@ specification" str [
    mkp " %[^( ] ( %[.+S] %[^%@ ] %@ %u ) " quad;
    mkp " %[^ ] %[.+S] %[^%@ ] %@ %u " quad;
  ] in
  let g = mk_numexpr_opt_goal ?input_expr env v rel in
  let integr = match integr with
    | "+" | "S" -> `Sum
    | "." | "" -> `Kth
    | s -> invalid_goal1 "unknown@ integration@ specification@ `%s'" s
  in
  Optim.make_ok_goal g k integr

let parse_ok ?input_expr env =
  let defaults = Util.list2mappe [
    ("pessimistic","false");
    ("be","false"); ("best-effort","false");
    ("dirty","false");
  ] in function
    | [], _ -> invalid_spec "missing@ optimization@ goal@ specification"
    | _::_::_, _ -> invalid_spec "too@ many@ optimization@ goals"
    | [g], opts ->
        let goal = parse_ok_spec ?input_expr env g in
        check_extra_options logger "ok" defaults opts;
        let best_effort = (get_option opts defaults "best-effort" bool_option
                           || get_option opts defaults "be" bool_option) in
        let variant =
          if get_option opts defaults "pessimistic" bool_option
          then `Pessimistic else `Strict
        in
        let dirty = get_option opts defaults "dirty" bool_option in
        KStepOptim (Optim.make_ok_param ~dirty ~best_effort variant goal)

let parse_od_spec ?input_expr env str =
  let pair rel v = rel, v in
  let rel, v = try_parse "discounted@ optimization@ goal@ specification" str [
    mkp " %[^( ] ( %[^) ] ) " pair;
    mkp " %[^ ] %[^ ] " pair;
  ] in
  mk_numexpr_opt_goal ?input_expr ~state_only:false env v rel

let rp_defaults = SynthEngine.rp_defaults @ [("stop_dri","0")]
let str_to_rp ?(defaults = rp_defaults) = SynthEngine.str_to_rp ~defaults

(* --- *)

let parse_samples_spec ?input_expr env s =
  let e = mk_numexpr_var ?input_expr ~state_only:false env s in
  `Samples (check_numexpr_non_ctrl_vars
              "in@ samples@ specification@ expression@ %s" s env e)

let make_stoch_vars ((_, cs) (* as env *)) (* ~spec *) s =
  let empty_vset = PSette.empty Env.Symb.compare in
  let u_vars = CtrlSpec.u_vars cs |> Helpers.list2psette Env.Symb.compare in
  (* let specsupp = match spec with *)
  (*   | `Samples e -> numexpr_support env e |> PSette.inter u_vars *)
  (* in *)
  let pl =
    let reg r =
      let r = Str.regexp r in
      PSette.filter (fun v -> Str.string_match r (Env.Symb.to_string v) 0) u_vars
    and var v =
      let v = Env.Symb.mk v in
      if PSette.mem v u_vars then PSette.add v empty_vset else
        invalid_spec2 "unexpected@ variable@ `%a'@ for@ stochastic@ input\
                      " Env.Symb.print v
    in [
      mkp "reg : %s%!" reg;
      mkp "r : %s%!" reg;
      mkp "%[a-zA-Z0-9]%!" var;
    ]
  in
  let gather_uvar acc s =
    try_parse "specification@ of@ stochastic@ variables" s pl
    |> PSette.union acc
  in
  match comma_str_to_strlist s with
    | [] | [""] -> u_vars
    | lst -> List.fold_left gather_uvar empty_vset lst

let make_ustoch_samples ?input_expr env =
  let default_expr = "1" in
  let defaults = Util.list2mappe [
    ("vars", "");                                 (* <- equiv support of spec *)
  ] in
  let make expr opts =
    check_extra_options logger "uprob" defaults opts;
    let up_spec = parse_samples_spec ?input_expr env expr in
    let make_up_inputs = make_stoch_vars env (* ~spec:up_spec *) in
    let up_inputs = get_option opts defaults "vars" make_up_inputs in
    Optim.{ up_inputs; up_spec }
  in
  function
    | [expr], opts -> make expr opts
    | [], opts -> make default_expr opts
    | _ -> invalid_spec "too@ many@ specification@ expressions@ for@ samples"

let make_ustoch ?input_expr env = function
  | ("samples" | "smpts" | "s"), opts
    -> make_ustoch_samples ?input_expr env opts
  | s, _
    -> invalid_spec1 "invalid@ specification@ `%s'@ for@ `ustoch'" s

let str_to_ustoch ?input_expr env s =
  try make_ustoch ?input_expr env (parse_param' s) with
    | Parsing.Parse_error
    | Parse.Lex_error
    | Exit -> raise (SynthEngine.InvalidOpts
                      (Util.pp1 "syntax@ error@ in@ `%s'" s))

let parse_od ?input_expr env =
  let defaults = Util.list2mappe [
    ("d", "1");
    ("dirty","false");
    ("approx", "false");
    ("rp", "stop_dri=0");
  ] and optionals = [
    "dthr"; "kmax"; "ustoch";
  ] in
  let str_to_ustoch = str_to_ustoch ?input_expr env in
  function
    | [], _ -> invalid_spec "missing@ optimization@ goal@ specification"
    | _::_::_, _ -> invalid_spec "too@ many@ optimization@ goals"
    | [g], opts ->
        let goal = parse_od_spec ?input_expr env g in
        check_extra_options logger "od" defaults ~optionals opts;
        let dirty = get_option opts defaults "dirty" bool_option in
        let d = get_option opts defaults "d" rational_option in
        let ustoch = get_option' opts "ustoch" str_to_ustoch in
        let dthr = get_option' opts "dthr" rational_option in
        let kmax = get_option' opts "kmax" int_of_string in
        let approx = get_option opts defaults "approx" bool_option in
        let rp = get_option opts defaults "rp" str_to_rp in
        let pars = Optim.make_od_param ~dirty ~approx ?dthr ?kmax
          ~rp ?ustoch d goal
        in
        DiscountedOptim (pars)

(******************************************************************************)
(* parses an optimization specification from a string *)
(******************************************************************************)

(* add new optimizations here *)
let str_to_specs ?input_expr env cs sstr =
  let sl = semicolon_str_to_strlist sstr in
  if sl = [] then
    invalid_spec "empty@ list";
  env, List.map
    begin fun s ->
      try match parse_param' s with
        | ("o1", options) -> parse_o1 ?input_expr (env, cs) options
        | (("ok"|"oL"), options) -> parse_ok ?input_expr (env, cs) options
        | ("od", options) -> parse_od ?input_expr (env, cs) options
        | (s, _) -> invalid_spec1 "unknown@ item@ `%s'" s
      with
        | Exit
        | Parse.Lex_error
        | Parsing.Parse_error -> invalid_spec1 "syntax@ error@ in@ `%s'" s
    end
    sl

(******************************************************************************)
(* run the optimization *)
(******************************************************************************)

let mk_optim env u step =
  let module E = (val Env.as_module env) in
  match step with
    | OneStepOptim params ->
        let module O = Optim.OneStep (E) in
        let params = O.upgrade_param params u in
        (O.optimize params, O.descr params, O.short_descr params)
    | KStepOptim params ->
        let module O = Optim.KStep (E) in
        let params = O.upgrade_param params u in
        (O.optimize params, O.descr params, O.short_descr params)
    | DiscountedOptim params ->
        let module O = Optim.Discounted (E) in
        let params = O.upgrade_param params u in
        (O.optimize params, O.descr params, O.short_descr params)

(* --- *)

(* run the optimization steps *)
let run env x ((prev_env, specs): spec) =
  (* For now, use physical equality to check that the environment is the same…
     We shall upgrade expressions later. *)
  assert (prev_env == env);
  let rec run' env_perm x = function
    | [] -> x
    | step :: rem_steps ->
        let spec, descr, short_descr = mk_optim env env_perm step in
        Log.i logger "@[%t@]:" descr;
        let res, env_perm' = spec x in
        Log.i logger "@[%t@ successfully@ completed.@]" short_descr;
        run' (Bdd.Idx.Map.compose' env_perm env_perm') res rem_steps
  in
  run' None x specs

(* -------------------------------------------------------------------------- *)
