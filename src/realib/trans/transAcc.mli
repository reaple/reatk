(** transformation implementation: transformations for abstract acceleration *)

(** decoupling mode: no decoupling, decoupling of Boolean and
    accelerable equations, decoupling of Boolean/non-accelerable and
    accelerable equations *)
type decoupling_mode_t = DecoupleNone | DecoupleBool | DecoupleBoolNacc

(** remove Boolean inputs and classify accelerable self-loops *)
val bool_inputs_to_guards : decoupling_mode_t -> Ops.trans

(** split accelerable self-loops into convex guards *)
val split_arcs : Ops.trans

(** decouple accelerable self-loops *)
val decouple_equs : Ops.trans

(** flatten (resp. expand multiple) accelerable self-loops *)
val flatten_loops : Ops.trans

(** inputize Boolean decoupled self-loops *)
val inputize : Ops.trans
