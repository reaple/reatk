open Format

(* -------------------------------------------------------------------------- *)

module type S = (* Identifiable.Thing *) sig
  include Map.OrderedType
  include Hashtbl.HashedType with type t := t
  val output : out_channel -> t -> unit
  val print : Format.formatter -> t -> unit
end

(* -------------------------------------------------------------------------- *)

type ufmt = (unit, Format.formatter, unit) format

module type SET = sig
  include Set.S
  val print': ?l:ufmt -> ?r:ufmt -> ?s:ufmt -> Format.formatter -> t -> unit
  val print: Format.formatter -> t -> unit
end

module type MAP = sig
  (** Include standard library signature for maps. *)
  include Map.S

  (** [print output_elem fmt map] writes a representation of the content of
      [map] on the formatter [fmt]. Values are output by using the [output_elem]
      procedure. *)
  val print': ?l:ufmt -> ?r:ufmt -> ?s:ufmt -> ?m:ufmt
    -> (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
  val print: (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit

  module Set: SET with type elt = key
  (** [keys m] retrives the set of keys of map [m]. *)
  val keys: 'a t -> Set.t
end

(* -------------------------------------------------------------------------- *)

(** {2 Some generic data-structures} *)
module type T = sig
  type t
  (** These containers essencialy add dedicated pretty-printing facilities to
      modules of the standard library. *)
  module Set: SET with type elt = t
  module Map: MAP with type key = t and module Set = Set
end

(* -------------------------------------------------------------------------- *)

module Make (M: S) = (struct
  open M
  type t = M.t

  (* --- *)

  module Set: SET with type elt = t = struct

    include Set.Make (M)

    let print' ?(l: ufmt = "{") ?(r: ufmt = "}") ?(s: ufmt = ",@ ") fmt set =
      try let max = max_elt set in
          fprintf fmt "@[%(%)@[%a@]%(%)@]" l
            (fun fmt -> iter (fun e -> print fmt e;
              if M.compare max e <> 0 then fprintf fmt s)) set
            r
      with
        | Not_found -> fprintf fmt "%(%)%(%)" l r

    let print fmt set = print' fmt set

  end

  (* --- *)

  module Map: MAP with type key = t and module Set = Set = struct
    include Map.Make (M)
    let print'
        ?(l: ufmt = "{") ?(r: ufmt = "}") ?(s: ufmt = ",@ ") ?(m: ufmt = " => ")
        pp_elem fmt map =
      try let max, _ = max_binding map in
          fprintf fmt "@[%(%)@[%a@]%(%)@]" l
            (fun fmt -> iter (fun k v ->
              fprintf fmt "%a%(%)%a" print k m pp_elem v;
              if M.compare max k <> 0 then fprintf fmt s)) map
            r
      with
        | Not_found -> fprintf fmt "%(%)%(%)" l r

    let print pp_elem fmt map = print' pp_elem fmt map

    module Set = Set
    let keys map = fold (fun v _ -> Set.add v) map Set.empty
  end

end: T with type t = M.t)

(* -------------------------------------------------------------------------- *)
