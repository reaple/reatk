(******************************************************************************)
(* verif *)
(* verification strategy engine *)
(* author: Peter Schrammel *)
(* version: 0.9.0 *)
(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)
(******************************************************************************)

open Format
open ParseParams

let logger = Log.mk "Verif"

exception InvalidStrategy of (formatter -> unit)
exception InvalidStrategyOptionValue of string * string
exception InvalidDomain of string
exception InvalidDomainOptions of string

(* -------------------------------------------------------------------------- *)

type numdom_spec =
  | Intervals
  | Octagons
  | ConvexPol of bool
type boolcomp_dd =
  | Mtbdd
  | Bdd
type boolcomp_method =
  | Prod
  | Pow
type boolcomp_spec = boolcomp_method * boolcomp_dd * numdom_spec
type domcomp =
  | Direct
  | Pwext of pwext_spec
and pwext_spec =
    {
      bdisj: bool;
      norelax: bool;
      uqbdd: bool;
    }
type dom_spec = domcomp * boolcomp_spec

(* -------------------------------------------------------------------------- *)

type trans_spec =
  | PartInitFinal
  | PartEnum of Env.vars_t * string
  | PartManual of Env.boolexpr_t list
  | PartDiscreteModesBool of partition_modes_bool_options
  | PartHybridModesBool of partition_modes_bool_options
  | PartHybridModesNumerical of AnalysisStd.hyb_param * dom_spec
  | PartHybridConvexStayConditions
  | PartBoolBackwardBisimulation
  | TransRelationalAbstractions
  | RefineTransFuncToArcs
  | RefineSplitNonConvexGuards
  | MoveBoolInputsToGuards
  | MoveBoolInputsToGuardsAcc of TransAcc.decoupling_mode_t
  | RefineSplitNonConvexGuardsInAccSelfLoops
  | RefineFlattenAccSelfLoops
  | RefineDecoupleAccSelfLoops
  | RefineInputizationDecoupledSelfLoops
and partition_modes_bool_options = Env.vars_t * partition_modes_ignore
and partition_modes_ignore =
  | IgnoreBoolInputs
  | IgnoreBoolInputsNNumConstrs

(* -------------------------------------------------------------------------- *)

type analysis_spec =
  | AnalysisBoolean of AnalysisStd.bool_param
  | AnalysisStandard of AnalysisStd.std_param * dom_spec
  | AnalysisAccel of AnalysisAcc.acc_param * dom_spec
  | AnalysisHybrid of AnalysisStd.hyb_param * dom_spec

(* -------------------------------------------------------------------------- *)

type descr = formatter -> unit

type stratspec =
  | Trans of trans_spec
  | Analysis of analysis_spec

type strategy = stratspec list

type spec = Env.t * strategy

(******************************************************************************)
(* strategy descriptions *)
(******************************************************************************)
(* add new strategy descriptions here *)

let strats =  (* identifier * short description * long description list *)
  [
  (********** PARTITIONING ***********************************************)
   ("pIF",("initial, final and other states",
           "partition by initial, final and other states"));
   ("pE",("enumeration of Boolean states",
          "enumerate (non initial/non final) Boolean states, options: v=<vars>...restrict enumeration to variables"));
   ("pM",("manual partitioning",
          "manual partitioning, options: e=<exprs>...partitioning predicates"));
   ("pMD",("discrete numerical modes (Boolean-defined)",
           "partition by discrete numerical modes (Boolean-defined), "^
              "options: v=<vars>...restrict to given numerical variables, "^
              "i=<forget>...bi=ignore Boolean inputs (default), bic=ignore Boolean inputs and numerical constraints"));
   ("pMHB",("continuous modes (Boolean-defined)",
           "partition by continuous modes (Boolean-defined), "^
              "options: v=<vars>...restrict to given numerical variables, "^
              "i=<forget>...bi=ignore Boolean inputs (default), bic=ignore Boolean inputs and numerical constraints"));
   ("pMHN",("continuous modes (numerically defined)",
           "partition by continuous modes (numerically defined), "^
              "options: d=<dom>...domain (default: FdpI), "^
              "ws=<widening start>, wd=<descending iterations>"));
   ("pQ",("enumeration of hybrid translation variables",
          "enumerate hybrid translation state variables"));
   ("pS",("convex staying conditions",
          "split into convex staying conditions"));

   ("tR",("relational abstractions",
          "relational abstractions"));

   ("pB",("boolean backward bisimulation",
          "refine partition by boolean backward bisimulation"));

  (********** PREPROCESSING ***********************************************)
   ("rT",("refine transition functions to arcs",
          "refine transition functions to arcs"));
   ("rB",("move boolean inputs into guards",
          "move boolean inputs into guards"));
   ("rS",("split non-convex guards",
          "split non-convex guards"));

   ("rAB",("move boolean inputs into guards and categorize accelerable loops",
           "move boolean inputs into guards and categorize accelerable loops, "^
             "options: d=<decoupling mode>, O...no decoupliing, "^
             "B...boolean/accelerable decoupling, "^
             "N...boolean+non-accelerable/accelerable decoupling (default)"));
   ("rAS",("split non-convex numerical guards in accelerable self-loops",
          "split non-convex numerical guards in accelerable self-loops"));
   ("rAF",("flatten accelerable self-loops",
          "flatten accelerable self-loops"));
   ("rAD",("decouple accelerable from non-accelerable or Boolean self-loops",
          "decouple accelerable from non-accelerable or Boolean self-loops"));
   ("rAI",("inputization for decoupled self-loops",
          "inputization for decoupled self-loops"));

  (********** ANALYSIS ***********************************************)
  (********** discrete ***********************************************)
   ("aB",("boolean analysis",
           "boolean analysis, options: b...backward"));
   ("aS",("standard analysis",
           "standard analysis, options: d=<domain>, b...backward, ws=<widening start>, wd=<descending iterations>"));
   ("aA",("analysis with abstract acceleration",
           "analysis with abstract acceleration, options:d=<domain>, b...backward, ws=<widening start>, wd=<descending iterations>, aws=<widening start for accelerable transitions>"));

  (********** ANALYSIS ***********************************************)
  (********** hybrid ***********************************************)
   ("aH",("hybrid analysis with time elapse",
           "hybrid analysis with time elapse, options: d=<domain>, ws=<widening start>, wd=<descending iterations>"));
  ]

let pp_short_stratdesc s fmt = pp_print_string fmt (fst (List.assoc s strats))

let print_strategies fmt () =
  List.iter
    (fun (s,(_,longdesc)) ->
      fprintf fmt "@[ ";
      Util.print_fixed fmt 5 s;
      fprintf fmt "@[<hov>";
      Util.print_breakable fmt longdesc;
      fprintf fmt "@]@]@.")
    strats

(******************************************************************************)
(* domains *)
(******************************************************************************)

let domain_descs =
  [("P",("convex polyhedra","convex polyhedra, options: l...without strict inequalities, p...power domain"));
   ("O",("octagons","octagons, options: p...power domain"));
   ("I",("intervals","intervals, options: p...power domain"));
   (* ("J",("anticonvex intervals","anticonvex intervals, options: p...power domain")); *)
   (* ("TE",("template polyhedra (emulation)","template polyhedra (emulation), options: t=<template expressions>, p...power domain")); *)
   (* ("FdpI",("finitely disjunctive partitioned interval domain","finitely disjunctive partitioned interval domain")); *)
  ]

let print_domains fmt () =
  List.iter
    (fun (s,(_,longdesc)) ->
      fprintf fmt "@[ ";
      Util.print_fixed fmt 5 s;
      fprintf fmt "@[<hov>";
      Util.print_breakable fmt longdesc;
      fprintf fmt "@]@]@.")
    domain_descs

let invalid_dom d = raise (InvalidDomain d)
let invalid_dom_options o = raise (InvalidDomainOptions o)

let pwext_defaults = Util.list2mappe [
  ("bdisj", string_of_bool DomExt.DefaultFPParams.booldisj);
  ("norelax", string_of_bool (not DomExt.DefaultFPParams.relax));
  ("uqbdd", string_of_bool DomExt.DefaultFPParams.uqbdd);
]

let parse_pwext _env opts =
  let defaults = pwext_defaults in
  let bdisj = get_option opts defaults "bdisj" bool_option in
  let norelax = get_option opts defaults "norelax" bool_option in
  let uqbdd = get_option opts defaults "uqbdd" bool_option in
  { bdisj; norelax; uqbdd }

let mk_pwext_params { bdisj; norelax; uqbdd } =
  (module struct
    let booldisj = bdisj
    let relax = not norelax
    let uqbdd = uqbdd
  end : DomExt.FP_PARAMS)

let domain_defaults = Util.list2mappe [
  ("l","s");("m","b");("p","c");("t","OCT");
  ("D","false");
  ("s","s");
]

let full_domain_defaults =
  Mappe.merge (fun _ a -> a) domain_defaults pwext_defaults

let parse_domain ?(force_power=false) ?(forbid_pwext=false) env dom : dom_spec =
  let domstr, options = try parse_param dom with _ -> invalid_dom dom in
  let defaults = domain_defaults in
  check_extra_options logger "d" full_domain_defaults options;
  let power = get_option options defaults "p" (function ""->"p" |x->x)
  and pwext = get_option options defaults "D" (function
    | "false" -> None
    | "true" -> Some ""
    | x -> Some x)
  and mtbdd = get_option options defaults "m" (function ""->"m" |x->x)
  and loose = match domstr with
    | "P" -> get_option options defaults "l" (function ""->"l" |x->x)
    | _ -> ""
  in
  let power = if force_power && power <> "p"
    then (Log.i logger "Forcing selection of power domain."; "p")
    else power
  and pwext = if forbid_pwext && pwext <> None
    then (Log.i logger "Forcing de-selection of powerset extension."; None)
    else pwext
  in
  let num = match domstr with
    | "I" -> Intervals
    | "O" -> Octagons
    | "P" when loose = "s" -> ConvexPol true
    | "P" when loose = "l" -> ConvexPol false
    | _ -> invalid_dom dom                              (* XXX precise message *)
  in
  let dd = match mtbdd with
    | "b" -> Bdd
    | "m" -> Mtbdd
    | _ -> invalid_dom dom                              (* XXX precise message *)
  in
  let meth = match power with
    | "c" -> Prod
    | "p" -> Pow
    | _ -> invalid_dom dom                              (* XXX precise message *)
  in
  let boolcomp_spec = meth, dd, num in
  let domcomp = match pwext with
    | None -> Direct
    | Some "" -> Pwext (parse_pwext env options)
    | Some opts ->
        let opts = try parse_options opts with _ -> invalid_dom_options opts in
        Pwext (parse_pwext env opts)
  in
  (domcomp, boolcomp_spec)

(* --- *)

let mk_boolcomp_dd: _ -> (module BddapronDoman.T) = function
  | Bdd,   Intervals -> (module BddapronDoman.Box)
  | Mtbdd, Intervals -> (module BddapronDoman.BoxMt)
  | Bdd,   Octagons  -> (module BddapronDoman.Oct)
  | Mtbdd, Octagons  -> (module BddapronDoman.OctMt)
  | Bdd,   ConvexPol true  -> (module BddapronDoman.StrictPol)
  | Mtbdd, ConvexPol true  -> (module BddapronDoman.StrictPolMt)
  | Bdd,   ConvexPol false -> (module BddapronDoman.LoosePol)
  | Mtbdd, ConvexPol false -> (module BddapronDoman.LoosePolMt)

let mk_boolcomp (module E: Env.T) (comp, dd, num) : (module Domain.T) =
  let module BaseDoman = (val mk_boolcomp_dd (dd, num)) in
  match comp with
    | Prod -> Domain.prod (module BaseDoman) (module E)
    | Pow -> Domain.pow  (module BaseDoman) (module E)

let mk_domain env ((domcomp, boolcomp) : dom_spec) =
  let module E = (val Env.as_module env) in
  let module Dom = (val mk_boolcomp (module E) boolcomp) in
  match domcomp with
    | Direct -> (module Dom : Domain.T)
    | Pwext opts -> DomExt.pwext (module Dom) (mk_pwext_params opts)

(******************************************************************************)
(* parsing of strategy options *)
(******************************************************************************)
(* add parsing of strategy options here *)

let invalid_opt o v = raise (InvalidStrategyOptionValue (o, v))

let comma_str_to_vars str = comma_str_to_strlist str |> List.map Env.Symb.mk

let parse_pE env options =
  let defaults = Util.list2mappe [("v","")] in
  let vars = get_option options defaults "v" comma_str_to_vars in
  let vars = if Util.list_is_empty vars then env.Env.bs_vars else vars in
  PartEnum (vars, "pE")

let parse_pM env options =
  let defaults = Util.list2mappe [("e","")] in
  let exprlist = get_option options defaults "e" comma_str_to_strlist in
  let exprs = Env.strlist_to_boolexprlist env exprlist in
  PartManual exprs

let parse_pMD env options =
  let defaults = Util.list2mappe [("v","");("i","bi")] in
  let vars = get_option options defaults "v" comma_str_to_vars in
  let vars = if Util.list_is_empty vars then env.Env.ns_vars else vars in
  match get_option options defaults "i" (fun x -> x) with
    |"bi" -> PartDiscreteModesBool (vars, IgnoreBoolInputs)
    |"bic" -> PartDiscreteModesBool (vars, IgnoreBoolInputsNNumConstrs)
    |x -> invalid_opt "i" x

let parse_pMHB env options =
  let defaults = Util.list2mappe [("v","");("i","bi")] in
  let vars = get_option options defaults "v" comma_str_to_vars in
  let vars = if Util.list_is_empty vars then env.Env.ns_vars else vars in
  match get_option options defaults "i" (fun x -> x) with
    |"bi" -> PartHybridModesBool (vars, IgnoreBoolInputs)
    |"bic" -> PartHybridModesBool (vars, IgnoreBoolInputsNNumConstrs)
    |x -> invalid_opt "i" x

let parse_pMHN env options =
  let defaults = Util.list2mappe
    [("d","FdpI");("ws","0");("wd","1")] in
  let ws = get_option options defaults "ws" (int_of_string) in
  let wd = get_option options defaults "wd" (int_of_string) in
  let dom = get_option options defaults "d" (fun x -> x) in
  let params = AnalysisStd.make_hyb_param ws wd in
  PartHybridModesNumerical (params, parse_domain env dom)

let parse_rAB env options =
  let defaults = Util.list2mappe [("d","N")] in
  let decouplemode = get_option options defaults "d" (function
      |"N" -> TransAcc.DecoupleBoolNacc
      |"B" -> TransAcc.DecoupleBool
      |"O" -> TransAcc.DecoupleNone
      |x -> invalid_opt "d" x) in
  MoveBoolInputsToGuardsAcc decouplemode

let parse_aS env options =
  let defaults = Util.list2mappe
    [("b","f");("d","P:s,c");("ws","2");("wd","2")] in
  let dir = get_option options defaults "b"
    (function |"f" -> `Forward |_-> `Backward) in
  let ws = get_option options defaults "ws" (int_of_string) in
  let wd = get_option options defaults "wd" (int_of_string) in
  let dom = get_option options defaults "d" (fun x -> x) in
  let params = AnalysisStd.make_std_param dir ws wd in
  AnalysisStandard (params, parse_domain env dom)

let parse_aB _env options =
  let defaults = Util.list2mappe [("b","f")] in
  let dir = get_option options defaults "b"
    (function |"f" -> `Forward |_-> `Backward) in
  let params = AnalysisStd.make_bool_param dir in
  AnalysisBoolean params

let parse_aA env options =
  let defaults = Util.list2mappe
    [("b","f");("d","P:s,c");("ws","2");("aws","7");("wd","2")] in
  let dir = get_option options defaults "b"
    (function |"f" -> `Forward |_-> `Backward) in
  let ws = get_option options defaults "ws" (int_of_string) in
  let aws = get_option options defaults "aws" (int_of_string) in
  let wd = get_option options defaults "wd" (int_of_string) in
  let dom = get_option options defaults "d" (fun x -> x) in
  let params = AnalysisAcc.make_acc_param dir ws aws wd in
  AnalysisAccel (params, parse_domain env dom)

let parse_aH env options =
  let defaults = Util.list2mappe
    [("d","P:s,c");("ws","2");("wd","2")] in
  let ws = get_option options defaults "ws" (int_of_string) in
  let wd = get_option options defaults "wd" (int_of_string) in
  let dom = get_option options defaults "d" (fun x -> x) in
  let params = AnalysisStd.make_hyb_param ws wd in
  AnalysisHybrid (params, parse_domain env dom)

(******************************************************************************)
(* parses a strategy from a string *)
(******************************************************************************)

let invalid_strat msg o = raise (InvalidStrategy (fun fmt -> fprintf fmt msg o))
let invalid_strat' msg = raise (InvalidStrategy (fun fmt -> fprintf fmt msg))

(* add new strategies here *)
let str_to_strategy env str =
  let ss = semicolon_str_to_strlist str in
  if ss = [] then
    invalid_strat' "empty@ strategy@ specification";
  env, List.map (fun s ->
    let p = try parse_param s with
      | Stdlib.Exit | Parse.Lex_error | Parsing.Parse_error ->
          invalid_strat "syntax@ error@ in@ `%s'" s
    in
    match p with

      (********** PARTITIONING ***********************************************)
      |("pIF",_) -> Trans PartInitFinal
      |("pE",options) -> Trans (parse_pE env options)
      |("pM",options) -> Trans (parse_pM env options)
      |("pMD",options) -> Trans (parse_pMD env options)
      |("pMHB",options) -> Trans (parse_pMHB env options)
      |("pMHN",options) -> Trans (parse_pMHN env options)
      |("pQ",_) -> Trans (PartEnum (env.Env.cont_q_vars, "pQ"))
      |("pS",_) -> Trans PartHybridConvexStayConditions
      |("pB",_) -> Trans PartBoolBackwardBisimulation
      |("tR",_) -> Trans TransRelationalAbstractions

  (********** PREPROCESSING ***********************************************)
      |("rT",_) ->  Trans RefineTransFuncToArcs
      |("rAB",options) -> Trans (parse_rAB env options)
      |("rAS",_) -> Trans RefineSplitNonConvexGuardsInAccSelfLoops
      |("rAF",_) -> Trans RefineFlattenAccSelfLoops
      |("rAD",_) -> Trans RefineDecoupleAccSelfLoops
      |("rAI",_) -> Trans RefineInputizationDecoupledSelfLoops
      |("rB",_) -> Trans MoveBoolInputsToGuards
      |("rS",_) -> Trans RefineSplitNonConvexGuards

  (********** ANALYSIS ***********************************************)
  (********** discrete ***********************************************)
      |("aS",options) -> Analysis (parse_aS env options)
      |("aB",options) -> Analysis (parse_aB env options)
      |("aA",options) -> Analysis (parse_aA env options)

  (********** ANALYSIS ***********************************************)
  (********** hybrid *************************************************)
      |("aH",options) -> Analysis (parse_aH env options)

  (*******************************************************************)
      |(s,_) -> invalid_strat "unknown@ item@ `%s'" s
    )
    ss

(******************************************************************************)
(* utilities *)
(******************************************************************************)
(******************************************************************************)
(* refines a CFG according to the given analysis result *)
let refine ?(refine_bool=true) env cfprog refine_loc =
  let cfg = cfprog.Program.c_cfg in
  (* replace invariants *)
  PSHGraph.iter_vertex cfg
    (fun v inv ~pred:_ ~succ:_ ->
      match refine_loc ~refine_bool v inv with
	|None -> PSHGraph.remove_vertex cfg v
	|Some refinv -> PSHGraph.replace_attrvertex cfg v refinv
    );
  (* check feasibility of arcs*)
  let _ = Cfg.remove_infeasible_arcs env cfg cfprog.Program.c_ass in
  (* remove unconnected locations *)
  let (unreach,_) = PSHGraph.reachable_multi
    Cfg.locid_dummy Cfg.arcid_dummy cfg
    (Cfg.get_locidset_by_inv env cfg cfprog.Program.c_init) in
  PSHGraph.iter_vertex cfg
    (fun v inv ~pred ~succ ->
      if PSette.mem v unreach then PSHGraph.remove_vertex cfg v);
  cfprog

(******************************************************************************)
(* run the strategy *)
(******************************************************************************)

let mk_trans env = function
  | PartInitFinal ->
      TransGen.initfinal, pp_short_stratdesc "pIF"
  | PartEnum (vars, sd) ->
      TransGen.enumerate vars, pp_short_stratdesc sd
  | PartManual bexprs ->
      TransGen.manual bexprs, pp_short_stratdesc "pM"
  | PartDiscreteModesBool (vars, IgnoreBoolInputs) ->
      TransDiscrete.modes_bool1 vars, pp_short_stratdesc "pMD"
  | PartDiscreteModesBool (vars, IgnoreBoolInputsNNumConstrs) ->
      TransDiscrete.modes_bool2 vars, pp_short_stratdesc "pMD"
  | PartHybridModesBool (vars, IgnoreBoolInputs) ->
      TransHybrid.modes_bool1 vars, pp_short_stratdesc "pMHB"
  | PartHybridModesBool (vars, IgnoreBoolInputsNNumConstrs) ->
      TransHybrid.modes_bool2 vars, pp_short_stratdesc "pMHB"
  | PartHybridModesNumerical (params, dom_spec) ->
      let module Dom = (val mk_domain env dom_spec) in
      let module An = AnalysisStd.Hyb (Dom) in
      (TransHybrid.modes_num (An.analyze params),
       fun fmt -> fprintf fmt "%t@ analyzed@ with@ %t" (pp_short_stratdesc "pMHN")
         Dom.descr)
  | PartHybridConvexStayConditions ->
      TransHybrid.convex_staycond, pp_short_stratdesc "pS"
  | PartBoolBackwardBisimulation ->
      TransGen.boolbacksim, pp_short_stratdesc "pB"
  | TransRelationalAbstractions ->
      RelAbstr.transform, pp_short_stratdesc "tR"
  | RefineTransFuncToArcs ->
      TransGen.refine_by_destloc, pp_short_stratdesc "rT"
  | RefineSplitNonConvexGuards ->
      TransGen.split_arcs, pp_short_stratdesc "rS"
  | MoveBoolInputsToGuards ->
      TransGen.bool_inputs_to_guards, pp_short_stratdesc "rB"
  | MoveBoolInputsToGuardsAcc decouplemode ->
      TransAcc.bool_inputs_to_guards decouplemode, pp_short_stratdesc "rAB"
  | RefineSplitNonConvexGuardsInAccSelfLoops ->
      TransAcc.split_arcs, pp_short_stratdesc "rAS"
  | RefineFlattenAccSelfLoops ->
      TransAcc.flatten_loops, pp_short_stratdesc "rAF"
  | RefineDecoupleAccSelfLoops ->
      TransAcc.decouple_equs, pp_short_stratdesc "rAD"
  | RefineInputizationDecoupledSelfLoops ->
      TransAcc.inputize, pp_short_stratdesc "rAI"

let mk_analysis env = function
  | AnalysisBoolean params ->
      AnalysisStd.Bool.analyze params, AnalysisStd.Bool.descr params
  | AnalysisStandard (params, dom_spec) ->
      let module Dom = (val mk_domain env dom_spec) in
      let module A = AnalysisStd.Std (Dom) in
      A.analyze params, A.descr params
  | AnalysisAccel (params, dom_spec) ->
      let module Dom = (val mk_domain env dom_spec) in
      let module A = AnalysisAcc.Acc (Dom) in
      A.analyze params, A.descr params
  | AnalysisHybrid (params, dom_spec) ->
      let module Dom = (val mk_domain env dom_spec) in
      let module A = AnalysisStd.Hyb (Dom) in
      A.analyze params, A.descr params

(* (\* display CFG info *\) *)
(* let display_cfg_size cfg = *)
(*   let (cntv,cnth,_,_) = PSHGraph.size cfg in *)
(*   Log.i logger "CFG (%d location(s), %d arc(s)" cntv cnth *)

(* run the verification strategy *)
let run env ((prev_env, strategy): spec) cfprog =
  (* For now, use physical equality to check that the environment is the
     same… We shall upgrade expressions later. *)
  assert (prev_env == env);
  let rec do_run strats cfprog get_anres =
    match strats with
      |[] -> (false,get_anres,cfprog)
      |Trans trans :: tailstrats ->
          let trans, descr = mk_trans env trans in
          Log.i logger "@[transformation '%t'@]" descr;
          let cfprog = trans env cfprog in
          Log.i logger "@[CFG: (%a)%t@]" (Program.pp_cfg_short env) cfprog
            (fun fmt -> Log.cd logger "@\n%a" fmt (Program.pp_cfg env) cfprog);
          do_run tailstrats cfprog get_anres
      |Analysis analysis :: tailstrats ->
          let analysis, descr = mk_analysis env analysis in
          Log.i logger "%t" descr;
          let (res, refine_loc, print_res, get_anres) = analysis env cfprog in
          Log.i logger "%t@ returned %a" descr Analysis.pp_result res;
          Log.i logger "analysis@ result:@ %t" print_res;
          (* checkres env cfprog direction anres*)
          (*print_union_reach env cfprog anres; *)
          if Analysis.conclusive res || tailstrats == []
          then (res, get_anres, cfprog)
          else begin
            (* Env.cudd_reorder env; *)
            let cfprog = refine env cfprog (refine_loc) in
            Log.i logger "@[Refined CFG: (%a)%t@]\
                         " (Program.pp_cfg_short env) cfprog
              (fun fmt -> Log.cd logger "@\n%a" fmt (Program.pp_cfg env) cfprog);
            do_run tailstrats cfprog get_anres
          end

  in
  do_run strategy cfprog (fun () -> Analysis.bddapron_res_empty)
