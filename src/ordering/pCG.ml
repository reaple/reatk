open Format
open Graph
open CtrlNbac
open AST

module Make (W: Weight.T) = struct

  module W = W
  module G = Imperative.Graph.ConcreteLabeled (Symb.M) (W)
  module Components = Components.Undirected (G)

  module ToDot =
    Graphviz.Dot (struct
      include G
      let graph_attributes _ = []
      let default_vertex_attributes _ = []
      let vertex_name v =
        asprintf "\"%a\"" Symb.print (V.label v)
      let vertex_attributes _ = []
      let get_subgraph _ = None
      let default_edge_attributes _ = []
      let edge_attributes e = [
        `Label (W.to_string (E.label e));
        `Arrowhead `None
      ]
    end)

  include G

  let print_path = Realib.Util.pp_lst (fun fmt v -> Symb.print fmt (V.label v))

  module OfWSDeps
    (S: sig
      module W: Weight.T
      module V: Sig.VERTEX with type t = Symb.t
      include Gmap.E_SRC with type E.t = V.t * W.t * V.t
    end)
    (MapWeight: Weight.CONVERTION with type src = S.W.t and type dst = W.t)
    =
  struct

    module EdgeMap = Gmap.Edge (S) (struct
      include G
      let empty () = create ()
      let add_vertex g v = add_vertex g v; g
      let add_edge_e g e =
        let u, v =
          let u, v = E.src e, E.dst e in
          if Symb.M.compare (V.label u) (V.label v) < 0 then u, v
          else v, u
        in
        let l = E.label e in
        (try let e' = find_edge g u v in
             remove_edge_e g e';
             add_edge_e g (E.create u (W.sum (E.label e') l) v)
         with Not_found -> add_edge_e g (E.create u l v));
        g
    end)

    let convert wsdeps keep =
      let vset = SSet.fold (fun v -> SMap.add v (V.create v)) keep SMap.empty in
      let g = EdgeMap.map (fun (v, w, v') ->
        E.create (SMap.find v vset) (MapWeight.convert w) (SMap.find v' vset))
        wsdeps
      in
      SMap.iter (fun _ -> add_vertex g) vset;
      g

  end

end
