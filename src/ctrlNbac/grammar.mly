(* This file is part of ReaTk released under the GNU GPL; Please read the
   LICENSE file packaged in the distribution. *)

%{
  (* open Lexing *)
  open Format
  open AST
  open ParserUtils

  let error = perror
  let error1 = perror1
  let error2 = perror2
  let mk_symb = Symb.of_string
  let pp_symb = Symb.print

  let tt = mk_bcst true and ff = mk_bcst false
  and zero = mk_nicst 0

  let tt'= mk_bcst' true

%}

(* %======================================================================== *)
(* \subsection{Tokens} *)
(* %======================================================================== *)

%token EOF

(* delimiters *)
%token TK_LPAREN TK_RPAREN TK_LBRACE TK_RBRACE TK_LBRACKET TK_RBRACKET
%token TK_COMMA TK_SEMI
%token TK_PRIME
(*%token TK_DOT*)
%token TK_COLON
%token TK_QMARK
%token TK_COLONEQ
%token TK_DASH
%token TK_RARROW

(* boolean operations *)
%token TK_TRUE TK_FALSE
%token TK_NOT TK_OR TK_AND TK_NE TK_EQ TK_SHARP TK_IN TK_RARROW2

(* bitwise operations *)
%token TK_AMP TK_HAT TK_PIPE TK_TILDE

(* arithmetic operations *)
%token TK_STAR TK_SLASH TK_PLUS TK_MINUS TK_GTGT TK_LTLT TK_GTGTGT TK_LTLTLT

(* mixed operations *)
%token TK_GE TK_LE TK_GT TK_LT

(* control *)
%token TK_IF TK_THEN TK_ELSE

(* declarations *)
%token TK_TYPEDEF TK_ENUM TK_BOOL TK_INT TK_REAL TK_UINT TK_SINT
%token TK_INVARIANT TK_INITIAL TK_FINAL TK_ASSERTION TK_REACHABLE TK_ATTRACTIVE
%token TK_AUTOMATON
%token TK_LOCATION
%token TK_VALUE
%token TK_INPUT
%token TK_OUTPUT
%token TK_CONTR
%token TK_LOCAL
%token TK_STATE
%token TK_TRANS TK_DEFINITION

%token <int> TK_NUM
%token <Mpqf.t> TK_MPQF
%token <string> TK_ID

(* Priorities *)
%left TK_ELSE
%left TK_OR TK_RARROW2                      /* 1 */
%left TK_AND                                /* 2 */
%left TK_LT TK_GT TK_LE TK_GE
%left TK_EQ TK_NE
%left TK_LTLT TK_GTGT TK_LTLTLT TK_GTGTGT
%left TK_PIPE
%left TK_HAT
%left TK_AMP
%left TK_PLUS TK_MINUS                      /* 5 */
%left TK_STAR TK_SLASH                      /* 6 */

%start node cfg func pred pexpr

%type <ParserUtils.node_descr> node
%type <ParserUtils.cfg_descr> cfg
%type <ParserUtils.func_descr> func
%type <ParserUtils.pred_descr> pred
%type <Loc.t AST.exp> pexpr
%type <Symb.t * Loc.t> id
%type <(Symb.t * Loc.t) list> idlist

%% /* end */

(* %============================================================== *)
(* \section{Rules} *)
(* %============================================================== *)

node:
| node_declarations definitions transitions node_formulas EOF { ($1, $2, $3, $4) }
| error EOF { error $startpos $endpos "Syntax@ error"; raise Parsing.Parse_error }

cfg:
| node_declarations definitions graph transitions_opt node_formulas EOF { ($1, $2, $3, $4, $5) }
| error EOF { error $startpos $endpos "Syntax@ error"; raise Parsing.Parse_error }

func:
| func_declarations definitions func_formulas EOF { ($1, $2, $3) }
| error EOF { error $startpos $endpos "Syntax@ error"; raise Parsing.Parse_error }

pred:
| pred_declarations definitions pred_formulas EOF { match $3 with
    | None -> raise Parsing.Parse_error
    | Some v -> ($1, $2, v)
}
| error EOF { error $startpos $endpos "Syntax@ error"; raise Parsing.Parse_error }

  (* %======================================================================== *)
  (* \subsection{Declarations} *)
  (* %======================================================================== *)
  /* end */

node_declarations:
| typedef state ucdecls local { ($1, $2, $3, $4) }
  /* end */

func_declarations:
| typedef iodecls local { ($1, $2, $3) }
  /* end */

pred_declarations:
| typedef ucdecls local { ($1, $2, $3) }
  /* end */

typedef:
| { [] }
| TK_TYPEDEF typdecls { List.rev $2 }
 ;

 maybe_eof: EOF {} | {};

 semi_or_eof: TK_SEMI {} | EOF {};

 typdecls:
| { [] }
| typdecls typdecl TK_SEMI {
  match $2 with
    | Some ((tn, l), ls) -> (tn, l, ls) :: $1
    | None -> $1
}
| typdecls error TK_SEMI {
  (* error $startpos($2) $endpos($2) "syntax@ error@ in@ type@ declaration"; *)
  error $startpos($2) $endpos($2) "Syntax@ error";
    $1
}
| typdecls error EOF {
  error $endpos($1) $endpos($1) "Type@ declaration,@ `!state',@ `!input'...@ expected";
    raise Parsing.Parse_error
}
 ;

 typdecl:
| id TK_EQ typspec {
  match $3 with
    | Some t -> Some ($1, t)
    | None ->
        error2 $startpos($3) $endpos($3)
          "Syntax@ error@ in@ declaration@ of@ type@ `%a'" pp_symb (fst $1);
        None
}
| error TK_EQ typspec {
  (error $startpos $endpos "Syntax@ error@ in@ type@ declaration";
   if $3 = None then
     error $startpos($3) $endpos($3) "Type@ specification@ expected";
   None)
}
 ;

 typspec:
| TK_ENUM TK_LBRACE idlist TK_RBRACE { Some (List.rev $3) }
| error maybe_eof { None }
 ;

 (* Variables ;; *)

 (* Alternating U/C groups (can be empty) *)
 ucdecls_list: | { [] } | ucdecls_list ucdecl { $2 :: $1 };
 ucdecl: | input contr { ($1, $2) };
 ucdecls:
| ucdecls_list { List.rev $1 }
| ucdecls_list input { List.rev (($2, []) :: $1) }
| contr { [([], $1) ] }
 ;

 (* Alternating I/O groups (cannot be empty) *)
 iodecls_list: | { [] } | iodecls_list iodecl { $2 :: $1 };
 iodecl: | input output { ($1, $2) };
 iodecls:
| iodecls_list iodecl { List.rev ($2 :: $1) }
| iodecls_list input { List.rev (($2, []) :: $1) }
| output { [([], $1) ] }
 ;

 state: | { [] } | TK_STATE vardecls { List.rev $2 };
 local: | { [] } | TK_LOCAL vardecls { List.rev $2 };
 input: | TK_INPUT vardecls { List.rev $2 };
 contr: | TK_CONTR ctrldecls { List.rev $2 };
 output: | TK_OUTPUT vardecls { List.rev $2 };

 vardecls:
| { [] }
| vardecls vardecl TK_SEMI {
  match $2 with
    | Some (var_list, typ) ->
        List.fold_left (fun res (var, loc) -> (var, typ, loc) :: res)
          $1 var_list
    | None -> $1
}
| vardecls error TK_SEMI {
  (* error $startpos($2) $endpos($2) "syntax@ error@ in@ variable@ declarations"; *)
  error $startpos($2) $endpos($2) "Syntax@ error";
    $1
}
| vardecls error EOF {
  error $endpos($1) $startpos($3) "Variable@ declarations,@ `!definition'...@ expected";
    raise Parsing.Parse_error
}
 ;

 vardecl:
| idlist TK_COLON typ { Some (List.rev $1, $3) }
| idlist TK_COLON error maybe_eof {
  vardecl_error $endpos($2) $endpos (List.map fst $1);
    None
}
| error TK_COLON typ {
  error $startpos $endpos "Variable@ declarations@ expected";
    None
}
| error TK_COLON error maybe_eof {
  (error $startpos($1) $endpos($1) "Variables@ expected";
   error $startpos($3) $endpos($3) "Type@ name@ expected";
   None)
}
 ;

 ctrldecls:
| { [] }
| ctrldecls ctrldecl TK_SEMI {
  match $2 with
    | Some (lvars, t) ->
        List.fold_left (fun res (var, pspec, loc) -> (var, pspec, t, loc) :: res)
          $1 lvars
    | None -> $1
}
| ctrldecls error TK_SEMI {
  (* error $startpos($2) $endpos($2) "syntax@ error@ in@ variable@ declarations"; *)
  error $startpos($2) $endpos($2) "Syntax@ error"; $1
}
| ctrldecls error EOF {
  error $endpos($1) $endpos($1) "Variable@ declarations,@ `!definition'...@ expected";
    raise Parsing.Parse_error
}
 ;

 ctrldecl:
| cidlist TK_COLON typ { Some (List.rev $1, $3) }
| cidlist TK_COLON error maybe_eof {
  vardecl_error $endpos($2) $endpos (List.map (fun (a, _, _) -> a) $1);
    None
}
| error TK_COLON typ {
  error $startpos $endpos "Controllable@ variable@ declarations@ expected";
    None
}
| error TK_COLON error maybe_eof {
  (error $startpos($1) $endpos($1) "Controllable@ variables@ expected";
   error $startpos($3) $endpos($3) "Type@ name@ expected";
   None)
}
 ;

 cid:
| id                { fst $1, `None,          snd $1 }
| id TK_QMARK TK_LT { fst $1, `Ascend,        snd $1 }
| id TK_QMARK TK_GT { fst $1, `Descend,       snd $1 }
| id TK_QMARK expr  { fst $1, `Expr (fst $3), snd $1 }
 ;

 cidlist:
| cid                  { [ $1 ] }
| cidlist TK_COMMA cid { $3 :: $1 }
 ;

 id:
| TK_ID { mk_symb $1, Loc.make $startpos $endpos }
 ;

 idlist:
| id { [ $1 ] }
| idlist TK_COMMA id { $3 :: $1 }
| idlist id {
  warning $endpos($1) $endpos($1) "Missing@ comma@ (probably)";
    (* warning $endpos($1) (pos_pred $startpos($2)) "missing@ comma@ (probably)"; *)
    $2 :: $1
}
 ;

 typ:
| TK_BOOL { `Bool }
| TK_INT  { `Int }
| TK_REAL { `Real }
| bint { `Bint $1 }
| id { (`Enum (mk_typname (fst $1))) }
 ;

 bint:
| TK_UINT TK_LBRACKET TK_NUM TK_RBRACKET { (false, $3) }
| TK_SINT TK_LBRACKET TK_NUM TK_RBRACKET { (true, $3) }
 ;

 (* %======================================================================== *)
 (* \subsection{Equations} *)
 (* %======================================================================== *)

 definitions:
| { [] }
| TK_DEFINITION defs { List.rev $2 }
 ;

 defs:
| { [] }
| defs def_ TK_SEMI {
  match $2 with
    | Some ((var, loc), exp) -> (var, exp, loc) :: $1
    | None -> $1
}
| defs error TK_SEMI {
  (* error $startpos($2) $endpos($2) "syntax@ error@ in@ definitions"; *)
  error $startpos($2) $endpos($2) "Syntax@ error";
    $1
}
| defs error EOF {
  error $endpos($1) $endpos($1) "Definitions,@ `!transition'...@ expected";
    raise Parsing.Parse_error
}
 ;

 def_:
| id TK_EQ expr { Some ($1, fst $3) }
(* | id TK_EQ error maybe_eof { *)
(*   error1 $startpos $endpos *)
(*   "syntax@ error@ in@ equation@ definition@ for@ variable@ `%s'" (fst $1); *)
(*     None *)
(* } *)
 ;

 transitions_opt:
| { [] }
| transitions { $1 }
 ;

 transitions:
| TK_TRANS equations TK_SEMI { List.rev $2 }
| TK_TRANS equations error TK_SEMI {
  (* error $startpos($2) $endpos($2) "syntax@ error@ in@ transition@ function"; *)
  error $startpos($2) $endpos($2) "Syntax@ error"; $2
}
 ;

 equations:
| equation {
  match $1 with
    | Some ((var, loc), exp) -> [ var, exp, loc ]
    | None -> []
}
| equations TK_SEMI equation {
  match $3 with
    | Some ((var, loc), exp) -> (var, exp, loc) :: $1
    | None -> $1
}
| equations error EOF {
  error $endpos($1) $endpos($1) "Transition@ function,@ `!initial'...@ expected";
    raise Parsing.Parse_error
}
 ;

 assign_op:
| TK_PRIME TK_EQ {}
| TK_COLONEQ {}
 ;

 equation:
| id assign_op expr { Some ($1, fst $3) }
(* | id assign_op error maybe_eof { *)
(*   error1 $startpos($3) $endpos *)
(*   "syntax@ error@ in@ transition@ function@ definition@ for@ variable@ `%s'" *)
(*   (fst $1); *)
(*     None *)
(* } *)
 ;

 graph:
| TK_AUTOMATON id locations {
  (Some $2, snd $2, List.rev $3)
}
| TK_AUTOMATON locations {
  (None, Loc.make $startpos($1) $endpos($1), List.rev $2)
}
 ;

 locations:
| location {
  match $1 with
    | Some x -> [ x ]
    | None -> []
}
| locations location {
  match $2 with
    | Some x -> x :: $1
    | None -> $1
}
 ;

 location:
| TK_LOCATION id location_formulas arcs {
  Some (fst $2, snd $2, $3, $4)
}
| TK_LOCATION id location_formulas TK_SEMI {
  Some (fst $2, snd $2, $3, [])
}
| TK_LOCATION id error semi_or_eof {
  error $endpos($2) $startpos($4) "Arcs or location formulas expected"; None
}
;

 arcs:
| arc TK_SEMI { match $1 with Some a -> [a] | None -> [] }
| arc TK_SEMI arcs { match $1 with Some a -> a :: $3 | None -> $3 }
 ;

 arc:
| TK_DASH bexpr TK_QMARK arc_updates TK_RARROW id {
  Some ($2, List.rev $4, $6)
}
| TK_DASH arc_updates TK_RARROW id {
  Some (None, List.rev $2, $4)
}
| TK_DASH error TK_RARROW id {
  error $endpos($1) $startpos($3) "Guard or variable updates expected"; None
}
| TK_RARROW id {
  Some (None, [], $2)
}
;

 arc_updates:
| { [] }
| equations { $1 }
| equations TK_SEMI { $1 }
 ;

%inline location_formula:
| _e = location_init   { fun a -> { a with ld_initial   = _e } }
| _e = location_final  { fun a -> { a with ld_final     = _e } }
| _e = location_invar  { fun a -> { a with ld_final     = opt mk_neg' _e } }
| _e = location_assert { fun a -> { a with ld_assertion = _e } }
| _e = location_reach  { fun a -> { a with ld_reachable = _e } }
;
 (* Note the last one among duplicate formulas is selected. *)
 location_formulas:
| { { ld_initial = None; ld_final = None; ld_assertion = None;
      ld_reachable = None } }
| fs = location_formulas f = location_formula { f fs }
(*| error {
  error $startpos $endpos "Syntax@ error"; raise Parsing.Parse_error
  }*)
 ;

 location_init:
| TK_INITIAL bexpr { $2 }
| TK_INITIAL       { Some (flag (Loc.make $startpos $endpos) tt') }
 ;
 location_final:
| TK_FINAL bexpr { $2 }
| TK_FINAL       { Some (flag (Loc.make $startpos $endpos) tt') }
 ;
 location_invar:
| TK_INVARIANT bexpr { $2 }
| TK_INVARIANT       { Some (flag (Loc.make $startpos $endpos) tt') }
 ;
 location_assert:
| TK_ASSERTION bexpr { $2 }
 ;
 location_reach:
| TK_REACHABLE bexpr { $2 }
| TK_REACHABLE       { Some (flag (Loc.make $startpos $endpos) tt') }
 ;

 (* %======================================================================== *)
 (* \subsection{Formulas} *)
 (* %======================================================================== *)

%inline node_formula:
| _e = assertion { fun a -> { a with nf_assertion  = _e } }
| _e = initial   { fun a -> { a with nf_initial    = _e } }
| _e = final     { fun a -> { a with nf_invariant  = opt mk_neg' _e } }
| _e = invariant { fun a -> { a with nf_invariant  = _e } }
| _e = reach     { fun a -> { a with nf_reachable  = _e } }
| _e = attract   { fun a -> { a with nf_attractive = _e } }
;

 (* Note the last one among duplicate formulas is selected. *)
 node_formulas: | node_formulas_list { $1 };
 node_formulas_list:
| { { nf_assertion = None; nf_initial = None; nf_invariant = None;
      nf_reachable = None; nf_attractive = None; } }
| fs = node_formulas_list f = node_formula { f fs }
| node_formulas_list node_formula error {
  error $startpos($3) $endpos "Syntax@ error"; raise Parsing.Parse_error
};

 func_formulas:
| { { ff_assertion = None; } }
| assertion { { ff_assertion  = $1 } }
 ;

 pred_formulas:
| value { $1 }
 ;

 assertion: | TK_ASSERTION  bexpr TK_SEMI { $2 };
 initial:   | TK_INITIAL    bexpr TK_SEMI { $2 };
 final:     | TK_FINAL      bexpr TK_SEMI { $2 };
 invariant: | TK_INVARIANT  bexpr TK_SEMI { $2 };
 reach:     | TK_REACHABLE  bexpr TK_SEMI { $2 };
 attract:   | TK_ATTRACTIVE bexpr TK_SEMI { $2 };
 value:     | TK_VALUE      bexpr TK_SEMI { $2 };

 bexpr:
| expr { select_exp as_bexp $1 }
| error maybe_eof { merror $startpos $endpos "Boolean@ expression@ expected";
                    raise Parsing.Parse_error }
 ;

 (* %======================================================================== *)
 (* \subsection{Standard expressions} *)
 (* %======================================================================== *)

%inline bunop:
| _o = TK_NOT { mk_neg', as_bexp, ff, $startpos(_o), $endpos(_o) }
;
%inline nunop:
| _o = TK_MINUS { mk_opp', as_nexp, zero, $startpos(_o), $endpos(_o) }
| _o = TK_TILDE { mk_lnot', as_nexp, zero, $startpos(_o), $endpos(_o) }
;

%inline bbinop:
| _o = TK_OR      { mk_or',  as_bexp, ff, $startpos(_o), $endpos(_o) }
| _o = TK_AND     { mk_and', as_bexp, ff, $startpos(_o), $endpos(_o) }
| _o = TK_RARROW2 { mk_imp', as_bexp, ff, $startpos(_o), $endpos(_o) }
;
%inline pbinop:
| _o = TK_EQ { mk_peq', (fun i -> i), ff, $startpos(_o), $endpos(_o) }
| _o = TK_NE { mk_pne', (fun i -> i), ff, $startpos(_o), $endpos(_o) }
;
%inline nbinop:
| _o = TK_GE { mk_ge', as_nexp, ff, $startpos(_o), $endpos(_o) }
| _o = TK_GT { mk_gt', as_nexp, ff, $startpos(_o), $endpos(_o) }
| _o = TK_LE { mk_le', as_nexp, ff, $startpos(_o), $endpos(_o) }
| _o = TK_LT { mk_lt', as_nexp, ff, $startpos(_o), $endpos(_o) }
;
%inline lbop:
| _o = TK_PIPE   { mk_lor',  as_nexp, zero, $startpos(_o), $endpos(_o) }
| _o = TK_HAT    { mk_lxor', as_nexp, zero, $startpos(_o), $endpos(_o) }
| _o = TK_AMP    { mk_land', as_nexp, zero, $startpos(_o), $endpos(_o) }
;
%inline lsop:
| _o = TK_LTLT   { mk_lsl', as_nexp, zero, $startpos(_o), $endpos(_o) }
| _o = TK_GTGT   { mk_lsr', as_nexp, zero, $startpos(_o), $endpos(_o) }
| _o = TK_LTLTLT { mk_asl', as_nexp, zero, $startpos(_o), $endpos(_o) }
| _o = TK_GTGTGT { mk_asr', as_nexp, zero, $startpos(_o), $endpos(_o) }
;
%inline nnop:
| _o = TK_PLUS   { mk_sum', as_nexp, zero, $startpos(_o), $endpos(_o) }
| _o = TK_MINUS  { mk_sub', as_nexp, zero, $startpos(_o), $endpos(_o) }
| _o = TK_STAR   { mk_mul', as_nexp, zero, $startpos(_o), $endpos(_o) }
| _o = TK_SLASH  { mk_div', as_nexp, zero, $startpos(_o), $endpos(_o) }
;

pexpr:
| expr_ EOF { fst $1 }
;
expr:
| expr_ { flag' (Loc.make $startpos $endpos) (fst $1) }
;
expr_:
| prefix_expr      { $1 }
(* | e = expr; binop error maybe_eof { e } *)
(* | e = expr; nop   error maybe_eof { e } *)
| e1 = expr op = bbinop e2 = expr { mk_bbinop $startpos $endpos op e1 e2 }
| e1 = expr op = pbinop e2 = expr { mk_bbinop $startpos $endpos op e1 e2 }
| e1 = expr op = nbinop e2 = expr { mk_bbinop $startpos $endpos op e1 e2 }
| e1 = expr op = lbop   e2 = expr { mk_lbinop $startpos $endpos op e1 e2 }
| e1 = expr op = lsop   e2 = expr { mk_lsinop $startpos $endpos op e1 e2 }
| e1 = expr op = nnop   e2 = expr { mk_nbinop $startpos $endpos op e1 e2 }
;

prefix_expr:
| TK_SHARP TK_LPAREN expr TK_COMMA list_expr
    TK_RPAREN { flag' (Loc.make $startpos $endpos)
                  (mk_excl (fst $3) (List.map fst & List.rev $5)) }
| op = bunop exp = prefix_expr { mk_bunop $startpos $endpos op exp }
| op = nunop exp = prefix_expr { mk_nunop $startpos $endpos op exp }
| TK_IF expr TK_THEN expr TK_ELSE expr {
  (* match opt_bexp $startpos($2) $endpos($2) $2 with *)
  match select_exp as_bexp $2 with
    | None -> $4
    | Some c -> flag' (Loc.make $startpos $endpos)
        (mk_cond' c (fst $4) (fst $6))
}
| bint TK_LPAREN expr TK_RPAREN {
  let s, w = $1 in flag' (Loc.make $startpos $endpos) (mk_ncast s w (fst $3))
}
| suffix_expr          { $1 }
;
suffix_expr:
| atom { $1 }
| atom TK_IN TK_LBRACE list_expr TK_RBRACE {
  let l = List.rev_map fst $4 in
  flag' (Loc.make $startpos $endpos) (mk_in (fst $1) (List.hd l) (List.tl l))
}
;
atom:
| cst { $1 }
| id { flag' (snd $1) (mk_ref (fst $1)) }
| numcst id { flag' (Loc.make $startpos $endpos)
                (mk_mul (fst $1) (mk_ref (fst $2)) []) }
| TK_LPAREN expr TK_RPAREN { flag' (Loc.make $startpos $endpos)
                               (fst $2) }
| TK_LPAREN error maybe_eof TK_RPAREN {
  error $startpos($2) $endpos($2) "Expression@ expected";
    (mk_ref Symb.dummy, Loc.make $startpos $endpos)
}
;
boolcst:
| TK_TRUE  { flag' (Loc.make $startpos $endpos) tt }
| TK_FALSE { flag' (Loc.make $startpos $endpos) ff }
;
numcst:
| TK_NUM  { flag' (Loc.make $startpos $endpos) (mk_nicst $1) }
| TK_MPQF { flag' (Loc.make $startpos $endpos) (mk_nqcst $1) }
(*| bint TK_LPAREN          TK_NUM TK_RPAREN {
  let s, w = $1 in flag' (Loc.make $startpos $endpos) (mk_nbicst s w   $3)
}
| bint TK_LPAREN TK_MINUS TK_NUM TK_RPAREN {
  let s, w = $1 in flag' (Loc.make $startpos $endpos) (mk_nbicst s w (-$4))
  }*)
;
cst:
| boolcst  { $1 }
| numcst   { $1 }
;
list_expr:
| list_expr_strict { $1 }
| list_expr_strict TK_COMMA { $1 }
;
list_expr_strict:
| expr { [$1] }
| list_expr_strict TK_COMMA expr { $3::$1 }
;
