(** {2 Optimization Module Type } *)

type env_perm = int array
type t = CtrlSpec.t * Synthesis.controller * Program.cfprog_t ->
    (CtrlSpec.t * Synthesis.controller * Program.cfprog_t) * env_perm option

module type T =
  sig
    type param
    val optimize: param -> t
    val descr: param -> Format.formatter -> unit
    val short_descr: param -> Format.formatter -> unit
    val upgrade_param: param -> env_perm option -> param
  end
