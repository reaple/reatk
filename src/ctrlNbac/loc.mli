(* Interface documentation is in `loc.ml' only. *)
(**)

(* -------------------------------------------------------------------------- *)

type t
val compare: t -> t -> int
val make: Lexing.position -> Lexing.position -> t
val print: Format.formatter -> t -> unit

(* -------------------------------------------------------------------------- *)
