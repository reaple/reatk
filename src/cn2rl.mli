(* Interface documentation is in `cn2rl.ml' only. *)
(** *)

(* -------------------------------------------------------------------------- *)

type ('f, 'm) res =
  [
  | `TOk of 'm
  | `TKo of [ `TError of ('f option * (Format.formatter -> unit)) list ] list
  ]

type feature =
    [
    | `EnumDefault of CtrlNbac.AST.symb
    | `BintDefault of CtrlNbac.AST.symb
    | `Verb of string
    ]
exception Unsupported of feature

type static_variable_ordering_algorithm =
  | SVOBasic
  | SVOTopoBFS of static_variable_ordering_algorithm_basic_options
and static_variable_ordering_algorithm_basic_options =
    {
      svo_make_log_fmt: make_log_fmt_func option;
    }
and make_log_fmt_func = ?descr:Realib.Util.ufmt -> ?suff:string -> string
    -> Format.formatter * (unit -> unit)

type 'f local_defs = 'f CtrlNbac.AST.exp CtrlNbac.AST.SMap.t

val translate_node
  : ?max_cudd_mem: int64
  -> ?env_booking_factor: int
  -> ?env_cond_factor: int
  -> ?env_max_conds: int
  -> ?enable_primed_state_vars: bool
  -> ?enable_primed_input_vars: bool
  -> ?static_variable_ordering: static_variable_ordering_algorithm
  -> ?dynamic_variable_reordering: Realib.CuddUtil.reordering_algorithm
  -> ?strong_variable_affinities: CtrlNbac.AST.SSet.t list
  -> 'f CtrlNbac.AST.checked_node
  -> ('f, (Realib.Env.t * Realib.CtrlSpec.t * Realib.Program.dfprog_t
         * 'f local_defs)) res

val translate_cfg
  : ?max_cudd_mem: int64
  -> ?env_booking_factor: int
  -> ?env_cond_factor: int
  -> ?env_max_conds: int
  -> ?enable_primed_state_vars: bool
  -> ?enable_primed_input_vars: bool
  -> ?static_variable_ordering: static_variable_ordering_algorithm
  -> ?dynamic_variable_reordering: Realib.CuddUtil.reordering_algorithm
  -> ?strong_variable_affinities: CtrlNbac.AST.SSet.t list
  -> ?cdfequs_only: bool                     (* take care when turning that on *)
  -> 'f CtrlNbac.AST.checked_cfg
  -> ('f, (Realib.Env.t * Realib.CtrlSpec.t * Realib.Program.cfprog_t
         * 'f local_defs)) res

val translate_pred
  : ?max_cudd_mem: int64
  -> ?env_booking_factor: int
  -> ?env_cond_factor: int
  -> ?env_max_conds: int
  -> ?env: Realib.Env.t
  -> ?enable_primed_state_vars: bool
  -> ?enable_primed_input_vars: bool
  -> ?dynamic_variable_reordering: Realib.CuddUtil.reordering_algorithm
  -> 'f CtrlNbac.AST.checked_pred
  -> ('f, (Realib.Env.t * Realib.CtrlSpec.t * Realib.Env.boolexpr_t
         * 'f local_defs)) res

val translate_func
  : ?max_cudd_mem: int64
  -> ?env_booking_factor: int
  -> ?env_cond_factor: int
  -> ?env_max_conds: int
  -> ?env: Realib.Env.t
  -> ?enable_primed_state_vars: bool
  -> ?enable_primed_input_vars: bool
  -> ?dynamic_variable_reordering: Realib.CuddUtil.reordering_algorithm
  -> 'f CtrlNbac.AST.checked_func
  -> ('f, (Realib.Env.t * Realib.Env.equs_t list * Realib.Env.boolexpr_t
         * 'f local_defs)) res

(* -------------------------------------------------------------------------- *)

val translate_symb: CtrlNbac.Symb.t -> Realib.Env.Symb.t
val translate_label: CtrlNbac.AST.label -> Realib.Env.Symb.t
val translate_typ: CtrlNbac.AST.typ -> Realib.Env.typ

(** Exception raised by {translate_exp} for ill-typed expressions if
    [illtypedexn] is not given. *)
exception IllTypedExpression of string

type locals_repo = (CtrlNbac.AST.symb, Realib.Env.expr_t) Hashtbl.t
val translate_exp
  : ?illtypedexn:(?flag:'f -> 'f CtrlNbac.AST.exp -> exn)
  -> ?locals_repo: locals_repo
  -> Realib.Env.t
  -> 'f local_defs
  -> 'f CtrlNbac.AST.exp
  -> Realib.Env.expr_t

(* -------------------------------------------------------------------------- *)
