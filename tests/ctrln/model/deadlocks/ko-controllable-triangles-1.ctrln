(* `sS:d=I,split' succeeds but yields an invalid controller due to the
   convexity of Bad. `sD:d=I' fails indeed. *)

!typedef
  State = enum { Up1, Down1, Up2, Down2 };

!state
  s:State;
  x:int;

!input
  up, down, _c: bool;

!controllable
  c ? _c:bool;

!transition
  s' = if s in {Up1, Down1} and up then Up2 else
       if s in {Up2, Down2} and down then Up1 else
       if s = Up1 and c then Down1 else
       if s = Down1 and x <= 1 then Up1 else
       if s = Up2 and c then Down2 else
       if s = Down2 and x <= 0 then Up2 else
       s;
  x' = if up or down then 5 else
       if s in {Up1, Up2} then x+1 else x-1;

!assertion #(up, down);

!initial x = 5 and s = Up1;

!invariant 0 <= x and x <= 10;
