open Graph

module WeightedBFS
  (G: Sig.G)
  (W: Sig.ORDERED_TYPE with type t = G.E.label) =
struct
  open G

  type color =
    | White
    | Gray
    | Black

  let fold_all_paths vfilter f g =
    let vertex_color = Hashtbl.create (nb_vertex g) in
    let queue = Queue.create () in
    fold_vertex begin fun v0 ->
      if vfilter v0 then begin
        iter_vertex (fun v -> Hashtbl.add vertex_color v White) g;
        Queue.clear queue;
        Queue.add v0 queue;
        let rec visit order =
          if Queue.is_empty queue then order else
            let v = Queue.pop queue in
            Hashtbl.add vertex_color v Black;
            let successors =
              fold_succ_e begin fun e acc ->
                let u = E.dst e in
                if Hashtbl.find vertex_color u = White then begin
                  Hashtbl.add vertex_color u Gray;
                  (u, E.label e) :: acc
                end else acc
              end g v [] |> Array.of_list
            in
            Array.fast_sort (fun (_, w) (_, w') -> W.compare w' w) successors;
            Array.iter (fun (v, _) -> Queue.add v queue) successors;
            visit (v :: order)
        in
        visit [] |> List.rev |> f
      end else fun acc -> acc
    end g

end
