(* Optim goals like `o1:min y' should work. *)

!typedef
  S = enum { A, B, C, D };

!state
  s1, s2: S;
  x1, x2, y: int;

!input
  u1, u2: bool;
  _c1, _c2: bool;

!controllable
  c1 ? _c1, c2 ? _c2: bool;

!local
  t1, t2: S;
  y1, y2: int;

!definition
  t1 = if s1 = A then if u1 then B else C  else
       if s1 = B then if c1 then C else D  else
       if s1 = C then if c1 then D else s1 else
       if s1 = D then A                    else s1;
  t2 = if s2 = A then if u2 then B else C  else
       if s2 = B then if c2 then C else D  else
       if s2 = C then if c2 then D else s2 else
       if s2 = D then A                    else s2;
  y1 = if t1 = A then 0 else
       if t1 = B then 1 else
       if t1 = C then 2 else 3;
  y2 = if t2 = A then 0 else
       if t2 = B then 1 else
       if t2 = C then 2 else 3;

!transition
  s1 := t1;
  x1 := y1;
  s2 := t2;
  x2 := y2;
  y  := y1 + y2;

!initial
  s1 = A and x1 = 0 and s2 = A and x2 = 0 and y = 0;

!invariant
  not (s2 in {D});
