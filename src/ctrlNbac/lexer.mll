(* This file is part of ReaTK released under the GNU GPL.  Please read the
   LICENSE file packaged in the distribution *)

{
  open Format
  open Grammar
  open Lexing

  type msg =
    [
    | `LError of (Loc.t * (formatter -> unit)) list
    ]

  exception Lexical_error of msg list

  let make_loc lexbuf = Loc.make (lexeme_start_p lexbuf) (lexeme_end_p lexbuf)

  let lex_error lexbuf msg =
    raise (Lexical_error [
      `LError [ make_loc lexbuf, fun fmt -> pp_print_string fmt msg ]
    ])
  let lex_error' lst =
    raise (Lexical_error (List.map (fun l -> `LError [ l ]) lst))

  (*c Keywords *)
  let keywords = Hashtbl.create 53
  let _ =
    Array.iter
      (fun (keyword,token) -> Hashtbl.add keywords keyword token)
      [|
        ("!assertion", TK_ASSERTION);
        ("!attractive", TK_ATTRACTIVE);
        ("!automaton", TK_AUTOMATON);
        ("!controllable", TK_CONTR);
        ("!definition", TK_DEFINITION);
        ("!final", TK_FINAL);
        ("!initial", TK_INITIAL);
        ("!input", TK_INPUT);
        ("!invariant", TK_INVARIANT);
        ("!local", TK_LOCAL);
        ("!location", TK_LOCATION);
        ("!output", TK_OUTPUT);
        ("!reachable", TK_REACHABLE);
        ("!state", TK_STATE);
        ("!transition", TK_TRANS);
        ("!typedef", TK_TYPEDEF);
        ("!value", TK_VALUE);
        ("and", TK_AND);
        ("bool",TK_BOOL);
        ("else", TK_ELSE);
        ("enum",TK_ENUM);
        ("false",TK_FALSE);
        ("if", TK_IF);
        ("in",TK_IN);
        ("int",TK_INT);
        ("not", TK_NOT);
        ("or", TK_OR);
        ("real", TK_REAL);
        ("sint",TK_SINT);
        ("then", TK_THEN);
        ("true", TK_TRUE);
        ("uint",TK_UINT);
        (* ("eq", TK_EQ); *)
        (* ("xor", TK_NE); *)
      |]
}

rule lexer = parse
    eof    { EOF }
| [' ' '\t'] +  { lexer lexbuf }
| "\r\n" | ['\r' '\n'] | ['\n'] { new_line lexbuf; lexer lexbuf }

(* Delimiters *)
| ","   { TK_COMMA }
| ";"   { TK_SEMI }
| '''   { TK_PRIME }
(* | '.'   { TK_DOT } *)
| "("   { TK_LPAREN }
| ")"   { TK_RPAREN }
| "{"   { TK_LBRACE }
| "}"   { TK_RBRACE }
| "["   { TK_LBRACKET }
| "]"   { TK_RBRACKET }
| ":"   { TK_COLON }
| '?'   { TK_QMARK }
| "--"  { TK_DASH }
| "-->" { TK_RARROW }

(* Boolean operations *)
| "#"   { TK_SHARP }
| "=>"  { TK_RARROW2 }

(* Arithmetic operations *)
| "+"   { TK_PLUS }
| "-"   { TK_MINUS }
| "*"   { TK_STAR }
| "/"   { TK_SLASH }
| "|"   { TK_PIPE }
| "^"   { TK_HAT }
| "&"   { TK_AMP }
| "~"   { TK_TILDE }
| ">>"  { TK_GTGT }
| "<<"  { TK_LTLT }
| ">>>"  { TK_GTGTGT }
| "<<<"  { TK_LTLTLT }

(* Mixed operations *)
| "<>"  { TK_NE }
| ">"   { TK_GT }
| "<"   { TK_LT }
| ">="  { TK_GE }
| "<="  { TK_LE }
| "="   { TK_EQ }

(* Transition function assignment *)
| ":="   { TK_COLONEQ }

(* Numbers *)
| ['0'-'9']+
    { TK_NUM(int_of_string (lexeme lexbuf)) }
| ['0'-'9']+'/'['0'-'9']+
  {
    let str = lexeme lexbuf in
    TK_MPQF(Mpqf.of_string str)
  }
| ['0'-'9']* ('.' ['0'-'9']+) (['e' 'E'] ['+' '-']? ['0'-'9']+)?
  {
    let str = lexeme lexbuf in
    let str = String.uppercase_ascii str in
    let (eindex, exponent) =
      try
	let eindex = String.index str 'E' in
	let eindex1 =
	  if str.[eindex+1] = '+' then eindex+2 else eindex+1
	in
	let substr = String.sub str eindex1 (String.length str - eindex1) in
	(eindex, int_of_string substr)
      with Not_found ->
	(String.length str, 0)
    in
    let dotindex = String.index str '.' in
    let nbaftercomma = eindex - (dotindex+1) in
    let num1 = match dotindex with
      | 0 -> Mpzf.of_int 0
      | i -> Mpzf.of_string (String.sub str 0 i)
    in
    let num2 = Mpzf.of_string (String.sub str (dotindex+1) nbaftercomma) in
    let mpz = Mpz.init () in
    Mpz.ui_pow_ui mpz 10 nbaftercomma;
    let den = Mpzf.of_mpz mpz in
    let num = Mpzf.add (Mpzf.mul num1 den) num2 in
    let r = Mpqf.of_mpz2 num den in
    let exponent =
      if exponent = 0 then
	Mpqf.of_int 1
      else begin
	Mpz.ui_pow_ui mpz 10 (abs exponent);
	let exp = Mpzf.of_mpz mpz in
	if exponent > 0 then
	  Mpqf.of_mpz exp
	else
	  Mpqf.of_mpz2 (Mpzf.of_int 1) exp
      end
    in
    TK_MPQF(Mpqf.mul r exponent)
  }

(* Identifiers *)
| ['A'-'Z' 'a'-'z' '_' '@' '!'] ( ['_' '@' '!' 'A'-'Z' 'a'-'z' '.' '0'-'9'] ) *
    { let id = lexeme lexbuf in
      try Hashtbl.find keywords id with Not_found -> TK_ID (id) }
| "(*" {comment [ make_loc lexbuf ] lexbuf; lexer lexbuf}
| _ { lex_error lexbuf ("Unexpected character `"^lexeme lexbuf^"'") }

and comment starts = parse
| "*)" { match starts with a::b::tl -> comment (b::tl) lexbuf | _ -> () }
| "(*" { comment (make_loc lexbuf :: starts) lexbuf }
| '\r'? '\n' { new_line lexbuf; comment starts lexbuf }
| _    { comment starts lexbuf }
| eof  {
  lex_error' (List.rev_map (fun l -> l, fun fmt ->
    pp_print_string fmt "Unterminated comment") starts)
}
