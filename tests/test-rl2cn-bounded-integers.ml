open Rutils
open Realib
open Rl2cn

let () = Log.globallevel := Log.Debug3

let emptysmap = PMappe.empty Env.Symb.compare

let int = `Bint (true, 2)
let a = Env.Symb.mk "a"
and b = Env.Symb.mk "b"
and c = Env.Symb.mk "c"
and d = Env.Symb.mk "d"
and i = Env.Symb.mk "i"
let svars = [ a, int; d, int ]
and ivars = [ b, int; c, `Bool; i, int ]

let declvars = List.fold_left (fun m (v, t) -> Mappe.add v t m) Mappe.empty
let svardefs = declvars svars
let ivardefs = declvars ivars
let env = Env.make emptysmap svardefs ivardefs

module E: Env.T = (val Env.as_module env)
module NS = BddapronUtil.BEnvNS (E.BEnv)
(* module BOps = BddapronUtil.BOps (E.BEnv) *)
(* module PPrt = BddapronUtil.PPrt (E.BEnv) *)
module Expr = Bddapron.Expr0

open NS.BAll
let cs = CtrlSpec.make [ CtrlSpec.make_uc_group [i; b] [] [c, `Bool tt] []; ]
let equs, init, asser, invar =
  let open E.BEnv in
  let c = var c in
  let b = Expr.Bint.var env cond b in
  let n = Expr.Bint.of_int env cond int 0 in
  let b_gt_n = Expr.Bint.sup env cond b n in
  let d_eq_0 = Expr.Bint.zero env cond (Expr.Bint.var env cond d) in
  let a_eq_0 = Expr.Bint.zero env cond (Expr.Bint.var env cond a) in
  let equs = [
    a,
    (
      Expr.Bint.ite env cond c
      (* Expr.Bint.ite env cond (c ||~ b_gt_n) *)
        (Expr.Bint.var env cond a)
        (Expr.Bint.lnot env cond b)
       (* (expr.Bint.succ env cond b) *)
    ) |> Expr.Bint.to_expr;
    (* a, Expr.Bint.var env cond b |> Expr.Bint.to_expr *)
    d, Expr.Bint.of_int env cond int (-1) |> Expr.Bint.to_expr;
  ]
  and init = d_eq_0 &&~ !~a_eq_0
  and asser = b_gt_n
  and invar = !~a_eq_0
  in
  equs, init, asser, invar

let () =
  Format.printf "init: %a@." pp_bexpr init;
  Format.printf "equs: %a@." pp_equs equs;
  let df = Program.make_dfprog equs [] [] init !~invar asser ff ff in
  let n: _ CtrlNbac.AST.node = extract_ctrln env cs df in
  Format.printf "%a@." (fun fmt -> CtrlNbac.AST.print_node fmt) n;
  let _, errs = CtrlNbac.AST.check_node n in
  List.iter (CtrlNbac.Parser.Reporting.report_msg Format.err_formatter) errs;
  ()
