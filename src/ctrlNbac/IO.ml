(** This module constains basic helpers for reading Controllable-Nbac entities
    from external sources, and reporting errors *)

open Format
open Parser
open Reporting

(* -------------------------------------------------------------------------- *)

type msg =
  [
  | Parser.msg
  | `TError of (Loc.t option * (formatter -> unit)) list
  ]

(* -------------------------------------------------------------------------- *)

let report_msgs ?filename fmt msgs =
  let report_msg = report_msg ?filename fmt in
  List.iter begin function
    | #Parser.msg as msg -> report_msg msg
    | `TError info -> report_msg (`MError info)
  end msgs

let abort ?filename n msgs =
  report_msgs ?filename err_formatter msgs;
  eprintf "@[Aborting@ due@ to@ errors@ in@ %(%)@]@." n;
  exit 1

(* -------------------------------------------------------------------------- *)

(** Parses the given input file. *)
let parse_input
    ?(abort: ?filename:string -> _ format -> msg list -> _ = abort)
    ?filename
    (parse: ?filename:string -> _)
    =
  try
    let s, n, msgs = parse ?filename () in
    report_msgs ?filename err_formatter msgs;
    s, n
  with
    | Error (n, msgs) ->
        abort ?filename (Scanf.format_from_string n "") (msgs :> msg list)

let input_ctrln filename = parse_input ~filename parse_node
let input_ctrlr filename = parse_input ~filename parse_pred
let input_ctrlf filename = parse_input ~filename parse_func

(* -------------------------------------------------------------------------- *)
