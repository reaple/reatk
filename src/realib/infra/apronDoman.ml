open Format

(* --- *)

module type T = sig
  type dom
  val make: unit -> dom Apron.Manager.t
  val descr: formatter -> unit
end

module LoosePol: T = struct
  type dom = Polka.loose Polka.t
  let make = Polka.manager_alloc_loose
  let descr fmt = fprintf fmt "loose@ convex@ polyhedra"
end

module StrictPol: T = struct
  type dom = Polka.strict Polka.t
  let make = Polka.manager_alloc_strict
  let descr fmt = fprintf fmt "strict@ convex@ polyhedra"
end

module Oct: T = struct
  type dom = Oct.t
  let make = Oct.manager_alloc
  let descr fmt = pp_print_string fmt "octagons"
end

module Box: T = struct
  type dom = Box.t
  let make = Box.manager_alloc
  let descr fmt = pp_print_string fmt "boxes"
end

(* --- *)

type dim = Apron.Dim.t

module MakeNumDom (N: T) (M: sig
  val man: N.dom Apron.Manager.t
end) (BEnv: BddapronUtil.BDDAPRON_ENV): sig
  module POps: module type of BddapronUtil.POps (BEnv)
  module A0 = Apron.Abstract0
  open BEnv
  open POps
  type man = N.dom Apron.Manager.t
  type t = N.dom A0.t
  val man: man
  val copy: t -> t
  val hash: t -> int
  val canonicalize: t -> unit
  val print: formatter -> t -> unit
  val bottom: unit -> t
  val top: unit -> t
  val of_lincons_array: ApronUtil.lincons0 array -> t
  val to_lincons_array: t -> ApronUtil.lincons0 array
  val of_tcons_array: Apron.Tcons0.t array -> t
  val to_tcons_array: t -> Apron.Tcons0.t array
  val is_bottom: t -> bool
  val is_top: t -> bool
  val is_eq: t -> t -> bool
  val is_leq: t -> t -> bool
  val meet: t -> t -> t
  val meet1: t -> N.dom ApronUtil.num1 -> t
  val meet_condition': t -> Arith.Cond.t -> t
  val forget_list: t -> vars -> t
  val forget_list_with: t -> vars -> unit
  val project_list: t -> vars -> t
  val closure_with: t -> unit
  type bounds = Apron.Scalar.t * Apron.Scalar.t
  val var_bounds: t -> var -> bounds
  val lin_bounds: t -> ApronUtil.linexpr0 -> bounds
  val e_bounds: t -> Apron.Texpr0.t -> bounds
end = struct
  open Bddapron
  open Apron
  open BEnv
  module POps = BddapronUtil.POps (BEnv)
  open POps

  module A0 = Abstract0
  type man = N.dom Manager.t
  type t = N.dom A0.t

  include M

  let vardim v = Env.aprondim_of_var env v

  (* let man: man = N.make () *)

  let copy: t -> t = A0.copy man
  let hash: t -> int = A0.hash man
  let canonicalize: t -> unit = A0.canonicalize man
  let print: formatter -> t -> unit = fun fmt ->
    fprintf fmt "@<1>⟦@[%a@]@<1>⟧"
      (ApronUtil.pp_num0 (Env.string_of_aprondim env))

  let bld make = let dims = Environment.dimension (apronenv ()) in
                 make man dims.Dim.intd dims.Dim.reald
  let bottom () : t = bld A0.bottom
  let top () : t = bld A0.top
  let of_lincons_array = bld A0.of_lincons_array
  let to_lincons_array = A0.to_lincons_array man
  let of_tcons_array = bld A0.of_tcons_array
  let to_tcons_array = A0.to_tcons_array man

  let is_bottom: t -> bool = A0.is_bottom man
  let is_top: t -> bool = A0.is_top man
  let is_eq: t -> t -> bool = A0.is_eq man
  let is_leq: t -> t -> bool = A0.is_leq man

  let meet = A0.meet man
  let meet1 d l = ApronUtil.num0_of_num1 l |> meet d
  let meet_condition': t -> Arith.Cond.t -> t = fun d c ->
    A0.meet_tcons_array man d
      [| Apronexpr.Condition.to_tcons0 symbol (apronenv ()) c |]

  let vararr vars = Array.of_list (List.map vardim vars)
  let forget_list: t -> vars -> t = fun x vars ->
    A0.forget_array man x (vararr vars) false
  let forget_list_with: t -> vars -> unit = fun x vars ->
    A0.forget_array_with man x (vararr vars) false

  let project_list: t -> vars -> t = fun x vars ->
    A0.forget_array man x (vararr vars) true

  let closure_with =
    A0.closure_with man

  type bounds = Apron.Scalar.t * Apron.Scalar.t
  let var_bounds x v = let itv = A0.bound_dimension man x (vardim v) in
                       itv.Interval.inf, itv.Interval.sup
  let lin_bounds x e =
    let itv = A0.bound_linexpr man x e in
    itv.Interval.inf, itv.Interval.sup
  let e_bounds x e =
    let itv = A0.bound_texpr man x e in
    itv.Interval.inf, itv.Interval.sup
end
