(** Utilities for evaluation of expressions *)

open AST

module BintUtils = struct

  let min_bint_width s i =
    if i = 0 then 0 else
      let sign = i < 0 in
      let rec cont acc w =
        if (not sign && acc <= i) || (sign && acc < -i)
        then cont (acc lsl 1) (w + 1)
        else if sign then w + 1 else w
      in
      cont 1 0 + if s && not sign then 1 else 0

  exception ConstantIntegerTooLarge of bool * int * int

  let bndi w i : 'f nexp =
    let s = i < 0 in
    let mw = min_bint_width s i in
    if mw > w then raise (ConstantIntegerTooLarge (s, w, mw));
    (`Ncst (s, w, `Int i))

end

module type PARAMS = sig
  type flg
end

module Make (P: PARAMS) =  struct

  open P

  type bexp' = flg bexp
  type eexp' = flg eexp
  type nexp' = flg nexp
  type bexp = bexp'
  type eexp = eexp'
  type nexp = nexp'

  module Typ = struct
    type bint = bool * width * Mpz.t
    type ratio = Mpq.t
    type enum = label
    type _ p =
      | Bool : bool p
      | Enum : enum p
      | Bint : bint p
      | Ratio : ratio p
    type t =
      | Typ : 'a p -> t

    (**/**)

    let eq:
    type a b. a p -> b p -> bool = fun a b -> match a, b with
      | Bool, Bool -> true
      | Enum, Enum -> true
      | Bint, Bint -> true
      | Ratio, Ratio -> true
      | _ -> false

    (**/**)

    module Num = struct
      type _ p =
        | Bint : bint p
        | Ratio : ratio p
      type t = NumTyp : 'a p -> t
    end
  end

  module Val = struct
    open Typ
    open Typ.Num

    type (_,_) alt =
      | Bool : (bool, bexp) alt
      | Enum : (enum, eexp) alt
      | Bint : (bint, nexp) alt
      | Ratio : (ratio, nexp) alt

    type t =
      | Val : ('a, _) alt * 'a -> t
      | Let : ('a, 'b) alt * 'b -> t

    (**/**)

    let typ_of_alt (type a) (type e) : (a, e) alt -> a Typ.p = function
      | Bool -> Bool
      | Enum -> Enum
      | Bint -> Bint
      | Ratio -> Ratio

    (* Equality witness *)
    type (_,_,_,_) eq2 = Eq2 : ('a,'a,'b,'b) eq2
    let alt_eq (type a) (type e) (type a') (type e') :
        (a, e) alt -> (a', e') alt -> (a, a', e, e') eq2 option =
      fun a b -> match a, b with
        | Bool, Bool -> Some Eq2
        | Enum, Enum -> Some Eq2
        | Bint, Bint -> Some Eq2
        | Ratio, Ratio -> Some Eq2
        | _ -> None

    (**/**)

    let typof: t -> Typ.t = function
      | Val (t, _) -> Typ (typ_of_alt t)
      | Let (t, _) -> Typ (typ_of_alt t)

    module Num = struct

      type t = NVal :  'a Typ.Num.p * 'a -> t

      let typof: t -> Typ.Num.t = function
        | NVal (t, _) -> NumTyp t

      type typed =
        | Bint : bint -> typed
        | Ratio : ratio -> typed

      let typed: t -> typed = function
        | NVal (Bint, v) -> Bint v
        | NVal (Ratio, v) -> Ratio v
    end

    type typed =
      | Bool : bool -> typed
      | Enum : enum -> typed
      | Bint : bint -> typed
      | Ratio : ratio -> typed
      | Bexp : bexp -> typed
      | Eexp : eexp -> typed
      | Nexp : nexp -> typed

    let typed: t -> typed = function
      | Val (Bool, v) -> Bool v
      | Val (Enum, v) -> Enum v
      | Val (Bint, v) -> Bint v
      | Val (Ratio, v) -> Ratio v
      | Let (Bool, v) -> Bexp v
      | Let (Enum, v) -> Eexp v
      | Let (Bint, v) -> Nexp v
      | Let (Ratio, v) -> Nexp v
  end

  (** Typed environment *)
  module Env = struct

    open Typ
    open Val

    type typ = Typ.t
    type value = Val.t
    type t = value SMap.t

    let empty: t = SMap.empty
    let add: symb -> value -> t -> t = SMap.add
    let rem: symb -> t -> t = SMap.remove
    let find: symb -> t -> value option = fun s e ->
      try Some (SMap.find s e) with Not_found -> None
    let find_typed: symb -> t -> Val.typed option = fun s e ->
      try Some (SMap.find s e |> typed) with Not_found -> None

    type (_,_) binding =
      | Constant : 'a -> ('a, _) binding
      | LetBind : ('a, 'b) alt * 'b -> ('a, 'b) binding
      | IllBound : _ binding
      | Unbound : _ binding

    type bool_binding = (bool, bexp) binding
    type enum_binding = (enum, eexp) binding
    type bint_binding = (bint, nexp) binding
    type ratio_binding = (ratio, nexp) binding

    let get (type a) (type e) : (a, e) alt -> symb -> t -> (a, e) binding =
      begin fun t s e -> match SMap.find s e with
        | Val (t', x) ->
            (match alt_eq t t' with Some Eq2 -> Constant x | _ -> IllBound)
        | Let (t', x) ->
            (match alt_eq t t' with Some Eq2 -> LetBind (t', x) | _ -> IllBound)
        | exception Not_found -> Unbound
      end

    let get_bool: symb -> t -> bool_binding = get Bool
    let get_enum: symb -> t -> enum_binding = get Enum
    let get_bint: symb -> t -> bint_binding = get Bint
    let get_ratio: symb -> t -> ratio_binding = get Ratio

    let bind_cst (type a) (type e) : (a, e) alt -> symb -> a -> t -> t =
      fun t s x -> SMap.add s (Val (t, x))

    let bind_let (type a) (type e) : (a, e) alt -> symb -> e -> t -> t =
      fun t s x -> SMap.add s (Let (t, x))

    let bind_bool: symb -> bool -> t -> t = bind_cst Bool
    let bind_enum: symb -> enum -> t -> t = bind_cst Enum
    let bind_bint: symb -> bint -> t -> t = bind_cst Bint
    let bind_ratio: symb -> ratio -> t -> t = bind_cst Ratio

    let bind_bexp: symb -> bexp -> t -> t = bind_let Bool
    let bind_eexp: symb -> eexp -> t -> t = bind_let Enum
    let bind_biexp: symb -> nexp -> t -> t = bind_let Bint
    let bind_qexp: symb -> nexp -> t -> t = bind_let Ratio

  end

  exception Unsupported of flg exp * flg option
  exception IllTyped of flg option
  exception Unbound of flg option * symb

  open BintUtils
  open Typ
  open Val
  open Env

  type evals =
      {
        bcst: Env.t -> ?flag:flg -> bexp -> bool;
        ecst: Env.t -> ?flag:flg -> eexp -> enum;
        bicst: Env.t -> ?flag:flg -> nexp -> bint;
        qcst: Env.t -> ?flag:flg -> nexp -> ratio;
        ncst: Env.t -> ?flag:flg -> nexp -> Num.typed;
      }

  (**/**)

  include struct
    open Num

    let ncmp ?flag (o: totrel) a b : bool = match o, a, b with
      | `Eq, Ratio a, Ratio b -> Mpq.cmp a b = 0
      | `Ne, Ratio a, Ratio b -> Mpq.cmp a b <> 0
      | `Lt, Ratio a, Ratio b -> Mpq.cmp a b < 0
      | `Gt, Ratio b, Ratio a -> Mpq.cmp a b < 0
      | `Le, Ratio a, Ratio b -> Mpq.cmp a b <= 0
      | `Ge, Ratio b, Ratio a -> Mpq.cmp a b <= 0
      | _, Bint (s, w, _), Bint (s', w', _) when s <> s' || w <> w' ->
          raise (IllTyped flag)
      | `Eq, Bint (s, w, i), Bint (_, _, j) -> Mpz.cmp i j = 0
      | `Ne, Bint (s, w, i), Bint (_, _, j) -> Mpz.cmp i j <> 0
      | `Lt, Bint (_, _, i), Bint (_, _, j) -> Mpz.cmp i j < 0
      | `Gt, Bint (_, _, j), Bint (_, _, i) -> Mpz.cmp i j < 0
      | `Le, Bint (_, _, i), Bint (_, _, j) -> Mpz.cmp i j <= 0
      | `Ge, Bint (_, _, j), Bint (_, _, i) -> Mpz.cmp i j <= 0
      | _ -> raise (IllTyped flag)

    let birepr i : typed =
      let s = i < 0 in
      let mw = min_bint_width s i in
      Bint (s, mw, Mpz.of_int i)

    let bicast ?flag s w : _ -> bint = function
      | Ratio _ -> raise (IllTyped flag)
      | Bint (_, w', i) when w >= w' -> (s, w, i)                    (* w ≥ w' *)
      | Bint (true, _, i) -> Mpz.tdiv_r_2exp i i w; (s, w, i)       (* s, w < w' *)
      | Bint (false, _, i) -> Mpz.fdiv_r_2exp i i w; (s, w, i)      (* u, w < w' *)

    let bineg ?flag (s, w, i) : bint =
      Mpz.neg i i;
      if s then s, w, i else bicast ?flag false w (Num.Bint (s, w, i))

    let binot ?flag (s, w, i) : bint =
      Mpz.neg i i;
      Mpz.add_ui i i 1;
      bicast ?flag s w (Num.Bint (s, w, i))

    (* --- *)

    let biuop: [< nuop | luop] -> ?flag:flg -> bint -> bint = function
      | `Opp -> bineg
      | `LNot -> binot

    let bibop: [< nnop | lbop] -> ?flag:flg -> bint -> bint -> bint =
      fun op ?flag (s1, w1, i1) (s2, w2, i2) ->
        assert (s1 = s2 && w1 = w2);
        begin match op with
          | `Sum -> Mpz.add
          | `Sub -> Mpz.sub
          | `Mul -> Mpz.mul
          | `Div -> Mpz.tdiv_q
          | `LAnd -> Mpz.gand
          | `LOr -> Mpz.ior
          | `LXor -> Mpz.xor
        end i1 i1 i2;
        bicast ?flag s1 w1 (Num.Bint (s1, w1, i1))

    (* --- *)

    let quop = let op op ?flag e = op e e; e in function
      | `Opp -> op Mpq.neg

    let qnop = let op op ?flag e f = op e e f; e in function
      | `Sum -> op Mpq.add | `Sub -> op Mpq.sub
      | `Mul -> op Mpq.mul | `Div -> op Mpq.div

  end

  (* --- *)

  let find_typed (type a) (type e) (t: (a, e) Val.alt)
      { bcst; ecst; bicst; qcst } env ?flag s : a =
    match Env.get t s env with
      | Constant x -> x
      | LetBind (Bool, e) -> bcst env e
      | LetBind (Enum, e) -> ecst env e
      | LetBind (Bint, e) -> bicst env e
      | LetBind (Ratio, e) -> qcst env e
      | IllBound -> raise (IllTyped flag)
      | Unbound -> raise (Unbound (flag, s))

  let find_bool = find_typed Bool
  let find_ecst = find_typed Enum
  let find_qcst = find_typed Ratio
  let find_bicst = find_typed Bint

  let find_ncst { ncst } env ?flag s = match Env.find s env with
    | Some v -> (match Val.typed v with
        | Bint v -> Num.Bint v
        | Ratio v -> Num.Ratio v
        | Nexp e -> ncst env e
        | _ -> raise (IllTyped flag))
    | None -> raise (Unbound (flag, s))

  (* --- *)

  let nnop' typedcst typedop ?flag v l =
    List.fold_left (fun e x -> typedop ?flag e (typedcst x)) v l

  let rec bcst ({ ecst; bicst; ncst } as evals) env ?flag e =
    let open List in
    let ecst = ecst env and bicst = bicst env and ncst = ncst env in
    let rec bcst ?flag : bexp -> bool = fun e -> match e with
      | `Ref r -> find_bool evals env ?flag r
      | `Bool b -> b
      | `Buop (`Neg, e) -> not (bcst e)
      | `Bbop (`Imp, e, f) -> not (bcst e) || (bcst f)
      | `Bnop (`Conj, e, f, l) -> for_all bcst (e::f::l)
      | `Bnop (`Disj, e, f, l) -> exists bcst (e::f::l)
      | `Bnop (`Excl, _, _, _) as e -> raise (Unsupported (`Bexp e, flag))
      | `Bcmp (`Eq, e, f) -> bcst e == bcst f
      | `Bcmp (`Ne, e, f) -> bcst e != bcst f
      | `Ecmp (`Eq, e, f) -> ecst e == ecst f
      | `Ecmp (`Ne, e, f) -> ecst e != ecst f
      | `Bin (e, f, l) -> let e = bcst e in exists (fun f -> bcst f == e) (f::l)
      | `Ein (e, f, l) -> let e = ecst e in exists (fun f -> ecst f == e) (f::l)
      | `BIin (e, f, l) -> let e = bicst e in exists (fun f -> bicst f == e) (f::l)
      | `Ncmp (o, e, f) -> ncmp ?flag o (ncst e) (ncst f)
      | `Ite (c, t, e) -> if bcst c then bcst t else bcst e
      | #flag as f -> apply' bcst f
      | `Pin _ | `Pcmp _ -> raise (Unsupported (`Bexp e, flag))
      | exception Not_found -> raise (Unsupported (`Bexp e, flag))
    in
    bcst ?flag e
  and ecst ({ bcst } as evals) env ?flag e =
    let rec ecst ?flag : eexp -> enum = function
      | `Ref r -> find_ecst evals env ?flag r
      | `Enum l -> l
      | `Ite (c, t, e) -> if bcst env c then ecst t else ecst e
      | #flag as f -> apply' ecst f
    in
    ecst ?flag e
  and qcst ({ bcst; ncst } as evals) env ?flag e =
    let open Num in
    let rec qcst ?flag : nexp -> ratio = function
      | `Ref r -> find_qcst evals env ?flag r
      | `Int i -> Mpq.of_int i
      | `Real r -> Mpq.of_float r
      | `Mpq q -> Mpqf.to_mpq q
      | `Ite (c, t, e) -> if bcst env c then qcst t else qcst e
      | #flag as f -> apply' qcst f
      | e -> match ncst env ?flag e with Ratio r -> r | _ -> raise (IllTyped flag)
    in
    qcst ?flag e
  and bicst ({ bcst; ncst } as evals) env ?flag e =
    let open Num in
    let rec bicst ?flag : nexp -> bint = function
      | `Ref r -> find_bicst evals env ?flag r
      | `Ncst (s, w, `Int i) -> bicast ?flag s w (birepr i)
      | `Ite (c, t, e) -> if bcst env c then bicst t else bicst e
      | #flag as f -> apply' bicst f
      | e -> match ncst env ?flag e with Bint i -> i | _ -> raise (IllTyped flag)
    in
    bicst ?flag e
  and ncst ({ bcst; bicst; qcst } as evals) env ?flag e =
    let open Num in
    let bcst = bcst env and bicst = bicst env and qcst = qcst env in
    let rec ncst ?flag : nexp -> Val.Num.typed = function
      | `Ref r -> find_ncst evals env ?flag r
      | `Int i -> Ratio (Mpq.of_int i)
      | `Real r -> Ratio (Mpq.of_float r)
      | `Mpq q -> Ratio (Mpqf.to_mpq q)
      | `Ncst _ as e -> Bint (bicst ?flag e)
      | `Nuop (o, e) -> nuop ?flag o e
      | `Nnop (o, e, f, l) -> nnop ?flag o e (f::l)
      | `Luop (o, e) -> luop ?flag o e
      | `Lbop (o, e, f) -> lnop ?flag o e [f]
      | `Ite (c, t, e) -> if bcst c then ncst t else ncst e
      | #flag as f -> apply' ncst f
      | `Lsop _ as e -> raise (Unsupported (`Nexp e, flag))
    and nuop ?flag op e = match ncst e, op with
      | Ratio v, o -> Ratio (quop o ?flag v)
      | Bint v, o -> Bint (biuop o ?flag v)
    and nnop ?flag op e l = match ncst e, op with
      | Ratio v, o -> Ratio (nnop' qcst (qnop o) ?flag v l)
      | Bint v, o -> Bint (nnop' bicst (bibop o) ?flag v l)
    and luop ?flag op e = match ncst e, op with
      | Ratio v, o -> raise (IllTyped flag)
      | Bint v, o -> Bint (biuop o ?flag v)
    and lnop ?flag op e l = match ncst e, op with
      | Ratio v, o -> raise (IllTyped flag)
      | Bint v, o -> Bint (nnop' bicst (bibop o) ?flag v l)
    in
    ncst ?flag e

  (* --- *)

  let neg_label typdefs labeltyp l _ = match labeltyp l with
    | `Enum tn -> (match find_typdef tn typdefs with
        | EnumDef [a, _; b, _], _ when l = a -> Constant b
        | EnumDef [a, _; b, _], _ when l = b -> Constant a
        | _ -> IllBound)
    | _ -> IllBound

  (**/**)

  let rec evals =
    {
      bcst = (fun x -> bcst evals x);
      ecst = (fun x -> ecst evals x);
      bicst = (fun x -> bicst evals x);
      qcst = (fun x -> qcst evals x);
      ncst = (fun x -> ncst evals x);
    }

  exception NonConstant of flg exp * flg option
  exception BadConj of (* exp option * flg option *)bexp

  (* --- *)

  let try_assign_bint env v ~mask value =
    match evals.ncst env mask, evals.ncst env value with
      | Num.Ratio _, _ | _, Num.Ratio _ -> raise Exit
      | Num.Bint mask, Num.Bint value ->
          let newbits = bibop `LAnd mask value in
          let oldbits =
            try bibop `LAnd (biuop `LNot mask) (find_bicst evals env v) with
              | Unbound _ -> newbits in
          Env.bind_bint v (bibop `LOr newbits oldbits) env

  (**/**)

  let deflag_bexp = deflag_bexp ~max_depth:3
  let deflag_eexp = deflag_eexp ~max_depth:3
  let deflag_nexp = deflag_nexp ~max_depth:2

  let fold_assign_conj
      (pos: symb -> _ -> bool_binding)
      (negl: label -> _ -> enum_binding)
      (assign_bint: 'a -> symb -> mask: nexp -> nexp -> 'a)
      assign
      =
    let dflb = deflag_bexp and dfle = deflag_eexp and dfln = deflag_nexp in
    let rec fold ?(first = false) : 'a -> bexp -> 'a = fun acc -> function
      | `Bool true -> acc
      | `Ref v as e -> (match pos v acc with
          | Constant b -> assign acc v (`Bexp (`Bool b))
          | Unbound -> assign acc v (`Bexp (`Bool true))
          | LetBind (Bool, e) -> fold' acc e
          | IllBound -> raise (BadConj e)
          | _ -> .)
      | `Buop (`Neg, `Ref v) -> assign acc v (`Bexp (`Bool false))
      | `Buop (`Neg, (`Ecmp (`Eq, `Ref v, `Enum l)
                         | `Ecmp (`Eq, `Enum l, `Ref v))) as e ->
          (match negl l acc with
            | Constant l' -> assign acc v (`Eexp (`Enum l'))
            | _ -> raise (BadConj e))
      | `Ncmp (`Eq, `Lbop (`LAnd, `Ref v, mask), value)
      | `Ncmp (`Eq, value, `Lbop (`LAnd, `Ref v, mask)) as e ->
          (try assign_bint acc v ~mask value with
            | Exit -> raise (BadConj e))
      | `Bnop (`Conj, e, f, l) -> List.fold_left fold' acc (e::f::l)
      | `Bcmp (`Eq, `Ref v, f) | `Bcmp (`Eq, f, `Ref v) -> assign acc v (`Bexp f)
      | `Ecmp (`Eq, `Ref v, f) | `Ecmp (`Eq, f, `Ref v) -> assign acc v (`Eexp f)
      | `Ncmp (`Eq, `Ref v, f) | `Ncmp (`Eq, f, `Ref v) -> assign acc v (`Nexp f)
      | `Buop (`Neg, e) when first -> fold acc (`Buop (`Neg, dflb e))
      | `Bcmp (`Eq, e, f) when first -> fold acc (`Bcmp (`Eq, dflb e, dflb f))
      | `Ecmp (`Eq, e, f) when first -> fold acc (`Ecmp (`Eq, dfle e, dfle f))
      | `Ncmp (`Eq, e, f) when first -> fold acc (`Ncmp (`Eq, dfln e, dfln f))
      | #flag as e -> fold' acc (dflb e)
      | e -> Format.eprintf "Bad conj: %a@." print_bexp e; raise (BadConj e)
    and fold' acc = fold ~first:true acc in
    fold'

  (**/**)

  (** [bind_var symbtyp env binding] adds [binding] into [env], relying on
      [symbtyp] to retrieve variables types. *)
  let bind_var symbtyp env (v, e) = match symbtyp v, deflag e with
    | `Bool, `Bexp e -> Env.bind_bexp v e env
    | `Enum _, `Eexp e -> Env.bind_eexp v e env
    | `Bint _, `Nexp e -> Env.bind_biexp v e env
    | #ntyp, `Nexp e -> Env.bind_qexp v e env
    | _ -> raise (IllTyped (flagof e))

  (** [bind symbtyp env bindings] adds [bindings] into [env], relying on
      [symbtyp] to retrieve variables types. *)
  let bind symbtyp = List.fold_left (bind_var symbtyp)

  (** [const_assignments typdefs labeltyp env b] "tries to" interprets [b] as a
      conjunction of equalities between variables and constants, and augment
      [env] accordingly.  Raises {!BadConj} if it fails or if [b] comprises
      non-constant equalities. *)
  let const_assignments typdefs labeltyp =
    let { bcst; ecst; ncst } = evals in
    let assign env v = function
      | `Bexp e -> Env.bind_bool v (bcst env e) env
      | `Eexp e -> Env.bind_enum v (ecst env e) env
      | `Nexp e -> match ncst env e with
          | Num.Bint bi -> Env.bind_bint v bi env
          | Num.Ratio r -> Env.bind_ratio v r env
    in
    fold_assign_conj Env.get_bool (neg_label typdefs labeltyp)
      try_assign_bint
      assign

  (** [assignments typdefs symbtyp labeltyp env b] "tries to" interprets [b] as
      a conjunction of equalities between variables and any other expression (on
      inputs hopefuly), and augment [env] accordingly.  Relies on [symbtyp] to
      retrieve variable types.  Raises {!BadConj} if it fails. *)
  let assignments typdefs symbtyp labeltyp =
    fold_assign_conj Env.get_bool (neg_label typdefs labeltyp)
      try_assign_bint
      (fun env v e -> bind_var symbtyp env (v, e))

end
