(******************************************************************************)
(* parseParams *)
(* utilities to parse parameters and options *)
(* author: Peter Schrammel *)
(* version: 0.9.0 *)
(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)
(******************************************************************************)

type options_t = (string, string) Mappe.t
type param_t = string * options_t
type param_ordrd_t = string * (string list * options_t)

let parse_param str =
  Parse.parse_string ~lexer:LexerParam.param' ~parser:ParserParam.param str

let parse_param' str =
  Parse.parse_string ~lexer:LexerParam.param' ~parser:ParserParam.param_ordrd str

let parse_options str =
  Parse.parse_string ~lexer:LexerParam.param' ~parser:ParserParam.options_opt str

let get_option options defaults key converter =
  try converter (Mappe.find key options)
  with Not_found -> converter (Mappe.find key defaults)

let get_option' options key converter =
  try Some (converter (Mappe.find key options))
  with Not_found -> None

exception InvalidBooleanSpecification of string
let bool_option = function
  | "true" | "t" | "T" | "" -> true
  | "false" | "f" | "F" -> false
  | s -> raise (InvalidBooleanSpecification s)

exception InvalidRationalSpecification of string
let rational_option s =
  try Mpqf.of_string s with
    | Invalid_argument _ -> try float_of_string s |> Mpqf.of_float with
        | Failure _ -> raise (InvalidRationalSpecification s)

let string_option = function
  | "" -> raise Not_found
  | s -> s

let semicolon_str_to_strlist = Str.split (Str.regexp_string ";")
let comma_str_to_strlist = Str.split (Str.regexp_string ",")

let check_extra_options logger n defaults ?(optionals=[]) options =
  let opts = Mappe.maptoset defaults |> List.fold_right Sette.add optionals in
  let extra = Mappe.diffset options opts in
  Mappe.iter (fun k -> function
    | "" -> Log.w logger "Ignored option for `%s': %s" n k
    | v  -> Log.w logger "Ignored option for `%s': %s=%s" n k v) extra
