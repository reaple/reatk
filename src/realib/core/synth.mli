(** *)

(* -------------------------------------------------------------------------- *)

(** {2 Over-approximating synthesis} *)

type std_param_t
val make_std_param
  : ws:int
  -> wd:int
  -> ?fwcs:int
  -> split_convex:bool
  -> split_outer:bool
  -> deads:bool
  -> bdisj:bool
  -> ?initonly: bool
  -> no_bnd:bool
  -> oldassert:bool
  -> std_param_t

module Std (Dom: Domain.T) :
  (Synthesis.T with type param = std_param_t)

(* -------------------------------------------------------------------------- *)

(** {2 Co-reachability analysis with controllable variables (for convenience)} *)

type coreach_param_t
val make_coreach_param
  : ws:int
  -> wd:int
  -> ?fwcs:int
  -> split_convex:bool
  -> split_outer:bool
  -> deads:bool
  -> bdisj:bool
  -> oldassert:bool
  -> coreach_param_t

module type COREACH = sig
  val apply
    : coreach_param_t
    -> CtrlSpec.t * Synthesis.controller * Program.cfprog_t
    -> Env.boolexpr_t
end

module Coreach (Dom: Domain.T) : COREACH

(* -------------------------------------------------------------------------- *)

(** {2 Over-approximating synthesis; single-fixpoint (data-flow-based)} *)

type sfp_param_t
val make_sfp_param
  : ws:int
  -> split_convex:bool
  -> deads:bool
  -> bdisj:bool
  -> no_bnd:bool
  -> oldassert:bool
  -> sfp_param_t

module Sfp (Dom: Domain.T) :
  (Synthesis.T with type param = sfp_param_t)

(* -------------------------------------------------------------------------- *)

(** {2 Deadlock-avoiding Over-approximating synthesis} *)

type df_param_t
val make_df_param
  : ws:int
  -> wd:int
  -> ?fwcs:int
  -> ws2:int
  -> emws:int
  -> pwext:bool
  -> deads:bool
  -> no_bnd:bool
  -> oldassert:bool
  -> df_param_t

module type DF_FP_PARAMS = DomExt.FP_PARAMS

module Df (Dom: Domain.T) (Params: DF_FP_PARAMS) :
  (Synthesis.T with type param = df_param_t)

(* -------------------------------------------------------------------------- *)

(** {2 Boolean synthesis} *)

type bool_param_t
val make_bool_param
  : bool
  -> Env.boolexpr_t option
  -> bool
  -> Env.boolexpr_t option
  -> deads:bool
  -> no_bnd:bool
  -> rp:BddapronReorderPolicy.config
  -> oldassert:bool
  -> bool_param_t

module Bool (E: Env.T) : (Synthesis.T0 with type param = bool_param_t)

(* -------------------------------------------------------------------------- *)

(** {2 Relational Boolean synthesis (SCT-inspired)} *)

type relb_param_t
val make_relb_param
  : ?dirty: bool
  -> ?decomp: [ `Disj ]
  -> ?check: bool
  -> rp: BddapronReorderPolicy.config
  -> reach: bool
  -> oldassert: bool
  -> no_bnd: bool
  -> relb_param_t

module RelBool (E: Env.T) : (Synthesis.T0 with type param = relb_param_t)

(* -------------------------------------------------------------------------- *)

(** {2 Limited lookahead synthesis} *)

type lla_param_t
val make_lla_param
  : ?dirty:bool
  -> ?ndr:bool
  -> ?best_effort:bool
  -> ?recov:int
  -> ?defer:[ `None | `Grow of bool | `Shrink ]
  -> int
  -> lla_param_t

module LLA (E: Env.T) : (Synthesis.T0 with type param = lla_param_t)

(* -------------------------------------------------------------------------- *)
