val make_man: ?cache_factor:int -> ?max_mem:int64 -> unit -> Cudd.Man.vt
val print_limit: int ref
val fullcheck: 'a Bddapron.Env.t -> bool
val clear: 'a Bddapron.Env.t -> 'a Bddapron.Cond.t -> unit

type reordering_algorithm = Cudd.Man.reorder

module Options: sig
  (** forbids any reordering of variables *)
  val enable_reordering : bool ref

  (** allows dynamical/asynchronous reordering of variables *)
  val enable_dynamic_reordering : bool ref

  val reorder_policy: reordering_algorithm ref
end

val reorder: 'a Bddapron.Env.t -> unit
val reorder_conv: 'a Bddapron.Env.t -> unit
val start_dynamic_reordering': Cudd.Man.vt -> unit
val start_dynamic_reordering: 'a Bddapron.Env.t -> unit
val stop_dynamic_reordering': Cudd.Man.vt -> unit
val stop_dynamic_reordering: 'a Bddapron.Env.t -> unit

type dynamic_reordering_status
val dynamic_reordering_status': Cudd.Man.vt -> dynamic_reordering_status
val dynamic_reordering_status: 'a Bddapron.Env.t -> dynamic_reordering_status
val dynamic_reordering_save_n_select: 'a Bddapron.Env.t -> reordering_algorithm -> dynamic_reordering_status
val dynamic_reordering_save_n_stop: 'a Bddapron.Env.t -> dynamic_reordering_status
val dynamic_reordering_save_n_stop_if: bool -> 'a Bddapron.Env.t -> dynamic_reordering_status
val dynamic_reordering_restore: 'a Bddapron.Env.t -> dynamic_reordering_status -> unit
val dynamic_reordering_select1: 'a Bddapron.Env.t -> reordering_algorithm -> ('b -> 'c) -> 'b -> 'c
val dynamic_reordering_activate1: 'a Bddapron.Env.t -> ('b -> 'c) -> 'b -> 'c
val dynamic_reordering_suspend1: 'a Bddapron.Env.t -> ('b -> 'c) -> 'b -> 'c
val dynamic_reordering_suspend2: 'a Bddapron.Env.t -> ('b -> 'c -> 'd) -> 'b -> 'c -> 'd
val print_dynamic_reordering_status: Format.formatter -> dynamic_reordering_status -> unit

val enable_reordering: 'a Bddapron.Env.t -> unit
val disable_reordering: 'a Bddapron.Env.t -> unit

type reordering_status
val reordering_status': Cudd.Man.vt -> reordering_status
val reordering_status: 'a Bddapron.Env.t -> reordering_status
val reordering_save_n_disable: 'a Bddapron.Env.t -> reordering_status
val reordering_save_n_enable: 'a Bddapron.Env.t -> reordering_status
val reordering_save_n_disable_if: bool -> 'a Bddapron.Env.t -> reordering_status
val reordering_save_n_enable_if: bool -> 'a Bddapron.Env.t -> reordering_status
val reordering_restore: 'a Bddapron.Env.t -> reordering_status -> unit
val reordering_suspend1: 'a Bddapron.Env.t -> ('b -> 'c) -> 'b -> 'c
val reordering_suspend2: 'a Bddapron.Env.t -> ('b -> 'c -> 'd) -> 'b -> 'c -> 'd
val reordering_suspend1_if: bool -> 'a Bddapron.Env.t -> ('b -> 'c) -> 'b -> 'c
val reordering_suspend2_if: bool -> 'a Bddapron.Env.t -> ('b -> 'c -> 'd) -> 'b -> 'c -> 'd
val reordering_activate1: 'a Bddapron.Env.t -> ('b -> 'c) -> 'b -> 'c
val reordering_activate2: 'a Bddapron.Env.t -> ('b -> 'c -> 'd) -> 'b -> 'c -> 'd
val reordering_activate1_if: bool -> 'a Bddapron.Env.t -> ('b -> 'c) -> 'b -> 'c
val reordering_activate2_if: bool -> 'a Bddapron.Env.t -> ('b -> 'c -> 'd) -> 'b -> 'c -> 'd
val print_reordering_status: Format.formatter -> reordering_status -> unit

val print_stats: 'a Bddapron.Env.t -> unit
val print_params: Format.formatter -> 'a Bddapron.Env.t -> unit

(* --- *)

(** {2 BDD Decompositions} *)

module Decomp: sig

  type kind =
    | Disjunctive
    | Conjunctive

  type algorithm =
    | VariableBased
    | Generalized
    | Iterative

  val count
    : kind: kind
    -> algorithm: algorithm
    -> ?stop: ('d Cudd.Bdd.t -> bool)
    -> 'd Cudd.Bdd.t -> int

  val to_list
    : kind: kind
    -> algorithm: algorithm
    -> ?stop: ('d Cudd.Bdd.t -> bool)
    -> 'd Cudd.Bdd.t -> 'd Cudd.Bdd.t list

  val fold
    : kind: kind
    -> algorithm: algorithm
    -> ?stop: ('d Cudd.Bdd.t -> bool)
    -> ('d Cudd.Bdd.t -> 'a -> 'a) -> 'd Cudd.Bdd.t -> 'a -> 'a

end

(* --- *)
