open BddapronUtil
open Format

let logger = Log.mk ~level:Log.Debug3 ".L"

module Lookahead = functor (E: Env.T) -> struct
  open E.BEnvNS
  open E.BEnvNS.PAll
  open E.BEnvNS.BEnvExt

  (* --- *)

  (** Debug utility to help detecting extraneous lookaheads w.r.t
      transitions. *)
  let prime_oflw = false

  (* --- *)

  let prime_cvars cs varsreq =
    List.fold_left begin fun acc v ->
      try PMappe.add v (PMappe.find v acc |> fst, true) acc with
        | Not_found -> acc
    end varsreq (CtrlSpec.c_vars cs)

  (* --- *)

  let prime_uvars cs varsreq =
    List.fold_left begin fun acc v ->
      try PMappe.add v (PMappe.find v acc |> fst, true) acc with
        | Not_found -> acc
    end varsreq (CtrlSpec.u_vars cs)

  (* --- *)

  (* let u0vars cs varsreq = *)
  (*   List.fold_left begin fun acc v -> *)
  (*     if PMappe.mem v varsreq then PSette.add v acc else acc *)
  (*   end empty_vset (CtrlSpec.u_vars cs) *)

  (* --- *)

  (* let kdeps ?(not_in=empty_vset) evols svars k = *)
  (*   assert (k >= 0); *)
  (*   let supports = evols_supports evols in *)
  (*   let maybe_add v m = *)
  (*     PMappe.add v (try PMappe.find v m |> succ with Not_found -> 0) m in *)
  (*   let rec unroll vcnt vars = function *)
  (*     | 0 -> PSette.fold maybe_add svars vcnt *)
  (*     | i -> let deps = PMappe.interset supports vars in *)
  (*           let vars = PMappe.fold (fun _ -> PSette.union) deps vars in *)
  (*           let vcnt = PSette.fold maybe_add vars vcnt in *)
  (*           unroll vcnt vars (pred i) *)
  (*   in *)
  (*   PMappe.diffset (unroll empty_vmap svars k) not_in *)
  (*   |> PMappe.map (fun x -> x, prime_oflw) *)

  let kdeps ?(not_in=empty_vset) evols sdeps adeps k =
    assert (k >= 0);
    let supports = evols_supports evols in
    let maybe_add v m =
      PMappe.add v (try PMappe.find v m |> succ with Not_found -> 0) m in
    let rec unroll vcnt vars = function
      | i when i < 0 -> PSette.fold maybe_add sdeps vcnt
      | i -> let deps = PMappe.interset supports vars in
            let vars = PMappe.fold (fun _ -> PSette.union) deps vars in
            let vcnt = PSette.fold maybe_add vars vcnt in
            unroll vcnt vars (pred i)
    in
    PMappe.diffset (unroll empty_vmap (PSette.union sdeps adeps) k) not_in
    |> PMappe.map (fun x -> x, prime_oflw)

  let pp_varsreq = pp_vmap (fun fmt (a, b) -> fprintf fmt "%i,%B" a b)

  (* --- *)

  type gammavars = vset
  type upsilonvars = vset
  type uc_k_data = gammavars * upsilonvars
  type uc_data = uc_k_data list

  let uc_vars cs (kvars: var array vmap) : uc_data =
    let cvars = CtrlSpec.c_vars cs and uvars = CtrlSpec.u_vars cs in
    let acc_vars f acc v =
      try
        Array.fold_left (fun (i, acc) v -> succ i, f i v acc) (0, acc)
          (PMappe.find v kvars) |> snd
      with
        | Not_found -> acc
    in
    let imap = PMappe.empty (-) in
    let iadd empty i add m =
      PMappe.add i (add (try PMappe.find i m with Not_found -> empty)) m in
    let acc_cvars = acc_vars (fun i v -> iadd empty_vset i (PSette.add v))
    and acc_uvars = acc_vars (fun i v -> iadd empty_vset i (PSette.add v)) in
    let gi = List.fold_left acc_cvars imap cvars in
    let ui = List.fold_left acc_uvars imap uvars in

    let ucks = PMappe.combine begin fun _ gi ui -> match gi, ui with
      | Some gi, Some ui -> Some (gi, ui)
      | Some gi, None -> Some (gi, empty_vset)
      | None, Some ui -> Some (empty_vset, ui)
      | _ -> assert false
    end gi ui in
    PMappe.bindings ucks |> List.split |> snd

  let nfirsts lst n =
    assert (n >= 0);
    let rec nxt acc l = function
      | 0 -> List.rev acc
      | i -> nxt (List.hd l :: acc) (List.tl l) (pred i)
    in
    nxt [] lst n

  (* --- *)

  let subst_i kvars i =
    assert (i >= 0);
    PMappe.fold
      (fun v a acc -> (assert (i < Array.length a);
                    if i = 0 then acc else (v, a.(i)) :: acc))
      kvars [] |> subst_vars

  (* --- *)

  let ishift_expr kvars =
    let oflw acc v =
      if prime_oflw then (v, get_primed_var E.env.Env.env v) :: acc else acc in
    PMappe.fold
      (fun _v a acc -> match Array.to_list a with
        | [] -> assert false
        | [v] -> oflw acc v
        | hd::tl -> (List.fold_left
                      (fun (acc, prev) cur -> ((prev, cur) :: acc, cur))
                      (acc, hd) tl |> (fun (acc, lst) -> oflw acc lst)))
      kvars [] |> subst_vars

  (* --- *)

  (* XXX Not sure which one is best in general… differences lie in the cost of
     substitutions… *)

  (* let lookahead_expr kvars evols k e = *)
  (*   assert (k >= 0); *)

  (*   (\* let rec subst_j = function *\) *)
  (*   (\*   | 0 -> e *\) *)
  (*   (\*   | i -> subst_i kvars (k - i) (subst_j (pred i) //~ evols) *\) *)
  (*   (\* in *\) *)
  (*   (\* subst_j k *\) *)

  (*   (\* tail-rec equiv. *\) *)
  (*   let rec subst_j e = function *)
  (*     | i when i > k -> e *)
  (*     | i -> subst_j (subst_i kvars (k - i) (e //~ evols)) (succ i) *)
  (*   in *)
  (*   subst_j e 1 *)

  let lookahead_expr kvars evols k e =
    assert (k >= 0);
    let ishift = ishift_expr kvars in
    let rec e_i acc = function
      | i when i = k -> acc
      | i -> e_i (ishift acc //~ evols) (succ i)
    in
    e_i e 0

  (* --- *)

  let lookahead_bexpr kvars evols k e =
    of_bexpr e |> lookahead_expr kvars evols k |> to_bexpr

  let lookahead_nexpr kvars evols k e =
    of_nexpr e |> lookahead_expr kvars evols k |> to_nexpr


  (* --- *)

  let lookahead_expr_incr kvars evols prev_e =
    ishift_expr kvars prev_e //~ evols

  let lookahead_bexpr_incr kvars evols prev_e =
    of_bexpr prev_e |> lookahead_expr_incr kvars evols |> to_bexpr

  let lookahead_nexpr_incr kvars evols k prev_e =
    of_nexpr prev_e |> lookahead_expr_incr kvars evols |> to_nexpr

  (* --- *)

  (* Heavy version: *)

  (* let sum_expr pp lookahead accum e0 k e = *)
  (*   assert (k >= 0); *)
  (*   let rec sum_i acc = function *)
  (*     | i when i > k -> acc *)
  (*     | i -> sum_i (lookahead i e |> accum acc) (succ i) *)
  (*   in *)
  (*   sum_i e0 1 *)

  (* let sum_bexpr kvars evols = *)
  (*   lookahead_bexpr kvars evols |> sum_expr pp_bexpr *)

  (* let sum_nexpr kvars evols = *)
  (*   lookahead_nexpr kvars evols |> sum_expr pp_nexpr *)

  (* --- *)

  (* lightweigth, incremental version *)

  (* let sum_expr lla_incr accum e0 k e = *)
  (*   assert (k >= 0); *)
  (*   let rec sum_i acc pei = function *)
  (*     | i when i > k -> acc *)
  (*     | i -> let ei = lla_incr pei in sum_i (accum acc ei) ei (succ i) *)
  (*   in *)
  (*   sum_i e0 e 1 *)

  (* let sum_bexpr kvars evols = *)
  (*   let lla_incr e = ishift_expr kvars (of_bexpr e) //~ evols |> to_bexpr in *)
  (*   sum_expr lla_incr *)

  (* let sum_nexpr kvars evols = *)
  (*   let lla_incr e = ishift_expr kvars (of_nexpr e) //~ evols |> to_nexpr in *)
  (*   sum_expr lla_incr *)

  (* --- *)

  let sum_expr to_expr of_expr kvars evols accum e0 k e =
    assert (k >= 0);
    let ishift = ishift_expr kvars in
    let rec sum_i acc pei = function
      | i when i > k -> acc
      | i -> let ei = ishift (to_expr pei) //~ evols |> of_expr in
            sum_i (accum acc ei) ei (succ i)
    in
    sum_i e0 e 1

  let sum_bexpr kvars = sum_expr of_bexpr to_bexpr kvars
  let sum_nexpr kvars = sum_expr of_nexpr to_nexpr kvars

end

(* -------------------------------------------------------------------------- *)

module Prefix =
  functor (P: DomExt.ELIM_UTILS_PARAMS) (L: Log.T) (E: Env.T) ->
struct
  module Elim = DomExt.ElimUtils (P) (L) (E)
  module L = Lookahead (E)
  open E.BEnvNS
  open E.BEnvNS.BAll
  open Elim

  let setup_kvars evols root_vars extra_vars k =
    let svars = varset E.env.Env.s_vars in
    let sdeps = PSette.inter (PSette.union extra_vars root_vars) svars in
    let adeps = PSette.diff root_vars svars in
    (* Log.d3 logger "sdeps: @[<v>%a@]" pp_vset sdeps; *)
    (* Log.d3 logger "adeps: @[<v>%a@]" pp_vset adeps; *)
    L.kdeps evols sdeps adeps ~not_in:svars k

  let uc_vars ?(lp = lp) cs kvars =
    Log.d3 logger "kvars: @[<v>%a@]" (pp_vmap pp_varr) kvars;
    let uc_data = L.uc_vars cs kvars in
    List.iteri (fun i (gi, ui) ->
      Log.d3 logger "%t@<1>Υ%d = %a" lp (succ i) pp_vset ui;
      Log.d3 logger "%t@<1>Γ%d = %a" lp (succ i) pp_vset gi)
      uc_data;
    uc_data

  (* --- *)

  let mkai ?(lp = lp) ?(la = Util.pc 'A') ?(lai = Util.pp "A%i") kvars evols a k
      =
    let rec mkai acc i =
      if i <= k
      then let ai = L.lookahead_bexpr kvars evols i a in
           Log.d3 logger "%t%a = @[%a@]" lp lai i print ai;
           mkai (ai :: acc) (succ i)
      else List.rev acc
    in
    Log.d logger "%t%t0 = %a" lp la print a;
    mkai [a] 1

  (* --- *)

  let ls fmt = pp_print_string fmt "^"
  let ls' = ls

  let prefixc_ha ?(lp = lp) ?(ls = ls) ~ld ucks al j x =
    let ucks = ith ucks j in
    let al = ith al j in
    Log.i logger "%tComputing @[%t = prefix%i_c(%t)@<1>…@]" lp ld j ls;
    let (xj, _) = List.fold_left2
      begin fun (x, (ls, i)) (gi, ui) a ->
        Log.d3 logger "%t@<1>Υ%i   = @[%a@]" lp i pp_vset ui;
        Log.d3 logger "%t@<1>Γ%i   = @[%a@]" lp i pp_vset gi;
        Log.d3 logger "%tA%i   = @[%a@]" lp (pred i) pp_repr a;
        let gi = PSette.elements gi and ui = PSette.elements ui in
        let h = Log.roll (exist' gi) a in
        Log.d3 logger "%tH%i  @[ = @[%a@]@ (= @<2>∃Γ%iA%i)@]\
                      " lp i (pp_repr' gi) h i (pred i);
        let x = Log.roll (inter a) x in
        Log.d  logger "%t%t@<2>∩A%i%a\
                      " lp ls (pred i) (fun f -> log3 " = @[%a@]\
                                                   " f (pp_repr' gi)) x;
        let x = Log.roll (exist' gi) x in
        Log.d  logger "%t@<2>∃Γ%i^%a\
                      " lp i (fun f -> log3 " = @[%a@]" f (pp_repr' gi)) x;
        let x = Log.roll (imply h) x in
        Log.d3 logger "%tH%i@<5>⇒^ = @[%a@]" lp i pp_repr x;
        let x = Log.roll (forall' ui) x in
        Log.d  logger "%t@<2>∀Υ%i^@[%a@]\
                      " lp i (fun f -> log3 " = @[%a@]" f (pp_repr' ui)) x;
        x, (ls', pred i)
      end (x, (ls, List.length ucks + j)) (List.rev ucks) (List.rev al)
    in
    if Log.check_level logger Log.Debug3
    then Log.d3 logger "%t@[%t = ^@]" lp ld
    else if Log.check_level logger Log.Debug2
    then Log.d3 logger "%t%t = %a" lp ld pp_repr xj
    else Log.i logger "%tFinished computing @[%t (= prefix%i_c(%t))@]\
                      " lp ld j ls;
    xj

  let prefixcj ucks ais j ?lp ?ls ~ld = match ucks with
    | [] -> fun x -> x
    | ucks -> prefixc_ha ucks ais ?lp ?ls ~ld j

  let prefixcjk ucks ais j k =
    if j = k
    then (fun ?(lp = lp) ?(ls = ls) ~ld x ->
      Log.d2 logger "%t@[%t = %t@]" lp ld ls; x)
    else prefixcj (L.nfirsts ucks k) (L.nfirsts ais k) j

  (* --- *)

  let lres ?(debug = P.debug) ?(info = P.info) ?(lp = lp) ?ld ls ppd d =
    let dbg f = Log.log debug logger f and info f = Log.log info logger f in
    begin match ld with
      | None ->
          dbg "%t@[%t = %a@]" lp ls ppd d;
          d, ls
      | Some ld when Log.check_level logger debug ->
          dbg "%t%t@[ = %a@ (= %t)@]" lp ld ppd d ls;
          d, ld
      | Some ld ->
          info "%t%t = %t" lp ld ls;
          d, ld
    end

  let idest ?(info = P.info) ?(lp = lp) = function
    | None -> ()
    | Some ld -> Log.i logger "%tComputing %t@<1>…" lp ld

  let plp ?lp = function
    | None -> lp
    | Some ld -> match lp with
        | None -> Some (Util.pp1 "%t: " ld)
        | Some lp -> Some (Util.pp2 "%t%t: " lp ld)

  (* --- *)

  let prefix1_c ?info ?debug ?lp ucks ais =
    assert (List.length ucks == List.length ais);
    let prefix1k_c = prefixcjk ucks ais 1 in
    fun ?ld ?(la = Util.pc 'A') a ~lr1 ~lr i ri ->
      idest ?info ?lp ld;
      let r1 = prefix1k_c ?lp:(plp ?lp ld) ~ld:lr1 ~ls:lr i ri in
      let d = Log.roll (inter a) r1 in
      lres ?debug ?info ?lp ?ld
        (Util.pp2 "@[%t@ @<1>∩@ %t@]" la lr1)
        pp_repr d

  (* --- *)

  let prefix01_c ?info ?lp ucks ais =
    let prefix01_c = prefixcjk ucks ais 0 1 in
    fun ~lgoal ->
      idest ?info ?lp lgoal;
      prefix01_c ?lp:(plp ?lp lgoal)

  let growk ?debug ?info ?lp ucks ais =
    let prefix01_c = prefix01_c ?info ?lp ucks ais in
    fun ?ld ~lk0 ~lk k ~lr1 r1 ->
      let k0 = prefix01_c ~lgoal:ld ~ld:lk0 ~ls:lk k in
      let d = Log.roll (fun r1 -> union k (inter (neg k0) r1)) r1 in
      lres ?debug ?info ?lp ?ld
        (Util.pp3 "@[%t@ @<1>∪@ (¬%t@ @<1>∩@ %t)@]" lk lk0 lr1)
        pp_repr d

  let shrink ?debug ?info ?lp ucks ais =
    let prefix01_c = prefix01_c ?info ?lp ucks ais in
    fun ?ld ~lk k ~lr0 ~lr1 r1 ->
      let r0 = prefix01_c ~lgoal:ld ~ld:lr0 ~ls:lr1 r1 in
      let d = Log.roll (fun k -> union (inter (neg r0) k) (inter r0 r1)) k in
      lres ?debug ?info ?lp ?ld
        (Util.pp4 "@[(@<1>¬%t@ @<1>∩@ %t)@ @<2>∪@ (%t@ @<1>∩@ %t)@]\
                  " lr0 lk lr0 lr1)
        pp_repr d

  (* --- *)

  let mkbe ?debug ?info ?lp ucks ais ?ld ?(la = Util.pc 'A') a ~lku ~lk k =
    let elim = Elim.exist ?lp:(plp ?lp ld) in
    let g1, _u1 = List.hd ucks in
    let g1 = PSette.elements g1 in
    let ku = elim ~ld:lku ~ls:lk ~lv:"@<2>Γ1" g1 k in
    let d = Log.roll (fun k -> inter a (imply ku k)) k in
    lres ?debug ?info ?lp ?ld
      (Util.pp3 "@[%t@ @<1>∩@ (%t@ @<1>⇒@ %t)@]" la lku lk)
      pp_repr d

  (* --- *)

  let mkrecov ?debug ?info ?lp ucks ais ?(la = Util.pc 'A') a ?ld ~lk k ?le e =
    let elim = Elim.exist ?lp:(plp ?lp ld) in
    let lec = Util.ps "Ec" and le0 = Util.ps "E." in
    let g1, u1 = List.hd ucks in
    let g1, u1 = PSette.elements g1, PSette.elements u1 in
    let ec = elim ~ld:lec ?ls:le ~lv:"@<2>Υ1" u1 e in
    let e0 = elim ~ld:le0 ~ls:lec ~lv:"@<2>Γ1" g1 ec in
    let d = Log.roll (fun k -> union (inter a ec) (inter (neg e0) k)) k in
    lres ?debug ?info ?lp ?ld
      (Util.pp2 "@[(%t@ @<1>∩@ Ec)@ @<1>∪@ (¬E.@ ∩@ %t)@]" la lk)
      pp_repr d

end
