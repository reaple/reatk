(******************************************************************************)
(* Program *)
(* program structures, interface to frontends,
   interface for data-to-control-flow *)
(* author: Peter Schrammel *)
(* version: 0.9.0 *)
(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)
(******************************************************************************)

(******************************************************************************)
(** {2 Program structures } *)
(******************************************************************************)

(** type and variable declarations *)
type declaration_t =
    {
      typdef: (Env.var_t, Env.typdef) PMappe.t;
      state: (Env.var_t, Env.typ) Mappe.t;
      local: (Env.var_t, Env.typ) Mappe.t;
      input: (Env.var_t, Env.typ) Mappe.t;
    }

(** dataflow program (with additional infos):
      the representation produced by the frontend *)
type dfprog_t =
{
  d_disc_equs : Env.equs_t;
  d_cont_equs : Env.equs_t;
  d_zero_defs : Env.zerodefs_t;
  d_init : Env.boolexpr_t;
  d_final : Env.boolexpr_t;
  d_ass : Env.boolexpr_t;
  d_reach : Env.boolexpr_t;
  d_attract: Env.boolexpr_t;
}

(** CFG program - the representation for analysis *)
type cfprog_t =
{
  c_cfg : Cfg.t;
  c_disc_equs : Env.equs_t;
  c_cont_equs : Env.equs_t;
  c_init : Env.boolexpr_t;
  c_final : Env.boolexpr_t;
  c_ass : Env.boolexpr_t;
  c_reach : Env.boolexpr_t;
  c_attract : Env.boolexpr_t;
}

(** default constructor for data-flow program structure *)
let make_dfprog d_disc_equs d_cont_equs d_zero_defs d_init d_final d_ass d_reach
    d_attract =
  {d_disc_equs; d_cont_equs; d_zero_defs; d_init; d_final; d_ass; d_reach;
   d_attract }
(* add new make_* functions here, if you add fields to df_prog_t *)

let make_empty_dfprog env =
  let t = Cudd.Bdd.dtrue (Env.cuddman env) in
  {d_disc_equs=[]; d_cont_equs=[]; d_zero_defs=[];
   d_init=t; d_final=t; d_ass=t; d_reach=t; d_attract=t}

(** default constructor for CFG program structure *)
let make_cfprog c_cfg c_disc_equs c_cont_equs c_init c_final c_ass c_reach
    c_attract =
  { c_cfg; c_disc_equs; c_cont_equs; c_init; c_final; c_ass; c_reach; c_attract}
(* add new make_* functions here, if you add fields to cf_prog_t *)

(** duplicates a CFG program structure *)
let copy_cfprog cfprog =
  make_cfprog (Cfg.copy cfprog.c_cfg)
    cfprog.c_disc_equs cfprog.c_cont_equs
    cfprog.c_init cfprog.c_final cfprog.c_ass cfprog.c_reach cfprog.c_attract

(******************************************************************************)
(** {2 Frontend interface } *)
(******************************************************************************)

(** function protoypes for the frontend *)
type translate_t = Env.t -> dfprog_t
type parser_t = string -> (declaration_t * translate_t * string * string)
type parser4synth_t = parser_t                  (* the same signature for now *)

(******************************************************************************)
(** {2 Miscellaneous } *)
(******************************************************************************)

open BddapronUtil

(******************************************************************************)

(** apply an environment permutation to the params *)
let apply_env_permutation_to_dfprog p dfprog =
  {
    d_disc_equs = permute_equs p dfprog.d_disc_equs;
    d_cont_equs = permute_equs p dfprog.d_cont_equs;
    d_zero_defs = dfprog.d_zero_defs;                                  (* XXX *)
    d_init = permute_boolexpr p dfprog.d_init;
    d_final = permute_boolexpr p dfprog.d_final;
    d_ass = permute_boolexpr p dfprog.d_ass;
    d_reach = permute_boolexpr p dfprog.d_reach;
    d_attract = permute_boolexpr p dfprog.d_attract;
  }
let apply_env_permutation_to_dfprog' = function
  | None -> fun dfprog -> dfprog
  | Some p -> apply_env_permutation_to_dfprog p

(** apply an environment permutation to the params *)
let apply_env_permutation_to_cfprog p cfprog =
  {
    c_cfg = Cfg.apply_permutations p cfprog.c_cfg;
    c_disc_equs = permute_equs p cfprog.c_disc_equs;
    c_cont_equs = permute_equs p cfprog.c_cont_equs;
    c_init = permute_boolexpr p cfprog.c_init;
    c_final = permute_boolexpr p cfprog.c_final;
    c_ass = permute_boolexpr p cfprog.c_ass;
    c_reach = permute_boolexpr p cfprog.c_reach;
    c_attract = permute_boolexpr p cfprog.c_attract;
  }
let apply_env_permutation_to_cfprog' = function
  | None -> fun cfprog -> cfprog
  | Some p -> apply_env_permutation_to_cfprog p

let cfprog_as_dfprog { c_disc_equs; c_cont_equs; c_init; c_final; c_ass;
                       c_reach; c_attract; } =
  {
    d_disc_equs = c_disc_equs;
    d_cont_equs = c_cont_equs;
    d_zero_defs = [];
    d_init = c_init;
    d_final = c_final;
    d_ass = c_ass;
    d_reach = c_reach;
    d_attract = c_attract;
  }

let cfprog_is_a_self_loop { c_cfg } = Cfg.is_a_self_loop c_cfg

(* * Gathers all groups of inputs altogether *)
(* let merge_signature { signature } = *)
(*   let all_inputs, all_contrs = List.fold_left *)
(*     begin fun (all_inputs, all_contrs) { input; contr } -> *)
(*       (Mappe.addmap all_inputs input, (contr :: all_contrs)) *)
(*     end (Mappe.empty, []) signature in *)
(*   let all_contrs = List.flatten all_contrs in *)
(*   all_inputs, all_contrs *)

(* let merge_all_inputs { signature } : (Env.var_t, Env.typ) Mappe.t = *)
(*   let all_inputs = List.fold_left *)
(*     begin fun all_inputs { input; contr } -> *)
(*       let all_inputs = Mappe.addmap input all_inputs in *)
(*       List.fold_left (fun all_inputs (v, (_, t)) -> Mappe.add v t all_inputs) *)
(*         all_inputs contr *)
(*     end Mappe.empty signature in *)
(*   all_inputs *)

(* let uc_groups { signature } : (Env.vars_t * Env.vars_t) list = *)
(*   List.fold_right begin fun sigs { input; contr } -> *)
(*     (List.map fst (Mappe.bindings input), List.map fst contr) :: sigs *)
(*   end signature [] *)

(* let log_cfg_info ~logger ?(full_level = Log.Debug) env { c_cfg } = *)
(*   if Log.check_level logger full_level *)
(*   then Log.log full_level logger "@[CFG: %a@]" (Cfg.print_short env) c_cfg *)
(*   else let cntv, cnth, _, _ = PSHGraph.size c_cfg in *)
(*        Log.i logger "CFG (%d location(s), %d arc(s)" cntv cnth *)

let pp_cfg env fmt { c_cfg } =
  Cfg.print_short env fmt c_cfg

let pp_cfg_short _env fmt { c_cfg } =
  let cntv, cnth, _, _ = PSHGraph.size c_cfg in
  Format.fprintf fmt "%d location(s), %d arc(s)" cntv cnth
