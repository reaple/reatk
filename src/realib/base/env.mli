(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)

(** framework base: environment *)

module Symb: sig
  type t = private string
  val mk: string -> t
  val compare: t -> t -> int
  val print: Format.formatter -> t -> unit
  val to_string: t -> string
end

type var_t = Symb.t                                 (** variable type *)
type symbol_t = var_t Bdd.Symb.t                    (**BddApron variable type *)

type cuddman = Cudd.Man.v Cudd.Man.t                    (** CUDD manager *)
type apronenv = Apron.Environment.t                     (** APRON environment *)

type typ = var_t Bddapron.Typ.t                           (** type *)
type typdef = var_t Bddapron.Typ.findef                   (** type definition *)

type expr_t = var_t BddapronUtil.expr_t              (** expression *)
type boolexpr_t = var_t BddapronUtil.boolexpr_t      (** Boolean expression *)
type numexpr_t = var_t Bddapron.Expr0.Apron.t        (** numerical expression *)
type vars_t = var_t BddapronUtil.vars_t              (** variables *)
type vset = var_t BddapronUtil.vset
type equ_t = var_t BddapronUtil.equ_t    (** equation (transition function) *)
type equs_t = var_t BddapronUtil.equs_t  (** equations (transition functions) *)

type zeroexpr_t = numexpr_t (** zero-crossing expression *)
type zerodef_t = var_t * numexpr_t (** zero-crossing definition *)
type zerodefs_t = zerodef_t list (** zero-crossing definition table *)

(** {2 environment } *)

(** environment structure with APRON and BddApron environments and
    (s)tate, (i)nput, (b)oolean and (n)umerical variables *)
type t =
    {
      env : var_t Bddapron.Env.t; (* logico-numerical environment (BDD APRON) *)
      cond : var_t Bddapron.Cond.t;  (* numerical constraints *)
      s_vars : vars_t; (* all state variables (only non-primed) *)
      i_vars : vars_t; (* all input variables
                          (including new inputs (double primed)) *)
      b_vars : vars_t; (* all Boolean variables
                          (including primed and new inputs) *)
      n_vars : vars_t; (* all numerical variables
                          (including primed and new inputs) *)
      bi_vars : vars_t; (* Boolean input variables (including new inputs) *)
      ni_vars : vars_t; (* numerical input variables (including new inputs)*)
      bs_vars : vars_t; (* Boolean state variables (only non-primed) *)
      ns_vars : vars_t; (* numerical state variables (only non-primed) *)

      mutable zero_vars : vars_t; (* placeholders for zero-crossings *)
      disc_q_vars : vars_t;  (* state variables introduced during
                                translation of discrete zero-crossings *)
      cont_q_vars : vars_t; (* state variables introduced during
                               translation of continuous zero-crossings *)
      q_i_vars : vars_t; (* input variables introduced during
                            translation of zero-crossings *)
      bool_size : int;                   (* number of finite-type variables *)
      cond_size : int;                   (* number of numerical constraints *)
      zeros_size : int;                  (* number of zero-crossing variables *)
    }

(** {2 constants} *)

val symbol : symbol_t                    (** BddApron variable type structure *)
val init_var : unit -> var_t              (** "init" variable *)
val epsilon : float                      (** epsilon (small real constant) *)

val apronenv: t -> apronenv
val cuddman: t -> cuddman

(** {2 constructor} *)

(** creates the environment *)
val make
  : ?max_mem:int64
  -> ?booking_factor:int
  -> ?cond_factor:int
  -> ?max_conds: int
  -> ?enable_primed_state_vars:bool
  -> ?enable_primed_input_vars:bool
  -> ?enable_zeros:bool
  -> (var_t, typdef) PMappe.t -> (var_t, typ) Mappe.t -> (var_t, typ) Mappe.t -> t

type varspec =
  | Var of var_t * typ * [ `State | `Input ] * bool
  | Fixed of varspecs
and varspecs = varspec list

(** creates the environment *)
val make'
  : ?max_mem:int64
  -> ?booking_factor:int
  -> ?cond_factor:int
  -> ?max_conds: int
  -> ?enable_zeros:bool
  -> (var_t, typdef) PMappe.t -> varspecs -> t

(** returns a new input variable with the same type as the given variable *)
val get_newinput :  t -> var_t -> var_t

(** returns a (discrete=false or continuous=true) q variable for the
    given zero-crossing variable *)
val get_q_var : var_t -> bool -> var_t

(** returns a the ith q bool input variable for the given zero-crossing
    variable *)
val get_q_bool_i_var :  var_t -> int -> var_t

val typof: t -> var_t -> typ

val elim_vars: t -> vars_t -> t
val respec_vars: t -> vars_t -> t        (* only given vars are state variables *)

(** computes the careset (some admissible combinations of
    numerical constraints *)
val compute_careset :  t -> unit

(** updates the careset (some admissible combinations of
    numerical constraints *)
val update_careset : ?numvars_threshold:int -> t -> unit

(** {2 zero-crossing definitions} *)

(** returns the zero-crossing variable corresponding to
    the given zero-crossing expression *)
val find_zero_var : zerodefs_t -> zeroexpr_t -> var_t

(** returns the zero-crossing corresponding to
    the given zero-crossing variable *)
val get_zero_def : zerodefs_t -> var_t -> zeroexpr_t

(** returns a new zero-crossing variable *)
val get_newzero_var :  t -> var_t

(** prints the zero-crossing definitions *)
val print_zero_defs :  t -> Format.formatter -> zerodefs_t -> unit

(** {2 utilities} *)

val print_vars : t -> Format.formatter -> vars_t -> unit
val print_boolexpr : t -> Format.formatter -> boolexpr_t -> unit
val print_equations : t -> Format.formatter -> equs_t -> unit

(** splits the equations into a list of variables and a list of expressions *)
val split_equs : equs_t -> vars_t * expr_t list

(** returns the boolean equations *)
val get_bool_equs : t -> equs_t -> equs_t

(** returns the numerical equations *)
val get_num_equs : t -> equs_t -> equs_t

(** returns the number of Boolean-equivalent variables in the given
    variable set *)
val number_of_boolvars : t -> vars_t -> int

(** traverses all the type definitions of an environment *)
val fold_typdefs: t -> (var_t -> typdef -> 'a -> 'a) -> 'a -> 'a

(** {2 CUDD utilities } *)

(** groups BDD variables (for fixing order during re-ordering) *)
val cudd_group :  t -> unit

(** returns the variable corresponding to the given BDD index *)
val var_of_cuddid :  t -> int -> var_t

(** defines a BDD variable mapping to existing state primed variables (for
    faster variable substitution) *)
val cudd_make_varmap : t -> unit

(** Clear the carset and collect CUDD resources (including caches) *)
val clear: t -> unit

(** {2 Parsing of expressions based on an environment} *)

exception InvalidBooleanExpressionSpecification of string
val str_to_boolexpr: t -> string -> boolexpr_t
(** [str_to_boolexpr env str] parses the Boolean bddapron expression [str];
    Raises {!InvalidBooleanExpressionSpecification} if [str] is not a Boolean
    expression. *)

(** parse list of Boolean expressions *)
val strlist_to_boolexprlist : t -> string list -> boolexpr_t list

exception UnknownVariables of string list
val strlist_to_vars: t -> string list -> vars_t

(** {2 module type for building functors } *)

module type T = sig
  val env: t
  type var = var_t
  module BEnv: BddapronUtil.BDDAPRON_ENV with type var = var
  module BEnvNS: module type of BddapronUtil.BEnvNS (BEnv)
end

val as_module: t -> (module T)
