(** {2 Synthesis Module Type } *)

type controller = Env.boolexpr_t
type synth = CtrlSpec.t * controller * Program.cfprog_t ->
    CtrlSpec.t * controller option * Program.cfprog_t
type coreach = CtrlSpec.t * controller * Program.cfprog_t ->
  Env.boolexpr_t * Program.cfprog_t

module type T0 = sig
  type param
  val synthesize: param -> synth
  val descr: param -> Format.formatter -> unit
end

module type T = sig
  include T0
  val coreach: param -> coreach
end
