open BddapronUtil

(* -------------------------------------------------------------------------- *)

type cond
type config

val never: cond
val always: cond
val at_ith_iteration: int -> cond
val thresholds_cond: ?i:int -> ?dtr_max:int -> ?dnr_max:int
  -> cond option -> cond option

(* -------------------------------------------------------------------------- *)

val mk_config: ?stop_r:cond -> ?stop_dr:cond -> ?log:bool -> ?logfile:string
  -> unit -> config

module Make (Logger: Log.T): functor (E: BDDAPRON_ENV) -> sig
  type t
  val make: config -> t
  val dispose: t -> unit
  val handle_bdd: t -> lp:Util.pu -> i:int -> v:string -> _ Cudd.Bdd.t -> unit
  val handle_vdd: t -> lp:Util.pu -> i:int -> v:string -> _ Cudd.Vdd.t -> unit
  val handle': ('x -> int * float) -> t -> lp:Util.pu -> i:int -> v:string -> 'x -> unit
end

(* -------------------------------------------------------------------------- *)
