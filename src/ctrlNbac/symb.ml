(** Internal representation of symbols.

    @author Nicolas Berthier *)

open Format

(* -------------------------------------------------------------------------- *)

(** Type of symbols; we know it's an integer to optimize possible array accesses
    and computations. *)
type symb = int

module Symbol2Int = Hashtbl.Make
  (struct
    type t = string
    let equal = (=)
    let hash = Hashtbl.hash
   end)

(** Dummy, invalid symbol, guaranteed to remain non associated with an actual
    string. Calling {!of_string} [dummy] raises {!Invalid_symbol}. *)
let dummy = -1
let s2i = Symbol2Int.create 100
let i2s = ref (Array.make 32 "")
let curr_id = ref dummy

(** Frees some memory by resetting the module's internal data structures. Any
    symbol previously registered by {!of_string} becomes invalid. *)
let reset () =
  Symbol2Int.clear s2i;
  Array.fill !i2s 0 (Array.length !i2s) "";
  curr_id := dummy

let grow () =
  let last = !i2s in
  i2s := Array.make (Array.length last * 4) "";
  Array.blit last 0 !i2s 0 (Array.length last)

(** Exception raised when trying to get a string representation of an invalid
    symbol. *)
exception Invalid_symbol of symb

(** [to_string symb] returns the string associated to the symbol [symb].

    Raises {!Invalid_symbol} if [symb] is invalid (typically, if {!reset} has
    been called since its creation). *)
let to_string i =
  if i > !curr_id then raise (Invalid_symbol i);
  try Array.get !i2s i with
    | Invalid_argument _ -> raise (Invalid_symbol i)

(** [of_string s] returns the symbol associated to [s] if it has already been
    created since the las call to {!reset}, of or a fresh one otherwise. *)
let of_string s =
  try Symbol2Int.find s2i s with
    | Not_found ->
        incr curr_id;
        let i = !curr_id in
        Symbol2Int.add s2i s i;
        if i = Array.length !i2s then grow ();
        Array.set !i2s i s;
        i

(* -------------------------------------------------------------------------- *)

module M: Structs.S with type t = symb = struct
  (** Alias type *)
  type t = symb
  let compare = (-)
  let equal = (==)
  let hash = Hashtbl.hash

  (** [output oc symb] writes the string associated to the symbol [symb] on the
      output channel [oc].

      Raises {!Invalid_symbol} when {!to_string} would. *)
  let output o i = output_string o (to_string i)

  (** [print fmt symb] writes the string associated to the symbol [symb] on the
      formatter [fmt].

      Raises {!Invalid_symbol} when {!to_string} would. *)
  let print fmt e = pp_print_string fmt (to_string e)
end

include M

(* -------------------------------------------------------------------------- *)

(** [capitalize symb] returns a symbol corresponding to the given one with the
    first character uppercased.

    Raises {!Invalid_symbol} when {!to_string} would. *)
let capitalize i = of_string (String.capitalize_ascii (to_string i))

(** [exists s] tests whether a given string belongs to the set of all symbols
    created since the last call to {!reset}. *)
let exists = Symbol2Int.mem s2i

(* --- *)

(** [fresh ~cnt_ref str] creates a new symbol by appending an integer greater or
    equal to the value referenced by [cnt_ref] if given ([0] otherwise), to the
    prefix [str]. *)
let fresh ?(cnt_ref = ref 0) =
  let rec fresh' prefix =
    let s = sprintf "%s%d" prefix !cnt_ref in
    if Symbol2Int.mem s2i s
    then (incr cnt_ref; fresh' prefix)
    else of_string s
  in
  fresh'

(** [fresh' ~cnt_ref str] acts as {!fresh}, except that the symbol [str] is
    tried first. *)
let fresh' ?cnt_ref s =
  if exists s then fresh ?cnt_ref s else of_string s

(* -------------------------------------------------------------------------- *)

include (Structs.Make (M): Structs.T with type t := t)

(* -------------------------------------------------------------------------- *)
