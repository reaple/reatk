open Cudd.Man

type reordering_algorithm = Cudd.Man.reorder

module Options = struct
  let enable_reordering = ref true
  let enable_dynamic_reordering = ref true
  (* let reorder_policy = ref REORDER_SYMM_SIFT *)
  (* let reorder_policy_conv = ref REORDER_SYMM_SIFT_CONV *)
  let reorder_policy = ref REORDER_GROUP_SIFT
  let reorder_policy_conv = ref REORDER_GROUP_SIFT_CONV
end

open Options

let print_limit = print_limit
let make_man ?(cache_factor = 4) ?max_mem () =
  let cudd = make_v
    ~numSlots:(256 * cache_factor)                         (* default: 256 *)
    ~cacheSize:(262144 * cache_factor)                     (* default: 262144 *)
    ()
  in
  match max_mem with
    | Some m -> set_max_mem cudd m; cudd
    | None -> cudd

let fullcheck env =
  let cudd = Bdd.Env.cuddman env in
  assert (not (Cudd.Man.debugcheck cudd));
  Gc.full_major ();
  ignore (Cudd.Man.garbage_collect cudd);
  not (Cudd.Man.debugcheck cudd)

(* --- *)

let clear env cond =
  Bdd.Cond.clear cond;
  Cudd.Man.garbage_collect (Bdd.Env.cuddman env) |> ignore

(* --- *)

let reorder env =
  if !enable_reordering then begin
    reduce_heap (Bdd.Env.cuddman env) !reorder_policy 0;
  end

let reorder_conv env =
  if !enable_reordering then begin
    Log.tf "Full synchronous reordering.";
    reduce_heap (Bdd.Env.cuddman env) !reorder_policy_conv 0;
  end

(* --- *)

let _ =
  set_gc 1000000
    Gc.full_major
    (fun () -> Gc.full_major (); Log.tf "CUDD reordering…")

(* --- *)

(* let enable_dynamic_reordering'' cudd p = *)
(*   Options.enable_reordering := true; *)
(*   Options.enable_dynamic_reordering := true; *)
(*   enable_autodyn cudd p *)
(* let enable_dynamic_reordering' cudd = *)
(*   enable_dynamic_reordering'' cudd !Options.reorder_policy *)
(* let enable_dynamic_reordering env = *)
(*   enable_dynamic_reordering' (Bdd.Env.cuddman env) *)

let disable_dynamic_reordering' = disable_autodyn
let disable_dynamic_reordering env =
  disable_dynamic_reordering' (Bdd.Env.cuddman env)

(* --- *)

type dynamic_reordering_status = reorder option

let dynamic_reordering_status' = autodyn_status
let dynamic_reordering_status env =
  dynamic_reordering_status' (Bdd.Env.cuddman env)

let start_dynamic_reordering'' cudd p =
  if !Options.enable_reordering && !Options.enable_dynamic_reordering then
    enable_autodyn cudd p
let start_dynamic_reordering' cudd =
  start_dynamic_reordering'' cudd !Options.reorder_policy
let start_dynamic_reordering env =
  start_dynamic_reordering' (Bdd.Env.cuddman env)

let stop_dynamic_reordering' cudd =
  if !Options.enable_reordering && !Options.enable_dynamic_reordering then
    disable_autodyn cudd
let stop_dynamic_reordering env =
  stop_dynamic_reordering' (Bdd.Env.cuddman env)

(* --- *)

let dynamic_reordering_save_n_select env reordering_algorithm =
  let s = dynamic_reordering_status env in
  start_dynamic_reordering'' (Bdd.Env.cuddman env) reordering_algorithm;
  s

let dynamic_reordering_save_n_stop env =
  let cudd = Bdd.Env.cuddman env in
  let s = autodyn_status cudd in disable_autodyn cudd; s

let dynamic_reordering_save_n_stop_if condition env =
  if condition then dynamic_reordering_save_n_stop env else None

let dynamic_reordering_restore env = function
  | Some s -> start_dynamic_reordering'' (Bdd.Env.cuddman env) s
  | _ -> ()

let dynamic_reordering_select1 env reordering_algorithm f a =
  let drs = dynamic_reordering_save_n_select env reordering_algorithm in
  let res = f a in dynamic_reordering_restore env drs; res

let dynamic_reordering_activate1 env =
  dynamic_reordering_select1 env !reorder_policy

let dynamic_reordering_suspend1 env f a =
  let drs = dynamic_reordering_save_n_stop env in
  let res = f a in dynamic_reordering_restore env drs; res

let dynamic_reordering_suspend2 env f a b =
  let drs = dynamic_reordering_save_n_stop env in
  let res = f a b in dynamic_reordering_restore env drs; res

let print_dynamic_reordering_status fmt = function
  | None -> Format.pp_print_string fmt "none"
  | Some r -> Format.pp_print_string fmt (string_of_reorder r)

(* --- *)

let enable_reordering env =
  Options.enable_reordering := true; start_dynamic_reordering env

let disable_reordering env =
  stop_dynamic_reordering env; Options.enable_reordering := false

(* --- *)

type reordering_status = bool * dynamic_reordering_status

let reordering_status' cudd =
  !Options.enable_reordering, dynamic_reordering_status' cudd
let reordering_status env = reordering_status' (Bdd.Env.cuddman env)

let reordering_save_n_disable env =
  let s = reordering_status env in disable_reordering env; s
let reordering_save_n_enable env =
  let s = reordering_status env in enable_reordering env; s
let reordering_save_n_disable_if condition env =
  if condition then reordering_save_n_disable env else reordering_status env
let reordering_save_n_enable_if condition env =
  if condition then reordering_save_n_enable env else reordering_status env

let reordering_restore env (s, d) =
  Options.enable_reordering := s;
  dynamic_reordering_restore env d

let reordering_suspend1 env f a =
  let drs = reordering_save_n_disable env in
  let res = f a in reordering_restore env drs; res
let reordering_suspend2 env f a b =
  let drs = reordering_save_n_disable env in
  let res = f a b in reordering_restore env drs; res

let reordering_suspend1_if condition env f a =
  if condition then reordering_suspend1 env f a else f a
let reordering_suspend2_if condition env f a b =
  if condition then reordering_suspend2 env f a b else f a b

let reordering_activate1 env f a =
  let drs = reordering_save_n_enable env in
  let res = f a in reordering_restore env drs; res
let reordering_activate2 env f a b =
  let drs = reordering_save_n_enable env in
  let res = f a b in reordering_restore env drs; res

let reordering_activate1_if condition env f a =
  if condition then reordering_activate1 env f a else f a
let reordering_activate2_if condition env f a b =
  if condition then reordering_activate2 env f a b else f a b

let print_reordering_status fmt = function
  | false, _ -> Format.pp_print_string fmt "disabled"
  | true, None -> Format.pp_print_string fmt "static@ only"
  | true, r -> print_dynamic_reordering_status fmt r

(* --- *)

let print_stats env =
  print_info (Bdd.Env.cuddman env)

open Format
let print_params fmt env =
  let cudd = Bdd.Env.cuddman env in
  Format.fprintf fmt "looseupto=%d@;max_live=%d@;max_mem=%Ld@;"
    (get_looseupto cudd) (get_max_live cudd) (get_max_mem cudd);
  Format.fprintf fmt "max_growth=%f@;max_growth_alt=%f@;reordering_cycle=%d"
    (get_max_growth cudd) (get_max_growth_alt cudd) (get_reordering_cycle cudd)

(* --- *)

module Decomp = struct

  type kind =
    | Disjunctive
    | Conjunctive

  type algorithm =
    | VariableBased
    | Generalized
    | Iterative

  (* --- *)

  open Cudd.Bdd

  let decomp ~kind ~algorithm = match algorithm, kind with
    | VariableBased, Conjunctive -> varconjdecomp
    | VariableBased, Disjunctive -> vardisjdecomp
    | Generalized, Conjunctive -> genconjdecomp
    | Generalized, Disjunctive -> gendisjdecomp
    | Iterative, Conjunctive -> iterconjdecomp
    | Iterative, Disjunctive -> iterdisjdecomp

  (* --- *)

  let fold_dec ~decomp ?(stop = fun _ -> false) f =
    let rec dec acc = function
      | [] -> acc
      | x :: tl when stop x -> dec (f x acc) tl
      | x :: tl -> match decomp x with
          | None -> dec (f x acc) tl
          | Some (x1, x2) -> dec acc (x1 :: x2 :: tl)
    in
    dec

  let acc_dec ~decomp ?stop = fold_dec ~decomp ?stop List.cons

  let count ~decomp ?stop p = fold_dec ~decomp ?stop (fun _ -> succ) 0 [p]
  let to_list ~decomp ?stop p = acc_dec ~decomp ?stop [] [p]
  let fold ~decomp ?stop f p i = fold_dec ~decomp ?stop f i [p]

  (* --- *)

  let count ~kind ~algorithm = count ~decomp:(decomp ~kind ~algorithm)
  let to_list ~kind ~algorithm = to_list ~decomp:(decomp ~kind ~algorithm)
  let fold ~kind ~algorithm = fold ~decomp:(decomp ~kind ~algorithm)

end

(* --- *)
