(* -*- coding: utf-8 -*- *)
(* This file is part of ReaTK released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)

(** BddApron-based optimization *)

open BddapronUtil

(* -------------------------------------------------------------------------- *)

let logger = Log.mk ~level:Log.Debug3 "oB"
let log3 f = Log.cd3 logger f

(* -------------------------------------------------------------------------- *)

type 'a optim_goal =
  [
  | `Minimize of 'a * string option
  | `Maximize of 'a * string option
  ]

let goal = function
  | `Minimize g | `Maximize g -> fst g

let map_goal f = function
  | `Minimize e -> `Minimize (f (fst e), None)
  | `Maximize e -> `Maximize (f (fst e), None)

(* -------------------------------------------------------------------------- *)

module GoalUtils = functor (BEnvNS: BDDAPRON_ENV_NS) -> struct
  open BEnvNS
  open BAll
  open NOps

  (* --- *)

  let permute_goal perms = map_goal (BddapronUtil.permute_numexpr perms)
  let permute_goals perms = List.map (permute_goal perms)

  (* --- *)

  type rel = (nexpr -> nexpr -> bexpr) * Util.relfmt

  let maxrel t' t = t' >.. t
  let minrel t' t = t' <.. t

  let relation_of_goal = function
    | `Minimize _ -> minrel, ("%(%) < %(%)": Util.relfmt)
    | `Maximize _ -> maxrel, ("%(%) > %(%)": Util.relfmt)

end

(* -------------------------------------------------------------------------- *)

(** {2 One-step optimization} *)

type 'a one_step_goal = 'a optim_goal

let pp_goal pp_e fmt = function
  | `Minimize (_, Some v) -> Format.fprintf fmt "minimization@ of@ %s" v
  | `Maximize (_, Some v) -> Format.fprintf fmt "maximization@ of@ %s" v
  | `Minimize (v, None) -> Format.fprintf fmt "minimization@ of@ %a" pp_e v
  | `Maximize (v, None) -> Format.fprintf fmt "maximization@ of@ %a" pp_e v

let pp_goals pp_e = Util.list_print' ~copen:(fun fmt -> ()) ~cclose:(fun fmt -> ())
  ~csep:(fun fmt -> Format.fprintf fmt ",@;") (pp_goal pp_e)

module OneStep = functor (BEnvNS: BDDAPRON_ENV_NS) -> struct
  module GoalUtils = GoalUtils (BEnvNS)
  open BEnvNS
  open BAll
  open NOps

  (* --- *)

  type goal = nexpr one_step_goal
  let pp_goal = pp_goal pp_nexpr
  let pp_goals = pp_goals pp_nexpr

  (* --- *)

  let goal_support (evols: equs) (g: goal) =
    POps.support (of_nexpr (goal g //.. evols))

  let goals_support (evols: equs) =
    let gs = goal_support evols in function
      | [] -> failwith "Missing optimization goal(s)"
      | [g] -> gs g
      | g::l -> List.fold_left (fun s g -> PSette.union (gs g) s) (gs g) l

  (* --- *)

  let one_step cc'_vmap (evolutions: equs) (g: goal) k =

    let (c_vars, c'_vars), prime_cvars =
      let cc' = PMappe.bindings cc'_vmap in List.split cc', subst_vars cc' in

    Log.d3 logger "K = @[%a@]" pp_bexpr k;

    let k' = of_bexpr k |> prime_cvars |> to_bexpr in
    Log.d3 logger "K'= K[c/c']";

    let rel, _ = GoalUtils.relation_of_goal g in

    let t = goal g //.. evolutions in
    let t' = to_nexpr (prime_cvars (of_nexpr t)) in
    Log.d3 logger "@<4>T ⇔ @[%a@]" pp_nexpr t;
    Log.d3 logger "@<11>T'⇔ T[c/c']";

    let r = rel t' t in
    Log.d3 logger "R = @[%a@]" print r;

    let k = k &&~ !~(exist c'_vars (k' &&~ r)) in
    (* let k = simplify k in *)
    Log.d  logger "K@[ = @[%a@]%t@]" print k (log3 "@ (= K ∩ ¬(∃c' (K' ∩ R)))");

    k

end

(* -------------------------------------------------------------------------- *)
