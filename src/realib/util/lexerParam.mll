(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)

{
  open ParserParam
  open Lexing
}

rule param = parse
  eof    { TK_EOF }
| ","   { TK_COMMA }
| ":"   { TK_COLON }
| "="   { TK_EQUAL }
| "{"   { str' 0 "" lexbuf }
| ( ['_' 'A'-'Z' 'a'-'z' '0'-'9' '-' '+'] ) * { TK_ID (lexeme lexbuf) }
| _ { raise Parse.Lex_error }

and param' = parse
  eof    { TK_EOF }
| ","   { TK_COMMA }
| ":"   { TK_COLON }
| "="   { TK_EQUAL }
| "\""  { str "" lexbuf }
| "{"   { str' 0 "" lexbuf }
| ( ['_' 'A'-'Z' 'a'-'z' '0'-'9' '.' ] ) * { TK_ID (lexeme lexbuf) }
| ( ['_' 'A'-'Z' 'a'-'z' '0'-'9' '.' '-' '(' ')' '/' ' ' ] ) * { TK_STR (lexeme lexbuf) }
| _ { raise Parse.Lex_error }

and str s = parse
| "\"" { TK_STR s }
| _   { str (s^(lexeme lexbuf)) lexbuf }
| eof { raise Parse.Lex_error }

and str' level s = parse
| "}" { if level=0 then TK_STR s
        else str' (level-1) (s^"}") lexbuf }
| "{" { str' (level+1) (s^"{") lexbuf }
| _   { str' level (s^(lexeme lexbuf)) lexbuf }
| eof { raise Parse.Lex_error }
