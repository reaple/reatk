(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)

(** ReaVer verification main loop *)

(** {2 Utilities and types for the verification loop} *)

(** informal description of strategies *)
type descr = Format.formatter -> unit

type dom_spec
type trans_spec
type analysis_spec

(** verification strategy *)
type stratspec =
  | Trans of trans_spec
  | Analysis of analysis_spec

(** verification strategies *)
type strategy = stratspec list

type spec

(** invalid verification strategy *)
exception InvalidStrategy of (Format.formatter -> unit)

(* (\** invalid verification strategy option *\) *)
(* exception InvalidStrategyOption of string *)

(** invalid verification strategy option value *)
exception InvalidStrategyOptionValue of string * string

(** invalid domain description *)
exception InvalidDomain of string

(** parse verification strategies *)
val str_to_strategy : Env.t -> string -> spec

(** print available verification strategies *)
val print_strategies : Format.formatter -> unit -> unit

(** print available abstract domains *)
val print_domains : Format.formatter -> unit -> unit

(** parses and instanciates the given abstract domain descriptor *)
val parse_domain : ?force_power:bool -> ?forbid_pwext:bool -> Env.t -> string ->
  dom_spec

val mk_domain : Env.t -> dom_spec -> (module Domain.T)

type pwext_spec
val parse_pwext: Env.t -> ParseParams.options_t -> pwext_spec
val mk_pwext_params: pwext_spec -> (module DomExt.FP_PARAMS)

(** {2 verification loop} *)

(** run the given verification strategies *)
val run : Env.t ->  spec ->  Program.cfprog_t ->
  Analysis.result * Analysis.result_to_bddapron_t * Program.cfprog_t
