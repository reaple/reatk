(** Extraction of {{:../realib/index.html}[Realib]} data-flow programs,
    equations and controllers into Controllable-nbac nodes, functions and
    predicates.

    @author Nicolas Berthier *)

open Bddapron.Apronexpr
open Bddapron.Apronexpr.Condition
open Realib
open CtrlNbac.AST
open Program
module Expr0 = Bddapron.Expr0
module BE = Bddapron.Env
module BC = Bddapron.Cond
module Apronexpr = Bddapron.Apronexpr

let (&) f a = f a
let level = Log.Info
let logger = Log.mk ~level "Rl2cn"

(* -------------------------------------------------------------------------- *)

module O: sig

  val extract_ctrlr
    : ?avoid_labels: Env.vars_t -> Env.t -> CtrlSpec.t -> Synthesis.controller
    -> 'f pred
  val extract_ctrlf
    : ?avoid_labels: Env.vars_t -> Env.t -> CtrlSpec.t -> Env.equs_t list
    -> 'f func
  val extract_ctrln
    : ?avoid_labels: Env.vars_t -> Env.t -> CtrlSpec.t -> Program.dfprog_t
    -> 'f node

  module Open: sig

    type 'f data
    val mk_data: ?avoid_labels: Env.vars_t -> Env.t -> 'f data
    val reset_data: 'f data -> unit
    val mk_equs_map: Env.equs_t -> Env.expr_t SMap.t
    val mk_equs_maps: Env.equs_t list -> Env.expr_t SMap.t list

    val translate_symb: Env.Symb.t -> symb
    val translate_label: Env.Symb.t -> label
    val translate_typ: Env.typ -> typ
    val translate_typdef: 'f data -> Env.typdef -> 'f typdef
    val translate_typdefs: 'f data -> 'f typdefs

    val translate_bexp: 'f data -> Env.boolexpr_t -> 'f bexp
    val translate_eexp: 'f data -> Env.var_t Expr0.Benum.t -> 'f eexp
    val translate_biexp: 'f data -> Env.var_t Expr0.Bint.t -> 'f nexp
    val translate_nexp: 'f data -> Env.numexpr_t -> 'f nexp
    val translate_exp: 'f data -> Env.expr_t -> 'f exp

    val declare_input_vars
      : 'f data -> (('f, [> 'f input_var_spec ]) decls as 'd)
      -> Env.vars_t -> group
      -> 'd

    val declare_contr_vars
      : 'f data -> (('f, [> 'f contr_var_spec ]) decls as 'd)
      -> Env.vars_t -> CtrlSpec.default_expr list -> Env.vars_t -> group
      -> 'd

    val declare_uc_groups
      : 'f data -> (('f, [> 'f input_var_spec |  'f contr_var_spec ]) decls as 'd)
      -> CtrlSpec.t
      -> 'd

    val declare_output_vars
      : 'f data -> (('f, [> 'f output_var_spec ]) decls as 'd)
      -> Env.vars_t -> group -> Env.expr_t SMap.t
      -> 'd

    val declare_state_vars
      : 'f data -> (('f, [> 'f state_var_spec ]) decls as 'd)
      -> Env.vars_t -> Env.expr_t SMap.t
      -> 'd

    val declare_locals
      : 'f data -> (('f, [> 'f local_var_spec ]) decls as 'd)
      -> 'd

  end

end = struct

  type var = Env.var_t                                             (* shortcuts *)
  type atyp = [ `Int | `Real ]
  type bityp = [ `Bint of bool * int ]
  type regtyp' = [ `Benum of var | `Bint of bool * int ]
  type regtyp = RegNone | RegVar of regtyp'

  type (* ( *)'f(* , 'x) *) dd =
      {
        env: Env.t;
        (* mutable decls: ('f, 'x) decls; *)
        bdd2s: (var Expr0.Bool.t, symb) Hashtbl.t;
        edd2s: (var Expr0.Benum.t, symb) Hashtbl.t;
        bidd2s: (var Expr0.Bint.t, symb) Hashtbl.t;
        vdd2s: (var Expr0.Apron.t, symb * atyp) Hashtbl.t;
        (* aprontree2s: (var Tree.t, symb * atyp) PHashhe.t; *)
        aproncond2s: (var Condition.t, symb) PHashhe.t;
        bexp2s: ('f bexp, symb) Hashtbl.t;                          (* for terms *)
        eexp2s: ('f eexp, symb) Hashtbl.t;
        nexp2s: ('f nexp, symb * atyp) Hashtbl.t;
        biexp2s: ('f nexp * bityp, symb) Hashtbl.t;
        regidxs: regtyp array;
        avoid_labels: var PSette.t;
        locnum: int ref;
        mutable locals: (symb * typ * 'f exp) list;
      }

  let translate_symb s = Env.Symb.to_string s |> mk_symb

  let translate_label l = translate_symb l |> mk_label

  let translate_typ = function
    |(`Bool | `Int | `Real | `Bint _) as t -> t
    | `Benum l -> `Enum (mk_typname (translate_symb l))

  let declare_new_local ({ locnum } as dd) typ exp =
    let l = mk_fresh_symb ~cnt_ref:locnum "l" in
    dd.locals <- (l, typ, exp) :: dd.locals;
    l

  (* - 1: roughtly 1 operation per definition
     - 2: slightly more elaborated definitions allowed
     - 3: even more… *)
  let max_level = 1

  let rec reg_me_at: int -> 'f bexp -> bool = function
    | l when l <= 0 -> fun _ -> true
    | l ->
        let r  = reg_me_at (pred l)
        and rn = reg_me_at_num (pred l) in
        function
          | `Ref _ | `Bool _ | `Buop (_, `Ref _) -> false
          | `Buop (_, e) -> r e
          | `Bnop (_, e, f, l) -> r e || r f || List.exists r l  (* TODO: count! *)
          | `Bbop (_, e, f) | `Bcmp (_, e, f) -> r e || r f
          | `Ncmp (_, e, f) -> rn e || rn f
          | `Ecmp _ | `Pcmp _
          | `Pin _ | `Bin _ | `Ein _ | `BIin _ -> true
          | `Flag (_, e) -> reg_me_at l e
          | #cond -> true

  and reg_me_at_num: int -> 'f nexp -> bool = function
    | l when l <= 0 -> fun _ -> true
    | l ->
        let r = reg_me_at_num (pred l) in
        function
          | `Ref _ | `Int _ | `Real _ | `Mpq _
          | `Nuop (_,(`Ref _ | `Int _))
          | `Luop (_,(`Ref _ | `Int _))
          | `Lsop (_,(`Ref _ | `Int _), _) -> false
          | `Ncst (_, _, e)
          | `Nuop (_, e) | `Luop (_, e) | `Lsop (_, e, _) -> r e
          | `Nnop (_, e, f, l) -> r e || r f || List.exists r l  (* TODO: count! *)
          | `Lbop (_, e, f) -> r e || r f
          | `Flag (_, e) -> reg_me_at_num l e
          | #cond -> true

  (* do not name constants. *)
  let regb ?var dd reg (bexp: 'f bexp) : 'f bexp = match var with
    | None when reg_me_at max_level bexp ->
        let l = declare_new_local dd `Bool (`Bexp bexp) in reg l; mk_bref' l
    | None -> bexp
    | Some v when (bexp = `Bool true) || (bexp = `Bool false) -> bexp
    | Some v -> reg v; bexp

  let rege ?var dd reg tn eexp = match var with
    | None -> let l = declare_new_local dd tn (`Eexp eexp) in reg l; mk_eref' l
    | Some v -> reg v; eexp

  type ('f, 't) nnexp = 'f nexp * ('t * int)

  let regn ?var dd reg ((e: 'f nexp), (typ, l)) : ('f, 't) nnexp = match var with
    | None when l >= max_level ->
        let l = declare_new_local dd (typ :> typ) (`Nexp e) in
        reg l; mk_nref' l, (typ, 0)
    | None -> e, (typ, l)
    | Some v when l = 0 -> e, (typ, l)                (* do not name constants *)
    | Some v -> reg v; e, (typ, l)

  (* --- *)

  let zeros = mk_nicst' 0
  let ones  = mk_lnot' zeros
  let rec mk_land' ~fullmsk a b =
    if a == zeros || b == zeros then zeros
    else if a == ones then b
    else if b == ones then a
    else match a, b with
      | `Int 0, x | x, `Int 0 -> zeros
      | `Int i, x when i = fullmsk -> x
      | x, `Int i when i = fullmsk -> x
      | `Luop (`LNot, `Int 0), x | x, `Luop (`LNot, `Int 0) -> x
      | a, `Luop (`LNot, b) when a = b -> zeros
      | `Luop (`LNot, b), a when a = b -> zeros
      | `Lbop (`LOr, a, `Int am), `Lbop (`LOr, b, `Int bm) when a = b ->
          mk_lor' ~fullmsk a (`Int (am land bm))
      | a, b when a = b -> a
      | `Int x, `Int y -> `Int (x land y)
      | a, b -> CtrlNbac.AST.mk_land' a b
  and mk_lor' ~fullmsk a b =
    if a == ones || b == ones then ones
    else if a == zeros then b
    else if b == zeros then a
    else match a, b with
      | `Int 0, x | x, `Int 0 -> x
      | `Int i, x when i = fullmsk -> ones
      | x, `Int i when i = fullmsk -> ones
      | (`Luop (`LNot, `Int 0) as x), _ | _, (`Luop (`LNot, `Int 0) as x) -> x
      | a, `Luop (`LNot, b) when a = b -> ones
      | `Luop (`LNot, b), a when a = b -> ones
      | `Lbop (`LAnd, a, `Int am), `Lbop (`LAnd, b, `Int bm) when a = b ->
          mk_land' ~fullmsk a (`Int (am lor bm))
      | a, b when a = b -> a
      | `Int x, `Int y -> `Int (x lor y)
      | a, b -> CtrlNbac.AST.mk_lor' a b

  let mk_fullmsk ~signed width =
    assert (width > 0 && width <= 31);                                   (* XXX *)
    let r = Bdd.Reg.Minterm.to_int ~signed (Array.make width Cudd.Man.True) in
    (* Log.d3 logger "fullmsk = %d" r; *)
    r

  (* --- *)

  let allow_label { avoid_labels } l = not (PSette.mem l avoid_labels)

  (* --- *)

  let trboolvar v = mk_bref' (translate_symb v)
  let trenumvar v = mk_eref' (translate_symb v)
  let trnumvar v = mk_nref' (translate_symb v)
  let trenumcst l = mk_ecst' (translate_label l)

  let trmpq q : ('f, atyp) nnexp =
    let num, den = Mpqf.to_mpzf2 q in
    if Mpzf.cmp_int den 1 = 0
    then let f = Mpqf.to_float q in
         let i = int_of_float f in
         if f = float_of_int i
         then mk_nicst' i, (`Int, 0)
         else mk_nrcst' f, (`Real, 0)
    else `Mpq q, (`Real, 0)

  let mpqnull q = Mpqf.cmp_int q 0 = 0

  let trrevcmp = function
    | SUPEQ -> `Le
    | SUP -> `Lt
    | EQ | EQMOD _ -> `Eq
    | DISEQ -> `Ne

  let trcmp = function
    | SUPEQ -> `Ge
    | SUP -> `Gt
    | EQ | EQMOD _ -> `Eq
    | DISEQ -> `Ne

  type term = var Bddapron.Apronexpr.Lin.term

  let atyp_of_var dd v : atyp = match BE.typ_of_var dd.env.Env.env v with
    | (`Real | `Int) as t -> t
    | _ -> failwith "Numerical expected"

  let atyp_coerce (t1: atyp) (t2: atyp) : atyp = match t1, t2 with
    | `Int, `Int -> `Int | _ -> `Real

  (* --- *)

  let bchkreg ?var dd e =
    try mk_bref' (Hashtbl.find dd.bexp2s e)
    with Not_found -> regb ?var dd (Hashtbl.add dd.bexp2s e) e

  let bchkreg_lazy ?var dd f b =
    try mk_bref' (Hashtbl.find dd.bdd2s b)
    with Not_found -> regb ?var dd (Hashtbl.add dd.bdd2s b) (f b)

  let echkreg ?var dd tn e =
    try mk_eref' (Hashtbl.find dd.eexp2s e)
    with Not_found -> rege ?var dd (Hashtbl.add dd.eexp2s e) tn e

  let echkreg_lazy ?var dd f tn e =
    try mk_eref' (Hashtbl.find dd.edd2s e)
    with Not_found -> rege ?var dd (Hashtbl.add dd.edd2s e) tn (f e)

  let nchkreg ?var dd ((e, (t, l)) as r: ('f, atyp) nnexp) =
    try let s, t = Hashtbl.find dd.nexp2s e in mk_nref' s, (t, 0)
    with Not_found -> regn ?var dd (fun v -> Hashtbl.add dd.nexp2s e (v, t)) r

  let nchkreg_lazy ?var dd f n =
    try let s, t = Hashtbl.find dd.vdd2s n in mk_nref' s, (t, 0)
    with Not_found -> let (e, (t, l)) as r = f n in
                     regn ?var dd (fun v -> Hashtbl.add dd.vdd2s n (v, t)) r

  (* --- *)

  let bichkreg' ?var dd t (e, l) =
    try let s = Hashtbl.find dd.biexp2s (e, t) in mk_nref' s, 0
    with Not_found -> let reg = Hashtbl.add dd.biexp2s (e, t) in
                     let e, (_, l) = regn ?var dd reg (e, (t, l)) in
                     e, l

  let bichkreg ?var dd : bityp -> 'f nexp -> 'f nexp * int = fun typ ->
    let reg ?var = bichkreg' ?var dd typ in
    let rec tr ?var : 'f nexp -> 'f nexp * int = function
      |(`Ref _ | `Int _
           | `Luop (`LNot, (`Ref _ | `Int _))) as x -> reg ?var (x, 0)
      | `Luop (op, e) -> let e, l = tr e in
                        reg ?var (`Luop (op, e), l + 0)
      | `Lbop (op, e, f) -> let e, l = tr e and f, l' = tr f in
                           reg ?var (`Lbop (op, e, f), l + l' + 1)
      | `Lsop (op, e, s) -> let e, l = tr e in
                           reg ?var (`Lsop (op, e, s), l + 1)
      | `Ite (c, e, f) -> let e, l = tr e and f, l' = tr f in
                         reg ?var (`Ite (c, e, f), l + l' + 2)
      | _ -> failwith "Internal error in Bounded Integer extraction"
    in
    fun expr -> tr ?var expr (* |> fst *)

  let bichkreg_lazy ?var dd f t n =
    try let s = Hashtbl.find dd.bidd2s n in mk_nref' s
    with Not_found -> let reg = Hashtbl.add dd.bidd2s n in
                     let e, l = f n in
                     let e, (_, l) = regn ?var dd reg (e, (t, l)) in
                     e

  (* --- *)

  let trterm ?var dd ((coeff, v): term) : ('f, atyp) nnexp =
    let e, (typ, _) = trmpq coeff in
    let e, l = match e with
      | `Int 0 as c -> c, 0
      | `Int 1 -> trnumvar v, 0
      | `Int (-1) -> mk_opp' (trnumvar v), 0
      | c -> mk_mul' c (trnumvar v) [], 1
    in
    nchkreg ?var dd (e, (atyp_coerce typ (atyp_of_var dd v), l))

  let trsumterms ?var dd : term list -> ('f, atyp) nnexp = function
    | [] -> mk_nicst' 0, (`Int, 0)                                  (* XXX ??? *)
    | [ x ] -> trterm ?var dd x
    | x::y::tl ->
        let accum_term ?var (e, (t, l)) x =
          let e', (t', l') = trterm dd x in
          let e = mk_sum' e e' [] and t = atyp_coerce t t' and l = l + l' in
          nchkreg dd (e, (t, l))
        in
        accum_term ?var (List.fold_left accum_term (trterm dd y) tl) x

  let traproncoeff c =
    let open Apron.Coeff in
    let open Apron.Scalar in
    match reduce c with
      | Scalar (Float f) -> `Real f, (`Real, 0)
      | Scalar (Mpqf f) -> trmpq f
      | Scalar (Mpfrf f) -> trmpq (Mpfrf.to_mpqf f)
      | Interval _ -> failwith "Unsupported interval coefficient!"

  let traprontree ?var dd : var Tree.t -> ('f, atyp) nnexp =
    let rec trtree ?var e =
      (* try let s, t = PHashhe.find dd.aprontree2s e in (mk_nref' s, t), 0 *)
      (* with Not_found -> *)
      let open Tree in
      let res = match e with
        | Cst c -> traproncoeff c
        | Var v when BE.typ_of_var dd.env.Env.env v = `Real -> trnumvar v, (`Real, 0)
        | Var v when BE.typ_of_var dd.env.Env.env v = `Int -> trnumvar v, (`Int, 0)
        | Var v -> assert false
        | Unop (Neg, e, _, _) ->
            let e, (t, l) = trtree e in mk_opp' e, (t, l + 1)
        | Binop (op, e, e', _, _) ->
            let e, (t, l) = trtree e and e', (t', l') = trtree e' in
            let e = match op with
              | Add -> mk_sum' e e' []
              | Sub -> mk_sub' e e' []
              | Mul -> mk_mul' e e' []
              | Div -> mk_div' e e' []
              | Mod -> failwith "Unsupported modulus operation!"
              | Pow -> failwith "Unsupported power operation!"
            in e, (atyp_coerce t t', l + l')
        | e -> failwith (Format.asprintf "Unsupported tree expression: %a!"
                          (print dd.env.Env.env.Bdd.Env.symbol) e)
      in
      nchkreg ?var dd res
    (* regn ?var dd (fun v -> PHashhe.add dd.aprontree2s e (v, typ)) nexp typ l *)
    in
    trtree ?var

  let trapronexpr ?var dd (e: var Apronexpr.t) : ('f, atyp) nnexp =
    match e with
      | Lin { Lin.lterm = lterms; Lin.cst = cst } when mpqnull cst ->
          trsumterms ?var dd lterms
      | Lin { Lin.lterm = lterms; Lin.cst = cst } ->
          let cst, (t, l) = trmpq cst and trm, (t', l') = trsumterms dd lterms in
          let sum = mk_sum' cst trm [] and t = atyp_coerce t t' and l = l + l' in
          nchkreg ?var dd (sum, (t, l))
      (* | Lin l -> traprontree ?var dd (tree_of_lin l) *)
      | Poly p -> traprontree ?var dd (tree_of_poly p)
      | Tree t -> traprontree ?var dd t

  let neglinterms: var Lin.term list -> term list =
    List.map (fun (a, x) -> Mpqf.neg a, x)

  let traproncond' dd : var Condition.t -> 'f bexp = fun c ->
    let trterm x = trterm dd x |> fst in
    let trsumterms x = trsumterms dd x |> fst in
    match c with
      | cmp, Lin { Lin.lterm = [ a, x ]; Lin.cst = cst }
            when Mpqf.sgn a > 0 ->
          (* x + c >= 0   ==>   x >= -c *)
          `Ncmp (trcmp cmp, trterm (a, x), trmpq (Mpqf.neg cst) |> fst)

      | cmp, Lin { Lin.lterm = [ a, x ]; Lin.cst = cst } ->
          (* -x + c >= 0   ==>   x >= c *)
          `Ncmp (trrevcmp cmp, trterm (Mpqf.neg a, x), trmpq cst |> fst)

      | cmp, Lin { Lin.lterm = [ a, x; b, y ]; Lin.cst = cst }
            when mpqnull cst && Mpqf.sgn a < 0 && Mpqf.sgn b > 0 ->
          (* -x + y + 0 >= 0   ==>   x <= y *)
          `Ncmp (trrevcmp cmp, trterm (Mpqf.neg a, x), trterm (b, y))

      | cmp, Lin { Lin.lterm = [ a, x; b, y ]; Lin.cst = cst }
            when mpqnull cst && Mpqf.sgn a > 0 && Mpqf.sgn b < 0 ->
          (* x + -y + 0 >= 0   ==>   x >= y *)
          `Ncmp (trcmp cmp, trterm (a, x), trterm (Mpqf.neg b, y))

      | cmp, Lin { Lin.lterm = lterm; Lin.cst = cst }
            when Mpqf.sgn cst < 0 ->
          (* x + .... + z - 2 >= 0   ==>   x + ... + z >= 2 *)
          `Ncmp (trcmp cmp, trsumterms lterm, trmpq (Mpqf.neg cst) |> fst)

      | cmp, Lin { Lin.lterm = lterm; Lin.cst = cst }
            when Mpqf.sgn cst > 0 ->
          (* x + ... + z + 2 >= 0   ==>  - x - ... - z <= 2 *)
          `Ncmp (trrevcmp cmp, trsumterms (neglinterms lterm), trmpq cst |> fst)

      | cmp, e -> let sum, (t, l) = trapronexpr dd e in match t with
          | `Int -> `Ncmp (trcmp cmp, sum, mk_nicst' 0)
          | `Real -> `Ncmp (trcmp cmp, sum, mk_nrcst' 0.)

  let traproncond ?var dd : var BC.cond -> 'f bexp = fun (`Apron c) ->
    try mk_bref' (PHashhe.find dd.aproncond2s c) with Not_found ->
      regb ?var dd (PHashhe.add dd.aproncond2s c) (traproncond' dd c)

  (* --- *)

  let trbterm ?var dd : var Bdd.Expr0.O.Expr.term -> 'f bexp =
    let open Bdd.Expr0.O.Expr in
    let trb = trboolvar and tre = trenumvar and trn = trnumvar in
    let trec = trenumcst and trni = mk_nicst' in
    function
      | Tatom (Tbool (v,  true)) -> trboolvar v
      | Tatom (Tbool (v, false)) -> mk_neg' (trb v)
      | Tatom (Tenum (v, labels)) ->
          (match List.filter (allow_label dd) labels with
            | [] -> assert false
            | [l] -> mk_eeq' (tre v) (tre l)
            | l::ls -> mk_ein' (tre v) (trec l) (List.map trec ls))
      | Tatom (Tint (v, [   ])) -> assert false
      | Tatom (Tint (v, [ i ])) -> mk_neq' (trn v) (trni i)
      | Tatom (Tint (v, i::is)) -> let v = trn v in
                                  List.fold_left
                                    (fun a i -> mk_or' a (mk_neq' v & trni i))
                                    (mk_neq' v & trni i) is
      | Tcst b -> mk_bcst' b
      | Texternal (v, p) ->
          let condidb = dd.env.Env.cond.Bdd.Cond.condidb in
          traproncond ?var dd (PDMappe.x_of_y (v, p) condidb)

  (* --- *)

  open Cudd.Bdd

  let trid ?var dd polar idx : 'f bexp =
    let term = Bdd.Expr0.O.Expr.term_of_idcondb dd.env.Env.env (idx, polar) in
    bchkreg ?var dd (trbterm ?var dd term)

  (* --- *)

  (* XXX: what could we do with such a structure, in the case of bounded
     integers? *)
  (* module Diet = struct *)
  (*   (\** Discrete Interval Encoding Tree *\) *)
  (*   (\* TODO: make it somehow balanced (using AVL properties maybe) *\) *)

  (*   (\* translated from *)
  (*      `http://web.engr.oregonstate.edu/~erwig/diet/diet.sml.txt' *\) *)

  (*   type t = *)
  (*     | Empty *)
  (*     | Node of int * int * t * t *)

  (*   let empty = Empty *)

  (*   let rec split_max x y l r = match r with *)
  (*     | Empty -> x, y, l *)
  (*     | Node (rx, ry, rl, rr) -> *)
  (*         let u, v, r' = split_max rx ry rl rr in u, v, Node (x, y, l, r') *)

  (*   let rec split_min x y l r = match l with *)
  (*     | Empty -> x, y, r *)
  (*     | Node (lx, ly, ll, lr) -> *)
  (*         let u, v, l' = split_min lx ly ll lr in u, v, Node (x, y, l', r) *)

  (*   let join_left x y l r = match l with *)
  (*     | Empty -> Node (x, y, l, r) *)
  (*     | Node (lx, ly, ll, lr) -> *)
  (*         let x', y', l' = split_max lx ly ll lr in *)
  (*         if y' + 1 = x then Node (x', y, l', r) else Node (x, y, l, r) *)

  (*   let join_right x y l r = match r with *)
  (*     | Empty -> Node (x, y, l, r) *)
  (*     | Node (rx, ry, rl, rr) -> *)
  (*         let x', y', r' = split_min rx ry rl rr in *)
  (*         if y + 1 = x' then Node (x, y', l, r') else Node (x, y, l, r) *)

  (*   let rec insert z = function *)
  (*     | Empty -> Node (z, z, Empty, Empty) *)
  (*     | Node (x, y, l, r) when z < x && z + 1 = x -> join_left z y l r *)
  (*     | Node (x, y, l, r) when z < x             -> Node (x, y, insert z l, r) *)
  (*     | Node (x, y, l, r) when z > y && z - 1 = y -> join_right x z l r *)
  (*     | Node (x, y, l, r) when z > y             -> Node (x, y, l, insert z r) *)
  (*     | t -> t *)

  (*   let rec mem z = function *)
  (*     | Empty -> false *)
  (*     | Node (x, y, l, r) -> (z >= x && z <= y) || (z < x && mem z l) || (mem z r) *)

  (* end *)

  (* --- *)

  module type LEAF = sig
    type 'f t
    val pp: Format.formatter -> 'f t -> unit
    val eq: 'f t -> 'f t -> bool
    val merge: 'f t -> 'f t -> 'f t
  end

  module Bddmap (E: Realib.Env.T) (L: LEAF) = struct
    module NS = BddapronUtil.BEnvNS (E.BEnv)
    open NS.BAll
    include Bddapron.Bddleaf

    let pp_elem fmt { guard; leaf } =
      Format.fprintf fmt "%a -> %a" pp_bexpr guard L.pp leaf

    let pp fmt = function
      | [] -> Format.fprintf fmt "[]"
      | [e] -> Format.fprintf fmt "[@[%a@]]" pp_elem e
      | e::el ->(Format.fprintf fmt "[@[%a" pp_elem e;
                List.iter (Format.fprintf fmt "@\n%a" pp_elem) el;
                Format.fprintf fmt "@]]")

    let cons guard bit expr acc =
      let cons = cons_disjoint
        ~is_equal:(* ((\* MappeI.equal  *\)(=)) *)L.eq
        ~merge:(* ((\* MappeI.merge *\) mk_lor') *)L.merge
      in
      let elem = { guard; leaf = expr } in
      (* Log.d3 logger "cons: @[<v>%a@]" pp_elem elem; *)
      let acc = cons elem acc in
      (* Log.d3 logger " res: @[<v>%a@]" pp acc; *)
      acc

  end

  (* --- *)

  let tt = mk_bcst' true
  let ff = mk_bcst' false

  let trb ?var dd (e: var Expr0.Bool.t) : 'f bexp =

    let rec trb ?var : var Expr0.Bool.t -> 'f bexp =
      bchkreg_lazy ?var dd begin fun b -> match Cudd.Bdd.inspect b with
        | Bool true -> tt
        | Bool false -> ff
        | Ite (id, _, _) when (id < Array.length dd.regidxs
                               && dd.regidxs.(id) <> RegNone) ->
            trbreg id b
        | Ite (id, t, e) when is_cst t || is_cst e ->
            let tt = is_true t and tf = is_false t
            and et = is_true e and ef = is_false e in
            if (tt && ef) || (tf && et)
            then trid ?var dd (tt || ef) id
            else let c = trid dd (tt || ef) id in
                 if      tt then mk_or'  c (trb e)
                 else if tf then mk_and' c (trb e)
                 else if et then mk_or'  c (trb t)
                 else (* ef *)   mk_and' c (trb t)
        | Ite (id, t, e) ->
            mk_bcond' (trid dd true id) (trb t) (trb e)
      end

    and trbreg: int -> var Expr0.Bool.t -> 'f bexp = fun id b ->
      let env = dd.env.Env.env in
      let cond = dd.env.Env.cond in
      let var = PMappe.find id env.Bdd.Env.id2var in

      (* Quite expensive stuff actually... *)
      let trbdiscrete eq_elem elems guard : 'f bexp =
        let tbl = Hashtbl.create (Array.length elems) in

        Array.iter
          (fun elem ->
            let b' = Expr0.Bool.cofactor b (eq_elem elem) in
            try Hashtbl.replace tbl b' (elem :: Hashtbl.find tbl b') with
              | Not_found -> Hashtbl.add tbl b' [ elem ])
          elems;

        (* Find a big set of labels, and remove it from the table; its key is
           used as fallback later. *)
        let belse, _ = Hashtbl.fold
          (fun bdd elems (big, size) ->
            let size' = List.length elems in
            if size' > size then bdd, size' else big, size)
          tbl (Expr0.Bool.dtrue env cond, 0)
        in
        Hashtbl.remove tbl belse;

        Hashtbl.fold
          (fun bdd elems e -> mk_bcond' (guard elems) (trb bdd) e |> bchkreg dd)
          tbl (trb belse)
      in

      let trbenumv: _ -> 'f bexp = fun tname ->
        let v = Expr0.Benum.var env cond var in
        let labels = Bdd.Enum.labels_of_typ env tname in
        trbdiscrete (Expr0.Benum.eq_label env cond v) (Bdd.Labels.as_array labels)
          (let v = trenumvar var in
           fun labels ->
             let labels = List.filter (allow_label dd) labels in
             match List.sort env.Bdd.Env.symbol.Bdd.Env.compare labels with
               | [] -> (* assert false *)ff                             (* ??? *)
               | [ l ] -> mk_eeq' v (trenumcst l)
               | h::t -> mk_ein' v (trenumcst h) (List.map trenumcst t))
      in

      let trbintv: bool -> width -> 'f bexp = fun signed width ->
        let man = env.Bdd.Env.cudd in
        let tid = PMappe.find var env.Bdd.Env.vartid in
        let v = trnumvar var in
        let fullmsk = mk_fullmsk ~signed width in

        let _pp_minterm =
          Util.array_print (fun fmt -> function
            | Cudd.Man.False -> Format.pp_print_char fmt '0'
            | Cudd.Man.True -> Format.pp_print_char fmt '1'
            | Cudd.Man.Top -> Format.pp_print_char fmt 'X')
        in

        let mk_cond (minterm: Bdd.Reg.Minterm.t) acc =
          let mk_msk = Array.map (function
            | Cudd.Man.Top -> Cudd.Man.False | _ -> Cudd.Man.True)
          and mk_res = Array.map (function
            | Cudd.Man.Top -> Cudd.Man.False | b -> b)
          in
          let msk = mk_msk minterm and res = mk_res minterm in
          (* if Array.for_all ((=) Cudd.Man.True) msk then *)
          (*   let res = Bdd.Reg.Minterm.to_int ~signed res in *)
          (*   mk_neq' v (mk_nicst' res) *)
          (* else *)
          let msk = Bdd.Reg.Minterm.to_int ~signed msk
          and res = Bdd.Reg.Minterm.to_int ~signed res in
          (* Log.d3 logger "msk=%d, res=%d" msk res; *)
          let msk'= mk_land' ~fullmsk v (mk_nicst' msk) in
          (* (\* let msk, _ = nchkreg dd (msk, (`Bint (signed, width), 1)) in *\) *)
          let res = bchkreg dd (mk_neq' msk' (mk_nicst' res)) in
          let res = try res :: MappeI.find msk acc with Not_found -> [res] in
          MappeI.add msk res acc
        in

        (* Log.d logger ">>> b = %a" (Expr0.Bool.print env cond) b; *)
        (* Log.d logger "var: %a; type: width=%u, signed=%B" Env.Symb.print var width signed; *)
        Log.d3 logger "++ var: %a" Env.Symb.print var;
        let guard2disj = Bdd.Reg.fold_minterms_in_guard man tid
          begin fun minterm guard acc ->
            Log.d3 logger " minterm=%a, guard=@[%a@]"
              _pp_minterm minterm (Expr0.Bool.print env cond) guard;
            let msks' = try PMappe.find guard acc with Not_found -> MappeI.empty in
            PMappe.add guard ((* bchkreg dd ( *)mk_cond minterm(* ) :: *) msks') acc
            (* let msks' = try PMappe.find guard acc with Not_found -> [] in *)
            (* PMappe.add guard ((\* bchkreg dd *\) (mk_cond minterm) :: msks') acc *)
          end b (PMappe.empty Stdlib.compare)
        in

        let mk_disj' = function
          | [] -> assert false
          | [c] -> c
          | c::tl -> bchkreg dd (mk_disj' c tl)
        in

        PMappe.fold begin fun guard disj acc ->
          Log.d3 logger " guard: %a" (Expr0.Bool.print env cond) guard;
          let c = MappeI.fold begin fun msk res_list acc ->
            (* let res_list = List.map begin fun res -> *)
            (*   mk_neq' (mk_land' ~fullmsk v (mk_nicst' msk)) (mk_nicst' res) *)
            (* end res_list in *)
            mk_or' (mk_disj' res_list) acc
          end disj (mk_bcst' false) in
          mk_bcond' c (trb guard) acc
        end guard2disj (mk_bcst' false)
      in

      match dd.regidxs.(id) with
        | RegVar (`Benum tname) -> trbenumv tname
        | RegVar (`Bint (s, w)) -> trbintv s w
        | RegNone -> assert false
    in

    trb ?var e

  (* --- *)

  let tre ?var dd : var Expr0.Benum.t -> 'f eexp = fun e ->
    let tn = translate_typ (`Benum (Env.Symb.mk e.Bdd.Enum.typ)) in
    echkreg_lazy ?var dd begin fun e ->
      match (Bdd.Enum.guardlabels dd.env.Env.env e
                |> List.filter (fun (g, l) -> allow_label dd l)) with
        | [] -> failwith "nil value for enum!"
        | [_, l] -> trenumcst l
        | gls ->
            let gls, _ = List.fold_left begin fun (gls, (pcond, first)) (g, l) ->
              ((tdrestrict g pcond, l, first) :: gls,
               (dand pcond (dnot g), false))
            end ([], (dtrue (Env.cuddman dd.env), true)) gls in
            List.fold_left begin fun expr (g, l, first) ->
              let guard = trb dd g in
              let expr = mk_econd' guard (trenumcst l) expr in
              echkreg ?var:(if first then var else None) dd tn expr
            end (trenumcst (let _, l, _ = List.hd gls in l)) (List.tl gls)
    end tn e

  (* --- *)

  let trbi ?var dd (e: var Expr0.Bint.t) : 'f nexp =
    let signed = e.Bdd.Int.signed in
    let width = Array.length e.Bdd.Int.reg in
    let bichkreg = bichkreg ?var dd (`Bint (signed, width)) in

    bichkreg_lazy ?var dd begin fun e ->

      let fullmsk = mk_fullmsk ~signed width in
      let mk_land' a = mk_land' ~fullmsk a
      and mk_lor' a = mk_lor' ~fullmsk a in

      let module E: Env.T = (val Realib.Env.as_module dd.env) in
      let module NS = BddapronUtil.BEnvNS (E.BEnv) in
      let module Bddmap = Bddmap (E) (struct
        type 'f t = 'f CtrlNbac.AST.nexp
        let pp = (* MappeI.print Format.pp_print_int *) CtrlNbac.AST.print_nexp
        let eq =  (* MappeI.equal *) (=)
        let merge = (* CtrlNbac.AST. *)mk_lor'
      end) in
      let open Bddmap in
      let open NS.BEnv in
      let open NS.BAll in

      let rec trbi' bitidx guard bdd acc =
        let ite id t e =
          let var = Cudd.Bdd.ithvar (Env.cuddman dd.env) id in
          let acc = trbi' bitidx (guard &&~   var) t acc in
          let acc = trbi' bitidx (guard &&~ !~var) e acc in
          acc
        in
        let selectbit typ tid v id t e = match typ with
          | `Bint (s, w) when w = width ->
              let rec bit_in_tid = function
                | i when i >= Array.length tid -> assert false
                | i when tid.(i) = id -> i
                | i -> bit_in_tid (i + 1)
              in
              let bitidx' = bit_in_tid 0 in
              let shift e =
                let s = bitidx - bitidx' in
                if s = 0 then e
                else if s > 0 then mk_lsl' e (`Int s)
                else mk_lsr' e (`Int (-s))
              in
              let acc = cons (guard &&~ t) bitidx (shift v) acc in
              let acc = cons (guard &&~ e) bitidx (shift (mk_lnot' v)) acc in
              acc
          | _ -> ite id t e
        in
        match Cudd.Bdd.inspect bdd with
          | Bool true -> cons guard bitidx ones acc
          | Bool false -> cons guard bitidx zeros acc
          | Ite (id, t, e) when id >= Array.length dd.regidxs -> ite id t e
          | Ite (id, t, e) -> match dd.regidxs.(id) with
              | RegVar typ ->
                  let var = PMappe.find id env.Bdd.Env.id2var in
                  let tid = PMappe.find var env.Bdd.Env.vartid in
                  let v = trnumvar var in
                  selectbit typ tid v id t e
              | _ -> ite id t e
      in

      let bitmap, _ = Array.fold_left begin fun (bitmap, bitidx) bdd ->
        (* Log.d3 logger "bdd.%u: %a" bitidx pp_bexpr bdd; *)
        let map = (* cons tt bitidx zeros *) [] in
        (trbi' bitidx tt bdd map :: bitmap, bitidx + 1)
      end ([], 0) e.Bdd.Int.reg in
      let bitmap = List.rev bitmap in

      let e, _ = List.fold_left begin fun (e, bitidx) map ->
        (* Log.d3 logger "map.%u=@[<h>%a@]" bitidx Bddmap.pp map; *)
        let bit = List.fold_left begin fun bit { guard; leaf } ->
          if Cudd.Bdd.is_true guard then leaf
          else if Cudd.Bdd.is_false guard then bit
          else mk_ncond' (trb dd guard) leaf bit
        end zeros map in
        (* Log.d3 logger "bit.%u=@[<h>%a@]" bitidx CtrlNbac.AST.print_nexp bit; *)
        let msk = mk_nicst' (1 lsl bitidx) in
        mk_lor' e (mk_land' bit msk), bitidx + 1
      end (zeros, 0) bitmap in

      bichkreg e
    end (`Bint (signed, width)) e

  (* --- *)

  let trn ?var dd (e: var Expr0.Apron.t) : 'f nexp =
    let rec trn ?var : var Expr0.Apron.t -> ('f, atyp) nnexp =
      let open Cudd.Mtbdd in
      nchkreg_lazy ?var dd begin fun n -> match inspect n with
        | Leaf u -> trapronexpr ?var dd (get u)
        | Ite (v, t, e) -> let v = trid dd true v in
                          let t, (typ, l)  = trn t and e, (typ', l') = trn e in
                          (mk_ncond' v t e, (atyp_coerce typ typ', l + l' + 2))
      end
    in
    trn ?var e |> fst

  (* --- *)

  let trp ?var dd : var Expr0.t -> 'f exp = function
    | `Bool e -> `Bexp (trb ?var dd e)
    | `Benum e -> `Eexp (tre ?var dd e)
    | `Bint e -> `Nexp (trbi ?var dd e)
    | `Apron e -> `Nexp (trn ?var dd e)

  (* ------------------------------------------------------------------------ *)

  module Open = struct

    type 'f data = 'f dd

    let translate_symb = translate_symb
    let translate_label = translate_label
    let translate_typ = translate_typ

    let translate_typdef { avoid_labels } = function
      | `Benum la ->
          let labels =
            Bdd.Labels.fold
              (fun l x -> if PSette.mem l avoid_labels then x else l :: x) la []
          in
          mk_etyp (List.map translate_label labels)

    let translate_typdefs dd =
      Env.fold_typdefs dd.env
        (fun tn td -> declare_typ (translate_symb tn) (translate_typdef dd td))
        empty_typdefs

    let translate_bexp dd = trb ?var:None dd
    let translate_eexp dd = tre ?var:None dd
    let translate_biexp dd = trbi ?var:None dd
    let translate_nexp dd = trn ?var:None dd
    let translate_exp dd = trp ?var:None dd

    let declare_locals dd decls =
      List.fold_left
        (fun decls (l, typ, exp) ->
          SMap.add l (typ, `Local (exp, None), None) decls)
        decls dd.locals

    let declare_input_vars dd decls vars group =
      List.fold_left (fun decls v ->
        let typ = translate_typ & Env.typof dd.env v in
        SMap.add (translate_symb v) (typ, `Input group, None) decls)
        decls vars

    let declare_contr_vars dd decls bvars bdefaults nvars group =
      let vtyp v = translate_symb v, translate_typ (Env.typof dd.env v) in
      let decls = List.fold_left2
        (fun (decls, rank) v default ->
          let var, typ = vtyp v in
          let default = match default with
            | `Bool bexpr -> `Expr (`Bexp (trb dd bexpr))
            | `Order way -> (way :> _ default_spec)
          in
          (SMap.add var (typ, `Contr (group, rank, default), None) decls,
           succ rank))
        (decls, one) bvars bdefaults |> fst in
      List.fold_left
        (fun (decls, rank) v ->
          let var, typ = vtyp v in
          (SMap.add var (typ, `Contr (group, rank, `None), None) decls,
           succ rank))
        (decls, one) nvars |> fst

    let declare_uc_groups dd decls cs =
      CtrlSpec.fold_uc_groups
        (fun (decls, group) ({ CtrlSpec.u_vars = u_vars;
                             CtrlSpec.bc_vars = bc_vars;
                             CtrlSpec.bp_specs = bp_specs;
                             CtrlSpec.nc_vars = nc_vars; }) ->
          let decls = declare_input_vars dd decls u_vars group in
          let decls = declare_contr_vars dd decls bc_vars bp_specs nc_vars
            group in
          decls, succ group)
        (decls, one) cs |> fst

    let declare_output_vars dd decls vars group equs =
      List.fold_left
        (fun decls v ->
          let typ = translate_typ & Env.typof dd.env v in
          let var = translate_symb v in
          let exp = trp dd (SMap.find var equs) in
          SMap.add var (typ, `Output (group, exp, None), None) decls)
        decls vars

    let declare_state_vars dd decls vars equs =
      List.fold_left
        (fun decls v ->
          let typ = translate_typ & Env.typof dd.env v in
          let var = translate_symb v in
          let exp = try trp dd (SMap.find var equs) with
            | Not_found -> `Ref var
          in
          SMap.add var (typ, `State (exp, None), None) decls)
        decls vars

    (* ---------------------------------------------------------------------- *)

    let mk_data ?(avoid_labels = []) ({ Env.env = e } as env) =
      let regidxs = Array.make (Bdd.Idx.nxt e.Bdd.Env.idx) RegNone in
      PMappe.iter begin fun v idxs -> match BE.typ_of_var e v with
        | (`Benum _ | `Bint _) as t ->
            Array.iter (fun idx -> regidxs.(idx) <- RegVar t) idxs
        | _ -> ()
      end e.Bdd.Env.vartid;

      let avoid_labels =
        List.fold_left (fun s l -> PSette.add l s)
          (PSette.empty e.Bdd.Env.symbol.Bdd.Env.compare) avoid_labels in

      (* let aprontree_eq a b = Tree.compare e.Bdd.Env.symbol a b = 0 in *)
      let aproncond_eq a b = Condition.compare e.Bdd.Env.symbol a b = 0 in
      {
        env;
        (* decls = SMap.empty; *)
        bdd2s = Hashtbl.create 100;                          (* XXX tune sizes? *)
        edd2s = Hashtbl.create 100;
        bidd2s = Hashtbl.create 100;
        vdd2s = Hashtbl.create 100;
        (* aprontree2s = PHashhe.create Hashtbl.hash aprontree_eq 100; *)
        aproncond2s = PHashhe.create Hashtbl.hash aproncond_eq 100;
        bexp2s = Hashtbl.create 100;
        eexp2s = Hashtbl.create 100;
        nexp2s = Hashtbl.create 100;
        biexp2s = Hashtbl.create 100;
        regidxs;
        avoid_labels;
        locnum = ref 0;
        locals = [];
      }

    let reset_data dd =
      Hashtbl.clear dd.bdd2s;
      Hashtbl.clear dd.edd2s;
      Hashtbl.clear dd.bidd2s;
      Hashtbl.clear dd.vdd2s;
      PHashhe.clear dd.aproncond2s;
      Hashtbl.clear dd.bexp2s;
      Hashtbl.clear dd.eexp2s;
      Hashtbl.clear dd.nexp2s;
      Hashtbl.clear dd.biexp2s;
      dd.locnum := 0;
      dd.locals <- []

    let mk_equs_map equs =
      List.fold_left (fun acc (v, e) -> SMap.add (translate_symb v) e acc)
        SMap.empty equs

    let mk_equs_maps equs =
      List.rev & List.fold_left (fun acc equs -> mk_equs_map equs :: acc) [] equs

  end

  open Open

  (* ------------------------------------------------------------------------ *)

  (** Extraction of a controller as a predicate *)
  let extract_ctrlr
      ?avoid_labels
      ({ Env.s_vars = s_vars } as env) cs
      bexpr
      =
    let dd = mk_data ?avoid_labels env in
    let decls = (SMap.empty :> 'f pred_decls) in
    (* state vars come in the first I/O group *)
    let decls = declare_input_vars dd decls s_vars one in
    let decls = declare_uc_groups dd decls cs in
    let pn_typs = translate_typdefs dd in
    let pn_value = trb dd bexpr in
    let pn_decls = declare_locals dd decls in
    `Desc { pn_typs; pn_decls; pn_value }

  (* --- *)

  (** Extraction of a triangularized controller as a function *)
  let extract_ctrlf
      ?avoid_labels
      ({ Env.s_vars = s_vars } as env) cs
      equs_list
      =
    let dd = mk_data ?avoid_labels env in
    let decls = (SMap.empty :> 'f func_decls) in
    (* state vars come in the first I/O group *)
    let decls = declare_input_vars dd decls s_vars one in
    let decls, _, _ = CtrlSpec.fold_uc_groups
      (fun (decls, equs, group) ({ CtrlSpec.u_vars = u_vars;
                                 CtrlSpec.c_vars = c_vars }) ->
        let equs_map = List.hd equs in
        let decls = declare_input_vars dd decls u_vars group in
        let decls = declare_output_vars dd decls c_vars group equs_map in
        decls, List.tl equs, succ group)
      (decls, mk_equs_maps equs_list, one) cs
    in
    let fn_typs = translate_typdefs dd in
    let fn_decls = declare_locals dd decls in
    `Desc { fn_typs; fn_decls; fn_assertion = mk_bcst' true }

  (* --- *)

  (** Extraction of a data-flow program *)
  let extract_ctrln
      ?avoid_labels
      ({ Env.env = e; Env.cond = c; Env.s_vars = s_vars } as env) cs
      ({ Program.d_disc_equs = equs;
         Program.d_init      = init;
         Program.d_final     = final;
         Program.d_ass       = assertion;
         Program.d_reach     = reachable;
         Program.d_attract   = attractive; })
      =
    let dd = mk_data ?avoid_labels env in
    let decls = (SMap.empty :> 'f node_decls) in
    let decls = declare_state_vars dd decls s_vars (mk_equs_map equs) in
    let decls = declare_uc_groups dd decls cs in
    let trb = trb dd in
    let trb' e = Some (trb e) in
    let dnot = Expr0.Bool.dnot e c in
    let cn_init       = trb  init in                         (* allow locals? *)
    let cn_assertion  = trb  assertion in                    (* ibid. *)
    let cn_invariant  = trb' (dnot final) in                 (* . *)
    let cn_reachable  = trb' reachable in                    (* . *)
    let cn_attractive = trb' attractive in                   (* . *)
    let cn_typs = translate_typdefs dd in
    let cn_decls = declare_locals dd decls in
    `Desc { cn_typs; cn_decls; cn_init; cn_assertion;
            cn_invariant; cn_reachable; cn_attractive }

  (* --- *)

end

(* -------------------------------------------------------------------------- *)

include O
