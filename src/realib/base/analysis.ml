(******************************************************************************)
(* analysis *)
(* analysis interface *)
(* author: Peter Schrammel *)
(* version: 0.9.0 *)
(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)
(******************************************************************************)

(******************************************************************************)
(** {2 Analysis Module Type } *)
(******************************************************************************)

type direction_t = ApronAccel.direction_t

type bddapron_res_t = (Cfg.locid_t, (Env.boolexpr_t * ApronUtil.linconss) list) Mappe.t
type refine_loc_t = refine_bool:bool -> Cfg.locid_t -> Loc.t -> Loc.t option
type print_result_t =  Format.formatter -> unit
type result_to_bddapron_t = unit -> bddapron_res_t
type result = bool
type t = Env.t -> Program.cfprog_t ->
  (result * refine_loc_t * print_result_t * result_to_bddapron_t)

let bddapron_res_empty = Mappe.empty
let conclusive r = r
let pp_result fmt = Format.fprintf fmt "%B"
let pp_dir fmt = function
  | `Forward -> Format.pp_print_string fmt "forward"
  | `Backward -> Format.pp_print_string fmt "backward"

module type T =
  sig
    type param
    val analyze : param -> t
    val descr: param -> Format.formatter -> unit
  end
