module type T = sig
  include Graph.Sig.ORDERED_TYPE_DFT
  val sum: t -> t -> t
  val min: t -> t -> t
  val to_string: t -> string
  val print: Format.formatter -> t -> unit
end

module I = struct
  type t = int
  let compare = (-)
  let default = 0
  let sum a b = let r = a + b in
                if a > 0 && b > 0 && r < 0 then max_int
                else if a < 0 && b < 0 && r > 0 then min_int
                else r
  let min = Stdlib.min
  let to_string = string_of_int
  let print = Format.pp_print_int
end

module F = struct
  type t = float
  let compare = Stdlib.compare
  let default = 0.0
  let sum = (+.)
  let min = Stdlib.min
  let to_string = string_of_float
  let print = Format.pp_print_float
end

(* --- *)

module type CONVERTION = sig
  type src
  type dst
  val convert: src -> dst
end

module FOfI = struct
  type src = int
  type dst = float
  let convert = float_of_int
end
