(* Deadlock due to convexity of invariant: `sS:d=I' succeeds but the
   controller is invalid. `sD:d=I' should fail due to the condition `x
   <= 0' in state Down (hence inescapably followed by a decrement of
   x). *)

!typedef
  State = enum { Up, Down };

!state
  s:State;
  x:int;

!input
  _c: bool;

!controllable
  c ? _c:bool;

!transition
  s' = if s=Up and c then Down else
       if s=Down and x <= 0 then Up else
       s;
  x' = if s=Up then x+1 else x-1;

!initial x=5 and s=Up;

!invariant 0<=x and x<=10;
