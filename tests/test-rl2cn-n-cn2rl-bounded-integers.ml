open Format
open Rutils
open Realib
open Cn2rl
open Rl2cn
open CtrlNbac
open AST

let () = Log.globallevel := Log.Debug3
let print_node fmt = print_node fmt;;

(* let emptysmap = PMappe.empty Env.Symb.compare *)

let check_node node =
  match check_node node with | Some n, _ -> n | _ -> assert false

let output_node_equs node =
  let env, cs, df(* , locals *) = match translate_node (check_node node) with
    | `TOk r -> r
    | _ -> assert false
  in
  printf "equations:@\n%a@." (Env.print_equations env) df.Program.d_disc_equs;
;;

let test_node node =
  printf "input node:@\n%a@." print_node node;
  let env, cs, df(* , locals *) = match translate_node (check_node node) with
    | `TOk r -> r
    | _ -> assert false
  in
  printf "equations:@\n%a@." (Env.print_equations env) df.Program.d_disc_equs;
  let node' = extract_ctrln env cs df in
  (* printf "output node:@\n%a@." print_node node'; *)
  let node'' = check_node node' in
  printf "output node (checked):@\n%a@." print_node node'';
  node''
;;

let tt = mk_bcst' true and ff = mk_bcst' false;;
let s, w = false, 2;;
let i = Symb.of_string "i";;
let o = Symb.of_string "value";;
(* let oe = mk_sum (mk_nref o) (mk_nbicst s w 1) [];; *)
(* let oe = mk_lnot (mk_nref o);; *)
let oe = mk_lsl (mk_nref o) (flag () (mk_nicst 1));;
let cn_decls: _ node_decls = begin SMap.empty
    (* |> SMap.add i (`Bint (s, w), `Input one, None) *)
    |> SMap.add o (`Bint (s, w), `State (oe, None), None)
end;;
let () =
  let n' = test_node (`Desc { cn_typs = empty_typdefs;
                              cn_decls;
                              cn_init = mk_neq' (mk_nref' o) (mk_nicst' 0);
                              cn_assertion = tt;
                              cn_invariant = None;
                              cn_reachable = None;
                              cn_attractive = None; }) in
  output_node_equs n'
;;
