(* This file is part of ReaTK released under the GNU GPL.  Please read the
   LICENSE file packaged in the distribution *)

open Format

(* -------------------------------------------------------------------------- *)

type 'a fmt = ('a, formatter, unit) format
type ufmt = unit fmt
type relfmt = (ufmt -> ufmt -> unit) fmt

type ('a, 'x) xfmt = ('a -> 'x, formatter, 'x) format

module P: sig
  type pu = formatter -> unit
  type 'a pp = formatter -> 'a -> unit
  val pu: formatter -> unit
  val pc: char -> formatter -> unit
  val ps: string -> formatter -> unit
  val pp: 'x fmt -> formatter -> 'x
  val pp1: ('a -> 'x) fmt -> 'a -> formatter -> 'x
  val pp2: ('a -> 'b -> 'x) fmt -> 'a -> 'b -> formatter -> 'x
  val pp3: ('a -> 'b -> 'c -> 'x) fmt -> 'a -> 'b -> 'c -> formatter -> 'x
  val pp4: ('a -> 'b -> 'c -> 'd -> 'x) fmt -> 'a -> 'b -> 'c -> 'd -> formatter -> 'x
  val pp1': ('a -> unit) fmt -> 'a option -> formatter -> unit
  val pl: ?fopen:ufmt -> ?fsep:ufmt -> ?fclose:ufmt -> ?fempty:ufmt ->
    (formatter -> 'r -> 's) -> 'r list -> formatter -> unit
end
include module type of P

val pp_lst: ?fopen:ufmt -> ?fsep:ufmt -> ?fclose:ufmt -> ?fempty:ufmt ->
  (formatter -> 'r -> 's) -> formatter -> 'r list -> unit

val ufmt_from_string: string -> ufmt
val ufmt_from_breakable_string: string -> ufmt
(** See {Scanf.format_from_string} for details. *)

(* -------------------------------------------------------------------------- *)

(** {2 Printing utilities} *)

val array_print : ?csep:string -> ?copen:string -> ?cclose:string ->
  (formatter -> 'a -> unit) -> formatter -> 'a array -> unit

val list_print : ?csep:string -> ?copen:string -> ?cclose:string ->
  (formatter -> 'a -> unit) -> formatter -> 'a list -> unit
val list_print' :
  ?csep:(formatter -> unit) ->
  ?copen:(formatter -> unit) ->
  ?cclose:(formatter -> unit) ->
  (formatter -> 'a -> 'b) -> formatter -> 'a list -> unit

(** prints a string such that linebreaks happen at whitespaces *)
val print_breakable : formatter -> string -> unit

(** prints a string such of a fixed length
    (truncates or fills up with whitespaces *)
val print_fixed : formatter -> int -> string -> unit
