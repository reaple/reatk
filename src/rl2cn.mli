(* Interface documentation is in `rl2cn.ml' only. *)
(** *)
open Realib
open CtrlNbac
open AST

(* -------------------------------------------------------------------------- *)

val extract_ctrlr
  : ?avoid_labels: Env.vars_t -> Env.t -> CtrlSpec.t -> Synthesis.controller
  -> 'f pred

(* -------------------------------------------------------------------------- *)

val extract_ctrlf
  : ?avoid_labels: Env.vars_t -> Env.t -> CtrlSpec.t -> Env.equs_t list
  -> 'f func

(* -------------------------------------------------------------------------- *)

val extract_ctrln
  : ?avoid_labels: Env.vars_t -> Env.t -> CtrlSpec.t -> Program.dfprog_t
  -> 'f node

(* -------------------------------------------------------------------------- *)

module Open: sig

  type 'f data
  val mk_data: ?avoid_labels: Env.vars_t -> Env.t -> 'f data
  val reset_data: 'f data -> unit
  val mk_equs_map: Env.equs_t -> Env.expr_t SMap.t
  val mk_equs_maps: Env.equs_t list -> Env.expr_t SMap.t list

  val translate_symb: Env.Symb.t -> symb
  val translate_label: Env.Symb.t -> label
  val translate_typ: Env.typ -> typ
  val translate_typdef: 'f data -> Env.typdef -> 'f typdef
  val translate_typdefs: 'f data -> 'f typdefs

  val translate_bexp: 'f data -> Env.boolexpr_t -> 'f bexp
  val translate_eexp: 'f data -> Env.var_t Bddapron.Expr0.Benum.t -> 'f eexp
  val translate_biexp: 'f data -> Env.var_t Bddapron.Expr0.Bint.t -> 'f nexp
  val translate_nexp: 'f data -> Env.numexpr_t -> 'f nexp
  val translate_exp: 'f data -> Env.expr_t -> 'f exp

  val declare_input_vars
    : 'f data -> (('f, [> 'f input_var_spec ]) decls as 'd)
    -> Env.vars_t -> group
    -> 'd

  val declare_contr_vars
    : 'f data -> (('f, [> 'f contr_var_spec ]) decls as 'd)
    -> Env.vars_t -> CtrlSpec.default_expr list -> Env.vars_t -> group
    -> 'd

  val declare_uc_groups
    : 'f data -> (('f, [> 'f input_var_spec |  'f contr_var_spec ]) decls as 'd)
    -> CtrlSpec.t
    -> 'd

  val declare_output_vars
    : 'f data -> (('f, [> 'f output_var_spec ]) decls as 'd)
    -> Env.vars_t -> group -> Env.expr_t SMap.t
    -> 'd

  val declare_state_vars
    : 'f data -> (('f, [> 'f state_var_spec ]) decls as 'd)
    -> Env.vars_t -> Env.expr_t SMap.t
    -> 'd

  val declare_locals
    : 'f data -> (('f, [> 'f local_var_spec ]) decls as 'd)
    -> 'd

end

(* -------------------------------------------------------------------------- *)
