(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)

(** transformation implementation: general transformations *)

(** partitions by initial, final and other states *)
val initfinal : Ops.trans

(** partitions by the given set of predicates *)
val manual : Env.boolexpr_t list -> Ops.trans

(** partitions by enumerating the values of the given Booean variables *)
val enumerate : Env.var_t list -> Ops.trans

(** refines the transition functions by the destination location
    (computes the arc assertions) *)
val refine_by_destloc : Ops.trans

(** boolean backward bisimulation refinement *)
val boolbacksim : Ops.trans

(** removes boolean inputs and splits arcs *)
val bool_inputs_to_guards : Ops.trans

(** splits non-convex guards and transitions *)
val split_arcs : Ops.trans
