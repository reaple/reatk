(* Interface documentation is in `AST.ml' only. *)
(**)

module Make (Symb: SymbSig.T) : (ASTSig.T with module Symb = Symb)
include ASTSig.T with module Symb = Symb
