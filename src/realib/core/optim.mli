(** *)

(** {2 One-Step Optimization} *)

type one_step_param
val make_o1_param
  : Env.numexpr_t BddapronOptim.one_step_goal list
  -> one_step_param

module OneStep (E: Env.T) :
  (Optimization.T with type param = one_step_param)

(** {2 K-Step Optimization} *)

type k_step_goal
type k_step_kind =
  [
  | `Kth
  | `Sum
  ]
type k_variant =
  [
  | `Strict
  | `Pessimistic
  ]
val make_ok_goal
  : Env.numexpr_t BddapronOptim.optim_goal
  -> int
  -> k_step_kind
  -> k_step_goal
type k_step_param
val make_ok_param
  : ?dirty:bool
  -> ?cudd_dr:bool
  -> ?best_effort: bool
  -> k_variant
  -> k_step_goal
  -> k_step_param

module KStep (E: Env.T) :
  (Optimization.T with type param = k_step_param)

(** {2 Discounted Optimization} *)

type discount = Mpqf.t
and ustoch_spec =
    {
      up_inputs: Env.vset;
      up_spec: probability_spec;
    }
and probability_spec =
  [
  | `Samples of Env.numexpr_t
  ]
type d_param
val make_od_param
  : ?dirty:bool
  -> ?approx:bool
  -> ?dthr: discount
  -> ?kmax: int
  -> rp:BddapronReorderPolicy.config
  -> discount
  -> ?ustoch: ustoch_spec
  -> Env.numexpr_t BddapronOptim.optim_goal
  -> d_param

module Discounted (E: Env.T) :
  (Optimization.T with type param = d_param)
