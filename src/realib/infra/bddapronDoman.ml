(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)

open Format
open Bddapron

(* --- *)

module type T = sig
  module N: ApronDoman.T
  type aprondom = N.dom
  type 'a bddapronman
  type ('a, 'b) t = ('a, aprondom, 'a bddapronman, 'b) Domain0.man

  val make : unit -> ('a, 'b) t * aprondom Apron.Manager.t
  val descr: Format.formatter -> unit
end

(* --- *)

module BddO = struct
  open Bdddomain0
  open Bddleaf

  let print ?(print_apron=ApronUtil.pp_num0) env fmt = function
    | { list = [] } -> fprintf fmt "@<1>⊥"
    | { list } ->
        let pp_bdd = Bdd.Expr0.O.print_bdd env
        and pp_apron = print_apron (Env.string_of_aprondim env) in
        Print.list ~first:"@<1>⟦@[<hov 0>" ~sep:",@ " ~last:"@]@<1>⟧"
          begin fun fmt { guard; leaf } ->
            if Bdd.Expr0.O.Bool.is_true env guard
            then fprintf fmt "@[<hv 0>%a@]" pp_apron leaf
            else fprintf fmt "@[<hv 0>%a@;@[<hv>@<1>⟼@ %a@]@]\
                             " pp_bdd guard pp_apron leaf
          end fmt list

  let make make_numdoman =
    let numdoman = make_numdoman () in
    let bman = Domain0.make_bdd numdoman in
    (Domain0.man_of_bdd { bman with Domain0.print = print },
     numdoman)
end

module MtbddO = struct
  open ApronDD

  let print ?(print_apron=ApronUtil.pp_num0) =
    Mtbdddomain0.print ~print_apron

  let make make_numdoman =
    let numdoman = make_numdoman () in
    let bman = Domain0.make_mtbdd numdoman in
    (Domain0.man_of_mtbdd { bman with Domain0.print = print },
     numdoman)
end

(* --- *)

module MakeBdd = functor (N: ApronDoman.T) -> (struct
  module N = N
  type aprondom = N.dom
  type 'a bddapronman = ('a, aprondom) Domain0.bdd
  type ('a, 'b) t = ('a, aprondom, 'a bddapronman, 'b) Domain0.man

  let make () = BddO.make N.make
  let descr fmt = fprintf fmt "%t@ with@ BDDs" N.descr
end : T)

module MakeMtbdd = functor (N: ApronDoman.T) -> (struct
  module N = N
  type aprondom = N.dom
  type 'a bddapronman = ('a, aprondom) Domain0.mtbdd
  type ('a, 'b) t = ('a, aprondom, 'a bddapronman, 'b) Domain0.man

  let make () = MtbddO.make N.make
  let descr fmt = fprintf fmt "%t@ with@ MTBDDs" N.descr
end : T)

(* --- *)

module LoosePol = MakeBdd (ApronDoman.LoosePol)
module StrictPol = MakeBdd (ApronDoman.StrictPol)
module LoosePolMt = MakeMtbdd (ApronDoman.LoosePol)
module StrictPolMt = MakeMtbdd (ApronDoman.StrictPol)
module Oct = MakeBdd (ApronDoman.Oct)
module OctMt = MakeMtbdd (ApronDoman.Oct)
module Box = MakeBdd (ApronDoman.Box)
module BoxMt = MakeMtbdd (ApronDoman.Box)

(* --- *)
