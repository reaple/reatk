open Format
open Graph
open CtrlNbac
open AST

(* -------------------------------------------------------------------------- *)

module Make (W: Weight.T) = struct

  module W = W
  module G = Imperative.Digraph.ConcreteBidirectionalLabeled (Symb.M) (W)

  module ToDot =
    Graphviz.Dot (struct
      include G
      let graph_attributes _ = []
      let default_vertex_attributes _ = []
      let vertex_name v =
        asprintf "\"%a\"" Symb.print v
      let vertex_attributes _ = []
      let get_subgraph _ = None
      let default_edge_attributes _ = []
      let edge_attributes e = [
        `Label (W.to_string (E.label e));
      ]
    end)

  include G

  let add_edge g v l v' =
    if v <> v' then
      try let e = find_edge g v v' in
          remove_edge_e g e;
          add_edge_e g (E.create v (W.sum (E.label e) l) v')
      with Not_found -> add_edge_e g (E.create v l v')

  let filter_vertexes symbs g =
    let rec close_pairs_self = function
      | [] | [_] -> ()
      | (v1, l1) :: tl ->
          List.iter begin fun (v2, l2) ->
            add_edge g v1 ((* W.sum *)min l1 l2) v2;
          end tl;
          close_pairs_self tl;
    and close_pairs_self' = function
      | [] | [_] -> ()
      | (v1, l1) :: tl ->
          List.iter begin fun (v2, l2) ->
            add_edge g v2 ((* W.sum *)min l1 l2) v1;
          end tl;
          close_pairs_self' tl;
    in
    iter_vertex begin fun v ->
      if not (SSet.mem v symbs) then
        begin
          (* eprintf "Eliminating vertex %a@." SPair.print v; *)
          let edges_from_v = succ_e g v in
          let dests = List.map begin fun e ->
            remove_edge_e g e;
              (* eprintf "> Destination is %a@." SPair.print (E.dst e); *)
            E.dst e, E.label e
          end edges_from_v in
          let edges_to_v = pred_e g v in
          let srcs = List.map begin fun e ->
            remove_edge_e g e;
            E.src e, E.label e
          end edges_to_v in
          remove_vertex g v;
          close_pairs_self dests;
          close_pairs_self' srcs;
        end
    end g

end

(* --- *)

module SharedBits = struct

  include Make (Weight.I)

  let revmap m =
    SMap.fold (fun v -> SSet.fold begin fun v' acc ->
      assert (not (SMap.mem v' acc));
      SMap.add v' v acc
    end) m SMap.empty

  let make symb_typ_size trans_deps keep affinities =
    let all_deps = SMap.fold (fun _ -> SSet.union) trans_deps SSet.empty in
    let omni_deps = SSet.diff all_deps keep in
    let omni_deps = SMap.fold (fun _ -> SSet.inter) trans_deps omni_deps in
    let trans_deps = SMap.map (fun s -> SSet.diff s omni_deps) trans_deps in
    let affinities' = revmap affinities in
    let sdeps = create () in
    SMap.iter begin fun o deps ->
      let os = symb_typ_size o in
      if os > 0 then
        let o = try SMap.find o affinities' with Not_found -> o in
        SSet.iter begin fun i ->
          let is = symb_typ_size i in
          if is > 0 then
            let i = try SMap.find i affinities' with Not_found -> i in
            add_edge sdeps i (W.min os is) o
        end deps
    end trans_deps;
    sdeps

end

(* --- *)
