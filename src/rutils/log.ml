(******************************************************************************)
(* Log *)
(* generic logging module *)
(* authors: Peter Schrammel & Nicolas Berthier *)
(* This file is part of ReaTK released under the GNU GPL.  Please read the
   LICENSE file packaged in the distribution *)
(******************************************************************************)

open Format

type level = Error | Warn | Info | Debug | Debug2 | Debug3

let fmt = err_formatter
type logger = {
  module_name: string;
  level: level;
}

let mk ?(level=Info) name = { module_name = name; level }

(* everything with less or equal verbosity will be logged,
if enabled in individual loggers *)
let globallevel = ref (Info)

(* the log level of all modules with less verbosity will be weakened
to this value (i.e. made more verbose) *)
let globallevel_weaken = ref (Error)

let pp_time fmt = fprintf fmt "%.3f" (Sys.time ())
let pp_time' fmt = fprintf fmt "%.1f" (Sys.time ())

let level2char = function
  | Error -> 'E'
  | Warn -> 'W'
  | Info -> 'I'
  | Debug -> 'D'
  | Debug2 -> 'd'
  | Debug3 -> '.'

let short_levels = [ "D"; "W"; "I"; "D1"; "D2"; "D3" ]

let string2level = function
  | "ERROR"  | "Error"  | "error"  | "E"  | "e" -> Error
  | "WARN"   | "Warn"   | "warn"   | "W"  | "w" -> Warn
  | "INFO"   | "Info"   | "info"   | "I"  | "i" -> Info
  | "DEBUG"  | "Debug"  | "debug"  | "D"  | "d"
  | "DEBUG1" | "Debug1" | "debug1" | "D1" | "d1" -> Debug
  | "DEBUG2" | "Debug2" | "debug2" | "D2" | "d2" -> Debug2
  | "DEBUG3" | "Debug3" | "debug3" | "D3" | "d3" -> Debug3
  | _ -> raise Exit

let level2int = function
  | Error -> 1
  | Warn -> 2
  | Info -> 3
  | Debug -> 4
  | Debug2 -> 5
  | Debug3 -> 6

let level_geq l1 l2 = (level2int l1)>=(level2int l2)

let check_level logger level =
  (level_geq logger.level level) && (level_geq !globallevel level) ||
  (level_geq !globallevel_weaken level)

let log logger level msg =
  if check_level logger level then
    fprintf fmt "[%t %c %s] %s@."
      pp_time (level2char level) logger.module_name msg

let log_o logger level print_o msg obj  =
  if check_level logger level then
    fprintf fmt "[%t %c %s] %s %a@."
      pp_time (level2char level) logger.module_name msg print_o obj

let debug2 logger msg = log logger Debug2 msg
let debug2_o logger print_o msg obj = log_o logger Debug2 print_o msg obj
let debug3 logger msg = log logger Debug3 msg
let debug3_o logger print_o msg obj = log_o logger Debug3 print_o msg obj
let debug logger msg = log logger Debug msg
let debug_o logger print_o msg obj = log_o logger Debug print_o msg obj
let info logger msg = log logger Info msg
let info_o logger print_o msg obj = log_o logger Info print_o msg obj
let warn logger msg = log logger Warn msg
let warn_o logger print_o msg obj = log_o logger Warn print_o msg obj
let error logger msg = log logger Error msg
let error_o logger print_o msg obj = log_o logger Error print_o msg obj

(* Some aliases: *)
let e' = error
let w' = warn
let i' = info
let d' = debug
let d2' = debug2
let d3' = debug3

(* We're ok thanks to OCaml's global lock. *)
let lock = ref false
let try_lock x = if !x then false else (x := true; true)
let unlock x = assert !x; x := false

let force_lock x = let x' = !x in x := true; x'
let restore_lock x x' = x := x'

(* Format-based polymorphic logging functions *)

type 'a fmtlog = logger -> ('a, formatter, unit, unit, unit, unit) format6 -> 'a

let log level : 'a fmtlog = fun logger ->
  if check_level logger level then
    let lock' = force_lock lock in
    kfprintf
      (kfprintf (fun f -> pp_print_newline f (); restore_lock lock lock'))
      fmt "[%t %c %s] " pp_time (level2char level) logger.module_name
  else
    ifprintf fmt                                (* ignore all extra arguments *)

let e: 'a fmtlog = fun logger -> log Error logger
let w: 'a fmtlog = fun logger -> log Warn logger
let i: 'a fmtlog = fun logger -> log Info logger
let d: 'a fmtlog = fun logger -> log Debug logger
let d2: 'a fmtlog = fun logger -> log Debug2 logger
let d3: 'a fmtlog = fun logger -> log Debug3 logger

(* unit cont *)

type 'a contlog = logger -> ('a, formatter, unit) format -> formatter -> 'a

let log_cont level : 'a contlog = fun logger f fmt ->
  if check_level logger level then fprintf fmt f else ifprintf fmt f

let ci: 'a contlog = fun logger -> log_cont Info logger
let cd: 'a contlog = fun logger -> log_cont Debug logger
let cd2: 'a contlog = fun logger -> log_cont Debug2 logger
let cd3: 'a contlog = fun logger -> log_cont Debug3 logger

(* Rotating mark... *)

let roll_first = 5.0
let roll_period = 0.33
let roll_states = "|/-\\"
let roll_active = ref false

(* let lock = Mutex.create () *)
(* let try_lock = Mutex.try_lock *)
(* let unlock = Mutex.unlock *)

let roll ?(first = roll_first) ?(period = roll_period) ?(level = Info) f g =
  if !roll_active || not (level_geq !globallevel level) then f g else
    let open Unix in
    let state = ref 0 in
    Sys.set_signal Sys.sigalrm (Sys.Signal_handle (fun _ ->
      if try_lock lock then begin
        fprintf fmt "[%t] %c \r@?" pp_time' (String.get roll_states !state);
        state := succ !state mod (String.length roll_states);
        unlock lock;
      end;
    ));
    let leave x =
      roll_active := false;
      ignore (setitimer ITIMER_REAL { it_value = 0.0;
                                      it_interval = 0.0 });
      x
    in
    let o = setitimer ITIMER_REAL { it_value = first;
                                    it_interval = period } in
    roll_active := true;
    assert (o.it_value = 0.0);
    try f g |> leave with e -> leave (); raise e

(* Transient info message... *)

let tf' ?(level = Info) msg = if !globallevel >= level && try_lock lock then begin
  fprintf fmt "[%t] %s\r@?" pp_time (Lazy.force msg);
  unlock lock;
end
let tf ?level msg = tf' ?level (lazy msg)

let tlog level ?p logger =
  if check_level logger level && try_lock lock then
    kfprintf
      (kfprintf (fun f -> (match p with
        | Some pl when check_level logger pl -> pp_print_newline f ();
        | _ -> fprintf f "\r@?");
        unlock lock))
      fmt "[%t %c %s] " pp_time (level2char level) logger.module_name
  else
    ifprintf fmt                                (* ignore all extra arguments *)

let ti ?p logger = tlog Info ?p logger
let td ?p logger = tlog Debug ?p logger
let td2 ?p logger = tlog Debug2 ?p logger
let td3 logger = tlog Debug3 logger

let ti_timed ?(period = roll_period) ?p logger =
  let prev_msg_time = ref 0.0 in
  fun f ->
    let new_msg_time = Sys.time () in
    if (match p with Some p -> check_level logger p | None -> false)
      || new_msg_time -. !prev_msg_time > period
    then (prev_msg_time := new_msg_time; ti ?p logger f)
    else ifprintf fmt f

module type T = sig
  val logger: logger
end
