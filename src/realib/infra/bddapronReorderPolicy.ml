open BddapronUtil

(* -------------------------------------------------------------------------- *)

type cond =
  | CConj of cond * cond
  | CDisj of cond * cond
  | AtomV of (string -> bool)
  | AtomI of (int -> bool)
  | AtomN of (int -> bool)
  | AtomM of (float -> bool)
  | AtomNr of (int -> bool)
  | AtomTr of (int -> bool)
  | AtomDN of (int -> bool)
  | AtomDM of (float -> bool)
  | AtomDNr of (int -> bool)
  | AtomDTr of (int -> bool)
  | AtomBool of bool

let never = AtomBool false
let always = AtomBool true
let at_ith_iteration i = assert (i > 0); AtomI ((=) i)

let if_neg_or_zero i op = match i with
  | Some i when i <= 0 -> Some op
  | _ -> None

let if_pos i op = match i with
  | Some i when i > 0 -> Some (op i)
  | _ -> None

let acc_cond =
  let disj acc a = match acc with
    | AtomBool false -> a
    | c -> CDisj (c, a)
  in
  List.fold_left begin fun acc -> function
    | `Stop, Some a -> disj acc (AtomI a)
    | `DTr, Some a -> disj acc (AtomDTr a)
    | `DNr, Some a -> disj acc (AtomDNr a)
    | _, None -> acc
  end (AtomBool false)

let all_none = List.for_all (fun (_, x) -> x = None)

let thresholds_cond ?i ?dtr_max ?dnr_max default =
  let args = [
    `Stop, if_pos i (=);
    `Stop, if_neg_or_zero i ((=) 0);
    `DTr, if_pos dtr_max (<=);
    `DNr, if_pos dnr_max (<=);
  ] in
  if all_none args then default else Some (acc_cond args)

(* -------------------------------------------------------------------------- *)

type inst_infos = {
  v: string; i: int;
  n: int; dn: int option;
  m: float; dm: float option;
  nr: int; dnr: int option;
  tr: int; dtr: int option;
}

let make_ii ~n ?dn ~m ?dm ~tr ?dtr ~nr ?dnr ~v i =
  { v; i; n; dn; m; dm; nr; dnr; tr; dtr }

let eval_cond { v; i; n; dn; m; dm; nr; dnr; tr; dtr } cond =
  let opt_a a = function
    | None -> false
    | Some v -> a v
  in
  let rec eval = function
    | CConj (a, b) -> eval a && eval b
    | CDisj (a, b) -> eval a || eval b
    | AtomV a -> a v
    | AtomI a -> a i
    | AtomN a -> a n
    | AtomM a -> a m
    | AtomNr a -> a nr
    | AtomTr a -> a tr
    | AtomDN a -> opt_a a dn
    | AtomDM a -> opt_a a dm
    | AtomDNr a -> opt_a a dnr
    | AtomDTr a -> opt_a a dtr
    | AtomBool b -> b
  in
  eval cond

(* -------------------------------------------------------------------------- *)

type config =
    {
      stop_r: cond;
      stop_dr: cond;
      log: bool;
      logfile: string option;
    }

let mk_config
    ?(stop_r = at_ith_iteration 5)
    ?(stop_dr = never)
    ?(log = false)
    ?logfile () =
  { stop_r; stop_dr; log; logfile; }

(* -------------------------------------------------------------------------- *)

module Make (Logger: Log.T) = functor (E: BDDAPRON_ENV) -> struct
  open Logger
  open E
  open CuddUtil

  type t =
      {
        infos: (string, memory) Hashtbl.t;
        inst_acts: inst_act list;
        full_acts: full_act list;
        orig_r_status: bool;
        orig_dr_status: dynamic_reordering_status;
        logfile: out_channel option;
      }
  and memory =
      {
        mutable prev_n: int;
        mutable prev_m: float;
        mutable prev_nr: int;
        mutable prev_tr: int;
      }
  and inst_act = lp:Util.pu
      -> inst_infos
      -> unit
  and full_act = lp:Util.pu
      -> n:int -> dn:int
      -> m:float -> dm:float
      -> nr:int -> dnr:int
      -> tr:int -> dtr:int
      -> v:string -> int
      -> unit

  let get_common_infos () =
    let cudd = Bdd.Env.cuddman env in
    Cudd.Man.get_reordering_nb cudd,
    Cudd.Man.get_reordering_time cudd

  let register' get_infos { inst_acts; infos } ~lp ?(i = 0) ~v x =
    let n, m = get_infos x and nr, tr = get_common_infos () in
    Hashtbl.add infos v { prev_n = n; prev_m = m; prev_nr = nr; prev_tr = tr };
    let ii = make_ii ~n ?dn:None ~m ?dm:None ~nr ?dnr:None ~tr ?dtr:None ~v i in
    List.iter (fun c -> c ~lp ii) inst_acts

  let handle' get_infos ({ inst_acts; full_acts; infos } as rp) ~lp ~i ~v x =
    try
      let { prev_n; prev_m; prev_nr; prev_tr } as vinfo = Hashtbl.find infos v in
      let n, m = get_infos x and nr, tr = get_common_infos () in
      let dn = n - prev_n and dm = m -. prev_m
      and dnr = nr - prev_nr and dtr = tr - prev_tr in
      vinfo.prev_n <- n;
      vinfo.prev_m <- m;
      vinfo.prev_nr <- nr;
      vinfo.prev_tr <- tr;
      let ii = make_ii ~n ?dn:(Some dn) ~m ?dm:(Some dm)
        ~nr ?dnr:(Some dnr) ~tr ?dtr:(Some dtr)~v i in
      List.iter (fun c -> c ~lp ii) inst_acts;
      List.iter (fun c -> c ~lp ~n ~dn ~m ~dm ~nr ~dnr ~tr ~dtr ~v i) full_acts
    with Not_found ->
      register' get_infos rp ~lp ~i ~v x

  let bdd_infos x = Cudd.Bdd.size x, Cudd.Bdd.nbpaths x
  let handle_bdd rp = handle' bdd_infos rp

  let vdd_infos x = Cudd.Vdd.size x, Cudd.Vdd.nbpaths x
  let handle_vdd rp = handle' vdd_infos rp

  let log_rstatus () =
    if !Options.enable_reordering then
      if !Options.enable_dynamic_reordering then
        Log.d3 logger "@[Dynamic@ reordering@ status:@ %a@]"
          print_dynamic_reordering_status (dynamic_reordering_status env)
      else
        Log.d3 logger "@[Dynamic@ reordering@ disabled.@]"
    else
      Log.d3 logger "@[Reordering@ disabled.@]"

  let maybe_log ~log = match log with
    | true ->
        [ fun ~lp ({ v; i; n; dn; m; dm } as _ii) ->
          let pp_dn = Util.pp1' " (@[@<1>δ@]n = %+6i)" dn
          and pp_dm = Util.pp1' " (@[@<1>δ@]m = %+6.0f)" dm in
          Log.i logger "%t%u: @<1>%1s: @[@[n = %6i%t@],@ @[m = %6.0f%t@]@]\
                       " lp i v n pp_dn m pp_dm;
        ]
    | false -> []

  let maybe_log_in_file ~logfile = match logfile with
    | Some oc ->
        let open Printf in
        fprintf oc "%-12s %12s %12s %12s" "\"var\"" "\"i\"" "\"n\"" "\"m\"";
        fprintf oc " %12s %12s\n%!" "\"n_r\"" "\"t_r\"";
        [ fun ~lp ({ v; i; n; m; nr; tr } as _ii) ->
          fprintf oc "%-12s %12i %12i %12.0f %12i %12i\n%!" v i n m nr tr
        ]
    | None -> []

  let maybe_stop_dr = function
    | AtomBool false -> []
    | cond ->
        [ fun ~lp ({ i } as ii) ->
          if (!Options.enable_reordering && !Options.enable_dynamic_reordering &&
                 eval_cond ii cond) then begin
            Log.i logger "%t%u: @[Suspending@ dynamic@ variable@ reorderings@]\
                           " lp i;
            stop_dynamic_reordering env;
          end
        ]

  let maybe_stop_r = function
    | AtomBool false -> []
    | cond ->
        [ fun ~lp ({ i } as ii) ->
          if (!Options.enable_reordering && eval_cond ii cond) then begin
            Log.i logger "%t%u: @[Disabling@ all@ variable@ reorderings@]" lp i;
            disable_reordering env;
          end
        ]

  let maybe_reorder : full_act list =
    [
      fun ~lp ~n ~dn ~m ~dm ~nr ~dnr ~tr ~dtr ~v _i ->
        if dm > 0. then reorder env
    ]

  let make { stop_r; stop_dr; log; logfile; } =
    let logfile = match logfile with
      | Some f ->
          Log.i logger "@[Logging@ BDD@ sizes@ in@ `%s'@]" f;
          Some (open_out f)
      | None -> None
    in
    log_rstatus ();
    {
      infos = Hashtbl.create 2;
      inst_acts =
        maybe_stop_dr stop_dr
      @ maybe_stop_r stop_r
      @ maybe_log_in_file ~logfile
      @ maybe_log ~log;
      full_acts =
        maybe_reorder;
      orig_r_status = !Options.enable_reordering;
      orig_dr_status = dynamic_reordering_status env;
      logfile;
    }

  let dispose { orig_r_status; orig_dr_status; logfile } =
    log_rstatus ();
    Options.enable_reordering := orig_r_status;
    dynamic_reordering_restore env orig_dr_status;
    if !Options.enable_reordering && !Options.enable_dynamic_reordering then
      Log.d logger "@[Restoring@ initial@ dynamic@ variable@ reorderings@ \
                      policy@]";
    log_rstatus ();
    match logfile with None -> () | Some oc -> close_out oc

end

(* -------------------------------------------------------------------------- *)
