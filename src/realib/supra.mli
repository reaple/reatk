(* see implementation file for documentation *)
(** *)

(* -------------------------------------------------------------------------- *)
(** {2 Data-flow to/from Control-flow translation} *)

val df2cf: string -> Env.t -> Program.dfprog_t -> Env.t * Program.cfprog_t
val cf2df: Env.t -> Program.cfprog_t -> Program.dfprog_t

(* -------------------------------------------------------------------------- *)
(** {2 Verification} *)

type verif_spec = VerifEngine.spec

val parse_verif: Env.t -> string -> verif_spec

type verif_res = Analysis.result

val verif: verif_spec -> Env.t -> Program.cfprog_t -> verif_res

(* -------------------------------------------------------------------------- *)
(** {2 Synthesis} *)

type synth_spec = SynthEngine.spec

val parse_synth: Env.t -> string -> synth_spec

type synth_inp = CtrlSpec.t * Env.boolexpr_t * Program.cfprog_t
type synth_res = CtrlSpec.t * Synthesis.controller * Program.cfprog_t

exception SynthesisFailure of CtrlSpec.t * Env.t * Program.cfprog_t

val synth: synth_spec -> Env.t -> synth_inp -> synth_res

type coreach_res = CtrlSpec.t * Env.boolexpr_t * Program.cfprog_t
val coreach: synth_spec -> Env.t -> synth_inp -> coreach_res

(* -------------------------------------------------------------------------- *)
(** {2 Optimization} *)

type optim_spec = OptimEngine.spec

val parse_optim
  : ?input_expr: (Env.t -> string -> Env.expr_t)
  -> Env.t -> CtrlSpec.t -> string -> optim_spec

type optim_res = CtrlSpec.t * Synthesis.controller * Program.cfprog_t

val optim: optim_spec -> Env.t -> synth_res -> optim_res

(* -------------------------------------------------------------------------- *)
(** {2 Triangulation} *)

type triang_res = Env.equs_t list * Env.boolexpr_t

val triang
  : ?allow_careset_updates: bool
  -> ?enable_reorderings: bool
  -> ?enable_dynamic_reorderings: bool
  -> CtrlSpec.t -> Env.t -> Synthesis.controller -> triang_res

(* -------------------------------------------------------------------------- *)
(** {2 Merging program and controller} *)

type merge_res = Env.t * CtrlSpec.t * Program.dfprog_t

val merge
  : ?allow_careset_updates: bool
  -> ?enable_reorderings: bool
  -> ?enable_dynamic_reorderings: bool
  -> ?discard_controllables: bool
  -> CtrlSpec.t -> Program.dfprog_t -> Env.t -> Env.equs_t list -> merge_res

(* -------------------------------------------------------------------------- *)
(** {2 Printing & Logging} *)

val log_env_info: ?cs:CtrlSpec.t -> Env.t -> unit
val log_dfprog_info: Env.t -> Program.dfprog_t -> unit
val log_cfprog_info: Env.t -> Program.cfprog_t -> unit
val output_dot_file: dotfile:string -> arcs:bool -> Env.t -> Program.cfprog_t ->
  unit

(* -------------------------------------------------------------------------- *)
