(******************************************************************************)
(* arc *)
(* data associated to an arc of a control flow graph*)
(* author: Peter Schrammel *)
(* version: 0.9.0 *)
(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)
(******************************************************************************)

(* let logger = Log.mk "Arc" *)

(******************************************************************************)
(** {2 Types} *)
(******************************************************************************)

type t =
    |Normal of Env.boolexpr_t * Env.equs_t
    |Loop of Env.boolexpr_t * Env.equs_t
    |Accel of Env.boolexpr_t * Env.equs_t
    |BoolAccel of Env.boolexpr_t * Env.equs_t
    |BoolNaccAccel of Env.boolexpr_t * Env.equs_t
    |Bool of Env.boolexpr_t * Env.equs_t
    |BoolNacc of Env.boolexpr_t * Env.equs_t
    |Nonacc of Env.boolexpr_t * Env.equs_t
    |Id
    |Flow of Env.boolexpr_t * Env.equs_t
    |Apron of Env.boolexpr_t * (ApronUtil.equs_t * Env.equs_t)

(******************************************************************************)
(** {2 Printing} *)
(******************************************************************************)

let print_type env fmt arc =
  let mode =
    match arc with
      |Normal(_,_) -> ""
      |Loop(_,_) -> "[Loop]"
      |Accel(_,_) -> "[Accel]"
      |BoolAccel(_,_) -> "[BoolAccel]"
      |BoolNaccAccel(_,_) -> "[BoolNaccAccel]"
      |Bool(_,_) -> "[Bool]"
      |BoolNacc(_,_) -> "[BoolNacc]"
      |Nonacc(_,_) -> "[Nonacc]"
      |Id -> "[Id]"
      |Flow(_,_) -> "[Flow]"
      |Apron(_,_) -> "[DiscFlow]"
 in
 Format.pp_print_string fmt mode

let print ?(dot=false) { Env.env = e; Env.cond = c } fmt arc =
  let strfmt = Format.str_formatter in
  let strcontent () = let r = Format.flush_str_formatter () in
                      if dot then Format.pp_set_margin strfmt 80;
                      r
  in
  if dot then
    Format.pp_set_margin strfmt 80;
  match arc with
    |Normal(a,f)|Loop(a,f)|Accel(a,f)|BoolAccel(a,f)
    |BoolNaccAccel(a,f)|Bool(a,f)|BoolNacc(a,f)|Nonacc(a,f) ->
      if not(Bddapron.Expr0.Bool.is_true e c a) then
      begin
	Bddapron.Expr0.Bool.print e c strfmt a;
        if dot
        then Format.fprintf strfmt " -->@."
        else let str = strcontent () in
             Format.pp_print_string fmt ((Util.string_compact str)^" --> ")
      end;
      List.iter
	(fun equ ->
	  let (v,expr) = equ in
	  Env.Symb.print strfmt v;
	  Format.pp_print_string strfmt "'=";
	  Bddapron.Expr0.print e c strfmt expr;
	  Format.pp_print_string strfmt "; ";
          if dot then Format.pp_print_break strfmt 0 0)
        (BddapronUtil.simplify_equs e c a f);
      let str = strcontent () in
      if dot
      then Format.pp_print_string fmt (Util.string_escape str)
      else Format.pp_print_string fmt (Util.string_compact str)
    |Id -> ()
    |Apron(a,(fn,fb)) ->
      if not(Bddapron.Expr0.Bool.is_true e c a) then
      begin
	Bddapron.Expr0.Bool.print e c strfmt a;
        if dot
        then Format.fprintf strfmt " -->@."
        else let str = strcontent () in
             Format.pp_print_string fmt ((Util.string_compact str)^" --> ")
      end;
      Array.iter
	(fun equ ->
	  let (v,expr) = equ in
	  Apron.Var.print strfmt v;
	  Format.pp_print_string strfmt "'=";
	  Apron.Linexpr1.print strfmt expr;
	  Format.pp_print_string strfmt "; ";
          if dot then Format.pp_print_cut strfmt ())
        fn;
      List.iter
	(fun equ ->
	  let (v,expr) = equ in
	  Env.Symb.print strfmt v;
	  Format.pp_print_string strfmt "'=";
	  Bddapron.Expr0.print e c strfmt expr;
	  Format.pp_print_string strfmt "; ";
          if dot then Format.pp_print_cut strfmt ())
        fb;
      let str = strcontent () in
      Format.pp_print_string fmt (Util.string_compact str)
    |Flow(a,f) ->
      if not(Bddapron.Expr0.Bool.is_true e c a) then
      begin
	Bddapron.Expr0.Bool.print e c strfmt a;
        let str = strcontent () in
        Format.pp_print_string fmt ((Util.string_compact str)^" -->\\n")
      end;
      let strfmt = Format.str_formatter in
      List.iter
	(fun equ ->
	  let (v,expr) = equ in
	  Format.pp_print_string strfmt ".";
	  Env.Symb.print strfmt v;
	  Format.pp_print_string strfmt "=";
	  Bddapron.Expr0.print e c strfmt expr;
	  Format.pp_print_string strfmt "; ")
      (BddapronUtil.simplify_equs e c a f);
      let str = strcontent () in
      Format.pp_print_string fmt (Util.string_compact str)

(******************************************************************************)
(** {2 Operations} *)
(******************************************************************************)

let get_ass_equs env arc =
  match arc with
    |Normal(a,f)|Loop(a,f)|Accel(a,f)|BoolAccel(a,f)
      |BoolNaccAccel(a,f)
      |Bool(a,f)|BoolNacc(a,f)|Nonacc(a,f) -> (a,f)|Flow(a,f) -> (a,f)
    |Id -> (Bddapron.Expr0.Bool.dtrue env.Env.env env.Env.cond,
           BddapronUtil.get_id_equs_for env.Env.env env.Env.cond env.Env.s_vars)
    |Apron(a,(fn,fb)) -> (a,fb)

let simplify env ?enums phi arc =
  let simplify_equs = BddapronUtil.simplify_equs env.Env.env env.Env.cond ?enums phi in
  match arc with
    |Normal(a,f) -> Normal (a, simplify_equs f)
    |Loop (a,f) -> Loop (a, simplify_equs f)
    |Accel(a,f) -> Accel (a, simplify_equs f)
    |BoolAccel(a,f) -> BoolAccel (a, simplify_equs f)
    |BoolNaccAccel(a,f) -> BoolNaccAccel (a, simplify_equs f)
    |Bool(a,f) -> Bool (a, simplify_equs f)
    |BoolNacc(a,f) -> BoolNacc (a, simplify_equs f)
    |Nonacc(a,f) -> Nonacc (a, simplify_equs f)
    |Flow(a,f) -> Flow (a, simplify_equs f)
    |Id -> arc
    |Apron (a, (fn, fb)) -> Apron(a, (fn, simplify_equs fb))

let refine_ass env arc phi =
  match arc with
    |Normal(a,f) -> Normal (Cudd.Bdd.dand a phi,f)
    |Loop (a,f) -> Loop (Cudd.Bdd.dand a phi,f)
    |Accel(a,f) -> Accel (Cudd.Bdd.dand a phi,f)
    |BoolAccel(a,f) -> BoolAccel (Cudd.Bdd.dand a phi,f)
    |BoolNaccAccel(a,f) -> BoolNaccAccel (Cudd.Bdd.dand a phi,f)
    |Bool(a,f) -> Bool (Cudd.Bdd.dand a phi,f)
    |BoolNacc(a,f) -> BoolNacc (Cudd.Bdd.dand a phi,f)
    |Nonacc(a,f) -> Nonacc (Cudd.Bdd.dand a phi,f)
    |Flow(a,f) -> Flow (Cudd.Bdd.dand a phi,f)
    |Id -> arc
    |Apron(a,(fn,fb)) -> Apron (Cudd.Bdd.dand a phi,(fn,fb))

let is_id env arc sinv =
  match arc with
    |Normal(a,f)
    |Loop (a,f)
    |Accel(a,f)
    |BoolAccel(a,f)
    |BoolNaccAccel(a,f)
    |Bool(a,f)
    |BoolNacc(a,f)
    |Nonacc(a,f)
    |Flow(a,f) -> BddapronAnalysis.is_id_equs2
      (BddapronUtil.get_primed_var env.Env.env)
      (BddapronUtil.get_unprimed_var env.Env.env)
      env.Env.env env.Env.cond a f sinv
    |Id -> true
    |Apron(a,(fn,fb)) ->
       BddapronAnalysis.is_id_bequs2
      (BddapronUtil.get_primed_var env.Env.env)
      (BddapronUtil.get_unprimed_var env.Env.env)
      env.Env.env env.Env.cond a fb sinv (* TODO apron-equs id check *)

let replace_ass_equs arc (a,f) =
  match arc with
    |Normal(_,_) -> Normal (a,f)
    |Loop (_,_) -> Loop (a,f)
    |Accel(_,_) -> Accel (a,f)
    |BoolAccel(_,_) -> BoolAccel (a,f)
    |BoolNaccAccel(_,_) -> BoolNaccAccel (a,f)
    |Bool(_,_) -> Bool (a,f)
    |BoolNacc(_,_) -> BoolNacc (a,f)
    |Nonacc(_,_) -> Nonacc (a,f)
    |Flow(_,_) -> Flow (a,f)
    |Id -> arc
    |Apron(_,(fn,_)) -> Apron (a,(fn,f))

let apply_permutations perm =
  let pb = BddapronUtil.permute_boolexpr perm
  and pequs = BddapronUtil.permute_equs perm in
  function
    | Normal (a,f) -> Normal (pb a, pequs f)
    | Loop (a,f) -> Loop (pb a, pequs f)
    | Accel (a,f) -> Accel (pb a, pequs f)
    | BoolAccel (a,f) -> BoolAccel (pb a, pequs f)
    | BoolNaccAccel (a,f) -> BoolNaccAccel (pb a, pequs f)
    | Bool (a,f) -> Bool (pb a, pequs f)
    | BoolNacc (a,f) -> BoolNacc (pb a, pequs f)
    | Nonacc (a,f) -> Nonacc (pb a, pequs f)
    | Flow (a,f) -> Flow (pb a, pequs f)
    | Id -> Id
    | Apron (a,(fn,fb)) -> Apron (pb a,(fn,pequs fb))
