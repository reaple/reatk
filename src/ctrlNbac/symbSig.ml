
(** {2 Symbol representation} *)
module type T = sig

  (* ------------------------------------------------------------------------ *)

  type symb
  type t = symb

  exception Invalid_symbol of t

  val of_string: string -> t
  val to_string: t -> string
  val dummy: t
  val reset: unit -> unit

  (* ------------------------------------------------------------------------ *)
  (** {3 Utilities} *)

  val capitalize: t -> t
  val exists: string -> bool
  val fresh: ?cnt_ref:int ref -> string -> t
  val fresh': ?cnt_ref:int ref -> string -> t

  (* ------------------------------------------------------------------------ *)
  (** {3 Pretty-printing} *)

  val output: out_channel -> t -> unit
  val print: Format.formatter -> t -> unit

  (* ------------------------------------------------------------------------ *)

  module M: Structs.S with type t = symb
  include Structs.T with type t := t

end
