(* -------------------------------------------------------------------------- *)

open IO
open Format
open CtrlNbac.IO

let logger = Log.mk "Main"

(* -------------------------------------------------------------------------- *)

let abort ?filename n msgs =
  report_msgs ?filename err_formatter msgs;
  Log.e logger "@[Aborting@ due@ to@ errors@ in@ %(%)@]" n;
  exit 1

let error ?cont msg =
  Log.e logger msg;
  match cont with None -> exit 1 | Some c -> c ()

(* -------------------------------------------------------------------------- *)

let sighandle env = fun _ ->
  print_string "\027[1m";                        (* XXX: Unix-specific stuff. *)
  flush stdout;
  Realib.CuddUtil.print_stats env.Realib.Env.env;
  print_string "\027[0m";
  flush stdout

let env_break env a f = catch_break ~handler:(sighandle env) (f env) a

(* -------------------------------------------------------------------------- *)

let check_n_cont
    ~(out_descr: descr)
    ~(check: 'm -> 'b option * _)
    ~(print: formatter -> 'm -> unit)
    =
  fun cont input ->
    Log.i logger "@[Checking@ %(%)…@]" out_descr;
    match check input with
      | Some res, msgs ->
          cont res;
          report_msgs err_formatter msgs
      | None, errs ->
          print err_formatter input;
          abort out_descr errs

let pass
    ~(out_descr: descr)
    ~(translate: 'i -> 'm)
    ~(check: 'm -> 'b option * _)
    ~(print: formatter -> 'm -> unit)
    ?(discard=false)
    =
  fun cont input ->
    if discard then
      Log.i logger "@[Discarding@ %(%).@]" out_descr
    else begin
      Log.i logger "@[Extracting@ %(%)…@]" out_descr;
      let m = translate input in
      check_n_cont ~out_descr ~check ~print cont m
    end

(** Pass with optional checking of the result. *)
let pass'
    ~(out_descr: descr)
    ~(translate: 'i -> 'm)
    ~(check: 'm -> 'm option * _)
    ~(print: formatter -> 'm -> unit)
    ?(check_enabled=true)
    ?(discard=false)
    =
  fun cont input ->
    if discard then
      Log.i logger "@[Discarding@ %(%).@]" out_descr
    else begin
      Log.i logger "@[Extracting@ %(%)…@]" out_descr;
      let m = translate input in
      if check_enabled
      then check_n_cont ~out_descr ~check ~print cont m
      else cont m
    end

(* -------------------------------------------------------------------------- *)

let parse_input ?filename p =
  parse_input ~abort:(fun ?filename n ->
    abort ?filename
      (PPrt.ufmt_from_breakable_string (string_of_format n)))
    ?filename p

(* -------------------------------------------------------------------------- *)

(* XXX: actually locals shouldn't map to realib exprs as otherwise we would need
   to propagate permutations; we need to keep the relevant parts of the
   Controllable-Nbac AST and rebuild… *)
let input_expr
    ?dynamic_variable_reordering
    (exp_checker: _ CtrlNbac.AST.exp_checker)
    locals
    =
  fun env ?flag ?typ str ->
    try
      let rs = Realib.(match dynamic_variable_reordering with
        | None -> CuddUtil.dynamic_reordering_suspend1 env.Env.env
        | Some d -> CuddUtil.dynamic_reordering_select1 env.Env.env d)
      in
      CtrlNbac.Parser.parse_expr_string exp_checker ?flag ?typ str |> fst |>
          rs (Cn2rl.translate_exp env locals)
    with
      | CtrlNbac.Parser.Error (descr, msgs) ->
          abort (PPrt.ufmt_from_breakable_string descr) msgs

(* -------------------------------------------------------------------------- *)

type input_options =
    {
      env_booking_factor: int;
      env_cond_factor: int;
      env_max_conds: int option;
      enable_primed_state_vars: bool;
      enable_primed_input_vars: bool;
      dynamic_variable_reordering: Realib.CuddUtil.reordering_algorithm option;
    }

let mk_input
    ~(in_descr: descr)
    ~(parse: ?filename:string -> unit -> 'a * 'b * 'c)
    ~(expchk)
    ~(build:
        ?max_cudd_mem: int64
      -> ?env_booking_factor: int
      -> ?env_cond_factor: int
      -> ?env_max_conds: int
      -> ?enable_primed_state_vars: bool
      -> ?enable_primed_input_vars: bool
      -> ?dynamic_variable_reordering:Realib.CuddUtil.reordering_algorithm
      -> 'b -> 'm)
    =
  fun ?filename { env_booking_factor; env_cond_factor; env_max_conds;
                enable_primed_state_vars; enable_primed_input_vars;
                dynamic_variable_reordering; } ->
    let b = build ?max_cudd_mem:None
      ~env_booking_factor ~env_cond_factor ?env_max_conds
      ~enable_primed_state_vars ~enable_primed_input_vars
      ?dynamic_variable_reordering in
    let _name, model = parse_input ?filename parse in
    match Log.roll b model with
      | `TOk ((_, _, _, locals) as res)
        -> res, input_expr ?dynamic_variable_reordering (expchk model) locals
      | `TKo err -> abort ?filename in_descr err

let discard_ast ?env_booking_factor ?env_cond_factor ?env_max_conds
    ?enable_primed_state_vars ?enable_primed_input_vars x = `TOk x

(* -------------------------------------------------------------------------- *)

let mk_output
    ~(ext: string)
    ~(out_descr: descr)
    ~(translate: 'i -> 'm)
    ~(check: 'm -> 'b option * _)
    ~(print: (?print_header:(formatter -> string -> string -> unit)
              -> formatter -> 'b -> unit))
    ?backup
    ?toolname
    ?check_enabled
    ?discard
    mk_fmt
    =
  pass' ~out_descr ~translate ~check ~print ?check_enabled ?discard
    (mk_output ~ext ~out_descr ~print ?backup ?toolname
       ~version:Version.str mk_fmt)

(* --- *)

let output_ctrlr env cs =
  mk_output
    ~ext:"ctrlr"
    ~out_descr:"generated@ controller"
    ~translate:(Rl2cn.extract_ctrlr env cs)
    ~check:(CtrlNbac.AST.check_pred :> _ -> 'f CtrlNbac.AST.pred option * _)
    ~print:CtrlNbac.AST.print_pred

let output_ctrlf env cs =
  mk_output
    ~ext:"ctrlf"
    ~out_descr:"triangularized@ controller"
    ~translate:(Rl2cn.extract_ctrlf env cs)
    ~check:(CtrlNbac.AST.check_func :> _ -> 'f CtrlNbac.AST.func option * _)
    ~print:CtrlNbac.AST.print_func

let output_ctrld env cs =
  mk_output
    ~ext:"ctrld"
    ~out_descr:"generated@ controlled@ program"
    ~translate:(Rl2cn.extract_ctrln env cs)
    ~check:(CtrlNbac.AST.check_node :> _ -> 'f CtrlNbac.AST.node option * _)
    ~print:CtrlNbac.AST.print_node

let output_ctrls () =
  mk_output
    ~ext:"ctrls"
    ~out_descr:"split@ triangularized@ controller"
    ~translate:(fun f -> f)
    ~check:(CtrlNbac.AST.check_func :> _ -> 'f CtrlNbac.AST.func option * _)
    ~print:CtrlNbac.AST.print_func

(* -------------------------------------------------------------------------- *)

let input_ctrln ?static_variable_ordering ?strong_variable_affinities =
  mk_input
    ~in_descr:"parsed@ node"
    ~parse:CtrlNbac.Parser.parse_node
    ~expchk:CtrlNbac.AST.exp_checker_from_node
    ~build:(Cn2rl.translate_node ?static_variable_ordering
              ?strong_variable_affinities)

let input_ctrlg ?static_variable_ordering ?strong_variable_affinities =
  mk_input
    ~in_descr:"parsed@ CFG"
    ~parse:CtrlNbac.Parser.parse_cfg
    ~expchk:CtrlNbac.AST.exp_checker_from_cfg
    ~build:(Cn2rl.translate_cfg ?static_variable_ordering
              ?strong_variable_affinities ?cdfequs_only:None)

let input_ctrlr ?env =
  mk_input
    ~in_descr:"parsed@ predicate"
    ~parse:CtrlNbac.Parser.parse_pred
    ~expchk:CtrlNbac.AST.exp_checker_from_pred
    ~build:(Cn2rl.translate_pred ?env)

let input_ctrlf ?env =
  mk_input
    ~in_descr:"parsed@ function"
    ~parse:CtrlNbac.Parser.parse_func
    ~expchk:CtrlNbac.AST.exp_checker_from_func
    ~build:(Cn2rl.translate_func ?env)

(* -------------------------------------------------------------------------- *)

let reset = CtrlNbac.Symb.reset
