(* See implementation file for documentation *)
(**)

open Lexing

(* -------------------------------------------------------------------------- *)

type msg =
  [
  | Loc.t AST.loc_msg
  | ParserUtils.syntax_msg
  | Lexer.msg
  ]

exception Error of string * msg list

type 'a file_parsing_func =
    ?filename: string -> unit -> string option * 'a * msg list
type 'a string_parsing_func =
    string -> 'a * msg list

val parse_node: Loc.t AST.checked_node file_parsing_func
val parse_cfg: Loc.t AST.checked_cfg file_parsing_func
val parse_func: Loc.t AST.checked_func file_parsing_func
val parse_pred: Loc.t AST.checked_pred file_parsing_func

val parse_node_string: Loc.t AST.checked_node string_parsing_func
val parse_cfg_string: Loc.t AST.checked_cfg string_parsing_func
val parse_func_string: Loc.t AST.checked_func string_parsing_func
val parse_pred_string: Loc.t AST.checked_pred string_parsing_func

val parse_expr_string: Loc.t AST.exp_checker
  -> ?flag: Loc.t -> ?typ: AST.typ -> Loc.t AST.exp string_parsing_func

(* -------------------------------------------------------------------------- *)

module Unsafe: sig
  val parse_node: Loc.t AST.node file_parsing_func
  val parse_cfg: Loc.t AST.cfg file_parsing_func
  val parse_func: Loc.t AST.func file_parsing_func
  val parse_pred: Loc.t AST.pred file_parsing_func

  val parse_node_string: Loc.t AST.node string_parsing_func
  val parse_cfg_string: Loc.t AST.cfg string_parsing_func
  val parse_func_string: Loc.t AST.func string_parsing_func
  val parse_pred_string: Loc.t AST.pred string_parsing_func

  val parse_expr_string: Loc.t AST.exp string_parsing_func
end

(* -------------------------------------------------------------------------- *)

module Reporting : sig

  type pp_located = ?filename:string -> ?loc:Loc.t -> (Format.formatter -> unit) ->
      Format.formatter -> unit

  val error: pp_located
  val warning: pp_located
  val info: pp_located
  val report_msg: ?filename:string -> Format.formatter -> [< msg ] -> unit

end

(* -------------------------------------------------------------------------- *)
