(** Translation of Controllable-nbac models into
    {{:../realib/index.html}[Realib]} internal representations.

    @author Nicolas Berthier *)

open Realib
open CtrlNbac.AST
open Program
module Symb = CtrlNbac.Symb
module Expr0 = Bddapron.Expr0
module E = Bddapron.Env

let level = Log.Debug3
let logger = Log.mk ~level "Cn2rl"

type feature =
    [
    | `EnumDefault of symb
    | `BintDefault of symb
    | `Verb of string
    ]
exception Unsupported of feature

(* -------------------------------------------------------------------------- *)

let translate_symb s = Symb.to_string s |> Env.Symb.mk

let translate_label s = label_symb s |> translate_symb

let translate_typdef' = function
  | EnumDef ll ->
      `Benum (Bdd.Labels.of_list Env.Symb.compare
                (List.rev_map translate_label (List.rev_map fst ll)))
let translate_typdef (td, _) = translate_typdef' td

let translate_typ = function
  | `Bool -> `Bool
  | `Int -> `Int
  | `Real -> `Real
  | `Bint (s, w) -> `Bint (s, w)
  | `Enum tn -> `Benum (translate_symb tn)

(* --- *)

type 'f local_defs = 'f CtrlNbac.AST.exp CtrlNbac.AST.SMap.t
let local_defs ld = List.fold_left (fun m (v, e) -> SMap.add v e m) SMap.empty ld

exception IllTypedExpression of string

(* Repository of translated locals *)
type locals_repo = (symb, Realib.Env.expr_t) Hashtbl.t

let illtyped ?flag e =
  Log.e logger "@[Ill-typed@ expression:@ @[%a@]@\n\
                  Most@ probable@ cause@ is@ a@ bug@ in@ the@ type@ checking@ \
                  code@ of@ ctrlNbac.@ Please@ report@ this@ to@ m@nberth.space\
                @]" print_exp e;
  IllTypedExpression (Format.asprintf "%a" print_exp e)

(* Translation of general expressions *)
let translate_exp
    (type flg)
    ?(illtypedexn: ?flag:flg -> flg exp -> exn = illtyped)
    ?(locals_repo: locals_repo = Hashtbl.create 17)
    { Env.env = e; Env.cond = c }
    (local_defs: flg local_defs)
    : flg exp -> Env.expr_t
  =

  let exception LocalIllTyped of flg option * flg exp in
  let illtyped ?flag e = raise (LocalIllTyped (flag, e)) in

  let ff = Expr0.Bool.dfalse e c and tt = Expr0.Bool.dtrue e c in
  let excl l =
    let rec excl (none, one, more) = function
      | [] -> Expr0.Bool.dor e c none one
      | a::tl ->
          let none = Expr0.Bool.dand e c none (Expr0.Bool.dnot e c a)
          and one = Expr0.Bool.ite e c a (Expr0.Bool.nor e c one more) one
          and more = Expr0.Bool.dor e c more (Expr0.Bool.dand e c one a) in
          excl (none, one, more) tl
    in excl (tt, ff, ff) l
  in

  let apron_cmp a b =
    let diff = Expr0.Apron.sub e c a b in function
      | `Eq -> Expr0.Apron.eq e c diff
      | `Ne -> Expr0.Bool.dnot e c (Expr0.Apron.eq e c diff)
      | `Gt -> Expr0.Apron.sup e c diff
      | `Ge -> Expr0.Apron.supeq e c diff
      | `Lt -> Expr0.Apron.sup e c (Expr0.Apron.negate e c diff)
      | `Le -> Expr0.Apron.supeq e c (Expr0.Apron.negate e c diff)
  and apron_op op : Env.numexpr_t -> Env.numexpr_t list -> Env.numexpr_t =
    let op = match op with
      | `Sum -> Expr0.Apron.add
      | `Sub -> Expr0.Apron.sub
      | `Mul -> Expr0.Apron.mul
      | `Div -> Expr0.Apron.div in
    List.fold_left (op e c ~typ:Apron.Texpr1.Real ~round:Apron.Texpr1.Rnd)
  and bint_cmp a b = function
    | `Eq -> Expr0.Bint.eq e c a b
    | `Ne -> Expr0.Bool.dnot e c (Expr0.Bint.eq e c a b)
    | `Gt -> Expr0.Bint.sup e c a b
    | `Ge -> Expr0.Bint.supeq e c a b
    | `Lt -> Expr0.Bint.sup e c b a
    | `Le -> Expr0.Bint.supeq e c b a
  and bint_op op =
    let op = match op with
      | `Sum -> Expr0.Bint.add
      | `Sub -> Expr0.Bint.sub
      | `Mul -> Expr0.Bint.mul
      | `Div -> raise (Unsupported (`Verb "`div' of bounded integers.")) in
    List.fold_left (op e c)
  and bint_lop = function
    | `LAnd -> Expr0.Bint.land' e c
    | `LOr -> Expr0.Bint.lor' e c
    | `LXor -> Expr0.Bint.lxor' e c
  and bint_sop o n u = match o with
    | `Lsl -> Expr0.Bint.shift_left_logical e c n u
    | `Lsr -> Expr0.Bint.shift_right_logical e c n u
    | `Asl -> Expr0.Bint.shift_left e c n u
    | `Asr -> Expr0.Bint.shift_right e c n u
  and bint_sop' o n u = assert (u >= 0); match o with
    | `Lsl | `Asl -> Expr0.Bint.shift_left' e c n u
    | `Lsr -> Expr0.Bint.shift_right_logical' e c n u
    | `Asr -> Expr0.Bint.shift_right' e c n u
  and trbnop a b l : _ -> Env.boolexpr_t = function
    | `Conj -> List.fold_left (Expr0.Bool.dand e c) a (b :: l)
    | `Disj -> List.fold_left (Expr0.Bool.dor e c) a (b :: l)
    | `Excl -> excl (a :: b :: l)
  and mem_op eq a l : Env.boolexpr_t =
    List.fold_left
      (fun b l -> Expr0.Bool.dor e c (eq e c a l) b) ff l
  in

  let tbin = mem_op Expr0.Bool.eq
  and tein = mem_op Expr0.Benum.eq
  and tbiin = mem_op Expr0.Bint.eq
  in

  let rec tb ?flag : 'f bexp -> Env.boolexpr_t = function
    | `Ref v -> Expr0.Bool.of_expr (tv v)
    | `Bool b -> Expr0.Bool.of_bool e c b
    | `Buop (`Neg,a) -> Expr0.Bool.dnot e c (tb a)
    | `Bbop (`Imp,a,b) -> Expr0.Bool.dor e c (Expr0.Bool.dnot e c (tb a)) (tb b)
    | `Bnop (op,a,b,l) -> trbnop (tb a) (tb b) (List.map tb l) op
    | `Bcmp (`Eq,a,b) -> Expr0.Bool.eq e c (tb a) (tb b)
    | `Bcmp (`Ne,a,b) -> tb (`Buop (`Neg, `Bcmp (`Eq, a, b)))
    | `Ecmp (`Eq,a,b) -> Expr0.Benum.eq e c (te a) (te b)
    | `Ecmp (`Ne,a,b) -> tb (`Buop (`Neg, `Ecmp (`Eq, a, b)))
    | `Ncmp (op,a,b) as e -> (try apron_cmp (tn a) (tn b) op with
        | Failure _ | Exit -> try bint_cmp (tnb a) (tnb b) op with
            | Exit -> illtyped ?flag (`Bexp e))
    | `Pcmp _ | `Pin _ as e -> illtyped ?flag (`Bexp e)
    | `Bin (e,l,l') -> tbin (tb e) (List.map tb (l::l'))
    | `Ein (e,l,l') -> tein (te e) (List.map te (l::l'))
    | `BIin (e,l,l') -> tbiin (tnb e) (List.map tnb (l::l'))
    | `Ite (b,x,y) -> Expr0.Bool.ite e c (tb b) (tb x) (tb y)
    | #flag as f -> apply' tb f
  and te ?flag : 'f eexp -> 's Expr0.Benum.t = function
    | `Ref v -> Expr0.Benum.of_expr (tv v)
    | `Enum l -> Bdd.Enum.of_label e (translate_label l)
    | `Ite (b,x,y) -> Expr0.Benum.ite e c (tb b) (te x) (te y)
    | #flag as f -> apply' te f
  and tn ?flag : 'f nexp -> Env.numexpr_t = function
    | `Ref v -> Expr0.Apron.of_expr (tv v)
    | `Int i -> Expr0.Apron.cst e c (Apron.Coeff.s_of_int i)
    | `Real r -> Expr0.Apron.cst e c (Apron.Coeff.s_of_float r)
    | `Mpq q -> Expr0.Apron.cst e c (Apron.Coeff.s_of_mpqf q)
    | `Ncst _ | `Luop _ | `Lbop _ | `Lsop _ -> raise Exit (* Retry with tnb *)
    | `Nuop (`Opp,a) -> Expr0.Apron.negate e c (tn a)
    | `Nnop (op,a,b,l) -> apron_op op (tn a) (List.map tn (b::l))
    | `Ite (b,x,y) -> Expr0.Apron.ite e c (tb b) (tn x) (tn y)
    | #flag as f -> apply' tn f
  and tnb ?flag : 'f nexp -> 'a Expr0.Bint.t = function
    | `Ref v -> Expr0.Bint.of_expr (tv v)
    |(`Int _ | `Real _ | `Mpq _) -> raise Exit
    | `Ncst (s,w,a) -> (match deflag a with
        | `Int i -> Expr0.Bint.of_int e c (`Bint (s, w)) i
        | a -> Expr0.Bint.cast e c (`Bint (s, w)) (tnb a))
    | `Nuop (`Opp,a) -> Expr0.Bint.neg e c (tnb a)
    | `Nnop (op,a,b,l) -> bint_op op (tnb a) (List.map tnb (b::l))
    | `Luop (`LNot,a) -> Expr0.Bint.lnot e c (tnb a)
    | `Lbop (op,a,b) -> bint_lop op (tnb a) (tnb b)
    | `Lsop (op,a,s) -> (match deflag s with
        | `Int s when s >= 0 -> bint_sop' op (tnb a) s
        | s -> bint_sop op (tnb a) (tnb s))
    | `Ite (b,x,y) -> Expr0.Bint.ite e c (tb b) (tnb x) (tnb y)
    | #flag as f -> apply' tnb f
  and tv v =
    try Hashtbl.find locals_repo v with Not_found -> tlv v
  and tlv v =
    try let e = tp (SMap.find v local_defs) in
        Hashtbl.add locals_repo v e; e
    with Not_found -> Expr0.var e c (translate_symb v)
  and tp ?flag = function
    | `Bexp e -> Expr0.Bool.to_expr (tb e)
    | `Eexp e -> Expr0.Benum.to_expr (te e)
    | `Nexp e -> (try Expr0.Apron.to_expr (tn e) with
        | Failure _ | Exit -> try Expr0.Bint.to_expr (tnb e) with
            | Exit -> illtyped ?flag (`Nexp e))
    | #flag as f -> apply' tp f
    | e -> illtyped ?flag e
  in
  fun e -> try tp e with LocalIllTyped (flag, e) -> raise (illtypedexn ?flag e)

(* -------------------------------------------------------------------------- *)

let mk_typdefs_declaration typs =
  let cmp = fun a b -> Stdlib.compare a b in
  fold_typdefs
    (fun tn typdef -> PMappe.add (translate_symb tn) (translate_typdef typdef))
    typs (PMappe.empty cmp)

(* --- *)

let mk_vars_declaration ?(init=Mappe.empty) vars =
  SMap.fold
    (fun v t -> Mappe.add (translate_symb v) (translate_typ t))
    vars init

(* --- *)

let mk_uc_inputs_declaration uc_vars =
  List.fold_left
    begin fun input { cnig_input_vars; cnig_contr_vars' } ->
      let input = mk_vars_declaration ~init:input cnig_input_vars in
      List.fold_left
        (fun acc (v, t, _) -> Mappe.add (translate_symb v) (translate_typ t) acc)
        input cnig_contr_vars'
    end
    Mappe.empty uc_vars

let mk_io_declarations io_vars =
  List.fold_left
    begin fun (i,o) { fnig_input_vars; fnig_output_vars } ->
      mk_vars_declaration ~init:i fnig_input_vars,
      mk_vars_declaration ~init:o fnig_output_vars
    end
    (Mappe.empty, Mappe.empty) io_vars

(* --- *)

let mk_uc_declarations typs ni =
  let typdefs = mk_typdefs_declaration typs in
  let state = mk_vars_declaration ni.cni_state_vars in
  let input = mk_uc_inputs_declaration ni.cni_uc_vars in
  (typdefs, state, input, ni)

let mk_node_declaration (node: 'f checked_node) =
  mk_uc_declarations (node_desc node).cn_typs (gather_node_info node)

let mk_cfg_declaration (cfg: 'f checked_cfg) =
  let { cg_typs; cg_graph } = cfg_desc cfg in
  let typdefs, state, input, ni =
    mk_uc_declarations cg_typs (gather_cfg_info cfg) in
  (* Define a new enumeration type and variable for locations, in case it has
     not already been done: *)
  let loc_typ, loc_var_spec, typdefs = match CFG.location_typ' cg_graph with
    | Some (tn, flag) ->
        translate_typ (`Enum tn), (`Enum tn, `State', flag), typdefs
    | None ->
        let tn = Symb.fresh' "Location" in
        let loc_typdef =
          translate_typdef' (EnumDef (CFG.all_locations cg_graph)) in
        let loc_typname = translate_symb tn in
        let typdefs = PMappe.add loc_typname loc_typdef typdefs in
        `Benum loc_typname, (`Enum tn, `State', None), typdefs
  in
  let loc_symb, state = match CFG.location_var cg_graph with
    | Some v ->
        v, state
    | None ->
        let v = Symb.fresh' "loc" in
        v, Mappe.add (translate_symb v) loc_typ state
  in
  (typdefs, state, input, ni, loc_symb, loc_var_spec)

let mk_pred_declaration (pred: 'f checked_pred) =
  let { pn_typs } = pred_desc pred in
  let typdefs = mk_typdefs_declaration pn_typs in
  let pi = gather_pred_info pred in
  let input = mk_uc_inputs_declaration pi.pni_uc_vars in
  (typdefs, input, pi)

let mk_func_declaration (func: 'f checked_func) =
  let { fn_typs } = func_desc func in
  let typdefs = mk_typdefs_declaration fn_typs in
  let fi = gather_func_info func in
  let input, outputs = mk_io_declarations fi.fni_io_vars in
  (typdefs, input, outputs, fi)

(* -------------------------------------------------------------------------- *)

let make_ctrlspec ({ Env.env = e; Env.cond = c } as env) uc_vars =

  let open CtrlSpec in
  let tt = Expr0.Bool.dtrue e c and ff = Expr0.Bool.dfalse e c in
  let zero = Expr0.Apron.cst e c (Apron.Coeff.s_of_int 0) in

  let cs_groups = List.fold_right
    begin fun { cnig_input_vars; cnig_contr_vars' } uc_groups ->

      let bu_vars, nu_vars = SMap.fold
        begin fun v t (bu, nu) ->
          let v = translate_symb v in
          match t with
            | `Int | `Real -> bu, v :: nu
            | _            -> v :: bu, nu
        end cnig_input_vars ([], []) in

      let bcp, ncp = List.fold_right
        begin fun (var, t, p) (bcp, ncp) ->
          let v = translate_symb var in
          let te = translate_exp env SMap.empty in
          let teb x : default_expr = `Bool (Expr0.Bool.of_expr (te x)) in
          match t, p with
            | `Bool,(`None | `Descend) -> (v, `Bool tt) :: bcp, ncp
            | `Bool, `Ascend -> (v, `Bool ff) :: bcp, ncp
            | `Bool, `Expr p -> (v, teb p) :: bcp, ncp
            |(#etyp|`Bint _),(`None | `Ascend) -> (v, `Order `Ascend) :: bcp, ncp
            |(#etyp|`Bint _), `Descend -> (v, `Order `Descend) :: bcp, ncp
            | #etyp,   `Expr p -> raise (Unsupported (`EnumDefault var))
            | `Bint _, `Expr p -> raise (Unsupported (`BintDefault var))
            (* | #etyp, Some p -> v :: bc, nc, Some (tee p) :: bp, np *)
            | #ntyp, _ -> bcp, (v, zero) :: ncp             (* XXX: np *)
        end cnig_contr_vars' ([], []) in

      CtrlSpec.make_uc_group bu_vars nu_vars bcp ncp :: uc_groups
    end
    uc_vars
    []
  in
  CtrlSpec.make cs_groups

(* -------------------------------------------------------------------------- *)

(** Return type of the {!translate_node}, {!translate_cfg}, and
    {!translate_pred} functions bellow. *)
type ('f, 'm) res =
  [
  | `TOk of 'm
  | `TKo of [ `TError of ('f option * (Format.formatter -> unit)) list ] list
  ]

let build ?dynamic_variable_reordering ~cond_factor ~build_env build =
  let rec try_build ~cond_factor =
    try
      let e = build_env ~cond_factor in
      let rs = match dynamic_variable_reordering with
        | None -> CuddUtil.dynamic_reordering_save_n_stop e.Env.env
        | Some d -> CuddUtil.dynamic_reordering_save_n_select e.Env.env d in
      let restore x = CuddUtil.dynamic_reordering_restore e.Env.env rs; x in
      let m = try build e |> restore with e -> restore (); raise e in
      Env.compute_careset e;
      `TOk m
    with
      | Bdd.Env.Bddindex ->
          let cond_factor = Stdlib.succ cond_factor in
          Log.w logger "@[Retrying@ with@ cond_factor@ =@ %d…@]" cond_factor;
          try_build ~cond_factor
      | Unsupported (`Verb m) ->
          `TKo [`TError [None, fun fmt -> Format.pp_print_string fmt m]]
  in try_build ~cond_factor

(* -------------------------------------------------------------------------- *)

type static_variable_ordering_algorithm =
  | SVOBasic
  | SVOTopoBFS of static_variable_ordering_algorithm_basic_options
and static_variable_ordering_algorithm_basic_options =
    {
      svo_make_log_fmt: make_log_fmt_func option;
    }
and make_log_fmt_func = ?descr:Realib.Util.ufmt -> ?suff:string -> string
  -> Format.formatter * (unit -> unit)

(* --- *)

let varspecs ~enable_primed_state_vars ~enable_primed_input_vars
    state input (order, rem) =
  let rev_order = List.rev_map translate_symb order
  and rev_rem   = List.rev_map translate_symb rem in
  let rev_classify = List.rev_map (fun v ->
    try Env.Var (v, Mappe.find v state, `State, enable_primed_state_vars)
    with Not_found ->
      try Env.Var (v, Mappe.find v input, `Input, enable_primed_input_vars)
      with Not_found ->
        failwith (Format.asprintf "not found: %a" Env.Symb.print v))
  in
  rev_classify rev_rem @ [ Env.Fixed (rev_classify rev_order) ]

let varspecs_basic ~enable_primed_state_vars ~enable_primed_input_vars
    state input =
  let s = Mappe.bindings state |>
      List.map (fun (v, t) -> Env.Var (v, t, `State, enable_primed_state_vars)) in
  let i = Mappe.bindings input |>
      List.map (fun (v, t) -> Env.Var (v, t, `Input, enable_primed_input_vars)) in
  s @ i

let enum_size = function
  | 0 -> 0
  | n when n >= 0 ->
      let size = ref 0 and reg = ref 1 in
      while n >= !reg do reg := !reg lsl 1; incr size done;
      !size
  | n ->
      let size = ref 1 and reg = ref 1 in
      while -n > !reg do reg := !reg lsl 1; incr size done;
      !size

let enum_sizes typdefs =
  PMappe.fold begin fun tn (`Benum labels) ->
    SMap.add (Symb.of_string (Env.Symb.to_string tn))
      (enum_size (Bdd.Labels.cardinal labels))
  end typdefs SMap.empty

(* --- *)

let dynamic_if_basic ?dynamic_variable_reordering = function
  | SVOBasic when dynamic_variable_reordering = None ->
      Some (!CuddUtil.Options.reorder_policy)
  | _ -> dynamic_variable_reordering

let log_svo_algo s =
  Log.i logger "@[Computing@ initial@ variable@ order@ using@ %s@ \
                  algorithm@<1>…@]" s

let variable_specs_n_order
    ~enable_primed_state_vars
    ~enable_primed_input_vars
    ?(static_variable_ordering = SVOBasic)
    ?dynamic_variable_reordering
    ?strong_variable_affinities
    typdefs
    state
    input
    e
    =
  let varspecs = match static_variable_ordering with
    | SVOBasic ->
        log_svo_algo "Basic";
        varspecs_basic ~enable_primed_state_vars ~enable_primed_input_vars
          state input
    | SVOTopoBFS { svo_make_log_fmt = make_log_fmt } ->
        log_svo_algo "TopoBFS";
        Ordering.Supra.order_variables e ?make_log_fmt
          ?strong_var_affinities:strong_variable_affinities
          Ordering.Supra.State (enum_sizes typdefs) |>
              (varspecs ~enable_primed_state_vars ~enable_primed_input_vars
                 state input)
  in
  (varspecs,
   dynamic_if_basic ?dynamic_variable_reordering static_variable_ordering)

(* -------------------------------------------------------------------------- *)

let build_node ni node ({ Env.env = e; Env.cond = c } as env) =

  let { cn_init; cn_assertion; cn_invariant; cn_reachable;
        cn_attractive } = node_desc node in
  let cs = make_ctrlspec env ni.cni_uc_vars in

  let te = translate_exp env in
  let local_equs = sorted_node_definitions node in
  let local_defs = local_defs local_equs in                 (* fill in locals *)
  List.iter (fun (v, e) -> ignore (te local_defs e)) local_equs;

  let trans_func = SMap.map (te local_defs) ni.cni_trans_specs in
  let trans_func = SMap.bindings trans_func in
  let trans_func = List.rev_map (fun (v, e) -> (translate_symb v, e)) trans_func in
  let tb e = Expr0.Bool.of_expr (te local_defs (`Bexp e)) in
  let opt f d = function | None -> d | Some e -> f e in
  let ff = Expr0.Bool.dfalse e c and tt = Expr0.Bool.dtrue e c in
  let df = {
    d_disc_equs = trans_func;
    d_cont_equs = [];
    d_zero_defs = [];
    d_init    = tb cn_init;
    d_ass     = tb cn_assertion;
    d_final   = opt (fun e -> tb (mk_neg' e)) ff cn_invariant;
    d_reach   = opt tb tt cn_reachable;
    d_attract = opt tb tt cn_attractive;
  } in
  (env, cs, df, local_defs)

(* XXX this has become useless due to new checks in CtrlNbac.AST; that should
   always trigger an intenral error *)
let report_unsupported_default decls = function
  | (`EnumDefault v | `BintDefault v) as err ->
      match SMap.find v decls with
        | _, `Contr (_, _, `Expr p), loc ->
            let loc = match flagof p with None -> loc | p -> p in
            `TKo [`TError [loc, fun fmt -> Format.fprintf fmt
              "Unsupported@ feature:@ default@ expression@ for@ \
               controllable@ %(%)" (match err with
                 | `EnumDefault _ -> "enumeration"
                 | `BintDefault _ -> "Bounded@ Integer")]]
        | _ -> failwith "Internal error in `Cn2rl'."

(** [translate_node ?cond_factor ?enable_primed_state_vars
    ?enable_primed_input_vars n] translates the node [n] into environments
    structures and a data-flow program, that can be fed to algorithms of the
    {{:../realib/index.html}[Realib]} library.

    Default value for [cond_factor] is [1].

    Returned value either indicates a successful translation, or a series of
    error messages. *)
let translate_node
    ?max_cudd_mem
    ?env_booking_factor
    ?(env_cond_factor = 1)
    ?env_max_conds
    ?(enable_primed_state_vars = true)
    ?(enable_primed_input_vars = false)
    ?static_variable_ordering
    ?dynamic_variable_reordering
    ?strong_variable_affinities
    (node: 'f checked_node)
    =
  let typdefs, state, input, ni = mk_node_declaration node in
  let varspecs, dynamic_variable_reordering =
    variable_specs_n_order
      ~enable_primed_state_vars
      ~enable_primed_input_vars
      ?static_variable_ordering
      ?dynamic_variable_reordering
      ?strong_variable_affinities
      typdefs state input (`Node (node, Some ni))
  in
  try
    build ?dynamic_variable_reordering ~cond_factor:env_cond_factor
      ~build_env:(fun ~cond_factor ->
        Env.make' ?max_mem:max_cudd_mem
          ?booking_factor:env_booking_factor ~cond_factor
          ~enable_zeros:false ?max_conds:env_max_conds
          typdefs varspecs)
      (build_node ni node)
  with
    | Unsupported (`EnumDefault _ | `BintDefault _ as err) ->
        report_unsupported_default (node_desc node).cn_decls err

(* --- *)

module CfgBuild (E: Env.T) (Opts: sig
  val cdf_trans_only: bool
  val ctrls: E.BEnv.vars
end) = struct
  open E
  open Opts
  module POps = BddapronUtil.POps (BEnv)
  module BOps = BddapronUtil.BOps (BEnv)
  open BOps
  open BEnv

  let eq_label = Expr0.Benum.eq_label env cond
  let of_label = Bdd.Enum.of_label env
  let ite = Expr0.ite env cond
  let var = Expr0.var env cond
  let opt f d = function | None -> d | Some e -> f e

  type t =
      {
        cfg: Cfg.t;
        trans: (Env.var_t, Env.expr_t) Mappe.t;
        locids: (Cfg.locid_t * Env.boolexpr_t) SMap.t;
        init: Env.boolexpr_t;
        final: Env.boolexpr_t;
        assertion: Env.boolexpr_t;
        reachable: Env.boolexpr_t;
      }

  open CFG

  let locid loc_expr cfg loc_label locids =
    let loc_symb = label_symb loc_label in
    try SMap.find loc_symb locids, locids with
      | Not_found ->
          let locinv = eq_label loc_expr (translate_label loc_label) in
          let locid = Cfg.add_loc E.env cfg (Loc.make_inv locinv) in
          let locids = SMap.add loc_symb (locid, locinv) locids in
          (locid, locinv), locids

  let use_df =
    if not cdf_trans_only then fun _ -> true
    else let ctrls = varset ctrls in
         fun e -> not (PSette.is_empty (PSette.inter ctrls (POps.support e)))

  let build_loc tb te loc_var locid
      ({ loc_name; loc_arcs } as l)
      (acc, locids, loc_trans)
      =
    let (srcid, inloc), locids = locid loc_name locids in
    let acc = { acc with
      init = acc.init ||~ (inloc &&~ opt tb ff l.loc_init);
      final = acc.final ||~ (inloc &&~ opt tb ff l.loc_final);
      assertion = acc.assertion &&~ (inloc =>~ opt tb tt l.loc_assertion);
      reachable = acc.reachable &&~ (inloc =>~ opt tb ff l.loc_reachable);
    } in
    fold_arcs begin fun { arc_dest; arc_guard; arc_updates }
      ((acc, locids, loc_trans), pg) ->
        let (destid, _), locids = locid arc_dest locids  in
        let dest = of_label (translate_label arc_dest) in
        let guard = opt tb tt arc_guard &&~ !~pg in
        let global_guard = inloc &&~ guard in
        let gtf, tf = SMap.fold begin fun v (`State (e, _)) (gtf, tf) ->
          let v = translate_symb v and e = te e in
          let gtf =
            if use_df (POps.of_bexpr global_guard) || use_df e then
              let e' = try Mappe.find v gtf with Not_found -> var v in
              Mappe.add v (ite global_guard e e') gtf
            else gtf
          in
          (gtf, ((v, e) :: tf))
        end arc_updates (acc.trans, []) in
        let enter_eq = loc_var, Expr0.Benum.to_expr dest in
        let arc = Arc.Normal (guard, (enter_eq :: tf)) in
        ignore (Cfg.add_arc E.env acc.cfg srcid destid arc);
        let loc_trans = Expr0.Benum.ite env cond global_guard dest loc_trans in
        ({ acc with trans = gtf }, locids, loc_trans), pg ||~ guard
    end loc_arcs ((acc, locids, loc_trans), ff) |> fst

  let build tb te loc_var
      { cg_decls; cg_graph; cg_init; cg_assertion; cg_invariant; cg_reachable;
        cg_attractive } =
    let loc_expr = Expr0.Benum.var env cond loc_var in
    let empty = {
      cfg = Cfg.make_empty ();
      trans = Mappe.empty;
      locids = SMap.empty;
      init = ff; final = ff; assertion = tt; reachable = tt;
    } in
    let locid = locid loc_expr empty.cfg in
    let acc, locids, loc_trans =
      fold_locations (build_loc tb te loc_var locid)
        cg_graph (empty, SMap.empty, loc_expr)
    in
    let trans =
      let e = Expr0.Benum.to_expr loc_trans in
      if use_df e
      then Mappe.add loc_var e acc.trans
      else acc.trans
    in
    { acc with
      trans;
      init      = acc.init &&~ tb cg_init;
      final     = acc.final ||~ opt (fun e -> tb (mk_neg' e)) ff cg_invariant;
      assertion = acc.assertion &&~ tb cg_assertion;
      reachable = acc.reachable &&~ opt tb tt cg_reachable; }

end

let build_cfg ?(cdfequs_only = true) ni cfg loc_var
    ({ Env.env = e; Env.cond = c } as env) =

  let { cg_decls; cg_graph; cg_init; cg_assertion; cg_invariant; cg_reachable;
        cg_attractive } as desc = cfg_desc cfg in
  let cs = make_ctrlspec env ni.cni_uc_vars in

  let te = translate_exp env in
  let local_equs = sorted_cfg_definitions cfg in
  let local_defs = local_defs local_equs in                 (* fill in locals *)
  List.iter (fun (v, e) -> ignore (te local_defs e)) local_equs;
  let te = te local_defs in
  let tb e = Expr0.Bool.of_expr (te (`Bexp e)) in

  let module E = (val Env.as_module env) in
  let module O = struct
    let cdf_trans_only = cdfequs_only
    let ctrls = CtrlSpec.c_vars cs
  end in
  let module B = CfgBuild (E) (O) in
  let x = B.build tb te loc_var desc in
  let cf = B.{
    c_cfg       = x.cfg;
    c_disc_equs = Mappe.bindings x.trans;
    c_cont_equs = [];
    c_init      = x.init;
    c_final     = x.final;
    c_ass       = x.assertion;
    c_reach     = x.reachable;
    c_attract   = BOps.tt;
    (* c_reach     = opt tb tt cg_reachable;                              (\* XXX *\) *)
    (* c_attract   = opt tb tt cg_attractive;                             (\* XXX *\) *)
  } in
  (env, cs, cf, local_defs)

(** [translate_cfg ?env_booking_factor ?env_cond_factor ?env_max_conds
    ?enable_primed_state_vars ?enable_primed_input_vars g] translates the CFG
    [g] into environment structures and a control-flow program, that can be fed
    to algorithms of the {{:../realib/index.html}[Realib]} library.

    Default value for [env_cond_factor] is [1].

    Returned value either indicates a successful translation, or a series of
    error messages. *)
let translate_cfg
    ?max_cudd_mem
    ?env_booking_factor
    ?(env_cond_factor = 1)
    ?env_max_conds
    ?(enable_primed_state_vars = true)
    ?(enable_primed_input_vars = false)
    ?static_variable_ordering
    ?dynamic_variable_reordering
    ?strong_variable_affinities
    ?cdfequs_only
    (cfg: 'f checked_cfg)
    =
  let typdefs, state, input, ni, loc_var, loc_var_spec = mk_cfg_declaration cfg in
  let varspecs, dynamic_variable_reordering =
    variable_specs_n_order
      ~enable_primed_state_vars
      ~enable_primed_input_vars
      ?static_variable_ordering
      ?dynamic_variable_reordering
      ?strong_variable_affinities
      typdefs state input (`Cfg (cfg, loc_var, loc_var_spec))
  in
  try
    build ?dynamic_variable_reordering ~cond_factor:env_cond_factor
      ~build_env:(fun ~cond_factor ->
        Env.make' ?max_mem:max_cudd_mem
          ?booking_factor:env_booking_factor ~cond_factor
          ~enable_zeros:false ?max_conds:env_max_conds
          typdefs varspecs)
      (build_cfg ?cdfequs_only ni cfg (translate_symb loc_var))
  with
    | Unsupported (`EnumDefault _ | `BintDefault _ as err) ->
        report_unsupported_default (cfg_desc cfg).cg_decls err

(* --- *)

let build_pred pi pred ({ Env.env = e; Env.cond = c } as env) =

  let { pn_decls; pn_value } = pred_desc pred in
  let cs = make_ctrlspec env pi.pni_uc_vars in

  let te = translate_exp env in
  let local_equs = sorted_pred_definitions pred in
  let local_defs = local_defs local_equs in                 (* fill in locals *)
  List.iter (fun (v, e) -> ignore (te local_defs e)) local_equs;
  let tb e = Expr0.Bool.of_expr (te local_defs (`Bexp e)) in
  let predicate = tb pn_value in
  (env, cs, predicate, local_defs)

(** [translate_pred ?num_factor ?enable_primed_state_vars
    ?enable_primed_input_vars p] translates the predicate [p] into environments
    structures and a Boolean expression suitable for handling by the
    {{:../realib/index.html}[Realib]} library.

    Default value for [cond_factor] is [1].

    Returned value either indicates a successful translation, or a series of
    error messages. *)
let translate_pred
    ?max_cudd_mem
    ?env_booking_factor
    ?(env_cond_factor=1)
    ?env_max_conds
    ?env
    ?enable_primed_state_vars
    ?enable_primed_input_vars
    ?dynamic_variable_reordering
    (pred: 'f checked_pred)
    =
  let typdefs, input, pi = mk_pred_declaration pred in
  try
    build ?dynamic_variable_reordering ~cond_factor:env_cond_factor
      ~build_env:(fun ~cond_factor -> match env with
        | Some e -> e                                 (* TODO: Env.upgrade ... *)
        | None ->
            Env.make ?max_mem:max_cudd_mem
              ?booking_factor:env_booking_factor ~cond_factor
              ~enable_zeros:false ?max_conds:env_max_conds
              ?enable_primed_state_vars ?enable_primed_input_vars
              typdefs Mappe.empty input)
      (build_pred pi pred)
  with
    | Unsupported (`EnumDefault _ | `BintDefault _ as err) ->
        report_unsupported_default (pred_desc pred).pn_decls err

(* -------------------------------------------------------------------------- *)

let build_func fi func ({ Env.env = e; Env.cond = c } as env) =

  let { fn_assertion } = func_desc func in

  let te = translate_exp env in
  let local_equs = sorted_func_definitions func in
  let local_defs = local_defs local_equs in                 (* fill in locals *)
  List.iter (fun (v, e) -> ignore (te local_defs e)) local_equs;
  let output_equs = fi.fni_io_vars
    |> List.rev_map
        (fun { fnig_output_vars = outputs } ->
          SMap.merge (fun _ typ expr -> match typ, expr with
            | Some t, Some e -> Some (te local_defs e)
            | _ -> None) outputs fi.fni_all_specs
            |> SMap.bindings)
    |> List.rev_map (List.rev_map (fun (v, e) -> (translate_symb v, e)))
  in
  let tb e = Expr0.Bool.of_expr (te local_defs (`Bexp e)) in
  let assertion = tb fn_assertion in
  (env, output_equs, assertion, local_defs)

(** [translate_func ?num_factor ?enable_primed_state_vars
    ?enable_primed_input_vars f] translates the function f[] into environments
    structures and a function (equaltions) suitable for handling by the
    {{:../realib/index.html}[Realib]} library.

    Default value for [cond_factor] is [1].

    Returned value either indicates a successful translation, or a series of
    error messages. *)
let translate_func
    ?max_cudd_mem
    ?env_booking_factor
    ?(env_cond_factor=1)
    ?env_max_conds
    ?env
    ?enable_primed_state_vars
    ?enable_primed_input_vars
    ?dynamic_variable_reordering
    (func: 'f checked_func)
    =
  let typdefs, input, output, fi = mk_func_declaration func in
  build ?dynamic_variable_reordering ~cond_factor:env_cond_factor
    ~build_env:(fun ~cond_factor -> match env with
      | Some e -> e                                   (* TODO: Env.upgrade ... *)
      | None ->
          Env.make ?max_mem:max_cudd_mem
            ?booking_factor:env_booking_factor ~cond_factor
            ~enable_zeros:false ?max_conds:env_max_conds
            ?enable_primed_state_vars ?enable_primed_input_vars
            typdefs output input)
    (build_func fi func)

(* -------------------------------------------------------------------------- *)
