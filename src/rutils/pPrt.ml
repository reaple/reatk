(******************************************************************************)
(* author: Nicolas Berthier *)
(* This file is part of ReaTK released under the GNU GPL.  Please read the
   LICENSE file packaged in the distribution *)
(******************************************************************************)

open Format

(* -------------------------------------------------------------------------- *)

type 'a fmt = ('a, formatter, unit) format
type ufmt = unit fmt
type relfmt = (ufmt -> ufmt -> unit) fmt

type ('a, 'x) xfmt = ('a -> 'x, formatter, 'x) format

module P = struct
  type pu = formatter -> unit
  type 'a pp = formatter -> 'a -> unit
  let pu fmt = ()
  let pc c fmt = pp_print_char fmt c
  let ps s fmt = pp_print_string fmt s
  let pp f fmt = fprintf fmt f
  let pp1 f a fmt = fprintf fmt f a
  let pp2 f a b fmt = fprintf fmt f a b
  let pp3 f a b c fmt = fprintf fmt f a b c
  let pp4 f a b c d fmt = fprintf fmt f a b c d
  let pp1' f = function | None -> pu | Some a -> pp1 f a
  let pl
      ?(fopen: ufmt = "[@[")
      ?(fsep: ufmt = ",@ ")
      ?(fclose: ufmt = "@]]")
      ?(fempty: ufmt option)
      pp_e lst fmt
      =
    let empty = List.fold_left begin fun first e ->
      (if first then pp fopen else pp fsep) fmt; pp_e fmt e; false
    end true lst in
    if empty then match fempty with
      | None -> pp2 "%(%)%(%)" fopen fclose fmt
      | Some fempty -> pp fempty fmt
    else pp fclose fmt
end
include P

let pp_lst ?fopen ?fsep ?fclose ?fempty pp_e fmt l =
  pl ?fopen ?fsep ?fclose ?fempty pp_e l fmt

let ufmt_from_string s : ufmt =
  Scanf.format_from_string s ""

let ufmt_from_breakable_string s =
  Str.global_replace (Str.regexp_string " ") "@ " s |> ufmt_from_string

(* -------------------------------------------------------------------------- *)

(* prints an array *)
let array_print ?(csep=";") ?(copen="[") ?(cclose="]") pp fmt l =
  pp_print_string fmt copen;
  let start = ref(true) in
  Array.iter
    (fun e->
      if not(!start) then pp_print_string fmt csep
      else start := false;
      pp fmt e)
    l;
  pp_print_string fmt cclose

(* prints a list *)
let list_print ?(csep=";") ?(copen="[") ?(cclose="]") pp fmt l =
  pp_print_string fmt copen;
  let start = ref(true) in
  List.iter
    (fun e->
      if not(!start) then pp_print_string fmt csep
      else start := false;
      pp fmt e)
    l;
  pp_print_string fmt cclose

(* prints a list (second variant) *)
let list_print'
    ?(csep=pc ';')
    ?(copen=pc '[')
    ?(cclose=pc ']')
    pp fmt l =
  copen fmt;
  let _ =
    List.fold_left (fun start e -> if not start then csep fmt; pp fmt e; false)
      true l
  in
  cclose fmt

(* prints a string such that linebreaks happen at whitespaces *)
let print_breakable fmt s =
  let ss = Str.split (Str.regexp_string " ") s in
  List.iter (fun s -> fprintf fmt "%s@ " s) ss

(* prints a string such of a fixed length (truncates or fills up with whitespaces *)
let print_fixed fmt len s =
  let slen = String.length s in
  if slen>len then
    pp_print_string fmt (String.sub s 0 len)
  else
  begin
    pp_print_string fmt s;
    for i=slen to len-1 do pp_print_string fmt " "; done
  end

(* -------------------------------------------------------------------------- *)
