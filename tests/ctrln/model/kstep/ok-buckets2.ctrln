(* `sL:k=2' should work. *)

!typedef
  Move = enum { Y2Z, Z2X, None };

!state
  x, y, z: int;

!input
  m: Move;

!controllable
  x2y: int;

!transition
  x' = x - x2y        + (if m = Z2X and z > 0 then 1 else 0);
  y' = y - (if m = Y2Z and y > 0 then 1 else 0) + x2y;
  z' = z - (if m = Z2X and z > 0 then 1 else 0) + (if m = Y2Z and y > 0 then 1 else 0);

!initial
  x = 10 and y = 0 and z = 0;

!assertion
  (*x >= 0 and y >= 0 and z >= 0 and*)
  (*x2y <= x and*)
  x2y >= 0 and x2y <= x and
  (*x2y <= 30 and*)
  (if m = Y2Z then y > 0 else true) and
  (if m = Z2X then z > 0 else true) and
  (*x <= 10 and y <= 10 and z <= 10 and*)
  (*x + y + z = 10 and*)
  true
;

!invariant
  (*x >= 0 and y >= 0 and z >= 0 and (* <- necessary, unless option
                                           "deads" is given! *)*)
  (*x <= 10 and y <= 10 and z <= 10 and*)
  z <= 3
;
