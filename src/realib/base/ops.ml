(** High-level operations on CFGs *)

(* -------------------------------------------------------------------------- *)

(** {2 Data-Flow to Control-Flow Interface } *)

(** DF program into CF program translation *)
type df2cf = Env.t -> Program.dfprog_t -> Env.t * Program.cfprog_t

(* -------------------------------------------------------------------------- *)

(** {2 Control-Flow Preprocessing Interface } *)

(** CFG transformation procedure *)
type trans = Env.t -> Program.cfprog_t -> Program.cfprog_t

(* -------------------------------------------------------------------------- *)

(** {2 Analysis Interface } *)

(** Analysis function *)
type analysis = Analysis.t

(* -------------------------------------------------------------------------- *)

(** {2 Synthesis Interface } *)

(** Synthesis function *)
type synthesis = Synthesis.synth
type coreach = Synthesis.coreach

(* -------------------------------------------------------------------------- *)

(** {2 Optimization Interface } *)

(** Optimization function *)
type optimization = Optimization.t

(* -------------------------------------------------------------------------- *)
