(** Utilities used by the Controllable-Nbac parser.

    @author Nicolas Berthier *)

open Format
open Lexing
open Parsing
open AST

let (&) f a = f a

(* -------------------------------------------------------------------------- *)

type 'm error_state =
    {
      mutable error_flag: bool;
      mutable error_msgs: 'm list;
    }

let mk_error_state () = { error_flag = false; error_msgs = [] }

let reset_error_state es =
  es.error_flag <- false;
  es.error_msgs <- []

let push_error es e =
  es.error_flag <- true;
  es.error_msgs <- e :: es.error_msgs

let push_warn es e =
  es.error_msgs <- e :: es.error_msgs

(* -------------------------------------------------------------------------- *)

type loc = Loc.t

let ( <% ) a b = Loc.compare a b < 0
let orderloc a b = if b <% a then b, a else a, b

type syntax_msg =
  [
  | `PError of Loc.t * (formatter -> unit)
  | `PWarn of Loc.t * (formatter -> unit)
  ]

type model_msg = Loc.t loc_msg

let serrs: syntax_msg error_state = mk_error_state ()
let merrs: model_msg error_state = mk_error_state ()
let get_syntax_msgs () = List.rev serrs.error_msgs
let get_model_msgs () = List.rev merrs.error_msgs

(* -------------------------------------------------------------------------- *)

let pos_pred ({ pos_bol; pos_cnum } as p) =
  if pos_cnum = pos_bol then p else { p with pos_cnum = pred pos_cnum }

(* let pos_succ ({ pos_cnum } as p) = { p with pos_cnum = succ pos_cnum } *)

let perror0 loc msg = push_error serrs (`PError (loc, msg))
let perror s f msg = perror0 (Loc.make s f) (fun fmt -> fprintf fmt msg)
let perror1 s f msg a = perror0 (Loc.make s f) (fun fmt -> fprintf fmt msg a)
let perror2 s f msg a b = perror0 (Loc.make s f) (fun fmt -> fprintf fmt msg a b)

let merror0 ?loc msg = push_error merrs (`MError [ loc, msg ])
let merror s f msg = merror0 ~loc:(Loc.make s f) (fun fmt -> fprintf fmt msg)
let merror1 s f msg a = merror0 ~loc:(Loc.make s f) (fun fmt -> fprintf fmt msg a)

let warning s f msg =
  push_warn serrs (`PWarn (Loc.make s f, (fun fmt -> fprintf fmt msg)))

(* -------------------------------------------------------------------------- *)

let vardecl_error s f lst =
  let ps = Symb.print in
  let msg = match lst with
    | [] -> failwith "Internal error in parsing of variable declarations"
    | [v] -> (fun fmt -> fprintf fmt "variable@ `%a'" ps v)
    | [v; w] -> (fun fmt -> fprintf fmt "variables@ `%a,@ %a'" ps v ps w)
    | v :: w :: _ -> (fun fmt -> fprintf fmt "variables@ `%a,@ %a...'" ps v ps w)
  in
  push_error serrs
    (`PError (Loc.make s (pos_pred f),
              fun fmt -> fprintf fmt "Syntax@ error@ in@ declaration@ of@ %t" msg))

(* -------------------------------------------------------------------------- *)

let opt e = function
  | Some a -> Some (e a)
  | None -> None

let flag' l e = flag l e, l

let protect_uop s f (op, def) (e, _) =
  let loc = Loc.make s f in
  flag' loc (try op e with
    | TypeError msg -> merror0 ~loc msg; def)

let protect_op s f (op, def) (e1, _) (e2, _) =
  let loc = Loc.make s f in
  flag' loc (try op e1 e2 with
    | TypeError msg -> merror0 ~loc msg; def)

let protect_nop s f (op, def) (e1, _) (e2, _) =
  let loc = Loc.make s f in
  flag' loc (try op e1 e2 [] with
    | TypeError msg -> merror0 ~loc msg; def)

let select_exp select (e, loc) =
  try Some (select e) with
    | TypeError msg -> (merror0 ~loc msg; None)

(* let opt_bexp s f (e, _) = try Some (as_bexp e) with *)
(*   | TypeError msg -> merror0 ~loc:(Loc.make s f) (split msg); None *)

let mk_unop mk s f (op, select, def, sop, fop) e =
  let loc: loc = Loc.make sop fop in
  flag loc (match select_exp select e with
    | Some e ->
        (try mk (op e) with
          | TypeError msg -> (merror0 ~loc msg; def))
    | _ -> def),
  Loc.make s f

let mk_bunop x = mk_unop (fun e -> `Bexp e) x
let mk_nunop x = mk_unop (fun e -> `Nexp e) x

let mk_binop mk s f (op, select, def, sop, fop) e1 e2 =
  let loc: loc = Loc.make sop fop in
  flag loc (match select_exp select e1, select_exp select e2 with
    | Some e1, Some e2 ->
        (try mk (op e1 e2) with
          | TypeError msg -> (merror0 ~loc msg; def))
    | _ -> def),
  Loc.make s f

(* let mk_binop' mk s f (op, select, def, sop, fop) e1 e2 = *)
(*   let loc: loc = Loc.make sop fop in *)
(*   flag loc (match select_exp select e1 with *)
(*     | Some e1 -> *)
(*         (try mk (op e1 e2) with *)
(*           | TypeError msg -> (merror0 ~loc msg; def)) *)
(*     | _ -> def), *)
(*   Loc.make s f *)

let mk_bbinop x = mk_binop (fun e -> `Bexp e) x
let mk_nbinop x = mk_binop (fun e -> `Nexp (e [])) x
let mk_lbinop x = mk_binop (fun e -> `Nexp e) x
let mk_lsinop x = mk_binop (fun e -> `Nexp e) x

(* -------------------------------------------------------------------------- *)

type typdescs = (symb * loc * (symb * loc) list) list
type var_decls = (symb * typ * loc) list
type equ_decls = (symb * loc exp * loc) list

(* --- *)

type ctrl_decls = (symb * loc default_spec * typ * loc) list
type uc_decls = var_decls * ctrl_decls
type node_formulas =
    {
      nf_initial:    loc bexp option;
      nf_assertion:  loc bexp option;
      nf_invariant:  loc bexp option;
      nf_reachable:  loc bexp option;
      nf_attractive: loc bexp option;
    }
type node_descr = (typdescs * var_decls * uc_decls list * var_decls)
    * equ_decls * equ_decls * node_formulas

type location_formulas =
    {
      ld_initial:   loc bexp option;
      ld_final:     loc bexp option;
      ld_assertion: loc bexp option;
      ld_reachable: loc bexp option;
      (* nf_attractive: loc bexp option; *)
    }
type id = symb * loc
type arc = loc bexp option * equ_decls * id
type arcs = arc list
type location = symb * loc * location_formulas * arcs
type locations = location list
type graph = id option * loc * locations

type cfg_descr = (typdescs * var_decls * uc_decls list * var_decls)
    * equ_decls * graph * equ_decls * node_formulas

(* --- *)

type io_decls = var_decls * var_decls
type func_formulas =
    {
      ff_assertion:  loc bexp option;
    }
type func_descr = (typdescs * io_decls list * var_decls)
    * equ_decls * func_formulas

(* --- *)

type pred_descr = (typdescs * uc_decls list * var_decls)
    * equ_decls * loc bexp

(* --- *)

let pp_symb = Symb.print
let err0 loc msg = merror0 ~loc msg
let err2 loc f a b = err0 loc (fun fmt -> fprintf fmt f a b)
let err4 loc f a b c d = err0 loc (fun fmt -> fprintf fmt f a b c d)

let pp_varkind f = function
  | `State _ | `State' -> fprintf f "state"
  | `Input _ -> fprintf f "non-controllable@ input"
  | `Contr _ -> fprintf f "controllable@ input"
  | `Local _ -> fprintf f "local"
  | `Output _ -> fprintf f "output"

let redeftyp l ?prev_loc =
  let r msg msg' tn = match prev_loc with
    | None -> err2 l msg pp_symb tn
    | Some l' -> push_error merrs (`MError (
      [ Some l, (fun fmt -> fprintf fmt msg pp_symb tn);
        Some l', (fun fmt -> fprintf fmt msg')]) :> model_msg)
  in
  r "In@ type@ definitions,@ redefinition@ of@ type@ `%a'"
    "Previous@ definition"

let mk_typdefs =
  List.fold_left
    begin fun tds (tn, loc, labels) ->
      let typname = mk_typname tn in
      let labels = List.map (fun (l, loc) -> mk_label l, loc) labels in
      try match find_typdef typname tds with
        | _, Some prev_loc -> redeftyp loc ~prev_loc tn; tds
        | _, None -> redeftyp loc tn; tds
      (* | exception Not_found -> ... when we restrict to OCaml >= 4.02 *)
      with
        | Not_found -> declare_typ typname (mk_etyp' ~flag:loc labels) tds
    end
    empty_typdefs

let respec msg prev_msg l l' symb = push_error merrs
  (`MError (let l, l' = orderloc l l' in
            [ Some l', (fun fmt -> fprintf fmt msg pp_symb symb);
              Some l, (fun fmt -> fprintf fmt prev_msg) ]) :> model_msg)
let redecl = respec "Multiple@ declarations@ of@ variable@ `%a'" "Previous@ \
                     declaration"
let redef = respec "Multiple@ definitions@ for@ variable@ `%a'" "Previous@ \
                    definition"

let fold_decls dbl =
  List.fold_left
    (fun acc (v, t, loc) -> try dbl loc (snd & SMap.find v acc) v; acc with
      | Not_found -> SMap.add v (t, loc) acc)
    SMap.empty

let dummy_exp typdefs = function
  | `Bool -> mk_bcst true
  | `Enum tn -> mk_ecst (match find_typdef tn typdefs |> fst with
      | EnumDef labels -> fst & List.hd labels)
  | `Int -> mk_nicst 0
  | `Real -> mk_nrcst 0.0
  | `Bint (s, w) -> mk_nbicst s w 0

(* --- *)

let merge_decls aspec adecls bspec bdecls =
  SMap.merge
    begin fun var a b -> match a, b with
      | Some v, None | None, Some v -> Some v
      | Some (_, _, Some l), Some (_, _, Some l') -> redecl l l' var; None
      | Some (_, _, Some l), Some _ ->
          redecl l (SMap.find var bspec |> snd) var; b
      | Some _, Some (_, _, Some l) ->
          redecl (SMap.find var aspec |> snd) l var; b
      | _ -> assert false
    end adecls bdecls

let add_uc_list decls uc_list =
  (* groups of non-controllable & controllable inputs *)
  let decls, _ = List.fold_left begin fun (decls, g) (input, contr) ->

    let input = fold_decls redecl input in

    let decls = SMap.merge begin fun var a b -> match a, b with
      | None, Some (t, l) -> Some (t, `Input g, Some l)
      | Some x, None -> Some x
      | Some (_, _, Some pl) as s, Some (_, l) -> redecl l pl var; s
      | _ -> assert false
    end decls input in

    let decls, _ = List.fold_left begin fun (decls, r) (var, pspec, typ, loc) ->
      try match SMap.find var decls with
        | _, _, Some prev_loc -> redecl loc prev_loc var; (decls, r)
        | _ -> assert false
      with
        | Not_found ->
            SMap.add var (typ, `Contr (g, r, pspec), Some loc) decls, succ r
    end (decls, one) contr in

    (decls, succ g)
  end (decls, one) uc_list in

  decls

let add_io_list dummy_exp defs decls io_list =
  let decls, _ = List.fold_left begin fun (decls, g) (input, output) ->

    let input = fold_decls redecl input in
    let decls = SMap.merge begin fun var a b -> match a, b with
      | None, Some (t, l) -> Some (t, `Input g, Some l)
      | Some x, None -> Some x
      | Some (_, _, Some pl) as s, Some (_, l) -> redecl l pl var; s
      | _ -> assert false
    end decls input in

    let output = fold_decls redecl output in
    let decls = SMap.fold begin fun var (t, l) decls ->
      let e, l' = (try let e, l = SMap.find var defs in e, Some l with
        | Not_found -> err2 l "Missing@ equation@ for@ output@ variable@ \
                              `%a'" pp_symb var; dummy_exp t, None) in
      SMap.add var (t, `Output (g, e, Some l), l') decls
    end output decls in

    (decls, succ g)
  end (decls, one) io_list in

  ignore (SMap.merge begin fun var a b -> match a, b with
    | None, Some (_, l) -> err2 l "Undeclared@ variable@ `%a'" pp_symb var; None
    | _ -> None
  end decls defs);

  decls

let add_local_vars ?(notify_local=true) dummy_exp defs local =
  SMap.merge begin fun var exp decl -> match exp, decl with
    | Some (e, l), Some (t, l') -> Some (t, `Local (e, Some l), Some l')
    | Some (_, l), None when not notify_local -> None
    | Some (_, l), None -> err2 l "Undeclared@ local@ variable@ \
                                  `%a'" pp_symb var; None
    | None, Some (t, l) ->(err2 l "Missing@ equation@ for@ local@ variable@ \
                                  `%a'" pp_symb var;
                          Some (t, `Local (dummy_exp t, Some l), None))
    | _ -> assert false
  end defs local

let add_state_vars dummy_exp trans state =
  SMap.merge begin fun var exp decl -> match exp, decl with
    | Some (e, l), Some (t, l') -> Some (t, `State (e, Some l), Some l')
    | Some (_, l), None -> err2 l "Undeclared@ state@ variable@ \
                                  `%a'" pp_symb var; None
    | None, Some (t, l) ->(err2 l "Missing@ transition@ function@ for@ \
                                  variable@ `%a'" pp_symb var;
                          Some (t, `State (dummy_exp t, Some l), None))
    | _ -> assert false
  end trans state

(* --- *)

let add_state_decls trans state =
  SMap.merge begin fun var exp decl -> match exp, decl with
    | None, Some (t, l) -> Some (t, `State', Some l)
    | _ -> assert false
  end trans state

let add_updates partial_updates (decls: loc AST.cfg_decls) =
  SMap.fold begin fun var (e, l) map ->
    try match SMap.find var decls with
      | _, `State', _ -> SMap.add var (`State (e, Some l)) map
      | _, spec, l' -> err4 l "State@ variable@ expected@ \
                              (got@ %a@ `%a')" pp_varkind spec pp_symb var; map
    with
      | Not_found -> err2 l "Undeclared@ state@ variable@ `%a'" pp_symb var; map
  end partial_updates SMap.empty

  (* SMap.merge begin fun var exp typ -> match exp, typ with *)
  (*   (\* | Some (e, l), Some (t, l') -> Some (t, `State (e, Some l), Some l') *\) *)
  (*   | Some (_, l), None -> err2 l "Undeclared@ state@ variable@ \ *)
  (*                                 `%a'" pp_symb var; None *)
  (*   | None, Some (t, l) -> Some (t, `State (Some l), None) *)
  (*   | _ -> assert false *)
  (* end partial_updates state *)

let mk_graph typdefs decls (loc_var, flag, locations) =
  (* let dummy_exp = dummy_exp typdefs in *)
  let cfg = CFG.empty ~flag ?loc_var:(match loc_var with
    | None -> None
    | Some (v, l) -> Some (v, Some l)) () in
  List.fold_left begin fun cfg (loc_name, flag, forms, arcs) ->
    let loc_name = mk_label loc_name in
    (try match CFG.find_location loc_name cfg with
      | { CFG.loc_flag = Some prev_flag } ->
          respec "Multiple@ declarations@ of@ location@ `%a'"
            "Previous@ declaration" flag prev_flag (label_symb loc_name)
      | _ -> assert false    (* Normally unreachable as we always attach flags *)
     with Not_found -> ());
    let arcs = List.fold_left begin fun arcs (guard, updates, (dest, flag)) ->
      let dest = mk_label dest in
      let updates = fold_decls redef updates in
      let updates = add_updates updates decls in
      CFG.append_arc arcs ~flag ?guard updates ~dest
    end CFG.no_arc arcs in
    CFG.add_location cfg ~flag loc_name
      ?init:forms.ld_initial
      ?final:forms.ld_final
      ?assertion:forms.ld_assertion
      ?reachable:forms.ld_reachable
      arcs
  end cfg locations

(* --- *)

let mk_node_decls typdefs (_, state, uc_list, local) defs trans =

  let dummy_exp = dummy_exp typdefs in

  let state = fold_decls redecl state in
  let local = fold_decls redecl local in
  let defs = fold_decls redef defs in
  let trans = fold_decls redef trans in

  (* state & local variable definitions *)
  let local_decls = add_local_vars dummy_exp defs local in
  let state_decls = add_state_vars dummy_exp trans state in
  let decls = merge_decls state state_decls local local_decls in

  add_uc_list decls uc_list

(* --- *)

let mk_cfg_decls typdefs (_, state, uc_list, local) defs global_trans =

  let dummy_exp = dummy_exp typdefs in

  let state = fold_decls redecl state in
  let local = fold_decls redecl local in
  let defs = fold_decls redef defs in
  let trans = fold_decls redef global_trans in

  (* state & local variable definitions *)
  let local_decls = add_local_vars dummy_exp defs local in
  let state_decls = add_state_decls trans state in
  let decls = merge_decls state state_decls local local_decls in

  add_uc_list decls uc_list

(* --- *)

let mk_func_decls typdefs (_, io_list, local) defs =

  let dummy_exp = dummy_exp typdefs in

  let local = fold_decls redecl local in
  let defs = fold_decls redef defs in

  (* local variable definitions *)
  let decls = add_local_vars ~notify_local:false dummy_exp defs local in
  (* let decls = SMap.merge begin fun var exp typ -> match exp, typ with *)
  (*   | Some (e, l), Some (t, l') -> Some (t, `Local (e, Some l), Some l') *)
  (*   | Some (_, l), None -> (\* err2 l "Undeclared@ local@ variable@ \ *\) *)
  (*                         (\*         `%a'" pp_symb var;  *\)None *)
  (*   | None, Some (t, l) ->(err2 l "Missing@ equation@ for@ local@ variable@ \ *)
  (*                                 `%a'" pp_symb var; *)
  (*                         Some (t, `Local (dummy_exp t, Some l), None)) *)
  (*   | _ -> assert false *)
  (* end defs local in *)

  (* I/O groups *)
  add_io_list dummy_exp defs decls io_list

(* --- *)

let mk_pred_decls typdefs (_, uc_list, local) defs =

  let dummy_exp = dummy_exp typdefs in

  let local = fold_decls redecl local in
  let defs = fold_decls redef defs in
  let decls = add_local_vars dummy_exp defs local in

  add_uc_list decls uc_list

(* --- *)

let mk_node_desc
    ((((typdefs, _, _, _) as decls), defs, trans,
      { nf_assertion; nf_initial; nf_invariant; nf_reachable; nf_attractive })
        : node_descr) =

  let typdefs = mk_typdefs typdefs in                (* XXX: default initial? *)
  let initial   = match nf_initial   with None -> mk_bcst' true | Some s -> s in
  let assertion = match nf_assertion with None -> mk_bcst' true | Some s -> s in
  { cn_typs = typdefs;
    cn_decls = mk_node_decls typdefs decls defs trans;
    cn_init = initial;
    cn_assertion = assertion;
    cn_invariant = nf_invariant;
    cn_reachable = nf_reachable;
    cn_attractive = nf_attractive; }

(* --- *)

let mk_cfg_desc
    ((((typdefs, _, _, _) as decls), defs, graph, trans,
      { nf_assertion; nf_initial; nf_invariant; nf_reachable; nf_attractive })
        : cfg_descr) =

  assert (trans == []);
  let typdefs = mk_typdefs typdefs in                (* XXX: default initial? *)
  let decls = mk_cfg_decls typdefs decls defs trans in
  let graph = mk_graph typdefs decls graph in
  let initial   = match nf_initial   with None -> mk_bcst' true | Some s -> s in
  let assertion = match nf_assertion with None -> mk_bcst' true | Some s -> s in
  { cg_typs = typdefs;
    cg_decls = decls;
    cg_graph = graph;
    cg_init = initial;
    cg_assertion = assertion;
    cg_invariant = nf_invariant;
    cg_reachable = nf_reachable;
    cg_attractive = nf_attractive; }

(* --- *)

let mk_func_desc
    ((((typdefs, _, _) as decls), defs, { ff_assertion }) : func_descr) =

  let typdefs = mk_typdefs typdefs in
  let assertion = match ff_assertion with None -> mk_bcst' true | Some s -> s in
  { fn_typs = typdefs;
    fn_decls = mk_func_decls typdefs decls defs;
    fn_assertion = assertion; }

(* --- *)

let mk_pred_desc ((((typdefs, _, _) as decls), defs, value) : pred_descr) =

  let typdefs = mk_typdefs typdefs in
  { pn_typs = typdefs;
    pn_decls = mk_pred_decls typdefs decls defs;
    pn_value = value; }

(* -------------------------------------------------------------------------- *)
