(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)

(** framework base: data-flow and control-flow program types *)

(** {2 Data-flow program } *)

(** variable declarations *)
type declaration_t =
    {
      typdef: (Env.var_t, Env.typdef) PMappe.t;
      state: (Env.var_t, Env.typ) Mappe.t;
      local: (Env.var_t, Env.typ) Mappe.t;
      input: (Env.var_t, Env.typ) Mappe.t;
    }

(** dataflow program (with additional infos):
      the representation produced by the frontend *)
type dfprog_t =
{
  d_disc_equs : Env.equs_t;
  d_cont_equs : Env.equs_t;
  d_zero_defs : Env.zerodefs_t;
  d_init : Env.boolexpr_t;
  d_final : Env.boolexpr_t;
  d_ass : Env.boolexpr_t;
  d_reach : Env.boolexpr_t;
  d_attract : Env.boolexpr_t;
}

(** creates a DF program *)
val make_dfprog : Env.equs_t -> Env.equs_t -> Env.zerodefs_t -> Env.boolexpr_t -> Env.boolexpr_t -> Env.boolexpr_t -> Env.boolexpr_t -> Env.boolexpr_t -> dfprog_t

(** creates the empty DF program *)
val make_empty_dfprog : Env.t -> dfprog_t

(** applies the given BDD variable permutation to
     the given DF program (after an environment change) *)
val apply_env_permutation_to_dfprog : int array -> dfprog_t -> dfprog_t
val apply_env_permutation_to_dfprog': int array option -> dfprog_t -> dfprog_t

(** {2 Control-flow program } *)

(** CFG program - the representation for analysis *)
type cfprog_t =
{
  c_cfg : Cfg.t;
  c_disc_equs : Env.equs_t;
  c_cont_equs : Env.equs_t;
  c_init : Env.boolexpr_t;
  c_final : Env.boolexpr_t;
  c_ass : Env.boolexpr_t;
  c_reach: Env.boolexpr_t;
  c_attract: Env.boolexpr_t;
}

(* creates a CF program *)
val make_cfprog: Cfg.t -> Env.equs_t -> Env.equs_t -> Env.boolexpr_t -> Env.boolexpr_t -> Env.boolexpr_t -> Env.boolexpr_t -> Env.boolexpr_t -> cfprog_t

(** duplicates a CFG program structure *)
val copy_cfprog : cfprog_t -> cfprog_t

(** applies the given BDD variable permutation to
     the given CF program (after an environment change) *)
val apply_env_permutation_to_cfprog : int array -> cfprog_t -> cfprog_t
val apply_env_permutation_to_cfprog': int array option -> cfprog_t -> cfprog_t

(** {2 Front-End Interface } *)

(** produces a data-flow program (callback returned by parser_t) *)
type translate_t = Env.t -> dfprog_t

(** parses the given input file and returns
      the variable declarations, a data-flow program translator, and
      the default DF2CF and verification/synthesis strategies *)
type parser_t = string -> (declaration_t * translate_t * string * string)
type parser4synth_t = parser_t                  (* the same signature for now *)

val cfprog_as_dfprog: cfprog_t -> dfprog_t
val cfprog_is_a_self_loop: cfprog_t -> bool

(* -------------------------------------------------------------------------- *)

(* val log_cfg_info *)
(*   : logger:Log.logger *)
(*   -> ?full_level:Log.level *)
(*   -> Env.t *)
(*   -> cfprog_t *)
(*   -> unit *)

val pp_cfg: Env.t -> Format.formatter -> cfprog_t -> unit
val pp_cfg_short: Env.t -> Format.formatter -> cfprog_t -> unit
