# -*- makefile-gmake -*-
# ----------------------------------------------------------------------
#
# Makefile for the ReaTK project, using the `opam-pkgdev' utilities.
#
# Copyright (C) 2015 Nicolas Berthier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------

QUIET =
JOBS ?= 8

PKGNAME = reatk
AVAILABLE_LIBs = rutils realib ctrlNbac cn2rl rl2cn
AVAILABLE_LIB_ITFs = $(AVAILABLE_LIBs)
INSTALL_LIBS = yes
INSTALL_DOCS = yes
ENABLE_BYTE = yes
ENABLE_NATIVE = no
ENABLE_DEBUG = no
ENABLE_PROFILING = no

TEST_EXECS_AVAILABLE =							\
	test-rl2cn-bounded-integers					\
	test-rl2cn-n-cn2rl-bounded-integers

# e.g., for calling with TEST_FLTR=-bdd-
__FILTER = $(foreach v,$(2),$(if $(findstring $(1),$(v)),$(v),))
TEST_FLTR ?= -
TEST_EXECS = $(call __FILTER,$(TEST_FLTR),$(TEST_EXECS_AVAILABLE))

-include config.mk

HAS_REALIB = $(and $(findstring realib,$(AVAILABLE_LIBs)),yes)
HAS_CN2RL  = $(and $(findstring  cn2rl,$(AVAILABLE_LIBs)),yes)
HAS_RL2CN  = $(and $(findstring  rl2cn,$(AVAILABLE_LIBs)),yes)
ifeq ($(and $(HAS_REALIB), $(HAS_CN2RL), $(HAS_RL2CN)),yes)
  EXECS = reaver reax
endif

# ---

MENHIRFLAGS = --fixed-exception
OCAMLBUILDFLAGS += $(if $(JOBS),-j $(JOBS),)
OCAMLBUILDFLAGS += -use-menhir -menhir "menhir $(MENHIRFLAGS)"
OCAMLDOCFLAGS = -charset iso-10646-1
NO_PREFIX_ERROR_MSG = Missing prefix: execute configure script first

# ---

OPAM_PKGDEV_DIR ?= opam-pkgdev

OPAM_DIR = opam
OPAM_FILES = descr opam
DIST_FILES = configure LICENSE Makefile myocamlbuild.ml README src	\
    _tags version.ml.in etc META.in

# ---

# Mainstream branch
MAIN_BRANCH = reatk

# ----------------------------------------------------------------------

-include generic.mk

GENERIC_MK = $(OPAM_PKGDEV_DIR)/generic.mk
generic.mk:
	@if test -f $(GENERIC_MK); then ln -s $(GENERIC_MK) $@;		\
	 elif test \! -f generic.mk; then echo				\
"To build from this development tree, you first need to retrieve the"	\
"$(OPAM_PKGDEV_DIR) submodule using \`git submodule update --init'."	\
	 >/dev/stderr; exit 1; fi;

# ----------------------------------------------------------------------

.PHONY: distclean
distclean: force clean clean-version
	$(QUIET)rm -f config.mk

# ----------------------------------------------------------------------
