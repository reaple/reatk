(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)

(** logging *)

type level = Error | Warn | Info | Debug | Debug2 | Debug3

(** logger: declare one logger per module *)
type logger = {
  module_name: string;
  level: level;
}

(** Default formatter used by the functions of this module *)
val fmt: Format.formatter
val mk: ?level:level -> string -> logger

(** {2 Global configuration} *)

(** everything with less or equal verbosity will be logged,
if enabled in individual loggers *)
val globallevel : level ref

(** the log level of all modules with less verbosity will be weakened
to this value (i.e. made more verbose) *)
val globallevel_weaken : level ref

(** {2 Logging functions} *)

val debug2 : logger -> string -> unit
val debug2_o : logger -> (Format.formatter -> 'a -> unit) -> string -> 'a -> unit
val debug3 : logger -> string -> unit
val debug3_o : logger -> (Format.formatter -> 'a -> unit) -> string -> 'a -> unit
val debug : logger -> string -> unit
val debug_o : logger -> (Format.formatter -> 'a -> unit) -> string -> 'a -> unit
val info : logger -> string -> unit
val info_o : logger -> (Format.formatter -> 'a -> unit) -> string -> 'a -> unit
val warn : logger -> string -> unit
val warn_o : logger -> (Format.formatter -> 'a -> unit) -> string -> 'a -> unit
val error : logger -> string -> unit
val error_o : logger -> (Format.formatter -> 'a -> unit) -> string -> 'a -> unit

(** {3 Some Aliases} *)

val i': logger -> string -> unit
val w': logger -> string -> unit
val e': logger -> string -> unit
val d': logger -> string -> unit
val d2': logger -> string -> unit
val d3': logger -> string -> unit

(** {3 Format-based polymorphic logging functions} *)

type 'a fmtlog =
    logger -> ('a, Format.formatter, unit, unit, unit, unit) format6 -> 'a
(** Format-based polymorphic logging functions take a logger, a format string,
    plus any number of parameters corresponding to the given format.

    Note that of course, all arguments to functions of this type are always
    evaluated. *)

val log: level -> 'a fmtlog
val i: 'a fmtlog
val w: 'a fmtlog
val e: 'a fmtlog
val d: 'a fmtlog
val d2: 'a fmtlog
val d3: 'a fmtlog

(** {4 "Continuations"} *)

type 'a contlog =
    logger -> ('a, Format.formatter, unit) format -> Format.formatter -> 'a
(** Helpers to extend logging messages depending on the log level. *)

val ci: 'a contlog
val cd: 'a contlog
val cd2: 'a contlog
val cd3: 'a contlog

(** {4 Transient info messages} *)

val tf: ?level:level -> string -> unit
val tf': ?level:level -> string Lazy.t -> unit

val ti: ?p:level -> 'a fmtlog
val td: ?p:level -> 'a fmtlog
val td2: ?p:level -> 'a fmtlog
val td3: 'a fmtlog

val ti_timed: ?period: float -> ?p: level -> 'a fmtlog

(** {4 Transient rotating mark} *)

val roll: ?first:float -> ?period:float -> ?level:level -> ('a -> 'b) -> 'a -> 'b

(** {2 Helpers} *)

(** parses a log level (upper case) from a string *)
val string2level : string -> level

(** list of short log level descriptors to be given to {!string2level}. *)
val short_levels : string list

(** returns true if the given level is activated in the given logger *)
val check_level : logger -> level -> bool

module type T = sig
  val logger: logger
end
