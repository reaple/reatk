open Format
open Graph
open CtrlNbac
open AST

let level = Log.Debug3
let logger = Log.mk ~level "TopoBFS"

(* -------------------------------------------------------------------------- *)

module Make (PCG: module type of PCG.Make (Weight.F)) = struct

  module WBFS = PathsIteration.WeightedBFS (PCG) (PCG.W)
  open PCG

  let find_good_order pcg =

    let path_cost vertexp g order =
      let cost e =
        let u = E.src e in
        if vertexp u
        then let u = V.label u and v = V.label (E.dst e) in
             let d = abs (SMap.find u order - SMap.find v order) in
             E.label e ** log1p (float_of_int d)
        else 0.0
      in
      fold_edges_e (fun e c -> c +. cost e) g 0.0
    in

    let mk_order_map path =
      List.fold_left (fun (i, order) v -> i+1, SMap.add (V.label v) i order)
        (0, SMap.empty) path |> snd
    in

    let order =
      let nc, component_of = Components.components pcg in
      let rec next_comp acc c =
        if c = nc then acc else
          let vertex_filter v = component_of v = c in
          let best_cost, best_order = WBFS.fold_all_paths
            vertex_filter
            begin fun path (prev_cost, prev_order) ->
              let cost = path_cost vertex_filter pcg (mk_order_map path) in
              (* eprintf "cost: %f@." cost; *)
              if cost < prev_cost then cost, path else prev_cost, prev_order
            end pcg (max_float, [])
          in
          (* eprintf "best path(%i): [@[%a@]]@." c print_path best_order; *)
          (* eprintf "best cost(%i): %f@." c best_cost; *)
          next_comp (List.rev_append best_order acc) (c + 1)
      in
      next_comp [] 0
    in

    List.rev_map V.label order

end

(* -------------------------------------------------------------------------- *)
