(******************************************************************************)
(* environment data structure and utilities *)
(* This file is part of ReaTK released under the GNU GPL.  Please read the
   LICENSE file packaged in the distribution *)
(******************************************************************************)

let level = Log.Debug
let logger = Log.mk ~level "Env"

module Symb = struct
  type t = string
  let mk x = x
  let compare = String.compare
  let print = Format.pp_print_string
  let to_string x = x
end

type var_t = Symb.t
type symbol_t = var_t Bdd.Symb.t

type cuddman = Cudd.Man.v Cudd.Man.t
type apronenv = Apron.Environment.t

type typ = var_t Bddapron.Typ.t
type typdef = var_t Bddapron.Typ.findef

type expr_t = var_t BddapronUtil.expr_t
type boolexpr_t = var_t BddapronUtil.boolexpr_t
type numexpr_t = var_t BddapronUtil.numexpr_t
type vars_t = var_t BddapronUtil.vars_t
type vset = var_t BddapronUtil.vset
type equ_t = var_t BddapronUtil.equ_t
type equs_t = var_t BddapronUtil.equs_t

type zeroexpr_t = numexpr_t
type zerodef_t = var_t * numexpr_t
type zerodefs_t = zerodef_t list


type t =
    {
      env : var_t Bddapron.Env.t;
      cond : var_t Bddapron.Cond.t;
      s_vars : vars_t;
      i_vars : vars_t;
      b_vars : vars_t; n_vars : vars_t;
      bi_vars : vars_t; ni_vars : vars_t;
      bs_vars : vars_t; ns_vars : vars_t;

      mutable zero_vars : vars_t;
      disc_q_vars : vars_t;
      cont_q_vars : vars_t;
      q_i_vars : vars_t;
      bool_size: int;
      cond_size: int;
      zeros_size: int;
    }

let symbol = Bdd.Symb.string

let zero_prefix = "__z"
let epsilon = 0.015625
let init_var () = Symb.mk "init"

let apronenv { env } = Bddapron.Env.apron env
let cuddman { env } = env.Bdd.Env.cudd

(******************************************************************************)
(* CUDD variable index layout *)
(******************************************************************************)
(*

[0...bool_size]: Boolean and enumerated variables
- 1 for each Bool
- m_t bits for each Benum or Bint of type t
- 3 for each bit of a state variable (var,primed_var,new_input)
- 1 for each bit of an input variable

[bool_size+1...bool_size+1+cond_size]: numerical constraints
- 2 per term (c, -c)
*)

let print_vars env fmt vars =
  Util.list_print Symb.print fmt vars

let print_boolexpr env fmt bexpr =
  BddapronUtil.print_boolexpr env.env env.cond fmt bexpr

let print_equations env fmt equs =
  BddapronUtil.print_equations env.env env.cond fmt equs


(******************************************************************************)
(* CUDD utilities etc. *)
(******************************************************************************)

(* computes the careset (admissible combinations of numerical constraints) *)
let compute_careset env =
  Log.d2 logger "Computing careset…";
  Bdd.Cond.update_careset env.cond

let update_careset ?(numvars_threshold = 5) env =
  if List.length env.n_vars <= numvars_threshold then begin
    Log.d2 logger "Updating careset…";
    Bdd.Cond.update_careset env.cond
  end

let typof env = Bddapron.Env.typ_of_var env.env

(* grouping variables (only used in ZeroTrans actually…) *)
let cudd_group { bool_size; cond_size; env } =
  Log.w logger "Skipping@ `cudd_group':@ \
                please@ use@ Bddapron's@ grouping@ helpers@ instead."
  (* TODO: Use Bddapron groupping capabilities! *)
  (* fix the order of all bits of a variable including primed and new_inputs *)
  (* Mappe.iter *)
  (*   (fun var typ -> *)
  (*     match typ with *)
  (*       | #Bdd.Typ.t -> *)
  (*         let tid = PMappe.find var env.Bdd.Env.vartid in *)
  (*         Cudd.Man.group env.Bdd.Env.cudd tid.(0) (3*(Array.length tid)) *)
  (*           Cudd.Man.MTR_FIXED *)
  (*       | _ -> ()) *)
  (*    s_vars; *)
  (* (\* make zero-variables reorderable *\) *)
  (* (\*Cudd.Man.group env.Bdd.Env.cudd 0 !zeros_size Cudd.Man.MTR_DEFAULT;*\) *)
  (* (\* make finite type variables (all their bits etc) reorderable *\) *)
  (* (\*Cudd.Man.group env.Bdd.Env.cudd !zeros_size (!bool_size - !zeros_size) *\) *)
  (* Cudd.Man.group env.Bdd.Env.cudd 0 bool_size *)
  (*   Cudd.Man.MTR_DEFAULT; *)
  (* (\* make numerical constraints reorderable *\) *)
  (* Cudd.Man.group env.Bdd.Env.cudd bool_size cond_size *)
  (*   Cudd.Man.MTR_DEFAULT; *)
  (* (\* keep numerical constraints below the finite type varibles *\) *)
  (* Cudd.Man.group env.Bdd.Env.cudd 0 (bool_size + cond_size + 1) *)
  (*   Cudd.Man.MTR_FIXED *)

let cudd_make_varmap' env sp_map =
  let to_prime = PMappe.bindings sp_map in
  let perm = Bdd.Expr0.O.permutation_of_rename env to_prime in
  Cudd.Man.set_varmap (Bdd.Env.cuddman env) perm

(* setting up varmap for exchanging unprimed and primed variable *)
let cudd_make_varmap { env; bs_vars } =
  List.fold_left (fun m v ->
    let v' = BddapronUtil.get_primed_var env v in
    if Bdd.Env.mem_var env v' then PMappe.add v v' m else m)
    (PMappe.empty symbol.Bdd.Symb.compare) bs_vars
  |> cudd_make_varmap' env

let clear { env; cond } = CuddUtil.clear env cond

(******************************************************************************)
(* utilities *)
(******************************************************************************)

(* returns a q variable for the given zero-crossing id *)
let get_q_var zvar is_continuous =
  "_q"^(Symb.to_string zvar)^(if is_continuous then "c" else "d") |> Symb.mk

(* returns a the ith q bool input variable for the given zero-crossing id *)
let get_q_bool_i_var zvar i =
  (get_q_var zvar true |> Symb.to_string)^"_i"^(string_of_int i) |> Symb.mk

(* returns a new input variable with the same type as v *)
let get_newinput env v =
  let new_v = Symb.to_string v^"''" in
  Log.debug_o logger Format.pp_print_string "new input: " new_v;
  Symb.mk new_v

(* returns the list of new inputs *)
(* let create_newinputs s_vars = *)
(*   List.map (fun (v,t) -> (v^"''",t)) s_vars *)

let number_of_boolvars_varstyp env =
  List.fold_left (fun acc (_, t) -> acc + BddapronUtil.typ_finsize env t) 0

let number_of_boolvars ({ env }: t) = BddapronUtil.vars_finsize env

let number_of_num_vars =
  List.fold_left (fun acc (_, t) -> acc + match t with |`Int |`Real -> 1 |_ -> 0) 0

(* (\* computes the size of BDD layout sections *\) *)
(* let compute_sizes cuddman typedefs (\* ~enable_primed_state_vars *\) s_vars i_vars = *)
(*   let tempenv = Bddapron.Env.make cuddman ~symbol *)
(*     ~relational:false ~bddindex0:0 ~bddsize:1 *)
(*   in *)
(*   PMappe.iter (Bddapron.Env.add_typ_with tempenv) typedefs; *)
(*   Log.d3 logger "@[<v>typedefs:%a@]" *)
(*     (fun fmt td -> PMappe.iter begin fun a b -> *)
(*       Format.fprintf fmt "@\n%a = @[%a@]" Symb.print a *)
(*         (Bdd.Env.print_typdef (fun fmt -> Format.fprintf fmt "@,%a" Symb.print)) b *)
(*     end td) typedefs; *)
(*   let bool_state = number_of_boolvars_varstyp tempenv s_vars in *)
(*   (\* let bool_state = bool_state * if enable_primed_state_vars then 2 else 1 in *\) *)
(*   let bool_input = number_of_boolvars_varstyp tempenv i_vars in *)
(*   let num_vars = number_of_num_vars s_vars + number_of_num_vars i_vars in *)
(*   (bool_state, bool_input, num_vars) *)

(* returns the variable corresponding to the BDD index i *)
let var_of_cuddid env i =
  let s = PMappe.filter
    (fun v iarr ->
      Array.fold_right (fun ii accu -> accu || (ii=i)) iarr false)
    env.env.Bdd.Env.vartid
  in
  let (v,_) = PMappe.choose s in
  v

let fold_typdefs env f i = PMappe.fold f env.env.Bdd.Env.typdef i

(* -------------------------------------------------------------------------- *)

type varspec =
  | Var of var_t * typ * [ `State | `Input ] * bool
  | Fixed of varspecs
and varspecs = varspec list

let gather_vars cuddman typedefs varstyp =
  Log.d3 logger "@[<v>typedefs:%a@]"
    (fun fmt td -> PMappe.iter begin fun a b ->
      Format.fprintf fmt "@\n%a = @[%a@]" Symb.print a
        (Bdd.Env.print_typdef (fun fmt -> Format.fprintf fmt "@,%a" Symb.print)) b
    end td) typedefs;
  let btsize = function
    | `Bool -> 1
    | `Bint (_, s) -> s
    | `Benum td -> (match PMappe.find td typedefs with
        | `Benum labels -> Bdd.Labels.cardinal labels - 1 |> Bdd.Reg.min_size)
    | _ -> 0
  in
  let rec gather_vars (s, bss, i, bis, ns, p, a, gt) = function
    | Var (v, t, k, prime) ->
        let bsize = btsize t * if prime then 2 else 1 in
        let a = (v, t) :: a in
        let is_num = match t with `Int | `Real -> true | _ -> false in
        let p, a, gt =
          if prime
          then let v' = BddapronUtil.get_primed_var' symbol v in
               (v', t) :: p, (v', t) :: a,
               if is_num then gt else `InterleavedVars [v; v'] :: gt
          else p, a, if is_num then gt else `Var v :: gt
        in
        let ns = ns + if is_num then 1 else 0 in
        (match k with
          | `State -> ((v, t) :: s, bss + bsize, i, bis, ns, p, a, gt)
          | `Input -> (s, bss, (v, t) :: i, bis + bsize, ns, p, a, gt))
    | Fixed vars ->
        let s, bss, i, bis, ns, p, a, gt' =
          List.fold_left gather_vars (s, bss, i, bis, ns, p, a, []) vars
        in
        s, bss, i, bis, ns, p, a, `Fixed (List.rev gt') :: gt
  in
  let s_varstyp, bs_size, i_varstyp, bi_size, n_size, p_varstyp, all_vars, gt =
    List.fold_left gather_vars ([], 0, [], 0, 0, [], [], []) varstyp
  in
  List.rev s_varstyp, bs_size,
  List.rev i_varstyp, bi_size, n_size,
  List.rev p_varstyp,
  List.rev all_vars,
  List.rev gt

(******************************************************************************)
(* creates the environment *)
(******************************************************************************)
let make'
    ?max_mem
    ?(booking_factor=1)
    ?(cond_factor=1)
    ?max_conds
    ?(enable_zeros=true)
    typedefs varstyp =

  let cuddman = CuddUtil.make_man ~cache_factor:4 ?max_mem () in
  let s_varstyp, bool_state_size, i_varstyp, bool_input_size, num_size,
    p_varstyp, all_vars, gt =
    gather_vars cuddman typedefs varstyp in

  let zeros_size = if enable_zeros then num_size*2*cond_factor else 0 in
  let bool_size = bool_state_size + bool_input_size + 10 * zeros_size + 1 in
  let bool_size = bool_size * booking_factor in

  (* a factor 2 is minimum, our benchmarks need 3 without retry *)
  let cond_size = 30 * cond_factor * num_size in

  Log.d3 logger "bool_size=%d" bool_size;
  Log.d3 logger "cond_size=%d (factor = %d * 30)" cond_size cond_factor;
  Log.d3 logger "zeros_size=%d" zeros_size;

  (* create environment *)
  let env = Bddapron.Env.make cuddman ~symbol
    ~relational:false ~bddindex0:0 ~bddsize:bool_size in
  let cond = Bddapron.Cond.make cuddman ~symbol:symbol
    ~bddindex0:(succ bool_size) ~bddsize:cond_size ?bddmax:max_conds
    ~upper_hooks:env.Bdd.Env.hooks
  in

  Bdd.Env.register_extension_hook env.Bdd.Env.hooks
    (Bdd.Env.Hook.make "Env.on_extension_log" (fun offset ->
      Log.i logger "@[<h>Shifting@ conditions@ environment@ by@ offset@ \
                      %d@]" offset));

  (* add types *)
  PMappe.iter (Bddapron.Env.add_typ_with env) typedefs;

  (* add all vars *)
  let add_vars l = Bddapron.Env.add_vars_with env ~booking_factor l |> ignore in
  add_vars all_vars;

  (* --- *)

  let boolnum_part = List.fold_left
    (fun (b, n) -> function
      | v, (`Int | `Real) -> b, v :: n | v, _ -> v :: b, n)
    ([],[])
  in

  (* XXX NB: I comment this for now to avoid congesting the environment; this
     may breack some things in preprocessing and verification algorithms
     (what?), but we do not need them for synthesis... *)
  (* let new_i_vars = [] in *)
  (* let new_i_varstyp = create_newinputs s_varstyp in *)
  (* let (new_i_vars,_) = List.split new_i_varstyp in *)
  (* let _ = Bddapron.Env.add_vars_with env new_i_varstyp in *)

  (* lists for different kinds of variables *)
  (* let s_vars = List.split s_varstyp |> fst in *)
  let bp_vars, np_vars = boolnum_part p_varstyp in
  let bi_vars, ni_vars = boolnum_part i_varstyp in
  let bs_vars, ns_vars = boolnum_part s_varstyp in
  (* let (b_vars,n_vars) = env_to_boolnumvars env in *)
  let i_vars = List.rev_append bi_vars ni_vars in
  let s_vars = List.rev_append bs_vars ns_vars in
  let b_vars = List.rev_append bi_vars bp_vars |> List.rev_append bs_vars in
  let n_vars = List.rev_append ni_vars np_vars |> List.rev_append ns_vars in
  (* let bs_vars = Util.list_inter b_vars s_vars in *)
  (* let ns_vars = Util.list_inter n_vars s_vars in *)

  (* add zerodef variables *)
  let rec make_zero_varstyp acc l =
    let zerovar_name l = zero_prefix ^ string_of_int l in
    if l < 0 then acc
    else make_zero_varstyp ((Symb.mk (zerovar_name l), `Bool) :: acc) (pred l)
  in
  let _zero_varstyp = make_zero_varstyp [] (pred zeros_size) |> add_vars in

  Bdd.Env.group_vars_with env gt |> ignore;

  (* set variable mapping for exchanging primed and unprimed variables *)
  (* XXX: keep this here for now, for internal operations within Cfg, triggered
     from PartitionUtil. *)
  if p_varstyp <> [] then begin
    let sp_map =
      let rec gather_spvars acc = function
        | Var (v, #Bdd.Typ.t, `State, true) ->
            PMappe.add v (BddapronUtil.get_primed_var env v) acc
        | Var _ -> acc
        | Fixed vars ->
            List.fold_left gather_spvars acc vars
      in
      List.fold_left gather_spvars
        (PMappe.empty symbol.Bdd.Symb.compare) varstyp
    in
    cudd_make_varmap' env sp_map;
  end;

  (* --- *)

  (* (\* create environment for APRON *\) *)
  (* let is_int v = Bddapron.Env.typ_of_var env v = `Int in *)
  (* let (iintvars,irealvars) = List.partition (is_int) ni_vars in *)
  (* let (iintvars,irealvars) = (BddapronUtil.vars_to_apronvars env iintvars, *)
  (*                             BddapronUtil.vars_to_apronvars env irealvars) in *)
  (* let (sintvars,srealvars) = List.partition (is_int) ns_vars in *)
  (* let (pintvars,prealvars) = *)
  (*   if enable_primed_state_vars then *)
  (*     (BddapronUtil.vars_to_apronvars env *)
  (*        (List.map (BddapronUtil.get_primed_var env) sintvars), *)
  (*      BddapronUtil.vars_to_apronvars env *)
  (*        (List.map (BddapronUtil.get_primed_var env) srealvars)) *)
  (*   else [| |] , [| |] in *)
  (* let (sintvars,srealvars) = (BddapronUtil.vars_to_apronvars env sintvars, *)
  (*                             BddapronUtil.vars_to_apronvars env srealvars) in *)

  (* let i_apronenv = Apron.Environment.make iintvars irealvars in *)
  (* let s_apronenv = Apron.Environment.make sintvars srealvars in *)
  (* let apronenv = Apron.Environment.make *)
  (*   (Array.concat [sintvars;pintvars;iintvars]) *)
  (*   (Array.concat [srealvars;prealvars;irealvars]) in *)
  (* Log.debug2_o logger (Apron.Environment.print) "apronenv: " apronenv; *)
  (* Log.debug2_o logger (Apron.Environment.print) "apronenv': " (Bddapron.Env.apron env); *)
  (* Log.debug2_o logger (Apron.Environment.print) "s_apronenv: " s_apronenv; *)
  (* Log.debug2_o logger (Apron.Environment.print) "i_apronenv: " i_apronenv; *)

  Log.d3 logger "@[CUDD parameters:@;%a@]" CuddUtil.print_params env;
  CuddUtil.start_dynamic_reordering env;
  {
    env; cond;
    s_vars; i_vars; b_vars; n_vars;
    bi_vars; ni_vars;
    bs_vars; ns_vars;
    (* new_i_vars; *)
    (* primed_vars; *)
    zero_vars = []; disc_q_vars = []; cont_q_vars = []; q_i_vars = [];
    bool_size; cond_size; zeros_size;
  }

let make
    ?max_mem
    ?booking_factor
    ?cond_factor
    ?max_conds
    ?(enable_primed_state_vars=true)
    ?(enable_primed_input_vars=false)
    ?enable_zeros
    typedefs s_varstyp i_varstyp =
  let s = Mappe.bindings s_varstyp |>
      List.map (fun (v, t) -> Var (v, t, `State, enable_primed_state_vars)) in
  let i = Mappe.bindings i_varstyp |>
      List.map (fun (v, t) -> Var (v, t, `Input, enable_primed_input_vars)) in
  make'
    ?max_mem
    ?booking_factor
    ?cond_factor
    ?max_conds
    ?enable_zeros
    typedefs (s @ i)

(** splits the equations into a list of variables and a list of expressions *)
let split_equs equs = List.split equs

(** returns the boolean equations *)
let get_bool_equs { env } =
  List.filter (fun equ -> not (BddapronUtil.is_num_equ env equ))

(** returns the numerical equations *)
let get_num_equs { env } =
  List.filter (BddapronUtil.is_num_equ env)

(** Eliminates given variables from the environment.

    Note it does NOT register them from the underlying bddarpon environment! *)
let elim_vars ({ env = e; cond = c;
                 s_vars; i_vars; b_vars; n_vars;
                 bs_vars; ns_vars; bi_vars; ni_vars; } as env) vars =
  let module BEnv = (val BddapronUtil.bddapronenv_module e c) in
  let module EU = BddapronUtil.EnvUtils (BEnv) in
  let vars = EU.varset vars in
  let filter = List.filter (fun v -> not (PSette.mem v vars)) in
  { env with
    s_vars = filter s_vars;
    i_vars = filter i_vars;
    b_vars = filter b_vars;
    n_vars = filter n_vars;
    bs_vars = filter bs_vars;
    ns_vars = filter ns_vars;
    bi_vars = filter bi_vars;
    ni_vars = filter ni_vars; }

(** Change the specification of given variables from the environment. *)
let respec_vars ({ env = e; cond = c;
                   s_vars; i_vars; b_vars; n_vars;
                   bi_vars; ni_vars; bs_vars; ns_vars; } as env)
    s_vars =
  let module BEnv = (val BddapronUtil.bddapronenv_module e c) in
  let module EU = BddapronUtil.EnvUtils (BEnv) in
  let state_vars = EU.varset s_vars in
  let state v = PSette.mem v state_vars in
  let states = List.filter state in
  let inputs = List.filter (fun v -> not (state v)) in
  { env with
    s_vars = states (i_vars @ s_vars);
    i_vars = inputs (i_vars @ s_vars);
    bs_vars = states b_vars;
    ns_vars = states n_vars;
    bi_vars = inputs b_vars;
    ni_vars = inputs n_vars; }

(* Parsing of expressions based on an environment *)

exception InvalidBooleanExpressionSpecification of string
let str_to_boolexpr { env; cond } str =
  (** [str_to_boolexpr env str] parses the Boolean bddapron expression [str];
      Raises {!InvalidBooleanExpressionSpecification} if [str] is not a Boolean
      expression. *)
  match Bddapron.Parser.expr0_of_string env cond str with
    | `Bool e -> e
    | e -> raise (InvalidBooleanExpressionSpecification str)

(* parses a string list into a list of boolean BDD APRON expressions *)
let strlist_to_boolexprlist env strlist =
  List.rev
    (List.fold_left
       (fun acc str -> try str_to_boolexpr env str :: acc with
         | InvalidBooleanExpressionSpecification str ->
             Log.w logger "Ignored expression: %s" str;
             acc)
       [] strlist)

exception UnknownVariables of string list
let strlist_to_vars ({ env = e; cond = c; s_vars; i_vars; }) strs =
  let module BEnv = (val BddapronUtil.bddapronenv_module e c) in
  let module EU = BddapronUtil.EnvUtils (BEnv) in
  let all_vars = PSette.union (EU.varset s_vars) (EU.varset i_vars) in
  match
    List.fold_left begin fun (acc, unkns) s ->
        let v = Symb.mk s in
        if PSette.mem v all_vars then v :: acc, unkns else acc, s :: unkns
    end ([], []) strs
  with
    | vars, [] -> List.rev vars
    | _, unkns -> raise @@ UnknownVariables unkns

(******************************************************************************)
(* zero crossing definition table *)
(******************************************************************************)

let find_zero_var zeros zexpr = Util.assoclist_find_key zeros zexpr
let get_zero_def zeros zvar = List.assoc zvar zeros

let get_newzero_var ({ zeros_size } as env) =
  let l = List.length env.zero_vars in
  if l >= zeros_size then
    raise Bdd.Env.Bddindex;
  let v = zero_prefix^(string_of_int l) |> Symb.mk in
  env.zero_vars <- List.append env.zero_vars [v];
  v

let print_zero_defs env fmt zeros =
  Util.list_print
    (fun fmt (z,e) ->
      Format.fprintf fmt "%a = %a" Symb.print z
        (Bddapron.Expr0.Apron.print env.env env.cond) e)
    fmt zeros

(* --- *)

(******************************************************************************)
(* basic module type for functors *)
(******************************************************************************)

module type T = sig
  val env: t
  type var = var_t
  module BEnv: BddapronUtil.BDDAPRON_ENV with type var = var
  module BEnvNS: module type of BddapronUtil.BEnvNS (BEnv)
end

let as_module ({ env = e; cond = c } as env) =
  let module BEnv = (val BddapronUtil.bddapronenv_module e c) in
  (module struct
    let env = env
    type var = var_t
    module BEnv = BEnv
    module BEnvNS = BddapronUtil.BEnvNS (BEnv)
  end : T)
