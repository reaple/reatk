(** framework base: controllable program specification

    @author Nicolas Berthier *)

open Env

(* -------------------------------------------------------------------------- *)

(** Accepted default expressions. *)
type default_expr = var_t BddapronSynthesis.default_expr

(** Sequence of variables. *)
type var_seq = var_t list

(** Group of Non-controllable/Controllable variables.

    Ordering of controllable variables and default expressions is always
    preserved by operations of this module. *)
type uc_group =
    {
      u_vars: vars_t;          (** Non-controllable variables *)
      c_vars: var_seq;         (** Sequence of controllable variables *)
      bu_vars: vars_t;         (** Boolean non-controllable variables *)
      nu_vars: vars_t;         (** Numerical non-controllable variables *)
      bc_vars: var_seq;        (** Sequence of Boolean controllable variables *)
      nc_vars: var_seq;        (** Sequence of Numerical controllable variables
                                   (unused) *)
      bp_specs: default_expr list; (** Sequence of Boolean default expressions *)
      np_specs: numexpr_t list;    (** Sequence of numerical defaults
                                       (irrelevant...) *)
    }

(** The type of controllable program specifications.

    It comprizes a sequence of {i n} couples of non-controllable/controllable
    variables {i <U{_1}, C{_1}> ... <U{_n}, C{_n}>}, thereby encoding potential
    implicit dependencies: essentially, variables of set {i U{_ i+1}} can depend
    on the computation of those of {i C{_i}}, and non-controllable variables
    belonging to {i U{_i}} are assumed to be available before computing the
    controllables of {i C{_i}}. *)
type t =
    {
      (* TODO: associate environment? *)
      cs_groups: uc_group list; (** Successive groups, starting from the 1st. *)
    }

(** [make_uc_group bu nu bcp ncp] builds a group of
    non-controllable/controllable variables from the Boolean non-controllables
    [bu], the numerical non-controllables [nu], the Boolean controllables
    coupled with their default expressions [bcp], and the numerical
    controllables (with their - unused - default expressions) [ncp].

    All variables involved should belong to the same environment (this is not
    checked). *)
let make_uc_group bu_vars nu_vars bcp ncp =
  let u_vars = bu_vars @ nu_vars in
  let bc_vars, bp_specs = List.split bcp in
  let nc_vars, np_specs = List.split ncp in
  let c_vars = bc_vars @ nc_vars in
  { u_vars; c_vars; bu_vars; nu_vars; bc_vars; nc_vars;
    bp_specs; np_specs }

(** Builds a controllable program specification from a given list of groups,
    ordered from 1.

    It is assumed that all variables involved in all groups belong to the same
    environment, and that all sets and sequences are disjoint. *)
let make cs_groups = { cs_groups }
let uc_groups { cs_groups } = cs_groups

(* -------------------------------------------------------------------------- *)
(* Utilities *)

(**/**)
open List
let (|>) f g = g f
(**/**)

(** Returns a list of groups of non-controllable/controllable variables, in the
    form of couples {i <U, C>}, sorted from 1 to {i n}. *)
let uc_groups { cs_groups = gs } : (vars_t * var_seq) list =
  fold_left (fun g { u_vars; c_vars } -> (u_vars, c_vars) :: g) [] gs |> rev

(** Returns a list of groups of Boolean non-controllable/controllable variables,
    in the form of couples {i <U, C>}, sorted from 1 to {i n}. *)
let buc_groups { cs_groups = gs } : (vars_t * var_seq) list =
  fold_left (fun g { bu_vars; bc_vars } -> (bu_vars, bc_vars) :: g) [] gs |> rev

(** Returns a list of groups of numerical non-controllable/controllable
    variables, in the form of couples {i <U, C>}, sorted from 1 to {i n}. *)
let nuc_groups { cs_groups = gs } : (vars_t * var_seq) list =
  fold_left (fun g { nu_vars; nc_vars } -> (nu_vars, nc_vars) :: g) [] gs |> rev

(** Returns a list of sequences of default expressions associated to each lists
    of controllable variables, from 1 to {i n}. *)
let bp_specs { cs_groups = gs } =
  fold_left (fun acc { bp_specs } -> bp_specs :: acc) [] gs |> rev

(* Some predicates. *)

(** Tests whether the given controllable program specification involves
    numerical controllable variables. *)
let has_nc_vars { cs_groups = gs } = exists (fun { nc_vars } -> nc_vars <> []) gs

(* Traversals. *)

(** Traversal of U/C groups *)
let fold_uc_groups f i { cs_groups = gs } = fold_left f i gs
let foldi_uc_groups f i { cs_groups = gs } =
  fold_left (fun (acc, i) e -> (f i acc e, succ i))
    (i, 0) gs |> fst
let foldi_uc_groups_rev f i { cs_groups = gs } =
  fold_left (fun (acc, i) e -> (f i acc e, pred i))
    (i, (List.length gs - 1)) (List.rev gs) |> fst

(** Counts variables belonging to a controllable program specification.  Returns
    a tupple {i < Non-controllable Booleans, Non-controllable numericals,
    Controllable Booleans, Controllable numericals >}. *)
let nc_vars_info env = fold_uc_groups
  begin fun (bu, nu, bc, nc) { bu_vars; nu_vars; bc_vars; nc_vars } ->
    (bu + Env.number_of_boolvars env bu_vars, nu + List.length nu_vars,
     bc + Env.number_of_boolvars env bc_vars, nc + List.length nc_vars)
  end
  (0, 0, 0, 0)

let print_nc_vars_info env fmt cs =
  let bu, nu, bc, nc = nc_vars_info env cs in
  Format.fprintf fmt ",@ u=(%d/%d),@ c=(%d/%d)" bu nu bc nc

let u_vars cs = List.rev_map fst (uc_groups cs) |> List.flatten
let c_vars cs = List.rev_map snd (uc_groups cs) |> List.flatten
let i_vars cs = List.fold_left
  (fun acc (u, c) -> List.rev_append u (List.rev_append c acc))
  [] (uc_groups cs)

(* -------------------------------------------------------------------------- *)

let apply_env_permutation perm { cs_groups } =
  { cs_groups = List.map (fun uc_group ->
    { uc_group with bp_specs =
        List.map (BddapronSynthesis.permute_default_expr perm)
          uc_group.bp_specs; }) cs_groups; }

(* -------------------------------------------------------------------------- *)
