(******************************************************************************)
(* ReaX main *)
(* author: Nicolas Berthier *)
(* This file is part of ReaTK released under the GNU GPL.  Please read the
   LICENSE file packaged in the distribution *)
(******************************************************************************)

open Format
open Filename
open Realib.Supra
open Processing
open Rutils.IO
module Env = Realib.Env

let toolname = "ReaX"
let (&) f g = f g
let logger = Processing.logger
let globallevel = Log.Info            (* default log level *)
let globallevel_weaken = Log.Error    (* default log level weakening all default
                                         log levels*)

(* -------------------------------------------------------------------------- *)
(* ReaX options: *)

(** File extensions officially understood by ReaX, with associated input
    types. *)
let ityps_alist = [
  "ctrln", `Ctrln; "cn", `Ctrln;
  "ctrlg", `Ctrlg; "cg", `Ctrlg;
  "ctrlf", `Ctrlf; "cf", `Ctrlf;
  "ctrlr", `Ctrlr; "cr", `Ctrlr;
  "ctrld", `Ctrld; "cd", `Ctrld;
]

(** Name of official input types as understood by ReaX. *)
let ityps = List.map fst ityps_alist

(** File extensions effectively understood by ReaX. *)
let ityps_alist = ityps_alist @ [
  "ctrls", `Ctrlf; "ctrl-s", `Ctrlf; "cs", `Ctrlf;
  "ctrl-n", `Ctrln; "ctrl-g", `Ctrlg;
  "ctrl-f", `Ctrlf; "ctrl-r", `Ctrlr;
  "ctrl-d", `Ctrld;
]

let set_input_type r t =
  try r := Some (List.assoc t ityps_alist) with
    | Not_found -> raise (Arg.Bad (asprintf "Unknown input file type: `%s'" t))

let set_log_level r l =
  try r := Log.string2level l with
    | Exit -> Log.e logger "Unknown debug level: `%s'" l

let algo = ref ""
let optims = ref ""
let inputs = ref []
let output = ref ""
let dotout = ref ""
let dot_arcs = ref true
let cond_factor = ref 2
let max_conds = ref 0
let input_type = ref None
let inst_reord = ref false
let elim_vars = ref ""
let triang_ctrlr = ref false
let triang_reord = ref false
let triang_dyn_reord = ref false
let merge_ctrlf = ref false
let merge_discard_controllables = ref true
let merge_reord = ref false
let merge_dyn_reord = ref false
let split_ctrlf = ref false
let check_outputs = ref true
let discard_ctrlr = ref false
let make_backups = ref false

let svo = ref `TopoBFS
let svo_log = ref false
let svo_default = "topobfs"
let svo_names = [ "basic"; "topobfs" ]
let set_svo = function
  | "TopoBFS" | "topobfs" -> svo := `TopoBFS
  | "Basic" | "basic" -> svo := `Basic
  | s -> raise (Arg.Bad (asprintf "unknown static variable ordering: `%s'" s))

exception Help
let usage = "Usage: reax { [options] | <filename> } [ -- { <filename> } ]"
let print_vers () =
  fprintf err_formatter "reax-%s (Compiled on %s, %s)@." Version.str
    Version.compile_host Version.compile_time;
  exit 0
let anon x = inputs := x :: !inputs
let u = Arg.Unit (fun _ -> ())
let e s = ("", u, " "^s)
let t s = ("", u, " \ro __"^s^"__")
let h = Arg.Unit (fun _ -> raise Help)
let v = Arg.Unit print_vers
let log r = Arg.Symbol (Log.short_levels, set_log_level r)
let options = Arg.align
  [
    e""; t"General"; e"";

    ("-a", Arg.Set_string algo, "<algo> ");
    ("--algo", Arg.Set_string algo, "<algo> Synthesis algorithm specification");

    ("-O", Arg.Set_string optims, "<optim> ");
    ("--optim", Arg.Set_string optims, "<optim> Optimization specification");

    ("-i", Arg.String anon, "<file> ");
    ("--input", Arg.String anon, "<file> Input file");
    ("--input-type", Arg.Symbol (ityps, set_input_type input_type),
     " Input file type");
    ("-o", Arg.Set_string output, "<file> ");
    ("--output", Arg.Set_string output,
     "<file> Output file (`-' means standard output)");
    ("--dot", Arg.Set_string dotout,
     "<file> Output final CFG in DOT format (`-' means standard");
    e" output)";
    ("--dot-no-arcs", Arg.String (fun s -> dotout := s; dot_arcs := false),
     "<file> Output final CFG without arc labels in DOT format");
    e" (`-' means standard output)";
    ("-uo", Arg.Clear check_outputs, " ");
    ("--unsafe-outputs", Arg.Clear check_outputs,
     " Disable consistency checking of produced outputs");
    ("--", Arg.Rest anon, " Treat all remaining arguments as input files");

    e""; t"Handling of controllers (ctrln, ctrlg, ctrlr)"; e"";

    "--post-elim-vars", Arg.Set_string elim_vars, "<vars> \
      Eliminate (a comma-separated list of) output";
    e"variables from the systems before triangularizing";
    e"controllers";
    "--discard-ctrlr", Arg.Set discard_ctrlr, " \
      Disable extraction and output of controllers";
    "-t", Arg.Set triang_ctrlr, " ";
    "--triang", Arg.Set triang_ctrlr, " \
      Triangularize the controller to produce a function";
    "-t+r", Arg.Set triang_reord, " ";
    "-t-r", Arg.Clear triang_reord, " \
      Enable/disable variable reorderings during";
    "--triang-reorder", Arg.Set triang_reord, " \
      triangularization the controller (disabled by";
    "--triang-no-reorder", Arg.Clear triang_reord, " \
      default)";
    "-t+dr", Arg.Set triang_dyn_reord, " ";
    "-t-dr", Arg.Clear triang_dyn_reord, " \
      Enable/disable dynamic variable reorderings during";
    "--triang-dyn-reorder", Arg.Set triang_dyn_reord, " \
      triangularization the controller (disabled by";
    "--triang-no-dyn-reorder", Arg.Clear triang_dyn_reord, " \
      default)";
    "-m", Arg.Set merge_ctrlf, " \
      Merge triangularized controller with input node";
    "--merge", Arg.Set merge_ctrlf, " \
      (implies `-t')";
    "-m+r", Arg.Set merge_reord, " ";
    "-m-r", Arg.Clear merge_reord, " ";
    "--merge-reorder", Arg.Set merge_reord, " \
      Enable/disable variable reorderings during";
    "--merge-no-reorder", Arg.Clear merge_reord, " \
      merging (disabled by default)";
    "-m+dr", Arg.Set merge_dyn_reord, " ";
    "-m-dr", Arg.Clear merge_dyn_reord, " ";
    "--merge-dyn-reorder", Arg.Set merge_dyn_reord, " \
      Enable/disable dynamic variable reorderings during";
    "--merge-no-dyn-reorder", Arg.Clear merge_dyn_reord, " \
      merging (disabled by default)";
    "-mdc", Arg.Set merge_discard_controllables, " ";
    "-mkc", Arg.Clear merge_discard_controllables, " \
      Enable/disable discarding of controllable variables";
    "--merge-discard-ctrlbs", Arg.Set merge_discard_controllables, " \
      during merge (enabled by default); they are output";
    "--merge-keep-ctrlbs", Arg.Clear merge_discard_controllables, " \
      otherwise";

    e""; t"Handling of functions (ctrln, ctrlg, ctrlr, ctrlf)"; e"";

    ("-s", Arg.Set split_ctrlf, " \
       Split the function (implies `-t' when dealing with a");
    ("--split", Arg.Set split_ctrlf, " \
       node");

    e""; t"CUDD & Environment"; e"";

    ("--svo-algo", Arg.Symbol (svo_names, set_svo),
     (sprintf " Static variable ordering algorithm (default: %s)" svo_default));
    ("--svo-log", Arg.Set svo_log, " Make static variable ordering algorithm");
    e" log its internal structures (graphs...)";
    ("--cudd-print-limit", Arg.Set_int Realib.CuddUtil.print_limit,
     "<n> BDD size above which formulas are printed in");
    e(sprintf "summarized form (defaults to %d)" !Realib.CuddUtil.print_limit);
    ("--cudd-ir", Arg.Set inst_reord,
     " Enable CUDD variable reorderings during");
    e"instantiations (disabled by default except for the";
    e"basic variable ordering algorithm)";
    ("--cudd-nir", Arg.Clear inst_reord,
     " Disable CUDD variable reorderings during");
    e"instantiations (see `--cudd-ir')";
    ("--cudd-ndr", Arg.Clear Realib.CuddUtil.Options.enable_dynamic_reordering,
     " Disable CUDD dynamic reordering");
    ("--cudd-nr", Arg.Clear Realib.CuddUtil.Options.enable_reordering,
     " Disable any CUDD reordering");
    ("--env-cond-factor", Arg.Set_int cond_factor,
     "<n> Set the increase factor for determining the initial");
    e"number of constraint variables in BDDs";
    e(sprintf "(=30*<n>*#<numerical vars> --- defaults to %d)" !cond_factor);
    ("--env-cond-max", Arg.Set_int max_conds,
     "<n> Set the maximum number of constraint variables in");
    e"BDDs, and enable dynamic growth of the conditions";
    e"environment";

    e""; t"Miscellaneous"; e"";

    ("--debug", log Log.globallevel, " Set debug level");
    ("--debug-force", log Log.globallevel_weaken, " Force global debug level");
    ("-V", v, " ");
    ("--version", v, " Display version info");
    ("-h", h, " ");
    ("-help", h, " ");
    ("--help", h, " Display this list of options");
  ]

(* -------------------------------------------------------------------------- *)

let maybe_back ?(force = false) extra_ext = match force || !make_backups with
  | false -> Forget
  | true -> BackupCond (Some (default_backup_cond ~extra_ext))

let output_ctrlr x = output_ctrlr ~toolname ~check_enabled:!check_outputs x
let output_ctrls x = output_ctrls ~toolname ~check_enabled:!check_outputs x
let output_ctrlf x = output_ctrlf ~toolname ~check_enabled:!check_outputs x
let output_ctrld x = output_ctrld ~toolname ~check_enabled:!check_outputs x

let output_ctrlr x = output_ctrlr ~discard:!discard_ctrlr x

(* --- *)

let split_n_output_func ?backup mk_fmt func =
  Log.i logger "@[Splitting@ triangularized@ controller…@]";
  let funcs = CtrlNbac.AST.split_func func in

  if List.length funcs > 1 && not mk_fmt.out_mult then
    error "@[Unable@ to@ output@ multiple@ results@ in@ specified@ stream.@]";

  ignore (List.fold_left (fun i func ->
    output_ctrls ?backup ()
      { mk_fmt with
        out_exec = (fun ~backup ?descr ?suff (* ext *) ->
          let suff = match suff with
            | None ->  sprintf ".%d" i
            | Some suff -> sprintf "%s.%d" suff i
          in
          mk_fmt.out_exec ~backup ?descr ~suff (* (sprintf "%d.%s" i ext) *)) }
      func;
    succ i) 0 funcs)

let split_n_output_ctrlf ?backup env cs mk_fmt ctrlf =
  pass
    ~out_descr:"triangularized@ controller"
    ~translate:(Rl2cn.extract_ctrlf env cs)
    ~check:CtrlNbac.AST.check_func
    ~print:CtrlNbac.AST.print_func
    (split_n_output_func ?backup mk_fmt)
    ctrlf

let merge_n_output_ctrlf env cs mk_fmt cf ctrlf =
  let merge = merge
    ~enable_reorderings:(!merge_reord || !merge_dyn_reord)
    ~enable_dynamic_reorderings:!merge_dyn_reord
    ~discard_controllables:!merge_discard_controllables
  in
  let env, cs, ctrld = env_break env ctrlf (merge cs cf) in
  output_ctrld ~backup:(maybe_back "merge-bak") env cs mk_fmt ctrld

(* -------------------------------------------------------------------------- *)

let elim_vars env = match !elim_vars with
  | "" -> None
  | s ->
      Some (Env.strlist_to_vars env (Realib.ParseParams.comma_str_to_strlist s))

let do_var_elims_pp env cs ?elim_vars ?df ctrlr = match elim_vars with
  | None -> env, cs, df, ctrlr
  | Some vars ->
      let module E = (val Env.as_module env) in
      let module OE = Realib.TransBool.ElimOutputs (E) in
      let eop = Realib.TransBool.make_eo_param vars in
      Log.i logger "@[%t@]" (OE.descr eop);
      let df = match df with Some df -> Some (OE.df eop df) | None -> None in
      let ctrlr = OE.controller eop ctrlr in
      OE.new_env eop, cs, df, ctrlr

(* -------------------------------------------------------------------------- *)

let do_ctrlr_pp env cs ?elim_vars ?df ctrlr mk_fmt =
  let env, cs, df, ctrlr = do_var_elims_pp env cs ?elim_vars ?df ctrlr in

  let triang = triang
      ~enable_reorderings:(!triang_reord || !triang_dyn_reord)
      ~enable_dynamic_reorderings:!triang_dyn_reord
  in
  let ctrlf, _ = env_break env ctrlr (triang cs) in                 (* triang *)

  let merge_ctrlf = !merge_ctrlf && df <> None in
  (match df with
    | Some df when merge_ctrlf ->                                     (* merge *)
        merge_n_output_ctrlf env cs mk_fmt df ctrlf;
    | _ -> ());

  if !split_ctrlf && (not merge_ctrlf || mk_fmt.out_mult)                (* split *)
  then (split_n_output_ctrlf ~backup:(maybe_back "split-bak")
          env cs mk_fmt ctrlf);

  if (not merge_ctrlf || not !split_ctrlf) || mk_fmt.out_mult               (* none *)
  then output_ctrlf ~backup:(maybe_back "func-bak") env cs mk_fmt ctrlf

let do_ctrlr_pp_p ~allow_merge ~allow_split =
  !triang_ctrlr
  || (allow_merge && !merge_ctrlf)
  || (allow_split && !split_ctrlf)

(* -------------------------------------------------------------------------- *)

let do_synth aspec env (cs, assertion, cf) = match aspec with
  | Some a ->
      let (_, _, cf) as res = env_break env (cs, assertion, cf) (synth a) in
      output_dot_file ~dotfile:!dotout ~arcs:!dot_arcs env cf;
      res
  | None -> (cs, assertion, cf)

let do_optim ospec env (cs, ctrlr, cf) = match ospec with
  | Some s -> env_break env (cs, ctrlr, cf) (optim s)
  | None -> (cs, ctrlr, cf)

let do_synth_n_optim
    ?(ctrld = false)
    ?backup_ctrlr
    ?algo
    ?(optims = !optims)
    ?input_expr
    mk_fmt
    env cs system =

  log_env_info env ~cs;
  (match system with
    | `Df df -> log_dfprog_info env df
    | `Cf cf -> log_cfprog_info env cf);

  let elim_vars = elim_vars env in
  let input_expr = match input_expr with
    | None -> None
    | Some f -> Some (fun env str -> f env ?flag:None ?typ:None str)
  in
  let aspec = match algo with Some a -> Some (parse_synth env a) | _ -> None in
  let ospec = let parse_optim = parse_optim ?input_expr env cs in
              if optims <> "" then Some (parse_optim optims) else None in

  let allow_merge = true and allow_split = true in
  let do_ctrlr_pp_p = do_ctrlr_pp_p ~allow_merge ~allow_split in

  if aspec = None && ospec = None && not do_ctrlr_pp_p then
    Log.i logger "@[Nothing@ to@ do.@]"
  else
    let env, cf = match system with
      | `Df df -> env_break env df (df2cf "d")
      | `Cf cf -> env, cf
    in
    let a = cf.Realib.Program.c_ass in
    let cs, ctrlr, cf as system = do_synth aspec env (cs, a, cf) in  (* synth *)

    if ctrld then
      Log.i logger "@[Discarding@ resulting@ controller:@ input@ was@ a@ \
                      controlled@ node.@]"
    else
      let cs, ctrlr, cf as system = do_optim ospec env system in     (* optim *)

      if (not do_ctrlr_pp_p || mk_fmt.out_mult)
      then (output_ctrlr ~backup:(maybe_back ?force:backup_ctrlr "synth-bak")
              env cs mk_fmt ctrlr);

      if do_ctrlr_pp_p
      then do_ctrlr_pp env cs ?elim_vars ~df:(cf2df env cf) ctrlr mk_fmt

(* -------------------------------------------------------------------------- *)

let mk_log_fmt mk_fmt =
  if !svo_log then Some (mk_fmt.out_exec ~backup:(maybe_back "bak")) else None

let svo_arg mk_fmt f =
  let open Cn2rl in
  f ?static_variable_ordering:(match !svo with
    | `TopoBFS -> Some (SVOTopoBFS { svo_make_log_fmt = mk_log_fmt mk_fmt })
    | `Basic -> Some SVOBasic)

(* --- *)

let input_options () =
  {
    enable_primed_state_vars = false;
    enable_primed_input_vars = false;
    env_cond_factor = !cond_factor;
    env_booking_factor = 2;
    env_max_conds = if !max_conds = 0 then None else Some !max_conds;
    dynamic_variable_reordering =
      if !inst_reord then Some !Realib.CuddUtil.Options.reorder_policy else None;
  }

(* --- *)

let handle_ctrln ?ctrld ?filename mk_fmt =
  let algo = match !algo with "" -> "sB" | a -> a in
  let opts = input_options () in
  let (env, cs, df, _), input_expr
    = (svo_arg mk_fmt input_ctrln) ?filename opts in
  do_synth_n_optim ~input_expr ?ctrld ~algo mk_fmt env cs (`Df df)

(* --- *)

let handle_ctrln_ctrlr cn cr mk_fmt =
  let algo = match !algo with "" -> None | a -> Some a in
  let opts = input_options () in
  let (env, cs, df, _), input_expr
    = (svo_arg mk_fmt input_ctrln) ~filename:cn opts in
  let (_env, _cs, ctrlr, _), _ = try input_ctrlr ~filename:cr ~env opts with
    | Failure msg ->
        Log.e logger "@[Given@ predicate@ seems@ non-compatible@ with@ \
                        given@ node:@ %s@]" msg;
        exit 1
  in
  let df = { df with Realib.Program.d_ass = ctrlr } in
  do_synth_n_optim ~input_expr ?algo ~backup_ctrlr:true mk_fmt env cs (`Df df)

(* --- *)

let handle_ctrlg ?filename mk_fmt =
  let algo = match !algo with "" -> "sB" | a -> a in
  let opts = input_options () in
  let (env, cs, cf, _), input_expr
    = (svo_arg mk_fmt input_ctrlg) ?filename opts in
  do_synth_n_optim ~input_expr ~algo mk_fmt env cs (`Cf cf)

(* --- *)

let handle_ctrlg_ctrlr cg cr mk_fmt =
  let algo = match !algo with "" -> None | a -> Some a in
  let opts = input_options () in
  let (env, cs, cf, _), input_expr
    = (svo_arg mk_fmt input_ctrlg) ~filename:cg opts in
  let (_env, _cs, ctrlr, _), _ = try input_ctrlr ~filename:cr ~env opts with
    | Failure msg ->
        Log.e logger "@[Given@ predicate@ seems@ non-compatible@ with@ \
                        given@ CFG:@ %s@]" msg;
        exit 1
  in
  let cf = { cf with Realib.Program.c_ass = ctrlr } in
  do_synth_n_optim ~input_expr ?algo ~backup_ctrlr:true mk_fmt env cs (`Cf cf)

(* --- *)

let handle_ctrlf ?filename mk_fmt =
  let func_name, func = parse_input ?filename CtrlNbac.Parser.parse_func in

  if !triang_ctrlr
  then Log.w logger "@[Nothing@ to@ triangularize:@ ignoring@ option.@]";
  if !merge_ctrlf
  then Log.w logger "@[Nothing@ to@ merge:@ ignoring@ option.@]";

  if !split_ctrlf
  then split_n_output_func ~backup:(maybe_back "split-bak") mk_fmt func
  else Log.i logger "@[Nothing@ to@ do@ with@ input@ function.@]"

(* --- *)

let handle_ctrln_ctrlf cn cf mk_fmt =
  let opts = input_options () in
  let (env, cs, df, _), input_expr
    = (svo_arg mk_fmt input_ctrln) ~filename:cn opts in
  let elim_vars = elim_vars env in
  let (_env, ctrlf, _, _), _ = try input_ctrlf ~filename:cf ~env opts with
    | Failure msg ->
        Log.e logger "@[Given@ function@ seems@ non-compatible@ with@ \
                        given@ node:@ %s@]" msg;
        exit 1
  in
  match do_var_elims_pp env cs ?elim_vars ~df df.Realib.Program.d_ass with
    | env, cs, Some df, _ when !merge_ctrlf
        -> merge_n_output_ctrlf env cs mk_fmt df ctrlf
    | _ when !merge_ctrlf
        -> Log.w logger "@[Nothing@ to@ merge:@ ignoring@ option.@]"
    | _ -> Log.e logger "@[Unable@ to@ eliminate@ variables.@]"

(* --- *)

let handle_ctrlr ?filename mk_fmt =
  let (env, cs, ctrlr, _), _ = input_ctrlr ?filename (input_options ()) in
  log_env_info env ~cs;
  let elim_vars = elim_vars env in

  let allow_merge = false and allow_split = true in
  if do_ctrlr_pp_p ~allow_merge ~allow_split
  then do_ctrlr_pp env cs ?elim_vars ctrlr mk_fmt
  else Log.i logger "@[Nothing@ to@ do@ with@ input@ predicate.@]"

(* -------------------------------------------------------------------------- *)

let ityp_name_n_handle = function
  | `Ctrln | `Cn _ -> "node", handle_ctrln ~ctrld:false
  | `Ctrlg | `Cg _ -> "CFG", handle_ctrlg
  | `Ctrlf | `Cf _ -> "function", handle_ctrlf
  | `Ctrlr | `Cr _ -> "predicate", handle_ctrlr
  | `Ctrld | `Cd _ -> "controlled node", handle_ctrln ~ctrld:true

let ityp_name x = ityp_name_n_handle x |> fst

let guesstyp f =
  try match !input_type with
    | Some t -> t
    | None -> snd (List.find (fun (s, _) -> check_suffix f s) ityps_alist)
  with
    | Not_found ->
        raise (Arg.Bad (sprintf "Cannot guess input type of `%s'" f))

let guesstyp' f = match guesstyp f with
  | `Ctrln -> `Cn f
  | `Ctrlg -> `Cg f
  | `Ctrlf -> `Cf f
  | `Ctrlr -> `Cr f
  | `Ctrld -> `Cd f

let guessout f = match !output with
  | "-" -> mk_std_fmt
  | "" -> (try chop_extension f |> mk_fmt with
      | Invalid_argument _ when f <> "" -> mk_fmt f
      | Invalid_argument _ -> mk_std_fmt)
  | o -> mk_fmt' o

let guess_output_fmt () = match !output with
  | "-" | "" -> mk_std_fmt
  | o -> mk_fmt' o

let handle_input_file ?ityp filename =
  let ityp, mk_fmt = match ityp with
    | None -> guesstyp filename, guessout filename
    | Some ityp -> ityp, guessout filename
  in
  let itypname, handle = ityp_name_n_handle ityp in
  Log.i logger "@[Reading@ %s@ from@ `%s'…@]" itypname filename;
  handle ~filename mk_fmt;
  reset ()

let handle2 process ta tb a b =
  let mk_fmt = guessout a in                                 (* output from a *)
  let aname = ityp_name ta and bname = ityp_name tb in
  Log.i logger "@[Reading@ %s@ from@ `%s'@ and@ %s@ from@ `%s'…@]\
               " aname a bname b;
  process a b mk_fmt;
  reset ()

let handle_cn_cr = handle2 handle_ctrln_ctrlr `Ctrln `Ctrlr
let handle_cn_cf = handle2 handle_ctrln_ctrlf `Ctrln `Ctrlf
let handle_cg_cr = handle2 handle_ctrlg_ctrlr `Ctrlg `Ctrlr

let handle_input_stream = function
  | None ->
      Log.i logger "@[Reading@ node@ from@ standard@ input…@]";
      handle_ctrln (guess_output_fmt ())
  | Some ityp ->
      let itypname, handle = ityp_name_n_handle ityp in
      Log.i logger "@[Reading@ %s@ from@ standard@ input…@]" itypname;
      handle (guess_output_fmt ())

let distinguish_n_handle_inputs =
  let try_cn_cr a b = match guesstyp' a, guesstyp' b with
    | `Cn n, `Cr r | `Cr r, `Cn n -> handle_cn_cr n r
    | `Cn n, `Cf f | `Cf f, `Cn n -> handle_cn_cf n f
    | `Cg g, `Cr r | `Cr r, `Cg g -> handle_cg_cr g r
    | _ -> List.iter (handle_input_file ?ityp:!input_type) [a;b]
  in
  function
    | [] -> handle_input_stream !input_type
    | [a;b] -> try_cn_cr a b
    | lst -> List.iter (handle_input_file ?ityp:!input_type) lst

let main () =
  Arg.parse options anon usage;
  Log.i logger "@[This@ is@ ReaX@ version@ %s.@]" Version.str;
  distinguish_n_handle_inputs (List.rev !inputs)

(* -------------------------------------------------------------------------- *)

let _ =
  begin
    let columns = try int_of_string (Sys.getenv "COLUMNS") - 1 with _ -> 80 in
    Format.pp_set_margin Format.err_formatter columns;
    Format.pp_set_margin Format.std_formatter columns;
    try
      (* Printexc.record_backtrace true; *)
      Log.globallevel := globallevel;
      Log.globallevel_weaken := globallevel_weaken;
      main ();
      exit 0
    with
      | Help ->
          Arg.usage options usage; exit 0
      | Realib.Df2cfEngine.InvalidDf2cf s ->
          Log.e logger "@[invalid@ preprocessing@ method:@ %s@]" s
      | Realib.SynthEngine.InvalidAlgo s ->
          Log.e logger "@[invalid@ synthesis@ algorithm:@ %t@]" s
      | Realib.SynthEngine.InvalidOpts s ->
          Log.e logger "@[invalid@ option@ for@ synthesis@ algorithm:@ %t@]" s
      | Realib.OptimEngine.InvalidSpec s ->
          Log.e logger "@[invalid@ optimization@ specification:@ %t@]" s
      | Realib.OptimEngine.InvalidGoal s ->
          Log.e logger "@[invalid@ optimization@ goal:@ %t@]" s
      | Realib.VerifEngine.InvalidStrategy s ->
          Log.e logger "@[invalid@ verification@ strategy:@ %t@]" s
      | Realib.VerifEngine.InvalidStrategyOptionValue(o,v) ->
          Log.e logger "@[invalid@ value@ `%s'@ for@ option@ `%s'@]" v o
      | Realib.VerifEngine.InvalidDomain s ->
          Log.e logger "@[invalid@ domain@ specification:@ `%s'@]" s
      | Realib.ParseParams.InvalidBooleanSpecification v ->
          Log.e logger "@[invalid@ Boolean@ specification@ in@ options:@ `%s'@]" v
      | Realib.ParseParams.InvalidRationalSpecification v ->
          Log.e logger "@[invalid@ Rational@ specification@ in@ options:@ `%s'@]" v
      | Realib.BddapronUtil.NotSupported (s)
      | Realib.Domain.NotSupported (s) ->
          Log.e logger "@[not@ supported:@ %s@]" s;
      | Realib.Env.UnknownVariables vars ->
          Log.e logger "@[Unknown@ state@ or@ input@ variable(s):@ %a@]"
            (PPrt.pp_lst ~fopen:"" ~fclose:"" pp_print_string) vars;
      | Bdd.Env.Bddindex as e ->
          let rb = Printexc.get_raw_backtrace () in
          Log.e logger "@[insufficient@ environment@ size:@ try@ either@ \
                        increasing@ values@ for@ option@ \
                        `--env-cond-factor\'@ (current@ value@ is@ %d),@ \
                        or@ %t.@]" !cond_factor
            (fun fmt -> if !max_conds = 0 then
                fprintf fmt "enable@ automatic@ growth@ of@ the@ conditions@ \
                             environment@ by@ setting@ a@ value@ for@ option@ \
                             `--env-cond-max\'"
              else
                fprintf fmt "for@ option@ `--env-cond-max\'@ (current@ limit@ \
                             is@ %d)" !max_conds);
          prerr_endline (Printexc.to_string e);
          Printexc.print_raw_backtrace stderr rb
      | Apron.Manager.Error exclog as e ->
          let rb = Printexc.get_raw_backtrace () in
          Log.e logger "Apron@ error: %a@." Apron.Manager.print_exclog exclog;
          prerr_endline (Printexc.to_string e);
          Printexc.print_raw_backtrace stderr rb
      | Arg.Bad s | Sys_error s (* | Failure s *) ->
          Log.e' logger s
      | Exit ->
          Log.e' logger "aborted."
      | SynthesisFailure (_, env, cf) ->            (* Output dot file anyway. *)
          output_dot_file ~dotfile:!dotout ~arcs:!dot_arcs env cf
  end;
  exit 1

(* -------------------------------------------------------------------------- *)
