(******************************************************************************)
(* Domain *)
(* wrapper for abstract domains *)
(* author: Peter Schrammel *)
(* version: 0.9.0 *)
(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)
(******************************************************************************)

let logger = Log.mk ~level:Log.Debug3 "Domain"

exception NotSupported of string

(******************************************************************************)
(** {2 Domain Interface } *)
(******************************************************************************)

module type T =
  sig
    module Man: BddapronDoman.T

    type t
    type num = Man.aprondom
    type numdomain = num ApronUtil.num0

    val print : Format.formatter -> t -> unit

    val bottom : t
    val top : unit -> t

    val canonicalize : t -> unit

    val is_eq : t -> t -> bool
    val is_leq : t -> t -> bool
    val is_top : t -> bool
    val is_bottom : t -> bool

    val join : t -> t -> t
    val meet : t -> t -> t
    val meet_condition : Env.boolexpr_t -> t -> t
    val widening: t -> t -> t
    type wc_spec = private [> `Default ]
    val widening_force_conv: wc_spec -> (lp: Util.pu -> t -> t -> t * t) option
    val assign_lexpr : t -> Env.equs_t -> t
    val substitute_lexpr : t -> Env.equs_t -> t
    val forget_list : t -> Env.vars_t -> t
    val forall_bool_list : t -> Env.vars_t -> t

    val flow : t -> Env.equs_t -> Env.boolexpr_t -> t
    val accel : ?dir:ApronAccel.direction_t -> t -> Env.equs_t -> Env.boolexpr_t -> t

    val of_num : numdomain -> t
    val of_boolexpr : Env.boolexpr_t -> t
    val of_boolnumlist : (Env.boolexpr_t * numdomain) list -> t

    val to_boolexpr : t -> Env.boolexpr_t
    val to_boolexpr': t -> Env.boolexpr_t
    val to_boolnumlist : t -> (Env.boolexpr_t * numdomain) list
    val to_boolexprbool : t -> Env.boolexpr_t
    val to_boollinconsslist : t -> (Env.boolexpr_t * ApronUtil.linconss) list

    val meet_to_boolexpr : t -> Env.boolexpr_t -> Env.boolexpr_t
    val meetbool_to_boolexpr : t -> Env.boolexpr_t -> Env.boolexpr_t

    val descr: Format.formatter -> unit
    val numdoman: num Apron.Manager.t

    module E: Env.T
  end

module type FUNC =
  functor (Man : BddapronDoman.T) ->
    functor (Env: Env.T) -> (T with module Man = Man
                              and  module E = Env)

(******************************************************************************)
(** Bddapron t: B^p -> A *)
(******************************************************************************)
module Pow (Man: BddapronDoman.T) (E: Env.T) =
struct
module Man = Man
module D0 = Bddapron.Domain0
type t = Env.var_t D0.t
type num = Man.aprondom
type numdomain = num Apron.Abstract0.t
module E = E
open E.BEnvNS.BEnv
open E.BEnvNS.BOps

let doman, numdoman = Man.make ()

let descr fmt = Format.fprintf fmt "power@ domain@ over@ %t" Man.descr

let meet_condition: Env.boolexpr_t -> t -> t = fun c x ->
  D0.meet_condition doman env cond x c
let is_top = D0.is_top doman
let is_bottom = D0.is_bottom doman
let print = D0.print doman env
let is_eq = D0.is_eq doman
let bottom = D0.bottom doman env
let top () = D0.top doman env
let canonicalize = D0.canonicalize doman
let is_leq = D0.is_leq doman
let join = D0.join doman
let meet = D0.meet doman
let widening = D0.widening doman
type wc_spec = [ `Default ]
let widening_force_conv = function _ -> None
let forget_list = D0.forget_list doman env
let forall_bool_list = D0.forall_bool_list doman env

let assign_lexpr s equs =
  let (vars,exprs) = Env.split_equs equs in
  D0.assign_lexpr doman env cond s vars exprs None

let substitute_lexpr s equs =
  let (vars,exprs) = Env.split_equs equs in
  D0.substitute_lexpr doman env cond s vars exprs None

let flow s equs staycond =
  let cont_elapse = BddapronHybrid.cont_elapse2 ~convexify:false
    (BddapronUtil.get_primed_var env) in
  BddapronHybrid.flow ~trans_doman:doman env cond doman
    ~cont_elapse staycond equs E.env.Env.i_vars s

let accel ?(dir=`Forward) s equs g =
  let trans_apronman = D0.man_get_apron doman in
  join s (BddapronAccel.acc ~dir ~trans_apronman
            env cond doman g equs E.env.Env.ni_vars s)

let of_boolnumlist = D0.of_bddapron doman env
let of_num = D0.of_apron doman env
let of_boolexpr b = D0.meet_condition doman env cond (top ()) b

let to_boolnumlist s = D0.to_bddapron doman s

let to_boolexprbool s =
  List.fold_left (fun bacc (be,_)-> bacc ||~ be) ff (to_boolnumlist s)

let to_boolexpr s =
  let apronman = D0.man_get_apron doman in
  let apronenv = Env.apronenv E.env in
  List.fold_left
    (fun bacc (be,ne)->
      bacc ||~ (be &&~
                  (BddapronUtil.apron_to_boolexpr env cond apronman
                     (ApronUtil.abstract1_of_abstract0 apronenv ne))))
    ff
    (to_boolnumlist s)

let to_boolexpr' s =
  let apronman = D0.man_get_apron doman in
  let apronenv = Env.apronenv E.env in
  List.fold_left
    (fun bacc (be,ne)->
      bacc &&~ (!~be ||~
                   (BddapronUtil.apron_to_boolexpr' env cond apronman
                      (ApronUtil.abstract1_of_abstract0 apronenv ne))))
    tt
    (to_boolnumlist s)

let to_boollinconsslist s =
  let apronman = D0.man_get_apron doman in
  let apronenv = Env.apronenv E.env in
  List.map (fun (b,n) -> (b,Apron.Abstract1.to_lincons_array apronman
                        (ApronUtil.abstract1_of_abstract0 apronenv n)))
    (to_boolnumlist s)

let meet_to_boolexpr s bexpr = bexpr &&~ (to_boolexpr s)
let meetbool_to_boolexpr s bexpr = bexpr &&~ (to_boolexprbool s)
end

let pow (module Base: BddapronDoman.T) (module E: Env.T) =
  (module Pow(Base)(E) : T)

(******************************************************************************)
(** product t: p(B^p) x A, based on Bddapron t *)
(******************************************************************************)
module Prod (Man: BddapronDoman.T) (E: Env.T) =
struct
module Man = Man
module D0 = Bddapron.Domain0
type t = Env.var_t D0.t
type num = Man.aprondom
type numdomain = num Apron.Abstract0.t
module E = E
open E.BEnvNS.BEnv
open E.BEnvNS.BOps

let doman, numdoman = Man.make ()

let descr fmt = Format.fprintf fmt "product@ domain@ over@ %t" Man.descr

let print = D0.print doman env

let convexify s =
  let apronman = D0.man_get_apron doman in
  let apronenv = Env.apronenv E.env in
  let (b,n) = List.fold_left
    (fun (bacc,nacc) (be,ne) ->
      (bacc ||~ be, Apron.Abstract0.join apronman nacc ne))
    (ff, ApronUtil.bottom0 apronman apronenv)
    (D0.to_bddapron doman s)
  in
  D0.meet_condition doman env cond (D0.of_apron doman env n) b

let meet_condition: Env.boolexpr_t -> t -> t = fun e s ->
  convexify (D0.meet_condition doman env cond s e)

let is_top = D0.is_top doman
let is_bottom = D0.is_bottom doman
let is_eq = D0.is_eq doman

let bottom = D0.bottom doman env
let top () = D0.top doman env

let canonicalize = D0.canonicalize doman

let is_leq = D0.is_leq doman

let join s1 s2 = convexify (D0.join doman s1 s2)
let meet s1 s2 = convexify (D0.meet doman s1 s2)
let widening s1 s2 = convexify (D0.widening doman s1 s2)
type wc_spec = [ `Default ]
let widening_force_conv = function _ -> None

let assign_lexpr s equs =
  let (vars,exprs) = Env.split_equs equs in
  convexify (D0.assign_lexpr doman env cond s vars exprs None)

let forget_list s vars =
  convexify (D0.forget_list doman env s vars)

let forall_bool_list s vars =
  convexify (D0.forall_bool_list doman env s vars)

let substitute_lexpr s equs =
  let (vars,exprs) = Env.split_equs equs in
  convexify (D0.substitute_lexpr doman env cond s vars exprs None)

let flow s equs staycond =
  let trans_doman = doman in
  let cont_elapse = BddapronHybrid.cont_elapse2 ~convexify:false
    (BddapronUtil.get_primed_var env) in
  convexify (BddapronHybrid.flow ~trans_doman env cond doman
               ~cont_elapse staycond equs E.env.Env.i_vars s)

let accel ?(dir=`Forward) s equs g =
  let trans_apronman = D0.man_get_apron doman in
  join s (BddapronAccel.acc ~dir ~trans_apronman
            env cond doman g equs E.env.Env.ni_vars s)

let of_boolnumlist = function
  | [ bexpr, apron ] ->
      D0.meet_condition doman env cond (D0.of_apron doman env apron) bexpr
  |bnlist -> convexify (D0.of_bddapron doman env bnlist)

let of_num = D0.of_apron doman env
let of_boolexpr b = D0.meet_condition doman env cond (top ()) b

let to_boolexprbool s =
  List.fold_left (fun bacc (be,_)-> bacc ||~ be) ff (D0.to_bddapron doman s)

let to_boolnumlist = D0.to_bddapron doman

let to_boolexpr s =
  let apronman = D0.man_get_apron doman in
  let apronenv = Env.apronenv E.env in
  List.fold_left
    (fun bacc (be,ne)->
      bacc ||~ (be &&~
                  (BddapronUtil.apron_to_boolexpr env cond apronman
                     (ApronUtil.abstract1_of_abstract0 apronenv ne))))
    ff
    (to_boolnumlist s)

let to_boolexpr' s =
  let apronman = D0.man_get_apron doman in
  let apronenv = Env.apronenv E.env in
  List.fold_left
    (fun bacc (be,ne)->
      bacc &&~ (!~be ||~
                   (BddapronUtil.apron_to_boolexpr' env cond apronman
                      (ApronUtil.abstract1_of_abstract0 apronenv ne))))
    tt
    (to_boolnumlist s)

let to_boollinconsslist s =
  let apronman = D0.man_get_apron doman in
  let apronenv = Env.apronenv E.env in
  List.map (fun (b,n) -> (b,Apron.Abstract1.to_lincons_array apronman
                        (ApronUtil.abstract1_of_abstract0 apronenv n)))
    (to_boolnumlist s)

let meet_to_boolexpr s bexpr = bexpr &&~ (to_boolexpr s)
let meetbool_to_boolexpr s bexpr = bexpr &&~ (to_boolexprbool s)
end

let prod (module Base: BddapronDoman.T) (module E: Env.T) =
  (module Prod(Base)(E) : T)
