(******************************************************************************)
(* DomExt *)
(* domain extensions *)
(* author: Nicolas Berthier *)
(* version: 0.9.0 *)
(* This file is part of ReaTK released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)
(******************************************************************************)
(** various utilities for domain extensions *)

(**/**)

let (&) f g = f g
let (|>) f g = g f
let logger = Log.mk ~level:Log.Debug3 "c."

let pp_doms pp_dom = Util.list_print'
  ~copen:(fun fmt -> Format.fprintf fmt "{@[<hv 0>")
  ~csep:(fun fmt -> Format.fprintf fmt ",@ ")
  ~cclose:(fun fmt -> Format.fprintf fmt "@]}")
  pp_dom

(* -------------------------------------------------------------------------- *)

module type CONVEX_DISJ = sig
  module CDom: Domain.T
  module CTrv: module type of BddapronUtil.CTrv (CDom.E.BEnvNS)
  open CDom.E.BEnvNS.BAll
  type t = CDom.t list
  val pp: t Util.pp
  val abstract_convex_disjuncts_of
    : booldisj: bool
    -> ?relax: bool
    -> ?elim_numvars: vars
    -> ?lp: Util.pu
    -> ?name: string
    -> bexpr -> t
  val concretize_disj: ?init: bexpr -> t -> bexpr
  val concretize_conj_neg: ?init: bexpr -> t -> bexpr
end

module ConvexDisj (ConvexDom: Domain.T) : sig
  include CONVEX_DISJ with module CDom = ConvexDom
end = struct
  module CDom = ConvexDom
  module CTrv = BddapronUtil.CTrv (CDom.E.BEnvNS)
  include CTrv.Bool
  open Bddapron.Apronexpr
  open CDom.E
  open CDom.E.BEnvNS.BAll

  (* --- *)

  type t = CDom.t list
  let pp: t Util.pp = pp_doms CDom.print

  (* --- *)

  type num0 = CDom.num ApronUtil.num0
  type num1 = CDom.num ApronUtil.num1
  let abstract_to_numdom l = ApronUtil.num0_of_num1 l |> CDom.of_num
  let meet_abstract d l = abstract_to_numdom l |> CDom.meet d

  (* --- *)

  module NumCond = struct
    type t = var Condition.t
    let compare: t -> t -> int = Condition.compare symbol
    let negate: t -> t = Condition.negate vartyp
    let print = Condition.print symbol
  end
  module NumConds = Util.MakeSet (NumCond)
  module BddMap = BEnvNS.BddMap

  type numconj = NumConds.t
  type numdisj = numconj list
  type bddmap = numdisj BddMap.t

  (* --- *)

  let print_numconj = NumConds.print
  let print_numdisj fmt disj =
    Format.fprintf fmt "[@[";
    List.fold_left (fun first c ->
      if not first then Format.fprintf fmt ";@ ";
      print_numconj fmt c; false) true disj |> ignore;
    Format.fprintf fmt "@]]"

  let fold_numconvex_abstr': bexpr -> bddmap = fun b ->
    foldcut_numconvex
      ~convex:begin fun (numdom, numconds) numcond condition ->
        let numdom = CDom.meet_condition numcond numdom in
        if CDom.is_bottom numdom then [] else [
          numdom, NumConds.add condition numconds
        ]
      end
      ~diseq:begin fun (numdom, numconds) _numcond expr ->
        let inf, sup = BddapronUtil.diseq_linexpr env.Env.env expr in
        let l = ApronUtil.abstract_lincons_list CDom.numdoman [ inf; sup ] in
        let l = List.map (meet_abstract numdom) l in
        let inf = List.nth l 0 and sup = List.nth l 1 in
        let l = if CDom.is_bottom inf then [] else
            [ inf, NumConds.add (NumCond.negate (Condition.SUPEQ, expr)) numconds ] in
        let l = if CDom.is_bottom sup then l else
            (sup, NumConds.add (Condition.SUP, expr) numconds) :: l in
        l
      end
      (CDom.top (), NumConds.empty)
      begin fun acc booldom (_numdom, numconds) ->
        let x = try BddMap.find booldom acc with Not_found -> [] in
        BddMap.add booldom (numconds :: x) acc
      end
      (BddMap.empty)
      b

  let relax_numdisj: numdisj -> numdisj =
    let sort_numdisj: numdisj -> numdisj =
      List.sort (fun a b -> NumConds.cardinal a - NumConds.cardinal b)
    in
    let elim c cs acc : numdisj =
      let cs = NumConds.remove c cs in
      (* Log.d3 logger "Removing %a @<2>→ %a" NumCond.print c NumConds.print
         cs; *)
      if NumConds.is_empty cs then acc else cs :: acc
    in
    let rec elim0 cs acc =
      if NumConds.is_empty cs then acc else
        let c = NumConds.choose cs in                    (* XXX combinatorial *)
        let c' = NumCond.negate c in
        let cs = NumConds.remove c cs in
        let cs'= NumConds.add c' cs in
        List.fold_left (fun acc cs'' ->
          (* Log.d3 logger "%a ⊆ %a ?" NumConds.print cs' NumConds.print cs''; *)
          if NumConds.subset cs' cs'' then elim c' cs'' acc else cs'' :: acc)
          [] acc
    in
    let rec elim1 (acc: numdisj) : numdisj -> numdisj = function
      | [] -> acc
      | cs::tl -> elim1 (cs :: elim0 cs acc) (elim0 cs tl)
    in
    fun numdisjs -> sort_numdisj numdisjs |> elim1 []

  let relax_bddmap: bddmap -> bddmap = BddMap.map relax_numdisj

  let print_bddmap: bddmap Util.pp = BddMap.print print_numdisj

  let meet_numconj: num0 -> numconj -> num0 = fun i x ->
    NumConds.fold (fun c acc ->
      match Condition.to_apron0 symbol (apronenv ()) c with
        | `Lin l -> l :: acc
        | `Tree _ -> assert false)
      x [] |> Array.of_list |> Apron.Abstract0.meet_lincons_array CDom.numdoman i

  let disj_of_bddmap abstract (bddmap: bddmap) : t =
    let numtop = ApronUtil.top0 CDom.numdoman (apronenv ()) in
    BddMap.fold (fun bool numdisj disj ->
      List.fold_left (fun disj numconj ->
        meet_numconj numtop numconj |> CDom.of_num |> abstract disj bool)
        disj numdisj)
      bddmap []

  (* --- *)

  exception NonConvexPredicate

  let abstract elim_numvars acc booldom numdom : t =
    let numdom = match elim_numvars with
      | [] -> numdom
      | vars -> CDom.forget_list numdom vars
    in
    let aval = CDom.meet_condition booldom numdom in
    if CDom.is_top aval then raise NonConvexPredicate else
      if List.exists (CDom.is_leq aval) acc then acc           (* early filter *)
      else aval :: acc

  (* --- *)

  let fold_numconvex_abstr f i =
    foldcut_numconvex
      ~convex:begin fun numdom numcond _ ->
        let numdom = CDom.meet_condition numcond numdom in
        if CDom.is_bottom numdom then [] else [ numdom ]
      end
      ~diseq:begin fun numdom _ expr ->
        let inf, sup = BddapronUtil.diseq_linexpr env.Env.env expr in
        let l = ApronUtil.abstract_lincons_list CDom.numdoman [ inf; sup ] in
        let l = List.rev_map (meet_abstract numdom) l in
        List.filter (fun l -> not (CDom.is_bottom l)) l
      end
      (CDom.top ()) f i

  let abstract_convex_disjuncts_of
      ~booldisj
      ?(relax=true)
      ?(elim_numvars=[])
      ?lp
      ?name
      b =
    try
      let disjs = if relax
        then
          let bddmap = fold_numconvex_abstr' b in
          (* Log.d2 logger " conds: %a" print_bddmap bddmap; *)
          let bddmap = relax_bddmap bddmap in
          (* Log.d2 logger "conds': %a" print_bddmap bddmap; *)
          disj_of_bddmap (abstract elim_numvars) bddmap
        else
          fold_numconvex_abstr
            (if not booldisj then abstract elim_numvars else
                (fun acc booldom numdom ->
                  BddapronUtil.boolexpr_fold_paths env.Env.env
                    (fun bdom acc -> abstract elim_numvars acc bdom numdom)
                    booldom acc))
            [] b
      in
      (match lp, name with
        | Some lp, Some name ->
            Log.i logger "%t@[Covering@ %s@ with@ %d@ abstract@ values.@]\
                         " lp name (List.length disjs);
        | _ -> ());
      disjs
    with
      (* XXX: optional error case? *)
      | NonConvexPredicate -> [ CDom.top () ]

  (* --- *)

  let concretize_disj ?(init=ff) disj =
    List.fold_left (fun b l -> b ||~ CDom.to_boolexpr l) init disj |>
        BddapronUtil.normalize_benumneg env.Env.env env.Env.cond

  let concretize_conj_neg ?(init=tt) disj =
    List.fold_left (fun b l -> b &&~ CDom.to_boolexpr' l) init disj |>
        BddapronUtil.normalize_benumneg env.Env.env env.Env.cond

end

(* -------------------------------------------------------------------------- *)

module type N = sig
  type num
  val numdoman: num ApronUtil.man_t
end

module NumDisj (E: Env.T) (NumDom: N) = struct
  open E
  open NumDom

  module H = Hashtbl.Make (struct
    type t = num Apron.Abstract0.t
    let equal x = Apron.Abstract0.is_eq numdoman x
    let hash x = Apron.Abstract0.hash numdoman x
  end)

  (* --- *)

  let make_table () = Cudd.Mtbddc.make_table
    ~hash:(fun x -> Apron.Abstract0.hash numdoman x)
    ~equal:(fun x -> Apron.Abstract0.is_eq numdoman x)
  let clear_table tbl = Cudd.PWeakke.clear tbl

  let num_tbl = make_table ()
  let clear () = clear_table num_tbl

  (* --- *)

  type elt = num Apron.Abstract0.t
  type u = elt Cudd.Mtbddc.unique
  type t = u list

  let empty = []
  let is_empty: t -> bool = ((=) [])
  let add e l = let e = Cudd.Mtbddc.unique num_tbl e in
                if List.mem e l then l else e :: l
  let remove e l = let e = Cudd.Mtbddc.unique num_tbl e in
                   List.filter ((<>) e) l
  let choose: t -> elt = fun l -> List.hd l |> Cudd.Mtbddc.get
  let fold f l a = List.fold_left (fun a e -> f (Cudd.Mtbddc.get e) a) a l
  let inspect e = e
  (* let elements l = List.rev_map Cudd.Mtbddc.get *)
  let union l l' = fold add l l'

  let hash: t -> int = List.length                          (* arg... whatever *)
  let equal (l: t) (l': t) =
    l == l' ||
      ((* TODO: use List.compare_lenghts when switching to OCaml≥4.05.0 *)
        List.length l = List.length l' &&
          List.for_all (fun e -> List.exists ((=) e) l') l)

  (* --- *)

  let print_n fmt e =
    ApronUtil.pp_num0 (Bddapron.Env.string_of_aprondim BEnv.env) fmt e

  let print fmt e =
    Format.fprintf fmt "{@[";
    fold (fun c first -> if not first then Format.fprintf fmt ",@ "; print_n fmt c;
      false) e true |> ignore;
    Format.fprintf fmt "@]}"

  (* Non-redundant: *)

  let join_elt l s =
    let uselessp = Apron.Abstract0.is_leq numdoman in
    let rec join acc l' s =
      if is_empty s then add l' acc else
        let l = choose s in
        let nl = inspect l in
        if uselessp nl l' then join acc l' (remove l s)
        else if uselessp l' nl then union acc s
        else join (add l acc) l' (remove l s)
    in
    join empty l s

  let join = fold join_elt

  let fold_prod f a b acc =
    let rec pw acc = function
      | s when is_empty s -> acc
      | s -> let a = choose s in
            pw (fold (f (inspect a)) b acc) (remove a s)
    in
    pw acc a

  let meet a b =
    let meet_n a b = let x = Apron.Abstract0.meet numdoman a b in
                     Apron.Abstract0.canonicalize numdoman x;
                     x
    in
    fold_prod (fun a b -> join_elt (meet_n a b)) a b empty

end

(* -------------------------------------------------------------------------- *)

module NumDisjDD (E: Env.T) (NumDom: N) = struct
  module ND = NumDisj (E) (NumDom)
  open E
  open Cudd
  open Mtbdd

  let ddtbl =
    make_table ~hash:ND.hash ~equal:(fun x y -> ND.equal x y)

  let clear () =
    ND.clear ();
    PWeakke.clear ddtbl

  let bottom () = cst BEnvNS.POps.cuddman ddtbl ND.empty

  (* --- *)

  let assoc g numdisj =
    let d = List.fold_left (fun acc a -> ND.join_elt a acc) ND.empty numdisj in
    let l = cst (Env.cuddman env) ddtbl d in
    ite g l (bottom ())

  let meet = User.make_op2
    ~memo:(Memo.Cache(Cache.create2 ()))
    ~commutative:true ~idempotent:false
    ~special:(fun dd1 dd2 ->
      if is_cst dd1 && ND.is_empty (dval dd1) then Some dd1
      else if is_cst dd2 && ND.is_empty (dval dd2) then Some dd2
      else None)
    (fun x y -> ND.meet (get x) (get y) |> unique ddtbl)

  let join = User.map_op2
    ~memo:(Memo.Cache(Cache.create2 ()))
    ~commutative:true ~idempotent:false
    ~special:(fun dd1 dd2 ->
      if is_cst dd1 && ND.is_empty (dval dd1) then Some dd2
      else if is_cst dd2 && ND.is_empty (dval dd2) then Some dd1
      else None)
    (fun x y -> ND.fold ND.join_elt (get x) (get y) |> unique ddtbl)

  (* --- *)

  let forall vars t =
    if vars=[] then t else begin
      let (supp,tadim) = Bddapron.Common.lvar_split env.Env.env vars in
      if tadim <> [| |]then
        failwith "forall: numerical variables in universal quantification"
      else
        if Bdd.is_true supp then t else
          let forall = User.make_exist ~memo:(Memo.Cache(Cache.create2 ())) meet in
          let res = User.apply_exist forall ~supp t in
          User.clear_op2 meet;
          User.clear_exist forall;
          res
    end

  let fold_guardleaves = fold_guardleaves

end

(* -------------------------------------------------------------------------- *)

exception NotSupported of string

module type FP_PARAMS = sig
  val booldisj: bool
  val relax: bool
  val uqbdd: bool
end

module DefaultFPParams = struct
  let booldisj = false
  let relax = true
  let uqbdd = true
end

module type POWEXT = sig
  module CDom: Domain.T
  module CDisj: CONVEX_DISJ with module CDom = CDom
  include Domain.T with module Man = CDisj.CDom.Man

  open E.BEnvNS.BAll

  val widening_em: lp:Util.pu -> t -> t -> t
  val widening_em_connect: lp:Util.pu -> t -> t -> t * t

  val of_boolexpr_elim: bexpr -> vars -> t

  type disj = CDisj.t
  val to_disj: t -> disj
  val of_disj: disj -> t
  val overapprox_diff: t -> disj -> t
  val join_disj: t -> disj -> t
end

(** Finite Powerset Domain.  Helps computing sets of least upper bounds with
    "parallel" ascending chains.

    Non-redundancy of elements is always enforced.

    Corresponds to the Hoare powerset extension for abstract domains, where an
    abstract value {i S{_1}} is lower or equal than {i S{_2}} iff all elements of
    {i S{_1}} are covered by elements of {i S{_2}}.

    Also, the widening operator provided (delay-ably) enforces Egli-Milder
    ordering (where {i S{_1}} is lower or equal than {i S{_1}} iff ..., i.e. each
    element of {i S{_2}} must also cover at least one element of {i S{_1}}) on
    its arguments to ensure convergence. *)
module FinPowset (ConvexDom: Domain.T) (Params: FP_PARAMS) : sig
  include POWEXT with module CDom = ConvexDom
end = struct

  module CDom = ConvexDom
  module CDisj = ConvexDisj (CDom)
  module Man = CDom.Man
  module E = CDom.E
  open E
  open E.BEnvNS.BOps

  type disj = CDisj.t
  open Params

  type num = CDom.num
  type numdomain = CDom.numdomain

  let descr fmt = Format.fprintf fmt "powerset@ extension@ of@ %t" CDom.descr
  let numdoman = CDom.numdoman

  (** Lists of non-redundant abstract values, associated with their age (the
      number of times they grew) for delayed base widening. *)
  type t = { mutable disj: disj; }

  let bottom: t = { disj = [] }
  let top () : t = { disj = [ CDom.top () ]; }
  let is_bottom { disj } =
    disj = [] || List.length disj = 1 && CDom.is_bottom (List.hd disj)
  let is_top { disj } = List.exists CDom.is_top disj
  let canonicalize x =
    List.iter CDom.canonicalize x.disj;
    x.disj <- List.stable_sort Stdlib.compare x.disj

  let cons ~is_eq ~is_leq l list =
    let rec cons acc l' = function
      | [] -> l' :: acc
      | l :: tl when is_eq l l' -> l :: List.rev_append acc tl
      | l :: tl when is_leq l l' -> cons acc l' tl
      | l :: tl when is_leq l' l -> l :: List.rev_append acc tl
      | l :: tl -> cons (l :: acc) l' tl
    in
    cons [] l list

  (** [gcons a h] inserts the disjunct [h] iff it is not lower or equal than any
      element of [a]. Ensures non-redundancy of abstract values. *)
  let bons { disj } h : t =
    { disj = cons ~is_eq:CDom.is_eq ~is_leq:CDom.is_leq h disj }

  (** [cons' a h] inserts the disjunct [h] iff it is not greater or equal than
      any element of [a]. Ensures non-redundancy of abstract values. *)
  let bons' { disj } h : t =
    { disj = cons ~is_eq:CDom.is_eq ~is_leq:(fun a b -> CDom.is_leq b a) h disj }

  let buildup op { disj } = List.fold_left op bottom disj

  let apply1 op arg =
    buildup (fun acc l -> bons acc (op l arg))

  let applyl op arg =
    buildup (fun acc l -> List.fold_left bons acc (op l arg))

  let meet { disj = a } =
    buildup (fun acc b -> List.fold_left (fun acc a -> bons acc (CDom.meet a b)) acc a)

  let join_disj (a: t) (d: disj) = List.fold_left bons a d
  let join: t -> t -> t = fun a { disj } -> join_disj a disj

  let of_disj (d: disj) = join_disj bottom d
  let to_disj { disj } : disj = disj

  let incomp a b = not (CDom.is_leq a b || CDom.is_leq b a)
  let rec check' : disj -> bool = function
    | [] | [_] -> true
    | l :: tl -> List.for_all (incomp l) tl && check' tl
  let check (a: t) = check' (to_disj a)

  let print fmt { disj } = pp_doms CDom.print fmt disj

  (* --- *)

  let hoare_leq { disj = a } { disj = b } =
    List.for_all (fun a -> List.exists (CDom.is_leq a) b) a

  let is_leq a b = hoare_leq a b

  let egli_milner_leq ({ disj = da } as a) ({ disj = db } as b) =
    is_bottom a || hoare_leq a b &&
      List.for_all (fun b -> List.exists (fun a -> CDom.is_leq a b) da) db

  let is_eq a b = is_leq a b && is_leq b a

  (* --- *)

  (** [overapprox_diff l d] returns all elements of [l] that are not covered
      by a disjunct of [d]. *)
  let overapprox_diff { disj = a } (d: disj) =
    { disj = List.filter (fun a -> not (List.exists (CDom.is_leq a) d)) a }

  let pp_card fmt { disj } = Format.fprintf fmt "%d" (List.length disj)

  let egli_milner_connect ?(lp=fun _ -> ()) ({ disj = a } as la) ({ disj = b } as lb) =
    Log.i logger "%tEnforcing Egli-Milner ordering." lp;
    Log.d logger "%t|a| = %a, |b| = %a" lp pp_card la pp_card lb;
    let join' cons l =
      List.fold_left (fun acc l' ->
        let r = CDom.join l l' in
        Log.d3 logger "%t@[<v>@<2>⊔[@[<v>%a,@\n%a@]]@\n@<2>→ @[<v>%a@]@]\
                      " lp CDom.print l CDom.print l' CDom.print r;
        cons acc r)
    in
    let _lower_bounds_of_join acc l =
      (* For each non-covering element of b, select all lower bounds of their
         joins with all elements of a (l) *)
      join' bons' l acc a
    in
    (* let _join_with_all_noncovering acc l = *)
    (*   (\* Join elements of a (l') with all non-covering elements of b *\) *)
    (*   join' bons' l acc a *)
    (* in *)
    let covering, noncovering =
      List.partition (fun b -> List.exists (fun a -> CDom.is_leq a b) a) b
    in
    let low = List.fold_left _lower_bounds_of_join bottom noncovering in
    if Log.check_level logger Log.Debug3
    then Log.d3 logger "%tAdding: %a" lp print low
    else Log.d logger "%tAdding %d value(s)." lp (List.length low.disj);
    join { disj = covering } low

  (* --- *)

  let ( <~ ) l l' = not (CDom.is_eq l l') && (CDom.is_leq l l')

  (** [widening_em ~lp a b] performs widening by relying on base widening
      operations in order to increase abstract values individually.  To ensure
      convergence, [a] and [b] must have previously been joined using
      [widening_em_connect a b] so as to ensure Egli-Milner ordering (otherwise
      some elements of [b] may not cover elements of [a]). *)
  let widening_em ~lp (a: t) (b: t): t =

    (* raises Exit if ⊤ *)
    let base_widening l' acc l =                       (* we know that l < l' *)
      assert (l <~ l');
      let r = CDom.widening l l' in
      if Log.check_level logger Log.Debug3
      then Log.d3 logger "%t@[<v>Ba@[<v>se widening in finite powerset domain:\
                            @\n%a,@;%a@]@\n@<2>→ %a@]\
                           " lp CDom.print l CDom.print l' CDom.print r
      else Log.d2 logger "%tBase widening in finite powerset domain." lp;
      if CDom.is_top r then raise Exit;                  (* anticipate failure *)
      r :: acc
    in

    try
      let res = List.fold_left begin fun acc l' ->
        if CDom.is_top l' then raise Exit;               (* anticipate failure *)
        let ll = List.find_all (fun l -> l <~ l') a.disj in (* strict comparison!*)
        let ll = List.fold_left (base_widening l') [] ll in
        if ll <> [] then join_disj acc ll    (* already used to widen *)
        else bons acc l'                   (* no greater than any value in a *)
      end bottom b.disj in
      assert (check res);
      res
    with
      | Exit -> top ()

  let widening = widening_em ~lp:(fun fmt -> ())

  let widening_em_connect ~lp a b =
    if egli_milner_leq a b then
      (Log.i logger "%tEgli-Milner ordering already satisfied." lp;
       a, b)
    else
      (Log.i logger "%tEnforcing Egli-Milner ordering." lp;
       a, egli_milner_connect ~lp a b)

  type wc_spec = [ `Default | `EgliMilner ]
  let widening_force_conv = function
    | `Default | `EgliMilner -> Some widening_em_connect

  (* --- *)

  let assign_lexpr x equs = apply1 CDom.assign_lexpr equs x
  let substitute_lexpr x equs = apply1 CDom.substitute_lexpr equs x
  let forget_list x vars = apply1 CDom.forget_list vars x

  let to_boolexpr x = to_disj x |> CDisj.concretize_disj
  let to_boolexpr' x = to_disj x |> CDisj.concretize_conj_neg

  (* --- *)

  module BL = Bddapron.Bddleaf
  module DD = NumDisjDD (CDom.E) (CDom)
  module ND = DD.ND

  (* --- *)

  let uniquify_nums gns =
    let module AH = ND.H in
    let nh = AH.create (List.length gns) in
    List.iter
      (fun (g, n) -> AH.replace nh n (try AH.find nh n ||~ g with Not_found -> g))
      gns;
    AH.fold (fun n g acc -> (g, n) :: acc) nh []

  (* --- *)

  let forall_bool_list_bdd x vars =

    let open BL in
    let join_n = cons_disjoint ~is_equal:ND.equal ~merge:ND.join in
    let meet_n = cons_disjoint ~is_equal:ND.equal ~merge:ND.meet in

    let disj = to_disj x in
    let gls = List.rev_map CDom.to_boolnumlist disj |> List.flatten in

    let guard, gnl = List.fold_left begin fun (guard, gnl) (g, n) ->
      guard ||~ g,
      join_n { guard = g; leaf = ND.add n ND.empty } gnl
    end (ff, []) gls in

    let vars = bsupp vars in
    let nguard = forall' vars guard in
    let gnl = List.fold_left begin fun gnl { guard; leaf } ->
      meet_n { guard = existand' vars guard nguard; leaf } gnl
    end [] gnl in

    let gnl = List.fold_left begin fun acc { guard; leaf } ->
      ND.fold (fun n acc -> (guard, n) :: acc) leaf acc
    end [] gnl in

    ND.clear ();
    let ec = BddapronUtil.full_enum_careset env.Env.env env.Env.cond in
    let x = List.fold_left begin fun acc (g, n) ->
      let g = g ^~ ec in
      bons acc (CDom.of_boolnumlist [g, n])
    end bottom (uniquify_nums gnl) in
    x

  (* --- *)

  let forall_bool_list_mtbdd x vars =
    let dds = List.fold_left begin fun dd gl ->
      List.fold_left (fun dd (g, l) -> DD.assoc g [l] |> DD.join dd)
        dd (CDom.to_boolnumlist gl)
    end (DD.bottom ()) (to_disj x) in
    let dds = DD.forall vars dds in
    let ec = BddapronUtil.full_enum_careset env.Env.env env.Env.cond in
    let x = DD.fold_guardleaves begin fun g numdisj ->
      let g = g ^~ ec in
      ND.fold (fun num acc -> bons acc (CDom.of_boolnumlist [g, num])) numdisj
    end dds bottom in
    DD.clear ();
    x

  (* --- *)

  let forall_bool_list = match uqbdd with
    | false -> forall_bool_list_mtbdd
    | true -> forall_bool_list_bdd

  (* --- *)

  let flow x vars b = raise (NotSupported "flow")
  let accel ?(dir=`Forward) x equs g = raise (NotSupported "accel")

  (* --- *)

  let of_num apron = raise (NotSupported "of_num")

  let of_boolexpr' ?(relax=relax) b =
    CDisj.abstract_convex_disjuncts_of ~booldisj ~relax b |>
        of_disj
  let of_boolexpr = of_boolexpr' ~relax

  let of_boolexpr_elim' ?(relax=relax) b elim_numvars =
    CDisj.abstract_convex_disjuncts_of ~booldisj ~relax ~elim_numvars b |>
        of_disj
  let of_boolexpr_elim = of_boolexpr_elim' ~relax

  let to_boolexprbool b = raise (NotSupported "to_boolexprbool")
  let of_boolnumlist bnlist = raise (NotSupported "of_boolnumlist")
  let to_boolnumlist x = raise (NotSupported "to_boolnumlist")
  let to_boollinconsslist s = raise (NotSupported "to_boollinconsslist")

  let meet_to_boolexpr x bexpr = bexpr &&~ to_boolexpr x

  let meetbool_to_boolexpr x bexpr = raise (NotSupported "meetbool_to_boolexpr")

  (* --- *)

  let meet_condition c { disj } =
    CDisj.CTrv.Bool.fold_numconvex tt begin fun acc b n ->
      List.fold_left
        (fun acc d -> CDom.meet_condition b d |> CDom.meet_condition n |> bons acc)
        acc disj
    end bottom c

end

let pwext (module ConvexDom: Domain.T) (module Params: FP_PARAMS) =
  (module FinPowset (ConvexDom) (Params) : Domain.T)

(* -------------------------------------------------------------------------- *)

module type ELIM_UTILS_PARAMS = sig
  val relax: bool
  val uqbdd: bool
  val pp_neg: bool
  val debug: Log.level
  val info: Log.level
end

module DefaultElimUtilsParams: ELIM_UTILS_PARAMS = struct
  let relax = true
  let uqbdd = false
  let pp_neg = true
  let debug = Log.Debug3
  let info = Log.Debug
end

module ElimUtils = functor (P: ELIM_UTILS_PARAMS) (L: Log.T) (E: Env.T) -> struct
  open L
  (**/**)

  module D =
    FinPowset (Domain.Pow (BddapronDoman.StrictPol) (E)) (struct
      include DefaultFPParams
      include P
    end)
  open E.BEnvNS
  open E.BEnvNS.BAll
  open Format

  let log3 f = Log.cd3 logger f

  let cond_save f x =
    let r = Bdd.Cond.save BEnv.cond in
    let x = f x in
    Bdd.Cond.restore_with r BEnv.cond;
    x

  let concr = D.to_boolexpr
  let abstr = D.of_boolexpr
  let abstr_exist nl b = D.of_boolexpr_elim b nl
  let concr' = D.to_boolexpr'
  let concr'_abstr = cond_save (fun x -> concr' x |> abstr)
  let concr'_abstr_exist nl = cond_save (fun x -> concr' x |> abstr_exist nl)

  type repr =
    | Pos of bexpr
    | PoA of D.t
    | Neg of bexpr
    | NeA of D.t

  let repr x = Pos x
  let repr' x = Neg x
  let arepr x = PoA x
  let arepr' x = NeA x

  let retr = function
    | Pos x -> simplify x
    | PoA x -> concr x
    | Neg x -> !~x |> simplify
    | NeA x -> concr' x

  let aretr = function
    | Pos x -> abstr x
    | PoA x -> x
    | Neg x -> abstr !~x
    | NeA x -> concr'_abstr x

  (* --- *)

  let axist vars x = D.forget_list x vars

  let aforall vars x = D.forall_bool_list x vars

  let axist_bxist vars x =                    (* delay abstraction operation *)
    let nl, bl = List.partition is_num vars in
    exist bl x |> abstr_exist nl

  let axist_neg_borall vars x =         (* delay abstraction of neg operation *)
    let nl, bl = List.partition is_num vars in
    forall bl x |> (!~) |> abstr_exist nl

  let axist_neg_aorall vars x =  (* delay (-abstr of-) neg of concr operation *)
    let nl, bl = List.partition is_num vars in
    (* concr x |> forall bl |> (!~) |> abstr_exist nl *)
    aforall bl x |> concr'_abstr_exist nl

  let exist' ev = let has_nums = has_nums ev in function
    | Pos x when has_nums -> PoA (axist_bxist ev x)
    | PoA x when has_nums -> PoA (axist ev x)
    | Neg x when has_nums -> PoA (axist_neg_borall ev x)
    | NeA x when has_nums -> PoA (axist_neg_aorall ev x)
    | Pos x -> Pos (BOps.exist ev x)
    | PoA x -> PoA (axist ev x)
    | Neg x -> Neg (forall ev x)
    | NeA x -> NeA (aforall ev x)

  let forall' fv = let has_nums = has_nums fv in function
    | Pos x when has_nums -> NeA (axist_neg_borall fv x)
    | PoA x when has_nums -> NeA (axist_neg_aorall fv x)
    | Neg x when has_nums -> NeA (axist_bxist fv x)
    | Pos x -> Pos (BOps.forall fv x)
    | PoA x -> PoA (aforall fv x)
    | Neg x -> Neg (BOps.exist fv x)
    | NeA x -> NeA (axist fv x)

  let neg = function
    | Pos x -> Neg x
    | PoA x -> NeA x
    | Neg x -> Pos x
    | NeA x -> PoA x

  let nexist' ev x = exist' ev x |> neg
  let nforall' fv x = forall' fv x |> neg

  let is_bottom = function
    | Pos b -> is_ff b
    | Neg b -> is_tt b
    | PoA a -> D.is_bottom a
    | NeA a -> D.is_top a

  let union a b = match a, b with
    | a, _ when is_bottom a -> b
    | _, b when is_bottom b -> a
    | Pos a, Pos b -> Pos (a ||~ b)
    | Neg a, Neg b -> Neg (a &&~ b)
    | Pos a, Neg b | Neg b, Pos a -> Pos (b =>~ a)
    (* | NeA a, NeA b -> NeA (D.meet_condition (concr b) a) (\* Not sure meet is ok *\) *)
    | NeA a, NeA b -> NeA (D.meet a b)
    | PoA a, PoA b -> PoA (D.join a b) (* ok as long as no Dom.join is involved *)
    | PoA a, Pos b | Pos b, PoA a -> Pos (concr a ||~ b)
    (* | PoA a, Pos b | Pos b, PoA a -> PoA (D.join a (abstr b)) *)
    | PoA a, Neg b | Neg b, PoA a -> Pos (b =>~ concr a)
    | NeA a, Pos b | Pos b, NeA a -> Pos (concr' a ||~ b)
    | NeA a, Neg b | Neg b, NeA a -> NeA (D.meet_condition b a)
    | NeA a, PoA b | PoA b, NeA a -> Pos (concr' a ||~ concr b)
    (* | PoA a, Neg b | Neg b, PoA a -> PoA (D.join (abstr !~b) a) *)

  let inter a b = neg (union (neg a) (neg b))

  let imply a b = union (neg a) b

  let retr_neg x = neg x |> retr

  (* --- *)

  let pp_repr fmt = function
    | Pos k -> print fmt k
    | PoA k -> D.print fmt k
    | Neg k when P.pp_neg -> fprintf fmt "~(@[%a@])" print k
    | Neg k -> print fmt !~k
    | NeA k when P.pp_neg -> fprintf fmt "~@[%a@]" D.print k
    | NeA k -> print fmt (concr' k)

  let pp_repr' = function
    | [] -> fun fmt _ -> pp_print_char fmt '^'
    | _ -> pp_repr

  let pp_repr'' = function
    | [] -> fun fmt _ -> pp_print_string fmt "~^"
    | _ -> pp_repr

  (**/**)

  let lp fmt = ()
  (* let ls: (unit, formatter, unit) format = "^" *)
  let ls = Util.pc '^'

  let lvars ?(debug = P.debug) ?(lp = lp) ~lv vars =
    Log.log debug logger "%t %(%)  = @[%a@]" lp lv pp_vars vars

  let lres ?(debug = P.debug) ?(info = P.info) ?(lp = lp)
      ?ld ~lop ~lv ?(ls = ls) ppx x =
    let dbg f = Log.log debug logger f and info f = Log.log info logger f in
    begin match ld with
      | None ->
          dbg "%t%(%)%(%)%t = @[%a@]" lp lop lv ls ppx x
      | Some ld when Log.check_level logger debug ->
          dbg "%t %t @[ = %a@ (= %(%)%(%)%t)@]" lp ld ppx x lop lv ls
      | Some ld ->
          info "%t %t @[= %(%)%(%)%t@]" lp ld lop lv ls
    end;
    x

  let exist ?debug ?info ?lp ?ld ?ls ~lv vl x =
    lvars ?debug ?lp ~lv vl;
    Log.roll (exist' vl) x |>
        lres ?debug ?info ?lp ?ld ?ls ~lop:"@<1>∃" ~lv (pp_repr' vl)

  let nexist ?debug ?info ?lp ?ld ?ls ~lv vl x =
    lvars ?debug ?lp ~lv vl;
    Log.roll (nexist' vl) x |>
        lres ?debug ?info ?lp ?ld ?ls ~lop:"@<1>∄" ~lv (pp_repr'' vl)

  let forall ?debug ?info ?lp ?ld ?ls ~lv vl x =
    lvars ?debug ?lp ~lv vl;
    Log.roll (forall' vl) x |>
        lres ?debug ?info ?lp ?ld ?ls ~lop:"@<1>∀" ~lv (pp_repr' vl)

  let nforall ?debug ?info ?lp ?ld ?ls ~lv vl x =
    lvars ?debug ?lp ~lv vl;
    Log.roll (nforall' vl) x |>
        lres ?debug ?info ?lp ?ld ?ls ~lop:"@<2>¬∀" ~lv (pp_repr'' vl)

  (* --- *)

  let rec ith l = function
    | 0 -> l
    | i -> ith (List.tl l) (pred i)

  let par ?(lp = lp) ~ls ~ld ~lv ~lo op vl i lx =
    let vl = ith vl i in
    let lx = ith lx i in
    List.fold_left2
      begin fun (acc, i) v x ->
        Log.d3 logger "%t %(%i%)  = @[%a@]" lp lv i pp_vars v;
        Log.d3 logger "%t %(%i%)  = @[%a@]" lp ls i pp_repr x;
        let x = Log.roll (op v) x in
        Log.d3 logger "%t %(%i%) @[ = @[%a@]@ (= %(%)%(%i%)%(%i%))@]\
                      " lp ld i (pp_repr' v) x lo lv i ls i;
        x :: acc, pred i
      end ([], List.length vl + i - 1) (List.rev vl) (List.rev lx)
    |> fst

  let exist_par vl = par ~lo:"@<1>∃" exist' vl
  let forall_par vl = par ~lo:"@<1>∀" forall' vl

  (* --- *)

  (* type elim_pair = *)
  (*     { *)
  (*       exist_vars: vars; *)
  (*       forall_vars: vars; *)
  (*     } *)

  (* let elim_step exist_vars = { exist_vars; forall_vars = [] } *)
  (* let forall_step forall_vars = { forall_vars; exist_vars = [] } *)
  (* let mk_step ~exist ~forall = { exist_vars = exist; forall_vars = forall } *)

  (* let ls' = ls *)

  (* let forall_exist_altern ?(lp = lp) ?(ls = ls) ~le ~lf fevl i x = *)
  (*   let fevl = ith fevl i in *)
  (*   List.fold_left *)
  (*     begin fun (x, (ls, i)) { exist_vars = ev; forall_vars = fv } -> *)
  (*       Log.d3 logger "%t %(%i%)  = @[%a@]" lp le i pp_vars ev; *)
  (*       let x = Log.roll (exist' ev) x in *)
  (*       Log.d3 logger "%t@<1>∃%(%i%)%t = @[%a@]" lp le i ls (pp_repr' ev) x; *)
  (*       Log.d3 logger "%t %(%i%)  = @[%a@]" lp lf i pp_vars fv; *)
  (*       let x = Log.roll (forall' fv) x in *)
  (*       Log.d3 logger "%t@<1>∀%(%i%)^ = @[%a@]" lp lf i (pp_repr' fv) x; *)
  (*       x, (ls', pred i) *)
  (*     end (x, (ls, List.length fevl + i - 1)) (List.rev fevl) *)
  (*   |> fst *)

  (* let exist_forall_altern ?(lp = lp) ?(ls = ls) ~le ~lf efvl i x = *)
  (*   let efvl = ith efvl i in *)
  (*   List.fold_left *)
  (*     begin fun (x, (ls, i)) { exist_vars = ev; forall_vars = fv } -> *)
  (*       Log.d3 logger "%t %(%i%)  = @[%a@]" lp lf i pp_vars fv; *)
  (*       let x = Log.roll (forall' fv) x in *)
  (*       Log.d3 logger "%t@<1>∀%(%i%)%t = @[%a@]" lp lf i ls (pp_repr' fv) x; *)
  (*       Log.d3 logger "%t %(%i%)  = @[%a@]" lp le i pp_vars ev; *)
  (*       let x = Log.roll (exist' ev) x in *)
  (*       Log.d3 logger "%t@<1>∃%(%i%)^ = @[%a@]" lp le i (pp_repr' ev) x; *)
  (*       x, (ls', pred i) *)
  (*     end (x, (ls, List.length efvl + i - 1)) (List.rev efvl) *)
  (*   |> fst *)

  (* let nxist_forall_altern ?(lp = lp) ?(ls = ls) ~le ~lf efvl i x = *)
  (*   let efvl = ith efvl i in *)
  (*   List.fold_left *)
  (*     begin fun (x, (ls, i)) { exist_vars = ev; forall_vars = fv } -> *)
  (*       Log.d3 logger "%t %(%i%)  = @[%a@]" lp lf i pp_vars fv; *)
  (*       let x = Log.roll (forall' fv) x in *)
  (*       Log.d3 logger "%t@<1>∀%(%i%)%t = @[%a@]" lp lf i ls (pp_repr' fv) x; *)
  (*       Log.d3 logger "%t %(%i%)  = @[%a@]" lp le i pp_vars ev; *)
  (*       let x = Log.roll (nexist' ev) x in *)
  (*       Log.d3 logger "%t@<1>∄%(%i%)^ = @[%a@]" lp le i (pp_repr'' ev) x; *)
  (*       x, (ls', pred i) *)
  (*     end (x, (ls, List.length efvl + i - 1)) (List.rev efvl) *)
  (*   |> fst *)

end

module DefaultElimUtils = ElimUtils (DefaultElimUtilsParams)

(* -------------------------------------------------------------------------- *)
