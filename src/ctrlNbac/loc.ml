(** Localization of source code elements.

    @author Nicolas Berthier *)

open Format
open Lexing

(** Extended position information *)
type t =
    {
      loc_lstart: int;
      loc_lstop: int;
      loc_cstart: int;
      loc_cstop: int;
    }

(** Compares two locations according to their starting lines or else column
    numbers *)
let compare
    { loc_lstart = l1; loc_cstart = c1 }
    { loc_lstart = l2; loc_cstart = c2 }
    =
  if l1 < l2 || (l1 = l2 && c1 < c2) then -1
  else if l1 = l2 && c1 = c2 then 0
  else 1

(** [make start stop] builds a localization structure ranging from [start] to
    [stop]. *)
let make s e =
  {
    loc_lstart = s.pos_lnum;
    loc_lstop = e.pos_lnum;
    loc_cstart = s.pos_cnum - s.pos_bol;
    loc_cstop = e.pos_cnum - e.pos_bol;
  }

(** [print fmt l] pretty-prints [l] onto the formatter [fmt]. *)
let print fmt { loc_lstart; loc_lstop; loc_cstart; loc_cstop; } =
  let same_line = loc_lstart = loc_lstop in
  let same_char = same_line && loc_cstart = loc_cstop in
  if same_char then
    fprintf fmt "line %d, character %d" loc_lstart loc_cstart
  else if not same_line || loc_cstart > loc_cstop then
    fprintf fmt "line %d, characters %d-" loc_lstart loc_cstart
  else
    fprintf fmt "line %d, characters %d-%d" loc_lstart loc_cstart loc_cstop
