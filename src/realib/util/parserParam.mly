/* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution */

%token TK_EOF
%token TK_COMMA TK_COLON TK_EQUAL
%token <string> TK_ID
%token <string> TK_STR

%type <string * (string, string) Mappe.t> param
%type <string * (string list * (string, string) Mappe.t)> param_ordrd
%type <(string, string) Mappe.t> options_opt

%start param
%start param_ordrd
%start options_opt

%%

param:
| TK_ID TK_EOF { ($1,Mappe.empty) }
| TK_ID TK_COLON options TK_EOF { ($1,$3) }

options:
| opt { let (v,e) = $1 in Mappe.add v e Mappe.empty }
| opt TK_COMMA options { let (v,e) = $1 in  Mappe.add v e $3 }

options_opt:
| TK_EOF { Mappe.empty }
| options TK_EOF { $1 }

opt:
| TK_ID { ($1,"") }
| TK_ID TK_EQUAL { ($1,"") }
| TK_ID TK_EQUAL TK_ID { ($1,$3) }
| TK_ID TK_EQUAL TK_STR { ($1,$3) }

param_ordrd:
| TK_ID TK_EOF { ($1, ([], Mappe.empty)) }
| TK_ID TK_COLON options_ordrd TK_EOF { ($1, $3) }

options_ordrd:
| opt_ordrd { match $1 with
    | `Str str -> [ str ], Mappe.empty
    | `Opt (id, v) -> [], Mappe.add id v Mappe.empty }
| opt_ordrd TK_COMMA options_ordrd { match $1 with
    | `Str str -> str :: fst $3, snd $3
    | `Opt (id, v) -> fst $3, Mappe.add id v (snd $3) }

opt_ordrd:
| TK_STR { `Str $1 }
| opt { `Opt $1 }
