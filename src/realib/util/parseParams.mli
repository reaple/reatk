(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)

(** ReaTK main utilities: parsing of command line options *)

type options_t = (string, string) Mappe.t (** option map *)
type param_t = string * options_t (** parameter with options *)
type param_ordrd_t = string * (string list * options_t)
(** parameter with ordered options *)

(** parse a parameter *)
val parse_param : string -> param_t

(** parse a parameter  *)
val parse_param': string -> param_ordrd_t

(** parse a list of options  *)
val parse_options: string -> options_t

(** get option with given default values *)
val get_option : options_t -> options_t -> string -> (string -> 'a) -> 'a
val get_option' : options_t -> string -> (string -> 'a) -> 'a option

(** get option with given default values *)
val get_option': options_t -> string -> (string -> 'a) -> 'a option

exception InvalidBooleanSpecification of string
val bool_option : string -> bool

val string_option : string -> string            (* raise Not_found if given "" *)

exception InvalidRationalSpecification of string
val rational_option: string -> Mpqf.t

(** parse semicolon-separated string list *)
val semicolon_str_to_strlist : string -> string list

(** parse comma-separated string list *)
val comma_str_to_strlist : string -> string list

(** prints a warning message in case of ignored options *)
val check_extra_options
  : Log.logger
  -> string
  -> (string, 'a) Mappe.t
  -> ?optionals: string list
  -> (string, string) Mappe.t
  -> unit
