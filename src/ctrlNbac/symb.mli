(* Default interface documentation is mostly in `symb.ml'. *)
(**)

type symb = private int
include SymbSig.T with type symb := symb
