(** BddApron-based Boolean synthesis *)

open BddapronUtil

(* -------------------------------------------------------------------------- *)

type 'x decomposition =
  [
  | `Auto of CuddUtil.Decomp.algorithm option * int
  | `Manual of 'x list
  ]

module Core: functor (BEnvNS: BDDAPRON_ENV_NS) -> sig
  open BEnvNS.BEnv

  (** Internal type encapsulating data for Boolean synthesis. Its main purpose
      is to help keeping signatures "simple", and easily defining operators. *)
  type boolenv

  (* --- *)

  val make_boolenv
    : ?disable_dynamic_variable_reordering: bool
    -> ?careset_update_period: int
    -> (vars * vars) list
    -> boolenv

  (* --- *)

  val make_controller
    : ?no_reorder: bool
    -> ?no_bnd: bool
    -> ?decomp: bexpr decomposition
    -> evolutions: equs
    -> assertion: bexpr
    -> bexpr
    -> bexpr

  (* --- *)

  val synth
    : boolenv
    -> rp: BddapronReorderPolicy.config
    -> oldassert: bool
    -> evolutions: equs
    -> assertion: bexpr
    -> init: bexpr
    -> ?reach: bexpr option
    -> ?attract: bexpr option
    -> bad: bexpr
    -> bexpr option

end

(* -------------------------------------------------------------------------- *)

module Relational: functor (BEnvNS: BDDAPRON_ENV_NS) -> sig
  module Core: module type of Core (BEnvNS)
  open BEnvNS
  open BEnv

  type varext
  val setup: vars -> varext * BEnvExt.upgrade
  val dispose: ?dirty: bool -> varext -> BEnvExt.upgrade

  val synth
    : varext
    -> be: Core.boolenv
    -> rp: BddapronReorderPolicy.config
    -> oldassert: bool
    -> ?decomp: [ `Disj ]
    -> ?check: bool
    -> evolutions: equs
    -> assertion: bexpr
    -> reachable: bexpr
    -> init: bexpr
    -> bad: bexpr
    -> [ `Controller | `SafeStates ]
    -> bexpr option

end

(* -------------------------------------------------------------------------- *)

(** {2 Triangulation} *)

(** Akin a subtype of boolexpr, for default expression specifications *)
type 'a default_expr =
  [
  | `Bool of 'a boolexpr_t
  | `Order of [ `Ascend | `Descend ]
  ]

val permute_default_expr: int array -> ('a default_expr as 'x) -> 'x

module Triang: functor (BEnvNS: BDDAPRON_ENV_NS) -> sig
  open BEnvNS.BAll

  type eqsys_data

  val mk_eqsys_data
    : ?allow_simplifications: bool
    -> ?allow_careset_updates: bool
    -> bexpr
    -> eqsys_data

  val step
    : eqsys_data
    -> bexpr
    -> var
    -> bexpr default_expr
    -> bexpr * (var * expr)
end
