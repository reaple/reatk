open Ocamlbuild_plugin;;

let make_version_rule name src dest =
  rule name ~deps:[src] ~prod:dest begin fun env _ ->
    Echo ([
      read_file (env src);
      Printf.sprintf "let compile_time = %S\n"
        (Ocamlbuild_pack.My_unix.run_and_open "date +\"%a, %d %b %Y %T %z\"\
                                            " input_line);
      Printf.sprintf "let compile_host = %S\n" (Unix.gethostname ())
    ], env dest)
  end
;;

(* Let's manage the context manually to avoid messing up the namespace: *)
let realib d l =
  let prep s = "src/realib"^s in
  let d = prep d and l = List.rev_map prep l in
  Pathname.define_context d (List.rev (d :: l))
;;

dispatch begin function
  | After_rules ->

      realib "/frontend/nbac"                            [ "/base"; "/infra"; "/util"; ];
      realib "" [ "/engine"; "/core"; "/dftocf"; "/trans"; "/base"; "/infra"; "/util"; ];
      realib "/engine"     [ "/core"; "/dftocf"; "/trans"; "/base"; "/infra"; "/util"; ];
      realib "/trans"                           [ "/core"; "/base"; "/infra"; "/util"; ];
      realib "/core"                                     [ "/base"; "/infra"; "/util"; ];
      realib "/dftocf"                                   [ "/base"; "/infra"; "/util"; ];
      realib "/base"                                              [ "/infra"; "/util"; ];
      realib "/infra"                                                       [ "/util"; ];

      make_version_rule "version" "version.ml.in" "src/version.ml";
      (* make_version_rule "realib.version" "version.ml.in" "src/realib/version.ml"; *)
      (* make_version_rule "ctrln.version" "version.ml.in" "src/ctrlNbac/version.ml"; *)

      (* Use both ml and mli files to build documentation: *)
      rule "ocaml: ml & mli -> odoc"
        ~insert:`top
        ~prod:"%.odoc"
        (* "%.cmo" so that cmis of ml dependencies are already built: *)
        ~deps:["%.ml"; "%.mli"; "%.cmo"]
        begin fun env build ->
          let mli = env "%.mli" and ml = env "%.ml" and odoc = env "%.odoc" in
          let tags =
            (Tags.union (tags_of_pathname mli) (tags_of_pathname ml))
            ++"doc_use_interf_n_implem"++"ocaml"++"doc" in
          let include_dirs = Pathname.include_dirs_of (Pathname.dirname ml) in
          let include_flags =
            List.fold_right (fun p acc -> A"-I" :: A p :: acc) include_dirs [] in
          Cmd (S [!Options.ocamldoc; A"-dump"; Px odoc;
                  T (tags++"doc"++"pp"); S (include_flags);
                  A"-intf"; P mli; A"-impl"; P ml])
        end;

      (* Specifying merge options. *)
      pflag ["ocaml"; "doc"; "doc_use_interf_n_implem"] "merge" (fun s -> S[A"-m"; A s]);

      (* Pass `-open' to ocamldoc as well. *)
      pflag ["ocaml"; "doc"] "open" (fun m -> S [A "-open"; A m]);

      flag ["ocaml"; "doc"] & S [A "-charset"; A "utf-8" ];
      flag ["ocaml"; "compile"; "noassert"] & S [A "-noassert" ];

      if Scanf.sscanf Sys.ocaml_version "%d.%d" (fun maj min -> maj < 4 || min < 3) then begin
        let s = S [A "-ppopt"; A "-DOCAML_VERSION_LT_4_03"] in
        flag ["ocaml"; "compile"] s;
        flag ["ocaml"; "depend"] s;
        flag ["ocaml"; "doc"] s;
      end;
  | _ -> ()
end
