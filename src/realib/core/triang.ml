open Format
open CtrlSpec
open BddapronSynthesis

module Logger = struct
  let level = Log.Debug3
  let logger = Log.mk ~level "t."
end
open Logger

let pp_split_controller env fmt equs_list =
  pp_open_vbox fmt 0;
  let pp_group fmt = fprintf fmt "@[<v>%a@]" (Env.print_equations env) in
  (match equs_list with
    | [] -> ()
    | [equs] -> pp_group fmt equs
    | equs_list ->
        List.fold_left begin fun (first, i) -> function
          | [] -> first, succ i
          | equs -> (if not first then fprintf fmt "@\n";
                    fprintf fmt "#%d: %a" i pp_group equs;
                    false, succ i)
        end (true, 1) equs_list |> ignore);
  pp_close_box fmt ()

(* -------------------------------------------------------------------------- *)

type param =
    {
      allow_careset_updates: bool;
      enable_reorderings: bool;
      enable_dynamic_reorderings: bool;
    }

let make_param
    ?(allow_careset_updates = false)
    ?(enable_reorderings = false)
    ?(enable_dynamic_reorderings = false)
    () =
  {
    allow_careset_updates;
    enable_reorderings;
    enable_dynamic_reorderings;
  }

(** Triangularization with numerical inputs. *)
module Triang (E: Env.T) = struct
  module Sub = BddapronSynthesis.Triang (E.BEnvNS)
  module Elim = DomExt.DefaultElimUtils (Logger) (E)
  open E.BEnvNS.BAll

  let ti_timed = Log.ti_timed ~period:0.1 logger

  (** [compute ~allow_careset_updates cs k] triangularizes the equation system
      encoded by [k] according to U/C groups specifications given by [cs].

      Raises {!Failure} if the lists of controllable variables used to build
      [be] and [pspecs] do not have the same length. *)
  let compute { allow_careset_updates;
                enable_reorderings;
                enable_dynamic_reorderings } cs k =

    let _, _, nb_controllables, _ = nc_vars_info E.env cs in
    let rs =
      if enable_reorderings && enable_dynamic_reorderings then
        CuddUtil.dynamic_reordering_save_n_select E.env.Env.env
          !CuddUtil.Options.reorder_policy
      else
        CuddUtil.dynamic_reordering_save_n_stop E.env.Env.env
    in

    (* --- *)

    let eqsys = Sub.mk_eqsys_data ~allow_careset_updates k in
    let solve_one_eq = Log.roll Sub.step eqsys in
    (* Log.d3 logger "K = %a" print k; *)

    let k, equs_list, _ = foldi_uc_groups_rev
      begin fun i (q, equss, j) { c_vars; bp_specs; bu_vars; nu_vars; u_vars } ->
        let lp fmt = fprintf fmt "#%d: " (succ i) in

        (* Log.d3 logger "%t  @[U = %a,@;C = %a@]\ *)
        (*               " lp pp_vars u_vars pp_vars c_vars; *)

        (* Reverse traversal of the controllable variables of the current group
           to build the corresponding equations and successive substitutions. *)
        let c_vars = List.rev c_vars and bp_specs = List.rev bp_specs in
        let q, equs, j = List.fold_left2 begin fun (q, eqs, j) cv cp ->
          let q, eq = solve_one_eq q cv cp in
          ti_timed "%u/%u" j nb_controllables;
          q, eq :: eqs, succ j
        end (q, [], j) c_vars bp_specs in

        Log.d2 logger "%t  K = %a" lp print q;

        (* Eliminate all non-controllable variables of the current group, unless
           no more variable is to be substituted. *)
        let q =
          if i = 0 || u_vars = [] then q
          else Elim.repr q |> Elim.exist ~lp ~lv:"U" u_vars |> Elim.retr
        in

        if enable_reorderings then
          CuddUtil.reorder_conv E.env.Env.env;

        (q, equs :: equss, j)
      end (k, [], 1) cs
    in

    CuddUtil.dynamic_reordering_restore E.env.Env.env rs;

    equs_list, k

end

(* -------------------------------------------------------------------------- *)

(* TODO: optionally disable (forced/automatic) reordering during
   triangularization... *)
let run params env cs k =

  let module E = (val Env.as_module env) in
  let module T = Triang (E) in
  Log.i logger "Triangularization@<1>…";
  let equs_list, k' = T.compute params cs k in
  Log.d2 logger "@[<h>Triangularized@ controller%a@]:@\n%a"
    (fun fmt -> function [_] -> () | _ -> pp_print_char fmt 's') equs_list
    (pp_split_controller env) equs_list;

  (* --- *)

  equs_list, k'

(* -------------------------------------------------------------------------- *)
