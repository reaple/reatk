(* This file is part of ReaVer released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)

(** framework base: abstract domains *)

exception NotSupported of string (** operation not supported *)

(** {2 Domain Interface } *)

module type T =
  sig
    module Man: BddapronDoman.T

    (** {2 Types } *)

    type t                           (** logico-numerical abstract value type *)
    type num = Man.aprondom
    type numdomain = num ApronUtil.num0     (** numerical abstract value type *)

    (** {2 Domain operations } *)

    (** prints an abstract value *)
    val print : Format.formatter -> t -> unit

    (** returns the abstract value bottom *)
    val bottom : t

    (** returns the abstract value top *)
    val top : unit -> t

    (** canonicalizes the abstract values *)
    val canonicalize : t -> unit

    (** returns true if the given abstract values are equal *)
    val is_eq : t -> t -> bool

    (** returns true if the first abstract value is contained
        in the second one *)
    val is_leq : t -> t -> bool

    (** returns true if the given abstract value is top *)
    val is_top : t -> bool

    (** returns true if the given abstract value is bottom *)
    val is_bottom : t -> bool

    (** joins two abstract values *)
    val join : t -> t -> t

    (** meets two abstract values *)
    val meet : t -> t -> t

    (** meets an abstract value with a (mixed) Boolean formula *)
    val meet_condition : Env.boolexpr_t -> t -> t

    (** widening operator *)
    val widening: t -> t -> t
    type wc_spec = private [> `Default ]
    val widening_force_conv: wc_spec -> (lp: Util.pu -> t -> t -> t * t) option

    (** applies the given discrete transition function (equations)
        in forward direction *)
    val assign_lexpr : t -> Env.equs_t -> t

    (** applies the given discrete transition function (equations)
        in backward direction *)
    val substitute_lexpr : t -> Env.equs_t -> t

    (** existential quantification of the given variables *)
    val forget_list : t -> Env.vars_t -> t

    (** universal quantification of the given Boolean variables *)
    val forall_bool_list : t -> Env.vars_t -> t

    (** applies the given flow relation (continuous equations) *)
    val flow : t -> Env.equs_t -> Env.boolexpr_t -> t

    (** abstract acceleration of the given discrete transition function
        (equations) in the given direction *)
    val accel : ?dir:ApronAccel.direction_t -> t -> Env.equs_t ->
      Env.boolexpr_t -> t


    (** {2 Conversions } *)

    (** converts a numerical abstract value into
        a logico-numerical abstract value *)
    val of_num : numdomain -> t

    (** converts a (mixed) Boolean formula into
        a logico-numerical abstract value *)
    val of_boolexpr : Env.boolexpr_t -> t

    (** converts a list of (Boolean formula, numerical abstract value) into
        a logico-numerical abstract value *)
    val of_boolnumlist : (Env.boolexpr_t * numdomain) list -> t

    (** converts a logico-numerical abstract value into
       a (mixed) Boolean formula *)
    val to_boolexpr : t -> Env.boolexpr_t

    (** converts a logico-numerical abstract value into its negation as a
        (mixed) Boolean formula *)
    val to_boolexpr': t -> Env.boolexpr_t

    (** converts a logico-numerical abstract value into
        a list of (Boolean formula, numerical abstract value) *)
    val to_boolnumlist : t -> (Env.boolexpr_t * numdomain) list

    (** converts a logico-numerical abstract value into
       a (purely) Boolean formula *)
    val to_boolexprbool : t -> Env.boolexpr_t

    (** converts a logico-numerical abstract value into
        a list of (Boolean formula, numerical constraints) *)
    val to_boollinconsslist : t -> (Env.boolexpr_t * ApronUtil.linconss) list

    (** meets the given abstract value with a (mixed) Boolean formula
        and returns the result as a (mixed) Boolean formula *)
    val meet_to_boolexpr : t -> Env.boolexpr_t -> Env.boolexpr_t

    (** meets the given abstract value with a (mixed) Boolean formula
        and returns the result as a (purely) Boolean formula *)
    val meetbool_to_boolexpr : t -> Env.boolexpr_t -> Env.boolexpr_t

    (** {2 Miscellaneous } *)

    (** prints an informal description *)
    val descr: Util.pu

    val numdoman: num Apron.Manager.t

    (** provides access to the underlying environment *)
    module E: Env.T
  end

(** {2 Utilities to build BddApron-based domains } *)

(** Take care that directly applying the following functors forcibly
    instanciates the bddapron managers. Use the functions to build only the
    needed ones at runtime. *)

(** {3 Functors } *)

(** functor type for BddApron-domains *)
module type FUNC =
  functor (Man: BddapronDoman.T) ->
    functor (Env: Env.T) -> (T with module Man = Man
                              and module E = Env)

(** functor for BddApron-based logico-numerical power domain *)
module Pow : FUNC

(** functor for BddApron-based logico-numerical product domain *)
module Prod : FUNC


(** {3 Functions } *)

(** applies the {!Pow} functor to the given base bddapron domain manager *)
val pow: (module BddapronDoman.T) -> (module Env.T) -> (module T)

(** applies the {!Prod} functor to the given base bddapron domain manager *)
val prod: (module BddapronDoman.T) -> (module Env.T) -> (module T)
