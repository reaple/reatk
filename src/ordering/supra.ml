open Format
open CtrlNbac
open AST

module WSDeps = WeightedSymbDeps.SharedBits
module PCG = PCG.Make (Weight.F)
module PCGOfWSDeps = PCG.OfWSDeps (WSDeps) (Weight.FOfI)

let level = Log.Debug3
let logger = Log.mk ~level "Ordering"

(* -------------------------------------------------------------------------- *)

let maybe_output (descr: Realib.Util.ufmt) suff ext pp_graph graph = function
  | Some mk_fmt ->
      let fmt, close = mk_fmt ?descr:(Some descr) ?suff:(Some suff) ext in
      pp_graph fmt graph;
      close ()
  | None -> ()

(* -------------------------------------------------------------------------- *)

let ftyp = function
  | `Bool | #etyp | `Bint _ -> true
  | _ -> false
let state_decl = function
  | `State _ | `State' -> true
  | _ -> false
let input_decl = function
  | `Input _ | `Contr _ -> true
  | _ -> false

type order_subset =
  | State
  | StateNInput

let partition_vars order_subset (decls: _ decls) =
  SMap.fold begin fun v (t, d, _) ((k, r) as acc) ->
    if not (ftyp t) then
      if state_decl d || input_decl d then k, v :: r else k, r
    else
      if state_decl d || input_decl d && order_subset = StateNInput then
        SSet.add v k, r
      else if input_decl d then
        k, v :: r
      else
        acc
  end decls (SSet.empty, [])

(* -------------------------------------------------------------------------- *)

let add_dep v v' m =
  let s = try SMap.find v m with Not_found -> SSet.empty in
  SMap.add v (SSet.add v' s) m

let add_deps v s' m =
  let s = try SMap.find v m with Not_found -> SSet.empty in
  SMap.add v (SSet.union s' s) m

let acc_local_deps v v' m =
  try add_deps v (SMap.find v' m) m with Not_found -> add_dep v v' m

let acc_trans_deps local_deps v v' =
  add_deps v (try SMap.find v' local_deps with Not_found -> SSet.singleton v')

let acc_dep local_deps v s =
  SSet.union (try SMap.find v local_deps with Not_found -> SSet.singleton v) s

(* --- *)

let repr_strong_affinities strong_affinities keep =
  let make_saff_repr (keep, saff) symbs =
    (* Choose placeholder symbol to be used in wsdeps and pcg: *)
    let kept_symbs = SSet.inter symbs keep in
    let placeholder =
      SSet.choose (if SSet.is_empty kept_symbs then symbs else kept_symbs) in
    let away = SSet.remove placeholder symbs in
    SSet.diff keep away, add_deps placeholder away saff
  in
  List.fold_left make_saff_repr (keep, SMap.empty) strong_affinities

(* -------------------------------------------------------------------------- *)

let pp_slist = Realib.Util.pp_lst Symb.print
let pp_sset = Realib.Util.pp_lst ~fopen:"{@[" ~fclose:"@]}" Symb.print

let gather_locals_deps defs =
  let acc_deps m (v, e) = fold_exp_dependencies (acc_local_deps v) e m in
  List.fold_left acc_deps SMap.empty defs

let symb_typ_size enum_sizes decls v =
  match SMap.find v decls with
    | `Bool, _, _ -> 1
    | `Bint (_, w), _, _ -> w
    | `Enum tn, _, _ -> SMap.find tn enum_sizes
    | _ -> 0

let make_wsdeps ?make_log_fmt tf symb_typ_size keep strong_affinities =
  let trans_deps = Lazy.force tf in
  Log.d3 logger "@[State@ variable@ dependencies:@ %a@]"
    (SMap.print SSet.print) trans_deps;
  let wsdeps = WSDeps.make symb_typ_size trans_deps keep strong_affinities in
  maybe_output "relevant@ weighted@ variable@ dependencies" "-deps" "dot"
    WSDeps.ToDot.fprint_graph wsdeps make_log_fmt;
  wsdeps

let pcg_of_wsdeps ?make_log_fmt keep wsdeps =
  WSDeps.filter_vertexes keep wsdeps;
  let pcg = PCGOfWSDeps.convert wsdeps keep in
  maybe_output "PCG" "-pcg" "dot"
    PCG.ToDot.fprint_graph pcg make_log_fmt;
  pcg

let make decls gather_tf ?make_log_fmt
    ?(strong_var_affinities = []) order_subset enum_sizes =
  let keep, rem = partition_vars order_subset decls in
  let keep', strong_affinities =
    repr_strong_affinities strong_var_affinities keep in
  Log.d3 logger "@[Strong@ variable@ affinities@ (map):@ %a@]\
                " (SMap.print SSet.print) strong_affinities;
  let order =
    if SSet.cardinal keep <= 1 then SSet.elements keep else begin
      let symb_typ_size = symb_typ_size enum_sizes decls in
      let wsdeps = make_wsdeps ?make_log_fmt gather_tf symb_typ_size keep
        strong_affinities in
      let pcg = pcg_of_wsdeps ?make_log_fmt keep' wsdeps in
      let module TopoBFS = TopoBFS.Make (PCG) in
      TopoBFS.find_good_order pcg
    end
  in
  (* Reintegrate variables left out due to strong affinities: *)
  let rev_order, rem = List.fold_left begin fun (acc, rem) s ->
    try let symbs = SMap.find s strong_affinities in
        SSet.fold List.cons symbs (s :: acc),
        List.filter (fun s -> not (SSet.mem s symbs)) rem
    with Not_found -> s :: acc, rem
  end ([], rem) order in
  let order = List.rev rev_order in
  Log.d logger "@[Variable@ order:@ %a@]" pp_slist order;
  Log.d3 logger "@[Other@ variables:@ %a@]" pp_sset rem;
  order, rem

(* --- *)

let gather_tf_deps tf local_deps =
  let acc_deps v = fold_exp_dependencies (acc_trans_deps local_deps v) in
  SMap.fold acc_deps tf SMap.empty

let handle_node (node, ni) =
  let ni = match ni with Some ni -> ni | None -> gather_node_info node in
  let nd = node_desc node in
  let decls = (nd.cn_decls :> ('f, 'f var_spec) decls) in
  make decls
    (lazy (gather_tf_deps ni.cni_trans_specs
             (gather_locals_deps (sorted_node_definitions node))))

(* --- *)

let gather_cfg_tf_deps graph loc_var local_deps =
  let open CFG in
  let acc_deps v = fold_exp_dependencies (acc_trans_deps local_deps v) in
  let add_guard_deps gdeps = function
    | Some g -> fold_exp_dependencies (acc_dep local_deps) (`Bexp g) gdeps
    | None -> gdeps
  and acc_update_deps guard_deps v (`State (e, _)) deps =
    acc_deps v e deps |> add_deps v guard_deps
  in
  let acc_arc_deps arc_src { arc_dest; arc_guard; arc_updates } (deps, gdeps) =
    (* propagate guards' dependencies according to CFG's semantics *)
    let gdeps = add_guard_deps gdeps arc_guard in
    let deps = SMap.fold (acc_update_deps gdeps) arc_updates deps in
    if arc_dest = arc_src
    then (deps, gdeps)
    else (add_deps loc_var gdeps deps, gdeps)
  in
  fold_locations begin fun { loc_name; loc_arcs } deps ->
    fold_arcs (acc_arc_deps loc_name) loc_arcs (deps, SSet.empty) |> fst
  end graph SMap.empty

let handle_cfg (cfg, loc_var, loc_spec) =
  let cd = cfg_desc cfg in
  let decls = (cd.cg_decls :> ('f, 'f var_spec) decls) in
  let decls = SMap.add loc_var loc_spec decls in
  make decls
    (lazy (gather_cfg_tf_deps cd.cg_graph loc_var
             (gather_locals_deps (sorted_cfg_definitions cfg))))

(* -------------------------------------------------------------------------- *)

let order_variables = function
  | `Cfg c -> handle_cfg c
  | `Node n -> handle_node n

(* -------------------------------------------------------------------------- *)
