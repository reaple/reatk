(** optimization algorithms implementation *)

open Format

module Logger = struct
  let level = Log.Debug3
  let logger = Log.mk ~level "o."
  let log2 f = Log.cd2 logger f
  let l2 f fmt = log2 f fmt
  let l2_1 f x fmt = log2 f fmt x
  let l2_2 f x y fmt = log2 f fmt x y
  let log3 f = Log.cd3 logger f
  let l3 f fmt = log3 f fmt
  let l3_1 f x fmt = log3 f fmt x
  let l3_2 f x y fmt = log3 f fmt x y
end
open Logger

type pt = formatter -> unit
let ( ++ ) a b = a |> b

(* -------------------------------------------------------------------------- *)

(** {2 One-step Optimization} *)

type one_step_param =
    {
      o1_goals: Env.numexpr_t BddapronOptim.one_step_goal list;
    }

let make_o1_param o1_goals = { o1_goals }

module OneStep (E: Env.T) = struct
  module O = BddapronOptim.OneStep (E.BEnvNS)
  open E.BEnvNS.BEnvExt
  open E.BEnvNS.POps

  let logger = Log.mk ~level "o1"

  type param = one_step_param

  let descr { o1_goals } fmt =
    fprintf fmt "One-step@ optimization@ for@ %a" O.pp_goals o1_goals
  let short_descr _ fmt =
    fprintf fmt "One-step@ optimization"

  (* --- *)

  let maybe_upgrade cs k cfprog eu =
    upgrade CtrlSpec.apply_env_permutation eu cs,
    upgrade BddapronUtil.permute_boolexpr eu k,
    upgrade Program.apply_env_permutation_to_cfprog eu cfprog

  let upgrade_goals gls eu =
    upgrade O.GoalUtils.permute_goals eu gls

  let upgrade_param { o1_goals } perms =
    { o1_goals = upgrade_goals o1_goals (upgrade_of_perms perms) }

  let filter_c_vars subset cs =
    List.fold_left (fun s v -> PSette.add v s) empty_vset (CtrlSpec.c_vars cs) |>
        PSette.inter subset

  let optimize { o1_goals = goals } (cs, a, cfprog) =

    (* Gather dependencies induced by evolutions w.r.t goals. *)
    let subset = O.goals_support cfprog.Program.c_disc_equs goals in

    (* Augment environment with one new (primed) variable per controllable input
       belonging to the dependencies computed above, and then update the
       controller and the data-flow program. *)
    Log.d logger "Extending environment with primed variables.";
    let c_vars = filter_c_vars subset cs in
    let cc'_vmap, oe, eu = make_primes c_vars in
    let cs, a, cfprog = maybe_upgrade cs a cfprog eu
    and goals = upgrade_goals goals eu in

    (* Compute successive goals *)
    let k = List.fold_left begin fun k g ->
      Log.d logger "@[One-step@ %a…@]" O.pp_goal g;
      O.one_step cc'_vmap cfprog.Program.c_disc_equs g k
    end a goals in

    (* Restore environment and alive expressions. *)
    Log.d logger "Restoring environment.";
    let eu' = restore_environment oe in
    let cs, k, cfprog = maybe_upgrade cs k cfprog eu' in

    Log.d2 logger "K = @[%a@]" E.BEnvNS.BOps.print k;

    (cs, k, cfprog), upgrade_to_perms (compose_upgrades eu eu')

end

(* -------------------------------------------------------------------------- *)

(** {2 K-step Optimization} *)

type k_step_goal = Env.numexpr_t BddapronOptim.optim_goal * int * k_step_kind
and k_step_kind =
  [
  | `Kth
  | `Sum
  ]

let pp_koal ?(short = false) pp_e fmt (g, k, i) =
  let v = match g with
    | `Minimize (v, _) -> fprintf fmt "minimization"; v
    | `Maximize (v, _) -> fprintf fmt "maximization"; v
  in
  match i with
    | `Kth when not short -> fprintf fmt "@ of@ %a@ after@ %u@ steps" pp_e v k
    | `Kth -> fprintf fmt "@ after@ %u@ steps" k
    | `Sum when not short -> fprintf fmt "@ of@ sum-%u(%a)" k pp_e v
    | `Sum -> fprintf fmt "@ of@ sum-%u(@<2>…)" k

type k_variant =
  [
  | `Strict
  | `Pessimistic
  ]

let pp_k_variant fmt = function
  | `Strict -> fprintf fmt "strict"
  | `Pessimistic -> fprintf fmt "pessimistic"

type k_step_param =
    {
      ok_goal: k_step_goal;
      ok_variant: k_variant;
      ok_be: bool;
      ok_dirty: bool;
      ok_cudd_dr: bool;
    }

let make_ok_goal g k i = (g, k, i)
let make_ok_param ?(dirty = false) ?(cudd_dr = true) ?(best_effort = false)
    ok_variant ok_goal =
  { ok_goal; ok_variant; ok_be = best_effort;
    ok_dirty = dirty; ok_cudd_dr = cudd_dr; }

module type KSTEP_PARAMS = sig
  val cudd_dr: bool
end

module KStepDefaultParams: KSTEP_PARAMS = struct
  let cudd_dr = true
end

module KStep (E: Env.T) = struct
  module GoalUtils = BddapronOptim.GoalUtils (E.BEnvNS)
  module Logger = struct let logger = Log.mk ~level "oL" end
  module UnfoldPrefix =
    Unfold.Prefix (DomExt.DefaultElimUtilsParams) (Logger) (E)
  open UnfoldPrefix
  open Logger
  open Elim
  open E.BEnvNS
  open E.BEnvNS.BAll
  open E.BEnvNS.NOps
  open E.BEnvNS.BEnvExt

  type param = k_step_param

  let pp_goal = pp_koal ~short:false pp_nexpr
  let pp_goal' = pp_koal ~short:true pp_nexpr

  let descr { ok_goal; ok_variant } fmt =
    fprintf fmt "k-step@ optimization@ for@ %a@ %a\
                " pp_k_variant ok_variant pp_goal ok_goal
  let short_descr { ok_goal; ok_variant } fmt =
    fprintf fmt "k-step@ optimization@ for@ %a@ %a\
                " pp_k_variant ok_variant pp_goal' ok_goal

  (* --- *)

  let maybe_upgrade cs k cfprog eu =
    upgrade CtrlSpec.apply_env_permutation eu cs,
    upgrade BddapronUtil.permute_boolexpr eu k,
    upgrade Program.apply_env_permutation_to_cfprog eu cfprog

  let upgrade_goal (g, k, i) eu =
    (upgrade GoalUtils.permute_goal eu g, k, i)

  let upgrade_param ({ ok_goal } as param) perms =
    { param with ok_goal = upgrade_goal ok_goal (upgrade_of_perms perms) }

  (* --- *)

  let gather_primed f pvars lst =
    List.flatten @@ List.rev_map f lst |>
        List.rev_map (fun v -> v, PMappe.find v pvars)
  let gather_primed_g = gather_primed (fun (g, _) -> PSette.elements g)
  let gather_primed_u = gather_primed (fun (_, u) -> PSette.elements u)

  (* --- *)

  let compute_r1 ucks pvars variant (rel, relpp) ek ak ais =

    let n = List.length ucks in

    let gg' = gather_primed_g pvars ucks in
    Log.d3 logger "  Γ  = ⋃_i∈[1,%d]{Γi}" n;

    let uu' = match variant with
      | `Strict ->
          Log.d3 logger "  Υ  = ∅"; []
      | `Pessimistic ->
          Log.d3 logger "  Υ  = %(%)_i∈[2,%d]{Υi}" "@<1>⋃" n;
          gather_primed_u pvars (List.tl ucks)
    in
    Log.d3 logger "  I  = Γ∪Υ";

    let prime_gamma   = subst_vars gg'  in
    let prime_upsilon = subst_vars uu'  in
    (* let prime_inputs  = subst_vars (List.rev_append gg' uu') in *)

    let _prime_gamma_b b = of_bexpr b |> prime_gamma  |> to_bexpr in
    let _prime_gamma_n b = of_nexpr b |> prime_gamma  |> to_nexpr in
    let _prime_upsilon_b b = of_bexpr b |> prime_upsilon |> to_bexpr in
    let _prime_upsilon_n b = of_nexpr b |> prime_upsilon |> to_nexpr in
    (* let _prime_inputs_b b = of_bexpr b |> prime_inputs |> to_bexpr in *)
    (* let _prime_inputs_n b = of_nexpr b |> prime_inputs |> to_nexpr in *)

    let g' = List.split gg' |> snd in
    let u' = List.split uu' |> snd in

    (* --- *)

    let rc () =                                                     (* strict *)
      let ak' = _prime_gamma_b ak in Log.d3 logger " Aκ' = Aκ[Γ/Γ']";
      let ek' = _prime_gamma_n ek in Log.d3 logger " Eκ' = Eκ[Γ/Γ']";
      let ak = repr ak and ak' = repr ak' in
      let lr = Util.ps "Rc" and lr1 = Util.ps "Rc1" in

      let r = Log.roll (rel ek') ek |> repr in
      Log.d logger " Rc0@[ %t= %(%(%)%(%)%)%t@]"
        (fun f -> log3 "= @[%a@]@ (" f pp_repr r)
        relpp "@<8>Eκ[Γ/Γ']" "@<2>Eκ"
        (log3 ")");

      if is_bottom r then (Log.i logger " %t  = ∅ (surely)" lr; r) else
        let r = Log.roll (fun r -> imply ak (inter ak' r)) r in
        Log.d logger " Rc1@[ %t= Aκ ⇒ Aκ[Γ/Γ'] ∩ Rc0%t@]"
          (fun f -> log3 "= @[%a@]@ (" f pp_repr r)
          (log3 ")");
        let r = nexist ~ld:lr ~lv:"@<2>Γ'" g' ~ls:lr1 r in
        Log.d logger "@<7>α(Rc)= %a" D.print (aretr r);
        r
    in

    (* --- *)

    let rp () =                                                (* pessimistic *)
      let ak' = _prime_upsilon_b ak in Log.d3 logger " Aκ\" = Aκ[Υ/Υ']";
      let ek' = _prime_upsilon_n ek in Log.d3 logger " Eκ\" = Eκ[Υ/Υ']";
      let ak = repr ak and ak' = repr ak' in
      let lr = Util.ps "Rp" and lr1 = Util.ps "Rp1" in

      let r = Log.roll (rel ek') ek |> repr in
      Log.d logger " Rp0@[ %t= %(%(%)%(%)%)%t@]"
        (fun f -> log3 "= @[%a@]@ (" f pp_repr r)
        relpp "@<8>Eκ[Υ/Υ']" "@<2>Eκ"
        (log3 ")");

      if is_bottom r then (Log.i logger " %t  = ∅ (surely)" lr; r) else
        let r = Log.roll (fun r -> imply ak (inter ak' r)) r in
        Log.d logger " Rp1@[ %t= Aκ ⇒ Aκ[Υ/Υ'] ∩ Rp0%t@]"
          (fun f -> log3 "= @[%a@]@ (" f pp_repr r)
          (log3 ")");
        let r = nexist ~ld:lr ~lv:"@<2>Υ'" u' ~ls:lr1 r in
        Log.d logger "@<7>α(Rp)= %a" D.print (aretr r);
        r
    in

    (* --- *)

    let rk, rkfmt = match variant with
      | `Strict -> rc (), "Rc"
      | `Pessimistic -> let rc = rc () in union rc (rp ()), "Rc∪Rp"
    in
    if Log.check_level logger Log.Debug2
    then Log.d logger " @<3>Rκ @[ = %a@ (= %s)@]" pp_repr rk rkfmt
    else Log.d logger " @<6>Rκ  = %s" rkfmt;
    (* Log.d2 logger "@<6>α(R)= %a" D.print (aretr r); *)

    let prefixc1 = prefixcj ucks ais 1 in
    let r1 = prefixc1 ~ld:(Util.pp "R1") rk in
    r1

  (* --- *)

  let optimize
      { ok_goal = (g, k, kind); ok_variant; ok_be; ok_dirty }
      (cs, asser, cfprog) =

    let e = BddapronOptim.goal g in
    Log.d3 logger "κ = %d" k;

    let avars = BOps.support asser in
    let gvars = POps.support (of_nexpr e) in
    let varsreq = setup_kvars cfprog.Program.c_disc_equs avars gvars k in
    let varsreq = L.prime_cvars cs varsreq in
    let cvarsreq = varsreq and varsreq = match ok_variant with
      | `Pessimistic -> L.prime_uvars cs varsreq
      | _ -> varsreq
    in

    let (cs, k, cfprog), eu = if PMappe.is_empty cvarsreq then begin
      Log.i logger "@[Non-controllable@ lookahead:@ skipping@ optimization@ \
                      step.@]";
      (cs, asser, cfprog), no_upgrade
    end else begin
      Log.d logger "@[Extending@ environment@ with@ extra@ state@ and@ primed@ \
                      variables.@]";
      let kvars, pvars, oe, eu = decl_vars varsreq in
      let cs, a, cfprog = maybe_upgrade cs asser cfprog eu
      and e = upgrade BddapronUtil.permute_numexpr eu e in
      let evols = cfprog.Program.c_disc_equs in
      let evols = simplify_equs a evols in
      assert (CuddUtil.fullcheck BEnv.env);

      let ucks = uc_vars cs kvars in

      CuddUtil.stop_dynamic_reordering BEnv.env;
      let ais = mkai kvars evols a (List.length ucks - 1) in
      let ak = List.fold_left (&&~) tt ais in
      let ais = List.map repr ais in
      Log.d2 logger "@<7> Aκ  = @[%a@]" pp_bexpr ak;
      CuddUtil.start_dynamic_reordering BEnv.env;

      assert (List.length ucks == List.length ais);
      assert (CuddUtil.fullcheck BEnv.env);

      let mk_ek, pp = match kind with
        | `Kth -> (L.lookahead_nexpr kvars evols k,
                  fun fmt -> fprintf fmt "%a|%d" pp_nexpr e k)
        | `Sum -> (L.sum_nexpr kvars evols add zero k,
                  fun fmt -> fprintf fmt "@<7>Σ_i∈[1,%d@<2>]{%a|i}" k pp_nexpr e)
      in
      let ek = Log.roll mk_ek e ^^.. ak in
      assert (CuddUtil.fullcheck BEnv.env);
      Log.d2 logger "@<4> Eκ @[ = @[%a@]@ (= %t)@]" pp_nexpr (ek ^^.. ak) pp;
      assert (CuddUtil.fullcheck BEnv.env);

      let rel = GoalUtils.relation_of_goal g in
      let r1 = compute_r1 ucks pvars ok_variant rel ek ak ais in

      (* CuddUtil.disable_dynamic_reordering BEnv.env; *)
      let a = repr a in
      let k = Log.roll (inter a) r1 in

      let lku = Util.ps "Ku" and lk = Util.pc 'K' and lk' = Util.ps "K'" in
      let k, _kf = match ok_be with
        | false ->(Log.d logger "  %t @[%t= A@ @<1>∩@ R1%t@]" lk
                    (fun f -> log3 " = %a@ (" f pp_repr k) (log3 ")");
                  k, lk)
        | true -> (Log.d logger "  %t @[%t= A@ @<1>∩@ R1%t@]" lk'
                    (fun f -> log3 " = %a@ (" f pp_repr k) (log3 ")");
                  Log.i logger "@[Computing@ best-effort@ controller@<1>…@]";
                  mkbe ucks ais ~ld:lk a ~lku ~lk:lk' k)
      in
      let k = retr k in
      (* CuddUtil.enable_dynamic_reordering BEnv.env; *)

      if ok_dirty then begin
        Log.d logger "@[Leaving@ environment@ as@ is@ as@ requested@ \
                       (dirty=true).@]";
        (cs, k, cfprog), eu
      end else begin
        Log.d logger "@[Restoring@ environment.@]";
        let eu' = restore_environment oe in
        maybe_upgrade cs k cfprog eu', compose_upgrades eu eu'
      end
    end in

    let k = simplify k in

    let ok_ppa = true in
    let ka = Log.roll D.of_boolexpr k in
    if ok_ppa then Log.d logger "@<8>α℘(K) = %a" D.print ka;
    let k = Log.roll D.to_boolexpr ka in

    (cs, k, cfprog), upgrade_to_perms eu

end

(* -------------------------------------------------------------------------- *)

(** {2 Discounted Optimization} *)

type d_param =
    {
      od_goal: Env.numexpr_t BddapronOptim.optim_goal;
      od_discount: discount;                               (* discount factor *)
      od_ustoch: ustoch_spec option;
      od_approx: bool;
      od_dthr: discount option;
      od_kmax: int option;
      od_dirty: bool;
      od_rp: BddapronReorderPolicy.config;
    }
and discount = Mpqf.t
and ustoch_spec =
    {
      up_inputs: Env.vset;
      up_spec: probability_spec;
    }
and probability_spec =
  [
  | `Samples of Env.numexpr_t
  ]

let pp_ustoch pp_e fmt = function
  | `Samples e -> fprintf fmt "input@ samples@ matrix@ %a" pp_e e

let make_od_param ?(dirty = false) ?(approx = false) ?dthr ?kmax ~rp
    od_discount ?ustoch od_goal =
  { od_goal; od_discount; od_approx = approx; od_dthr = dthr; od_kmax = kmax;
    od_ustoch = ustoch; od_dirty = dirty; od_rp = rp; }

module Discounted (E: Env.T) = struct
  module OneStep = BddapronOptim.OneStep (E.BEnvNS)
  module Logger = struct let logger = Log.mk ~level "od" end
  module Elim = DomExt.DefaultElimUtils (Logger) (E)
  module RP = BddapronReorderPolicy.Make (Logger) (E.BEnvNS.BEnv)
  open Logger
  open Elim
  open E.BEnvNS
  open E.BEnvNS.BAll
  open E.BEnvNS.NOps
  open E.BEnvNS.BEnvExt

  type param = d_param

  let pp_goal = OneStep.pp_goal
  let descr { od_goal; od_discount; od_approx; od_ustoch } fmt =
    if od_approx then fprintf fmt "approx.@ ";
    fprintf fmt "%a-discounted@ %a" Mpqf.print od_discount pp_goal od_goal;
    match od_ustoch with
      | None -> ()
      | Some _ -> fprintf fmt "@ with@ stochastic@ inputs"
  let short_descr = descr

  (* --- *)

  let maybe_upgrade cs k cfprog eu =
    upgrade CtrlSpec.apply_env_permutation eu cs,
    upgrade BddapronUtil.permute_boolexpr eu k,
    upgrade Program.apply_env_permutation_to_cfprog eu cfprog

  let upgrade_goal g eu = upgrade OneStep.GoalUtils.permute_goal eu g

  let upgrade_prob = function
    | Some ({ up_spec = `Samples e } as x) -> fun eu ->
      Some ({ x with
        up_spec = `Samples (upgrade BddapronUtil.permute_numexpr eu e) })
    | None -> fun eu -> None

  let upgrade_param ({ od_goal; od_ustoch } as param) perms =
    let ue = upgrade_of_perms perms in
    { param with
      od_goal = upgrade_goal od_goal ue;
      od_ustoch = upgrade_prob od_ustoch ue; }

  (* ------------------------------------------------------------------------ *)

  module XOps = BddapronUtil.XOps (BEnv)
  module Convex = BddapronUtil.CTrv (E.BEnvNS)
  module XNum = Convex.XNum (XOps)
  open XOps

  let pp_xexpr = print
  (* let pp_xexpr_leaves fmt e = *)
  (*   Bddapron.AerexprDD.fold_values (fun l () -> fprintf fmt "%a,@ " XArith.print l) *)
  (*     e () *)

  let pp lo lv c = function
    | [] -> fun _ x -> x
    | v -> fun ppe x -> Log.d logger "%(%)%(%)%s^%t" lo lv c (l3_2 " = %a" ppe x); x

  (* --- *)

  type opname = (unit, formatter, unit) format
  type vsetname = (unit, formatter, unit) format

  (* --- *)

  let filter_vars subset vars =
    List.filter (fun v -> PSette.mem v subset) vars |> varset
  let gather_vars vars pvars =
    List.fold_left (fun acc v -> try (v, PMappe.find v pvars) :: acc with
      | Not_found -> acc) [] vars
  let bsubst_vars vars e = vsubst_vars e vars
  let nsubst_vars vars e = POps.of_nexpr e |> subst_vars vars |> to_nexpr
  let xsubst_vars vars =
    let vars = List.rev_map (fun (v, v') -> v, POps.var v') vars in
    fun e -> e //.~ vars

  (* --- *)

  type order_op =
      {
        name: opname;
        xcmp: bexpr binop;
        xtop: Bddapron.AERDom.t;
        xbot: Bddapron.AERDom.t;
        epsilon_truth: bexpr;
      }

  let min epsilon_truth : order_op =
    {
      name = "min_";
      xcmp = (<.~);
      xtop = Bddapron.AERDom.minus_inf;
      xbot = Bddapron.AERDom.plus_inf;
      epsilon_truth;
    }

  let max epsilon_truth : order_op =
    {
      name = "max_";
      xcmp = (>.~);
      xtop = Bddapron.AERDom.plus_inf;
      xbot = Bddapron.AERDom.minus_inf;
      epsilon_truth;
    }

  (* Combined min/max w.r.t given order *)
  let best { xcmp = (>~<); epsilon_truth } x y =
    XOps.ite ((x >~< y) ^~ epsilon_truth) x y

  module type EPSILON = sig
    val var: var
    val truth: bexpr
    val restore_conds: unit -> upgrade
  end

  module type ORDER = sig
    val improve: order_op
    val degrade: order_op
    val pp_ord: Format.formatter -> unit
    val epsilon: var
  end

  module Min (E: EPSILON) : ORDER = struct
    let improve = min E.truth
    let degrade = max E.truth
    let pp_ord = Util.pc '<'
    let epsilon = E.var
  end

  module Max (E: EPSILON) : ORDER = struct
    let improve = max E.truth
    let degrade = min E.truth
    let pp_ord = Util.pc '>'
    let epsilon = E.var
  end

  (* ------------------------------------------------------------------------ *)

  module type LEAF_FILTER = sig val apply: xexpr -> xexpr end

  (* ------------------------------------------------------------------------ *)

  module type UT = sig
    module Order: ORDER
    type t
    val to_xexpr: t -> xexpr
    val of_xexpr: xexpr -> t
    val print: t pp
    val ddinfos: t -> int * float
    val guard_xexpr: bexpr -> xexpr -> t -> t
    val top: t
    val bot: t
    val elim_supp: order_op -> support -> t -> t
    val fold_guardleaves: (bexpr -> xexpr -> 'a -> 'a) -> t -> 'a -> 'a
    val accum: factor: Mpqf.t -> xexpr -> t -> t
    val avg_supp: support -> xexpr -> t -> t
    val pre: (* order_op ->  *)bexpr -> equs -> t -> t
    val rename_vars: (var * var) list -> t -> t
    val support: t -> vset
    val permute: int array -> t -> t
    type _ repr =
      | Bexpr: bexpr repr
      | Vexpr: bexpr Cudd.Mtbddc.t repr
      (* | Bvbdd: bool Cudd.Vdd.t Cudd.Mtbddc.t repr *)
    (* val cmp: order_op -> bexpr -> t -> t -> 'a repr -> 'a *)
    val cmp_n_elim
      : ?lp: Util.pu -> order_op -> supp: support -> bexpr -> t -> t -> 'a repr -> 'a
    (* val cmp': order_op -> t -> t -> bexpr *)
    val ( ^. ): t -> bexpr -> t
  end

  module MtbddU (LF: LEAF_FILTER) (O: ORDER) : UT with module Order = O = struct

    module Order = O
    open Bddapron
    open Cudd.Mtbddc

    let xtop = of_arith (Aerexpr.cst' Order.improve.xtop)
    let xbot = of_arith (Aerexpr.cst' Order.improve.xbot)

    (* --- *)

    (* Representation separating finite and interpreted variables in guards,
       using MTBDDs *)
    type t = xexpr Cudd.Mtbddc.t

    let ( =. ) a b = a == b || Cudd.Vdd.is_equal a b
    let ( <>. ) a b = not (a =. b)

    let print: formatter -> t -> unit = print pp_bexpr pp_xexpr
    let ddinfos x = size x, nbpaths x
    let tbl = make_table ~hash:Hashtbl.hash ~equal:( =.~ )
    let mk_xexpr_leaf: xexpr -> t = cst BEnv.env.Bdd.Env.cudd tbl
    let mk_xexpr_u: xexpr unique -> t = cst_u BEnv.env.Bdd.Env.cudd

    let uniq = unique tbl
    let u_nan = uniq nan
    let u_zero = uniq zero
    let u_top = uniq xtop
    let u_bot = uniq xbot

    let zero = mk_xexpr_u u_zero
    let nan = mk_xexpr_u u_nan
    let top = mk_xexpr_u u_top
    let bot = mk_xexpr_u u_bot

    let guard_xexpr g n = ite g (mk_xexpr_leaf n)

    let to_xexpr: t -> xexpr = fun x -> fold_guardleaves XOps.ite x xbot

    let of_xexpr: xexpr -> t = fun x ->
      let x' = XNum.fold_numconvex tt begin fun (acc: t) e c ->
        fold_u begin fun g l (acc: t) ->
          let l = Cudd.Mtbdd.cst_u BEnv.env.Bdd.Env.cudd l in
          (* match eval_cst acc g with *)
          (*   | Some u when is_cst u ->                              (\* shortcut *\) *)
          (*       guard_xexpr g (XOps.ite c l (dval u)) acc *)
          (*   | _ -> *)
          fold_guardleaves (fun g' u (acc: t) ->
            guard_xexpr (g &&~ g') (XOps.ite c l u) acc)
            (constrain acc g) acc
        end e acc
      end bot x in
      (* Log.i logger "xxx = %a" print x'; *)
      x'

    let fold_guardleaves: (bexpr -> xexpr -> 'a -> 'a) -> t -> 'a -> 'a = fold_guardleaves

    let elim_supp oo supp : t -> t =
      if is_tt supp then fun t -> t else begin
        let xtop = of_arith (Aerexpr.cst' oo.xtop)
        and xbot = of_arith (Aerexpr.cst' oo.xbot) in
        let top = mk_xexpr_leaf xtop and bot = mk_xexpr_leaf xbot in
        let merge =
          Cudd.User.make_op2
            ~memo:(Cudd.Memo.Hash (Cudd.Hash.create 2))
            ~commutative:false
            ~idempotent:true
            ~special:(fun x y ->
              if x =. top || y =. bot then Some x
              else if y =. top || x =. bot then Some y
              else None)
            (fun x y -> best oo (get x) (get y) (* |> LF.apply *) |> uniq)
        in
        let elim =
          Cudd.User.make_exist
            ~memo:(Cudd.Memo.Hash (Cudd.Hash.create 2))
            merge
        in
        fun t -> let t = Cudd.User.apply_exist elim ~supp t in
              Cudd.User.clear_op2 merge;
              Cudd.User.clear_exist elim;
              t
      end

    let avg_supp supp : xexpr -> t -> t =
      let xzero = Cudd.Mtbdd.dval_u XOps.zero in
      let two = XOps.csti 2 in
      let memo1 = Cudd.Memo.Hash (Cudd.Hash.create 2)
      and memo2 = Cudd.Memo.Hash (Cudd.Hash.create 2)
      and memo3 = Cudd.Memo.Hash (Cudd.Hash.create 2) in
      let merge =
        Cudd.User.make_op2 ~memo:memo1
          (* ~memo:(Cudd.Memo.Cache (Cudd.Cache.create 2)) *)
          ~commutative:true
          ~idempotent:true
          ~special:(fun x y ->
            if x =. nan then Some y
            else if y =. nan then Some x
            else None)
          (fun x y ->
            if x == u_nan then y
            else if y == u_nan then x
            else ((get x +.~ get y) /.~ two (* |> LF.apply *) |> uniq))
      in
      let elim = Cudd.User.make_exist ~memo:memo2 merge in
      fun e x ->
        let ex = Cudd.User.map_op2 ~memo:memo3
          (* ~memo:(Cudd.Memo.Cache (Cudd.Cache.create 2)) *)
          ~commutative:false
          ~idempotent:false
          ~special:(fun (e: xexpr) (x: t) ->
            if e =.~ XOps.zero then Some nan
            else if e =.~ XOps.one then Some x
            else None)
          (fun (e: XArith.t Cudd.Mtbdd.unique) (x: xexpr unique) ->
            if e == xzero then u_nan
            else (get x *.~ of_arith (Cudd.Mtbdd.get e) (* |> LF.apply *) |> uniq))
          e x
        in
        (* Log.i logger "ex = %a" print ex; *)
        let t = Cudd.User.apply_exist elim ~supp ex in
        Cudd.User.clear_op2 merge;
        Cudd.User.clear_exist elim;
        (* Log.i logger "t = %a" print t; *)
        t

    (* XXX: second argument xexpr MUST NOT have any arithmethic condition in its
       guards *)
    let accum ~factor : xexpr -> t -> t =
      let factor = cstm factor in
      let memo1 = Cudd.Memo.Cache (Cudd.Cache.create 2) in
      fun e q ->
        Cudd.User.map_op2 ~memo:memo1
          (* ~memo:(Cudd.Memo.Hash (Cudd.Hash.create 2)) *)
          ~commutative:false
          ~idempotent:false
          ~special:(fun (e: xexpr) (x: t) ->
            if e =.~ XOps.zero then Some x
            else None)
          (fun (e: XArith.t Cudd.Mtbdd.unique) (x: xexpr unique) ->
            let x = if factor =.~ one then get x else factor *.~ get x in
            x +.~ of_arith (Cudd.Mtbdd.get e) (* |> LF.apply *) |> uniq)
          e q

    let mk_xexpr_leaf' x = LF.apply x |> mk_xexpr_leaf

    let pre assertion evols : t -> t =
      let memo1 = Cudd.Memo.Cache (Cudd.Cache.create 1)
      (* and memo2 = Cudd.Memo.Cache (Cudd.Cache.create 1) *)
      and memo3 = Cudd.Memo.Cache (Cudd.Cache.create 1) in
      fun q ->
        let qsupp = support q in
        let (comp, _map) as subst = Expr0.O.compose_of_subst
          BEnv.env BEnv.cond (Expr0.O.Support qsupp) evols in
        let comp = match comp with
          | None -> fun g x -> g, x
          | Some c -> fun g x -> (Cudd.Bdd.vectorcompose ~memo:memo1 c g,
                              Cudd.Mtbdd.vectorcompose ~memo:memo1(* 2 *) c x)
        in
        let parsubst = AerexprDD.parsubstdd' XOps.table BEnv.env ~memo:memo3 in
        let q' = fold_guardleaves begin fun g (x: xexpr) ->
          let g, x = comp g x in
          (* let g = g &&~ assertion in *)
          fold (fun g' l acc -> ite (g &&~ g') (mk_xexpr_leaf' (of_arith l)) acc)
            (parsubst x subst)
        end q bot |> guard_xexpr !~assertion xbot in
        q'

    let pick_leaf q = cst_u BEnv.env.Bdd.Env.cudd (pick_leaf_u q)

    (* Side effect: Modifies CUDD's varmap *)
    let rename_vars vv' : t -> t =
      let to_prime, _of_prime =
        POps.setup_fast_substs'' (function
          | `P p -> `P (Cudd.Bdd.varmap p)
          | `X x -> `X (Cudd.Mtbdd.varmap x)
          | `T u -> `T (varmap u))
          vv'
      in
      let _to_prime_p x = match to_prime (`P x) with `P x -> x | _ -> assert false
      and to_prime_x x = match to_prime (`X x) with `X x -> x | _ -> assert false
      and to_prime_t x = match to_prime (`T x) with `T x -> x | _ -> assert false
      and vv'm = List.fold_left (fun m (v, v') -> PMappe.add v v' m) empty_vmap vv'
      and vs = List.fold_left (fun s (v, _) -> PSette.add v s) empty_vset vv' in
      fun q ->
        fold_guardleaves begin fun g (x: xexpr) ->
          let x' = AerexprDD.map_values XOps.table begin fun a ->
            if PSette.is_empty (PSette.inter (XArith.support a) vs)
            then raise Exit
            else Aerexpr.substitute_by_var BEnv.env.Bdd.Env.symbol a vv'm
          end (to_prime_x x) in
          guard_xexpr g x'
        end q (pick_leaf q) |> to_prime_t

    let permute p q =
      fold_guardleaves (fun g x ->
        guard_xexpr (BOps.permute p g) (XOps.permute p x))
        q (pick_leaf q)

    let support q =
      Array.fold_left (fun acc x -> PSette.union acc (XOps.support x))
        (BOps.support (support q))
        (leaves q)

    let ( ^. ) q careset = tdrestrict q careset

    (* --- *)

    let tt = Cudd.Vdd.cst BEnv.env.Bdd.Env.cudd true
    and ff = Cudd.Vdd.cst BEnv.env.Bdd.Env.cudd false
    let guardb g = Cudd.Vdd.ite g tt ff
    let of_guardb g : bexpr = Cudd.Vdd.guard_of_leaf g true


    (* --- *)

    let tbl'' = make_table ~hash:Hashtbl.hash ~equal:Cudd.Bdd.is_equal
    let grd'' = cst BEnv.env.Bdd.Env.cudd tbl''
    let tt'' = grd'' BOps.tt
    let ff'' = grd'' BOps.ff

    let of_guardb'' g : bexpr =
      Cudd.Mtbddc.fold_guardleaves (fun g l -> (||~) (g &&~ l)) g BOps.ff

    let existb'' ~supp : bexpr Cudd.Mtbddc.t -> bexpr Cudd.Mtbddc.t =
      let elim_in_leaves =
        Cudd.User.make_op1
          (* ~memo:(Cudd.Memo.Hash (Cudd.Hash.create 1)) *)
          ~memo:(Cudd.Memo.Cache (Cudd.Cache.create 1))
          (fun a -> get a |> exist' supp |> unique tbl'')
      and disjbop' =
        Cudd.User.make_op2
          ~memo:(Cudd.Memo.Hash (Cudd.Hash.create 2))
          (* ~memo:(Cudd.Memo.Cache (Cudd.Cache.create 2)) *)
          ~commutative:true
          ~idempotent:true
          ~special:(fun a b ->
            if is_cst a && a =. tt'' then Some a
            else if is_cst b && b =. tt'' then Some b
            else None)
          (fun a b -> get a ||~ get b |> unique tbl'')
      in
      let exist = Cudd.User.make_existop1
        ~memo:(Cudd.Memo.Cache (Cudd.Cache.create 2))
        ~op1:elim_in_leaves
        disjbop'
      in
      fun a ->
        let a = Cudd.User.apply_existop1 exist ~supp a in
        Cudd.User.clear_op1 elim_in_leaves;
        Cudd.User.clear_op2 disjbop';
        Cudd.User.clear_existop1 exist;
        a

    (* --- *)

    type _ repr =
      | Bexpr: bexpr repr
      | Vexpr: bexpr Cudd.Mtbddc.t repr
      (* | Bvbdd: bool Cudd.Vdd.t Cudd.Mtbddc.t repr *)

    (* let cmp: *)
    (* type r. order_op -> bexpr -> t -> t -> r repr -> r = fun oo -> *)
    (*   (\* let xtop = of_arith (Bddapron.Aerexpr.cst' oo.xtop) *\) *)
    (*   (\* and xbot = of_arith (Bddapron.Aerexpr.cst' oo.xbot) in *\) *)
    (*   (\* let top = mk_xexpr_leaf xtop and bot = mk_xexpr_leaf xbot in *\) *)
    (*   fun a b c -> *)
    (*     let d = ref 0 in *)
    (*     Log.d2 logger "@[Setting@ up@ the@ guard@<1>…@]"; *)
    (*     let a = guardb a in *)
    (*     Log.d logger "@[Please@ bear@ with@ me@<1>…@]"; *)
    (*     let q = Cudd.User.map_op3 *)
    (*       ~memo:(Cudd.Memo.Hash (Cudd.Hash.create 3)) *)
    (*       ~special:(fun (a: bool Cudd.Vdd.t) (x: t) (y: t) -> *)
    (*         if !d = 0 then *)
    (*           Log.ti logger "%u/%u/%u… " *)
    (*             (Cudd.Vdd.size a) (Cudd.Mtbddc.size x) (Cudd.Mtbddc.size y); *)
    (*         incr d; *)
    (*         if !d >= 1024 * 8 then d := 0; *)
    (*         if Cudd.Vdd.is_cst a && not (Cudd.Vdd.dval a) || x ==. y then Some ff' *)
    (*         else if Cudd.Vdd.is_cst a && is_cst x && is_cst y then *)
    (*           Some (ite (oo.xcmp (dval x) (dval y)) (grd a) ff') *)
    (*         (\* else if Cudd.Vdd.is_cst a && x ==. top || y ==. bot then Some tt' *\) *)
    (*         (\* else if x ==. bot || y ==. top then Some ff' *\) *)
    (*         else None) *)
    (*       (fun (a: bool) (x: xexpr unique) (y: xexpr unique) -> *)
    (*         (if not a then ff else guardb (oo.xcmp (get x) (get y))) |> unique tbl') *)
    (*       a b c *)
    (*     in *)
    (*     (\* Log.d2 logger "@[Almost@ done@ (0)@<1>…@]"; *\) *)
    (*     (\* ignore (of_guardb0' q); *\) *)
    (*     function *)
    (*       | Bexpr *)
    (*         -> Log.d logger "@[Almost@ done@<1>…@]"; *)
    (*           of_guardb' q *)
    (*       | Bvbdd *)
    (*         -> q *)

    (* --- *)

    let maxvlvl v =
      begin Cudd.Vdd.support v
          |> Cudd.Bdd.list_of_support
          |> List.fold_left (fun x v -> Stdlib.max x
            (Cudd.Man.level_of_var BEnv.env.Bdd.Env.cudd v))
              0
      end

    (* --- *)

    let vlvl bot v = if Cudd.Vdd.is_cst v then bot else
        Cudd.Man.level_of_var BEnv.env.Bdd.Env.cudd (Cudd.Vdd.topvar v)

    let unravel_guards = true

    let cmp_n_elim ?(lp = Util.pu) oo (type r) :
        supp: support -> bexpr -> t -> t -> r repr -> r =
      let xtop = of_arith (Aerexpr.cst' oo.xtop)
      and xbot = of_arith (Aerexpr.cst' oo.xbot) in
      let top = mk_xexpr_leaf xtop and bot = mk_xexpr_leaf xbot in
      fun ~supp a x y ->
        let exist' = exist' supp in
        Log.d logger "%t@[Setting@ up@ the@ guard@<1>…@]" lp;
        let a = guardb a in
        Log.i logger "%t@[Please@ bear@ with@ me@<1>…@]" lp;
        let shortcuts = ref 0 in
        (* let amax = ref 0 and xmax = ref 0 and ymax = ref 0 in *)
        let ret =
          let abot = maxvlvl a + 1
          and xbot = maxvlvl x + 1 and ybot = maxvlvl y + 1 in
          Log.i logger "%t@[Max@ levels:@ %u / %u / %u@]\
                       " lp abot xbot ybot;
          let d = ref 0 in
          fun a x y r ->
            if r <>. ff'' && r <>. tt'' then incr shortcuts;
            if !d = 0 then begin
              let alvl = vlvl abot a
              and xlvl = vlvl xbot x and ylvl = vlvl ybot y in
              (* amax := Pervasives.max !amax alvl; *)
              (* xmax := Pervasives.max !xmax xlvl; *)
              (* ymax := Pervasives.max !ymax ylvl; *)
              (* Log.ti logger "%t#s:%u / #l:%u / %u(%u) / %u(%u) / %u(%u)@<2>… " lp *)
              (*   !shortcuts (Cudd.PWeakke.count tbl'') *)
              (*   alvl !amax xlvl !xmax ylvl !ymax; *)
              Log.ti logger "%t#s:%u / #l:%u / %u / %u / %u@<2>… " lp
                !shortcuts (Cudd.PWeakke.count tbl'')
                alvl xlvl ylvl;
            end;
            incr d;
            if !d >= 1024 * 8 then d := 0;
            Some r
        in
        let q = Cudd.User.map_op3
          ~memo:(Cudd.Memo.Cache (Cudd.Cache.create 3))
          ~special:(fun (a: bool Cudd.Vdd.t) (x: t) (y: t) ->
            let ret = ret a x y in
            let acst = Cudd.Vdd.is_cst a in
            if acst && not (Cudd.Vdd.dval a) || x =. y then ret ff''
            else if acst || not unravel_guards then
              let xcst = is_cst x and ycst = is_cst y in
              if xcst && ycst && x <>. nan && y <>. nan then
                ret (ite (exist' (oo.xcmp (dval x) (dval y)))
                       (grd'' @@ exist' @@ of_guardb a) ff'')
              else if ycst && (y =. bot || y =. nan) then
                ret (grd'' @@ exist' @@ of_guardb a)
              else if xcst && (x =. top || x =. nan) then
                ret (grd'' @@ exist' @@ of_guardb a)
              else None
              else None)
          (fun (a: bool) (x: xexpr unique) (y: xexpr unique) ->
            if a
            then exist' (oo.xcmp (get x) (get y)) |> unique tbl''
            else dval_u ff'')
          a x y
        in
        Log.i logger "%t" lp;
        Log.i logger "%t@[One@ more@ elimination@<1>…@]" lp;
        function
          | Bexpr
            -> let q = existb'' ~supp q in
              Log.i logger "%t@[Almost@ done@<1>…@]" lp;
              of_guardb'' q
          | Vexpr
            -> existb'' ~supp q

  end

  module H (U: UT) = struct

    let normalize_wrt u x =
      let u = bsupp u in
      let usize = Cudd.Bdd.supportsize u in
      let sum = fold begin fun g l sum ->
        let g_u = project' u g and g_s = exist' u g and g_s' = forall' u g in
        let g_u_minterms = cstf (Cudd.Bdd.nbminterms usize g_u) in
        let l = of_arith l in
        sum +.~ (ite g_s' (l *.~ g_u_minterms) (ite g_s l zero))
      end x zero in
      Log.d3 logger "sum = %a" XOps.print sum;
      (* Log.d3 logger "norm. factor = %a" XOps.print (one /.~ sum); *)
      x /.~ sum

    open U

    let avg ?(lv: vsetname = "V") vars pmf : t -> t =
      let nvars, bvars = List.partition is_num vars in
      Log.d3 logger "%(%)b = %a" lv pp_vars bvars;
      Log.d3 logger "%(%)x = %a" lv pp_vars nvars;
      if nvars <> [] then
        failwith "avg over numerical variables is not supported.";
      let elim_bools = U.avg_supp (BOps.bsupp bvars) pmf in
      fun t -> t ++ elim_bools ++ pp "avg_" lv "" bvars print

  end

  (* --- *)

  (* module BddleafU: UT = struct *)

  (*   open Bddapron *)

  (*   (\* Representation separating finite and interpreted variables in guards, *)
  (*      using lists *\) *)
  (*   type t = (Cudd.Man.v, xexpr) Bddleaf.t *)

  (*   let print: formatter -> t -> unit = fun fmt -> function *)
  (*     | [] -> fprintf fmt "@<2>⟦⟧" *)
  (*     | list -> Print.list ~first:"@<1>⟦@[<hov 0>" ~sep:",@ " ~last:"@]@<1>⟧" *)
  (*         begin fun fmt Bddleaf.{ guard; leaf } -> *)
  (*           if is_tt guard *)
  (*           then fprintf fmt "@[<hv 0>%a@]" pp_xexpr leaf *)
  (*           else fprintf fmt "@[<hv 0>%a@;@[<hv>@<1>⟼@ %a@]@]\ *)
  (*                            " pp_bexpr guard pp_xexpr leaf *)
  (*         end fmt list *)

  (*   let nan = XOps.of_arith (Bddapron.Aerexpr.cst' Bddapron.AERDom.nan) *)
  (*   let bottom = [] *)

  (*   let guard_xexpr g leaf : t -> t = *)
  (*     (\* Bddleaf.cons_unique ~is_equal:(=) *\) *)
  (*     Bddleaf.cons_disjoint ~is_equal:(=) ~merge:(fun x _ -> x) *)
  (*       Bddleaf.{ guard = g; leaf } *)

  (*   let fold_guardleaves: (bexpr -> xexpr -> 'a -> 'a) -> t -> 'a -> 'a = fun f x acc -> *)
  (*     List.fold_left (fun acc Bddleaf.{ guard; leaf } -> f guard leaf acc) acc x *)

  (*   let to_xexpr: t -> xexpr = fun x -> fold_guardleaves XOps.ite x nan *)

  (*   (\* let tbl = make_table ~hash:Hashtbl.hash ~equal:Cudd.Vdd.is_equal *\) *)
  (*   (\* let mk_xexpr_leaf: xexpr -> t = cst BEnv.env.Bdd.Env.cudd tbl *\) *)
  (*   (\* let top = XOps.of_arith (Bddapron.Aerexpr.cst' Bddapron.AERDom.nan) *\) *)
  (*   (\* let bottom = mk_xexpr_leaf top *\) *)

  (*   (\* let to_xexpr: t -> xexpr = fun x -> fold_guardleaves XOps.ite x top *\) *)

  (*   let of_xexpr: xexpr -> t = fun x -> *)
  (*     let x' = XNum.fold_numconvex tt begin fun (acc: t) e c -> *)
  (*       fold_u begin fun g l (acc: t) -> *)
  (*         let l = Cudd.Mtbdd.cst_u BEnv.env.Bdd.Env.cudd l in *)
  (*         (\* match eval_cst acc g with *\) *)
  (*         (\*   | Some u when is_cst u ->                              (\\* shortcut *\\) *\) *)
  (*         (\*       guard_xexpr g (XOps.ite c l (dval u)) acc *\) *)
  (*         (\*   | _ -> *\) *)
  (*         guard_xexpr g l acc *)
  (*         (\* fold_guardleaves (fun g' u (acc: t) -> *\) *)
  (*         (\*   guard_xexpr (g &&~ g') (XOps.ite c l u) acc) *\) *)
  (*         (\*   (constrain acc g) acc *\) *)
  (*       end e acc *)
  (*     end bottom x in *)
  (*     (\* Log.i logger "xxx = %a" print x'; *\) *)
  (*     x' *)

  (*   let elim_supp oo supp : t -> t = fun t -> *)
  (*     if is_tt supp then t else *)
  (*       (Log.d logger "t = %a" print t; *)
  (*        let t' = fold_guardleaves (fun g leaf -> *)
  (*          if leaf = nan then fun acc -> acc else *)
  (*            let g' = exist' supp g in *)
  (*            Bddleaf.cons_disjoint ~is_equal:(=) ~merge:(best oo) *)
  (*              Bddleaf.{ guard = g'; leaf }) t bottom in *)
  (*        Log.d logger "t'= %a" print t'; *)
  (*        t') *)

  (*   (\* XXX: second argument xexpr MUST NOT have any arithmethic condition in its *)
  (*      guards *\) *)
  (*   let accum ~factor : xexpr -> t -> t = fun e x -> *)
  (*     Log.d logger "accum:"; *)
  (*     let factor = Apron.Coeff.Scalar (Apron.Scalar.of_mpqf factor) in *)
  (*     fold_guardleaves (fun g x -> *)
  (*       fold (fun gx x -> *)
  (*         let ggx = g &&~ gx in *)
  (*         fold (fun ge e -> *)
  (*           let e' = XArith.add (XArith.mul (XArith.cst factor) x) e in *)
  (*           guard_xexpr (ggx &&~ ge) (of_arith e')) e) x) *)
  (*       x bottom *)

  (*   let pre assertion evols (q: t) : t = *)
  (*     Log.d logger "pre:"; *)
  (*     let qsupp = List.fold_left (fun s Bddleaf.{ guard; leaf } -> *)
  (*       Cudd.Bdd.support_union (Cudd.Bdd.support_union (Cudd.Bdd.support guard) *)
  (*                                 (Cudd.Mtbdd.support leaf)) s) *)
  (*       tt q in *)
  (*     let (comp, map) as subst = Bddapron.Expr0.O.compose_of_subst BEnv.env BEnv.cond *)
  (*       (Bddapron.Expr0.O.Support qsupp) evols in *)
  (*     let q' = fold_guardleaves (fun g (x: xexpr) -> *)
  (*       let g = match comp with None -> g | Some comp -> Cudd.Bdd.vectorcompose comp g in *)
  (*       let x = match comp with None -> x | Some comp -> Cudd.Mtbdd.vectorcompose comp x in *)
  (*       let x' = Bddapron.AerexprDD.parsubstdd' XOps.table BEnv.env ?memo:None x subst in *)
  (*       fold (fun g' l acc -> (\* Bddleaf.{ guard = (g &&~ g' &&~ assertion); leaf = of_arith l} :: acc *\) *)
  (*         guard_xexpr (assertion &&~ g &&~ g') (of_arith l) acc) x') *)
  (*       q (guard_xexpr tt(\* !~assertion *\) nan bottom) *)
  (*     in *)
  (*     (\* let xx = guard_xexpr !~assertion nan q' in *\) *)
  (*     Log.d logger "pre done"; *)
  (*     q' *)

  (* end *)

  (* ------------------------------------------------------------------------ *)

  module type ORD_OPERATORS = sig
    module U: UT
    include ORDER
    val optim: order_op -> ?lv:vsetname -> vars -> U.t -> U.t
  end

  (* --- *)

  module ExactRepr (U: UT) : (ORD_OPERATORS with module U = U) = struct

    module U = U
    include U.Order

    (* --- *)

    (* A type for convex guards *)
    module G = ApronDoman.MakeNumDom (D.CDom.Man.N)
      (struct let man = D.CDom.numdoman end) (BEnv)

    let foldcut_numconvex d =
      XNum.foldcut_numconvex
        ~convex:begin fun numdom numcond c ->
          let numdom = G.meet_condition' numdom c in
          if G.is_bottom numdom then [] else [ numdom ]
        end
        ~diseq:begin fun numdom _ expr ->
          let inf, sup = BddapronUtil.diseq_linexpr BEnv.env expr in
          let l = ApronUtil.abstract_lincons_list G.man [ inf; sup ] in
          let l = List.rev_map (G.meet1 numdom) l in
          List.filter (fun l -> not (G.is_bottom l)) l
        end
        d

    module Lin = XArith.Lin

    let of_lin l = l |> XArith.of_lin |> XOps.of_arith
    let cinv = Bddapron.AERDom.inv
    let cneg = Bddapron.AERDom.neg
    let csgn = Bddapron.AERDom.sgn
    let ccsti = Bddapron.AERDom.of_int

    let var_bound dir v (t, l) =
      let add_epsilon sgn = Lin.add (Lin.term (ccsti sgn) epsilon) in
      let l = XArith.to_lin l in
      match Lin.var_coeff l v with
        | None -> None
        | Some qv ->
            (* Log.d3 logger "%a: dir=%d" Lin.print l dir; *)
            let l', dir =
              let l = Lin.sub l (Lin.term qv v) in
              if csgn qv > 0
              then Lin.scale (cinv qv) (Lin.negate l), dir
              else Lin.scale (cinv (cneg qv)) l, -dir
            in
            match t with
              | Apron.Lincons0.EQ -> Some l'
              | Apron.Lincons0.SUP when dir < 0 -> Some (add_epsilon dir l')
              | Apron.Lincons0.SUPEQ when dir < 0 -> Some l'
              | Apron.Lincons0.SUP
              | Apron.Lincons0.SUPEQ -> None
              | t -> failwith (asprintf "Unhandled comparison: %a\
                                       " ApronUtil.pp_cmp t)

    let acc_best oo : Lin.t option -> xexpr option -> xexpr option = function
      | None -> fun acc -> acc
      | Some x -> let x = of_lin x in function
          | None -> Some x
          | Some y -> Some (best oo x y)

    (* Downscale ε's coefficient to its sign. *)
    let downscale_epsilon =
      let is_epsilon v = symbol.Bdd.Env.compare epsilon v = 0 in
      Lin.map_terms
        (fun (q, v) -> if is_epsilon v then (ccsti (csgn q), v) else (q, v))

    let lpsolve_n_elim_vars ({ xtop } as oo)
        : G.t -> Lin.t -> vars -> (G.t * Lin.t) list
      =
      let xsgn = csgn xtop in
      let top = XOps.of_arith (Bddapron.Aerexpr.cst' xtop) in
      let lpsolve_n_elim_var v (g, l) = match Lin.var_coeff l v with
        | None -> [g, l]                            (* ll does not depend on c *)
        | Some qv ->
            let ls = Lin.project_out' (PSette.add v empty_vset) l in
            let dir = if xsgn = csgn qv then 1 else -1 in

            let obest = G.to_lincons_array g |> Array.fold_left
                begin fun acc c -> match XArith.Cond.of_lincons c with
                  | `Bool false -> failwith "Unsatisfiable constraint?"
                  | `Bool true -> acc
                  | `Cond c -> acc_best oo (var_bound dir v c) acc
                end None
            in
            let best_v_value = match obest with Some x -> x | None -> top in

            (* Substitute v by best_value in l: *)
            if Log.check_level logger Log.Debug3 then begin
              let best_expect = fold begin fun g l ->
                let l = XArith.to_lin l in
                Lin.add ls (Lin.scale qv l) |> downscale_epsilon |>
                    of_lin |> XOps.ite g
              end best_v_value nan in
              Log.d3 logger "@[best(%a) = %a@ @<7>⤳ best(%a) = %a@]"
                pp_var v XOps.print best_v_value
                Lin.print l XOps.print best_expect
            end;

            (* Actual result associates guards (as convex numerical domains)
               with arithmetic expressions *)
            foldcut_numconvex g begin fun acc l g ->
              let l = Cudd.Mtbdd.dval l |> XArith.to_lin in
              let l = Lin.add ls (Lin.scale qv l) |> downscale_epsilon in
              (g, l) :: acc
            end [] best_v_value
      in

      fun g l vars ->
        List.fold_left (fun acc v ->
          List.rev_map (lpsolve_n_elim_var v) acc |>
              List.flatten |> List.rev_map (fun (g, l) -> G.forget_list g [v], l))
          [g, l] vars

    (* --- *)

    let cons oo acc (g, l) =
      let l = of_lin l in
      if is_nan l then acc else begin
        (* Log.d3 logger "consing…"; *)
        let cons' g l a = if is_bottom g then a else (g, Lazy.force l) :: a in
        let g, acc = List.fold_left begin fun (g, acc) (g', l') ->
          let gg' = inter g g' in
          if is_bottom gg' then g, ((g', l') :: acc) else
            let acc = cons' (inter g' (neg g)) (lazy l') acc in
            let acc = cons' gg' (lazy (best oo l l')) acc in
            let gng' = inter (neg g') g in
            (* Log.d3 logger "consing…' with %a" pp_repr gng'; *)
            Log.tf' (lazy (asprintf ">> %i" (List.length acc)));
            (gng', acc)
        end (D.of_disj [D.CDom.of_boolnumlist [tt, g]] |> arepr, []) acc in
        let acc = cons' g (lazy l) acc in
        (* Log.d3 logger "|acc| = %d" (List.length acc); *)
        (* Log.d3 logger "consing… done"; *)
        acc
      end

    let rebuild = List.fold_left (fun acc (g, l) -> ite (retr g) l acc) nan

    let elim_nums oo vars q =
      if vars = [] then q else
        let lpsolve_n_elim_vars = lpsolve_n_elim_vars oo
        and cons = List.fold_left (cons oo) in
        U.fold_guardleaves begin fun (g: bexpr) (x: xexpr) ->
          (* CuddUtil.dynamic_reordering_suspend2 BEnv.env *)
          (foldcut_numconvex (G.top ()) (fun acc l g ->
            if is_nan l then acc else
              let l = Cudd.Mtbdd.dval l |> XArith.to_lin in
              lpsolve_n_elim_vars g l vars |> cons acc))
            [] x
          ++ rebuild
          ++ U.guard_xexpr g
        end q U.bot

    (* --- *)

    open U

    let optim oo ?(lv: vsetname = "V") vars : t -> t =
      let nvars, bvars = List.partition is_num vars in
      Log.d3 logger "%(%)b = %a" lv pp_vars bvars;
      Log.d3 logger "%(%)x = %a" lv pp_vars nvars;
      let elim_bools = U.elim_supp oo (BOps.bsupp bvars)
      and elim_nums = elim_nums oo nvars in
      fun t -> t ++
        (* elim in booleans, *)
        elim_bools ++ pp oo.name lv "b" bvars print ++
        (* then elim in arithmetic expressions *)
        elim_nums ++ pp oo.name lv "x" nvars print

  end

  (* ------------------------------------------------------------------------ *)

  module ApproxRepr (U: UT) : (ORD_OPERATORS with module U = U) = struct

    module U = U
    include U.Order

    let conds_supp nvars =
      if PSette.is_empty nvars then tt else
        PDMappe.fold begin fun (`Apron c) (v, _) acc ->
          let x = PSette.inter nvars (Arith.Cond.support c) in
          if PSette.is_empty x then acc else Cudd.Bdd.ithvar cuddman v &&~ acc
        end BEnv.cond.Bdd.Cond.condidb tt

    let elim_in_arith top nvars: XArith.t -> xexpr = fun e ->
      if (PSette.is_empty nvars
          || PSette.is_empty (PSette.inter nvars @@ XArith.support e))
      then XOps.of_arith e
      else XOps.of_arith (Bddapron.Aerexpr.cst' top)

    (* --- *)

    open U

    let optim oo ?(lv: vsetname = "V") vars : t -> t =
      let xvars, bvars = List.partition is_num vars in
      Log.d3 logger "%(%)b = %a" lv pp_vars bvars;
      Log.d3 logger "%(%)x = %a" lv pp_vars xvars;
      let nvars = varset xvars in
      let bsupp = BOps.bsupp bvars in
      let nsupp = conds_supp nvars in
      let elim_bools =
        U.elim_supp oo (bsupp &&~ nsupp)
      and elim_nums t : t =
        fold (fun c l -> elim_in_arith oo.xtop nvars l |> U.guard_xexpr c)
          t U.bot
      in
      fun t -> U.to_xexpr t ++
        (* elim in arithmetic expressions first, *)
        elim_nums ++ pp oo.name lv "x" xvars print ++
        (* then elim booleans, *)
        elim_bools ++ pp oo.name lv "b" bvars print

  end

  (* ------------------------------------------------------------------------ *)

  module NoLeafFilter: LEAF_FILTER = struct let apply x = x end

  (* ------------------------------------------------------------------------ *)

  let epsilon_module eps =
    (module struct
      let var: var = Env.Symb.mk eps;;
      assert (Bddapron.Env.add_vars_with ~packing:`None BEnv.env [var, `Real]
              = None);;
      let truth_cond e = e >.~ zero &&~ !~(e ==.~ zero) &&~ !~(e <.~ zero)
      let truth: bexpr = truth_cond (XOps.var var);;
      open BEnv
      let restore_conds () =
        let eps_supp = support' (truth_cond (XOps.var var)) in
        let rem_supp = Cudd.Bdd.support_diff cond.Bdd.Cond.condsupp eps_supp in
        let p1 = Bdd.Cond.reduce_with cond rem_supp in
        let p2 = Bddapron.Env.remove_vars_with ~packing:`None env [var] in
        Bdd.Idx.Map.compose' (Some p1) p2 |> upgrade_of_perms
    end : EPSILON)

  let obj_module (module LF: LEAF_FILTER) (module E: EPSILON) approx = function
    (* Over approximations (correct as long as exist_improve is not applied to
       numerical variables) *)
    | `Minimize _ when approx ->
        (module ApproxRepr (MtbddU (LF) (Min (E))) : ORD_OPERATORS)
    | `Maximize _ when approx ->
        (module ApproxRepr (MtbddU (LF) (Max (E))) : ORD_OPERATORS)
    | `Minimize _ ->
        (module ExactRepr (MtbddU (LF) (Min (E))) : ORD_OPERATORS)
    | `Maximize _ ->
        (module ExactRepr (MtbddU (LF) (Max (E))) : ORD_OPERATORS)

  (* --- *)

  module type TERM_CRITERIA = sig
    type algo_state
    val init: algo_state
    val next: algo_state -> algo_state
    val log: algo_state -> (Format.formatter -> 'a -> unit) -> 'a -> unit
    val stop: algo_state -> bool
    val iter: algo_state -> int
  end

  module type TERMCOMMON_PARAM = sig val qname: Util.ufmt end
  module type TERMKMAX_PARAM = sig val kmax: int end
  module TKmax (P0: TERMCOMMON_PARAM) (P: TERMKMAX_PARAM) = struct
    type algo_state = int
    let init = 0
    let next = succ
    let log i pp a =
      Log.d logger "%(%)%i%t\
                   " P0.qname i (l2_2 " = %a" pp a)
    let stop i = i = P.kmax
    let iter i = i
  end

  module type TERMDTHR_PARAM = sig val d: discount
                                   val dthr: discount end
  module TDthr (P0: TERMCOMMON_PARAM) (P: TERMDTHR_PARAM) = struct
    type algo_state = int * discount
    let init = (0, Mpqf.of_int 1)
    let next (i, f) = (succ i, Mpqf.mul P.d f)
    let log (i, f) pp x =
      Log.d logger "@[%(%)%i%t@ (dmin = %a)@]\
                   " P0.qname i (l2_2 " = %a" pp x) Mpqf.print f
    let stop (_, f) = Mpqf.cmp f P.dthr < 0
    let iter = fst
  end

  module TDthrKmax (P0: TERMCOMMON_PARAM)
    (Pd: TERMDTHR_PARAM) (Pk: TERMKMAX_PARAM) =
  struct
    include TDthr (P0) (Pd)
    let stop ((i, _) as s) = i = Pk.kmax || stop s
  end

  (* --- *)

  let tkmax (module P0: TERMCOMMON_PARAM) kmax =
    (module TKmax (P0) (struct let kmax = kmax end) : TERM_CRITERIA)

  let tdthr (module P0: TERMCOMMON_PARAM) d dthr =
    (module TDthr (P0) (struct let d = d let dthr = dthr end) : TERM_CRITERIA)

  let tdthrkmax (module P0: TERMCOMMON_PARAM) d dthr kmax =
    (module TDthrKmax (P0) (struct let d = d let dthr = dthr end)
        (struct let kmax = kmax end) : TERM_CRITERIA)

  module Loop (T: TERM_CRITERIA) = struct
    open T
    let run rph pp step t =
      let rec cont s t =
        if stop s then t, iter s else begin
          rph ~i:(iter s) t;
          let t' = step t and s = next s in
          if t' = t then (log s pp_print_string "^"; t, iter s)
          else cont s (log s pp t'; t')
        end
      in
      log init pp t;
      cont init t
  end

  (* --- *)

  let optimize
      { od_goal = g; od_discount = d; od_ustoch = ustoch;
        od_approx; od_dthr; od_kmax;
        od_dirty; od_rp }
      (cs, a, cfprog)
      =

    let e = of_nexpr (BddapronOptim.goal g) in
    let c = CtrlSpec.c_vars cs in
    let u = CtrlSpec.u_vars cs in

    (* --- *)

    let rp = RP.make od_rp in
    let evols = simplify_equs a cfprog.Program.c_disc_equs in

    (* --- *)

    Log.d2 logger "A = %a" pp_bexpr a;
    let lq: Util.ufmt = "@<1>η" in
    let module P0: TERMCOMMON_PARAM = struct let qname = lq end in
    let transcost =
      let esupp = support e and inputs = varset E.env.Env.i_vars in
      not (PSette.is_empty (PSette.inter esupp inputs))
    in
    let kmax = match od_kmax with
      | Some kmax when transcost && kmax >= 1 ->
          Log.i logger "@[Cost@ seems@ on@ transitions:@ iterating@ \
                          up@ to@ kmax-1 = %u@]" (pred kmax);
          Some (pred kmax)
      | x -> x
    in

    (* --- *)

    Log.i logger "@[Setting@ up@ pre(@<3>⋅)…@]";
    let module Epsilon = (val epsilon_module "ε" : EPSILON) in
    let module O =
          (val obj_module (module NoLeafFilter) (module Epsilon) od_approx g
              : ORD_OPERATORS) in
    let module H = H (O.U) in
    let open O in
    let rpu = RP.handle' U.ddinfos rp ~lp:Util.pu ~v:(asprintf "%(%)" lq) in

    (* --- *)

    (* Computing the probability mass function w.r.t the environment: *)
    let u_prob, u_ndet = match ustoch with
      | None ->
          None, u
      | Some { up_inputs } when PSette.is_empty up_inputs ->
          Log.w logger "@[Skipping@ setup@ for@ empty@ set@ of@ stochastic@ \
                          inputs.@]";
          None, u
      | Some { up_spec = `Samples samples; up_inputs = up } ->
          let un = PSette.diff (varset u) up in
          Log.i logger "Us = %a" pp_vset up;
          Log.i logger "Un = %a" pp_vset un;
          let un = PSette.elements un and up = PSette.elements up in
          let ax = begin repr a
              |> Elim.exist ~lv:"C" c
              |> Elim.exist ~lv:"Un" un
              |> retr
          end in
          let samples = XOps.ite ax (of_nexpr samples) XOps.zero in
          let u_pmf = H.normalize_wrt up samples in
          Log.d logger "Pu = %a" XOps.print u_pmf;
          Some (u_pmf, up), un
    in
    let pre =
      let improve = optim improve ~lv:"C" c
      and degrade = match u_ndet with
        | [] -> fun x -> x
        | u -> optim degrade ~lv:"Un" u
      and avg = match u_prob with
        | None -> fun x -> x
        | Some (pmf, u) -> H.avg ~lv:"Us" u pmf
      and pre = U.pre a evols
      and accum = U.accum ~factor:d e in
      fun q -> accum (pre (avg (degrade (improve q))))
    in

    (* --- *)

    Log.i logger "@[Computing %(%)0…@]" lq;
    let q0 = begin e
        |> Log.roll (CuddUtil.dynamic_reordering_suspend1 BEnv.env U.of_xexpr)
        |> fun q0 -> if transcost then Log.roll (U.pre a []) q0 else q0
    end in

    (* --- *)

    (* Bounded fixpoint: *)
    Log.i logger "@[Setting@ up@ fp(pre(@<4>⋅))…@]";
    let module T = (val (match kmax, od_dthr with
      | Some kmax, _ when kmax < 0 -> failwith "kmax < 0?"
      | Some kmax, None -> tkmax (module P0) kmax
      | None, Some dthr -> tdthr (module P0) d dthr
      | Some kmax, Some dthr -> tdthrkmax (module P0) d dthr kmax
      | None, None ->
          Log.w logger "@[No@ termination@ criteria@ specified:@ optimization@ \
                          algorithm@ may@ never@ terminate!@]";
          tkmax (module P0) (-1)) : TERM_CRITERIA)
    in
    let module BLoop = Loop (T) in

    (* --- *)

    Log.i logger "@[Computing@ fixpoint@<1>…@]";
    let q, i = Log.roll (BLoop.run rpu U.print pre) q0 in

    if Log.check_level logger Log.Debug then
      Log.i logger "@[%(%) = %(%)%i@ (i.e.@ computed@ in@ %i@ iteration%s)@]"
        lq lq i i (if i > 1 then "s" else "")
    else
      Log.i logger "@[%(%) was computed@ in@ %i@ iteration%s.@]"
        lq i (if i > 1 then "s" else "");

    Log.d2 logger "%(%) = %a" lq U.print q;

    (* --- *)

    RP.dispose rp;

    let (cs, k, cfprog), eu =
      (* Gather dependencies induced by η. *)
      let subset = U.support q in
      Log.d3 logger "support = %a" pp_vset subset;
      let pvars = filter_vars subset c in
      if PSette.is_empty pvars then begin
        Log.i logger "@[<h>System@ is@ not@ controllable@ enough:@ \
                        skipping@ optimization@ step.@]";
        (cs, a, cfprog), no_upgrade
      end else begin
        Log.i logger "@[<h>Extending@ environment@ with@ extra@ \
                        primed@ controllable@ variables@ (C').@]";
        let pvars, oe, eu = make_primes pvars in
        let rs = CuddUtil.dynamic_reordering_save_n_stop BEnv.env in
        let (cs, a, cfprog), q, improve = Log.roll begin fun () ->
          maybe_upgrade cs a cfprog eu,
          upgrade U.permute eu q,
          { improve with
            epsilon_truth = upgrade_bexpr eu improve.epsilon_truth; }
        end () in

        let _cc' = gather_vars c pvars in
        let _c, c' = List.split _cc' in
        let c'x, c'b = List.partition is_num c' in

        Log.d3 logger "%(%) = %a" lq U.print q;

        (* Rely on comparisons with nan that behave nicely: *)
        (* Log.d logger "   η = η if A, %a otherwise" pp_xexpr nan; *)
        (* let q = ite a q nan in *)
        let q = U.(^.) q a in
        let q' = Log.roll (U.rename_vars _cc') q in
        let a' = Log.roll (bsubst_vars _cc') a in

        Log.i logger "@[Computing R @<13>= ∃Cb'(A∩A'∩(%(%)'%t%(%)))@<1>…@]\
                     " lq pp_ord lq;

        let r = Log.roll begin fun q ->
          U.cmp_n_elim improve ~lp:(Util.ps "+ ") ~supp:(bsupp c'b)
            (a &&~ a') q' q U.Bexpr ^~ improve.epsilon_truth |> repr
        end q in
        Log.d logger "R@[ %t@<13>= ∃Cb'(A∩A'∩(%(%)'%t%(%)))%t@]\
                     " (l3_2 "= %a@ (" pp_repr r) lq pp_ord lq (l3 ")");

        Log.i logger "@[Computing K = @<8>A∩∄Cx'R…@]";

        let k = Log.roll begin fun r ->
          nexist ~lp:(Util.ps "+ ") ~debug:Log.Debug3 ~lv:"@<3>Cx'" c'x r
            |> retr |> (&&~) a |> fun k -> k ^~ improve.epsilon_truth
        end r in
        Log.d logger "K@[ %t= A∩∄Cx'R%t@]\
                     " (l2_2 "= %a@ (" pp_bexpr k) (l2 ")");

        CuddUtil.dynamic_reordering_restore BEnv.env rs;

        if od_dirty then begin
          Log.i logger "@[<h>Leaving@ environment@ as@ is@ as@ requested@ \
                          (dirty=true).@]";
          (cs, k, cfprog), eu
        end else begin
          Log.i logger "@[Restoring@ environment.@]";
          let eu' = Epsilon.restore_conds () in
          let eu' = compose_upgrades eu' (restore_environment oe) in
          (* upgrade BddapronUtil.permute_boolexpr perms (retr a), *)
          maybe_upgrade cs k cfprog eu', compose_upgrades eu eu'
        end
      end
    in


    (* --- *)

    let k = BOps.simplify k in

    (* let ok_ppa = true in *)

    (* let ka = Log.roll D.of_boolexpr k in *)
    (* (\* if ok_ppa then Log.i logger "@\n@\n@<8>α℘(K) = %a@\n" D.print ka; *\) *)
    (* let k = Log.roll D.to_boolexpr ka in *)

    (cs, k, cfprog), upgrade_to_perms eu

end

(* -------------------------------------------------------------------------- *)
