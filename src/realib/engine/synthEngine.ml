(******************************************************************************)
(* synth *)
(* synthesis algorithm engine *)
(* author: Nicolas Berthier *)
(* This file is part of ReaTK released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)
(******************************************************************************)

open Format
open ParseParams

(** ReaX synthesis engine *)

let (&) f g = f g
let logger = Log.mk "Synth"

exception InvalidAlgo of (formatter -> unit)
exception InvalidOpts of (formatter -> unit)

(* -------------------------------------------------------------------------- *)

type synth_spec =
  | SynthBoolean of Synth.bool_param_t
  | SynthRelationalBoolean of Synth.relb_param_t
  | SynthLimitedLookahead of Synth.lla_param_t
  | SynthStandard of Synth.std_param_t * VerifEngine.dom_spec
  | SynthSingleFp of Synth.sfp_param_t * VerifEngine.dom_spec
  | SynthDisjunct of (Synth.df_param_t * VerifEngine.dom_spec *
                        VerifEngine.pwext_spec)

(* -------------------------------------------------------------------------- *)

type descr = formatter -> unit

type synthstep =
  | VerifStrat of VerifEngine.spec
  | Synthesis of synth_spec

type algo = synthstep list

type spec = Env.t * algo

(******************************************************************************)
(* algorithm descriptions *)
(******************************************************************************)

let algos =         (* identifier * short description * long description list *)
  [
    ("sB",("Boolean synthesis", "basic boolean synthesis"));
    ("sR",("Relational Boolean synthesis", "Relational boolean synthesis"));
    ("sL",("Limited lookahead synthesis", "Limited lookahead synthesis"));
    ("sS",("logico-numerical synthesis",
           "logico-numerical synthesis with abstract interpretation"));
    ("sD",("disjunctive logico-numerical synthesis",
           "disjunctive logico-numerical synthesis with abstract \
            interpretation"));
  ]

let pp_short_algodesc s fmt = pp_print_string fmt (fst & List.assoc s algos)

let print_algos fmt () =
  List.iter
    (fun (s,(_,longdesc)) ->
      fprintf fmt "@[ ";
      Util.print_fixed fmt 5 s;
      fprintf fmt "@[<hov>";
      Util.print_breakable fmt longdesc;
      fprintf fmt "@]@]@.")
    algos


(******************************************************************************)
(* parsing of reordering policy configurations (TODO: move it elsewhere) *)
(******************************************************************************)

let make_rp defaults =
  let defaults = Util.list2mappe defaults in
  fun opts ->
    check_extra_options logger "rp" defaults opts;
    let stop_ri = get_option' opts "stop_ri" int_of_string
    and stop_dri = get_option' opts "stop_dri" int_of_string
    and dtr_max = get_option' opts "dtr_max" int_of_string
    and dnr_max = get_option' opts "dnr_max" int_of_string
    and log = get_option' opts "log" bool_option
    and logfile = get_option' opts "logfile" string_option in
    let open BddapronReorderPolicy in
    let stop_r = thresholds_cond ?i:stop_ri ?dtr_max ?dnr_max None
    and stop_dr = thresholds_cond ?i:stop_dri None in
    mk_config ?log ?logfile ?stop_r ?stop_dr ()

let rp_defaults = [("stop_ri","");("stop_dri","");
                   ("dtr_max","");("dnr_max","");
                   ("log","");("logfile","")]

let str_to_rp ?(defaults = rp_defaults) s =
  try make_rp defaults @@ parse_options s with
    | Parsing.Parse_error
    | Parse.Lex_error
    | Exit -> raise (InvalidOpts (Util.pp1 "syntax@ error@ in@ `%s'" s))

(******************************************************************************)
(* parsing of synthesis algo options *)
(******************************************************************************)

open Synth

let parse_boolexpr_option env = function
  | "" -> None
  | s -> Some (Env.str_to_boolexpr env s)

let parse_sB =
  let defaults = Util.list2mappe [("r","false");("reach","");
                                  ("a","false");("attr","");
                                  ("deads","false");("nobnd","false");
                                  ("rp","");
                                  ("oldassert","false")] in
  fun env opts ->
    check_extra_options logger "sB" defaults opts;
    let reach = get_option opts defaults "r" bool_option
    and reachp = get_option opts defaults "reach" (parse_boolexpr_option env)
    and attr = get_option opts defaults "a" bool_option
    and attrp = get_option opts defaults "attr" (parse_boolexpr_option env)
    and deads = get_option opts defaults "deads" bool_option
    and no_bnd = get_option opts defaults "nobnd" bool_option
    and rp = get_option opts defaults "rp" str_to_rp
    and oldassert = get_option opts defaults "oldassert" bool_option in
    let params = make_bool_param reach reachp attr attrp ~deads ~no_bnd ~rp
      ~oldassert in
    SynthBoolean params

let decomp_option = function
  | "" | "no" | "none" | "false" -> None
  | "disj" | "disjunctive" -> Some `Disj
  | s -> raise @@ InvalidOpts (Util.pp1 "unknown@ specification@ `%s'@ given@ \
                                        for@ `decomp'@ option" s)

let parse_sRB =
  let defaults = Util.list2mappe [("reach","false");
                                  ("decomp", "");
                                  ("nobnd","false");("rp","");
                                  ("dirty","false");
                                  ("oldassert","false")] in
  fun env opts ->
    check_extra_options logger "sRB" defaults opts;
    let reach = get_option opts defaults "reach" bool_option
    and decomp = get_option opts defaults "decomp" decomp_option
    and check = get_option' opts "check" bool_option
    and no_bnd = get_option opts defaults "nobnd" bool_option
    and rp = get_option opts defaults "rp" str_to_rp
    and oldassert = get_option opts defaults "oldassert" bool_option
    and dirty = get_option opts defaults "dirty" bool_option in
    let params = make_relb_param ~dirty ?decomp ?check ~rp ~reach ~oldassert
      ~no_bnd in
    SynthRelationalBoolean params

let parse_sL =
  let defaults = Util.list2mappe [
    ("k","2");
    ("be","false"); ("best-effort","false"); ("strict","true");
    ("recov","0");
    ("grow","false");
    ("growl","false");
    ("shrink","false");
    ("dirty","false");
    ("ndr","false"); ] in
  fun env opts ->
    check_extra_options logger "sL" defaults opts;
    let k = get_option opts defaults "k" int_of_string in
    let best_effort = (get_option opts defaults "be" bool_option
                       || get_option opts defaults "best-effort" bool_option
                       || not (get_option opts defaults "strict" bool_option)) in
    let recov = get_option opts defaults "recov" int_of_string in
    let grow = get_option opts defaults "grow" bool_option in
    let growl = get_option opts defaults "growl" bool_option in
    let shrink = get_option opts defaults "shrink" bool_option in
    let dirty = get_option opts defaults "dirty" bool_option in
    let ndr = get_option opts defaults "ndr" bool_option in
    let defer = match grow || growl, shrink with
      | false, false -> `None
      | true, false -> `Grow growl
      | false, true -> `Shrink
      | true, true -> raise @@
          InvalidOpts (Util.pp "incompatible `grow' and `shrink' options given")
    in
    let params = make_lla_param ~dirty ~ndr ~defer ~recov ~best_effort k in
    SynthLimitedLookahead params

let parse_sS =
  let defaults = Util.list2mappe [("d","P:p");("ws","2");("wd","2");("fwcs","");
                                  ("split_convex","false");("bdisj","false");
                                  ("deads","false");
                                  ("initonly","false");
                                  ("nobnd","false");
                                  ("oldassert","false");
                                  ("split_outer","true");] in
  fun env opts ->
    check_extra_options logger "sS" defaults opts;
    let dom = get_option opts defaults "d" (fun x -> x)
    and ws = get_option opts defaults "ws" int_of_string
    and wd = get_option opts defaults "wd" int_of_string
    and fwcs = get_option' opts "fwcs" int_of_string
    and split_outer = get_option opts defaults "split_outer" bool_option
    and split_convex = get_option opts defaults "split_convex" bool_option
    and deads = get_option opts defaults "deads" bool_option
    and bdisj = get_option opts defaults "bdisj" bool_option
    and initonly = get_option' opts "initonly" bool_option
    and no_bnd = get_option opts defaults "nobnd" bool_option
    and oldassert = get_option opts defaults "oldassert" bool_option
    in
    let params = make_std_param ~ws ~wd ?fwcs ~split_convex ~deads ~bdisj
      ?initonly ~no_bnd ~oldassert ~split_outer in
    let dom_spec = VerifEngine.parse_domain env ~force_power:true dom in
    SynthStandard (params, dom_spec)

let parse_sF =
  let defaults = Util.list2mappe [("d","P:p");("ws","2");
                                  ("split_convex","false");("bdisj","false");
                                  ("deads","false");("nobnd","false");
                                  ("oldassert","false")] in
  fun env opts ->
    check_extra_options logger "sF" defaults opts;
    let dom = get_option opts defaults "d" (fun x -> x)
    and ws = get_option opts defaults "ws" int_of_string
    and split_convex = get_option opts defaults "split_convex" bool_option
    and deads = get_option opts defaults "deads" bool_option
    and bdisj = get_option opts defaults "bdisj" bool_option
    and no_bnd = get_option opts defaults "nobnd" bool_option
    and oldassert = get_option opts defaults "oldassert" bool_option in
    let params = make_sfp_param ~ws ~split_convex ~deads ~bdisj ~no_bnd
      ~oldassert in
    let dom_spec = VerifEngine.parse_domain env ~force_power:true dom in
    SynthSingleFp (params, dom_spec)

let parse_sD =
  let defaults = Util.list2mappe [
    ("d","P:p");
    ("ws","2");("wd","2");("fwcs","");
    ("ws2","2");("emws","10");
    ("bdisj", string_of_bool DomExt.DefaultFPParams.booldisj);
    ("uqbdd", string_of_bool DomExt.DefaultFPParams.uqbdd); (* unused default *)
    ("norelax", string_of_bool (not DomExt.DefaultFPParams.relax));     (* ibid *)
    ("pwext", "false"); ("deads","false"); ("nobnd","false");
    ("oldassert","false")
  ] in
  fun env opts ->
    check_extra_options logger "sD" defaults opts;
    let dom = get_option opts defaults "d" (fun x -> x)
    and ws = get_option opts defaults "ws" int_of_string
    and wd = get_option opts defaults "wd" int_of_string
    and fwcs = get_option' opts "fwcs" int_of_string
    and ws2 = get_option opts defaults "ws2" int_of_string
    and emws = get_option opts defaults "emws" int_of_string
    and pwext = get_option opts defaults "pwext" bool_option
    and deads = get_option opts defaults "deads" bool_option
    and no_bnd = get_option opts defaults "nobnd" bool_option
    and oldassert = get_option opts defaults "oldassert" bool_option in
    let params = make_df_param ~ws ~wd ?fwcs ~ws2 ~emws ~pwext ~deads ~no_bnd
      ~oldassert in
    let pwextp = VerifEngine.parse_pwext env opts in
    let dom_spec = VerifEngine.parse_domain env ~force_power:true
      ~forbid_pwext:true dom in
    SynthDisjunct (params, dom_spec, pwextp)

(******************************************************************************)
(* parses an algorithm from a string *)
(******************************************************************************)

let invalid_algo msg o = raise & InvalidAlgo (Util.pp1 msg o)
let invalid_algo' msg = raise & InvalidAlgo (Util.pp msg)

(* add new algorithms here *)
let str_to_algo env sstr =
  let sl = semicolon_str_to_strlist sstr in
  if sl = [] then
    invalid_algo' "empty@ algorithm@ specification";
  env, List.map
    begin fun s ->
      try match parse_param s with
        | ("sB", options) -> Synthesis (parse_sB env options)
        | ("sRB", options) -> Synthesis (parse_sRB env options)
        | ("sL", options) -> Synthesis (parse_sL env options)
        | ("sk", options) -> Synthesis (parse_sL env options)
        | ("sS", options) -> Synthesis (parse_sS env options)
        | ("sF", options) -> Synthesis (parse_sF env options)
        | ("sD", options) -> Synthesis (parse_sD env options)
        | (_,    options) -> VerifStrat (VerifEngine.str_to_strategy env s)
      with
        | Exit
        | Parse.Lex_error
        | Parsing.Parse_error -> invalid_algo "syntax@ error@ in@ `%s'" s
    end
    sl

(******************************************************************************)
(* run the synthesis algorithm *)
(******************************************************************************)

let mk_synth env = let module E = (val Env.as_module env) in function
  | SynthBoolean params ->
      let module S = Bool (E) in
      S.synthesize params, S.descr params
  | SynthRelationalBoolean params ->
      let module S = RelBool (E) in
      S.synthesize params, S.descr params
  | SynthLimitedLookahead params ->
      let module S = LLA (E) in
      S.synthesize params, S.descr params
  | SynthStandard (params, dom_spec) ->
      let module Dom = (val VerifEngine.mk_domain env dom_spec) in
      let module S = Std (Dom) in
      S.synthesize params, S.descr params
  | SynthSingleFp (params, dom_spec) ->
      let module Dom = (val VerifEngine.mk_domain env dom_spec) in
      let module S = Sfp (Dom) in
      S.synthesize params, S.descr params
  | SynthDisjunct (params, dom_spec, pwextp) ->
      let module DfP = (val VerifEngine.mk_pwext_params pwextp) in
      let module Dom = (val VerifEngine.mk_domain env dom_spec) in
      let module S = Df (Dom) (DfP) in
      S.synthesize params, S.descr params

(* --- *)

(* run the synthesis steps *)
let run env x ((prev_env, algo): spec) =
  (* For now, use physical equality to check that the environment is the
     same… We shall upgrade expressions later. *)
  assert (prev_env == env);
  let rec do_run ((cs, k, cfprog) as x) = function
    | [] -> cs, None, cfprog                                    (* XXX really? *)
    | Synthesis synthesis :: tl ->
        let synth, descr = mk_synth env synthesis in
        Log.i logger "@[%t@]:" descr;
        let cs, res, cfprog = synth x in
        (match res with
          | None -> (Log.i logger "@[%t@ failed.@]" descr;
                    do_run (cs, k, cfprog) tl)
          | Some k -> (Log.i logger "@[%t@ succeeded.@]" descr;
                      if tl = [] then (cs, res, cfprog)
                      else do_run (cs, k, cfprog) tl))
    | VerifStrat s :: tl ->
        let ok, _, cfprog = VerifEngine.run env s
          { cfprog with Program.c_ass = k } in
        do_run (cs, cfprog.Program.c_ass, cfprog) tl
  in
  do_run x algo

(******************************************************************************)
(* run the coreach algorithm *)
(******************************************************************************)

let mk_coreach env = let module E = (val Env.as_module env) in function
  | SynthStandard (params, dom_spec) ->
      let module Dom = (val VerifEngine.mk_domain env dom_spec) in
      let module S = Std (Dom) in
      S.coreach params, S.descr params
  | SynthSingleFp (params, dom_spec) ->
      let module Dom = (val VerifEngine.mk_domain env dom_spec) in
      let module S = Sfp (Dom) in
      S.coreach params, S.descr params
  | SynthDisjunct (params, dom_spec, pwextp) ->
      let module DfP = (val VerifEngine.mk_pwext_params pwextp) in
      let module Dom = (val VerifEngine.mk_domain env dom_spec) in
      let module S = Df (Dom) (DfP) in
      S.coreach params, S.descr params
  | _ ->
    failwith "Unsupported access to coreachability-only algorithm"

(* --- *)

(* run the coreach steps *)
let run_coreach_only env x ((prev_env, algo): spec) =
  (* For now, use physical equality to check that the environment is the
     same… We shall upgrade expressions later. *)
  assert (prev_env == env);
  let rec do_run ((cs, k, cfprog) as x) = function
    | [] ->
      let module E = (val Env.as_module env) in
      cs, E.BEnvNS.BOps.ff, cfprog                             (* XXX really? *)
    | Synthesis synthesis :: tl ->
        let coreach, descr = mk_coreach env synthesis in
        Log.i logger "@[Co-reachability@ analysis@ of@ %t@]:" descr;
        let res, cfprog = coreach x in
        Log.i logger "@[Co-reachability@ analysis@ of@ %t@ done.@]" descr;
        if tl = [] then (cs, res, cfprog)
        else do_run (cs, k, cfprog) tl
    | VerifStrat s :: tl ->
        let ok, _, cfprog = VerifEngine.run env s
          { cfprog with Program.c_ass = k } in
        do_run (cs, cfprog.Program.c_ass, cfprog) tl
  in
  do_run x algo
