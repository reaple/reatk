open Format

let logger = Log.mk "IO"

(* -------------------------------------------------------------------------- *)

let catch_break ~handler f a =
  try
    Sys.catch_break true;
    Sys.set_signal Sys.sigquit (Sys.Signal_handle handler);
    f a
  with
    | Sys.Break -> Log.e logger "SIGINT received."; handler Sys.sigint; exit 1

(* -------------------------------------------------------------------------- *)

type descr = PPrt.ufmt
let pp_descr' fmt = function
  | Some descr -> fprintf fmt descr; pp_print_space fmt ()
  | None -> ()

(* -------------------------------------------------------------------------- *)

type filenaming =
  | Direct of string
  | Base of string * string * string

type backup_policy =
  | Forget
  | BackupCond of (filenaming -> string option) option

type 'a out =
    {
      out_mult: bool;    (* Are multiple calls to `out_exec' allowed? Not really
                            checked. *)
      out_exec: 'a out_func;
    }
and 'a out_func = backup: backup_policy
    -> ?descr: descr -> ?suff: string
      -> string -> 'a * (unit -> unit)                     (* out channel * close *)

(* -------------------------------------------------------------------------- *)

let default_backup_cond ~extra_ext constr =
  let rec tryname i =
    let n = match constr with
      | Direct filename -> sprintf "%s.%s-%d" filename extra_ext i
      | Base (basename, suff, ext) -> sprintf "%s%s-%s-%d.%s" basename suff extra_ext i ext
    in
    if Sys.file_exists n then tryname (succ i) else n
  in
  Some (tryname 1)

let maybe_backup filename constr = function
  | BackupCond c when Sys.file_exists filename ->
      let c = match c with
        | None -> default_backup_cond ~extra_ext:"bak"
        | Some c -> c
      in
      (match c constr with
        | None ->
            Log.i logger "@[Going@ to@ overwrite@ `%s'…@]" filename;
        | Some n ->
            Log.i logger "@[Creating@ backup:@ `%s'@ @<1>→@ `%s'.\
                          @]" filename n;
            (try Sys.rename filename n with
              | Sys_error _ as e ->
                  Log.e logger "@[Unable@ to@ backup@ `%s':@ aborting.\
                                @]" filename;
                  (* Re-raise e as future operations would probably do harm if
                     such renaming fails *)
                  raise e))
  | _ -> ()

let default_sys_error _e =
  Log.e logger "@[Unable@ to@ create@ file@ `%s'@]"

let mk_out open_out
    ~of_oc
    ?(sys_error = default_sys_error)
    ?(flush = fun _ -> ())
    basename
    =
  {
    out_exec = (fun ~backup ?descr ?(suff = "") ext ->
      let filename = asprintf "%s%s.%s" basename suff ext in
      maybe_backup filename (Base (basename, suff, ext)) backup;
      try
        let oc = open_out filename in
        let fmt = of_oc oc in
        Log.i logger "@[Outputting@ %ainto@ `%s'…@]" pp_descr' descr filename;
        fmt, (fun () -> flush fmt; close_out oc)
      with Sys_error _ as e -> sys_error e filename; raise e);
    out_mult = true;
  }

let mk_out' open_out
    ~of_oc
    ?(sys_error = default_sys_error)
    ?(flush = fun _ -> ())
    filename
    =
  {
    out_exec = (fun ~backup ?descr ?suff ext ->
      let filename, backup_policy = match suff with
        | None -> filename, (Direct filename)
        | Some suff ->
            let base = try Filename.chop_extension filename with
              | Invalid_argument _ -> filename in
            let filename = asprintf "%s%s.%s" base suff ext in
            filename, (Base (filename, suff, ext))
      in
      maybe_backup filename backup_policy backup;
      try
        let oc = open_out filename in
        let fmt = of_oc oc in
        Log.i logger "@[Outputting@ %ainto@ `%s'…@]" pp_descr' descr filename;
        fmt, (fun () -> flush fmt; close_out oc)
      with Sys_error _ as e -> sys_error e filename; raise e);
    out_mult = false;
  }

let mk_out_bin = mk_out open_out_bin ~of_oc:(fun oc -> oc) ~flush
let mk_out_bin' = mk_out' open_out_bin ~of_oc:(fun oc -> oc) ~flush
let mk_out ~of_oc = mk_out open_out ~of_oc
let mk_out' ~of_oc = mk_out' open_out ~of_oc

let mk_std o close =
  {
    out_exec = (fun ~backup ?descr ?suff _ ->
      Log.i logger "@[Outputting@ %aonto@ standard@ output…@]" pp_descr' descr;
      o, close o);
    out_mult = true;
  }

let mk_std_fmt = mk_std std_formatter pp_print_flush
let mk_std_oc = mk_std stdout (fun oc () -> flush oc)

let mk_oc basename =
  mk_out
    ~of_oc:(fun oc -> oc)
    basename

let mk_oc' basename =
  mk_out'
    ~of_oc:(fun oc -> oc)
    basename

let mk_fmt =
  mk_out
    ~of_oc:formatter_of_out_channel
    ~flush:(fun fmt -> pp_print_flush fmt ())

let mk_fmt' =
  mk_out'
    ~of_oc:formatter_of_out_channel
    ~flush:(fun fmt -> pp_print_flush fmt ())

let mk_fmt_of_oc { out_exec; out_mult } =
  { out_exec =
      begin fun ~backup ?descr ?suff ext ->
        let oc, close_oc = out_exec ~backup ?descr ?suff ext in
        let fmt = formatter_of_out_channel oc in
        fmt, (fun () -> pp_print_flush fmt (); close_oc ())
      end;
    out_mult; }

(* -------------------------------------------------------------------------- *)

let print_header ?toolname ~version fmt l r =
  let toolname = match toolname with
    | Some t -> t
    | None -> Filename.basename Sys.executable_name
  in
  fprintf fmt "%s@,@[<1> Generated@ by@ %s@ version@ %s.@\n\
                         The@ full@ command@ line@ was:@ @[<h>%a@]@]"
    l
    toolname version
    (PPrt.array_print ~csep:" " ~copen:"" ~cclose:""
       (* XXX: take care of any multiline comment closing in arguments. *)
       pp_print_string) Sys.argv;
  (match r with | "" -> () | r -> fprintf fmt "@ %s@\n" r);
  pp_print_newline fmt ()

type ('a, 'f) pretty_printer =
    ext: string
    -> out_descr: descr
    -> print: (?print_header: ('f -> string -> string -> unit) -> 'f -> 'a -> unit)
    -> ?backup: backup_policy
    -> ?toolname: string
    -> version: string
    -> 'f out
    -> 'a
    -> unit

let mk_output: ('b, formatter) pretty_printer =
  fun ~ext ~out_descr ~print ?(backup=Forget) ?toolname ~version
    mk_fmt e ->
      let fmt, close = mk_fmt.out_exec ~backup ~descr:out_descr ext in
      print ~print_header:(print_header ?toolname ~version) fmt e;
      close ()

let mk_output': ('b, out_channel) pretty_printer =
  fun ~ext ~out_descr ~print ?(backup=Forget) ?toolname ~version
    mk_oc e ->
      let oc, close = mk_oc.out_exec ~backup ~descr:out_descr ext in
      let print_header oc =
        print_header ?toolname ~version (formatter_of_out_channel oc) in
      print ~print_header oc e;
      close ()

(* -------------------------------------------------------------------------- *)
