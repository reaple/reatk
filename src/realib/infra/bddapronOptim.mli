(** *)

open BddapronUtil

(* -------------------------------------------------------------------------- *)

type 'a optim_goal =
  [
  | `Minimize of 'a * string option
  | `Maximize of 'a * string option
  ]

val goal: 'a optim_goal -> 'a
val map_goal: ('a -> 'b) -> 'a optim_goal -> 'b optim_goal

(* -------------------------------------------------------------------------- *)

module GoalUtils: functor (BEnvNS: BDDAPRON_ENV_NS) -> sig
  open BEnvNS.BEnv
  val permute_goal: int array -> ('a numexpr_t optim_goal as 'x) -> 'x
  val permute_goals: int array -> ('a numexpr_t optim_goal list as 'x) -> 'x
  type rel = (nexpr -> nexpr -> bexpr) * Util.relfmt
  val relation_of_goal: 'a optim_goal -> rel
end

(* -------------------------------------------------------------------------- *)

type 'a one_step_goal = 'a optim_goal

module OneStep: functor (BEnvNS: BDDAPRON_ENV_NS) -> sig
  module GoalUtils: module type of GoalUtils (BEnvNS)
  open BEnvNS.BEnv
  type goal = nexpr one_step_goal
  val pp_goal: Format.formatter -> goal -> unit
  val pp_goals: Format.formatter -> goal list -> unit

  val goal_support: var equs_t -> goal -> var PSette.t
  val goals_support: var equs_t -> goal list -> var PSette.t

  val one_step: var vmap -> var equs_t -> goal -> bexpr -> bexpr
end

(* -------------------------------------------------------------------------- *)
