open Env
open Program
open BddapronUtil

let logger = Log.mk ~level:Log.Debug "m."

(* -------------------------------------------------------------------------- *)

type param =
    {
      allow_careset_updates: bool;
      enable_reorderings: bool;
      enable_dynamic_reorderings: bool;
      discard_controllables: bool;
    }

let make_param
    ?(allow_careset_updates = false)
    ?(enable_reorderings = false)
    ?(enable_dynamic_reorderings = false)
    ?(discard_controllables = false)
    () =
  {
    allow_careset_updates;
    enable_reorderings;
    enable_dynamic_reorderings;
    discard_controllables;
  }

module Basic (E: Env.T) = struct
  open E
  open BEnvNS
  open BAll
  open POps

  let run
      { allow_careset_updates;
        enable_reorderings;
        enable_dynamic_reorderings;
        discard_controllables }
      cs
      ({ d_disc_equs = equs; d_final; d_ass; d_reach; d_attract } as df)
      cequs_list
      =
    Log.i logger "@[Merging@ data-flow@ program@ and@ controller@ equations…@]";

    let rs =
      if enable_reorderings && enable_dynamic_reorderings then
        CuddUtil.dynamic_reordering_save_n_select BEnv.env
          !CuddUtil.Options.reorder_policy
      else
        CuddUtil.dynamic_reordering_save_n_stop_if
          (not enable_dynamic_reorderings) BEnv.env
    in

    (* Also substitute controllable variables in group and rank order. *)
    Log.d logger "@[Substituting@ controllable@ variables@ in@ controller@ \
                    equations…@]";
    let cequs = List.flatten cequs_list in
    let cequs, _cl, _k = List.fold_left (fun (cx, cl, k) (c, ce) ->
      let ce = gencof_elim k cl ce in
      (c, ce) :: cx, c :: cl, vsubst k c ce) ([], [], tt) cequs in
    let cequs = List.rev cequs in               (* ← optional; just for debug *)

    Log.d logger "@[Substituting@ controllable@ variables@ in@ data-flow@ \
                    program…@]";
    let vl, el = List.split equs in
    let equs = Log.roll (fun cequs -> List.combine vl (el ///~ cequs)) cequs in

    (* Substitute in predicates *)
    (* no need in init, in principle *)
    (* Invariant (~d_final) becomes part of the assertion. *)
    let ( //~ ) = BOps.vsubsts in
    let d_final = d_final //~ cequs in
    let d_ass = (d_ass //~ cequs) &&~ !~d_final in
    let d_reach = d_reach //~ cequs in
    let d_attract = d_attract //~ cequs in

    let equs =  (* reverse equs to preverve order (again: for debug purposes) *)
      if discard_controllables
      then List.rev equs
      else List.rev_append equs cequs
    in

    if allow_careset_updates then update_careset env;  (* XXX: really useful? *)
    Log.d3 logger "@[Simplifying@ data-flow@ program@ equations@ using@ \
                     careset…@]";
    let equs =
      List.map (fun (v, ve) -> (v, ve ^~~ BEnv.cond.Bdd.Cond.careset)) equs in
    (* Log.i logger "@[<v>%a@]" PPrt.pp_equs equs; *)

    CuddUtil.dynamic_reordering_restore BEnv.env rs;
    if enable_reorderings then
      CuddUtil.reorder_conv BEnv.env;

    let s_vars, cs_groups = CtrlSpec.fold_uc_groups
      begin fun (contrs, cs_groups) CtrlSpec.{ c_vars; bu_vars; nu_vars } ->
        (List.rev_append contrs c_vars,
         CtrlSpec.make_uc_group bu_vars nu_vars [] [] :: cs_groups)
      end (env.s_vars, []) cs
    in
    let env = if discard_controllables then env else respec_vars env s_vars in
    let cs = CtrlSpec.make (List.rev cs_groups) in

    env,
    cs,
    { df with d_disc_equs = equs; d_final; d_ass; d_reach; d_attract }

end

(* -------------------------------------------------------------------------- *)
