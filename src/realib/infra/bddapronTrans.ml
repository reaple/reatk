(* -*- coding: utf-8 -*- *)
(******************************************************************************)
(* BddapronTrans *)
(* author: Nicolas Berthier *)
(* This file is part of ReaTK released under the GNU GPL.  Please read the
   LICENSE file packaged in the distribution *)
(******************************************************************************)

(** BddApron-based Boolean data-flow transformations *)

(**/**)

open BddapronUtil
open Bddapron.Expr0

(* -------------------------------------------------------------------------- *)

module Logger = struct
  let logger = Log.mk "tB"
  let log3 f = Log.cd3 logger f
  let l3_1 f x fmt = log3 f fmt x
  let l3_2 f x y fmt = log3 f fmt x y
end
open Logger

(**/**)

(* -------------------------------------------------------------------------- *)

(** {2 Utilities} *)

(* -------------------------------------------------------------------------- *)

module InputIndep = functor (BEnvNS: BDDAPRON_ENV_NS) -> struct
  module RP = BddapronReorderPolicy.Make (Logger) (BEnvNS.BEnv)
  open BEnvNS.BAll
  open BEnvNS.BEnv
  open BEnvNS.BEnvExt

  (** Internal type encapsulating data. Its main purpose is to help keeping
      signatures "simple", and easily defining operators. *)
  type boolenv =
      {
        forall_i: bexpr -> bexpr;
        to_i': expr -> expr;
        of_i': expr -> expr;
        forall_i': bexpr -> bexpr;
        enums: bexpr;
        envext: extension;
      }

  (* --- *)

  let setup i_vars =
    Log.d logger "Extending environment with extra primed variables.";
    Log.d3 logger "@[I = %a@]" pp_vars i_vars;
    let p_vars, envext, eu = make_primes (varset i_vars) in
    let i'_vars = List.map (fun i -> PMappe.find i p_vars) i_vars in
    let to_i', of_i' = BEnvNS.POps.setup_fast_substs p_vars in
    let i_supp = supp_of_vars env i_vars in
    let i'_supp = supp_of_vars env i'_vars in
    let forall_i = forall' i_supp and forall_i' = forall' i'_supp in
    let enums = full_enum_careset env cond in
    { forall_i; forall_i'; to_i'; of_i'; enums; envext }, eu

  let dispose ?(dirty = false) { envext } =
    if dirty then begin
      Log.d logger "@[Leaving@ environment@ as@ is@ as@ requested@ \
                      (dirty=true).@]";
      no_upgrade
    end else begin
      Log.d logger "Restoring environment.";
      restore_environment envext
    end

  (* --- *)

  (** Restricts the given Boolean expression according to legitimate enumaration
      variables. *)
  let norm_enums { enums } b = (b &&~ enums) ^~ enums

  (* -------------------------------------------------------------------------- *)

  (** {2 Boolean transformation} *)

  (** Type encapsulating data for Boolean transformation. Its main purpose is to
      help keeping signatures "simple". *)
  type trans_data = boolenv

  (* --- *)

  (** Input-independence `pre-image' operator. *)
  let pre_ni ~rp ({ to_i'; forall_i; forall_i' } as be) lp exclude
      : int -> (bexpr * equs) -> (bool * (bexpr * equs)) =
    let norm_enums = norm_enums be in
    let open BEnvNS.POps in
    fun i (x, evols) ->
      (* Log.d3 logger "%t@<5>→pre_ni(%a):" lp print x; *)
      let input_indep, evols' = List.fold_left begin fun (x, evols') (v, e) ->
        (* XXX: take assertion into account! *)
        let e' = to_i' e in
        (* XXX: robust to enumeration inputs? *)
        let x' = forall_i (forall_i' (e ==~~ e' |> norm_enums)) |> norm_enums in
        Log.d3 logger "%a: %a" pp_var v pp_bexpr x';
        x &&~ x', (v, e) :: evols'
      end (exclude, []) evols in
      RP.handle_bdd rp ~lp:Util.pu ~i ~v:"x" input_indep;
      Log.d3 logger "input_indep = %a" pp_bexpr input_indep;
      let changed, evols =
        if is_ff input_indep then false, evols else
          List.fold_left begin fun (chgd, evols') (v, e) ->
            let e' = CuddUtil.dynamic_reordering_suspend1 env
              (fun e -> e //~ evols) (ite input_indep e (var v)) in
            chgd || e <> e', (v, e') :: evols'
          end (false, []) evols'
      in
      Log.d3 logger "changed = %B" changed;
      changed, ((x ||~ input_indep) |> norm_enums, evols)

  (* --- *)

  (**/**)

  let fp pre_ni =
    let rec fp (x, i) = match pre_ni i x with
      | true, x' -> fp (x', succ i)
      | false, x' -> x', i
    in
    fp

  (**/**)

  let trans be
      ~(rp: BddapronReorderPolicy.config)
      ~(evolutions: equs)
      ~(initial: bexpr)
      ~(assertion: bexpr)
      : bexpr * equs =

    let rp = RP.make rp in
    let pre_ni = pre_ni ~rp be Util.pu !~initial in

    (* Main computation, starting from empty space (ff): *)
    let (x, evols), i = Log.roll (fp pre_ni) ((ff, evolutions), 0) in
    Log.i logger "@[Reached@ fixpoint@ after@ %i@ iteration%s.@]"
      i (if i > 1 then "s" else "");

    (* let evols = List.map (fun (v, e) -> v, e ^~~ !~x) evols in *)

    RP.dispose rp;

    x, evols

end

(* -------------------------------------------------------------------------- *)

module InputElim = functor (BEnvNS: BDDAPRON_ENV_NS) -> struct
  module RP = BddapronReorderPolicy.Make (Logger) (BEnvNS.BEnv)
  open BEnvNS.BAll
  open BEnvNS.BEnv
  open BEnvNS.BEnvExt

  (** Internal type encapsulating data. Its main purpose is to help keeping
      signatures "simple", and easily defining operators. *)
  type boolenv =
      {
        i_supp: support;
        bint_objectives: [ `Max | `Min ] vmap;
      }

  (* --- *)

  let setup bint_objectives i_vars =
    Log.d3 logger "@[I = %a@]" pp_vars i_vars;
    let num_vars = List.filter is_num i_vars in
    if num_vars <> [] then begin
      Log.e logger "Unsupported@ elimination@ of@ numerical@ variables@ %a"
        pp_vars num_vars;
    end;
    { i_supp = supp_of_vars env i_vars; bint_objectives }

  (* -------------------------------------------------------------------------- *)

  (* module BddMap = BEnvNS.BddMap *)

  (* let elim_in_bint { i_supp } (Bdd.Int.{ signed; reg } as i) = *)
  (*   let addl g l m = *)
  (*     try let l' = BddMap.find g m in *)
  (*         let lgtl' = Bdd.Int.greater cuddman l l' in *)
  (*         BddMap.add g (Bdd.Int.ite lgtl' l l') m *)
  (*     with *)
  (*       | Not_found -> BddMap.add g l m *)
  (*   in *)
  (*   let gl = Bdd.Int.fold_guardvalues cuddman *)
  (*     (fun g -> addl (exist' i_supp g)) i BddMap.empty in *)
  (*   let min = fst (Helpers.BInt.bint_min_max signed (Bdd.Reg.width reg)) in *)
  (*   BddMap.fold Bdd.Int.ite gl *)
  (*     (Bdd.Int.of_int cuddman signed (Bdd.Reg.width reg) min) *)

  (* let elim_in_benum { i_supp } e = *)
  (*   let addl g l m = *)
  (*     try let l' = BddMap.find g m in *)
  (*         if l' <> l then failwith "aaarggh!" else m *)
  (*     with *)
  (*       | Not_found -> BddMap.add g l m *)
  (*   in *)
  (*   let gl = Bdd.Enum.fold_guardvalues env *)
  (*     (fun g -> addl (exist' i_supp g)) e BddMap.empty in *)
  (*   BddMap.fold Bdd.Enum.ite gl (BddMap.choose gl |> snd) *)

  open Format
  module Guard = struct let print = pp_bexpr end

  (* --- *)

  module Labels = Helpers.MakeSet (struct
    type t = var
    let compare = symbol.Bdd.Env.compare
    let print = symbol.Bdd.Env.print
  end)

  let elim_in_bint
      { i_supp; bint_objectives }
      v (Bdd.Int.{ signed; reg } as i)
      =
    let o = try PMappe.find v bint_objectives with Not_found -> `Max in
    let module MTBDD4Bints = MtbddUtil.MTBDDOf (Guard) (struct
      type man = [ `Bint of bool * int ]
      type t = Bdd.Int.vt
      let hash = Hashtbl.hash
      let equal = ( = )
      let print = Bddapron.Expr0.Bint.print env cond
      let empty (`Bint (s, w)) =
        let sel = match o with `Max -> fst | `Min -> snd in
        Bdd.Int.of_int cuddman s w (sel (Helpers.BInt.bint_min_max s w))
      let merge _ l l' =
        let l, l' = match o with `Max -> l, l' | `Min -> l', l in
        Bdd.Int.ite (Bdd.Int.greater cuddman l l') l l'
      let simplify_rec _ s _ = s
    end) in
    let man = MTBDD4Bints.make cuddman (`Bint (signed, Bdd.Reg.width reg)) in
    let addl g l m = MTBDD4Bints.ite g (MTBDD4Bints.leaf man l) m in
    let gl = Bdd.Int.fold_guardvalues cuddman
      (fun g -> addl (exist' i_supp g)) i (MTBDD4Bints.empty man) in
    MTBDD4Bints.fold_guardleaves Bdd.Int.ite gl (MTBDD4Bints.pick_leaf gl)

  (* --- *)

  module MTBDD4Benums = MtbddUtil.MTBDDOf (Guard) (struct
    include Labels
    type man = unit
    let equal a b = compare a b = 0
    let hash = Hashtbl.hash
    let empty () = empty
    let merge () = union
    let simplify_rec () s _ = s
  end)

  let elim_in_benum { i_supp } e =
    let man = MTBDD4Benums.make cuddman () in
    let leaf l =
      if Labels.cardinal l = 1
      then Bdd.Enum.of_label env (Labels.choose l)
      else failwith @@ asprintf "Unable to eliminate inputs in guarded \
                                 enumeration expression: cannot join labels \
                                 %a" Labels.print l
    in
    match
      Bdd.Enum.fold_guardlabels env begin fun g l ->
        let g = exist' i_supp g in
        let l = MTBDD4Benums.leaf man (Labels.singleton l) in
        function
          | None -> Some l
          | Some e -> Some (MTBDD4Benums.ite g l e)
      end e None
    with
      | Some gl ->
          MTBDD4Benums.fold_guardleaves
            (fun c l -> Bdd.Enum.ite c (leaf l)) gl
            (leaf (MTBDD4Benums.pick_leaf gl))
      | None ->
          failwith @@ asprintf "Unable to eliminate inputs in guarded \
                                enumeration expression: ending up with an emty \
                                set of labels"

  (* --- *)

  let elim ({ i_supp } as be) = function
    | v, `Bool b -> v, `Bool (exist' i_supp b)
    | v, `Bint i -> v, `Bint (elim_in_bint be v i)
    | v, `Benum e -> v, `Benum (elim_in_benum be e)
    | v, _ -> failwith "Unsupported expression in input elimination"

  let trans
      ~(init: bexpr)
      ~(assertion: bexpr)
      ~(final: bexpr)
      ~(evolutions: equs)
      ?(bint_objectives = PMappe.empty symbol.Bdd.Env.compare)
      vars
      : bexpr * bexpr * bexpr * equs
      =

    let { i_supp } as be = setup bint_objectives vars in
    () |> Log.roll (fun () ->
      exist' i_supp init,
      exist' i_supp assertion,
      exist' i_supp final,                                          (* exist? *)
      List.map (elim be) evolutions)

end

(* -------------------------------------------------------------------------- *)
