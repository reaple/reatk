open Format
open BddapronUtil

(* -------------------------------------------------------------------------- *)

module Logger = struct
  let logger = Log.mk "tB"
  let log3 f = Log.cd3 logger f
  let l3_1 f x fmt = log3 f fmt x
  let l3_2 f x y fmt = log3 f fmt x y
end
open Logger

(* -------------------------------------------------------------------------- *)

type 'a mii_param_t =
    {
      c_rp: BddapronReorderPolicy.config;
      c_dirty: bool;
    }

let make_mii_param ?(dirty = false) ~rp =
  { c_rp = rp; c_dirty = dirty }

(** Merging input-indenpendent evolutions/states *)
module MergeInputIndep (E: Env.T) = struct
  open E
  open BEnvNS.BEnvExt
  open BEnvNS.BOps
  module II = BddapronTrans.InputIndep (BEnvNS)

  type param = var mii_param_t

  let descr _ fmt =
    fprintf fmt "Merging@ input-independent@ states"

  (* --- *)

  let maybe_upgrade cs k cfprog eu =
    upgrade CtrlSpec.apply_env_permutation eu cs,
    upgrade BddapronUtil.permute_boolexpr eu k,
    upgrade Program.apply_env_permutation_to_cfprog eu cfprog

  (** XXX: Operates on single self-loop programs for now *)
  let trans { c_rp; c_dirty = dirty } (cs, a, cfprog) =
    if not (Program.cfprog_is_a_self_loop cfprog) then begin
      Log.w logger "@[Program@ is@ @not@ a@ self-loop:@ ignoring.@]";
      ff, (cs, a, cfprog), None
    end else begin
      let be, eu = II.setup env.Env.i_vars in
      let cs, assertion, cfprog = maybe_upgrade cs a cfprog eu in
      let input_indep, c_disc_equs =
        II.trans be ~rp:c_rp
          ~evolutions:cfprog.Program.c_disc_equs
          ~initial:cfprog.Program.c_init
          ~assertion
      in
      let cfprog = Program.{ cfprog with c_disc_equs } in
      let eu' = II.dispose ~dirty be in
      upgrade BddapronUtil.permute_boolexpr eu' input_indep,
      maybe_upgrade cs assertion cfprog eu',
      upgrade_to_perms (compose_upgrades eu eu')
    end

  (* --- *)

  let maybe_upgrade cs dfprog eu =
    upgrade CtrlSpec.apply_env_permutation eu cs,
    upgrade Program.apply_env_permutation_to_dfprog eu dfprog

  let df_trans { c_rp; c_dirty = dirty } (cs, dfprog) =
    let be, eu = II.setup env.Env.i_vars in
    let cs, dfprog = maybe_upgrade cs dfprog eu in
    let input_indep, d_disc_equs =
      II.trans be ~rp:c_rp
        ~evolutions:dfprog.Program.d_disc_equs
        ~initial:dfprog.Program.d_init
        ~assertion:dfprog.Program.d_ass
    in
    let dfprog = Program.{ dfprog with d_disc_equs } in
    let eu' = II.dispose ~dirty be in
    upgrade BddapronUtil.permute_boolexpr eu' input_indep,
    maybe_upgrade cs dfprog eu',
    upgrade_to_perms (compose_upgrades eu eu')

end

(* -------------------------------------------------------------------------- *)

type 'a ei_param_t =
    {
      ei_vars: Env.vars_t;
    }

let make_ei_param vars = { ei_vars = vars }

(** Elimination of input variables in finite-state dataflow systems (Bounded
    Integer state variables are maximized) *)
(* TODO: add parameters to allow tuning behavior for Bounded Integers *)
module ElimInputs (E: Env.T) = struct
  open E
  open BEnvNS.PPrt
  open BEnvNS.BEnvExt
  open BEnvNS.BOps
  module EI = BddapronTrans.InputElim (BEnvNS)

  type param = var ei_param_t

  let descr { ei_vars } fmt =
    fprintf fmt "Elimination@ of@ inputs:@ %a" pp_vars ei_vars

  (* --- *)

  let df_trans { ei_vars } (cs, dfprog) =
    let d_init, d_ass, d_final, d_disc_equs =
      EI.trans
        ~init:dfprog.Program.d_init
        ~assertion:dfprog.Program.d_ass
        ~final:dfprog.Program.d_final
        ~evolutions:dfprog.Program.d_disc_equs
        ei_vars
    in
    cs, Program.{ dfprog with d_init; d_ass; d_final; d_disc_equs }

end

(* -------------------------------------------------------------------------- *)

type 'a eo_param_t =
    {
      eo_vars: Env.vars_t;
    }

let make_eo_param vars = { eo_vars = vars }

(** Elimination of output variables *)
module ElimOutputs (E: Env.T) = struct
  open E
  open BEnvNS.PPrt
  open BEnvNS.POps
  module Elim = DomExt.DefaultElimUtils (Logger) (E)
  open Elim

  type param = var eo_param_t

  let descr { eo_vars } fmt =
    fprintf fmt "Elimination@ of@ outputs:@ %a" pp_vars eo_vars

  (* --- *)

  let depends_on vars e = depends_on_any_var e vars

  let elim_in_evols vars =
    List.fold_left begin fun acc (v, e) ->
      if PSette.mem v vars then acc
      else if depends_on vars e
      then (let esupp = support e in
            Log.e logger "@[Unable@ to@ eliminate@ variables@ in@ equation@ \
                            for@ `%a':@ %a@ is/are@ actually@ state@ or@ \
                            input@ variable(s)\
                          @]" pp_var v pp_vset (PSette.inter esupp vars);
            failwith "Error in variable elimination")
      else (v, e) :: acc
    end []

  (* --- *)

  let controller { eo_vars } k =
    repr k |> exist ~lv:"K" eo_vars |> retr

  let df { eo_vars } df =
    let elimb ~lv x = repr x |> exist ~lv eo_vars |> retr in
    let elimb' ~lv x = repr x |> forall ~lv eo_vars |> retr in
    Program.{ df with
      d_init = elimb ~lv:"I" df.d_init;
      d_ass = elimb ~lv:"A" df.d_ass;
      d_final = elimb' ~lv:"@<1>𝔉" df.d_final;
      d_reach = elimb ~lv:"@<1>ℜ" df.d_reach;
      d_attract = elimb ~lv:"@<1>𝔄" df.d_attract;
      d_disc_equs = elim_in_evols (varset eo_vars) df.d_disc_equs;
    }

  let new_env { eo_vars = vars } = Env.elim_vars env vars

end

(* -------------------------------------------------------------------------- *)
