(* This file is part of ReaTK released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)

(** framework base: synthesis *)

(* -------------------------------------------------------------------------- *)

(** Logico-numerical synthesis result; a Boolean expression encoding the whole
    controller predicate *)
type controller = Env.boolexpr_t

(** Synthesis Interface: function type; a [(res, get)] result gathers a Boolean
    flag indicating the success of the synthesis, plus one callback for getting
    the resulting set of equations. *)
type synth = CtrlSpec.t * controller * Program.cfprog_t ->
  CtrlSpec.t * controller option * Program.cfprog_t

(** Coreachability only *)
type coreach = CtrlSpec.t * controller * Program.cfprog_t ->
  Env.boolexpr_t * Program.cfprog_t

(* -------------------------------------------------------------------------- *)
(** {2 Synthesis Interface } *)

(** Parametrizable synthesis module *)
module type T0 = sig
  type param                                         (** synthesis parameters *)
  val synthesize: param -> synth                      (** synthesis *)
  val descr: param -> Format.formatter -> unit
end

module type T = sig
  include T0
  val coreach: param -> coreach                        (** coreachability only *)
end

(* -------------------------------------------------------------------------- *)
