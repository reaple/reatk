(* This file is part of ReaTK released under the GNU GPL.
   Please read the LICENSE file packaged in the distribution *)
(** synthesis implementation: over-approximating and Boolean algorithms *)

(**/**)

module Logger = struct
  let level = Log.Debug3
  let logger = Log.mk ~level "s."
end
open Logger
open Util.P

let pp_locids fmt s =
  Util.list_print'
    ~copen:(pp "[@[<v>")
    ~csep:(pp ";@;")
    ~cclose:(pp "@]]")
    Format.pp_print_int fmt (PSette.elements s)

let lv' = Util.pc '^'

(* -------------------------------------------------------------------------- *)

module type SETS = sig
  module E: Env.T
  open E.BEnv
  val dead: bexpr -> bexpr
  val struth: bexpr -> bexpr
  val iguard: bexpr -> bexpr
  val cguards: CtrlSpec.t -> bexpr -> bexpr list
end

(** Module gatherring helpers for computing various sets of transitions, and the
    set of deadlocking states based on an assertion on states and inputs. *)
module Sets (Logger: Log.T) (E: Env.T) : SETS with module E = E = struct
  module E = E
  module Elim = DomExt.DefaultElimUtils (Logger) (E)
  open Logger
  open Elim

  let dead assertion =
    Log.i logger "Computing the original set of deadlocking states.";
    repr assertion |> nexist ~lv:"I" ~ls:(Util.pc 'A') E.env.Env.i_vars |> retr

  let struth assertion =
    Log.i logger "Computing the assertion on states.";
    repr assertion |> exist ~lv:"I" ~ls:(Util.pc 'A') E.env.Env.i_vars |> retr

  let iguard assertion =
    Log.i logger "Computing the guard on inputs.";
    repr assertion |> exist ~lv:"S" ~ls:(Util.pc 'A') E.env.Env.s_vars |> retr

  let cguards cs assertion =
    Log.i logger "Computing restrictions on controllable inputs.";
    let al, _ = CtrlSpec.foldi_uc_groups_rev
      begin fun i (acc, a) CtrlSpec.{ c_vars; u_vars } ->
        let a' = forall ~lv:"C" ~ld:(Util.pp1 "X%i" i) c_vars a in
        retr a' :: acc, exist ~lv:"I" (c_vars @ u_vars) a
      end ([], repr assertion) cs in
    List.rev al

end

(* -------------------------------------------------------------------------- *)

type ('a, 'p, 'x) _common_data =
    {
      ucvars: ('a * 'a) list;                       (* U/C groups (from n to 1) *)
      evolutions: 'x;
      init: 'p;
      assertion: 'p;
      iguard: 'p;
      cguards: 'p list;
      oldassert: bool;
      struth: 'p;
    }

module type COMMON = sig
  module E: Env.T
  module Logger: Log.T
  module Sets: SETS with module E = E
  open E.BEnvNS.BAll

  type data = (vars, bexpr, equs) _common_data
  type common_data = data

  val setup: oldassert:bool -> CtrlSpec.t * bexpr * Program.cfprog_t
    -> common_data
end

module Common (Logger: Log.T) (E: Env.T) : COMMON with module E = E = struct
  module E = E
  module Logger = Logger
  module Sets = Sets (Logger) (E)
  open E.BEnvNS.BAll

  type data = (vars, bexpr, equs) _common_data
  type common_data = data

  let setup ~oldassert (cs, assertion, { Program.c_disc_equs = evolutions;
                                         Program.c_init = init }) =
    let ucvars = CtrlSpec.uc_groups cs |> List.rev in
    let iguard = Sets.iguard assertion in
    let struth =
      if oldassert
      then tt
      else Sets.struth assertion
    in
    Log.d3 logger "@<4>𝔗 = %a" print struth;
    let cguards =
      if oldassert
      then List.rev_map (fun _ -> ff) (CtrlSpec.uc_groups cs)
      else Sets.cguards cs assertion
    in
    let init = init ^~ struth in
    { ucvars; evolutions; init; assertion; iguard; cguards; oldassert; struth }
end

(* -------------------------------------------------------------------------- *)

type ('d, 'c) _cfdata = { dd: 'd; cfg': 'c; }

module type CF_DATA = sig
  include COMMON
  open E.BEnvNS.BAll
  type cfdata = (common_data, Cfg.t) _cfdata
  val cfsetup: oldassert:bool -> CtrlSpec.t * bexpr * Program.cfprog_t -> cfdata
end

module CfData (Logger: Log.T) (E: Env.T) : CF_DATA with module E = E = struct
  module Common = Common (Logger) (E)
  include Common
  type cfdata = (common_data, Cfg.t) _cfdata
  let cfsetup ~oldassert ((cs, assertion, { Program.c_cfg = cfg }) as dfsystem) =
    { dd = Common.setup ~oldassert dfsystem;
      cfg' = Cfg.transpose cfg }
end

(* -------------------------------------------------------------------------- *)

module type FP_STEP = sig
  module Common: COMMON
  module Dom: Domain.T with module E = Common.E
  type data
  val fname: data -> Util.pu
  val apply
    : ?allow_shortcut:bool -> data -> lp:Util.pu -> ?lv:Util.pu -> Dom.t -> Dom.t
end

module type CF_FP_STEP = sig
  include FP_STEP
  module CfData: CF_DATA with module E = Dom.E
  val setup: ?fname:Util.pu -> CfData.cfdata -> data
  val apply: data -> lp:Util.pu -> ?lv:Util.pu -> Cfg.arcid_t -> Dom.t -> Dom.t
end

(* -------------------------------------------------------------------------- *)

(** Generic module for fixpoints operating on control-flow programs with
    abstract domains of kind {Domain.T} *)
module CfFp
  = functor (Step: CF_FP_STEP) ->
struct

  module Step = Step
  module Dom = Step.Dom
  module CfData = Step.CfData
  module Common = CfData
  open CfData
  open Logger
  open E
  open E.BEnvNS.BAll
  open Fixpoint

  (**/**)

  (** Creates the manager for the fixpoint module *)
  let make_lfp_man ?(fwcs=10) ?(wc=`Default) lp sd init =
    (* let d2 = Log.check_level logger Log.Debug2 *)
    (* and d3 = Log.check_level logger Log.Debug3 in *)
    let d2 = false and d3 = false in
    let ti_timed = Log.ti_timed ~period:0.1 ~p:Log.Debug2 logger in
    {
      bottom = (fun v -> Dom.bottom);
      is_bottom = (fun v -> Dom.is_bottom);
      is_leq = (fun v -> Dom.is_leq);
      join = (fun v -> Dom.join);
      join_list = (fun v -> List.fold_left Dom.join Dom.bottom);
      widening = begin let iter = ref 0 in fun v s1 s2 ->
        incr iter;
        ti_timed "%tWidening n@<1>º%d at location %d@<1>…" lp !iter v;
        let s1, s2 = match Dom.widening_force_conv wc with
          | None -> s1, s2
          | Some fwc when !iter >= fwcs -> fwc ~lp s1 s2
          | Some _ ->
              Log.i logger "%t@[Postponing@ enforcement@ of@ widening@ \
                                convergence.@]" lp; s1, s2
        in
        let r = Dom.widening s1 s2 in
        Log.d3 logger
          "%t@[<v>Wi@[<v>dening details:@\n%a,@;%a@]@\n\
           @<2>→ %a@]" lp Dom.print s1 Dom.print s2 Dom.print r;
        r
      end;

      canonical = (fun v -> Dom.canonicalize);
      odiff = None;
      apply = begin let apply = Step.apply sd ~lp in fun a t ->
        assert (Array.length t == 1);
        (), apply a t.(0)
      end;
      arc_init = (fun h -> ());
      abstract_init = init;
      accumulate = true;

      print_abstract = Dom.print;
      print_arc = (fun fmt () -> ());
      print_vertex = Format.pp_print_int;
      print_hedge = Format.pp_print_int;

      print_fmt = Format.err_formatter;
      print_analysis    = d2;
      print_component   = d3;
      print_step        = d2;
      print_state       = d2;
      print_postpre     = d3;
      print_workingsets = d3;

      dot_fmt = None;
      dot_vertex = (fun fmt v -> ());
      dot_hedge = (fun fmt h -> ());
      dot_attrvertex = (fun fmt v -> ());
      dot_attrhedge = (fun fmt h -> ());
    }

  (* --- *)

  let setup_dot_printer fpman cfg filename =
    let dot_channel = open_out filename in              (* XXX won't be closed. *)
    let dotfmt = Format.formatter_of_out_channel dot_channel in
    fpman.dot_fmt <- Some dotfmt;
    fpman.dot_vertex <- Format.pp_print_int;
    fpman.dot_hedge <- Format.pp_print_int;
    fpman.dot_attrvertex <- begin fun fmt locid ->
      (* Format.pp_print_string fmt ("("^(string_of_int locid)^")\n"); *)
      let loc = Cfg.get_loc cfg locid in
      let strfmt = Format.str_formatter in
      Loc.print env strfmt loc;
      let str = Format.flush_str_formatter () in
      Format.pp_print_string fmt (Util.string_compact str)
    end;
    fpman.dot_attrhedge <- begin fun fmt arcid ->
      (* Format.pp_print_string fmt ("("^(string_of_int arcid)^") "); *)
      let arc = Cfg.get_arc cfg arcid in
      Arc.print_type env fmt arc;
      Format.pp_print_string fmt "\n";
      let strfmt = Format.str_formatter in
      Arc.print env strfmt arc;
      let str = Format.flush_str_formatter () in
      let nl = Str.regexp_string "\\n" in
      Format.pp_print_string fmt                     (* strange behavior in Arc *)
        (Str.global_replace nl "\n" (Util.string_compact str))
    end

  (* --- *)

  let make_strategy ~ws ~wd =
    Fixpoint.make_strategy_default ~widening_start:ws ~widening_descend:wd
      ~vertex_dummy:Cfg.locid_dummy ~hedge_dummy:Cfg.arcid_dummy

  (* --- *)

  type params =
      {
        ws: int;
        wd: int;
        fwcs: int option;
      }
  type data =
      {
        params: params;
        fname: Util.pu option;
        cd: cfdata;
        step_data: Step.data;
      }

  (* --- *)

  let setup ?fname ~ws ~wd ?fwcs cd =
    { params = { ws; wd; fwcs }; fname; cd; step_data = Step.setup cd }

  let fname { fname; step_data } = match fname with
    | Some fname -> fname
    | None -> Util.pp1 "lfp(%t)" (Step.fname step_data)

  (* --- *)

  let mk_map cfg' f =
    PSHGraph.fold_vertex cfg'
      (fun locid _ ~pred:_ ~succ:_ -> Mappe.add locid (f locid))
      Mappe.empty

  (** Least fixpoint computation *)
  let apply' { params = { ws; wd; fwcs }; cd = { cfg' }; step_data }
      ~lp init_locs loc_init =
    mk_map cfg' @@ if PSette.is_empty init_locs then begin
      Log.i logger "%t@[Skipping@ least@ fixpoint@ computation:@ \
                        empty@ inits.@]" lp;
      loc_init
    end else begin
      let fpman = make_lfp_man ?fwcs lp step_data loc_init in
      let strat = make_strategy ~ws ~wd cfg' init_locs in
      let r = analysis_std fpman cfg' init_locs strat in
      (* Considering that the location identifiers of the resulting CFG match
         with the original one, let's try to build a map, so that the whole
         resulting graph (with its attached formulas) can be garbage
         collected. *)
      PSHGraph.attrvertex r
    end

  (* --- *)

  let accumulate acc i ?(allow_shortcut = true)
      ({ cd = { cfg' = cfg; dd = { init; assertion; oldassert } } } as sd)
      ~lp ?(lv = lv') map =
    let fname = Util.pp2 "%t(%t)" (fname sd) lv in
    let lvl =
      if PSHGraph.size_vertex cfg = 1
      then fun fmt _locid -> fname fmt
      else fun fmt locid -> Format.fprintf fmt "%t.%d" fname locid
    in
    PSHGraph.fold_vertex cfg begin fun locid loc ~pred:_ ~succ:_ b ->
      let abad = Mappe.find locid map in
      (if not (Dom.is_bottom abad) then Log.d2 else Log.d3)
        logger "%t%a = %a" lp lvl locid Dom.print abad;

      let abad_in_loc = Dom.meet_condition (Loc.get_inv loc) abad in
      if not allow_shortcut then
        acc abad_in_loc b
      else
        let inbad = Dom.meet_condition init abad_in_loc in
        if Dom.is_bottom inbad then
          (Log.d3 logger "%t%a ⊓. Θ = ⊥" lp lvl locid;                  (* ok *)
           acc abad_in_loc b)
        else                                                      (* shortcut *)
          (if Log.check_level logger Log.Debug2
           then Log.d2 logger "%t%a @[= %a@\n@<2>⊒ %a@]\
                              " lp lvl locid Dom.print abad Dom.print inbad
           else Log.i logger "%t%a @<2>⊒ %a" lp lvl locid Dom.print inbad;
           raise Exit)
    end i

  let concretize_accumulate =
    accumulate (fun l b -> b ||~ Dom.to_boolexpr l) ff
  let abstract_accumulate ?allow_shortcut cd ~lp =
    accumulate begin fun a b ->
      Log.d2 logger "%t@[<v>  %a@\n⊔ %a@]@<1>…" lp Dom.print a Dom.print b;
      Dom.join a b
    end Dom.bottom ?allow_shortcut cd ~lp

  (* --- *)

  let apply ?allow_shortcut ({ cd = { cfg' } } as sd) ~lp ?lv ainit =
    let concr_init = Dom.to_boolexpr ainit in
    let init_locs = Cfg.get_locidset_by_inv env cfg' concr_init in
    let vertex_init inv = Dom.meet_condition inv ainit in
    let loc_init v = vertex_init (Loc.get_inv (Cfg.get_loc cfg' v)) in
    apply' sd ~lp init_locs loc_init |>
        abstract_accumulate ?allow_shortcut sd ~lp ?lv

end

(* -------------------------------------------------------------------------- *)

module FpSplit (Logger: Log.T)
  = functor (Fp: FP_STEP) ->
struct
  include Fp
  open Logger

  let apply_mult ?allow_shortcut sd ~lp ~lvi adisj =
    let _, disj = List.fold_left begin fun (i, disj) abad ->
      let lp fmt = Format.fprintf fmt "%t%d: " lp i in
      (* let li fmt = Format.fprintf fmt "." (\* ".%d" i *\) in *)
      Log.d logger "%t%a = %a" lp lvi i Dom.print abad;
      let lv fmt = lvi fmt i in
      (* let lv = Scanf.format_from_string (Format.asprintf lvi i) "" in *)
      pred i, (apply ?allow_shortcut sd (* ~li *) ~lp ~lv abad) :: disj
    end (List.length adisj, []) adisj in
    disj
end

(* --- *)

module CfFpSplit
  = functor (Step: CF_FP_STEP) ->
struct
  module Fp = CfFp (Step)
  include FpSplit (Step.CfData.Logger) (Fp)
  let setup = Fp.setup
end

(* -------------------------------------------------------------------------- *)

module DblFp (Logger: Log.T)
  = functor (OStep: FP_STEP) ->
    functor (IStep: FP_STEP with module Common = OStep.Common
                            and  type Dom.t = OStep.Dom.t) ->
struct

  module Common = OStep.Common
  module Dom = OStep.Dom
  open Logger

  (* --- *)

  type params =
      {
        ws: int;
      }
  type data =
      {
        params: params;
        fname: Util.pu option;
        ostep_data: OStep.data;
        istep_data: IStep.data;
      }

  let setup ?fname ~ws ostep_data istep_data =
    { params = { ws }; fname; ostep_data; istep_data }

  let fname { fname; ostep_data = od; istep_data = id } = match fname with
    | Some fname -> fname
    | None -> Util.pp2 "lfp(%t@<1>∘%t)" (OStep.fname od) (IStep.fname id)

  let apply ?(allow_shortcut = true)
      ({ params = { ws }; ostep_data; istep_data } as sd) ~lp =
    let ostep = OStep.apply ~allow_shortcut ostep_data in
    let istep = IStep.apply ~allow_shortcut istep_data in

    let rec lfp j ?(lv = lv') x =
      let lp = pp2 "%t#%d: " lp j in
      let x' = istep ~lp ~lv x |> Dom.join x in
      let lv = lv' in
      (* let lp: Util.pu = pp2 "%t#%d: " lp j in *)

      (* Compute the union with unavoiding predecessors to take all disjuncts
         into account at once. *)
      let x' = Dom.join x' (ostep ~lp ~lv x') in
      Log.d2 logger "%t^@<1>⊔%t(^) = %a" lp (OStep.fname ostep_data)
        Dom.print x';

      let x' =
        try
          let x' = if j >= ws then Dom.widening x x' else x' in
          if allow_shortcut && Dom.is_top x' then raise Exit else x'
        with
          | Exit -> Log.e logger "%t%t(^) = ⊤" lp (fname sd); raise Exit
      in

      if Dom.is_eq x x' then x' else lfp (succ j) ~lv x'
    in

    lfp 1

end

(* -------------------------------------------------------------------------- *)

module DfFp (Logger: Log.T)
  = functor (Step: FP_STEP) ->
struct

  module Common = Step.Common
  module Dom = Step.Dom
  open Logger

  (* --- *)

  type params =
      {
        ws: int;
      }
  type data =
      {
        params: params;
        fname: Util.pu option;
        step_data: Step.data;
      }

  let setup ?fname ~ws step_data =
    { params = { ws }; fname; step_data }

  let fname { fname; step_data = sd } = match fname with
    | Some fname -> fname
    | None -> Util.pp1 "lfp(%t)" (Step.fname sd)

  let apply ?(allow_shortcut = true)
      ({ params = { ws }; step_data } as sd) ~lp =
    let step = Step.apply ~allow_shortcut step_data in

    let rec lfp j ?(lv = lv') x =
      let lp = pp2 "%t#%d: " lp j in
      let x' = step ~lp ~lv x |> Dom.join x in
      let lv = lv' in
      let x' =
        try
          if allow_shortcut && Dom.is_top x' then raise Exit else x'
        with
          | Exit -> Log.e logger "%t%t(^) = ⊤" lp (fname sd); raise Exit
      in
      if Dom.is_eq x x' then x' else lfp (succ j) ~lv x'
    in

    lfp 1

end

(* -------------------------------------------------------------------------- *)

module DfPre_u
  = functor (Common: COMMON) ->
    functor (Dom: Domain.T with module E = Common.E) ->
struct

  module Dom = Dom
  module Common = Common
  open Common.E.BEnvNS.BOps
  open Common
  open Logger

  let pre_open ?(allow_dynamic_reorderings = false) evols
      ?(meet_iguard = fun x -> x) x =
    (if allow_dynamic_reorderings then fun x -> x
     else CuddUtil.dynamic_reordering_suspend1 E.BEnv.env)
      (Dom.substitute_lexpr (meet_iguard x)) evols

  (** [apply' ... a x] computes in the abstract domain the states from which a
      non-controllable transition can lead to a state belonging to γ([x])
      through arc [a]. *)
  let apply' ?allow_dynamic_reorderings ucvars cguards guard evols
      ?meet_iguard ?(meet_src_invar = fun x -> x) ~lp ?(lv = lv') x' =

    Log.d3 logger "%t  %t  = %a" lp lv Dom.print x';
    let x = pre_open ?allow_dynamic_reorderings evols ?meet_iguard x' in
    let x = Dom.meet_condition guard x in
    Log.d3 logger "%t <%t> = %a" lp lv Dom.print x;

    let x = List.fold_left2 begin fun (x, i) (uvars, cvars) nai ->
      let f = Dom.forall_bool_list x cvars in
      Log.d3 logger "%t@<9>  ∀c^  = @[%a@]" lp Dom.print f;

      let x = if Dom.is_top nai then f else
          let g = Dom.join nai (Dom.forget_list x cvars) in
          Log.d3 logger "%tX%i@<7>⇒∃c^ = @[%a@]" lp i Dom.print g;
          Dom.meet f g
      in

      let x = Dom.forget_list x uvars in
      Log.d3 logger "%t@<9> ∃u^∩' = @[%a@]" lp Dom.print x;

      x, succ i
    end (x, 0) ucvars cguards |> fst in

    let x = meet_src_invar x in
    Log.d3 logger "%tpre^ = %a" lp Dom.print x;
    x

  type data =
      {
        dd: common_data;
        fname: Util.pu;
        allow_dynamic_reorderings: bool option;
        cguards: Dom.t list;
      }
  let setup ?allow_dynamic_reorderings ?(fname = Util.ps "Pre_u") dd =
    { dd; fname;
      allow_dynamic_reorderings;
      cguards = List.map (fun ai -> Dom.of_boolexpr !~ai) dd.cguards; }
  let fname { fname } = fname

  let apply ?allow_shortcut
      { dd = { ucvars; evolutions; assertion; iguard; oldassert };
        allow_dynamic_reorderings;
        cguards; } =
    let meet_iguard, guard = if oldassert
      then Some (Dom.meet_condition iguard), assertion
      else None, tt
    in
    let apply' = apply' ?allow_dynamic_reorderings
      ucvars cguards guard evolutions ?meet_iguard in
    let ti_timed = Log.ti_timed ~period:0.1 ~p:Log.Debug logger in
    fun ~lp ?lv x' ->
      ti_timed "%tComputing global non-controllable predecessors@<1>…" lp;
      apply' ~lp ?lv x'

end

(* --- *)

module LocPre_u
  = functor (Common: COMMON) ->
    functor (Dom: Domain.T with module E = Common.E) ->
struct

  module Dom = Dom
  module Common = Common
  module Df = DfPre_u (Common) (Dom)
  open Df
  open Common.E.BEnvNS.BOps
  open Common
  open Logger

  type data =
      {
        dfdata: Df.data;
        cfg: Cfg.t;
      }
  let setup ?allow_dynamic_reorderings ?(fname = Util.ps "LocPre_u") dd cfg =
    {
      dfdata = Df.setup ?allow_dynamic_reorderings ~fname dd;
      cfg;
    }
  let fname { dfdata } = Df.fname dfdata

  let apply ?allow_shortcut
      { dfdata = { dd = { ucvars; assertion; iguard; oldassert };
                   allow_dynamic_reorderings;
                   cguards; };
        cfg; } =
    let meet_iguard, guard = if oldassert
      then Some (Dom.meet_condition iguard), assertion
      else None, tt
    in
    let loc_arcs = PSette.fold begin fun (_, arc, pred, succ) acc ->
      let pre_guard, pre_updates = try PMappe.find pred acc with
        | Not_found -> ff, PMappe.empty Env.Symb.compare in
      let guard, updates = Arc.get_ass_equs E.env arc in
      let new_guard = pre_guard ||~ guard
      and new_updates = List.fold_left begin fun u (v, e) ->
        let e' = try PMappe.find v u with Not_found -> E.BEnvNS.POps.var v in
        PMappe.add v (E.BEnvNS.POps.ite guard e e') u
      end pre_updates updates in
      PMappe.add pred (new_guard, new_updates) acc
    end (Cfg.get_arcs cfg (fun _ -> true)) (PMappe.empty (-)) in
    let apply' = apply' ?allow_dynamic_reorderings ucvars cguards
      ?meet_iguard in
    let ti_timed = Log.ti_timed ~period:0.1 ~p:Log.Debug logger in
    fun ~lp ?lv x' ->
      PMappe.fold begin fun l (g, u) ->
        ti_timed "%tComputing non-controllable predecessors in location %d\
                    @<1>…" lp l;
        let inv = Loc.get_inv (Cfg.get_loc cfg l) in
        Log.d2 logger "%tl(%d) = %a" lp l E.BEnvNS.PPrt.pp_bexpr inv;
        Log.d2 logger "%tg(%d) = %a" lp l E.BEnvNS.PPrt.pp_bexpr g;
        Log.d2 logger "%tu(%d) = @[<v>%a@]" lp l E.BEnvNS.PPrt.pp_equmap u;
        Dom.join (apply' ~lp ?lv g (PMappe.bindings u)
                    ~meet_src_invar:(Dom.meet_condition inv) x')
      end loc_arcs Dom.bottom

end

(* --- *)

module CfPre_u
  = functor (CfData: CF_DATA) ->
    functor (Dom: Domain.T with module E = CfData.E) ->
struct

  module Dom = Dom
  module DfPre_u = DfPre_u (CfData) (Dom)
  module CfData = CfData
  module Common = CfData
  open CfData
  open Logger
  open Dom.E.BEnvNS.BAll

  type data =
      {
        cd: cfdata;
        fname: Util.pu;
        evols: (bexpr * equs) HashheI.t;
        cguards: Dom.t list;
        src_invars: dfunc HashheI.t;      (* arc → invariant of src location. *)
      }
  and dfunc = Dom.t -> Dom.t

  let fname { fname } = fname

  (* --- *)

  let setup ?(fname = Util.ps "Pre_u")
      ({ cfg'; dd = { assertion; cguards; oldassert } } as cd)
      =
    let arcs = Cfg.get_arcs cfg' (fun _ -> true) in
    let card = PSette.cardinal arcs in
    let evols = HashheI.create card and src_invars = HashheI.create card in
    PSette.iter
      (fun (i, a, _, s) -> let g, e = Arc.get_ass_equs Dom.E.env a in
                        let g = if oldassert then g &&~ assertion else g in
                        HashheI.add evols i (g, e);
                        let src_inv = Cfg.get_loc cfg' s |> Loc.get_inv in
                        HashheI.add src_invars i src_inv)
      arcs;
    let cguards = List.map (fun ai -> Dom.of_boolexpr !~ai) cguards in
    let src_invars = HashheI.map (fun _aid -> Dom.meet_condition) src_invars in
    { cd; fname; evols; src_invars; cguards }

  (* --- *)

  (* XXX: Dynamic partitioning would require adding a few update procedures *)

  (* --- *)

  (** [apply sd a x] computes in the abstract domain the states from which a
      non-controllable transition can lead to a state belonging to γ([x])
      through arc [a]. *)
  let apply { cd = { dd = { ucvars; assertion; iguard }; };
              evols; src_invars; cguards } =
    let meet_iguard = Dom.meet_condition iguard in
    let apply' = DfPre_u.apply' ucvars cguards ~meet_iguard in
    let ti_timed = Log.ti_timed ~period:0.1 ~p:Log.Debug3 logger in
    fun ~lp ?lv arcid x' ->
      let guard, equs = HashheI.find evols arcid
      and meet_src_invar = HashheI.find src_invars arcid in

      ti_timed "%tTraversing arc %d@<1>…" lp arcid;
      apply' guard equs ~meet_src_invar ~lp ?lv x'

end

(* -------------------------------------------------------------------------- *)

module type SYNTH_ALGOS = sig
  module Common: COMMON
  module Coreach_u: FP_STEP with module Dom.E = Common.E
  open Common.E.BEnvNS.BEnv
  type data = Coreach_u.data
  type coreach = ?allow_shortcut:bool -> data -> bexpr -> bexpr
  val basic: coreach
  val split: booldisj:bool -> coreach
  val coreach_only
    : ?coreach: coreach
    -> data
    -> Common.data
    -> ?deads: bool
    -> bexpr
    -> bexpr
  val apply
    : ?coreach: coreach
    -> data
    -> Common.data
    -> ?no_late_reorder: bool
    -> ?no_bnd: bool
    -> ?decomp: bexpr BddapronSynthesis.decomposition
    -> ?deads: bool
    -> bexpr
    -> bexpr option
end

(** Generic module for synthesis and construction of resulting controllers from
    our CFG representation *)
module Synth (Common: COMMON)
  (Coreach_u: FP_STEP with module Dom.E = Common.E) :
  (SYNTH_ALGOS with module Common = Common
               and module Coreach_u = Coreach_u) =
struct
  module Common = Common
  module Coreach_u = Coreach_u
  module Bool = BddapronSynthesis.Core (Coreach_u.Dom.E.BEnvNS)
  open Coreach_u
  open Common
  open E.BEnvNS.BOps
  open Logger

  type data = Coreach_u.data
  type coreach = ?allow_shortcut:bool -> data -> Env.boolexpr_t -> Env.boolexpr_t

  let basic ?allow_shortcut sd bad =
    let lp = Util.pu
    and lv = Util.pp "@<4>α(𝔉)" in
    let x = Dom.of_boolexpr bad in
    Log.d logger "%t = %a" lv Dom.print x;
    let x = apply ?allow_shortcut sd ~lp ~lv x in
    Log.d logger "%t(%t) = %a" (fname sd) lv Dom.print x;
    Dom.to_boolexpr x

  let split ~booldisj ?allow_shortcut sd bad =
    let module Coreach_u' = FpSplit (Logger) (Coreach_u) in
    let module CDU = DomExt.ConvexDisj (Dom) in
    let lp = Util.pc '#'
    and lv = Util.pp "@<5>α℘(𝔉)" and lvi = Util.pp "@<6>α℘(𝔉).%i" in
    let disj = CDU.abstract_convex_disjuncts_of ~booldisj ~name:"𝔉" bad in
    Log.d logger "%t = %a" lv CDU.pp disj;
    let abads = Coreach_u'.apply_mult ?allow_shortcut sd ~lp ~lvi disj in
    Log.d logger "%t(%t) = %a" (fname sd) lv CDU.pp abads;
    CDU.concretize_disj abads

  let bad0 { assertion; struth } ?(deads = false) bad =
    let bad =
      if not deads then
        bad ^~ struth
      else
        let deads = Sets.dead assertion in
        Log.d3 logger "@<4>𝔇 = %a" print deads;
        bad ||~ deads
    in
    Log.d3 logger "@<4>𝔉 = %a" print bad;
    bad

  let coreach_only ?(coreach=basic) sd data ?deads bad =
    let bad = bad0 data ?deads bad in
    if is_ff bad then begin
      Log.i logger "@[Skipping@ co-reachability@ analysis@ for@ obvious@ \
                      property.@]";
      ff
    end else begin
      Log.d logger "@[Beginning@ of@ least@ fixpoint@ computation.@]";
      let ibad = Log.roll (coreach ~allow_shortcut:false sd) bad in
      Log.d logger "@[End@ of@ least@ fixpoint@ computation.@]";
      ibad
    end

  let apply ?(coreach=basic) sd ({ evolutions; assertion; struth } as data)
      ?no_late_reorder ?no_bnd ?decomp ?deads bad =
    try
      let bad = bad0 data ?deads bad in
      if is_ff bad then begin
        Log.i logger "@[Skipping@ synthesis@ with@ obvious@ objective.@]";
        Some assertion
      end else begin
        Log.d logger "@[Beginning@ of@ least@ fixpoint@ computation.@]";
        let ibad = Log.roll (coreach sd) bad in
        Log.d logger "@[End@ of@ least@ fixpoint@ computation.@]";
        let good = !~ibad in
        Log.d3 logger "~^@[ = %a@ (= G)@]" print good;
        Some (Bool.make_controller ?no_reorder:no_late_reorder ?no_bnd
                ?decomp ~evolutions ~assertion good)
      end
    with
      | Exit -> None
end

(* -------------------------------------------------------------------------- *)

(** {2 Standard, over-approximating synthesis} *)

type std_param_t =
    {
      s_ws: int;
      s_wd: int;
      s_fwcs: int option;
      s_split_convex: bool;
      s_split_outer: bool;
      s_deads: bool;
      s_bdisj: bool;
      s_no_bnd: bool;
      s_oldassert: bool;
      s_initonly: bool;
    }

let make_std_param ~ws ~wd ?fwcs ~split_convex ~split_outer ~deads ~bdisj
    ?(initonly = false) ~no_bnd ~oldassert =
  { s_ws = ws; s_wd = wd; s_fwcs = fwcs;
    s_split_convex = split_convex;
    s_split_outer = split_outer;
    s_deads = deads; s_bdisj = bdisj; s_no_bnd = no_bnd;
    s_oldassert = oldassert;
    s_initonly = initonly; }

(** Synthesis algorithm based on standard abstract interpretation analysis
    method. *)
module Std (Dom: Domain.T) = struct

  module Logger = struct let logger = Log.mk ~level "sS" end
  module CfData = CfData (Logger) (Dom.E)
  module Coreach_u = CfFp (CfPre_u (CfData) (Dom))
  open Logger
  open CfData

  type param = std_param_t

  let descr { s_split_convex; s_deads } fmt =
    Format.fprintf fmt "%(%)ogico-numerical@ synthesis@ with@ %t"
      (if s_split_convex then "Split@ l" else "L") Dom.descr;
    if s_deads then Format.fprintf fmt "@ and@ capture@ of@ deadlocking@ states"

  let location_decomposition ~initonly Program.{ c_init = init; c_cfg = cfg } =
    let open Cfg in
    if initonly then
      let init_locs = get_locidset_by_inv E.env cfg init in
      let init_succs = PSette.fold begin fun (_, _, _, pred, succ) acc ->
        if PSette.mem pred init_locs
        then PSette.add succ acc
        else acc
      end (get_normal_arcs cfg) init_locs in
      PSette.fold (fun l -> get_loc cfg l |> Loc.get_inv |> List.cons)
        init_succs []
    else
      List.rev_map (fun l -> get_loc cfg l |> Loc.get_inv)
        (PSette.elements (get_locidset cfg))

  let synthesize
      ({ s_ws = ws; s_wd = wd; s_fwcs = fwcs;
         s_deads = deads; s_no_bnd = no_bnd;
         s_bdisj = booldisj; s_oldassert = oldassert;
         s_split_convex = split_conv;
         s_split_outer = split_outer;
         s_initonly = initonly })
      ((cs, _, ({ Program.c_final = bad; Program.c_cfg = cfg } as prog))
          as cfsystem) =

    if CtrlSpec.has_nc_vars cs then
      failwith "Synthesis with numerical controllables is not supported!";

    (* Setup internal data, and choose the appropriate algorithm *)
    let { dd } as cd = cfsetup ~oldassert cfsystem in
    let sd = Coreach_u.setup ~fname:(Util.ps "Coreach_u") ~ws ~wd ?fwcs cd in
    let decomp, synth =
      if Cfg.is_a_self_loop prog.Program.c_cfg then begin
        Log.i logger "@[Program@ is@ a@ self-loop:@ using@ \
                        single-fixpoint@ algorithm.@]";
        let module Synth = Synth (CfData) (Coreach_u) in
        let open Synth in
        None,
        apply ~coreach:(if split_conv then split ~booldisj else basic) sd
      end else begin
        Log.i logger "@[Program@ is@ not@ a@ self-loop:@ using@ \
                        double-fixpoint@ algorithm.@]";
        Some (`Manual (location_decomposition ~initonly prog)),
        if split_outer then begin
          Log.i logger "@[Computation@ of@ global@ non-controllable@ \
                          predecessors@ will@ be@ performed@ on@ each@ \
                          individual@ location.@]";
          let module LocPre_u = LocPre_u (CfData) (Dom) in
          let module LocCoreach_u = DblFp (Logger) (LocPre_u) (Coreach_u) in
          let module Synth = Synth (CfData) (LocCoreach_u) in
          let open Synth in
          let sd2 = LocCoreach_u.setup ~ws (LocPre_u.setup dd cfg) sd in
          apply ~coreach:(if split_conv then split ~booldisj else basic) sd2
        end else begin
          Log.i logger "@[Computation@ of@ global@ non-controllable@ \
                          predecessors@ will@ be@ perfomed directly@ on@ \
                          the@ data-flow@ representation.@]";
          let module DfPre_u = DfPre_u (CfData) (Dom) in
          let module DfCoreach_u = DblFp (Logger) (DfPre_u) (Coreach_u) in
          let module Synth = Synth (CfData) (DfCoreach_u) in
          let open Synth in
          let sd2 = DfCoreach_u.setup ~ws (DfPre_u.setup dd) sd in
          apply ~coreach:(if split_conv then split ~booldisj else basic) sd2
        end
      end
    in

    (* Launch synthesis *)
    (cs, synth dd ~no_late_reorder:oldassert ~no_bnd ?decomp ~deads bad, prog)

  (* --- *)

  let coreach
      ({ s_ws = ws; s_wd = wd; s_fwcs = fwcs;
         s_deads = deads; s_no_bnd = no_bnd;
         s_bdisj = booldisj; s_oldassert = oldassert;
         s_split_convex = split_conv;
         s_split_outer = split_outer;
         s_initonly = initonly })
      ((_, _, ({ Program.c_final = bad; Program.c_cfg = cfg } as prog))
          as cfsystem) =

    (* Setup internal data, and choose the appropriate algorithm *)
    let { dd } as cd = cfsetup ~oldassert cfsystem in
    let sd = Coreach_u.setup ~fname:(Util.ps "Coreach_u") ~ws ~wd ?fwcs cd in
    let decomp, coreach =
      if Cfg.is_a_self_loop prog.Program.c_cfg then begin
        Log.i logger "@[Program@ is@ a@ self-loop:@ using@ \
                        single-fixpoint@ algorithm.@]";
        let module Synth = Synth (CfData) (Coreach_u) in
        let open Synth in
        [],
        coreach_only ~coreach:(if split_conv then split ~booldisj else basic) sd
      end else begin
        Log.i logger "@[Program@ is@ not@ a@ self-loop:@ using@ \
                        double-fixpoint@ algorithm.@]";
        location_decomposition ~initonly prog,
        if split_outer then begin
          Log.i logger "@[Computation@ of@ global@ non-controllable@ \
                          predecessors@ will@ be@ performed@ on@ each@ \
                          individual@ location.@]";
          let module LocPre_u = LocPre_u (CfData) (Dom) in
          let module LocCoreach_u = DblFp (Logger) (LocPre_u) (Coreach_u) in
          let module Synth = Synth (CfData) (LocCoreach_u) in
          let open Synth in
          let sd2 = LocCoreach_u.setup ~ws (LocPre_u.setup dd cfg) sd in
          coreach_only ~coreach:(if split_conv then split ~booldisj else basic) sd2
        end else begin
          Log.i logger "@[Computation@ of@ global@ non-controllable@ \
                          predecessors@ will@ be@ perfomed directly@ on@ \
                          the@ data-flow@ representation.@]";
          let module DfPre_u = DfPre_u (CfData) (Dom) in
          let module DfCoreach_u = DblFp (Logger) (DfPre_u) (Coreach_u) in
          let module Synth = Synth (CfData) (DfCoreach_u) in
          let open Synth in
          let sd2 = DfCoreach_u.setup ~ws (DfPre_u.setup dd) sd in
          coreach_only ~coreach:(if split_conv then split ~booldisj else basic) sd2
        end
      end
    in

    (* Launch coreach *)
    let res = coreach dd ~deads bad in
    let open E.BEnvNS.BOps in
    List.fold_left (fun acc d -> acc ||~ (res &&~ d)) ff decomp, prog

end

(* -------------------------------------------------------------------------- *)

(** {2 Over-approximating co-reachability analysis with controllable variables} *)

type coreach_param_t =
    {
      cr_ws: int;
      cr_wd: int;
      cr_fwcs: int option;
      cr_split_convex: bool;
      cr_split_outer: bool;
      cr_deads: bool;
      cr_bdisj: bool;
      cr_oldassert: bool;
    }

let make_coreach_param ~ws ~wd ?fwcs ~split_convex ~split_outer ~deads ~bdisj
    ~oldassert =
  { cr_ws = ws; cr_wd = wd; cr_fwcs = fwcs;
    cr_split_convex = split_convex;
    cr_split_outer = split_outer;
    cr_deads = deads; cr_bdisj = bdisj;
    cr_oldassert = oldassert; }

module type COREACH = sig
  val apply
    : coreach_param_t
    -> CtrlSpec.t * Synthesis.controller * Program.cfprog_t
    -> Env.boolexpr_t
end

module Coreach (Dom: Domain.T) = struct

  module Logger = struct let logger = Log.mk ~level "cA" end
  module CfData = CfData (Logger) (Dom.E)
  module Coreach_u = CfFp (CfPre_u (CfData) (Dom))
  open Logger
  open CfData

  type param = coreach_param_t

  let descr { cr_split_convex; cr_deads } fmt =
    Format.fprintf fmt
      "%(%)ogico-numerical@ co-reachability@ analysis@ with@ %t"
      (if cr_split_convex then "Split@ l" else "L") Dom.descr;
    if cr_deads then Format.fprintf fmt "@ and@ capture@ of@ deadlocking@ states"

  let apply
      ({ cr_ws = ws; cr_wd = wd; cr_fwcs = fwcs;
         cr_deads = deads;
         cr_bdisj = booldisj; cr_oldassert = oldassert;
         cr_split_convex = split_conv;
         cr_split_outer = split_outer })
      ((cs, _, ({ Program.c_final = bad; Program.c_cfg = cfg } as prog))
          as cfsystem) =

    if CtrlSpec.has_nc_vars cs then
      failwith "Co-reachability with numerical controllables is not supported!";

    (* Setup internal data, and choose the appropriate algorithm *)
    let { dd } as cd = cfsetup ~oldassert cfsystem in
    let sd = Coreach_u.setup ~fname:(Util.ps "Coreach_u") ~ws ~wd ?fwcs cd in
    let coreach =
      if Cfg.is_a_self_loop prog.Program.c_cfg then begin
        Log.i logger "@[Program@ is@ a@ self-loop:@ using@ \
                        single-fixpoint@ algorithm.@]";
        let module Synth = Synth (CfData) (Coreach_u) in
        let open Synth in
        coreach_only ~coreach:(if split_conv then split ~booldisj else basic) sd
      end else begin
        Log.i logger "@[Program@ is@ not@ a@ self-loop:@ using@ \
                        double-fixpoint@ algorithm.@]";
        if split_outer then begin
          Log.i logger "@[Computation@ of@ global@ non-controllable@ \
                          predecessors@ will@ be@ performed@ on@ each@ \
                          individual@ location.@]";
          let module LocPre_u = LocPre_u (CfData) (Dom) in
          let module LocCoreach_u = DblFp (Logger) (LocPre_u) (Coreach_u) in
          let module Synth = Synth (CfData) (LocCoreach_u) in
          let open Synth in
          let sd2 = LocCoreach_u.setup ~ws (LocPre_u.setup dd cfg) sd in
          coreach_only ~coreach:(if split_conv then split ~booldisj else basic) sd2
        end else begin
          Log.i logger "@[Computation@ of@ global@ non-controllable@ \
                          predecessors@ will@ be@ perfomed directly@ on@ \
                          the@ data-flow@ representation.@]";
          let module DfPre_u = DfPre_u (CfData) (Dom) in
          let module DfCoreach_u = DblFp (Logger) (DfPre_u) (Coreach_u) in
          let module Synth = Synth (CfData) (DfCoreach_u) in
          let open Synth in
          let sd2 = DfCoreach_u.setup ~ws (DfPre_u.setup dd) sd in
          coreach_only ~coreach:(if split_conv then split ~booldisj else basic) sd2
        end
      end
    in

    coreach dd ~deads bad

end

(* -------------------------------------------------------------------------- *)

(** {2 Over-approximating synthesis that operates directly on the data-flow
    representation of the program} *)

type sfp_param_t =
    {
      sfp_ws: int;
      sfp_split_convex: bool;
      sfp_deads: bool;
      sfp_bdisj: bool;
      sfp_no_bnd: bool;
      sfp_oldassert: bool;
    }

let make_sfp_param ~ws ~split_convex ~deads ~bdisj ~no_bnd ~oldassert =
  { sfp_ws = ws; sfp_split_convex = split_convex; sfp_deads = deads;
    sfp_bdisj = bdisj; sfp_no_bnd = no_bnd; sfp_oldassert = oldassert }

(** Synthesis algorithm based on standard abstract interpretation analysis
    method, that operates on the data-flow representation of the program. *)
module Sfp (Dom: Domain.T) = struct

  module Logger = struct let logger = Log.mk ~level "sF" end
  module CfData = CfData (Logger) (Dom.E)
  module DfPre_u = DfPre_u (CfData) (Dom)
  module Coreach_u = DfFp (Logger) (DfPre_u)
  open Logger
  open CfData

  type param = sfp_param_t

  let descr { sfp_split_convex; sfp_deads } fmt =
    Format.fprintf fmt "%(%)ogico-numerical@ data-flow@ synthesis@ with@ %t"
      (if sfp_split_convex then "Split@ l" else "L") Dom.descr;
    if sfp_deads then Format.fprintf fmt "@ and@ capture@ of@ deadlocking@ states"

  let synthesize
      ({ sfp_ws = ws;
         sfp_deads = deads; sfp_no_bnd = no_bnd;
         sfp_split_convex = split_convex;
         sfp_bdisj = booldisj; sfp_oldassert = oldassert })
      ((cs, _, ({ Program.c_final = bad } as prog)) as cfsystem) =

    if CtrlSpec.has_nc_vars cs then
      failwith "Synthesis with numerical controllables is not supported!";

    (* Setup internal data, and choose the appropriate algorithm *)
    let { dd } as cd = cfsetup ~oldassert cfsystem in
    let sd0 = DfPre_u.setup dd in
    let sd = Coreach_u.setup ~fname:(Util.ps "Coreach_u") ~ws sd0 in
    let synth =
      let module Synth = Synth (CfData) (Coreach_u) in
      let open Synth in
      apply ~coreach:((if split_convex then split ~booldisj else basic)) sd
    in

    (* Launch synthesis *)
    (cs, synth dd ~no_late_reorder:oldassert ~no_bnd ~deads bad, prog)

  (* --- *)

  let coreach
      ({ sfp_ws = ws;
         sfp_deads = deads;
         sfp_split_convex = split_convex;
         sfp_bdisj = booldisj; sfp_oldassert = oldassert })
      ((_, _, ({ Program.c_final = bad } as prog)) as cfsystem) =

    (* Setup internal data, and choose the appropriate algorithm *)
    let { dd } as cd = cfsetup ~oldassert cfsystem in
    let sd0 = DfPre_u.setup dd in
    let sd = Coreach_u.setup ~fname:(Util.ps "Coreach_u") ~ws sd0 in
    let coreach =
      let module Synth = Synth (CfData) (Coreach_u) in
      let open Synth in
      coreach_only ~coreach:((if split_convex then split ~booldisj else basic)) sd
    in

    (* Launch coreach *)
    (coreach dd ~deads bad, prog)

end


(* -------------------------------------------------------------------------- *)

module type DISJ_FP_STEP = sig
  module DDom: DomExt.POWEXT
  include FP_STEP with module Dom := DDom
  val apply: data -> lp:Util.pu -> lvi:int Util.pp -> DDom.disj -> DDom.disj
end

(* --- *)

module DirectFp
  = functor (DDom: DomExt.POWEXT) ->
    functor (Step: CF_FP_STEP with type Dom.t = DDom.CDom.t) ->
struct
  module DDom = DDom
  include CfFpSplit (Step)
  let apply = apply_mult ~allow_shortcut:true
end

(* --- *)

module SplitFp
  = functor (DDom: DomExt.POWEXT) ->
    functor (Step: CF_FP_STEP with type Dom.t = DDom.t) ->
struct
  module DDom = DDom
  include CfFpSplit (Step)
  let apply cd ~lp ~lvi disj =
    List.rev_map (fun x -> DDom.of_disj [x]) disj |> apply_mult cd ~lp ~lvi |>
        List.rev_map DDom.to_disj |> List.flatten
end

(* --- *)

module DisjDblFp (Logger: Log.T)
  = functor (DfStep: FP_STEP) ->
    functor (IStep: DISJ_FP_STEP with module Common = DfStep.Common
                                 and  type DDom.t = DfStep.Dom.t) ->
struct

  module Common = IStep.Common
  module DDom = IStep.DDom
  module Dom = DDom
  open Common
  open Logger

  (* --- *)

  type params =
      {
        ws: int;
        emws: int;
      }
  type data =
      {
        params: params;
        fname: Util.pu option;
        ostep_data: DfStep.data;
        istep_data: IStep.data;
      }

  let setup ?fname ~ws ~emws ostep_data istep_data =
    { params = { ws; emws }; fname; ostep_data; istep_data }

  let fname { fname; ostep_data; istep_data } = match fname with
    | Some fname -> fname
    | None -> Util.pp2 "lfp(%t@<1>∘%t)"
        (DfStep.fname ostep_data) (IStep.fname istep_data)

  let apply ?(allow_shortcut = true)
      { params = { ws; emws }; ostep_data; istep_data } ~lp ?(lv = lv') =
    let ostep = DfStep.apply ~allow_shortcut ostep_data in
    let istep = IStep.apply istep_data in

    (** External fixpoint. [arch] gathers all already computed abstract
        values. *)
    let rec lfp j arch cur =
      let filter = true in
      let lp = pp2 "%t#%d: " lp j in
      let istep = istep ~lp:(pp2 "%t#%d." lp j) ~lvi:(Util.pp1 "%t%i" lv) in

      (* Filter the set of abstract values to avoid re-computing inner fixpoints
         from values lesser or equal than values previously considered; This is
         optional and does not reduce the number of global iterations, yet often
         greatly speeds up the computation. *)
      let incr = if filter then DDom.overapprox_diff cur arch else cur in

      if DDom.is_bottom incr
      then
        (Log.i logger "%t@[No@ new@ or@ increasing@ abstract@ value.@]" lp; cur)
      else
        let curl = DDom.to_disj incr in
        let arch = if filter then List.rev_append curl arch else arch in

        Log.i logger "%t@[%d@ new@ or@ increasing@ abstract@ values.@]\
                       " lp (List.length curl);
        Log.d logger "%t@<7>α(.) ⊒ %a" lp DDom.print incr;

        (* Join with inner fixpoint on current set of new or increasing abstract
           values. *)
        let next = istep curl |> DDom.join_disj cur in
        (* let curl'= coreach_u curl |> List.map BDom.to_disj in *)
        (* let next = List.fold_left DDom.join_disj cur curl' in *)
        (* Log.d logger "%t@<7>next ⊒ %a" lp DDom.print next; *)

        (* Compute the union with unavoiding predecessors to take all disjuncts
           into account at once. *)
        let next = DDom.join cur (ostep ~lp next) in
        Log.d3 logger "%t@<7>α(.) ⊒ %a" lp DDom.print next;

        let next =
          try
            if j >= ws then
              let cur, next =
                if j >= emws
                then DDom.widening_em_connect ~lp cur next
                else (Log.i logger "%t@[Postponing@ Egli-Milner@ ordering@ \
                                        enforcement.@]" lp; cur, next)
              in
              DDom.widening_em ~lp cur next
            else
              (Log.i logger "%t@[Postponing@ disjunctive@ widening.@]" lp;
               let next = DDom.join cur next in
               if allow_shortcut && DDom.is_top next then raise Exit else next)
          with
            | Exit -> Log.e logger "%tCoreach_u(.) = ⊤" lp; raise Exit
        in

        lfp (succ j) arch next
    in

    lfp 1 []

end

(* -------------------------------------------------------------------------- *)

(** {2 Deadlock-avoiding Over-approximating synthesis} *)

type df_param_t =
    {
      sd_ws: int;
      sd_wd: int;
      sd_fwcs: int option;
      sd_ws2: int;
      sd_emws: int;
      sd_pwext: bool;
      sd_deads: bool;
      sd_no_bnd: bool;
      sd_oldassert: bool;
    }

let make_df_param ~ws ~wd ?fwcs ~ws2 ~emws ~pwext ~deads ~no_bnd ~oldassert =
  { sd_ws = ws; sd_wd = wd; sd_fwcs = fwcs; sd_ws2 = ws2; sd_emws = emws;
    sd_pwext = pwext; sd_deads = deads; sd_no_bnd = no_bnd;
    sd_oldassert = oldassert; }

module type DF_FP_PARAMS = DomExt.FP_PARAMS

(** Disjunctive synthesis algorithm based on abstract interpretation
    analysis. *)
module Df (BaseDom: Domain.T) (Params: DF_FP_PARAMS) = struct

  module Logger = struct let logger = Log.mk ~level "sD" end
  module CfData = CfData (Logger) (BaseDom.E)
  module DDom = DomExt.FinPowset (BaseDom) (Params)
  open Logger
  open CfData

  type param = df_param_t

  let descr { sd_pwext; sd_deads } fmt =
    Format.fprintf fmt "Disjunctive@ logico-numerical@ synthesis@ with@ %t%t"
      (if sd_pwext then pp "(powerset@ extension@ of)@ " else pu)
      BaseDom.descr;
    if sd_deads then Format.fprintf fmt "@ and@ capture@ of@ deadlocking@ states"

  (* --- *)

  module type DISJ_FP_STEP_INNER_CF = sig
    include DISJ_FP_STEP with module DDom = DDom
    val setup: ?fname:Util.pu -> ws:int -> wd:int -> ?fwcs:int -> cfdata -> data
  end

  (* --- *)

  let synthesize
      ({ sd_ws = ws; sd_wd = wd; sd_fwcs = fwcs;
         sd_ws2 = ws2; sd_emws = emws;
         sd_deads = deads; sd_no_bnd = no_bnd;
         sd_oldassert = oldassert } as param)
      ((cs, _, ({ Program.c_final = bad } as prog)) as cfsystem) =

    if CtrlSpec.has_nc_vars cs then
      failwith "Synthesis with numerical controllables is not supported!";

    (* Inner coreach operates on control-flow graph *)
    let (module CfCoreach_u: DISJ_FP_STEP_INNER_CF) =
      if not param.sd_pwext
      then (module DirectFp (DDom) (CfPre_u (CfData) (BaseDom)))
      else (module SplitFp (DDom) (CfPre_u (CfData) (DDom)))
    in

    (* Outter step operates on data-flow *)
    let module DfPre_u = DfPre_u (CfData) (DDom) in

    (* Merge inner and outter steps *)
    let module Coreach_u = DisjDblFp (Logger) (DfPre_u) (CfCoreach_u) in
    let module Synth = Synth (CfData) (Coreach_u) in

    (* Setup internal data *)
    let { dd } as cd = cfsetup ~oldassert cfsystem in
    let cfd = CfCoreach_u.setup ~fname:(Util.ps "Coreach_u")
      ~ws ~wd ?fwcs cd
    and dfd = DfPre_u.setup dd in
    let dd2 = Coreach_u.setup ~fname:(Util.ps "Coreach_u@<1>℘")
      ~ws:ws2 ~emws dfd cfd in

    (* Launch synthesis *)
    (cs, Synth.apply dd2 dd ~no_late_reorder:oldassert ~no_bnd ~deads bad, prog)

  (* --- *)

  let coreach
      ({ sd_ws = ws; sd_wd = wd; sd_fwcs = fwcs;
         sd_ws2 = ws2; sd_emws = emws;
         sd_deads = deads;
         sd_oldassert = oldassert } as param)
      ((_, _, ({ Program.c_final = bad } as prog)) as cfsystem) =

    (* Inner coreach operates on control-flow graph *)
    let (module CfCoreach_u: DISJ_FP_STEP_INNER_CF) =
      if not param.sd_pwext
      then (module DirectFp (DDom) (CfPre_u (CfData) (BaseDom)))
      else (module SplitFp (DDom) (CfPre_u (CfData) (DDom)))
    in

    (* Outter step operates on data-flow *)
    let module DfPre_u = DfPre_u (CfData) (DDom) in

    (* Merge inner and outter steps *)
    let module Coreach_u = DisjDblFp (Logger) (DfPre_u) (CfCoreach_u) in
    let module Synth = Synth (CfData) (Coreach_u) in

    (* Setup internal data *)
    let { dd } as cd = cfsetup ~oldassert cfsystem in
    let cfd = CfCoreach_u.setup ~fname:(Util.ps "Coreach_u")
      ~ws ~wd ?fwcs cd
    and dfd = DfPre_u.setup dd in
    let dd2 = Coreach_u.setup ~fname:(Util.ps "Coreach_u@<1>℘")
      ~ws:ws2 ~emws dfd cfd in

    (* Launch coreach *)
    (Synth.coreach_only dd2 dd ~deads bad, prog)

end

(* -------------------------------------------------------------------------- *)

module EnvUpgrade (E: Env.T) = struct
  include E.BEnvNS.BEnvExt

  let maybe_upgrade cs k cfprog eu =
    upgrade CtrlSpec.apply_env_permutation eu cs,
    upgrade BddapronUtil.permute_boolexpr eu k,
    upgrade Program.apply_env_permutation_to_cfprog eu cfprog

  let maybe_upgrade' cs k cfprog eu =
    upgrade CtrlSpec.apply_env_permutation eu cs,
    upgrade' BddapronUtil.permute_boolexpr eu k,
    upgrade Program.apply_env_permutation_to_cfprog eu cfprog
end

(* -------------------------------------------------------------------------- *)

(** {2 Boolean synthesis} *)

type bool_param_t =
    {
      b_reach: bool;
      b_reachp: Env.boolexpr_t option;
      b_attr: bool;
      b_attrp: Env.boolexpr_t option;
      b_deads: bool;
      b_no_bnd: bool;
      b_rp: BddapronReorderPolicy.config;
      b_oldassert: bool;
    }

let make_bool_param b_reach b_reachp b_attr b_attrp ~deads ~no_bnd ~rp
    ~oldassert =
  {
    b_reach = b_reach || b_reachp <> None; b_reachp;
    b_attr  = b_attr  || b_attrp  <> None; b_attrp;
    b_deads = deads; b_no_bnd = no_bnd;
    b_rp = rp; b_oldassert = oldassert;
  }

let acc_numequs acc = function
  | (_, `Apron _ as e) -> e :: acc
  | _ -> acc

(** Exact synthesis algorithm (may not terminate if involving numerical state
    variables) *)
module Bool (E: Env.T) = struct
  module Sets = Sets (Logger) (E)
  module Bool = BddapronSynthesis.Core (E.BEnvNS)
  open E.BEnvNS.BOps

  type param = bool_param_t

  let descr { b_reach; b_attr; b_deads } fmt =
    Format.fprintf fmt "Boolean@ synthesis";
    if b_reach || b_attr || b_deads then Format.fprintf fmt "@ with:";
    if b_reach then Format.fprintf fmt "@ reachability;@;";
    if b_attr then Format.fprintf fmt "@ attractivity;@;";
    if b_deads then Format.fprintf fmt "@ capture@ of@ deadlocking@ states;"

  (* --- *)

  let synthesize
      ({ b_reach; b_reachp; b_attr; b_attrp; b_deads; b_no_bnd = no_bnd;
         b_rp = rp; b_oldassert = oldassert; })
      (cs, assertion,
       ({ Program.c_disc_equs = evolutions;
          Program.c_init = init; Program.c_final = final;
          Program.c_reach = reach; Program.c_attract = attr } as prog)) =

    let extra_prop name invariant prop p bp =
      let chk_prop p =
        if is_ff (p &&~ invariant) then
          failwith (Printf.sprintf "Incompatible sets of %s and invariant \
                                    states!" name);
        Some p
      and non_prop () = Log.w logger "@[Disabled@ one@ requested@ \
                                        computation:@ %s@ =@ tt.@]" name
      in
      match p, bp with
        | true, None when is_tt prop -> non_prop (); None
        | true, None -> chk_prop prop
        | true, Some p when is_tt p -> non_prop (); None
        | true, Some p -> chk_prop p
        | _ -> None
    in

    if CtrlSpec.has_nc_vars cs then begin
      Log.w logger "@[Boolean@ synthesis@ with@ numerical@ controllables@ is@ \
                      not@ supported:@ skipping.@]";
      cs, Some assertion, prog
    end else begin

      let invariant = !~final in
      let reach = extra_prop "reachable" invariant reach b_reach b_reachp in
      let attract = extra_prop "attractive" invariant attr b_attr b_attrp in

      (* Incorporate deadlocking states in 𝔉 (useless, but as we provide the
         option…). *)
      let deads = if b_deads then Sets.dead assertion else ff in
      let bad = final ||~ deads in

      if is_ff bad && attract = None && reach = None then begin
        Log.i logger "@[Skipping@ synthesis@ with@ obvious@ objectives.@]";
        cs, Some assertion, prog
      end else begin
        begin
          (* XXX: Maybe too simple! *)
          match List.fold_left acc_numequs [] evolutions with
            | equs when BddapronUtil.depends_on_some_var_equs
                E.env.Env.env E.env.Env.cond equs E.env.Env.ns_vars ->
                Log.w logger "@[Some@ numerical@ state@ variables@ depend@ on@ \
                              others:@\n\
                              Boolean@ synthesis@ may@ not@ terminate!@]";
            | _ -> ()
        end;

        let be = Bool.make_boolenv (CtrlSpec.buc_groups cs) in
        let k =
          match
            Bool.synth be ~rp ~oldassert ~evolutions ~assertion
              ~init ~reach ~attract ~bad
          with
            | Some g ->
                Some (Bool.make_controller ~no_bnd ~evolutions ~assertion g)
            | None -> None
        in
        cs, k, prog
      end
    end
end

(* -------------------------------------------------------------------------- *)

(** {2 Relational Boolean synthesis (SCT-inspired)} *)

type relb_param_t =
    {
      rb_rp: BddapronReorderPolicy.config;
      rb_reach: bool;
      rb_oldassert: bool;
      rb_decomp: [ `Disj ] option;
      rb_check: bool;
      rb_no_bnd: bool;
      rb_dirty: bool;
    }

let make_relb_param ?(dirty = false) ?decomp ?(check = false)
    ~rp ~reach ~oldassert ~no_bnd =
  {
    rb_rp = rp;
    rb_reach = reach;
    rb_decomp = decomp;
    rb_oldassert = oldassert;
    rb_check = check;
    rb_no_bnd = no_bnd;
    rb_dirty = dirty;
  }

module RelBool (E: Env.T) = struct
  module EnvExt = EnvUpgrade (E)
  module Implem = BddapronSynthesis.Relational (E.BEnvNS)
  open E.BEnvNS.BOps
  open EnvExt
  open Implem

  type param = relb_param_t

  let descr _ fmt =
    Format.fprintf fmt "Relational Boolean@ synthesis"

  (* --- *)

  let infinite = List.exists (function
    | _, #Bdd.Repr.t -> false
    | _ -> true)

  (* --- *)

  let synthesize
      ({ rb_rp = rp;
         rb_reach;
         rb_no_bnd = no_bnd;
         rb_decomp = decomp;
         rb_oldassert = oldassert;
         rb_check = check;
         rb_dirty = dirty })
      (cs, assertion,
       ({ Program.c_disc_equs = evolutions;
          Program.c_final = final; } as prog)) =

    if CtrlSpec.has_nc_vars cs then begin
      Log.w logger "@[Boolean@ synthesis@ with@ numerical@ controllables@ is@ \
                      not@ supported:@ skipping.@]";
      cs, Some assertion, prog
    end else if is_ff final && is_tt prog.Program.c_reach then begin
      Log.i logger "@[Skipping@ synthesis@ with@ obvious@ objectives.@]";
      cs, Some assertion, prog
    end else begin
      begin
        match List.fold_left acc_numequs [] evolutions with
          | equs when BddapronUtil.depends_on_some_var_equs
              E.env.Env.env E.env.Env.cond equs E.env.Env.ns_vars ->
              Log.w logger "@[Some@ numerical@ state@ variables@ depend@ on@ \
                              others:@\n\
                              Boolean@ synthesis@ may@ not@ terminate!@]";
          | _ -> ()
      end;

      let data, eu = setup E.env.Env.s_vars in
      let cs, assertion, prog = maybe_upgrade cs assertion prog eu in
      let { Program.c_disc_equs = evolutions;
            Program.c_final = bad;
            Program.c_reach = reachable;
            Program.c_init = init } = prog in
      let reachable = if rb_reach then reachable else tt in
      let target = (* if infinite evolutions then *) `SafeStates (* else `Controller *) in
      let be = Implem.Core.make_boolenv (CtrlSpec.buc_groups cs) in
      let res = synth data ~be ~rp ~oldassert ~check ?decomp
        ~evolutions ~assertion ~reachable ~init ~bad target in
      let k = match res, target with
        | None, _ -> None
        | k, `Controller -> k
        | Some g, `SafeStates ->
            Some (Implem.Core.make_controller ~no_bnd ~evolutions ~assertion g)
      in
      let eu = dispose ~dirty data in
      maybe_upgrade' cs k prog eu
    end
end

(* -------------------------------------------------------------------------- *)

(** {2 Limited lookahead synthesis} *)

type lla_param_t =
    {
      lla_k: int;
      lla_be: bool;
      lla_recov: int;
      lla_defer: [ `None | `Grow of bool | `Shrink ];
      lla_dirty: bool;
      lla_ndr: bool;
    }

let make_lla_param ?(dirty = false) ?(ndr = false) ?(best_effort = false)
    ?(recov = 0) ?(defer = `None) k =
  {
    lla_k = k;
    lla_be = best_effort;
    lla_recov = recov;
    lla_defer = defer;
    lla_dirty = dirty;
    lla_ndr = ndr;
  }

module LLA (E: Env.T) = struct
  module Logger = struct let logger = Log.mk ~level "sL" end
  module UnfoldPrefix =
    Unfold.Prefix (DomExt.DefaultElimUtilsParams) (Logger) (E)
  module EnvExt = EnvUpgrade (E)
  open UnfoldPrefix
  open Logger
  open Elim
  open E.BEnvNS
  open E.BEnvNS.BAll
  open EnvExt

  type param = lla_param_t

  let descr { lla_k } fmt =
    Format.fprintf fmt "%u-step@ lookahead@ synthesis" lla_k

  (* --- *)

  let gf: Util.ufmt = "@<1>𝔊"
  let kf: Util.ufmt = "K"
  let la = Util.pp "A"
  let lk = Util.pp kf
  let vxf = Util.pp2 "%(%)%i\""
  let v0f = Util.pp2 "%(%)%i."
  let v1f = Util.pp2 "%(%)%i'"
  let vkf = Util.pp2 "%(%)%i"

  type k = repr * Util.pu
  let emptyk = repr ff, Util.pp "@<1>∅"

  module type KBUILD = sig
    type acc
    val push: k Lazy.t -> int -> acc -> acc
    val init: acc
    val finalize: acc -> k * k
  end

  module type KBUILD_ARGS = sig
    val ucks: L.uc_data
    val ais: repr list
  end

  module type KBUILDER = functor (A: KBUILD_ARGS) -> KBUILD

  module GrowK (A: KBUILD_ARGS): KBUILD = struct
    open A
    type acc = (k Lazy.t * int) list
    let init: acc = []
    let push gi1 i (acc: acc) = (gi1, i) :: acc
    let finalize acc =
      let growk = growk ucks ais in
      let k, (kk, _) = List.fold_left begin fun ((k, lk), (kk, ki)) (g1, i) ->
        let g1, g1f = Lazy.force g1 in
        (growk ~ld:(vxf kf i) ~lk0:(v0f kf i) ~lk k ~lr1:g1f g1,
         if i > ki then (g1, g1f), i else kk, ki)
      end (emptyk, (emptyk, 0)) acc in
      k, kk
  end

  module ShrinK (A: KBUILD_ARGS): KBUILD = struct
    open A
    type acc = k * (k * int)
    let init: acc = emptyk, (emptyk, 0)
    let shrink = shrink ucks ais
    let push gi1 i ((k, lk), (kk, ki)) =
      let g1, g1f = Lazy.force gi1 in
      (shrink ~ld:(vxf kf i) ~lk k ~lr0:(v0f gf i) ~lr1:g1f g1,
       if i > ki then (g1, g1f), i else kk, ki)
    let finalize ((k, (kk, _)): acc) = k, kk
  end

  (* --- *)

  let synthesize
      { lla_k = k; lla_be; lla_recov; lla_defer;
        lla_dirty; lla_ndr }
      (cs, asser, cfprog0)
      =

    let rs = CuddUtil.dynamic_reordering_save_n_stop_if lla_ndr BEnv.env in

    let fvars = support cfprog0.Program.c_final in
    let avars = support asser in
    let varsreq = setup_kvars cfprog0.Program.c_disc_equs avars fvars k in

    let cs, k, cfprog = if PMappe.is_empty varsreq then begin
      Log.i logger "@[Non-controllable@ lookahead:@ skipping@ synthesis@ step.\
                    @]";
      (cs, Some asser, cfprog0)
    end else begin
      Log.d logger "@[Extending@ environment@ with@ extra@ state@ and@ primed@ \
                      variables.@]";
      let kvars, pvars, oe, eu = decl_vars varsreq in
      assert (PMappe.is_empty pvars || L.prime_oflw);
      let cs, a, cfprog = maybe_upgrade cs asser cfprog0 eu in
      let { Program.c_disc_equs = evols; Program.c_final = bad;
            Program.c_init = init } = cfprog in
      let evols = simplify_equs a evols in
      assert (CuddUtil.fullcheck BEnv.env);

      let ucks = uc_vars cs kvars in
      let ais = mkai kvars evols a (List.length ucks - 1) |> List.map repr in
      let a = repr a in
      let g = !~bad in

      assert (List.length ucks == List.length ais);
      (* Log.d logger "A = %a" pp_repr a; *)
      Log.d logger "%(%) = %a" gf pp_bexpr g;

      let prefix1_c i gi =
        let ld = vxf gf i and lr1 = v1f gf i and lr = vkf gf i in
        Log.d logger "%t = %a" lr pp_repr gi;
        prefix1_c ucks ais a ~ld ~lr1 ~lr i gi
      in

      let defer (module KBDR: KBUILDER) prefix1_c =
        let module KB = KBDR (struct let ucks = ucks and ais = ais end) in
        let rec recur acc prev_g prev_gi = function
          | i when i > k -> KB.finalize acc
          | i -> let g' = L.lookahead_bexpr_incr kvars evols prev_g in
                 let gi = prev_gi &&~ g' in
                (* assert (gi = L.sum_bexpr kvars evols (&&~) tt i g); *)
                 recur (KB.push (prefix1_c i (repr gi)) i acc) g' gi (succ i)
        in
        fun g -> recur KB.init g tt 1
      and prefix1_c' ?(l = false) i gi = match l with
        | true -> lazy (prefix1_c i gi)
        | false -> let gi1 = prefix1_c i gi in lazy gi1
      in

      let (k, kf), (kk, kkf) = match lla_defer with
        | `None -> let gk = L.sum_bexpr kvars evols (&&~) tt k g |> repr in
                   let k = prefix1_c k gk in k, k
        | `Grow l -> defer (module GrowK) (prefix1_c' ~l) g
        | `Shrink -> defer (module ShrinK) prefix1_c' g
      in

      let k, kf = match lla_be with                            (* best-effort *)
        | false -> k, kf
        | true -> (Log.i logger "@[Computing@ best-effort@ controller@<1>…@]";
                   mkbe ucks ais a ~lku:(Util.pp "Ku") ~lk:kf k)
      in

      let k, kf = match lla_recov with                          (* recovering *)
        | i when i <= 0 ->
            Log.d logger "%t = %t" lk kf;
            k, lk
        | i ->
            if i <> 1
            then Log.w logger "@[Recovery@ over@ more@ than@ 1@ steps@ is@ \
                                 not@ supported@ yet.@ Forcing@ 1.@]";
            let le = Util.pc 'E' in
            let lgoal = Some le in
            Log.i logger "@[Computing@ recovering@ transitions@<1>…@]";
            let d = prefix01_c ucks ais ~lgoal ~ld:(Util.pc 'D') ~ls:kkf kk in
            let e = L.lookahead_bexpr kvars evols 1 (retr d) |> repr in
            let e = Log.roll (fun e -> inter (neg d) e |> inter a) e in
            Log.d logger "E@[ %t= A ∩ ¬D ∩ D|1%t@]\
                         " (fun f -> log3 "= %a@ (" f pp_repr e) (log3 ")");
            mkrecov ucks ais a ~ld:lk ~lk:kf k ~le e
      in

      let k =
        let k_n_inter = inter k (repr init) in
        let success = not (is_bottom k_n_inter) in
        Log.d logger "@[%t@<11> ∩ Θ. ≠ ∅ ?@[ = %B@ (= success)@]@]" kf success;
        if success
        then (let k = retr k in
              Log.d2 logger "K = @[%a@]" print k;
              Some k)
        else (Log.d3 logger "@<5>Θ. = @[%a@]" pp_bexpr init;
              Log.i logger "@[%t@<8> ∩ Θ. = %a@]" kf pp_repr k_n_inter;
              None)
      in

      if lla_dirty then begin
        Log.d logger "@[Leaving@ environment@ as@ is@ as@ requested@ \
                        (dirty=true).@]";
        cs, k, cfprog
      end else begin
        Log.d logger "@[Restoring@ environment.@]";
        let eu = restore_environment oe in
        maybe_upgrade' cs k cfprog eu
      end
    end in

    CuddUtil.dynamic_reordering_restore BEnv.env rs;

    cs, k, cfprog

end

(* -------------------------------------------------------------------------- *)
